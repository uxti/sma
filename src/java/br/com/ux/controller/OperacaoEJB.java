/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Operacao;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class OperacaoEJB extends Conexao {

    public void salvar(Operacao operacao, String usuario) {
        if (operacao.getIdOperacao() != null) {
            Auditoria a = new Auditoria();
            a.setTabela("Operacao");
            a.setDescricao("Editou o registro " + operacao.getDescricao() + "");
            a.setDtHora(new Date());
            a.setUsuario(usuario);
             getEm().merge(a);
        } else {
            Auditoria a = new Auditoria();
            a.setTabela("Operacao");
            a.setDescricao("Inseriu o registro " + operacao.getDescricao());
            a.setDtHora(new Date());
            a.setUsuario(usuario);
             getEm().merge(a);
        }
         getEm().merge(operacao);
    }

    public Operacao selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select p From Operacao p where p.idOperacao = :id");
        query.setParameter("id", id);
        return (Operacao) query.getSingleResult();
    }
    
    public Operacao selecionarOperacao(String usuario) {
        Query query =  getEm().createQuery("Select p From Operacao p where p.tipo = :id");
        query.setParameter("id", "R");
        return (Operacao) query.getSingleResult();
    }

    public void excluir(Operacao operacao, String usuario) {
        Auditoria a = new Auditoria();
        a.setTabela("Operacao");
        a.setDescricao("Excluiu o registro "+operacao.getDescricao());
        a.setDtHora(new Date());
        a.setUsuario(usuario);
         getEm().merge(a);
        operacao =  getEm().getReference(Operacao.class, operacao.getIdOperacao());
         getEm().remove(operacao);
    }

    public List<Operacao> listarOperacoes() {
        return  getEm().createQuery("Select p From Operacao p").getResultList();
    }
}
