/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Emails;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class EmailsEJB extends Conexao {

    UsuarioSessao us = new UsuarioSessao();

    public void Salvar(Emails emails) {
        String acao = "";
        if (emails.getIdEmail() != null) {
            getEm().merge(emails);
            acao = "insert";
        } else {
            getEm().merge(emails);
            acao = "update";
        }
        getEm().merge(BCRUtils.criarAuditoria(acao, emails.getAssunto(), us.retornaUsuario(), new Date(), "Emails"));
    }

    public void atualizar(Emails emails) {
        getEm().merge(emails);
        getEm().merge(BCRUtils.criarAuditoria("update", emails.getAssunto(), us.retornaUsuario(), new Date(), "Emails"));
    }

    public void Excluir(Emails emails) {
        emails = getEm().getReference(Emails.class, emails.getIdEmail());
        getEm().remove(emails);
        getEm().merge(BCRUtils.criarAuditoria("delete", emails.getAssunto(), us.retornaUsuario(), new Date(), "Emails"));
    }

    public List<Emails> listarEmails() {
        return getEm().createQuery("Select e From Emails e ").getResultList();
    }

    public Emails selecionarPorID(Integer id) {
        Query query = getEm().createQuery("Select e From Emails e where e.idEmail = :id");
        query.setParameter("id", id);
        return (Emails) query.getSingleResult();
    }
}
