/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Motivo;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author BrunoS
 */
@Stateless
public class MotivoEJB extends Conexao {

    public void Salvar(Motivo motivo, String usuario) {
        Auditoria auditoria = new Auditoria();
        if (motivo.getIdMotivo() != null) {
            getEm().merge(motivo);
            auditoria.setTabela("Motivo");
            auditoria.setDescricao("Alteração na tabela Motivo no registro " + motivo.getDescricao());
            auditoria.setDtHora(new Date());
            auditoria.setUsuario(usuario);
            getEm().merge(auditoria);
        } else {
            getEm().merge(motivo);
            auditoria.setTabela("Motivo");
            auditoria.setDescricao("Inserção na tabela Motivo do registro " + motivo.getDescricao());
            auditoria.setDtHora(new Date());
            auditoria.setUsuario(usuario);
            getEm().merge(auditoria);
        }
    }

    public void Excluir(Motivo motivo, String usuario) {
        motivo = getEm().getReference(Motivo.class, motivo.getIdMotivo());
        getEm().remove(motivo);
        getEm().merge(BCRUtils.criarAuditoria("delete", motivo.getDescricao(), usuario, new Date(), "Motivo"));
    }

    public List<Motivo> listarMotivos() {
        return getEm().createQuery("Select m From Motivo m").getResultList();
    }
    
    public List<Motivo> listarMotivosPorTipo(String tipo) {
        Query query = getEm().createQuery("Select p From Motivo p where (p.tipo = :tipo OR p.tipo = 'Todos')");
        query.setParameter("tipo", tipo);
        return query.getResultList();
    }
    

    public Motivo selecionarPorID(Integer id, String usuario) {
        Query query = getEm().createQuery("Select m From Motivo m where m.idMotivo = :id");
        query.setParameter("id", id);
        return (Motivo) query.getSingleResult();
    }

    public List<Motivo> listarLazy(int primeiro, int qtd, String clausula) {
        Query query = getEm().createQuery("Select p From Motivo p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistro(String clausula) {
        Query query = getEm().createQuery("Select COUNT(p.idMotivo) From Motivo p " + clausula);
        return (Number) query.getSingleResult();
    }
}
