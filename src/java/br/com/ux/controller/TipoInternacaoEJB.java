/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.TipoInternacao;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author BrunoS
 */
@Stateless
public class TipoInternacaoEJB extends Conexao {

    public void Salvar(TipoInternacao tipoInternacao, String usuario) {
        Auditoria auditoria = new Auditoria();
        if (tipoInternacao.getIdTipoInternacao() != null) {
             getEm().merge(tipoInternacao);
            auditoria.setTabela("TipoInternacao");
            auditoria.setDescricao("Alteração na tabela TipoInternacao no registro " + tipoInternacao.getDescricao());
            auditoria.setDtHora(new Date());
            auditoria.setUsuario(usuario);
             getEm().merge(auditoria);
        } else {
             getEm().merge(tipoInternacao);
            auditoria.setTabela("TipoInternacao");
            auditoria.setDescricao("Inserção na tabela TipoInternacao do registro " + tipoInternacao.getDescricao());
            auditoria.setDtHora(new Date());
            auditoria.setUsuario(usuario);
             getEm().merge(auditoria);
        }
    }

    public void Excluir(TipoInternacao tipoInternacao, String usuario) {
        tipoInternacao =  getEm().getReference(TipoInternacao.class, tipoInternacao.getIdTipoInternacao());
         getEm().remove(tipoInternacao);
        Auditoria a = new Auditoria();
        a.setTabela("TipoInternacao");
        a.setDescricao("Exclusão na tabela TipoInternacao referente ao tipoInternacao " + tipoInternacao.getDescricao() + " código " + tipoInternacao.getDescricao());
        a.setDtHora(new Date());
        a.setUsuario(usuario);
         getEm().merge(a);
    }

    public List<TipoInternacao> listarTipoInternacoes() {
        return  getEm().createQuery("Select p From TipoInternacao p").getResultList();
    }

    public TipoInternacao selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select p From TipoInternacao p where p.idTipoInternacao = :id");
        query.setParameter("id", id);
        return (TipoInternacao) query.getSingleResult();
    }

    public List<TipoInternacao> listarTipoInternacoesLazyMode(int primeiro, int qtd) {
        Query query =  getEm().createQuery("Select p From TipoInternacao p");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<TipoInternacao> listarTipoInternacoesLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select p From TipoInternacao p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarTipoInternacoesRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(p.idTipoInternacao) From TipoInternacao p " + clausula);
        return (Number) query.getSingleResult();
    }
}
