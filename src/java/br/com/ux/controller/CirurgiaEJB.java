/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Orcamento;
import br.com.ux.model.Repasse;
import br.com.ux.model.ServicoItemCirurgiaItem;
import br.com.ux.model.TipoInternacao;
import br.com.ux.pojo.Grafico;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class CirurgiaEJB extends Conexao {

    @EJB
    UsuarioEJB uEJB;
    private UsuarioSessao us = new UsuarioSessao();
    @EJB
    ContaCorrenteEJB ccEJB;

    public void Salvar(Cirurgia cirurgia, List<CirurgiaItem> cirurgiaItens, List<ServicoItemCirurgiaItem> itensDosItens, List<Repasse> repasses) {
        if (cirurgia.getIdCirurgia() == null) {
            getEm().persist(cirurgia);
            getEm().flush();
            getEm().refresh(cirurgia);
            getEm().merge(BCRUtils.criarAuditoria("insert", "ID Fechamento: " + cirurgia.getIdCirurgia() + " - " + cirurgia.getIdProcedimento().getDescricao() + " - " + cirurgia.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Fechamento"));
            for (CirurgiaItem ci : cirurgiaItens) {
                ci.setIdCirurgia(cirurgia);
                getEm().merge(ci);
                getEm().merge(BCRUtils.criarAuditoria("insert", "ID Fechamento: " + cirurgia.getIdCirurgia() + " - " + ci.getIdServico().getDescricao() + " - " + ci.getValorTotal(), us.retornaUsuario(), new Date(), "Itens do Fechamento"));
                // BUG dos registros duplicados.
                for (ServicoItemCirurgiaItem si : itensDosItens) {
                    //si.setIdCirurgiaItem(ci);
                    si.setIdCirurgia(cirurgia);
                    getEm().merge(si);
                    for (Repasse repasse : repasses) {
                        if ((si.getIdServicoItem() == repasse.getIdSici().getIdServicoItem()) && (si.getIdCirurgia() == null) && (si.getIdCirurgiaItem() == null)) {
                            repasse.setIdSici(si);
                            si.setIdRepasse(repasse);
                        }
                    }
                }
            }

            List<ContaCorrente> contas = new ArrayList<>();

            for (Repasse r : repasses) {
                if (!r.getIdCheque().getMovimento().equals("R") && !r.getIdCheque().getMovimento().equals("A")) {
                    r.setIdCirurgia(cirurgia);
                    if (r.getIdCheque() == null) {
                        r.setFormaPagamento("Dinheiro");
                    }
                    r.setValorDocumento(r.getValor());
                    if ("S".equals(cirurgia.getGerarPagamentoResponsavel())) {
                        r.setIdPaciente(cirurgia.getIdResponsavel());
                    } else {
                        r.setIdPaciente(cirurgia.getIdPaciente());
                    }
                    getEm().merge(r);
                    getEm().merge(BCRUtils.criarAuditoria("insert", "Repasse: " + r.getValorDocumento() + " - " + r.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Repasse"));

                    ContaCorrente cc = new ContaCorrente();
                    cc.setIdContaCorrente(null);
                    cc.setIdTerceiro(r.getIdTerceiro());
                    cc.setIdRepasse(r);
                    cc.setIdCirurgia(r.getIdCirurgia());
                    cc.setIdCirurgiaItem(r.getIdSici().getIdCirurgiaItem());
                    cc.setIdPaciente(r.getIdPaciente());
                    cc.setIdCheque(r.getIdCheque().getIdCheque());
                    cc.setTipo("C");
                    cc.setDtLancamento(new Date());
                    cc.setSituacao("Não Liberado");
                    cc.setStatus("Não Retirado");
                    cc.setValor(r.getValor());
                    cc.setObservacao("Repasse gerado pelo fechamento " + r.getIdCirurgia().getIdCirurgia() + " da parcela " + r.getNumeroParcela() + " do paciente " + r.getIdPaciente().getNome() + ".");
                    contas.add(cc);
                }
            }
            for (ContaCorrente cccc : contas) {
                getEm().merge(cccc);
                getEm().merge(BCRUtils.criarAuditoria("insert", "Conta Corrente: " + cccc.getValor() + " - " + cccc.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Conta Corrente"));
            }
            if (!repasses.isEmpty()) {
                excluirDuplicados(cirurgia.getIdCirurgia());
            }
        } else {
            try {
                cirurgia.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
                cirurgia.setDtUltimaAlteracao(new Date());
                cirurgia.setServicoItemCirurgiaItemList(itensDosItens);
                getEm().merge(cirurgia);
                getEm().merge(BCRUtils.criarAuditoria("update", "ID Fechamento: " + cirurgia.getIdCirurgia() + " - " + cirurgia.getIdProcedimento().getDescricao() + " - " + cirurgia.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Fechamento"));

                for (CirurgiaItem ci : cirurgiaItens) {
                    ci.setIdCirurgiaItem(null);
                    ci.setIdCirurgia(cirurgia);
                    getEm().merge(ci);
                }

                for (ServicoItemCirurgiaItem si : itensDosItens) {
                    //si.setIdCirurgiaItem(ci);
                    si.setIdCirurgia(cirurgia);
                    getEm().merge(si);
                    for (Repasse repasse : repasses) {
                        if ((si.getIdServicoItem() == repasse.getIdSici().getIdServicoItem()) && (si.getIdCirurgia() == null) && (si.getIdCirurgiaItem() == null)) {
                            repasse.setIdSici(si);
                            si.setIdRepasse(repasse);
                        }
                    }
                }

                List<ContaCorrente> contas = new ArrayList<>();
                for (Repasse r : repasses) {
                    if (!r.getIdCheque().getMovimento().equals("R") && !r.getIdCheque().getMovimento().equals("A")) {
                        r.setIdRepasse(null);
                        r.setIdCirurgia(cirurgia);
                        r.setValorDocumento(r.getValor());
                        if ("S".equals(cirurgia.getGerarPagamentoResponsavel())) {
                            r.setIdPaciente(cirurgia.getIdResponsavel());
                        } else {
                            r.setIdPaciente(cirurgia.getIdPaciente());
                        }
                        getEm().merge(r);

                        ContaCorrente cc = new ContaCorrente();
                        cc.setIdTerceiro(r.getIdTerceiro());
                        cc.setIdRepasse(r);
                        cc.setIdCheque(r.getIdCheque().getIdCheque());
                        cc.setIdCirurgia(r.getIdCirurgia());
                        cc.setIdPaciente(r.getIdPaciente());
                        cc.setTipo("C");
                        cc.setDtLancamento(new Date());
                        cc.setSituacao("Não Liberado");
                        cc.setStatus("Não Retirado");
                        cc.setValor(r.getValor());
                        cc.setObservacao("Repasse gerado pelo fechamento " + r.getIdCirurgia().getIdCirurgia() + " da parcela " + r.getNumeroParcela() + " do paciente " + r.getIdPaciente().getNome() + ".");
                        contas.add(cc);
                    }
                }

                for (ContaCorrente cccc : contas) {
                    getEm().merge(cccc);
                    getEm().merge(BCRUtils.criarAuditoria("update", "Conta Corrente: " + cccc.getValor() + " - " + cccc.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Conta Corrente"));
                }
                if (!repasses.isEmpty()) {
                    excluirDuplicados(cirurgia.getIdCirurgia());
                }
            } catch (EJBException e) {
                Mensagem.addMensagemPadraoErro("salvar");
                System.out.println(e);
            }

        }
    }

    public void SalvarFechamentoSimples(Cirurgia c) {
        em.merge(c);
    }

    public void SalvarItem(CirurgiaItem ci) {
        em.merge(ci);
    }

    public void Cancelar(Cirurgia cirurgia) {
        deletarContaCorrente(cirurgia);
        deletaRepasses(cirurgia);
        deleteSicii(cirurgia);
        deleteCaixa(cirurgia);
        cirurgia.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
        cirurgia.setDtUltimaAlteracao(new Date());
        getEm().merge(cirurgia);
        getEm().merge(BCRUtils.criarAuditoria("cancelamento", "ID Fechamento: " + cirurgia.getIdCirurgia() + " - " + cirurgia.getIdProcedimento().getDescricao() + " - " + cirurgia.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Fechamento"));
    }

    public boolean retornaSeProcedimentoPacienteTemItemGerado(Integer idFechamento, Integer idProcedimento, Integer idContaPaciente) {
        boolean existe = false;
        try {
            Query query = em.createQuery("Select c From CirurgiaItem c where c.idProcedimentoPaciente.idProcedimentoPaciente = :idProc and c.idCirurgia.idCirurgia = :idFech and c.idCirurgia.idContaPaciente.idContaPaciente = :idCont");
            em.getEntityManagerFactory().getCache().evictAll();
            query.setParameter("idProc", idProcedimento);
            query.setParameter("idFech", idFechamento);
            query.setParameter("idCont", idContaPaciente);
            if (query.getResultList().size() > 0) {
                //System.out.println(idProcedimento + " existe na conta " + idContaPaciente);
                existe = true;
            }
        } catch (Exception e) {
            existe = false;
        }
        return existe;
    }

    public void SalvarAvulso(Cirurgia cirurgia, List<CirurgiaItem> itens, Repasse r) {
        cirurgia.setDtUltimaAlteracao(new Date());
        getEm().persist(cirurgia);
        getEm().flush();
        getEm().refresh(cirurgia);
        for (CirurgiaItem ci : itens) {
            ci.setIdCirurgia(cirurgia);
            getEm().merge(ci);
        }

        r.setIdCirurgia(cirurgia);
        if (r.getValorDocumento() != null) {
            getEm().merge(r);

            ContaCorrente cc = new ContaCorrente();
            cc.setIdContaCorrente(null);
            cc.setIdTerceiro(r.getIdTerceiro());
            cc.setIdRepasse(r);
            cc.setIdCirurgia(cirurgia);
            cc.setIdPaciente(r.getIdPaciente());
            cc.setIdCheque(r.getIdCheque().getIdCheque());
            if (cirurgia.getIdProcedimento().getPadrao().equals("D")) {
                cc.setTipo("D");
                cc.setObservacao("Débito gerado pelo lançamento avulso nº " + cc.getIdCirurgia().getIdCirurgia() + " do paciente " + r.getIdPaciente().getNome() + ".");
            } else {
                cc.setTipo("C");
                cc.setObservacao("Repasse gerado pelo lançamento avulso nº " + cc.getIdCirurgia().getIdCirurgia() + " do paciente " + r.getIdPaciente().getNome() + ".");
            }
            cc.setDtLancamento(new Date());
            cc.setSituacao("Não Liberado");
            cc.setStatus("Não Retirado");
            cc.setValor(r.getValorDocumento());
            getEm().merge(cc);
        }
    }

    public void SalvarExterno(Cirurgia cirurgia, List<CirurgiaItem> itens) {
        em.persist(cirurgia);
        em.flush();
        em.refresh(cirurgia);
        for (CirurgiaItem ci : itens) {
            ci.setIdCirurgia(cirurgia);
            em.merge(ci);
        }
    }

    public void AtualizarExterno(Cirurgia cirurgia, List<CirurgiaItem> itens) {
        em.merge(cirurgia);
        for (CirurgiaItem ci : itens) {
            ci.setIdCirurgia(cirurgia);
            em.merge(ci);
        }
    }

    public void atualizarExternoComoPago(Integer id) {
        Query query = getEm().createNativeQuery("UPDATE cirurgia SET pago = TRUE WHERE id_cirurgia = " + id);
        query.executeUpdate();
    }

    public void excluirDuplicados(Integer id) {
        Query query = getEm().createQuery("UPDATE ServicoItemCirurgiaItem s SET s.idRepasse = NULL WHERE s.principal is null and s.idCirurgia.idCirurgia = :id");
        query.setParameter("id", id);
        int i = query.executeUpdate();
        query = getEm().createQuery("Delete from ServicoItemCirurgiaItem s WHERE s.principal is null and s.idCirurgia.idCirurgia = :id");
        query.setParameter("id", id);
        i = query.executeUpdate();
//        query =  getEm().createQuery("Delete from Repasse r WHERE r.idCirurgia.idCirurgia IS NULL");
//        i = query.executeUpdate();
    }

    public void deletarContaCorrente(Cirurgia c) {
        Query query = getEm().createQuery("Delete from ContaCorrente c WHERE c.idCirurgia.idCirurgia = :cir");
        query.setParameter("cir", c.getIdCirurgia());
        int i = query.executeUpdate();
    }

    public void deletarRepasse(Cirurgia c) {
        Query query = getEm().createQuery("Delete from Repasse r where r.idCirurgia.idCirurgia = :cir");
        query.setParameter("cir", c.getIdCirurgia());
        query.executeUpdate();
    }

    public void deletarPagamentos(Cirurgia c) {
        Query query = getEm().createQuery("Delete from Cheque c where c.idCirurgia.idCirurgia = :cir");
        query.setParameter("cir", c.getIdCirurgia());
        query.executeUpdate();
    }

    public void deleteSicii(Cirurgia c) {
        Query query = getEm().createQuery("Delete from ServicoItemCirurgiaItem s where s.idCirurgia.idCirurgia = :cir");
        query.setParameter("cir", c.getIdCirurgia());
        System.out.println("Deletados: " + query.executeUpdate());
    }

    public void deleteCirurgiaItem(Cirurgia c) {
        Query query = getEm().createQuery("Delete from CirurgiaItem s where s.idCirurgia.idCirurgia = :cir");
        query.setParameter("cir", c.getIdCirurgia());
        query.executeUpdate();
    }

    public List<Repasse> pesquisarRepassesPorCirurgia(Cirurgia cirurgia) {
        Query query = getEm().createQuery("Select r From Repasse r where r.idCirurgia.idCirurgia = :cir");
        query.setParameter("cir", cirurgia.getIdCirurgia());
        return query.getResultList();
    }

    public Cirurgia pesquisarCirurgiaPorID(Integer id) {
        Query query = getEm().createQuery("Select c From Cirurgia c where c.idCirurgia = :c");
        query.setParameter("c", id);
        return (Cirurgia) query.getSingleResult();
    }

    public ContaCorrente pesquisarContaCorrentePorID(Integer id) {
        Query query = getEm().createQuery("Select c From ContaCorrente c where c.idContaCorrente = :c");
        query.setParameter("c", id);
        return (ContaCorrente) query.getSingleResult();
    }

    public void deletarContaCorrentePorRepasse(Repasse r) {
        Query query = getEm().createNativeQuery("Delete from conta_corrente where id_repasse = " + r.getIdRepasse());
        query.executeUpdate();
    }

    public ContaCorrente pesquisarContaCorrentePorRepasse(Repasse r) {
        Query query = getEm().createQuery("Select c From ContaCorrente c where c.idRepasse.idRepasse = :idr");
        query.setParameter("idr", r.getIdRepasse());
        ContaCorrente corrente;
        try {
            corrente = (ContaCorrente) query.getSingleResult();
        } catch (Exception e) {
            corrente = new ContaCorrente();
        }
        return corrente;
    }

    public void deletaRepasses(Cirurgia cir) {
        Query query = getEm().createQuery("DELETE FROM Repasse r where r.idCirurgia.idCirurgia = :cir");
        query.setParameter("cir", cir.getIdCirurgia());
        query.executeUpdate();
    }

    public void deletaRepasses(Repasse r) {
        Query query = getEm().createQuery("DELETE FROM Repasse r where r.idRepasse = :cir");
        query.setParameter("cir", r.getIdRepasse());
        query.executeUpdate();
    }

    public TipoInternacao retornaTipoInternacao(String s) {
        Query query = em.createQuery("SELECT t FROM TipoInternacao t where t.descricao like :tipo");
        query.setParameter("tipo", s);
        return (TipoInternacao) query.getSingleResult();
    }

    public void atualizarOrcto(Orcamento orcamento) {
        getEm().merge(orcamento);
        getEm().merge(BCRUtils.criarAuditoria("update", "ID Orçamento: " + orcamento.getIdOrcamento().toString(), us.retornaUsuario(), new Date(), "Orçamento"));
    }

    public void atualizaOrcamento(Orcamento orcamento) {
        getEm().merge(orcamento);
        getEm().merge(BCRUtils.criarAuditoria("update", "ID Orçamento: " + orcamento.getIdOrcamento().toString(), us.retornaUsuario(), new Date(), "Orçamento"));
    }

    public void atualizarCirurgia(Cirurgia cirurgia) {
        cirurgia.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
        cirurgia.setDtUltimaAlteracao(new Date());
        getEm().merge(cirurgia);
        getEm().merge(BCRUtils.criarAuditoria("update", "ID Fechamento: " + cirurgia.getIdCirurgia() + " - " + cirurgia.getIdProcedimento().getDescricao() + " - " + cirurgia.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Fechamento"));
    }

    public void atualizarFechamento(Cirurgia cirurgia) {
        cirurgia.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
        getEm().merge(cirurgia);
    }

    public List<Orcamento> pesquisarOrcamentosParaImportacao() {
        Query query = getEm().createQuery("Select o From Orcamento o WHERE o.dtCancelamento IS NULL and o.dtExecucao IS NULL and (o.idStatus.fixo = 2 or o.idStatus.fixo = 6)");
        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public Orcamento pesquisarOrcamentoPorNumero(Integer id) {
        Query query = getEm().createQuery("Select c From Orcamento c where c.idOrcamento = :id and c.dtExecucao IS  NULL and c.dtCancelamento IS  NULL");
        getEm().getEntityManagerFactory().getCache().evictAll();
        query.setParameter("id", id);
        return (Orcamento) query.getSingleResult();
    }

    public List<Cirurgia> pesquisarFechamentosPorPaciente(Integer idPaciente) {
        Query query = getEm().createQuery("Select c From Cirurgia c where c.idPaciente.idPaciente = :id ORDER BY c.dtCirurgia DESC");
        getEm().getEntityManagerFactory().getCache().evictAll();
        query.setParameter("id", idPaciente);
        return query.getResultList();
    }

    public Cirurgia selecionarPorID(Integer id) {
        Query query = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = :id");
        getEm().getEntityManagerFactory().getCache().evictAll();
        query.setParameter("id", id);
        return (Cirurgia) query.getSingleResult();
    }

    public CirurgiaItem selecionarItemPorID(Integer id) {
        try {
            Query query = getEm().createQuery("Select o From CirurgiaItem o where o.idCirurgiaItem = :id");
            getEm().getEntityManagerFactory().getCache().evictAll();
            query.setParameter("id", id);
            return (CirurgiaItem) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }

    }

    public List<Cirurgia> pesquisarCirurgias(Date dtInici, Date dtFinal, Integer idPaciente,
            Integer idCirurgia, Integer idProcedimento, Integer idTerceiro, Integer idTipoInternacao, boolean exceto) {
        StringBuffer clausula = new StringBuffer();
        if (dtInici != null && dtFinal != null) {
            clausula.append(" and c.dtCirurgia BETWEEN :dtIni and :dtFim ");
        }
        if (idPaciente != null) {
            clausula.append(" and c.idPaciente.idPaciente = :pac ");
        }
        if (idCirurgia != null) {
            clausula.append(" and c.idCirurgia = :cir");
        }
        if (idProcedimento != null) {
            clausula.append(" and c.idProcedimento.idProcedimento = :proc");
        }
        if (idTerceiro != null) {
            clausula.append(" and c.idTerceiro.idTerceiro = :terc");
        }
        if (idTipoInternacao != null) {
            if (exceto) {
                clausula.append(" and c.idTipoInternacao.idTipoInternacao != :tipoInternacao");
            } else {
                clausula.append(" and c.idTipoInternacao.idTipoInternacao = :tipoInternacao");
            }

        }
        String sql = "Select c From Cirurgia c where c.dtCirurgia is not null " + clausula;
        if (sql.contains("where  and")) {
            sql = sql.replace("where  and", " where ");
        }
        Query query = getEm().createQuery(sql);
        System.out.println(sql);
        if (idCirurgia != null) {
            query.setParameter("cir", idCirurgia);
        }
        if (dtInici != null && dtFinal != null) {
            query.setParameter("dtIni", dtInici);
            query.setParameter("dtFim", dtFinal);
        }
        if (idPaciente != null) {
            query.setParameter("pac", idPaciente);
        }

        if (idProcedimento != null) {
            query.setParameter("proc", idProcedimento);
        }
        if (idTerceiro != null) {
            query.setParameter("terc", idTerceiro);
        }
        if (idTipoInternacao != null) {
            query.setParameter("tipoInternacao", idTipoInternacao);
        }
        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public List<Cirurgia> pesquisarFechamentosQuePossuemRepassesMedico(Date dtInici, Date dtFinal, Integer idPaciente,
            Integer idCirurgia, Integer idProcedimento, Integer idTerceiro, Integer idTipoInternacao, boolean exceto) {
        StringBuffer clausula = new StringBuffer();
        List<Cirurgia> listaTemp = new ArrayList<>();
        List<Cirurgia> lista = new ArrayList<>();

        if (dtInici != null && dtFinal != null) {
            clausula.append(" and c.dtCirurgia BETWEEN :dtIni and :dtFim ");
        }
        if (idPaciente != null) {
            clausula.append(" and c.idPaciente.idPaciente = :pac ");
        }
        if (idCirurgia != null) {
            clausula.append(" and c.idCirurgia = :cir");
        }
        if (idProcedimento != null) {
            clausula.append(" and c.idProcedimento.idProcedimento = :proc");
        }
        if (idTerceiro != null) {
            clausula.append(" and c.idTerceiro.idTerceiro = :terc");
        }
        if (idTipoInternacao != null) {
            if (exceto) {
                clausula.append(" and c.idTipoInternacao.idTipoInternacao != :tipoInternacao");
            } else {
                clausula.append(" and c.idTipoInternacao.idTipoInternacao = :tipoInternacao");
            }

        }
        String sql = "Select c From Cirurgia c where c.dtCirurgia is not null " + clausula;
        if (sql.contains("where  and")) {
            sql = sql.replace("where  and", " where ");
        }
        Query query = getEm().createQuery(sql);
        System.out.println(sql);
        if (idCirurgia != null) {
            query.setParameter("cir", idCirurgia);
        }
        if (dtInici != null && dtFinal != null) {
            query.setParameter("dtIni", dtInici);
            query.setParameter("dtFim", dtFinal);
        }
        if (idPaciente != null) {
            query.setParameter("pac", idPaciente);
        }

        if (idProcedimento != null) {
            query.setParameter("proc", idProcedimento);
        }
        if (idTerceiro != null) {
            query.setParameter("terc", idTerceiro);
        }
        if (idTipoInternacao != null) {
            query.setParameter("tipoInternacao", idTipoInternacao);
        }
        getEm().getEntityManagerFactory().getCache().evictAll();

        // 06082016
        // Verifica se as fechamentos selecionados possuem algum repasse do médico logado e adiciona a uma nova lista para retornar.        
        if (uEJB.retornaUsuarioDaSessao() != null) {
            listaTemp = query.getResultList();
            for (Cirurgia c : listaTemp) {
                if (!ccEJB.listaMovimentoContaCorrentePorMedicoCirurgia(uEJB.retornaUsuarioDaSessao().getIdTerceiro().getIdTerceiro(), c.getIdCirurgia()).isEmpty()) {
                    lista.add(c);
                }
            }
        }
        return lista;
    }

    public List<Cirurgia> pesquisarAtendimentosPorPaciente(Integer idPaciente) {
        Query query = getEm().createQuery("SELECT c FROM Cirurgia c where c.dtCirurgia is not null and c.idPaciente.idPaciente = :id");
        query.setParameter("id", idPaciente);
        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public List<Cirurgia> pesquisarLancamentosAvulsos(Date dtInici, Date dtFinal, Integer idTipoInternacao) {
        StringBuffer clausula = new StringBuffer();
        if (dtInici != null && dtFinal != null) {
            clausula.append(" and c.dtCirurgia BETWEEN :dtIni and :dtFim ");
        }

        if (uEJB.retornaUsuarioDaSessao().getIdPapel().getVerLancamentosAvulsosPerfil()) {
            clausula.append(" and c.idUsuario.idPapel.idPapel = :idPapel");
        } else if (!uEJB.retornaUsuarioDaSessao().getIdPapel().getVerLancamentosAvulsos()) {
            clausula.append(" and c.idUsuario.idUsuario = :usu");
        }

        clausula.append(" and c.idTipoInternacao.idTipoInternacao != :tipoInternacao AND c.externo = false");
        String sql = "Select c From Cirurgia c where c.dtCirurgia is not null " + clausula;
        if (sql.contains("where  and")) {
            sql = sql.replace("where  and", " where ");
        }
        Query query = getEm().createQuery(sql);

        if (dtInici != null && dtFinal != null) {
            query.setParameter("dtIni", dtInici);
            query.setParameter("dtFim", dtFinal);
        }

        if (uEJB.retornaUsuarioDaSessao().getIdPapel().getVerLancamentosAvulsosPerfil()) {
            query.setParameter("idPapel", uEJB.retornaUsuarioDaSessao().getIdPapel().getIdPapel());
        } else if (!uEJB.retornaUsuarioDaSessao().getIdPapel().getVerLancamentosAvulsos()) {
            query.setParameter("usu", uEJB.retornaUsuarioDaSessao().getIdUsuario());
        }

        if (idTipoInternacao != null) {
            query.setParameter("tipoInternacao", idTipoInternacao);
        }

        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public String pesquisaNomePorID(Integer id) {
        String sql = "Select ifNull(t.nome, t.nome_Fantasia) nome From Terceiro t where t.id_Terceiro = " + id;
        Query query = getEm().createNativeQuery(sql);
        return (String) query.getSingleResult();
    }

    public void Excluir(Cirurgia cirurgia) {
        cirurgia = em.getReference(Cirurgia.class, cirurgia.getIdCirurgia());
        em.remove(cirurgia);
    }

    public void ExcluirItem(CirurgiaItem cirurgiaItem) {
        cirurgiaItem = em.getReference(CirurgiaItem.class, cirurgiaItem.getIdCirurgiaItem());
        em.remove(cirurgiaItem);
    }

    public Long contarPagamentos(Integer idCir) {
        return (Long) getEm().createQuery("Select count(r.idRepasse) From Repasse r where r.idCirurgia.idCirurgia = :idCir and r.situacaoPaciente = 'P' ").setParameter("idCir", idCir).getSingleResult();
    }

    public void deletarRepaases(ServicoItemCirurgiaItem sici) {
        sici = getEm().getReference(ServicoItemCirurgiaItem.class, sici.getIdSici());
        getEm().remove(sici);
    }

    public void excluirRepasse(Repasse repasse) {
        repasse = getEm().getReference(Repasse.class, repasse.getIdRepasse());
        getEm().remove(repasse);
    }

    public Long verificaSePodeCancelar(Cirurgia c) {
        Query query = getEm().createNativeQuery("select count(*)total from repasse where id_cirurgia = " + c.getIdCirurgia() + " and status = 'F'");
        return (Long) query.getSingleResult();
    }

    public List<Grafico> verificarOrcadoXRealizadoPorPeriodo(Date dtIni, Date dtFim) {
        DateFormat df = new SimpleDateFormat("y-M-d");
        String sql = "SELECT DATE_FORMAT(o.DT_ORCAMENTO, '%m/%Y') , "
                + "          COUNT(o.ID_ORCAMENTO), "
                + "          SUM(o.valor) ,  "
                + "          SUM(c.valor) ,  "
                + "          ((SUM(c.valor) * 100 / SUM(o.valor))-100) DIF "
                + "     FROM cirurgia c, orcamento o, procedimento p, terceiro t"
                + "    WHERE c.id_orcamento = o.id_orcamento"
                + "      AND c.ID_PROCEDIMENTO = p.id_procedimento"
                + "      AND t.id_terceiro = c.id_terceiro"
                + "      AND c.ID_TIPO_INTERNACAO = 1"
                + "      AND c.dt_cirurgia between '" + df.format(dtIni) + "' AND '" + df.format(dtFim) + "'"
                + "    GROUP BY DATE_FORMAT(o.DT_ORCAMENTO, '%m/%Y')";
        //System.out.println(sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Grafico> itens = new ArrayList<>();

        for (Object[] c : resultList) {
            Grafico gf = new Grafico();
            gf.setPeriodo((String) c[0]);
            gf.setQuantidade(((Long) c[1]).intValue());
            gf.setValorOrcado((Double) c[2]);
            gf.setValorFechamento((Double) c[3]);
            gf.setPercentual((Double) c[4]);
            itens.add(gf);
        }
        return itens;
    }

    public List<Grafico> verificarOrcadoXRealizado(Date dataInicial, Date dataFinal) {
        DateFormat df = new SimpleDateFormat("y-M-d");
        String sql = "SELECT DATE_FORMAT(c.DT_CIRURGIA, '%m/%Y') , "
                + "          SUM(o.valor) ,  "
                + "          SUM(c.valor) ,  "
                + "          ((SUM(c.valor) * 100 / SUM(o.valor))-100) DIF "
                + "     FROM cirurgia c, orcamento o, procedimento p, terceiro t"
                + "    WHERE c.id_orcamento = o.id_orcamento"
                + "      AND c.ID_PROCEDIMENTO = p.id_procedimento"
                + "      AND t.id_terceiro = c.id_terceiro"
                + "      AND c.ID_TIPO_INTERNACAO = 1"
                + "      AND c.dt_cirurgia between '" + df.format(dataInicial) + "' AND '" + df.format(dataFinal) + "'"
                + "    GROUP BY DATE_FORMAT(c.DT_CIRURGIA, '%m/%Y')";
        //System.out.println(sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Grafico> itens = new ArrayList<>();

        for (Object[] c : resultList) {
            Grafico gf = new Grafico();
            gf.setPeriodo((String) c[0]);
            gf.setValorOrcado((Double) c[1]);
            gf.setValorFechamento((Double) c[2]);
            gf.setPercentual((Double) c[3]);
            itens.add(gf);
        }
        return itens;
    }

    public Number contarFechamentos(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idCirurgia) From Cirurgia p " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<Cirurgia> listarFechamentosLazyModeWhere(int first, int pageSize, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Cirurgia p " + clausula);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

    public List<Cirurgia> retornaListaAtendimentoExternoSemPagamento() {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Cirurgia p where p.externo = TRUE and p.pago = FALSE");
        return query.getResultList();
    }

    public List<CirurgiaItem> retornaListaItensPorFechamento(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select c From CirurgiaItem c where c.idCirurgia.idCirurgia = :id");
        query.setParameter("id", id);
        try {
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }

    }

    public String retornaLoteOrigemPorFechamento(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p.loteOrigem From Caixa p where p.idCirurgia.idCirurgia = :id and p.loteOrigem is not null GROUP BY p.idCirurgia.idCirurgia");
        query.setParameter("id", id);
        if (!query.getResultList().isEmpty()) {
            return (String) query.getResultList().get(0);
        } else {
            return "";
        }

    }

    private void zerarListas(Cirurgia cir) {
        cir.setServicoItemCirurgiaItemList(null);
    }

    private void deleteCaixa(Cirurgia cirurgia) {
        Query query = getEm().createQuery("DELETE FROM Caixa c where c.idCirurgia.idCirurgia = :cir");
        query.setParameter("cir", cirurgia.getIdCirurgia());
        query.executeUpdate();
    }

}
