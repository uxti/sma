/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.util.Conexao;
import br.com.ux.util.RepasseEstatico;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class RepasseEstaticoEJB extends Conexao {

     public Integer retornaRepassePorMedicoFechamento(Integer idRepasse, Integer idTerceiro, Integer idCirurgia) {
//        String sql = "SELECT @rownum := @rownum + 1 AS Parcela, id_repasse, id_cheque"
//                + "  FROM repasse, (SELECT @rownum := 0) r"
//                + " WHERE id_terceiro = " + idTerceiro + " and id_cirurgia = " + idCirurgia + ""
//                + " GROUP BY id_repasse, valor_documento, dt_vencimento"
//                + " ORDER BY dt_vencimento";
          String sql = "SELECT convert( @rownum := @rownum + 1, unsigned integer) AS Parcela, id_repasse, id_cheque"
                  + "  FROM repasse, (SELECT @rownum := 0) r"
                  + " WHERE id_terceiro = " + idTerceiro + " and id_cirurgia = " + idCirurgia + ""
                  + " GROUP BY id_repasse, dt_vencimento"
                  + " ORDER BY dt_vencimento";
          //System.out.println(sql);
          Query query = getEm().createNativeQuery(sql);
          List<Object[]> resultList = query.getResultList();
          List<RepasseEstatico> rep = new ArrayList<RepasseEstatico>();

          for (Object[] c : resultList) {
               RepasseEstatico re = new RepasseEstatico();
              
               re.setParcela((BigInteger)c[0]);
               re.setIdRepasse((Integer) c[1]);
               re.setIdCheque((Integer) c[2]);
               rep.add(re);
          }

          Integer numParcela = 0;
          for (RepasseEstatico repasseEstatico : rep) {
               if (repasseEstatico.getIdRepasse().equals(idRepasse)) {
                    numParcela = repasseEstatico.getParcela().intValue();
                    //System.out.println("Numero da Parcela do Repasse: " + idRepasse + "é: " + numParcela);
                    break;
               }
          }
          return numParcela;
     }

     public Long retornaMaxParcelaPorMedicoFechamento(Integer idTerceiro, Integer idCirurgia) {
          Query query = getEm().createQuery("SELECT COUNT(r) FROM Repasse r WHERE r.idTerceiro.idTerceiro = :idTer AND r.idCirurgia.idCirurgia = :idCir");
          query.setParameter("idTer", idTerceiro);
          query.setParameter("idCir", idCirurgia);
          return (Long) query.getSingleResult();
     }
}
