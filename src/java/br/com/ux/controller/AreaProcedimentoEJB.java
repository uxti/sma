/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.LogIntegracao;
import br.com.ux.model.tasy.AreaProcedimento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class AreaProcedimentoEJB extends Conexao {

    public void Salvar(AreaProcedimento ap, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(ap);
        em.flush();
        em.refresh(ap);
        em.merge(BCRUtils.criarAuditoria("insert", "Nome: " + ap.getDescricao(), usuario, new Date(), "Area Procedimento"));
    }

    public void Atualizar(AreaProcedimento ap, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(ap);
        em.merge(BCRUtils.criarAuditoria("update", "Nome: " + ap.getDescricao(), usuario, new Date(), "Area Procedimento"));
    }

    public List<AreaProcedimento> listarAreaProcedimento() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select a From AreaProcedimento a ORDER BY a.idAreaProcedimento").getResultList();
    }

    public AreaProcedimento selecionarPorIDAreaProcedimentoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT a FROM AreaProcedimento a WHERE a.idAreaProcedimentoTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (AreaProcedimento) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }
    
    public AreaProcedimento retornaAreaProcedimentoPorNome(String desc) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT a FROM AreaProcedimento a WHERE a.descricao = :desc");
        query.setParameter("desc", desc);
        query.setMaxResults(1);
        try {
            return (AreaProcedimento) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + desc);
            return null;
        }
    }

    public void VincularAreaProcedimentoTasy(Object[] c, LogIntegracao logPai) {
        AreaProcedimento ap = new AreaProcedimento();
        Date dtInicio = new Date();
        if ((String) c[1] != null) {
            ap = retornaAreaProcedimentoPorNome((String) c[1]);
        }

        if (ap != null) {
            if (ap.getIdAreaProcedimentoTasy() == null) {
                try {
                    // Cria o vinculo atraves do ID_AREA_PROCEDIMENTO_TASY
                    ap.setIdAreaProcedimentoTasy(((BigDecimal) c[0]).intValue());
                    Salvar(ap, "sync");
                    //em.merge(BCRUtils.gerarLogIntegracao(ap.getDescricao() + " vinculado.", "VincularAreaProcedimentoTasy", dtInicio, new Date(), logPai));
                } catch (Exception e) {
                    System.out.println(e);
                    //em.merge(BCRUtils.gerarLogIntegracao("Erro ao vincular " + (String) c[1] + ". " + e.getMessage(), "VincularCboSaudeTasy", null, null, logPai));
                }
            }
        } else {
            // Se ele não encontrar a Area de Procedimento no SMA, salva um novo cadastro.
            cadastrarAreaProcedimento(c, dtInicio, logPai);
        }
    }

    public void VincularListaAreaProcedimentoTasy(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {
            VincularAreaProcedimentoTasy(c, logPai);
        }
    }

    public void cadastrarAreaProcedimento(Object[] c, Date dtInicio, LogIntegracao logPai) {
        try {
            AreaProcedimento ap = new AreaProcedimento();
            ap.setIdAreaProcedimentoTasy(((BigDecimal) c[0]).intValue());
            ap.setDescricao((String) c[1]);
            ap.setDtCadastro(new Date());
            ap.setAtivo(Boolean.TRUE);
            Salvar(ap, "sync");
            em.merge(BCRUtils.gerarLogIntegracao(ap.getDescricao() + " cadastrado.", "VincularAreaProcedimentoTasy", dtInicio, new Date(), logPai));
        } catch (Exception e) {
            System.out.println(e);
            em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar " + (String) c[1] + ". " + e.getMessage(), "VincularAreaProcedimentoTasy", null, null, logPai));
        }
    }
    
    public List<AreaProcedimento> autoCompleteAreaProcedimento(String var) {
        Query query = em.createQuery("Select s From AreaProcedimento s where s.ativo = TRUE AND (s.descricao LIKE :desc OR s.idAreaProcedimento like :id)");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();
    }

}
