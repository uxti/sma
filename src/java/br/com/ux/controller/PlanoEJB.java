/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Plano;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class PlanoEJB extends Conexao {

    public void Salvar(Plano plano, String usuario) {
        if (plano.getIdPlano() != null) {
            getEm().merge(plano);
            getEm().merge(BCRUtils.criarAuditoria("update", plano.getDescricao(), usuario, new Date(), "Plano"));
        } else {
            getEm().merge(plano);
            getEm().merge(BCRUtils.criarAuditoria("insert", plano.getDescricao(), usuario, new Date(), "Plano"));
        }
    }

    public void Excluir(Plano plano, String usuario) {
        plano = getEm().getReference(Plano.class, plano.getIdPlano());
        getEm().remove(plano);
        getEm().merge(BCRUtils.criarAuditoria("delete", plano.getDescricao(), usuario, new Date(), "Plano"));
    }

    public List<Plano> listarPlanos() {
        return getEm().createQuery("Select p From Plano p").getResultList();
    }

    public List<Plano> listaPlanosPorConvenio(Integer id) {
        Query query = getEm().createQuery("Select p FROM Plano p where p.idPlano in (SELECT pc.idPlano.idPlano FROM PlanoConvenio pc where pc.idConvenio.idConvenio = :id)");
//        Query query =  getEm().createNativeQuery("select * from plano p where p.ID_PLANO in (select pc.ID_PLANO from plano_convenio pc where pc.id_convenio = :id)", Plano.class);
        query.setParameter("id", id);
        return query.getResultList();
    }

    public Plano selecionarPorID(Integer id, String usuario) {
        Query query = getEm().createQuery("Select p From Plano p where p.idPlano = :id");
        query.setParameter("id", id);
        return (Plano) query.getSingleResult();
    }

    public List<Plano> listarPlanosLazyMode(int primeiro, int qtd) {
        Query query = getEm().createQuery("Select p From Plano p");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<Plano> listarPlanosLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query = getEm().createQuery("Select p From Plano p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarPlanosRegistro(String clausula) {
        Query query = getEm().createQuery("Select COUNT(p.idPlano) From Plano p " + clausula);
        return (Number) query.getSingleResult();
    }

    public Plano selecionarPlanoPorNome(String busca) {
        Query query = em.createQuery("Select p From Plano p where p.inativo = FALSE and p.descricao like :busca");
        query.setParameter("busca", "%" + busca + "%");
        try {
            return (Plano) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
}
