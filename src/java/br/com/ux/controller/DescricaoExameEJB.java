/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.DescricaoExame;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author PokerFace
 */
@Stateless
public class DescricaoExameEJB extends Conexao {

    public void Salvar(DescricaoExame descricaoExame, String usuario) {
        if (descricaoExame.getIdDescricaoExame() == null) {
             getEm().merge(descricaoExame);
             getEm().merge(BCRUtils.criarAuditoria("insert", descricaoExame.getDescricao(), usuario, new Date(), "Itens avulsos"));
        } else {
             getEm().merge(descricaoExame);
             getEm().merge(BCRUtils.criarAuditoria("update", descricaoExame.getDescricao(), usuario, new Date(), "Itens avulsos"));   
        }
    }

    public void Excluir(DescricaoExame descricaoExame, String usuario) {
        descricaoExame =  getEm().getReference(DescricaoExame.class, descricaoExame.getIdDescricaoExame());
         getEm().remove(descricaoExame);
         getEm().merge(BCRUtils.criarAuditoria("delete", descricaoExame.getDescricao(), usuario, new Date(), "Itens avulsos"));
        
    }

    public List<DescricaoExame> listaDescricaoExame(String tipoDescricao) {
        Query query =  getEm().createQuery("Select d From DescricaoExame d where d.tipo = :tipo");
        query.setParameter("tipo", tipoDescricao);
        return query.getResultList();
    }

    public DescricaoExame selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select d From DescricaoExame d where d.idDescricaoExame = :id");
        query.setParameter("id", id);
        return (DescricaoExame) query.getSingleResult();
    }

    public List<DescricaoExame> listareDescExameLazyMode(int primeiro, int qtd) {
        Query query =  getEm().createQuery("Select d From DescricaoExame d");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<DescricaoExame> listarDescExameLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select d From DescricaoExame d " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarDescExameRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(d.idDescricaoExame) From DescricaoExame d " + clausula);
        return (Number) query.getSingleResult();
    }
    
    public List<DescricaoExame> completeMethodDescricao(String var){
        Query query =  getEm().createQuery("Select d From DescricaoExame d where d.descricao LIKE :desc OR d.idDescricaoExame LIKE :id");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();
    }
}
