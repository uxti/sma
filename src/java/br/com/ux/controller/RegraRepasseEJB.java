/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.RegraRepasse;
import br.com.ux.model.RegraRepasseMedicos;
import br.com.ux.model.Servico;
import br.com.ux.model.Terceiro;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class RegraRepasseEJB extends Conexao {

    public void Salvar(RegraRepasse rp, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(rp);
        em.flush();
        em.refresh(rp);
        em.merge(BCRUtils.criarAuditoria("insert", "Nome: " + rp.getDescricao(), usuario, new Date(), "Regra Repasse"));
    }

    public void SalvarRepasseSecundario(RegraRepasseMedicos rpm) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(rpm);
        em.flush();
        em.refresh(rpm);
    }

    public void Atualizar(RegraRepasse rp, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(rp);
        em.merge(BCRUtils.criarAuditoria("update", "Nome: " + rp.getDescricao(), usuario, new Date(), "Regra Repasse"));
    }

    public List<RegraRepasse> listarRegraRepasses() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select r From RegraRepasse r ORDER BY R.idRegraRepasse").getResultList();
    }

    public RegraRepasse selecionarPorID(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT r FROM RegraRepasse r WHERE r.idRegraRepasse = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (RegraRepasse) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }

    public void Excluir(RegraRepasse rp, String usuario) {
        rp = getEm().getReference(RegraRepasse.class, rp.getIdRegraRepasse());
        getEm().remove(rp);
        getEm().merge(BCRUtils.criarAuditoria("delete", rp.getDescricao(), usuario, new Date(), "Regra Repasse"));
    }

    public void ExcluirMedicoSecundario(RegraRepasseMedicos rp) {
        rp = getEm().getReference(RegraRepasseMedicos.class, rp.getIdRegraRepasseMedicos());
        getEm().remove(rp);
    }

    public RegraRepasse retornaRegraRepasse(Servico s, Terceiro t, String categoriaConvenio, Boolean urgencia) {
        List<RegraRepasse> listaRegras;
        urgencia = urgencia == null ? Boolean.FALSE : urgencia;
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT r FROM RegraRepasse r WHERE r.idMedico.idTerceiro = :id and r.ativo = TRUE ");
        query.setParameter("id", t != null ? t.getIdTerceiro() : 0);
        if (query.getResultList().isEmpty()) {
            Query query0 = em.createQuery("SELECT r FROM RegraRepasse r WHERE r.idMedico.idCboSaude.idCboSaude = :id and r.idMedico is null and r.ativo = TRUE ");
            query0.setParameter("id", t != null && t.getIdCboSaude() != null ? t.getIdCboSaude().getIdCboSaude() : 0);
            if (query0.getResultList().isEmpty()) {
                Query query1 = em.createQuery("SELECT r FROM RegraRepasse r WHERE r.idServico.idServico = :id and r.idMedico is null and r.ativo = TRUE ");
                query1.setParameter("id", s.getIdServico());
                if (query1.getResultList().isEmpty()) {
                    Query query2 = em.createQuery("SELECT r FROM RegraRepasse r WHERE r.idGrupoProcedimentoCbhpm.idGrupoProcedimentoCbhpm = :id and r.idMedico is null and r.ativo = TRUE ");
                    query2.setParameter("id", s.getIdGrupoProcedimentoCbhpm().getIdGrupoProcedimentoCbhpm());
                    if (query2.getResultList().isEmpty()) {
                        Query query3 = em.createQuery("SELECT r FROM RegraRepasse r WHERE r.idEspecialidadeProcedimento.idEspecialidadeProcedimento = :id and r.idMedico is null and r.ativo = TRUE ");
                        query3.setParameter("id", s.getIdGrupoProcedimentoCbhpm().getIdEspecialidadeProcedimento().getIdEspecialidadeProcedimento());
                        if (query3.getResultList().isEmpty()) {
                            Query query4 = em.createQuery("SELECT r FROM RegraRepasse r WHERE r.idAreaProcedimento.idAreaProcedimento = :id and r.idMedico is null and r.ativo = TRUE ");
                            query4.setParameter("id", s.getIdGrupoProcedimentoCbhpm().getIdEspecialidadeProcedimento().getIdAreaProcedimento().getIdAreaProcedimento());
//                            System.out.println("pegou regra por área");
                            listaRegras = query4.getResultList();
                        } else {
                            listaRegras = query3.getResultList();
                        }
                    } else {
//                        System.out.println("pegou regra por grupo");
                        listaRegras = query2.getResultList();
                    }
                } else {
//                    System.out.println("pegou regra por servico");
                    listaRegras = query1.getResultList();
                }
            } else {
//                System.out.println("pegou regra por cbo saude");
                listaRegras = query0.getResultList();
            }
        } else {
//            System.out.println("pegou regra por terceiro");
            listaRegras = query.getResultList();
        }

        if (!listaRegras.isEmpty()) {
            // CHARLES - 07052018
            // Reorganiza a lista, priorizando as regras de repasse por serviço.            

            if (listaRegras.size() > 1) {
                listaRegras = ordenaRegrasPorServico(listaRegras);
            }

            for (RegraRepasse r : listaRegras) {
//                System.out.println("regra nº " + r.getIdRegraRepasse());
                if (r.getIdMedico() != null) {
                    if (r.getIdMedico().getIdTerceiro().equals(t != null ? t.getIdTerceiro() : 0)) {
                        if (verificarRegraPorServico(r, s)) {
                            if (urgencia) {
                                if (verificarRegraUrgencia(r)) {
                                    return r;
                                }
                            } else {
                                return r;
                            }
                        }
                    }
                } else if (r.getIdCboSaude() != null) {
                    if (r.getIdCboSaude().getIdCboSaude().equals(t != null && t.getIdCboSaude() != null ? t.getIdCboSaude().getIdCboSaude() : 0)) {
                        if (verificarRegraPorServico(r, s)) {
                            if (urgencia) {
                                if (verificarRegraUrgencia(r)) {
                                    return r;
                                }
                            } else {
                                return r;
                            }
                        }
                    }
                } else if (verificarRegraPorServico(r, s)) {
                    if (urgencia) {
                        if (verificarRegraUrgencia(r)) {
                            return r;
                        }
                    } else if (r.getCategoriaConvenio() != null) {
                        if (!r.getCategoriaConvenio().equals("")) {
                            if (r.getCategoriaConvenio().equals(categoriaConvenio)) {
                                return r;
                            }
                        } else {
                            return r;
                        }
                    } else {
                        return r;
                    }

                }
            }

            // 14032018 - CHARLES
            // Se não achou na lista de regras por médico, volta ao inicio, sem médico setado, para nova tentativa.
            // UPDATE: Testa novamente somente se houver médico setado na primeira tentativa.
            if (t != null) {
                return retornaRegraRepasse(s, null, categoriaConvenio, urgencia);
            }
        }
        return null;
    }

    private List<RegraRepasse> ordenaRegrasPorServico(List<RegraRepasse> lista) {
        try {
            List<RegraRepasse> novaLista = new ArrayList<>();
            List<RegraRepasse> listaAntiga = new ArrayList<>();
            for (RegraRepasse r : lista) {
                if (r.getIdServico() != null) {
                    novaLista.add(r);
                } else {
                    listaAntiga.add(r);
                }
            }
            novaLista.addAll(listaAntiga);
            return novaLista;
        } catch (Exception e) {
            System.out.println(e);
            return lista;
        }
    }

    private boolean verificarRegraPorServico(RegraRepasse r, Servico s) {
        if (r.getIdServico() != null) {
//            System.out.println("regra: " + r.getIdServico().getIdServico());
//            System.out.println("serviço: " + s.getIdServico());
            if (r.getIdServico().getIdServico().equals(s.getIdServico())) {
                return true;
            }
        } else if (r.getIdGrupoProcedimentoCbhpm() != null) {
            if (r.getIdGrupoProcedimentoCbhpm().getIdGrupoProcedimentoCbhpm().equals(s.getIdGrupoProcedimentoCbhpm().getIdGrupoProcedimentoCbhpm())) {
                return true;
            }
        } else if (r.getIdEspecialidadeProcedimento() != null) {
            if (r.getIdEspecialidadeProcedimento().getIdEspecialidadeProcedimento().equals(s.getIdGrupoProcedimentoCbhpm().getIdEspecialidadeProcedimento().getIdEspecialidadeProcedimento())) {
                return true;
            }
        } else if (r.getIdAreaProcedimento() != null) {
            if (r.getIdAreaProcedimento().getIdAreaProcedimento().equals(s.getIdGrupoProcedimentoCbhpm().getIdEspecialidadeProcedimento().getIdAreaProcedimento().getIdAreaProcedimento())) {
                return true;
            }
        }
        return false;
    }

    private boolean verificarRegraUrgencia(RegraRepasse r) {
        if (r.getUrgencia() != null) {
            return r.getUrgencia();
        }
        return false;
    }

    public List<RegraRepasseMedicos> listarMedicosSecundariosPorRegraRepasse(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT r FROM RegraRepasseMedicos r WHERE r.idRegraRepasse.idRegraRepasse = :id");
        query.setParameter("id", id);
        return query.getResultList();
    }
}
