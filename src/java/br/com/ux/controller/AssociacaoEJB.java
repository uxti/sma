/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Associacao;
import br.com.ux.model.Auditoria;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class AssociacaoEJB extends Conexao {

    public void Salvar(Associacao associacao, String usuario) {
        Auditoria a = new Auditoria();
        if (associacao.getIdAssociacao() != null) {
            getEm().merge(associacao);
            getEm().merge(BCRUtils.criarAuditoria("update", associacao.getNome(), usuario, new Date(), "Associação"));
        } else {
            getEm().merge(associacao);
            getEm().merge(BCRUtils.criarAuditoria("insert", associacao.getNome(), usuario, new Date(), "Associação"));
        }
    }

    public Associacao carregarAssociacao() {
        Associacao as = (Associacao) em.createQuery("Select a From Associacao a where a.idAssociacao = 1").getSingleResult();
        em.getEntityManagerFactory().getCache().evictAll();
        return as;
    }

    public Date retornaDataUltimoBkp() {
        Date data = new Date();
        Query query = em.createQuery("Select a.dtUltimoBkp From Associacao a where a.idAssociacao = 1");
        data = (Date) query.getSingleResult();
        return data;
    }

    public void atualizarDataUltimoBkp() {
        Query query = em.createQuery("UPDATE Associacao a SET a.dtUltimoBkp = CURRENT_TIMESTAMP where a.idAssociacao = 1");
        query.executeUpdate();
    }

    public Double retornaDiferencaPermitida() {
        Double valor = 0D;
        Query query = getEm().createQuery("Select a.diferencaTolerada From Associacao a where a.idAssociacao = 1");
        valor = (Double) query.getSingleResult();
        return valor;
    }

}
