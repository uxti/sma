/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Especialidade;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author BrunoS
 */
@Stateless
public class EspecialidadeEJB extends Conexao {

    public void Salvar(Especialidade especialidade, String usuario) {
        if (especialidade.getIdEspecialidade() == null) {
             getEm().merge(especialidade);
             getEm().merge(BCRUtils.criarAuditoria("insert", especialidade.getDescricao(), usuario, new Date(), "Especialidade"));
        } else {
             getEm().merge(especialidade);
             getEm().merge(BCRUtils.criarAuditoria("update", especialidade.getDescricao(), usuario, new Date(), "Especialidade"));
        }
    }

    public void Excluir(Especialidade especialidade, String usuario) {
        especialidade =  getEm().getReference(Especialidade.class, especialidade.getIdEspecialidade());
         getEm().remove(especialidade);
         getEm().merge(BCRUtils.criarAuditoria("delete", especialidade.getDescricao(), usuario, new Date(), "Especialidade"));
    }

    public List<Especialidade> listaEspecialidades() {
        return  getEm().createQuery("Select e From Especialidade e WHERE e.inativo = FALSE ").getResultList();
    }

    public Especialidade selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select e From Especialidade e where e.idEspecialidade = :id");
        query.setParameter("id", id);
        return (Especialidade) query.getSingleResult();
    }

    public List<Especialidade> listarEspecialidadeLazyMode(int primeiro, int qtd) {
        Query query =  getEm().createQuery("Select e From Especialidade e WHERE e.inativo = FALSE");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<Especialidade> listarEspecialidadeLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select e From Especialidade e " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarEspecialidadeRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(e.idEspecialidade) From Especialidade e " + clausula);
        return (Number) query.getSingleResult();
    }
}
