/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Exame;
import br.com.ux.model.Usuario;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author PokerFace
 */
@Stateless
public class ExameEJB extends Conexao {

    public void Salvar(Exame exame, String usuario) {
        Auditoria auditoria = new Auditoria();
        Usuario u = selecionarIDUsuario(usuario);
        exame.setIdUsuario(u);
        if (exame.getIdExame() == null) {
             getEm().persist(exame);
             getEm().flush();
            auditoria.setTabela("Exame");
            auditoria.setDescricao("Inserção na tabela Exame no registro " + exame.getIdDescricaoExame().getDescricao());
            auditoria.setDtHora(new Date());
            auditoria.setUsuario(usuario);
             getEm().merge(auditoria);
        } else {
             getEm().merge(exame);
            auditoria.setTabela("Exame");
            auditoria.setDescricao("Alteração na tabela Exame no registro " + exame.getIdDescricaoExame().getDescricao());
            auditoria.setDtHora(new Date());
            auditoria.setUsuario(usuario);
             getEm().merge(auditoria);
        }
    }

    public void Atualizar(Exame exame, String usuario) {
        Auditoria auditoria = new Auditoria();
        Usuario u = selecionarIDUsuario(usuario);
         getEm().merge(exame);
        auditoria.setTabela("Exame");
        auditoria.setDescricao("Pagamento do Exame / Consulta referente ao registro " + exame.getIdExame());
        auditoria.setDtHora(new Date());
        auditoria.setUsuario(usuario);
         getEm().merge(auditoria);
    }

    public void Excluir(Exame exame, String usuario) {
        exame =  getEm().getReference(Exame.class, exame.getIdExame());
         getEm().remove(exame);
        Auditoria a = new Auditoria();
        a.setTabela("Exame");
        a.setDescricao("Exclusão na tabela Exame referente ao exame " + exame.getIdDescricaoExame().getDescricao() + " código " + exame.getIdExame());
        a.setDtHora(new Date());
        a.setUsuario(usuario);
         getEm().merge(a);
    }

    public List<Exame> listarExames() {
        return  getEm().createQuery("Select p From Exame p").getResultList();
    }

    public List<Exame> listarExamesLazy(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select p From Exame p");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarExamesRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(p.idExame) From Exame p " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<ContaCorrente> listarContaCorrente() {
        Query query =  getEm().createQuery("Select c From ContaCorrente c where c.dtRetirada is not null and c.situacao <> :situacao");
        query.setParameter("situacao", "Não liberado");
        return query.getResultList();
    }

    public Exame selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select p From Exame p where p.idExame = :id");
        query.setParameter("id", id);
        return (Exame) query.getSingleResult();
    }

    public Usuario selecionarIDUsuario(String usuario) {
        Query query =  getEm().createQuery("Select u From Usuario u where u.usuario = :usuario");
        query.setParameter("usuario", usuario);
        return (Usuario) query.getSingleResult();
    }
}
