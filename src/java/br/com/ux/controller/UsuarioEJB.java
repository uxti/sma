/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Renato
 */
@Stateless
public class UsuarioEJB extends Conexao {

    public Usuario retornaUsuarioPorUsername(String user) {
        Query query =  em.createQuery("Select u from Usuario u where u.usuario = :user");
        query.setParameter("user", user);
        return (Usuario) query.getSingleResult();
    }

    public void Salvar(Usuario u, String usuario) {
        Auditoria a = new Auditoria();
        if (u.getIdUsuario() != null) {
             getEm().merge(u);
             getEm().merge(BCRUtils.criarAuditoria("update", u.getUsuario(), usuario, new Date(), "Usuário"));
        } else {
             getEm().merge(u);
             getEm().merge(BCRUtils.criarAuditoria("insert", u.getUsuario(), usuario, new Date(), "Usuário"));
        }
    }

    public void Excluir(Usuario u, String usuario) {
        u =  getEm().getReference(Usuario.class, u.getIdUsuario());
         getEm().remove(u);
         getEm().merge(BCRUtils.criarAuditoria("delete", u.getUsuario(), usuario, new Date(), "Usuário"));
    }

    public List<Usuario> listarUsuarios() {
        return  getEm().createQuery("Select u From Usuario u").getResultList();
    }
    
    public List<Usuario> listarUsuariosAtivos() {
        return  em.createQuery("Select u From Usuario u where u.ativo =  TRUE").getResultList();
    }
    
    public List<Usuario> listarUsuariosEmail() {
        return  getEm().createQuery("Select u From Usuario u where u.email is not null").getResultList();
    }
    
    public List<Usuario> listarUsuariosProntoAtendimento() {
        return  getEm().createQuery("Select u From Usuario u WHERE u.idPapel.prontoAtendimento = true").getResultList();
    }
    
    public List<Usuario> listarUsuariosTransferencia() {
        return  getEm().createQuery("Select u From Usuario u where u.receberRepasse = '1' AND u.ativo = TRUE").getResultList();

    }

    public int contarUsuariosRepassesHospitalares() {
        Query q =  getEm().createQuery("Select u From Usuario u where u.receberRepasseHospital = 'true'");
        return (int) q.getResultList().size();
    }

    public int contarUsuariosRepasses() {
        Query q =  getEm().createQuery("Select u From Usuario u where u.receberRepasse = 'true'");
        return (int) q.getResultList().size();
    }

    public Integer retornaUsuarioRepasseLote(String lote) {
        Query query =  getEm().createQuery("Select c.idUsuario.idUsuario From Caixa c where c.loteOrigem = :lote and c.tipoLancamento = 'C' and c.idUsuario.receberRepasseHospital IS NULL");
        query.setParameter("lote", lote);        
        return (Integer) query.getSingleResult();
    }
    
    public List<Usuario> retornaUsuariosPorTransferencia(String lote) {
        Query query =  getEm().createQuery("Select c.idUsuario From Caixa c where c.loteOrigem = :lote and c.tipoLancamento = 'C'");
        query.setParameter("lote", lote);        
        return query.getResultList();
    }

    public Usuario retornaUsuarioRepasseHospital() {
        return (Usuario)  getEm().createQuery("Select u From Usuario u where u.receberRepasseHospital = '1'").getSingleResult();
    }

    public List<Usuario> listarLazy(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select p From Usuario p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }
    
    public String verificaCPFRetornaNome(String cpf) {
         getEm().getEntityManagerFactory().getCache().evictAll();
        Query query =  getEm().createQuery("Select u.nome from Usuario u where u.cpf = :cpf");
        query.setParameter("cpf", cpf);
        String nome = (String) query.getSingleResult();
        if (nome == null) {
            nome = "Inexistente";
        }
        return nome;
    }

    public Number contarRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(p.idUsuario) From Usuario p " + clausula);
        return (Number) query.getSingleResult();
    }

    public Usuario selecionarPorID(Integer id) {
        Query query =  getEm().createQuery("Select u From Usuario u where u.idUsuario = :id");
        query.setParameter("id", id);
        return (Usuario) query.getSingleResult();
    }

    public Usuario selecionarPorUser(String s) {
        Query query =  getEm().createQuery("Select u From Usuario u where u.usuario = :us");
        query.setParameter("us", s);
        return (Usuario) query.getSingleResult();
    }

    public List<Usuario> usuariosPorRecebimento() {
        Query query =  getEm().createQuery("Select u From Usuario u where u.idPapel.receberLancamento = 1");
        return query.getResultList();
    }

    public void verificaUsuarioAdministradorExiste() {
        Query query =  getEm().createQuery("Select COUNT(p.idPapel) From Papel p where p.descricao ='ADMINISTRADOR'");
        Long papel = (Long) query.getSingleResult();
        if (papel <= 0) {
            String sql = "INSERT INTO `papel` (`ID_PAPEL`, `ROLE`, `DESCRICAO`, `MENUPACIENTE`, `MENUTERCEIRO`, `MENUITENSORCTO`, `MENUPLANO`, `MENUEXAME`, `MENUMOTIVO`, `MENUBANCO`, `MENUCONVENIO`, `MENUPROCEDIMENTO`, `MENUESPECIALIDADE`, `MENUUNIDADEATENDIMENTO`, `MENUFORMAPAGTO`, `MENUOPERACAOCAIXA`, `MENUORCAMENTO`, `MENUCIRURGIA`, `MENULANCAREXAME`, `MENUCONTROLECAIXA`, `MENULANCARCAIXA`, `MENUESTORNORECEBIMENTO`, `MENUCONTROLECHEQUES`, `MENUCONSULTAORCAMENTO`, `MENUCONSULTACIRURGIA`, `MENUCONSULTATERCEIRO`, `MENUCONSULTAPACIENTE`, `MENUCONSULTACONTACORRENTE`, `MENULISTAGEMTERCEIRO`, `MENULISTAGEMPACIENTE`, `MENULISTAGEMORCAMENTO`, `MENULISTAGEMCIRURGIA`, `MENULISTAGEMEXAME`, `MENUUSUARIO`, `MENUPERMISSAO`, `MENUAUDITORIA`, `MENUASSOCIACAO`, `MENUBACKUP`, `VERCAIXAS`, `ABRIRCAIXA`, `FECHARCAIXA`, `REABRIRFECHARCAIXA`, `LANCARCAIXA`, `EDITARPACIENTE`, `INFOPACIENTE`, `EXCLUIRPACIENTE`, `NOVOPACIENTE`, `EDITARTERCEIRO`, `INFOTERCEIRO`, `EXCLUIRTERCEIRO`, `NOVOTERCEIRO`, `EDITARITENSORCAMENTO`, `INFOITENSORCAMENTO`, `EXCLUIRITENSORCAMENTO`, `NOVOITENSORCAMENTO`, `EDITARDESCEXAME`, `EXCLUIRDESCEXAME`, `NOVODESCEXAME`, `EDITARMOTIVO`, `EXCLUIRMOTIVO`, `NOVOMOTIVO`, `EDITARBANCO`, `EXCLUIRBANCO`, `NOVOBANCO`, `EDITARCONVENIO`, `EXCLUIRCONVENIO`, `NOVOCONVENIO`, `EDITARPROCEDIMENTO`, `EXCLUIRPROCEDIMENTO`, `NOVOPROCEDIMENTO`, `EDITARESPECIALIDADE`, `EXCLUIRESPECIALIDADE`, `NOVOESPECIALIDADE`, `EDITARUNIDADEATENDIMENTO`, `EXCLUIRUNIDADEATENDIMENTO`, `NOVOUNIDADEATENDIMENTO`, `EDITARFORMAPAGAMENTO`, `EXCLUIRFORMAPAGAMENTO`, `NOVOFORMAPAGAMENTO`, `EDITAROPERACAOCAIXA`, `EXCLUIROPERACAOCAIXA`, `NOVOOPERACAOCAIXA`, `EDITARORCAMENTO`, `INFOORCAMENTO`, `EXCLUIRORCAMENTO`, `NOVOORCAMENTO`, `COPIARORCAMENTO`, `LIBERARORCAMENTO`, `CANCELARORCAMENTO`, `INFOINTERNACAO`, `EDITARINTERNACAO`, `EXCLUIRINTERNACAO`, `LIBERARINTERNACAO`, `CANCELARINTERNACAO`, `NOVAINTERNACAO`) VALUES (1, 'ROLE_ROOT', 'ADMINISTRADOR', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1');";
             getEm().createNativeQuery(sql).executeUpdate();
        }
        Query query2 =  getEm().createQuery("Select COUNT(p.idPapel) From Papel p where p.descricao ='MEDICO'");
        Long papel2 = (Long) query2.getSingleResult();
        if (papel2 <= 0) {
            String sql2 = "INSERT INTO `papel` (`ID_PAPEL`, `ROLE`, `DESCRICAO`, `MENUPACIENTE`, `MENUTERCEIRO`, `MENUITENSORCTO`, `MENUPLANO`, `MENUEXAME`, `MENUMOTIVO`, `MENUBANCO`, `MENUCONVENIO`, `MENUPROCEDIMENTO`, `MENUESPECIALIDADE`, `MENUUNIDADEATENDIMENTO`, `MENUFORMAPAGTO`, `MENUOPERACAOCAIXA`, `MENUORCAMENTO`, `MENUCIRURGIA`, `MENULANCAREXAME`, `MENUCONTROLECAIXA`, `MENULANCARCAIXA`, `MENUESTORNORECEBIMENTO`, `MENUCONTROLECHEQUES`, `MENUCONSULTAORCAMENTO`, `MENUCONSULTACIRURGIA`, `MENUCONSULTATERCEIRO`, `MENUCONSULTAPACIENTE`, `MENUCONSULTACONTACORRENTE`, `MENULISTAGEMTERCEIRO`, `MENULISTAGEMPACIENTE`, `MENULISTAGEMORCAMENTO`, `MENULISTAGEMCIRURGIA`, `MENULISTAGEMEXAME`, `MENUUSUARIO`, `MENUPERMISSAO`, `MENUAUDITORIA`, `MENUASSOCIACAO`, `MENUBACKUP`, `VERCAIXAS`, `ABRIRCAIXA`, `FECHARCAIXA`, `REABRIRFECHARCAIXA`, `LANCARCAIXA`, `EDITARPACIENTE`, `INFOPACIENTE`, `EXCLUIRPACIENTE`, `NOVOPACIENTE`, `EDITARTERCEIRO`, `INFOTERCEIRO`, `EXCLUIRTERCEIRO`, `NOVOTERCEIRO`, `EDITARITENSORCAMENTO`, `INFOITENSORCAMENTO`, `EXCLUIRITENSORCAMENTO`, `NOVOITENSORCAMENTO`, `EDITARDESCEXAME`, `EXCLUIRDESCEXAME`, `NOVODESCEXAME`, `EDITARMOTIVO`, `EXCLUIRMOTIVO`, `NOVOMOTIVO`, `EDITARBANCO`, `EXCLUIRBANCO`, `NOVOBANCO`, `EDITARCONVENIO`, `EXCLUIRCONVENIO`, `NOVOCONVENIO`, `EDITARPROCEDIMENTO`, `EXCLUIRPROCEDIMENTO`, `NOVOPROCEDIMENTO`, `EDITARESPECIALIDADE`, `EXCLUIRESPECIALIDADE`, `NOVOESPECIALIDADE`, `EDITARUNIDADEATENDIMENTO`, `EXCLUIRUNIDADEATENDIMENTO`, `NOVOUNIDADEATENDIMENTO`, `EDITARFORMAPAGAMENTO`, `EXCLUIRFORMAPAGAMENTO`, `NOVOFORMAPAGAMENTO`, `EDITAROPERACAOCAIXA`, `EXCLUIROPERACAOCAIXA`, `NOVOOPERACAOCAIXA`, `EDITARORCAMENTO`, `INFOORCAMENTO`, `EXCLUIRORCAMENTO`, `NOVOORCAMENTO`, `COPIARORCAMENTO`, `LIBERARORCAMENTO`, `CANCELARORCAMENTO`, `INFOINTERNACAO`, `EDITARINTERNACAO`, `EXCLUIRINTERNACAO`, `LIBERARINTERNACAO`, `CANCELARINTERNACAO`, `NOVAINTERNACAO`) VALUES (2, 'ROLE_ROOT', 'MEDICO', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', NULL, NULL, NULL, NULL, NULL, b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', NULL, b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0');";
             getEm().createNativeQuery(sql2).executeUpdate();
        }
        Query query3 =  getEm().createQuery("Select COUNT(p.idUsuario) From Usuario p where p.usuario ='ADMINISTRADOR'");
        Long usuario = (Long) query3.getSingleResult();
        if (usuario <= 0) {
            String sql3 = "INSERT INTO `usuario` (`ID_USUARIO`, `USUARIO`, `SENHA`, `ATIVO`, `ID_PAPEL`, `ID_TERCEIRO`, `EMAIL`) VALUES (1, 'ADMINISTRADOR', 'ADMIN', b'1', 1, NULL, NULL);";
             getEm().createNativeQuery(sql3).executeUpdate();
        }

    }
    
    public void atualizarSenha(String senha, Integer idUsuario){
        Query query =  getEm().createQuery("UPDATE Usuario u SET u.senha = :senha, u.trocarSenha = 0 WHERE u.idUsuario = :id");
        query.setParameter("senha", senha);
        query.setParameter("id", idUsuario);
        query.executeUpdate();
         getEm().merge(BCRUtils.criarAuditoria("Geral", "Troca de senha: " + retornaUsuario(), retornaUsuario(), new Date(), "Usuário"));
    }
    
    public Usuario retornaUsuarioDaSessao(){
        Usuario u = retornaUsuarioPorUsername(retornaUsuario());
        return  u;
    }
    
    public String retornaUsuario() {
        String usuarioLog = "";
        Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (usuarioLogado instanceof UserDetails) {
            return usuarioLog = ((UserDetails) usuarioLogado).getUsername();
        } else {
            return usuarioLog = usuarioLogado.toString().toUpperCase();
        }
    }
}
