/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Caixa;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.Operacao;
import br.com.ux.model.Orcamento;
import br.com.ux.model.OrcamentoItem;
import br.com.ux.pojo.Grafico;
import br.com.ux.pojo.OrcamentoItemDuplicado;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.apache.poi.hpsf.NoSingleSectionException;

/**
 *
 * @author Renato
 */
@Stateless
public class OrcamentoEJB extends Conexao {

    UsuarioSessao us = new UsuarioSessao();
    @EJB
    UsuarioEJB uEJB;

    public void Salvar(Orcamento orcamento, List<OrcamentoItem> itens, String usuario, String acao) {
        getEm().getEntityManagerFactory().getCache().evictAll();
        if (orcamento.getIdOrcamento() == null) {
            orcamento.setOrcamentoItemList(null);
            getEm().persist(orcamento);
            getEm().flush();
            getEm().refresh(orcamento);
            getEm().merge(BCRUtils.criarAuditoria("insert", "ID Orçamento: " + orcamento.getIdOrcamento() + " - " + orcamento.getIdProcedimento().getDescricao() + " - " + orcamento.getIdTerceiro().getNome().toString() + orcamento.getStatus(), usuario, new Date(), "Orçamento"));
            for (OrcamentoItem i : itens) {
                i.setIdOrcamento(orcamento);
                getEm().persist(i);
            }
            if (!acao.equals("Copiar")) {
                getEm().flush();
                getEm().refresh(orcamento);
            }
        } else {
            orcamento.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
            orcamento.setDtModificacao(new Date());
            for (OrcamentoItem i : itens) {
                i.setIdOrcamento(orcamento);
                getEm().merge(i);
            }
            orcamento.setOrcamentoItemList(null);
            getEm().merge(orcamento);
            getEm().merge(BCRUtils.criarAuditoria("update", "ID Orçamento: " + orcamento.getIdOrcamento() + " - " + orcamento.getIdProcedimento().getDescricao() + " - " + orcamento.getIdTerceiro().getNome().toString() + orcamento.getStatus(), usuario, new Date(), "Orçamento"));
        }
    }

    public void Liberar(Orcamento orcamento) {
        orcamento.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
        orcamento.setDtModificacao(new Date());
        getEm().merge(orcamento);
        // Auditoria via Managed Bean
    }

    public void cancelar(Orcamento orcamento) {
        orcamento.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
        orcamento.setDtModificacao(new Date());
        getEm().merge(orcamento);
        // Auditoria via Managed Bean
    }

    public Long pegaUltimoID() {
        Query query = getEm().createQuery("Select MAX(o.idOrcamento) From Orcamento o");
        return (Long) query.getSingleResult();
    }

    public OrcamentoItem selecionaItemPorID(Integer id) {
        Query query = getEm().createQuery("Select o From OrcamentoItem o where o.idOrcamentoItem = :id");
        query.setParameter("id", id);
        return (OrcamentoItem) query.getSingleResult();
    }

    public List<OrcamentoItemDuplicado> verificarRegistrosDuplicadosOrcamento() {
        Query query = null;
        String sql = "SELECT id_orcamento, id_servico, id_orcamento_item"
                + " FROM orcamento_item "
                + " GROUP BY id_servico, id_orcamento"
                + " HAVING count(id_servico) > 1"
                + " ORDER BY id_orcamento";
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<OrcamentoItemDuplicado> itens = new ArrayList<>();

        for (Object[] c : resultList) {
            OrcamentoItemDuplicado oid = new OrcamentoItemDuplicado();
            oid.setIdOrcamento((Integer) c[0]);
            oid.setIdServico((Integer) c[1]);
            oid.setIdOrcamentoItem((Integer) c[2]);
            itens.add(oid);
        }
        return itens;
    }

    public List<OrcamentoItemDuplicado> verificarRegistrosDuplicadosOrcamento(Integer idOrcamento) {
        Query query = null;
        String sql = "SELECT id_orcamento, id_servico"
                + " FROM orcamento_item "
                + " WHERE "
                + " GROUP BY id_servico, id_orcamento"
                + " HAVING count(id_servico) > 1"
                + " ORDER BY id_orcamento";
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<OrcamentoItemDuplicado> itens = new ArrayList<>();

        for (Object[] c : resultList) {
            OrcamentoItemDuplicado oid = new OrcamentoItemDuplicado();
            oid.setIdOrcamento((Integer) c[0]);
            oid.setIdServico((Integer) c[1]);
            itens.add(oid);
        }
        return itens;
    }

    public void ExcluirItem(OrcamentoItem orcamentoItem) {
        getEm().getEntityManagerFactory().getCache().evictAll();
        orcamentoItem = getEm().getReference(OrcamentoItem.class, orcamentoItem.getIdOrcamentoItem());
        getEm().remove(orcamentoItem);
        getEm().merge(BCRUtils.criarAuditoria("delete", "Itens do Orçamento ID: " + orcamentoItem.getIdOrcamento().toString() + ". Item: " + orcamentoItem.getIdOrcamentoItem().toString(), us.retornaUsuario(), new Date(), "Itens Orçamento"));
    }

    public void ExcluirItensPorServico(Integer idServico, Integer idOrcamento, Integer idOrcamentoItem) {
        List<OrcamentoItem> listaItems = new ArrayList<>();
        getEm().getEntityManagerFactory().getCache().evictAll();
        Query query = getEm().createQuery("Select o From OrcamentoItem o where o.idOrcamento.idOrcamento = :id and o.idOrcamentoItem != :idItem and o.idServico.idServico = :idServico");
        query.setParameter("id", idOrcamento);
        query.setParameter("idItem", idOrcamentoItem);
        query.setParameter("idServico", idServico);
        listaItems = query.getResultList();

        if (listaItems.size() > 0) {
            for (OrcamentoItem oit : listaItems) {
                OrcamentoItem oitTemp = getEm().getReference(OrcamentoItem.class, oit.getIdOrcamentoItem());
                getEm().remove(oitTemp);
                getEm().merge(BCRUtils.criarAuditoria("delete", "Excluído por duplicidade: " + oit.getIdOrcamento().getIdOrcamento().toString(), us.retornaUsuario(), new Date(), "Orçamento"));
                // System.out.println("Excluído: " + oitTemp.getIdOrcamentoItem() + " - ID: " + oitTemp.getIdOrcamento().getIdOrcamento());
                // System.out.println("------------------------------------------------------");
            }
        } else {
            // System.out.println("Não há registros duplicados.");
        }
    }

    public void ExcluirOrcamento(Orcamento orcamento) {
        getEm().getEntityManagerFactory().getCache().evictAll();
        orcamento = getEm().getReference(Orcamento.class, orcamento.getIdOrcamento());
        getEm().remove(orcamento);
        getEm().merge(BCRUtils.criarAuditoria("delete", orcamento.getIdOrcamento().toString(), us.retornaUsuario(), new Date(), "Orçamento"));
    }

    public Orcamento selecionarPorID(Integer id) {
        Query query = getEm().createQuery("Select o From Orcamento o where o.idOrcamento = :id");
        query.setParameter("id", id);
        getEm().getEntityManagerFactory().getCache().evictAll();
        return (Orcamento) query.getSingleResult();
    }

    public List<Orcamento> listaOrcamentos() {
        getEm().getEntityManagerFactory().getCache().evictAll();
        return getEm().createQuery("SELECT o From Orcamento o ORDER BY o.dtOrcamento").getResultList();
    }

    public List<Orcamento> listaOrcamentosPorPaciente(Integer idPaciente) {
        getEm().getEntityManagerFactory().getCache().evictAll();
        Query query = getEm().createQuery("SELECT o From Orcamento o WHERE o.idPaciente.idPaciente = :id ORDER BY o.dtOrcamento");
        query.setParameter("id", idPaciente);
        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public List<Orcamento> listaOrcamentosAImportar() {
        getEm().getEntityManagerFactory().getCache().evictAll();
        return getEm().createQuery("SELECT o From Orcamento o ORDER BY o.dtOrcamento and o.dt").getResultList();
    }

    public List<Orcamento> listaOrcamentosVencidos() {
        getEm().getEntityManagerFactory().getCache().evictAll();
        return getEm().createQuery("SELECT o From Orcamento o where o.dtValidade <= CURRENT_DATE and o.dtCancelamento IS NULL and o.dtExecucao IS NULL ORDER BY o.dtOrcamento").getResultList();
    }

    public Long totalOrcamentosVencidos() {
        return (Long) getEm().createQuery("SELECT COUNT(o.idOrcamento) From Orcamento o where o.dtValidade <= CURRENT_DATE and o.dtCancelamento IS NULL and o.dtExecucao IS NULL ORDER BY o.dtOrcamento").getSingleResult();
    }

    public Orcamento listaOrcamentoParaVerificarDuplicidade(Integer idPaciente, Integer idTerceiro, Integer idProcedimento, Double valor) {
        getEm().getEntityManagerFactory().getCache().evictAll();
        Query q = getEm().createQuery("SELECT o from Orcamento o where o.idTerceiro.idTerceiro = :idTer and o.idProcedimento.idProcedimento = :idPro "
                + "and o.valor = :valor and o.idPaciente.idPaciente = :idPac");
        q.setParameter("valor", valor);
        q.setParameter("idTer", idTerceiro);
        q.setParameter("idPro", idProcedimento);
        q.setParameter("idPac", idPaciente);
        try {
            return (Orcamento) q.getSingleResult();
        } catch (NoResultException e) {
            return new Orcamento();
        }

    }

    public List<Orcamento> pesquisarOrcamentos2(Date dtInici, Date dtFinal, String nomePaciente, Integer idOrcto, Integer idTerceiro, Integer status, Integer idCirurgia) {
        StringBuffer clausula = new StringBuffer();
        if (dtInici != null && dtFinal != null) {
            clausula.append(" and o.dtOrcamento BETWEEN :dtIni and :dtFim ");
        }
        if (nomePaciente != null) {
            if (nomePaciente.equals(0)) {
                clausula.append(" and o.idPaciente.nome is null");
            } else {
                clausula.append(" and o.idPaciente.nome = :pac ");
            }
        }

        if (idOrcto != 0) {
            clausula.append(" and o.idProcedimento.idProcedimento = :orcto");
        }
        if (idCirurgia != 0) {
            clausula.append(" and o.idOrcamento = :cir");
        }
        if (status != 0) {
            clausula.append(" and o.idStatus.idStatus = :status");
        }

        if (idTerceiro != null) {
            clausula.append(" and o.idTerceiro.idTerceiro = :ter");
        }

        String sql = "Select o From Orcamento o where o.dtOrcamento is not null " + clausula;
        System.out.println(sql);
        if (sql.contains("where  and")) {
            sql = sql.replace("where  and", " where ");
        }
        Query query = getEm().createQuery(sql);
        if (dtInici != null && dtFinal != null) {
            query.setParameter("dtIni", dtInici);
            query.setParameter("dtFim", dtFinal);
        }
        if (nomePaciente != null) {
            if (!nomePaciente.equals("")) {
                query.setParameter("pac", nomePaciente);
            }
        }
        if (idOrcto != 0) {
            query.setParameter("orcto", idOrcto);
        }

        if (idCirurgia != 0) {
            query.setParameter("cir", idCirurgia);
        }
        if (status != 0) {
            query.setParameter("status", status);
        }
        if (idTerceiro != null) {
            query.setParameter("ter", idTerceiro);
        }
        getEm().getEntityManagerFactory().getCache().evictAll();
        System.out.println("Qtd. registros2: " + query.getResultList().size());
        return query.getResultList();
    }

    public List<Orcamento> pesquisarOrcamentos(Date dtInici, Date dtFinal, Integer idPaciente, String situacao, Integer idOrcto, Integer idProcedimento,
            Integer idTerceiro, Integer idStatus, String adiantamento, String semFechamentos, String filtroData) {
        StringBuffer clausula = new StringBuffer();
        if (dtInici != null && dtFinal != null) {
            switch (filtroData) {
                case "Orçamento"  : 
                    clausula.append(" and o.dtOrcamento BETWEEN :dtIni and :dtFim ");
                    break;
                case "Vencimento" : 
                    clausula.append(" and o.dtValidade BETWEEN :dtIni and :dtFim ");
                    break;
                case "Agendado" : 
                    clausula.append(" and o.dtAgendado BETWEEN :dtIni and :dtFim ");
                    break;
                default : 
                    clausula.append(" and o.dtOrcamento BETWEEN :dtIni and :dtFim ");
            }
        }
        
        
        
        
        if (idPaciente != null) {
            if (idPaciente.equals(0)) {
                clausula.append(" and o.idPaciente is null");
            } else {
                clausula.append(" and o.idPaciente.idPaciente = :pac ");
            }
        }
        if (situacao != null) {
            clausula.append(" and o.situacao = :sit ");
        }
        if (idOrcto != null) {
            clausula.append(" and o.idOrcamento = :orcto");
        }
        if (idProcedimento != null) {
            clausula.append(" and o.idProcedimento.idProcedimento = :proc");
        }
        if (idTerceiro != null) {
            clausula.append(" and o.idTerceiro.idTerceiro = :ter");
        }
        if (idStatus != null) {
            if (!idStatus.equals(0)) {
                clausula.append(" and o.idStatus.idStatus = :status");
            }
        }
        if (adiantamento != null) {
            if (adiantamento.equals("Sim")) {
                clausula.append(" and o.adiantamentosHistoricoList IS NOT EMPTY");
            }
        }
        if (semFechamentos != null) {
            if (semFechamentos.equals("Sim")) {
                clausula.append(" and o.idOrcamento NOT IN (SELECT c.idOrcamento.idOrcamento FROM Cirurgia c)");
            } else {
                clausula.append(" and o.idOrcamento IN (SELECT C.idOrcamento.idOrcamento FROM Cirurgia c)");
            }
        }
        String sql = "Select o From Orcamento o where o.dtOrcamento is not null " + clausula;
        //System.out.println(sql);
        if (sql.contains("where  and")) {
            sql = sql.replace("where  and", " where ");
        }
        Query query = getEm().createQuery(sql);
        if (dtInici != null && dtFinal != null) {
            query.setParameter("dtIni", dtInici);
            query.setParameter("dtFim", dtFinal);
        }
        if (idPaciente != null) {
            if (!idPaciente.equals(0)) {
                query.setParameter("pac", idPaciente);
            }
        }
        if (situacao != null) {
            query.setParameter("sit", situacao);
        }
        if (idOrcto != null) {
            query.setParameter("orcto", idOrcto);
        }
        if (idProcedimento != null) {
            query.setParameter("proc", idProcedimento);

        }
        if (idTerceiro != null) {
            query.setParameter("ter", idTerceiro);
        }
        if (idStatus != null) {
            if (!idStatus.equals(0)) {
                query.setParameter("status", idStatus);
            }
        }

        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public void atualizarOrcamento(Orcamento orcamento) {
        orcamento.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
        orcamento.setDtModificacao(new Date());
        getEm().merge(orcamento);
    }

    public List<Orcamento> listarLazyModeWhere(int primeiro, int qtd, String clausula, Date dtInici, Date dtFinal, Integer idPaciente, String situacao, Integer idOrcto) {
        String sql = "Select p From Orcamento p ";
        StringBuffer clausulabf = new StringBuffer();
        if (dtInici != null && dtFinal != null) {
            clausulabf.append(" and o.dtOrcamento BETWEEN :dtIni and :dtFim ");
        }
        if (idPaciente != null) {
            clausulabf.append(" and o.idPaciente.idPaciente = :pac ");
        }
        if (situacao != null) {
            clausulabf.append(" and o.situacao = :sit ");
        }
        if (idOrcto != null) {
            clausulabf.append(" and o.idOrcamento = :orcto");
        }
        if (sql.contains("where  and")) {
            sql = sql.replace("where  and", " where ");
        }
        Query query = getEm().createQuery(sql + clausula + clausulabf);
        if (dtInici != null && dtFinal != null) {
            query.setParameter("dtIni", dtInici);
            query.setParameter("dtFim", dtFinal);
        }
        if (idPaciente != null) {
            query.setParameter("pac", idPaciente);
        }
        if (situacao != null) {
            query.setParameter("sit", situacao);
        }
        if (idOrcto != null) {
            query.setParameter("orcto", idOrcto);
        }
        getEm().getEntityManagerFactory().getCache().evictAll();
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistro(String clausula) {
        Query query = getEm().createQuery("Select COUNT(p.idOrcamento) From Orcamento p " + clausula);
        return (Number) query.getSingleResult();
    }

    public Number contarRegistroParametro(String situacao, Date dtIni, Date dtFim) {
        Query query = getEm().createQuery("SELECT COUNT(o.idOrcamento) FROM Orcamento o where o.dtOrcamento BETWEEN :dtI AND :dtF AND o.situacao LIKE :sit");
        query.setParameter("dtI", dtIni);
        query.setParameter("dtF", dtFim);
        query.setParameter("sit", situacao);
        return (Number) query.getSingleResult();
    }

    public Number contarRegistroParametroStatus(String status, Date dtIni, Date dtFim) {
        Query query = getEm().createQuery("SELECT COUNT(o.idOrcamento) FROM Orcamento o where o.dtOrcamento BETWEEN :dtI AND :dtF AND o.status LIKE :sta");
        query.setParameter("dtI", dtIni);
        query.setParameter("dtF", dtFim);
        query.setParameter("sta", status);
        return (Number) query.getSingleResult();
    }
    
    public List<Grafico> verificarOrcamentosPorPeriodo(Date dtIni, Date dtFim) {
        DateFormat df = new SimpleDateFormat("y-M-d");
        String sql = "SELECT COUNT(o.ID_ORCAMENTO), "
                + "          SUM(o.valor) ,  "
                + "          s.DESCRICAO "
                + "     FROM orcamento o, status s"
                + "    WHERE s.id_status = o.id_status"
                + "      AND o.dt_orcamento between '" + df.format(dtIni) + "' AND '" + df.format(dtFim) + "'"
                + "    GROUP BY s.id_status";
        //System.out.println(sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Grafico> itens = new ArrayList<>();

        for (Object[] c : resultList) {
            Grafico gf = new Grafico();
            gf.setQuantidade(((Long) c[0]).intValue());
            gf.setValorOrcado((Double) c[1]);
            gf.setDescricao((String) c[2]);
            itens.add(gf);
        }
        return itens;
    }

    public void SalvarSimples(Orcamento orc) {
        em.merge(orc);        
    }
}
