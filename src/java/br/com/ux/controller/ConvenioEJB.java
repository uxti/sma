/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Convenio;
import br.com.ux.model.PlanoConvenio;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class ConvenioEJB extends Conexao {

    public void Salvar(Convenio convenio, String usuario, List<PlanoConvenio> planos) {
         getEm().getEntityManagerFactory().getCache().evictAll();
        if (convenio.getIdConvenio() != null) {
             getEm().merge(convenio);
            for (PlanoConvenio p : planos) {
                p.setIdConvenio(convenio);
                 getEm().merge(p);
                 getEm().merge(BCRUtils.criarAuditoria("update", p.getIdPlano().getDescricao(), usuario, new Date(), "Plano"));
            }
             getEm().merge(BCRUtils.criarAuditoria("update", convenio.getDescricao(), usuario, new Date(), "Convênio"));
        } else {
             getEm().merge(convenio);
            for (PlanoConvenio p : planos) {
                p.setIdConvenio(convenio);
                 getEm().merge(p);
                 getEm().merge(BCRUtils.criarAuditoria("insert", p.getIdPlano().getDescricao(), usuario, new Date(), "Plano"));
            }
             getEm().merge(BCRUtils.criarAuditoria("update", convenio.getDescricao(), usuario, new Date(), "Convênio"));
        }
    }

    public void Excluir(Convenio convenio, String usuario) {
        convenio =  getEm().getReference(Convenio.class, convenio.getIdConvenio());
         getEm().remove(convenio);
         getEm().merge(BCRUtils.criarAuditoria("delete", convenio.getDescricao(), usuario, new Date(), "Convênio"));        
    }

    public void ExcluirOrcamentoPlano(PlanoConvenio pc, String usuario) {
        pc =  getEm().getReference(PlanoConvenio.class, pc.getIdPlanoConvenio());
         getEm().getEntityManagerFactory().getCache().evictAll();
         getEm().remove(pc);
         getEm().merge(BCRUtils.criarAuditoria("delete", pc.getIdPlano().getDescricao(), usuario, new Date(), "Plano"));        
        
    }
    
    public Convenio selecionarConvenioPadrao() {
        em.getEntityManagerFactory().getCache().evictAll();
        return (Convenio) em.createQuery("Select c From Convenio c WHERE c.padrao = TRUE").getSingleResult();
    }

    public List<Convenio> listarConvenios() {
         getEm().getEntityManagerFactory().getCache().evictAll();
        return  getEm().createQuery("Select c From Convenio c WHERE c.inativo = FALSE").getResultList();
    }

    public List<Convenio> listarConveniosLazy(int primeiro, int qnt, String clausula) {
         getEm().getEntityManagerFactory().getCache().evictAll();
        Query query =  getEm().createQuery("Select c From Convenio c " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qnt);
        return query.getResultList();
    }

    public Convenio selecionarPorID(Integer id, String usuario) {
         getEm().getEntityManagerFactory().getCache().evictAll();
        Query query =  getEm().createQuery("Select p From Convenio p where p.idConvenio = :id");
        query.setParameter("id", id);
        return (Convenio) query.getSingleResult();
    }
    
    public List<Convenio> listarLazy(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select p From Convenio p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(p.idConvenio) From Convenio p " + clausula);
        return (Number) query.getSingleResult();
    }

}
