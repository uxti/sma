/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.controller.integracao.BuscaDadosMySQLEJB;
import br.com.ux.controller.integracao.BuscaDadosTasyEJB;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.Paciente;
import br.com.ux.model.Servico;
import br.com.ux.model.tasy.AtendimentoPaciente;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.Mensagem;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class AtendimentoPacienteEJB extends Conexao {

    @EJB
    BuscaDadosMySQLEJB bdmEJB;
    @EJB
    TerceiroEJB tEJB;
    @EJB
    BuscaDadosTasyEJB bdTEJB;

    public void Salvar(AtendimentoPaciente ap, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(ap);
        em.flush();
        em.refresh(ap);
        //em.merge(BCRUtils.criarAuditoria("insert", "Nome: ", usuario, new Date(), "Atendimento Paciente"));
    }

    public void Atualizar(AtendimentoPaciente ap, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(ap);
        //em.merge(BCRUtils.criarAuditoria("update", "Nome: " + ap.getIdPaciente().getNome(), usuario, new Date(), "Atendimento Paciente"));
    }

    public List<AtendimentoPaciente> listarAtendimentoPaciente() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select a From AtendimentoPaciente a ORDER BY a.numeroAtendimentoPacienteTasy").getResultList();
    }

    public List<AtendimentoPaciente> listarAtendimentoPacienteSemVinculo() {
        em.getEntityManagerFactory().getCache().evictAll();
        Query q = em.createQuery("Select a From AtendimentoPaciente a ORDER BY a.numeroAtendimentoPacienteTasy");
        return q.getResultList();
    }

    public List<AtendimentoPaciente> listarAtendimentoPacientePorPaciente(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query q = em.createQuery("Select a From AtendimentoPaciente a where a.idPaciente.idPaciente = :id");
        q.setParameter("id", id);
        return q.getResultList();
    }

    public Number contarAtendimentosPacienteRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idAtendimentoPaciente) From AtendimentoPaciente p " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<AtendimentoPaciente> listarAtendimentosPacienteModeWhere(int primeiro, int qtd, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        System.out.println("clausula: " + clausula);
        Query query = em.createQuery("Select p From AtendimentoPaciente p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public AtendimentoPaciente selecionarPorNumeroAtendimentoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT a FROM AtendimentoPaciente a WHERE a.numeroAtendimentoPacienteTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (AtendimentoPaciente) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }

    public void CadastrarAtendimentoPacienteTasy(Object[] c, LogIntegracao logPai) {
        Date dtInicio = new Date();
        if (c[0] != null) {
            //System.out.println("Iniciando no atendimento: " + ((BigDecimal) c[0]).intValue());
            AtendimentoPaciente ap = selecionarPorNumeroAtendimentoTasy(((BigDecimal) c[0]).intValue());
            if (ap == null) {
                // Se o objeto selecionado não for encontrado, realiza o cadastro no SMA.
                cadastrarAtendimentoPaciente(c, dtInicio, logPai);
            } else {
                try {
                    // Se houver diferença em algum campo, atualiza TODOS os campos do objeto do SMA.
                    // ap.setNumeroAtendimentoPacienteTasy(((BigDecimal) c[0]).intValue());
                     if ((String) c[1] != null) {
                        ap.setIdPaciente(bdmEJB.selecionarPorIDPacienteTasySemCadastrarQuandoNull(Integer.parseInt((String) c[1])));
                     }                    
                    // ap.setDtEntrada((Date) c[7]);
                    
                    // if ((String) c[11] != null) {
                    //    ap.setIdServico(bdmEJB.selecionarPorIDProcedimentoTasy(Integer.parseInt((String) c[11])));
                    // }
                    ap.setDescricaoTipoAtendimento((String) c[4]);
                    ap.setStatus((String) c[5]);
                    ap.setObservacao((String) c[6]);
                    ap.setDtEncerramento((Date) c[10]);
                    ap.setTipoAtendimento((String) c[8]);
                    ap.setCategoria((String) c[9]);
                    // Se for 2 (Urgencia) = TRUE, 1 = Eletivo = FALSE
                    ap.setUrgencia(((String) c[12]).equals("02"));
                    
                    Atualizar(ap, "sync");
                    //em.merge(BCRUtils.gerarLogIntegracao("Atendimento " + ((BigDecimal) c[0]).toPlainString() + " atualizado.", "cadastrarAtendimentoPaciente", dtInicio, new Date(), logPai));
                } catch (Exception e) {
                    System.out.println(e);
                    //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar atendimento " + ((BigDecimal) c[0]).toPlainString() + ". " + e.getMessage(), "cadastrarAtendimentoPaciente", null, null, logPai));
                }
            }
        }
    }

    public void atualizarAtendimentoSobDemanda(Object[] c) {
        try {
            if (c != null) {
                AtendimentoPaciente ap = selecionarPorNumeroAtendimentoTasy(((BigDecimal) c[0]).intValue());
                ap.setDescricaoTipoAtendimento((String) c[4]);
                ap.setStatus((String) c[5]);
                ap.setObservacao((String) c[6]);
                ap.setDtEncerramento((Date) c[10]);
                ap.setTipoAtendimento((String) c[8]);
                ap.setUrgencia(((String) c[12]).equals("02"));
                
                Atualizar(ap, "sync");
            }
        } catch (Exception e) {
            System.out.println(e);            
        }
    }

    public void VincularListaAtendimentoPacienteTasy(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {
            CadastrarAtendimentoPacienteTasy(c, logPai);
        }
    }

    public void cadastrarAtendimentoPaciente(Object[] c, Date dtInicio, LogIntegracao logPai) {
        try {
            Paciente p = bdmEJB.selecionarPorIDPacienteTasy(Integer.parseInt((String) c[1]));
            AtendimentoPaciente ap = new AtendimentoPaciente();
            ap.setNumeroAtendimentoPacienteTasy(((BigDecimal) c[0]).intValue());
            ap.setIdPaciente(p);
            ap.setIdMedicoResp(bdmEJB.selecionarPorIDMedicoTasy(Integer.parseInt((String) c[2])));
            ap.setDescricaoTipoAtendimento((String) c[4]);
            ap.setStatus((String) c[5]);
            ap.setObservacao((String) c[6]);
            ap.setDtEntrada((Date) c[7]);
            ap.setTipoAtendimento((String) c[8]);
            ap.setCategoria((String) c[9]);
            ap.setDtEncerramento((Date) c[10]);
            if ((String) c[11] != null) {
                ap.setIdServico(bdmEJB.selecionarPorIDProcedimentoTasy(Integer.parseInt((String) c[11])));
            }
            ap.setUrgencia(((String) c[12]).equals("02"));
            Salvar(ap, "sync");
            //em.merge(BCRUtils.gerarLogIntegracao("Atendimento " + ((BigDecimal) c[0]).toPlainString() + " cadastrado.", "cadastrarAtendimentoPaciente", dtInicio, new Date(), logPai));

        } catch (Exception e) {
            System.out.println(e);
            //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar atendimento " + ((BigDecimal) c[0]).toPlainString() + ". " + e.getMessage(), "cadastrarAtendimentoPaciente", null, null, logPai));
        }
    }
}
