/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Papel;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class PapelEJB extends Conexao {

    public void Salvar(Papel papel, String usuario) {
        Auditoria a = new Auditoria();
        if (papel.getIdPapel() != null) {
             getEm().merge(papel);
            a.setTabela("Papel");
            a.setDescricao("Alteração na tabela papel");
            a.setDtHora(new Date());
            a.setUsuario(usuario);
             getEm().merge(a);
        } else {
             getEm().merge(papel);
            a.setTabela("Papel");
            a.setDescricao("Inserção na tabela papel");
            a.setDtHora(new Date());
            a.setUsuario(usuario);
             getEm().merge(a);
        }
    }

    public void Excluir(Papel papel, String usuario) {
        papel =  getEm().getReference(Papel.class, papel.getIdPapel());
         getEm().remove(papel);
        Auditoria a = new Auditoria();
        a.setTabela("Papel");
        a.setDescricao("Exclusão na tabela papel");
        a.setDtHora(new Date());
        a.setUsuario(usuario);
         getEm().merge(a);
    }

    public List<Papel> listarPapeis() {
        return  getEm().createQuery("Select p From Papel p").getResultList();
    }

    public Papel selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select p From Papel p where p.idPapel = :id");
        query.setParameter("id", id);
        return (Papel) query.getSingleResult();
    }
}
