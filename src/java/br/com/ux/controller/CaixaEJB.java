/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Caixa;
import br.com.ux.model.CaixaPai;
import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Operacao;
import br.com.ux.model.Repasse;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class CaixaEJB extends Conexao {

    private UsuarioSessao us = new UsuarioSessao();

    public List<CaixaPai> listarCaixasPai() {
        Query query = getEm().createQuery("Select c From CaixaPai c order by c.idCaixaPai DESC");
        return query.getResultList();
    }

    public List<CaixaPai> listarCaixasPaiUnico(Usuario u) {
        Query query = getEm().createQuery("Select c From CaixaPai c where c.idUsuario.idUsuario = :idu order by c.idCaixaPai DESC");
        query.setParameter("idu", u.getIdUsuario());
        return query.getResultList();
    }

    public void AbrirCaixa(CaixaPai caixa, List<Caixa> caixas) {
        getEm().persist(caixa);
        Caixa cc = new Caixa();
        cc.setDtCaixa(caixa.getDtCaixaPai());
        cc.setHistorico("Abertura de Caixa");
        cc.setIdCaixaPai(null);
        cc.setTipoLancamento('C');
        cc.setValor(0D);
        getEm().persist(cc);
        for (Caixa c : caixas) {
            c.setIdCaixaPai(null);
            c.setSituacao("Transferido de Caixa Anterior");
            getEm().persist(c);
        }
    }

    public void FecharCaixa(CaixaPai caixaPai) {
        getEm().merge(caixaPai);
    }

    public CaixaPai retornaCaixaAberto(String usuario) {
        Query queryUser = getEm().createQuery("Select u From Usuario u where u.usuario = :user");
        queryUser.setParameter("user", usuario);
        Usuario u = (Usuario) queryUser.getSingleResult();
        Query query = getEm().createQuery("Select cp From CaixaPai cp where  cp.situacao != 'Fechado' and cp.idUsuario.idUsuario = :id ORDER BY cp.idCaixaPai DESC");
        query.setParameter("id", u.getIdUsuario());
        query.setMaxResults(1);
        return (CaixaPai) query.getSingleResult();

    }
    
    public void excluirCaixaPorFechamento(Integer idFechamento) {
        Query query2 = em.createQuery("Delete from Caixa cc where cc.idCirurgia.idCirurgia = :id ");
        query2.setParameter("id", idFechamento);
        query2.executeUpdate();
    }

    public Integer verificaExistenciaCaixaAberto(String usuario) {
        Query queryUser = getEm().createQuery("Select u From Usuario u where u.usuario = :user");
        queryUser.setParameter("user", usuario);
        Usuario u = (Usuario) queryUser.getSingleResult();
        Query query = null;
        query = getEm().createNativeQuery("select count(*) total from caixa_pai where id_usuario = " + u.getIdUsuario() + " and situacao != 'Fechado' and dt_caixa_pai < CURRENT_DATE()");
        Long l = (Long) query.getSingleResult();
        return Integer.parseInt(l.toString());

    }

    public Integer verificaExistenciaCaixaAbertoAtual(String usuario) {
        Query queryUser = getEm().createQuery("Select u From Usuario u where u.usuario = :user");
        queryUser.setParameter("user", usuario);
        Usuario u = (Usuario) queryUser.getSingleResult();
        Query query = null;
//        if (u != null) {
        query = getEm().createNativeQuery("select count(*) total from caixa_pai where id_usuario = " + u.getIdUsuario() + " and situacao = 'Aberto' and dt_caixa_pai = CURRENT_DATE()");
//        }
        Long l = (Long) query.getSingleResult();
        return Integer.parseInt(l.toString());
    }

    public Integer verificarSeExisteCaixaAbertoPendente(String usuario) {
        Query queryUser = getEm().createQuery("Select u From Usuario u where u.usuario = :user");
        queryUser.setParameter("user", usuario);
        Usuario u = (Usuario) queryUser.getSingleResult();
        Query query = null;
//        if (u != null) {
        query = getEm().createNativeQuery("select count(*) total from caixa_pai where id_usuario = " + u.getIdUsuario() + " and situacao != 'Fechado' and dt_caixa_pai < CURRENT_DATE()");
//        }
        Long l = (Long) query.getSingleResult();
        return Integer.parseInt(l.toString());
    }

    public Double pegaSaldoAnterior(String usuario, String tipoCaixa) {
        Query queryUser = getEm().createQuery("Select u From Usuario u where u.usuario = :user");
        queryUser.setParameter("user", usuario);
        Usuario u = (Usuario) queryUser.getSingleResult();
        Query query = getEm().createNativeQuery("select saldo  "
                + "from caixa_pai where dt_caixa_pai < CURRENT_DATE() "
                + "and id_usuario = '" + u.getIdUsuario() + "' and situacao = 'Fechado' and tipo_caixa = '" + tipoCaixa + "' "
                + "order by id_caixa_pai  limit 1 ");
        Double valor;
        try {
            valor = (Double) query.getSingleResult();
        } catch (Exception e) {
            valor = 0D;
        }
        return valor;
    }

    public List<Caixa> carregarItensAbertoCaixaAnterior(String usuario, String tipoCaixa) {
        Query query = getEm().createQuery("Select c From Caixa c where c.dtCaixa < CURRENT_DATE and c.status = 'Não Baixado' and c.idCaixaPai.idUsuario.usuario = :usu and c.idCaixaPai.tipoCaixa = :tipo and c.idCaixaPai.situacao = 'Fechado'");
        query.setParameter("usu", usuario);
        query.setParameter("tipo", tipoCaixa);
        return query.getResultList();
    }

    public boolean retornaRegistroCaixa(Integer idRepasse) {
        Query query = getEm().createQuery("Select c From Caixa c where c.idRepasse.idRepasse = :idRepasse "
                + "and c.situacao = :sit and c.status = :sta");
        query.setParameter("idRepasse", idRepasse);
        query.setParameter("sit", "Aceito");
        query.setParameter("sta", "Não Baixado");
        if (query.getResultList().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean retornaRegistroCheque(Integer idRepasse, Integer usuario) {
        Query query = getEm().createQuery("Select r From Repasse r, Caixa c where r.idRepasse = c.idRepasse.idRepasse and r.idRepasse = :idRepasse and r.idCheque.compensado = :comp and c.idUsuario.idUsuario = :usu");
        query.setParameter("idRepasse", idRepasse);
        query.setParameter("usu", usuario);
        query.setParameter("comp", "S");
        if (query.getResultList().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void LancarRecebimentoNoCaixa(Caixa caixa) {
        getEm().merge(caixa);
        getEm().merge(BCRUtils.criarAuditoria("insert", "Lançamento no caixa. " + caixa.getHistorico(), us.retornaUsuario(), new Date(), "Caixa"));
    }

    public void lancarTransferencia(Caixa caixa2) {
        getEm().persist(caixa2);
        getEm().merge(BCRUtils.criarAuditoria("insert", "Transferência de caixa. " + caixa2.getHistorico(), us.retornaUsuario(), new Date(), "Caixa"));
    }

    public void salvarCaixas(List<Caixa> lista) {
        for (Caixa caixa : lista) {
            getEm().merge(caixa);
            getEm().merge(BCRUtils.criarAuditoria("insert", "Lançamento no caixa. " + caixa.getHistorico(), us.retornaUsuario(), new Date(), "Caixa"));
        }
    }

    public Caixa pegaCaixaPorRepasse(Repasse repasse) {
        Query query = getEm().createQuery("Select c From Caixa c where c.status != 'Baixado' and c.status is not null and c.idRepasse.idRepasse = :id ");
        query.setParameter("id", repasse.getIdRepasse());
        return (Caixa) query.getSingleResult();
    }

    public List<Caixa> pegaListaCaixaPorRepasse(Repasse repasse) {
        Query query = em.createQuery("Select c From Caixa c where c.idRepasse.idRepasse = :id ");
        query.setParameter("id", repasse.getIdRepasse());
        return query.getResultList();
    }
    
    public List<Caixa> ListarCaixaPorLoteOrigem(String loteOrigem) {
        Query query = em.createQuery("Select c From Caixa c where c.loteOrigem = :id ");
        query.setParameter("id", loteOrigem);
        return query.getResultList();
    }

    public void atualizarSaldoCaixaPai(CaixaPai caixaPai, Double valor, char Mov) {
        Double saldo = caixaPai.getSaldo();
        Double novoSaldo = 0D;
        if (Mov == 'D') {
            novoSaldo = saldo - valor;
        } else {
            novoSaldo = saldo + valor;
        }
        caixaPai.setSaldo(novoSaldo);
        getEm().merge(caixaPai);
    }

    public List<Caixa> listarCaixasAgrupados(CaixaPai cp) {
        Query query = null;
        if ("Repasse".equals(cp.getTipoCaixa())) {
            query = getEm().createNativeQuery("select dt_caixa, sum(valor) valor, tipo_lancamento, historico, id_caixa_pai, id_operacao, 'Não Baixado', id_cirurgia, numero_parcela, num_lote, situacao, liberado from caixa where id_caixa_pai = " + cp.getIdCaixaPai() + " and COALESCE(situacao,'Aceito')!='Aguardando' group by dt_caixa,tipo_lancamento, historico, id_caixa_pai, id_operacao, 'Não Baixado',	id_cirurgia, numero_parcela, num_lote,situacao, liberado order by dt_caixa , num_lote, tipo_lancamento  asc");
        } else {
            query = getEm().createNativeQuery("select dt_caixa, sum(valor) valor, tipo_lancamento, historico, id_caixa_pai, id_operacao, status, id_cirurgia, numero_parcela, num_lote, situacao, liberado from caixa where id_caixa_pai = " + cp.getIdCaixaPai() + " group by dt_caixa,tipo_lancamento, historico, id_caixa_pai, id_operacao, status, id_cirurgia, numero_parcela, num_lote,situacao, liberado order by dt_caixa , num_lote, tipo_lancamento  asc");
        }

        List<Object[]> resultList = query.getResultList();

        List<Caixa> caixas = new ArrayList<Caixa>();
        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
            cn.setIdCaixaPai(null);
            if (c[5] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[5]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[6]);
            if (c[7] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[7]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setNumeroParcela((Integer) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setSituacao((String) c[10]);
            cn.setLiberado((String) c[11]);
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosPendencia(CaixaPai cp) {
        Query query = getEm().createNativeQuery("select dt_caixa, sum(valor) valor, tipo_lancamento, historico, id_caixa_pai, id_operacao, status, id_cirurgia, numero_parcela, num_lote, lote_origem, situacao from caixa where id_caixa_pai = " + cp.getIdCaixaPai() + " and situacao = 'Aguardando' and tipo_lancamento = 'C' group by dt_caixa,tipo_lancamento, historico, id_caixa_pai, id_operacao, status,id_cirurgia, numero_parcela,num_lote, lote_origem, situacao order by num_lote, tipo_lancamento asc");
        List<Object[]> resultList = query.getResultList();

        List<Caixa> caixas = new ArrayList<Caixa>();
        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
            cn.setIdCaixaPai(null);
            if (c[5] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[5]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[6]);
            if (c[7] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[7]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setNumeroParcela((Integer) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            cn.setSituacao((String) c[11]);
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosTransferencia(CaixaPai cp) {
        Query query = getEm().createNativeQuery("select num_lote, dt_caixa, sum(valor) valor, tipo_lancamento, historico, id_caixa_pai, id_operacao, status, id_cirurgia, numero_parcela from caixa where id_caixa_pai = " + cp.getIdCaixaPai() + " and status = 'Não Baixado' and tipo_lancamento = 'C' group by num_lote, dt_caixa,tipo_lancamento, historico, id_caixa_pai, id_operacao, status,	id_cirurgia, numero_parcela");
        List<Object[]> resultList = query.getResultList();

        List<Caixa> caixas = new ArrayList<Caixa>();
        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
            cn.setIdCaixaPai(null);
            if (c[6] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[6]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[7]);
            if (c[8] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[8]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setNumeroParcela((Integer) c[9]);
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> pesquisarItensCaixaPorCaixaPaiCirurgiaParcela(CaixaPai caixa, Cirurgia c, Integer parcela) {
        String sql = "Select c From Caixa c where c.idCaixaPai.idCaixaPai = " + caixa.getIdCaixaPai() + " and c.idCirurgia.idCirurgia = " + c.getIdCirurgia() + " and c.numeroParcela = " + parcela + "";
        System.out.println("Comando rodado " + sql);
        Query query = getEm().createQuery("Select c From Caixa c where c.idCaixaPai.idCaixaPai = :cpi and c.idCirurgia.idCirurgia = :cir and c.numeroParcela = :par");
        query.setParameter("cpi", caixa.getIdCaixaPai());
        query.setParameter("cir", c.getIdCirurgia());
        query.setParameter("par", parcela);
        return query.getResultList();
    }

    public List<Caixa> pesquisarItensCaixaPorCaixaPaiCirurgiaParcelaLote(CaixaPai caixa, Cirurgia c, Integer parcela, String numLote) {
        Query query = getEm().createQuery("Select c From Caixa c where c.idCaixaPai.idCaixaPai != " + caixa.getIdCaixaPai() + " and c.idCirurgia.idCirurgia = :cir and c.numeroParcela = :par and c.numLote = :numLote");
        query.setParameter("cir", c.getIdCirurgia());
        query.setParameter("par", parcela);
        query.setParameter("numLote", numLote);
        return query.getResultList();
    }

    public List<CaixaPai> listarCaixasAbertosDiarios(CaixaPai caixaPai) {
        Query query = getEm().createQuery("Select c From CaixaPai c where c.dtCaixaPai = CURRENT_DATE and c.situacao = 'Aberto' and c.idCaixaPai != :id");
        query.setParameter("id", caixaPai.getIdCaixaPai());
        return query.getResultList();
    }

    public void aceitarTransferencia(Caixa caixa) {
        //Atualizar para situacao de aceite no meu caixa
        Query query = getEm().createQuery("UPDATE Caixa c SET c.situacao = 'Aceito', c.status = 'Não Baixado' where c.numLote = :numLote and c.tipoLancamento = 'C'");
        query.setParameter("numLote", caixa.getNumLote());
        query.executeUpdate();

        //Atualizo os registro de débito
        Query query1 = getEm().createQuery("UPDATE Caixa c set c.situacao ='Aceito', c.status ='Baixado' where c.numLote = :numLote and c.tipoLancamento = 'D'");
        query1.setParameter("numLote", caixa.getNumLote());
        query1.executeUpdate();

        //Atualizar registro do lote anterior 
        Query query2 = getEm().createQuery("UPDATE Caixa c set c.situacao= 'Aceito', c.status ='Baixado' where c.numLote = :numLote");
        query2.setParameter("numLote", caixa.getLoteOrigem());
        query2.executeUpdate();
    }

    public String gerarNumeroAleatorio() {
        Date d = new Date();
        Long numAleatorio = d.getTime();
        return numAleatorio.toString();
    }

    public void recusarPendencia(Caixa caixa) {
        //Atualiza os créditos origem para situacao recusado e status Não Baixado
        Query query = getEm().createQuery("Update Caixa c set c.situacao = 'Recusado', c.status = 'Não Baixado' where c.numLote = :numLote and c.tipoLancamento = 'C'");
        query.setParameter("numLote", caixa.getLoteOrigem());
        query.executeUpdate();

        //Deleto todos os registros que tiverem o Lote origem do registro atualizado a cima
        Query query1 = getEm().createQuery("Delete from Caixa c where c.loteOrigem = :lotOrigem");
        query1.setParameter("lotOrigem", caixa.getLoteOrigem());
        query1.executeUpdate();
    }

    public ContaCorrente pesquisarContaCorrentePorRepasse(Repasse r) {
        Query query = getEm().createQuery("Select c From ContaCorrente c where c.idRepasse.idRepasse = :idr");
        query.setParameter("idr", r.getIdRepasse());
        return (ContaCorrente) query.getSingleResult();
    }

    public void trocarCheque(Cheque chqTrocar, Integer id) {
        Query query1 = getEm().createQuery("Update Caixa c set c.idCheque = :chqTrocar where c.idCheque.idCheque = :id");
        query1.setParameter("chqTrocar", chqTrocar);
        query1.setParameter("id", id);
        query1.executeUpdate();
    }

    public void atualizarContaCorrente(ContaCorrente cc) {
        getEm().merge(cc);
        getEm().merge(BCRUtils.criarAuditoria("update", "Conta corrente: " + cc.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Conta Corrente"));
    }

    public void atualizarCaixa(Caixa cc) {
        em.merge(cc);
        em.merge(BCRUtils.criarAuditoria("update", "Lançamento no caixa: " + cc.getHistorico(), us.retornaUsuario(), new Date(), "Caixa"));
    }

    public void Salvar(Caixa c) {
        if (c.getIdCaixa() == null) {
            getEm().persist(c);
            getEm().flush();
            getEm().refresh(c);
            getEm().merge(BCRUtils.criarAuditoria("insert", "Lançamento no caixa: " + c.getHistorico(), us.retornaUsuario(), new Date(), "Caixa"));
        } else {
            getEm().merge(c);
        }
    }

    public void LiberarRepasses(String numLote, CaixaPai c) {
        Query query = getEm().createQuery("UPDATE Caixa c set c.situacao = 'Liberado', c.liberado = 'S' where c.numLote = :numlote and c.idCaixaPai.idCaixaPai = :ipi");
        query.setParameter("numlote", numLote);
        query.setParameter("ipi", c.getIdCaixaPai());
        query.executeUpdate();
        List<Caixa> contas = getEm().createQuery("Select c From Caixa c where c.numLote = :numlot and c.idCaixaPai.idCaixaPai = :ipi2 ")
                .setParameter("numlot", numLote)
                .setParameter("ipi2", c.getIdCaixaPai())
                .getResultList();
        for (Caixa cc : contas) {
            Query query1 = getEm().createQuery("UPDATE ContaCorrente cc set cc.situacao ='Liberado' where cc.idRepasse.idRepasse = :red");
            query1.setParameter("red", cc.getIdRepasse().getIdRepasse());
            query1.executeUpdate();
        }
    }

    public void atualizarCaixaPai(CaixaPai cc) {
        getEm().merge(cc);
    }

    //novos metodos para verificar caixa
    public boolean verificaExistenciaCaixaAbertoDataAnterior(String usuario) {
        Query queryUser = getEm().createQuery("Select u From Usuario u where u.usuario = :user");
        queryUser.setParameter("user", usuario);
        Usuario u = (Usuario) queryUser.getSingleResult();
        Query query = getEm().createQuery("Select COUNT(cp.idCaixaPai) from CaixaPai cp where cp.situacao = 'Aberto' and (cp.dtCaixaPai < CURRENT_DATE OR cp.dtCaixaPai = CURRENT_DATE) and cp.idUsuario.idUsuario = :idu");
        query.setParameter("idu", u.getIdUsuario());
        Long l = (Long) query.getSingleResult();
        if (l > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificaExistenciaCaixaAbertoDataAtual(String usuario) {
        Query queryUser = getEm().createQuery("Select u From Usuario u where u.usuario = :user");
        queryUser.setParameter("user", usuario);
        Usuario u = (Usuario) queryUser.getSingleResult();
        Query query = getEm().createQuery("Select COUNT(cp.idCaixaPai) from CaixaPai cp where cp.situacao = 'Aberto'  and cp.idUsuario.idUsuario = :idu");
        query.setParameter("idu", u.getIdUsuario());
        Long l = (Long) query.getSingleResult();
        if (l > 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<Caixa> itensDoCaixa(String numLote, CaixaPai cp) {
        Query query = getEm().createQuery("Select c From Caixa c where c.idCaixaPai.idCaixaPai = :cp and c.numLote = :nl");
        query.setParameter("cp", cp.getIdCaixaPai());
        query.setParameter("nl", numLote);
        return query.getResultList();
    }

    public boolean retornaSePagamentoFoiTransferido(Cirurgia c) {
        Query query = getEm().createQuery("Select c From Caixa c where c.idCirurgia.idCirurgia = :cp");
        query.setParameter("cp", c.getIdCirurgia());

        List<Caixa> list = query.getResultList();

        if (!list.isEmpty()) {
            for (Caixa cx : list) {
                if (cx.getSituacao() != null) {
                    if (cx.getSituacao().equals("Transferido")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void Excluir(Caixa cx) {
        cx = em.getReference(Caixa.class, cx.getIdCaixa());
        em.remove(cx);
    }
}
