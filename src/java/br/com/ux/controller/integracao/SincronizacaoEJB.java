/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller.integracao;

import br.com.ux.controller.AreaProcedimentoEJB;
import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.AtendimentoPacienteEJB;
import br.com.ux.controller.CBOSaudeEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.CirurgiaEJB;
import br.com.ux.controller.ContaPacienteEJB;
import br.com.ux.controller.EspecialidadeProcedimentoEJB;
import br.com.ux.controller.GrupoProcedimentoEJB;
import br.com.ux.controller.LogIntegracaoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.ProcedimentoPacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.ServicoEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Servico;
import br.com.ux.model.Terceiro;
import br.com.ux.model.tasy.AtendimentoPaciente;
import br.com.ux.model.tasy.ContaPaciente;
import br.com.ux.model.tasy.ProcedimentoPaciente;
import br.com.ux.util.BCRUtils;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

/**
 *
 * @author Charles
 */
@Stateless
public class SincronizacaoEJB {

    @EJB
    BuscaDadosTasyEJB bdTEJB;
    @EJB
    BuscaDadosMySQLEJB bdMEJB;
    @EJB
    LogIntegracaoEJB logEJB;

    @EJB
    PacienteEJB pEJB;
    @EJB
    TerceiroEJB tEJB;
    @EJB
    CBOSaudeEJB cboEJB;
    @EJB
    AreaProcedimentoEJB apEJB;
    @EJB
    EspecialidadeProcedimentoEJB epEJB;
    @EJB
    GrupoProcedimentoEJB gpEJB;
    @EJB
    ServicoEJB sEJB;
    @EJB
    AtendimentoPacienteEJB appEJB;
    @EJB
    ContaPacienteEJB cpEJB;
    @EJB
    ProcedimentoPacienteEJB ppEJB;
    @EJB
    CirurgiaEJB cEJB;
    @EJB
    RepasseEJB rrEJB;
    @EJB
    CaixaEJB cxEJB;
    @EJB
    ChequeEJB chEJB;
    @EJB
    AssociacaoEJB assEJB;

    private ContaPaciente cc;
    private final AtomicBoolean busy = new AtomicBoolean(false);

    //@Asynchronous
    @Schedule(hour = "*", minute = "*", second = "*/5", info = "Sincronizar TASY", persistent = false)
    public void sincronizarTasy() throws InterruptedException, ParseException {

        if (!busy.compareAndSet(false, true)) {
            return;
        }

        try {
            Date dtInicio = new Date();            
            System.out.println("Iniciando sincronização ..." + dtInicio);
            boolean sync = assEJB.carregarAssociacao().getSincronizarTasy() != null ? assEJB.carregarAssociacao().getSincronizarTasy() : false;

            LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Sincronização iniciada.", "sincronizar", dtInicio, null, null);

            // Preparar sincronização. Busca as informações necessárias para sincronização
            List<Object[]> listaPacientes;
            List<Object[]> listaMedicos;

            List<Object[]> listaAreaProcedimentos;
            List<Object[]> listaEspecialidades;
            List<Object[]> listaGruposProcedimentos;
            List<Object[]> listaProcedimentos;

            if (sync) {
                //listaPacientes = bdTEJB.listaCadastroPacienteTasy();
                listaPacientes = bdTEJB.listaCadastroPacienteTasyPorDataAtualizacao(dtInicio);
                listaMedicos = bdTEJB.listaCadastroMedicoTasy();
                listaAreaProcedimentos = bdTEJB.listaCadastroAreaProcedimentoTasy();
                listaEspecialidades = bdTEJB.listaCadastroEspecialidadeProcedimentoTasy();
                listaGruposProcedimentos = bdTEJB.listaCadastroGrupoProcedimentoTasy();
                listaProcedimentos = bdTEJB.listaCadastroProcedimentosCBHPMTasy();
            } else {
                listaPacientes = bdTEJB.listaCadastroPacienteTasyPorDataAtualizacao(dtInicio);
                listaMedicos = bdTEJB.listaCadastroMedicosTasyPorDataAtualizacao(dtInicio);
                listaAreaProcedimentos = bdTEJB.listaCadastroAreaProcedimentoTasyPorData(dtInicio);
                listaEspecialidades = bdTEJB.listaCadastroEspecialidadeProcedimentoTasyPorData(dtInicio);
                listaGruposProcedimentos = bdTEJB.listaCadastroGrupoProcedimentoTasyPorData(dtInicio);
                listaProcedimentos = bdTEJB.listaCadastroProcedimentosCBHPMTasyPorDataAtualizacao(dtInicio);
            }

            System.out.println("Consultas TASY ok.");
            sincronizarDadosPacientes(listaPacientes, logInicio);
            System.out.println("Sincronizando médicos.");
            sincronizarDadosMedicos(listaMedicos, logInicio);
            System.out.println("Sincronizando CBHPM.");
            importarTabelaCBHPM(listaAreaProcedimentos, listaEspecialidades, listaGruposProcedimentos, listaProcedimentos, logInicio);

            // Atendimentos 
            List<Object[]> listaAtendimentos = new ArrayList<>();
            if (assEJB.carregarAssociacao().getIgnorarAtendimentosComTitulos()) {
                // busca atendimentos com titulos já gerados e também todos os atendimentos atualizados no dia, e não na ultima hora.
                System.out.println("Buscando atendimentos.");
                listaAtendimentos = bdTEJB.listaAtendimentosPacientesTasyPorData(dtInicio);
            } else {
                System.out.println("Buscando atendimentos das ultimas duas horas ...");
                listaAtendimentos = bdTEJB.listaAtendimentosPacientesTasyPorDataHora(dtInicio);
            }
            System.out.println("Sincronizando atendimentos.");
            sincronizarAtendimentoPaciente(listaAtendimentos, logInicio);

//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException ex) {
//            }
            Date dtFimAtendimentoPaciente = new Date();
            long diffAtendimentoPaciente = dtFimAtendimentoPaciente.getTime() - dtInicio.getTime();
            long diffSecAtendimentoPaciente = TimeUnit.MILLISECONDS.toSeconds(diffAtendimentoPaciente);

            System.out.println("Cadastros básicos e atendimentos sincronizados em: " + diffSecAtendimentoPaciente + " segundos.");
            sincronizarContaPaciente(listaAtendimentos, logInicio);

//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException ex) {
//            }
            Date dtFimContaPaciente = new Date();
            long diffContaPaciente = dtFimContaPaciente.getTime() - dtInicio.getTime();
            long diffSecContaPaciente = TimeUnit.MILLISECONDS.toSeconds(diffContaPaciente);

            System.out.println("Contas sincronizadas em: " + (diffSecContaPaciente - diffSecAtendimentoPaciente) + " segundos.");
            sincronizarProcedimentoPaciente(listaAtendimentos, logInicio);

//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException ex) {
//            }
            Date dtFimProcedimentoPaciente = new Date();
            long diffProcedimentoPaciente = dtFimProcedimentoPaciente.getTime() - dtInicio.getTime();
            long diffSecProcedimentoPaciente = TimeUnit.MILLISECONDS.toSeconds(diffProcedimentoPaciente);

            System.out.println("Procedimentos sincronizados em: " + (diffSecProcedimentoPaciente - diffSecContaPaciente) + " segundos.");
            atualizarDadosFechamentosExterno(listaAtendimentos, logInicio);

//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException ex) {
//            }
            Date dtFimFechamentosExterno = new Date();
            long diffFechamentosExterno = dtFimFechamentosExterno.getTime() - dtInicio.getTime();
            long diffSecFechamentosExterno = TimeUnit.MILLISECONDS.toSeconds(diffFechamentosExterno);

//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException ex) {
//            }
            Date dtFim = new Date();
            long diff = dtFim.getTime() - dtInicio.getTime();
            long diffSec = TimeUnit.MILLISECONDS.toSeconds(diff);
            System.out.println("Externos sincronizados em: " + (diffSecFechamentosExterno - diffSecProcedimentoPaciente) + " segundos.");
            System.out.println(listaAtendimentos.size() + " atendimentos atualizados em: " + diffSec + " segundos.");
        } finally {
            busy.set(false);
        }
    }

    // Sincronização de dados dos pacientes do TASY com o SMA.
    // Se o paciente não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY. 
    public void sincronizarDadosPacientes(List<Object[]> lista, LogIntegracao logPai) {
        //Date dtInicio = new Date();

        // Compara, vincula e persiste as novas informações no SMA.
        pEJB.verificarCadastroPacientesEditados(lista, logPai);

        //Date dtFim = new Date();
        // Gerar e salvar log de integração.
        //logEJB.Salvar(BCRUtils.gerarLogIntegracao("Sincronização de pacientes finalizada. ", "sincronizarDadosPacientes", dtInicio, dtFim, logPai));
    }

    // Sincronização de dados dos médicos do TASY com o SMA.
    // Se o médico não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY. 
    public void sincronizarDadosMedicos(List<Object[]> lista, LogIntegracao logPai) {
        importarCadastrosBasicos(lista, 12, logPai);
        // Compara, vincula e persiste as novas informações no SMA.
        tEJB.verificarCadastroMedicosEditados(lista, logPai);

//        if (bdMEJB.retornaDataUltimaSincronizacao("sincronizarDadosPacientes") == null) {
//            vincularMedicos();
//        } else {
//            // Importar informações básicas para cadastro ou vincular médico        
//            System.out.println("importando cadastros básicos");
//            importarCadastrosBasicos(lista, 12, dtInicio, logPai);
//            // Compara, vincula e persiste as novas informações no SMA.
//            System.out.println("Comparando lista");
//            tEJB.verificarCadastroMedicosEditados(lista, logPai);
//            System.out.println("Terminando comparação da lista");
//        }
        //Date dtFim = new Date();
        // Gerar e salvar log de integração.
        //logEJB.Salvar(BCRUtils.gerarLogIntegracao("Sincronização de médicos finalizada. ", "sincronizarDadosMedicos", dtInicio, dtFim, logPai));
    }

    // Vinculação dos pacientes do TASY com o SMA.
    // Se o paciente não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.     
    public void vincularPacientes() {
        //Date dtInicio = new Date();

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaCadastroPacienteTasy();

        // Compara, vincula e persiste as novas informações no SMA.
        pEJB.VincularListaPacientesTasy(lista);

        //Date dtFim = new Date();
        // Gerar e salvar log de integração.
        //logEJB.Salvar(BCRUtils.gerarLogIntegracao("Vinculação de pacientes finalizada. " + lista.size() + " verificados.", "vincularPacientes", dtInicio, dtFim, null));
    }

    // Sincronização de dados dos médicos do TASY com o SMA.
    // Se o médico não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    public void vincularMedicos() {
        LogIntegracao logInicio = null;

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaCadastroMedicoTasy();

        // Importar informações básicas para cadastro ou vincular médico        
        importarCadastrosBasicos(lista, 12, logInicio);

        // Compara, vincula e persiste as novas informações no SMA.
        tEJB.VincularListaMedicosTasy(lista, logInicio);

        // Gerar e salvar log de integração.
        //Date dtFim = new Date();
        //LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Vinculação dos médicos finalizada.", "vincularMedicos", dtInicio, dtFim, logInicio);
        //logEJB.Salvar(logFim);
    }

    public void importarCadastrosBasicos(List<Object[]> lista, Integer posicaoArrayVerificador, LogIntegracao logPai) {
        // Verifica se na lista de médicos há algum cbo a importar sob demanda.
        for (Object[] c : lista) {
            // CBO Saude
            if (c[posicaoArrayVerificador] != null) {
                if (cboEJB.selecionarPorIDCboSaudeTasy(((BigDecimal) c[posicaoArrayVerificador]).intValue()) == null) {
                    cboEJB.cadastrarCboSaude(bdTEJB.retornaCadastroCBOSaudeTasyPorID(((BigDecimal) c[posicaoArrayVerificador]).intValue()), logPai);
                }
            }
        }
    }

    // Sincronização de dados do Area Procedimento do TASY com o SMA.
    // Se a area de procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    public void vincularOuImportarAreaProcedimento(List<Object[]> lista, LogIntegracao logInicio) {
        // Busca as informações do banco de dados do TASY
        // Compara, vincula e persiste as novas informações no SMA.
        apEJB.VincularListaAreaProcedimentoTasy(lista, logInicio);
    }

    // Sincronização de dados do Especialidade Procedimento do TASY com o SMA.
    // Se a especialidade procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    public void vincularOuImportarEspecialidadeProcedimento(List<Object[]> lista, LogIntegracao logInicio) {
        // Busca as informações do banco de dados do TASY
        // Compara, vincula e persiste as novas informações no SMA.     
        epEJB.VincularListaEspecialidadeProcedimentoTasy(lista, logInicio);
    }

    // Sincronização de dados do Especialidade Procedimento do TASY com o SMA.
    // Se a especialidade procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    public void vincularOuImportarGrupoProcedimento(List<Object[]> lista, LogIntegracao logInicio) {
        // Busca as informações do banco de dados do TASY
        // Compara, vincula e persiste as novas informações no SMA.     
        gpEJB.VincularListaGrupoProcedimentoTasy(lista, logInicio);
    }

    // Sincronização de dados de Procedimento do TASY com o SMA.
    // Se o procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    // É necessário executar somente uma vez.
    public void sincronizarProcedimentos(List<Object[]> lista, LogIntegracao logInicio) {
        Date dtInicio = new Date();
        // Compara, vincula e persiste as novas informações no SMA.             
        sEJB.VincularListaServicosTasy(lista, logInicio);
    }

    public void importarTabelaCBHPM(List<Object[]> listaArea, List<Object[]> listaEspecialidade, List<Object[]> listaGrupo, List<Object[]> listaProcedimento, LogIntegracao logInicio) {
        //System.out.println("Importando area de Procedimento ...");
        vincularOuImportarAreaProcedimento(listaArea, logInicio);
        //System.out.println("Importando especialidade de Procedimento ...");
        vincularOuImportarEspecialidadeProcedimento(listaEspecialidade, logInicio);
        //System.out.println("Importando grupo de Procedimento ...");
        vincularOuImportarGrupoProcedimento(listaGrupo, logInicio);
        //System.out.println("Sincronizando procedimentos ...");
        sincronizarProcedimentos(listaProcedimento, logInicio);
    }

    // Sincronização de dados dos atendimento do TASY com o SMA.
    // Se o procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    public void sincronizarAtendimentoPaciente(List<Object[]> lista, LogIntegracao logInicio) {
        //Date dtInicio = new Date();

        try {
            // Compara, vincula e persiste as novas informações no SMA.             
            appEJB.VincularListaAtendimentoPacienteTasy(lista, logInicio);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Gerar e salvar log de integração.
        //Date dtFim = new Date();
        //LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Importação dos Atendimentos pacientes finalizada.", "vincularOuImportarProcedimentosCBHPM", dtInicio, dtFim, logInicio);
        //logEJB.Salvar(logFim);
    }

    public void sincronizarAtendimentoPacientePorID(Integer numAtendimento) {
        try {

            //cEJB.ExcluirItem();
            List<Object[]> listaAtendimentos;
            listaAtendimentos = bdTEJB.listaAtendimentosPacientesTasyPorID(numAtendimento);
            appEJB.VincularListaAtendimentoPacienteTasy(listaAtendimentos, new LogIntegracao());

            for (ContaPaciente cp : bdMEJB.selecionarPorNumeroAtendimentoTasy(numAtendimento).getContaPacienteList()) {
                if (cpEJB.verificaSeContaPacienteFoiExcluida(cp.getIdContaPacienteTasy())) {
                    for (ProcedimentoPaciente pp : cp.getProcedimentoPacienteList()) {
                        ppEJB.Excluir(pp);
                    }
                    cpEJB.Excluir(cp);
                }
            }

            for (Object[] c : listaAtendimentos) {

                // atualiza as contas pacientes
                List<Object[]> listaContas = bdTEJB.listaContaPacientesTasyPorAtendimento(((BigDecimal) c[0]).intValue());
                cpEJB.vincularListaContaPacienteTasy(listaContas, new LogIntegracao());

                // atualiza os procedimentos pacientes
                List<Object[]> listaProcedimentos = bdTEJB.listaProcedimentosPacientesTasyPorAtendimento(((BigDecimal) c[0]).intValue());
                ppEJB.VincularListaProcedimentoPacienteTasy(listaProcedimentos, new LogIntegracao());

                // atualiza os fechamentos externos
                List<ContaPaciente> listaContasPaciente = cpEJB.listarContaPacientePorNumeroAtendimentoTasy(((BigDecimal) c[0]).intValue());
                for (ContaPaciente cp : listaContasPaciente) {
                    atualizarFechamentoExternoPorConta(cp);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // Sincronização de dados da Conta Paciente do TASY com o SMA.
    // Se a conta paciente não existir no SMA, realiza o cadastro e vincula aos dados do TASY.    
    public void sincronizarContaPaciente(List<Object[]> listaAtendimentos, LogIntegracao logInicio) {
        //Date dtInicio = new Date();

        try {
            for (Object[] c : listaAtendimentos) {
                // Busca as informações do banco de dados do TASY
                List<Object[]> lista = bdTEJB.listaContaPacientesTasyPorAtendimento(((BigDecimal) c[0]).intValue());
                // Compara, vincula e persiste as novas informações no SMA.
                cpEJB.vincularListaContaPacienteTasy(lista, logInicio);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Gerar e salvar log de integração.
        //Date dtFim = new Date();
        //LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Importação das Contas Paciente finalizada.", "vincularOuImportarContaPaciente", dtInicio, dtFim, logInicio);
        //logEJB.Salvar(logFim);
    }

    // Sincronização de dados da Procedimento Paciente do TASY com o SMA.
    // Se a procedimento paciente não existir no SMA, realiza o cadastro e vincula aos dados do TASY.    
    public void sincronizarProcedimentoPaciente(List<Object[]> listaAtendimentos, LogIntegracao logInicio) {
        //Date dtInicio = new Date();
        try {
            for (Object[] c : listaAtendimentos) {
                // Busca as informações do banco de dados do TASY
                List<Object[]> lista = bdTEJB.listaProcedimentosPacientesTasyPorAtendimento(((BigDecimal) c[0]).intValue());
                // Compara, vincula e persiste as novas informações no SMA.             
                ppEJB.VincularListaProcedimentoPacienteTasy(lista, logInicio);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Gerar e salvar log de integração.
        //Date dtFim = new Date();
        //LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Importação das Procedimento Paciente finalizada.", "vincularOuImportarProcedimentoPaciente", dtInicio, dtFim, logInicio);
        //logEJB.Salvar(logFim);
    }

    // Atualizar valores    
    public void atualizarDadosFechamentosExterno(List<Object[]> listaAtendimentos, LogIntegracao logInicio) {
        //Date dtInicio = new Date();
        try {
            for (Object[] c : listaAtendimentos) {
                List<ContaPaciente> listaContas = cpEJB.listarContaPacientePorNumeroAtendimentoTasy(((BigDecimal) c[0]).intValue());
                for (ContaPaciente cp : listaContas) {
                    atualizarFechamentoExternoPorConta(cp);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Gerar e salvar log de integração.
        //Date dtFim = new Date();
        //LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Atualização dos Valores dos Fechamentos Externos finalizada.", "atualizarDadosFechamentosExterno", dtInicio, dtFim, logInicio);
        //logEJB.Salvar(logFim);
    }

    public void atualizarFechamentoExternoPorConta(ContaPaciente cc) {
        try {
            List<Cirurgia> listaFechamentos = cpEJB.listarFechamentosPorConta(cc.getIdContaPacienteTasy());
            List<CirurgiaItem> listaItensFechamento;

            // Excluir fechamentos externos se a conta paciente vinculada estiver como cancelada
            List<Cirurgia> listaFechTemp = cpEJB.listarFechamentosPorConta(cc.getIdContaPacienteTasy());
            int i = 0;
            for (Cirurgia cirurgia : listaFechTemp) {
                if (cirurgia.isExterno()) {
                    if (cirurgia.getIdContaPaciente().getCancelada() && !cirurgia.isPago()) {
                        listaFechamentos.remove(i);
                        estornarPagamento(cirurgia);
                        cEJB.Excluir(cirurgia);
                    } else {
                        i++;
                    }
                }
            }

            for (Cirurgia c : listaFechamentos) {

//                // CHARLES - 27032018
//                // Exclui todos os registros para verificar inconscistências.
//                if (c.isExterno() && !c.isPago()) {
//                    ppEJB.deletarProcedimentoPacientePorContaPaciente(c.getIdContaPaciente().getIdContaPaciente());
//                    for (CirurgiaItem ci : c.getCirurgiaItemList()) {
//                        cEJB.ExcluirItem(ci);
//                    }
//                }
                List<CirurgiaItem> listaItensSalvos = cEJB.retornaListaItensPorFechamento(c.getIdCirurgia());
                if (c.isExterno() && !c.isPago()) {
                    listaItensFechamento = cpEJB.gerarListaDeServicosPorContaPaciente(c.getIdContaPaciente());
                    if (!listaItensSalvos.isEmpty()) {
                        try {
                            c.setCirurgiaItemList(null);
                            for (CirurgiaItem ci : listaItensFechamento) {
                                //System.out.println("cc: " + c.getIdContaPaciente().getIdContaPaciente());
                                if (ci.getIdProcedimentoPaciente() != null && ci.getIdCirurgia() != null) {
                                    ProcedimentoPaciente pp = ci.getIdProcedimentoPaciente();
                                    ci.setIdServico(pp.getIdServico());
                                    ci.setOrigem("TASY");
                                    ci.setQuantidade(pp.getQtdProcedimento());
                                    if (pp.getIdMedico() != null) {
                                        ci.setIdTerceiro(pp.getIdMedico());
                                    }
                                    if (ci.getDesconto() == null) {
                                        ci.setDesconto(0D);
                                    }

                                    Double valorSMA = cpEJB.retornaValorAjustadoPorRegraRepasse(pp);
                                    Double novoValorTotalGeral = (valorSMA + pp.getValorProcedimento() + ci.getDesconto());
                                    //System.out.println("entrando no: " + ci.getIdCirurgiaItem());
                                    ci = cpEJB.recalcularValoresItem(ci, novoValorTotalGeral);
                                    ci.setIdProcedimentoPaciente(pp);
                                }
                            }
//                            c.setValor(calcularTotalItensGeral(listaItensFechamento));
//                            cEJB.AtualizarExterno(c, listaItensFechamento);
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                    } else {
                        //listaItensFechamento = cpEJB.gerarListaDeServicosPorContaPaciente(c.getIdContaPaciente());
                        for (CirurgiaItem ci : listaItensFechamento) {
                            ci.setDesconto(0D);
                        }
                        cEJB.SalvarExterno(c, listaItensFechamento);
                    }
                    // CHARLES - 14032018
                    // Atualiza os dados totais do atendimento.
                    // UPDATE - CHARLES - 22032018
                    c.setValor(calcularTotalItensGeral(listaItensFechamento));
                    cEJB.SalvarFechamentoSimples(c);
                }
                // CHARLES - 22032018
                // Excluir fechamentos sem itens.
                if (c.isExterno() && !c.isPago()) {
                    listaItensFechamento = cpEJB.gerarListaDeServicosPorContaPaciente(c.getIdContaPaciente());
                    if (calcularTotalItensGeral(listaItensFechamento) < 0D || listaItensFechamento.isEmpty()) {
                        //System.out.println("excluindo atendimento:" + c.getIdContaPaciente().getIdAtendimentoPaciente().getNumeroAtendimentoPacienteTasy() + " total: " + calcularTotalItensGeral(cpEJB.gerarListaDeServicosPorContaPaciente(c.getIdContaPaciente())));
                        cEJB.Excluir(c);
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Erro ao atualizar fechamento externo por conta");
            System.out.println(e);
        }
    }

    public void estornarPagamento(Cirurgia c) {
        try {
            for (Repasse r : c.getRepasseList()) {
                rrEJB.excluirRepasseEContasCorrente(r);
            }
            cxEJB.excluirCaixaPorFechamento(c.getIdCirurgia());
            chEJB.excluirChequesPorFechamento(c.getIdCirurgia(), "sync");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private Double calcularTotalItensGeral(List<CirurgiaItem> lista) {
        Double valorTotal = 0D;
        for (CirurgiaItem cirurgiaItem : lista) {
            valorTotal += cirurgiaItem.getValorTotal();
        }
        return valorTotal;
    }

    // Cadastro em tabelas descritivas por demanda.
    @Lock(LockType.READ)
    public Paciente cadastrarPacienteTasySobDemanda(Integer id) {
        Object[] p = bdTEJB.retornaCadastroPacienteTasyPorID(id);
        if (p != null) {
            pEJB.VincularPacientesTasy(p);
            return bdMEJB.selecionarPorIDPacienteTasySemCadastrarQuandoNull(id);
        } else {
            p = null;
            return null;
        }
    }

    public boolean cadastrarPacienteTasySobDemandaPorNome(String nome, String cpf) {
        Object[] p = bdTEJB.retornaCadastroPacienteTasyPorNome(nome, cpf);
        if (p != null) {
            pEJB.VincularPacientesTasy(p);
            return true;
        } else {
            p = null;
            return false;
        }
    }

    public AtendimentoPaciente cadastrarAtendimentoPacienteTasySobDemanda(Integer id) {
        Object[] p = bdTEJB.retornaCadastroAtendimentoPacienteTasyPorID(id);
        if (p != null) {
            appEJB.cadastrarAtendimentoPaciente(p, new Date(), null);
            return bdMEJB.selecionarPorNumeroAtendimentoTasy(id);
        } else {
            p = null;
            return null;
        }
    }

    public Terceiro cadastrarMedicoTasySobDemanda(Integer id) {
        Object[] p = bdTEJB.retornaCadastroMedicoTasyPorID(id);
        if (p != null) {
            tEJB.VincularMedicosTasy(p, null);
            return bdMEJB.selecionarPorIDMedicoTasy(id);
        } else {
            p = null;
            return null;
        }
    }

    public Servico cadastrarServicoTasySobDemanda(Integer id) {
        Object[] p = bdTEJB.retornaProcedimentoCbhpmTasyPorID(id);
        if (p != null) {
            sEJB.VincularServicosProcedimentosTasy(p, null);
            return bdMEJB.selecionarPorIDProcedimentoTasy(id);
        } else {
            p = null;
            return null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SincronizacaoEJB other = (SincronizacaoEJB) obj;
        if (!Objects.equals(this.cc, other.cc)) {
            return false;
        }
        return true;
    }

}
