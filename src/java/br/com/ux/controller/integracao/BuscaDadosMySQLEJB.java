/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller.integracao;

import br.com.ux.model.LogIntegracao;
import br.com.ux.model.Paciente;
import br.com.ux.model.Servico;
import br.com.ux.model.Terceiro;
import br.com.ux.model.tasy.AtendimentoPaciente;
import java.util.Date;
import br.com.ux.util.Conexao;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class BuscaDadosMySQLEJB extends Conexao {
    
    @EJB
    SincronizacaoEJB syncEJB;
    
    public Date retornaDataUltimaSincronizacao(String metodo) {
        Query query = em.createQuery("SELECT max(l.dtInicio) FROM LogIntegracao l where l.metodo = :met");
        query.setParameter("met", metodo);
        try {
            return (Date) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    public LogIntegracao selecionarLogPai(Integer id) {
        Query query = em.createQuery("SELECT l FROM LogIntegracao l where l.idLogIntegracao = :id");
        query.setParameter("id", id);
        try {
             return (LogIntegracao) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e.getMessage() + "em: " + id);
            return null;
        }
       
    }
    
    public Paciente selecionarPorIDPacienteTasySemCadastrarQuandoNull(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Paciente p where p.idPacienteTasy = :id");
        query.setParameter("id", id);
        try {
            return (Paciente) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    public Paciente selecionarPorIDPacienteTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Paciente p where p.idPacienteTasy = :id");
        query.setParameter("id", id);
        try {
            return (Paciente) query.getSingleResult();
        } catch (Exception e) {
            return syncEJB.cadastrarPacienteTasySobDemanda(id);
        }
    }
    
    public Terceiro selecionarPorIDMedicoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select t From Terceiro t where t.idTerceiroTasy = :id");
        query.setParameter("id", id);
        try {
            return (Terceiro) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    public Servico selecionarPorIDProcedimentoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT s FROM Servico s WHERE s.idProcedimentoTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (Servico) query.getSingleResult();
        } catch (Exception e) {
            //System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }
    
    public AtendimentoPaciente selecionarPorNumeroAtendimentoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT a FROM AtendimentoPaciente a WHERE a.numeroAtendimentoPacienteTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (AtendimentoPaciente) query.getSingleResult();
        } catch (Exception e) {
            //System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }
    
    
    
    
    
}
