/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller.integracao;

import br.com.ux.model.CirurgiaItem;
import br.com.ux.util.Conexao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class BuscaDadosTasyEJB extends Conexao {

    public List<Object[]> listaCadastroPacienteTasy() {
        String sql = "SELECT DISTINCT   \n"
                + "       P.CD_PESSOA_FISICA ID,\n"
                + "       P.NM_PESSOA_PESQUISA NOME,\n"
                + "       (SELECT NM_CONTATO_PESQUISA FROM COMPL_PESSOA_FISICA WHERE IE_TIPO_COMPLEMENTO = 5 AND CD_PESSOA_FISICA = P.CD_PESSOA_FISICA) NOME_MAE,\n"
                + "       P.DT_NASCIMENTO DT_NASCIMENTO,\n"
                + "       regexp_replace(LPAD(P.NR_CPF, 11, '0'), '([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})','\\1.\\2.\\3-\\4') CPF,   \n"
                + "       (SELECT DECODE(NR_TELEFONE, NULL, NULL, '(' || NR_DDD_TELEFONE || ') ' || REPLACE(NR_TELEFONE, ' ', '')) FROM COMPL_PESSOA_FISICA WHERE IE_TIPO_COMPLEMENTO = 1 AND CD_PESSOA_FISICA = P.CD_PESSOA_FISICA) TELEFONE, \n"
                + "       DECODE(P.NR_TELEFONE_CELULAR, NULL, NULL, '(' || P.NR_DDD_CELULAR || ') ' || REPLACE(P.NR_TELEFONE_CELULAR, ' ', '')) TELEFONE_CELULAR\n"
                + "     FROM PESSOA_FISICA P, COMPL_PESSOA_FISICA CP \n"
                + "    WHERE P.CD_PESSOA_FISICA = CP.CD_PESSOA_FISICA ORDER BY 1";
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroPacienteTasyPorDataAtualizacao(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y");
        String data = df.format(dataUltimaSincronizacao);
        String sql = "SELECT DISTINCT   \n"
                + "       P.CD_PESSOA_FISICA ID,\n"
                + "       P.NM_PESSOA_PESQUISA NOME,\n"
                + "       (SELECT NM_CONTATO_PESQUISA FROM COMPL_PESSOA_FISICA WHERE IE_TIPO_COMPLEMENTO = 5 AND CD_PESSOA_FISICA = P.CD_PESSOA_FISICA) NOME_MAE,\n"
                + "       P.DT_NASCIMENTO DT_NASCIMENTO,\n"
                + "       translate(to_char(P.NR_CPF / 100, '000,000,000.00'), ',.', '.-') CPF,   \n"
                + "       (SELECT DECODE(NR_TELEFONE, NULL, NULL, '(' || NR_DDD_TELEFONE || ') ' || REPLACE(NR_TELEFONE, ' ', '')) FROM COMPL_PESSOA_FISICA WHERE IE_TIPO_COMPLEMENTO = 1 AND CD_PESSOA_FISICA = P.CD_PESSOA_FISICA) TELEFONE, \n"
                + "       DECODE(P.NR_TELEFONE_CELULAR, NULL, NULL, '(' || P.NR_DDD_CELULAR || ') ' || REPLACE(P.NR_TELEFONE_CELULAR, ' ', '')) TELEFONE_CELULAR\n"
                + "     FROM PESSOA_FISICA P, COMPL_PESSOA_FISICA CP \n"
                + "    WHERE (P.DT_ATUALIZACAO >= '" + data + "' OR CP.DT_ATUALIZACAO >= '" + data + "') AND P.CD_PESSOA_FISICA = CP.CD_PESSOA_FISICA";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public Object[] retornaCadastroPacienteTasyPorID(Integer id) {
        String sql = "SELECT      \n"
                + "       P.CD_PESSOA_FISICA ID,\n"
                + "       P.NM_PESSOA_PESQUISA NOME,\n"
                + "       (SELECT NM_CONTATO_PESQUISA FROM COMPL_PESSOA_FISICA WHERE IE_TIPO_COMPLEMENTO = 5 AND CD_PESSOA_FISICA = P.CD_PESSOA_FISICA) NOME_MAE,\n"
                + "       P.DT_NASCIMENTO DT_NASCIMENTO,\n"
                + "       regexp_replace(LPAD(P.NR_CPF, 11, '0'), '([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})','\\1.\\2.\\3-\\4') CPF,   \n"
                + "       (SELECT DECODE(NR_TELEFONE, NULL, NULL, '(' || NR_DDD_TELEFONE || ') ' || REPLACE(NR_TELEFONE, ' ', '')) FROM COMPL_PESSOA_FISICA WHERE IE_TIPO_COMPLEMENTO = 1 AND CD_PESSOA_FISICA = P.CD_PESSOA_FISICA) TELEFONE, \n"
                + "       DECODE(P.NR_TELEFONE_CELULAR, NULL, NULL, '(' || P.NR_DDD_CELULAR || ') ' || REPLACE(P.NR_TELEFONE_CELULAR, ' ', '')) TELEFONE_CELULAR\n"
                + "     FROM PESSOA_FISICA P \n"
                + "    WHERE P.CD_PESSOA_FISICA = " + id;
        try {
            Query query = getEmTasy().createNativeQuery(sql);
            query.setMaxResults(1);
            return (Object[]) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro no SQL: " + sql);
            System.out.println(e.getMessage());
            return null;
        }
    }

    public Object[] retornaCadastroPacienteTasyPorNome(String nome, String cpf) {
        String sql = "SELECT      \n"
                + "       P.CD_PESSOA_FISICA ID,\n"
                + "       P.NM_PESSOA_PESQUISA NOME,\n"
                + "       (SELECT NM_CONTATO_PESQUISA FROM COMPL_PESSOA_FISICA WHERE IE_TIPO_COMPLEMENTO = 5 AND CD_PESSOA_FISICA = P.CD_PESSOA_FISICA) NOME_MAE,\n"
                + "       P.DT_NASCIMENTO DT_NASCIMENTO,\n"
                + "       regexp_replace(LPAD(P.NR_CPF, 11, '0'), '([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})','\\1.\\2.\\3-\\4') CPF,   \n"
                + "       (SELECT DECODE(NR_TELEFONE, NULL, NULL, '(' || NR_DDD_TELEFONE || ') ' || REPLACE(NR_TELEFONE, ' ', '')) FROM COMPL_PESSOA_FISICA WHERE IE_TIPO_COMPLEMENTO = 1 AND CD_PESSOA_FISICA = P.CD_PESSOA_FISICA) TELEFONE, \n"
                + "       DECODE(P.NR_TELEFONE_CELULAR, NULL, NULL, '(' || P.NR_DDD_CELULAR || ') ' || REPLACE(P.NR_TELEFONE_CELULAR, ' ', '')) TELEFONE_CELULAR\n"
                + "     FROM PESSOA_FISICA P \n"
                + "    WHERE (P.NM_PESSOA_PESQUISA LIKE '%" + nome + "%' OR P.NR_CPF = '" + cpf + "')";
        try {
            //System.out.println("buscando por " + nome + " - cpf: " + cpf);
            Query query = getEmTasy().createNativeQuery(sql);
            query.setMaxResults(1);
            return (Object[]) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro no SQL: " + sql);
            System.out.println(e.getMessage());
            return null;
        }
    }

    public List<Object[]> listaCadastroMedicoTasy() {
        String sql = "SELECT "
                + "       p.cd_pessoa_fisica ID, " // c[0]
                + "       p.nm_pessoa_pesquisa NOME_MEDICO, " // c[1]
                + "       DECODE(m.cd_cgc, NULL, 'F', 'J') TIPO_PESSOA, " // c[2]
                + "       DECODE(m.cd_cgc, NULL, regexp_replace(LPAD(P.NR_CPF, 11, '0'), '([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})','\\1.\\2.\\3-\\4'), regexp_replace(LPAD(m.cd_cgc, 14, '0'), '([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})','\\1.\\2.\\3/\\4-\\5')) CGCCPF," // c[3]
                + "       m.ds_entidade_juridica NOME_FANTASIA, " // c[4]
                + "       j.ds_razao_social RAZAO_SOCIAL, " // c[5]
                + "       p.nr_identidade RG, " // c[6]
                + "       DECODE(CP.NR_TELEFONE, NULL, NULL, '(' || CP.NR_DDD_TELEFONE || ') ' || REPLACE(CP.NR_TELEFONE, ' ', '')) TELEFONE, " // c[7]
                + "       DECODE(P.NR_TELEFONE_CELULAR, NULL, NULL, '(' || P.NR_DDD_CELULAR || ') ' || REPLACE(P.NR_TELEFONE_CELULAR, ' ', '')) TELEFONE_CELULAR, " // c[8]
                + "       c.sg_conselho TIPO_CONSELHO, " // c[9]
                + "       p.ds_codigo_prof NUMERO_CONSELHO, " // c[10]
                + "       p.ie_sexo SEXO, " // c[11]
                + "       p.nr_seq_cbo_saude id_cbo_saude, " // c[12]
                + "       DECODE(m.ie_situacao, 'A', 0, 1) INATIVO " // c[13]
                + "     FROM pessoa_fisica p left outer join compl_pessoa_fisica cp on p.cd_pessoa_fisica = cp.cd_pessoa_fisica, "
                + "          medico m left outer join pessoa_juridica j on m.cd_cgc = j.cd_cgc, conselho_profissional c"
                + "    WHERE p.cd_pessoa_fisica = m.cd_pessoa_fisica "
                + "      AND p.nr_seq_conselho = c.nr_sequencia "
                + "      AND decode(cp.ie_tipo_complemento, null, 1) = 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroMedicosTasyPorDataAtualizacao(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y");
        String data = df.format(dataUltimaSincronizacao);
        String sql = "SELECT "
                + "       p.cd_pessoa_fisica ID, " // c[0]
                + "       p.nm_pessoa_pesquisa NOME_MEDICO, " // c[1]
                + "       DECODE(m.cd_cgc, NULL, 'F', 'J') TIPO_PESSOA, " // c[2]
                + "       DECODE(m.cd_cgc, NULL, translate(to_char(P.NR_CPF / 100, '000,000,000.00'), ',.', '.-'), regexp_replace(LPAD(m.cd_cgc, 14, '0'), '([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})','\\1.\\2.\\3/\\4-\\5')) CGCCPF," // c[3]
                + "       m.ds_entidade_juridica NOME_FANTASIA, " // c[4]
                + "       j.ds_razao_social RAZAO_SOCIAL, " // c[5]
                + "       p.nr_identidade RG, " // c[6]
                + "       DECODE(CP.NR_TELEFONE, NULL, NULL, '(' || CP.NR_DDD_TELEFONE || ') ' || REPLACE(CP.NR_TELEFONE, ' ', '')) TELEFONE, " // c[7]
                + "       DECODE(P.NR_TELEFONE_CELULAR, NULL, NULL, '(' || P.NR_DDD_CELULAR || ') ' || REPLACE(P.NR_TELEFONE_CELULAR, ' ', '')) TELEFONE_CELULAR, " // c[8]
                + "       c.sg_conselho TIPO_CONSELHO, " // c[9]
                + "       p.ds_codigo_prof NUMERO_CONSELHO, " // c[10]
                + "       p.ie_sexo SEXO, " // c[11]
                + "       p.nr_seq_cbo_saude id_cbo_saude, " // c[12]
                + "       DECODE(m.ie_situacao, 'A', 0, 1) INATIVO " // c[13]
                + "     FROM pessoa_fisica p left outer join compl_pessoa_fisica cp on p.cd_pessoa_fisica = cp.cd_pessoa_fisica, "
                + "          medico m left outer join pessoa_juridica j on m.cd_cgc = j.cd_cgc, conselho_profissional c"
                + "    WHERE p.cd_pessoa_fisica = m.cd_pessoa_fisica "
                + "      AND p.nr_seq_conselho = c.nr_sequencia "
                + "     AND (P.DT_ATUALIZACAO >= '" + data + "' OR CP.DT_ATUALIZACAO >= '" + data + "' OR M.DT_ATUALIZACAO >= '" + data + "' OR j.DT_ATUALIZACAO >= '" + data + "')"
                + "      AND decode(cp.ie_tipo_complemento, null, 1, cp.ie_tipo_complemento) = 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public Object[] retornaCadastroMedicoTasyPorID(Integer id) {
        try {
            String sql = "SELECT "
                    + "       p.cd_pessoa_fisica ID, " // c[0]
                    + "       p.nm_pessoa_pesquisa NOME_MEDICO, " // c[1]
                    + "       DECODE(m.cd_cgc, NULL, 'F', 'J') TIPO_PESSOA, " // c[2]
                    + "       DECODE(m.cd_cgc, NULL, regexp_replace(LPAD(P.NR_CPF, 11, '0'), '([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})','\\1.\\2.\\3-\\4'), regexp_replace(LPAD(m.cd_cgc, 14, '0'), '([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})','\\1.\\2.\\3/\\4-\\5')) CGCCPF," // c[3]
                    + "       m.ds_entidade_juridica NOME_FANTASIA, " // c[4]
                    + "       j.ds_razao_social RAZAO_SOCIAL, " // c[5]
                    + "       p.nr_identidade RG, " // c[6]
                    + "       DECODE(CP.NR_TELEFONE, NULL, NULL, '(' || CP.NR_DDD_TELEFONE || ') ' || REPLACE(CP.NR_TELEFONE, ' ', '')) TELEFONE, " // c[7]
                    + "       DECODE(P.NR_TELEFONE_CELULAR, NULL, NULL, '(' || P.NR_DDD_CELULAR || ') ' || REPLACE(P.NR_TELEFONE_CELULAR, ' ', '')) TELEFONE_CELULAR, " // c[8]
                    + "       c.sg_conselho TIPO_CONSELHO, " // c[9]
                    + "       p.ds_codigo_prof NUMERO_CONSELHO, " // c[10]
                    + "       p.ie_sexo SEXO, " // c[11]
                    + "       p.nr_seq_cbo_saude id_cbo_saude, " // c[12]
                    + "       DECODE(m.ie_situacao, 'A', 0, 1) INATIVO " // c[13]
                    + "     FROM pessoa_fisica p, medico m, compl_pessoa_fisica cp, pessoa_juridica j, conselho_profissional c "
                    + "    WHERE p.cd_pessoa_fisica = m.cd_pessoa_fisica "
                    + "      AND m.cd_cgc = j.cd_cgc(+) "
                    + "      AND p.cd_pessoa_fisica = cp.cd_pessoa_fisica(+) "
                    + "      AND p.nr_seq_conselho = c.nr_sequencia(+) "
                    + "      AND (cp.ie_tipo_complemento in(1, 2) or cp.ie_tipo_complemento is null)"
                    + "      AND p.cd_pessoa_fisica = " + id;
            //System.out.println("Escrevendo o SQL: " + sql);
            Query query = getEmTasy().createNativeQuery(sql);
            query.setMaxResults(1);
            return (Object[]) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }

    }

    public List<Object[]> listaCadastroCBOSaudeTasy() {
        String sql = "SELECT "
                + "       nr_sequencia,"
                + "       cd_cbo,"
                + "       ds_cbo,"
                + "       DECODE(ie_situacao, 'A', 1, 0)"
                + "     FROM cbo_saude"
                + "     ORDER BY 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public Object[] retornaCadastroCBOSaudeTasyPorID(Integer id) {
        String sql = "SELECT "
                + "       nr_sequencia,"
                + "       cd_cbo,"
                + "       ds_cbo,"
                + "       DECODE(ie_situacao, 'A', 1, 0)"
                + "     FROM cbo_saude "
                + "    WHERE nr_sequencia = " + id
                + "     ORDER BY 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        query.setMaxResults(1);
        return (Object[]) query.getSingleResult();
    }

    public Object[] retornaCadastroAreaProcedimentoPorID(Integer id) {
        String sql = "SELECT "
                + "       cd_area_procedimento,"
                + "       ds_area_procedimento,"
                + "       ie_origem_proced"
                + "     FROM area_procedimento"
                + "    WHERE cd_area_procedimento = " + id
                + "    ORDER BY 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        query.setMaxResults(1);
        return (Object[]) query.getSingleResult();
    }

    public List<Object[]> listaCadastroAreaProcedimentoTasy() {
        String sql = "SELECT "
                + "       cd_area_procedimento,"
                + "       ds_area_procedimento,"
                + "       ie_origem_proced"
                + "     FROM area_procedimento"
                + "    ORDER BY 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroAreaProcedimentoTasyPorData(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y");
        String data = df.format(dataUltimaSincronizacao);
        String sql = "SELECT "
                + "       cd_area_procedimento,"
                + "       ds_area_procedimento,"
                + "       ie_origem_proced"
                + "     FROM area_procedimento"
                + "    WHERE DT_ATUALIZACAO >= '" + data + "' "
                + "    ORDER BY 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroEspecialidadeProcedimentoTasy() {
        String sql = "SELECT"
                + "       cd_especialidade,"
                + "       ds_especialidade,"
                + "       cd_area_procedimento"
                + "     FROM especialidade_proc"
                + "    ORDER BY 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroEspecialidadeProcedimentoTasyPorData(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y");
        String data = df.format(dataUltimaSincronizacao);
        String sql = "SELECT"
                + "       cd_especialidade,"
                + "       ds_especialidade,"
                + "       cd_area_procedimento"
                + "     FROM especialidade_proc"
                + "    WHERE DT_ATUALIZACAO >= '" + data + "' "
                + "    ORDER BY 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroGrupoProcedimentoTasy() {
        String sql = "SELECT"
                + "       cd_grupo_proc,"
                + "       ds_grupo_proc,"
                + "       cd_especialidade,"
                + "       DECODE(ie_situacao, 'A', 1, 0)"
                + "     FROM grupo_proc";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroGrupoProcedimentoTasyPorData(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y");
        String data = df.format(dataUltimaSincronizacao);
        String sql = "SELECT"
                + "       cd_grupo_proc,"
                + "       ds_grupo_proc,"
                + "       cd_especialidade,"
                + "       DECODE(ie_situacao, 'A', 1, 0)"
                + "     FROM grupo_proc"
                + "    WHERE DT_ATUALIZACAO >= '" + data + "' ";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroProcedimentosCBHPMTasy() {
        String sql = "SELECT "
                + "       p.cd_procedimento,"
                + "       p.ds_procedimento_pesquisa,"
                + "       p.cd_grupo_proc,"
                + "       decode(p.ie_classificacao, 1, 'Procedimentos', 2, 'Serviços', 'Diárias') classificacao,"
                + "       p.ie_origem_proced, "
                + "       pi.ds_proc_exame apelido"
                + "     FROM procedimento p, proc_interno pi"
                + "    WHERE p.cd_procedimento = pi.cd_procedimento "
                + "      AND p.ie_origem_proced in (1, 5)";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaCadastroProcedimentosCBHPMTasyPorDataAtualizacao(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y");
        String data = df.format(dataUltimaSincronizacao);
        String sql = "SELECT "
                + "       p.cd_procedimento,"
                + "       p.ds_procedimento_pesquisa,"
                + "       p.cd_grupo_proc,"
                + "       decode(p.ie_classificacao, 1, 'Procedimentos', 2, 'Serviços', 'Diárias') classificacao, "
                + "       p.ie_origem_proced, "
                + "       pi.ds_proc_exame apelido"
                + "     FROM PROCEDIMENTO p, PROC_INTERNO pi"
                + "    WHERE pi.CD_PROCEDIMENTO = p.CD_PROCEDIMENTO "
                + "      AND p.DT_ATUALIZACAO >= '" + data + "' "
                + "      AND p.ie_origem_proced IN (1,5) ";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public Object[] retornaProcedimentoCbhpmTasyPorID(Integer id) {
        String sql = "SELECT "
                + "       cd_procedimento,"
                + "       ds_procedimento_pesquisa,"
                + "       cd_grupo_proc,"
                + "       decode(ie_classificacao, 1, 'Procedimentos', 2, 'Serviços', 'Diárias') classificacao, "
                + "       ie_origem_proced"
                + "     FROM PROCEDIMENTO"
                + "    WHERE cd_procedimento = " + id + " AND ie_origem_proced = 5";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        query.setMaxResults(1);
        return (Object[]) query.getSingleResult();
    }

    public List<Object[]> listaAtendimentosPacientesTasy() {
        String sql = "SELECT "
                + "       a.nr_atendimento, " // c[0]
                + "       a.cd_pessoa_fisica, " // c[1]
                + "       a.cd_medico_resp, " // c[2]
                + "       a.cd_medico_atendimento, " // c[3]
                + "       DECODE(a.ie_tipo_atendimento, 1, 'Internado', 3, 'Pronto Socorro', 7, 'Externo', 8, 'Atendimento Ambulatorial', 'Atendimento') tipo_atendimento, " // c[4]
                + "       SUBSTR(obter_valor_dominio(1060, a.ie_status_atendimento), 1, 30) status_atendimento, " // c[5]
                + "       a.ds_observacao, " // c[6]
                + "       a.dt_entrada, " // c[7]
                + "       c.ds_convenio, " // c[8]
                + "       cc.ds_categoria, " // c[9]
                + "       a.dt_alta," // c[10]
                + "       DECODE(substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10),0, NULL,substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10))   , " // c[11]
                + "       a.IE_CARATER_INTER_SUS " // c[12]
                + "     FROM atendimento_paciente a, atend_categoria_convenio acc, convenio c, categoria_convenio cc"
                + "    WHERE a.nr_atendimento = acc.nr_atendimento"
                + "      AND c.cd_convenio = acc.cd_convenio"
                + "      AND cc.cd_categoria = acc.cd_categoria"
                + "      AND c.cd_convenio = cc.cd_convenio"
                + "      AND a.nr_atendimento in (select nr_atendimento from procedimento_paciente pp, convenio c where c.cd_convenio = pp.cd_convenio and c.ie_tipo_convenio = 1)"
                + "      ORDER BY 1";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaAtendimentosPacientesTasyPorData(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y");
        String data = df.format(dataUltimaSincronizacao);
        String sql = "SELECT "
                + "       a.nr_atendimento, "        // c[0]
                + "       a.cd_pessoa_fisica, "      // c[1]
                + "       a.cd_medico_resp, "        // c[2]
                + "       a.cd_medico_atendimento, " // c[3]
                + "       DECODE(a.ie_tipo_atendimento, 1, 'Internado', 3, 'Pronto Socorro', 7, 'Externo', 8, 'Atendimento Ambulatorial', 'Atendimento') tipo_atendimento, " // c[4]
                + "       SUBSTR(obter_valor_dominio(1060, a.ie_status_atendimento), 1, 30) status_atendimento, " // c[5]
                + "       a.ds_observacao, "         // c[6]
                + "       a.dt_entrada, "            // c[7]
                + "       c.ds_convenio, "           // c[8]
                + "       cc.ds_categoria, "         // c[9]
                + "       a.dt_alta,"                // c[10]
                + "       DECODE(substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10),0, NULL,substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10)), " // c[11]
                + "       a.IE_CARATER_INTER_SUS  " // c[12]
                + "     FROM atendimento_paciente a, conta_paciente cp, atend_categoria_convenio acc, convenio c, categoria_convenio cc"
                + "    WHERE a.nr_atendimento = acc.nr_atendimento"
                + "      AND c.cd_convenio = acc.cd_convenio"
                + "      AND cc.cd_categoria = acc.cd_categoria"
                + "      AND c.cd_convenio = cc.cd_convenio"
                + "      AND cp.nr_atendimento = a.nr_atendimento"
                + "      AND ((a.nr_atendimento in (select a.nr_atendimento from atendimento_paciente a, conta_paciente cc, procedimento_paciente pp, convenio c where a.nr_atendimento = cc.nr_atendimento and a.nr_atendimento = pp.nr_atendimento and c.cd_convenio = pp.cd_convenio  AND c.ie_tipo_convenio = 1 and ((pp.dt_atualizacao >= '" + data + "' AND pp.cd_medico_executor is not null) or a.dt_atualizacao >= '" + data + "' or cc.dt_atualizacao >= '" + data + "'))))"
                + "      GROUP BY "
                + "         a.nr_atendimento,"
                + "         a.cd_pessoa_fisica,"
                + "         a.cd_medico_resp,"
                + "         a.cd_medico_atendimento,"
                + "         DECODE(a.ie_tipo_atendimento, 1, 'Internado', 3, 'Pronto Socorro', 7, 'Externo', 8, 'Atendimento Ambulatorial', 'Atendimento') ,"
                + "         SUBSTR(obter_valor_dominio(1060, a.ie_status_atendimento), 1, 30) ,"
                + "         a.ds_observacao,"
                + "         a.dt_entrada,"
                + "         c.ds_convenio,"
                + "         cc.ds_categoria,"
                + "         a.dt_alta,"
                + "         DECODE(substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10),0, NULL,substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10)),"
                + "         a.IE_CARATER_INTER_SUS";
        System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }
    
    public List<Object[]> listaAtendimentosPacientesTasyPorDataHora(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y HH:mm:ss");
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataUltimaSincronizacao);
        cal.add(Calendar.HOUR, -1);
        Date horaInicial = cal.getTime();
        cal.setTime(dataUltimaSincronizacao);
        cal.add(Calendar.HOUR, +1);
        Date horaFinal = cal.getTime();
        
        String dataInicio = df.format(horaInicial);
        String dataFinal = df.format(horaFinal);
        
        String sql = "SELECT "
                + "       a.nr_atendimento, " // c[0]
                + "       a.cd_pessoa_fisica, " // c[1]
                + "       a.cd_medico_resp, " // c[2]
                + "       a.cd_medico_atendimento, " // c[3]
                + "       DECODE(a.ie_tipo_atendimento, 1, 'Internado', 3, 'Pronto Socorro', 7, 'Externo', 8, 'Atendimento Ambulatorial', 'Atendimento') tipo_atendimento, " // c[4]
                + "       SUBSTR(obter_valor_dominio(1060, a.ie_status_atendimento), 1, 30) status_atendimento, " // c[5]
                + "       a.ds_observacao, " // c[6]
                + "       a.dt_entrada, " // c[7]
                + "       c.ds_convenio, " // c[8]
                + "       cc.ds_categoria, " // c[9]
                + "       a.dt_alta," // c[10]
                + "       DECODE(substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10),0, NULL,substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10)), " // c[11]
                + "       a.IE_CARATER_INTER_SUS " // c[12]
                + "     FROM atendimento_paciente a, conta_paciente cp, atend_categoria_convenio acc, convenio c, categoria_convenio cc"
                + "    WHERE a.nr_atendimento = acc.nr_atendimento"
                + "      AND c.cd_convenio = acc.cd_convenio"
                + "      AND cc.cd_categoria = acc.cd_categoria"
                + "      AND c.cd_convenio = cc.cd_convenio"
                + "      AND cp.nr_atendimento = a.nr_atendimento"
                + "      AND substr(obter_titulo_conta_protocolo(0, cp.NR_INTERNO_CONTA),1,100) IS NULL "
                + "      AND ((a.nr_atendimento in (select a.nr_atendimento from atendimento_paciente a, conta_paciente cc, procedimento_paciente pp, convenio c where a.nr_atendimento = cc.nr_atendimento and a.nr_atendimento = pp.nr_atendimento and c.cd_convenio = pp.cd_convenio  AND c.ie_tipo_convenio = 1 and ((pp.dt_atualizacao between TO_DATE('" + dataInicio + "', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dataFinal + "', 'DD/MM/YYYY HH24:MI:SS') and pp.cd_medico_executor is not null) or a.dt_atualizacao between TO_DATE('" + dataInicio + "', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dataFinal + "', 'DD/MM/YYYY HH24:MI:SS') or cc.dt_atualizacao between TO_DATE('" + dataInicio + "', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dataFinal + "', 'DD/MM/YYYY HH24:MI:SS')))))"
                + "      GROUP BY "
                + "         a.nr_atendimento,"
                + "         a.cd_pessoa_fisica,"
                + "         a.cd_medico_resp,"
                + "         a.cd_medico_atendimento,"
                + "         DECODE(a.ie_tipo_atendimento, 1, 'Internado', 3, 'Pronto Socorro', 7, 'Externo', 8, 'Atendimento Ambulatorial', 'Atendimento') ,"
                + "         SUBSTR(obter_valor_dominio(1060, a.ie_status_atendimento), 1, 30) ,"
                + "         a.ds_observacao,"
                + "         a.dt_entrada,"
                + "         c.ds_convenio,"
                + "         cc.ds_categoria,"
                + "         a.dt_alta,"
                + "         DECODE(substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10),0, NULL,substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10)),"
                + "         a.IE_CARATER_INTER_SUS";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaAtendimentosPacientesTasyPorID(Integer id) {
        String sql = "SELECT "
                + "       a.nr_atendimento, " // c[0]
                + "       a.cd_pessoa_fisica, " // c[1]
                + "       a.cd_medico_resp, " // c[2]
                + "       a.cd_medico_atendimento, " // c[3]
                + "       DECODE(a.ie_tipo_atendimento, 1, 'Internado', 3, 'Pronto Socorro', 7, 'Externo', 8, 'Atendimento Ambulatorial', 'Atendimento') tipo_atendimento, " // c[4]
                + "       SUBSTR(obter_valor_dominio(1060, a.ie_status_atendimento), 1, 30) status_atendimento, " // c[5]
                + "       a.ds_observacao, " // c[6]
                + "       a.dt_entrada, " // c[7]
                + "       c.ds_convenio, " // c[8]
                + "       cc.ds_categoria, " // c[9]
                + "       a.dt_alta," // c[10]
                + "       DECODE(substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10),0, NULL,substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10))     , " // c[11]
                + "       a.IE_CARATER_INTER_SUS " // c[12]
                + "     FROM atendimento_paciente a, atend_categoria_convenio acc, convenio c, categoria_convenio cc"
                + "    WHERE a.nr_atendimento = acc.nr_atendimento"
                + "      AND c.cd_convenio = acc.cd_convenio"
                + "      AND cc.cd_categoria = acc.cd_categoria"
                + "      AND c.cd_convenio = cc.cd_convenio"
                + "      AND a.nr_atendimento in ( " + id + " )";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public Object[] retornaCadastroAtendimentoPacienteTasyPorID(Integer id) {
        try {
            String sql = "SELECT "
                    + "       a.nr_atendimento, " // c[0]
                    + "       a.cd_pessoa_fisica, " // c[1]
                    + "       a.cd_medico_resp, " // c[2]
                    + "       a.cd_medico_atendimento, " // c[3]
                    + "       DECODE(a.ie_tipo_atendimento, 1, 'Internado', 3, 'Pronto Socorro', 7, 'Externo', 8, 'Atendimento Ambulatorial', 'Atendimento') tipo_atendimento, " // c[4]
                    + "       SUBSTR(obter_valor_dominio(1060,a.ie_status_atendimento),1,30) status_atendimento, " // c[5]
                    + "       a.ds_observacao, " // c[6]
                    + "       a.dt_entrada, " // c[7]
                    + "       c.ds_convenio, " // c[8]
                    + "       cc.ds_categoria, " // c[9]
                    + "       a.dt_alta," // c[10]
                    + "       DECODE(substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10),0, NULL,substr(obter_proc_principal(a.nr_atendimento, c.cd_convenio, a.ie_tipo_atendimento,0,'C'), 1, 10))    , " // c[11]
                    + "       a.IE_CARATER_INTER_SUS " // c[12]
                    + "     FROM atendimento_paciente a, atend_categoria_convenio acc, convenio c, categoria_convenio cc"
                    + "    WHERE a.nr_atendimento = acc.nr_atendimento"
                    + "      AND c.cd_convenio = acc.cd_convenio"
                    + "      AND cc.cd_categoria = acc.cd_categoria"
                    + "      AND c.cd_convenio = cc.cd_convenio"
                    + "      AND a.nr_atendimento = " + id;
            // System.out.println("Escrevendo o SQL: " + sql);
            Query query = getEmTasy().createNativeQuery(sql);
            query.setMaxResults(1);
            return (Object[]) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public List<Object[]> listaContaPacientesTasy() {
        String sql = "SELECT\n"
                + "      nr_interno_conta,\n"
                + "      nr_atendimento,\n"
                + "      DECODE(cd_proc_princ, 0, NULL, cd_proc_princ),\n"
                + "      dt_periodo_inicial,\n"
                + "      dt_acerto_conta,\n"
                + "      dt_atualizacao,\n"
                + "      DECODE(ie_status_acerto, 1, 'Provisório', 2, 'Definitivo') status,\n"
                + "      obter_valor_conta(nr_interno_conta, 0) valor_total,\n"
                + "      obter_valor_conta(nr_interno_conta, 1) valor_procedimento,\n"
                + "      obter_valor_conta(nr_interno_conta, 2) valor_material,\n"
                + "      obter_valor_conta(nr_interno_conta, 3) valor_medico,\n"
                + "      obter_valor_conta(nr_interno_conta, 7) valor_diaria,\n"
                + "      DECODE(ie_cancelamento, 'C', 1, 0) cancelado"
                + "    FROM conta_paciente\n"
                + "   WHERE nr_atendimento in (select nr_atendimento from procedimento_paciente pp, convenio c where c.cd_convenio = pp.cd_convenio and c.ie_tipo_convenio = 1)";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaContaPacientesTasyPorData(Date dataUltimaSincronizacao) {
        DateFormat df = new SimpleDateFormat("d\\M\\y");
        String data = df.format(dataUltimaSincronizacao);
        String sql = "SELECT\n"
                + "      nr_interno_conta,\n"
                + "      nr_atendimento,\n"
                + "      cd_proc_princ,\n"
                + "      dt_periodo_inicial,\n"
                + "      dt_acerto_conta,\n"
                + "      dt_atualizacao,\n"
                + "      DECODE(ie_status_acerto, 1, 'Provisório', 2, 'Definitivo') status,\n"
                + "      obter_valor_conta(nr_interno_conta, 0) valor_total,\n"
                + "      obter_valor_conta(nr_interno_conta, 1) valor_procedimento,\n"
                + "      obter_valor_conta(nr_interno_conta, 2) valor_material,\n"
                + "      obter_valor_conta(nr_interno_conta, 3) valor_medico,\n"
                + "      obter_valor_conta(nr_interno_conta, 7) valor_diaria,\n"
                + "      DECODE(ie_cancelamento, 'C', 1, 0) cancelado"
                + "    FROM conta_paciente\n"
                + "   WHERE nr_atendimento in (select nr_atendimento from procedimento_paciente pp, convenio c where c.cd_convenio = pp.cd_convenio and c.ie_tipo_convenio = 1)"
                + "     AND dt_atualizacao >= '" + data + "'";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaContaPacientesTasyPorAtendimento(Integer id) {
        String sql = "SELECT\n"
                + "      cc.nr_interno_conta,\n" // c[0]
                + "      cc.nr_atendimento,\n" // c[1]
                + "      cc.cd_proc_princ,\n" // c[2]
                + "      cc.dt_periodo_inicial,\n" // c[3]
                + "      cc.dt_acerto_conta,\n" // c[4]
                + "      cc.dt_atualizacao,\n" // c[5]
                + "      DECODE(cc.ie_status_acerto, 1, 'Provisório', 2, 'Definitivo') status,\n" // c[6]
                + "      obter_valor_conta(cc.nr_interno_conta, 0) valor_total,\n" // c[7]
                + "      obter_valor_conta(cc.nr_interno_conta, 1) valor_procedimento,\n" // c[8]
                + "      obter_valor_conta(cc.nr_interno_conta, 2) valor_material,\n" // c[9]
                + "      obter_valor_conta(cc.nr_interno_conta, 3) valor_medico,\n" // c[10]
                + "      obter_valor_conta(cc.nr_interno_conta, 7) valor_diaria,\n" // c[11]
                + "      DECODE(cc.ie_cancelamento, 'C', 1, 0) cancelado \n" // c[12]
                + "    FROM conta_paciente cc, convenio c\n"
                + "   WHERE c.cd_convenio = cc.cd_convenio_parametro "
                + "     AND c.ie_tipo_convenio = 1 "
                + "     AND NVL(cc.ie_cancelamento, 'X') != 'E' "
                + "     AND cc.nr_atendimento = " + id;
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaContaPacientesTasyPorID(Integer id) {
        String sql = "SELECT\n"
                + "      nr_interno_conta,\n" // c[0]
                + "      nr_atendimento,\n" // c[1]
                + "      cd_proc_princ,\n" // c[2]
                + "      dt_periodo_inicial,\n" // c[3]
                + "      dt_acerto_conta,\n" // c[4]
                + "      dt_atualizacao,\n" // c[5]
                + "      DECODE(ie_status_acerto, 1, 'Provisório', 2, 'Definitivo') status,\n" // c[6]
                + "      obter_valor_conta(nr_interno_conta, 0) valor_total,\n" // c[7]
                + "      obter_valor_conta(nr_interno_conta, 1) valor_procedimento,\n" // c[8]
                + "      obter_valor_conta(nr_interno_conta, 2) valor_material,\n" // c[9]
                + "      obter_valor_conta(nr_interno_conta, 3) valor_medico,\n" // c[10]
                + "      obter_valor_conta(nr_interno_conta, 7) valor_diaria,\n" // c[11]
                + "      DECODE(ie_cancelamento, 'C', 1, 0) cancelado \n" // c[12]
                + "    FROM conta_paciente\n"
                + "   WHERE nr_interno_conta = " + id;
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaProcedimentosPacientesTasy() {
        String sql = "SELECT\n"
                + "      nr_sequencia id_procedimento_paciente_tasy,\n" // c[0]
                + "      nr_atendimento,\n" // c[1]
                + "      nr_interno_conta,\n" // c[2]
                + "      cd_procedimento,\n" // c[3]
                + "      cd_medico_executor,\n" // c[4]
                + "      dt_procedimento,\n" // c[5]
                + "      dt_atualizacao,\n" // c[6]
                + "      qt_procedimento,\n" // c[7]
                + "      vl_procedimento,\n" // c[8]
                + "      vl_medico, \n" // c[9]
                + "      ie_origem_proced, " // c[10]
                + "      NR_SEQ_PROC_PACOTE " // c[11]
                + "    FROM procedimento_paciente_v";
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaProcedimentosPacientesTasyPorContaPaciente(Integer id) {
        String sql = "SELECT\n"
                + "      nr_sequencia id_procedimento_paciente_tasy,\n" // c[0]
                + "      nr_atendimento,\n" // c[1]
                + "      nr_interno_conta,\n" // c[2]
                + "      cd_procedimento,\n" // c[3]
                + "      cd_medico_executor,\n" // c[4]
                + "      dt_procedimento,\n" // c[5]
                + "      dt_atualizacao,\n" // c[6]
                + "      qt_procedimento,\n" // c[7]
                + "      vl_procedimento,\n" // c[8]
                + "      vl_medico, " // c[9]
                + "      NR_SEQ_PROC_PACOTE " // c[10]
                + "    FROM procedimento_paciente_v"
                + "   WHERE nr_interno_conta = " + id;
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaProcedimentosPacientesTasyPorAtendimento(Integer id) {
        String sql = "SELECT\n"
                + "      p.nr_sequencia id_procedimento_paciente_tasy,\n" // c[0]
                + "      p.nr_atendimento,\n" // c[1]
                + "      p.nr_interno_conta,\n" // c[2]
                + "      p.cd_procedimento,\n" // c[3]
                + "      p.cd_medico_executor,\n" // c[4]
                + "      p.dt_procedimento,\n" // c[5]
                + "      p.dt_atualizacao,\n" // c[6]
                + "      p.qt_procedimento,\n" // c[7]
                + "      p.vl_procedimento,\n" // c[8]
                + "      p.vl_medico, \n" // c[9]
                + "      p.cd_motivo_exc_conta \n," // c[10]
                + "      l.dt_laudo, " // c[11]
                + "      l.cd_medico_resp, " // c[12]
                + "      NR_SEQ_PROC_PACOTE " // c[13]
                + "    FROM procedimento_paciente_v p, convenio c, laudo_paciente l"
                + "   WHERE p.cd_convenio = c.cd_convenio "
                + "     AND l.nr_seq_proc(+) = p.nr_sequencia"
                + "     AND c.ie_tipo_convenio = 1"
                + "     AND p.nr_atendimento = " + id;
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public List<Object[]> listaProcedimentosPacientesTasyPorID(Integer id) {
        String sql = "SELECT\n"
                + "      nr_sequencia id_procedimento_paciente_tasy,\n" // c[0]
                + "      nr_atendimento,\n" // c[1]
                + "      nr_interno_conta,\n" // c[2]
                + "      cd_procedimento,\n" // c[3]
                + "      cd_medico_executor,\n" // c[4]
                + "      dt_procedimento,\n" // c[5]
                + "      dt_atualizacao,\n" // c[6]
                + "      qt_procedimento,\n" // c[7]
                + "      vl_procedimento,\n" // c[8]
                + "      vl_medico, \n"
                + "      cd_motivo_exc_conta \n" // c[9]
                + "    FROM procedimento_paciente_v"
                + "   WHERE nr_sequencia = " + id;
        //System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEmTasy().createNativeQuery(sql);
        return query.getResultList();
    }

    public void atualizarValoresTasy(CirurgiaItem ci) {
        Double vlProcedimento = ci.getValorTasy();
        Integer nrSequencia = ci.getIdProcedimentoPaciente().getIdProcedimentoPacienteTasy();
        String sql = "UPDATE procedimento_paciente "
                + "     SET ie_valor_informado = 'S', "
                + "          vl_procedimento = " + vlProcedimento + " , "
                + "          vl_custo_operacional = " + vlProcedimento + " , "
                + "          vl_medico = 0, "
                + "          vl_materiais = 0 "
                + "    WHERE nr_sequencia = " + nrSequencia;
        Query query = emTasy.createNativeQuery(sql);
        query.executeUpdate();
    }

}
