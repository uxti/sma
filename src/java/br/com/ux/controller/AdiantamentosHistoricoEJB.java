/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.AdiantamentosHistorico;
import br.com.ux.model.Orcamento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class AdiantamentosHistoricoEJB extends Conexao {
    
    private UsuarioSessao us;

    public void Salvar(AdiantamentosHistorico ah, String usuario) {
        if (ah.getIdAdiantamentoHistorico() == null) {
             getEm().merge(BCRUtils.criarAuditoria("insert", "ID Orçamento: " + ah.getIdOrcamento().getIdOrcamento() + ". Valor: " + ah.getValor() + ". " + ah.getObservacao(), usuario, new Date(), "Adiantamentos em Orçamentos"));
             getEm().persist(ah);
             getEm().flush();
        } else {
             getEm().merge(ah);
             getEm().merge(BCRUtils.criarAuditoria("update", "ID Orçamento: " + ah.getIdOrcamento().getIdOrcamento() + ". Valor: " + ah.getValor()+ ". " + ah.getObservacao(), usuario, new Date(), "Adiantamentos em Orçamentos"));
        }
    }

    public void Excluir(AdiantamentosHistorico ah, String usuario) {
        ah =  getEm().getReference(AdiantamentosHistorico.class, ah.getIdAdiantamentoHistorico());
         getEm().remove(ah);
         getEm().merge(BCRUtils.criarAuditoria("delete", "ID Orçamento: " + ah.getIdOrcamento().getIdOrcamento() + ". Valor: " + ah.getValor()+ ". " + ah.getObservacao(), usuario, new Date(), "Adiantamentos em Orçamentos"));
    }

    public void SalvaremOrcamento(AdiantamentosHistorico ah, Orcamento o, String usuario) {
        if (o.getIdOrcamento() != null) {
            ah.setIdOrcamento(o);
        }
         getEm().merge(BCRUtils.criarAuditoria("insert", "ID Orçamento: " + ah.getIdOrcamento().getIdOrcamento() + ". Valor: " + ah.getValor()+ ". " + ah.getObservacao(), usuario, new Date(), "Adiantamentos em Orçamentos"));
         getEm().merge(ah);
    }

    public void SalvarListaemOrcamento(List<AdiantamentosHistorico> ad, Orcamento o) {
        for (AdiantamentosHistorico ah : ad) {
            if (o.getIdOrcamento() != null) {
                ah.setIdOrcamento(o);
            }
             getEm().merge(ah);
             getEm().merge(BCRUtils.criarAuditoria("insert", "ID Orçamento: " + ah.getIdOrcamento().getIdOrcamento() + ". Valor: " + ah.getValor()+ ". " + ah.getObservacao(), us.retornaUsuario(), new Date(), "Adiantamentos em Orçamentos"));
        }
    }

    public void atualizarAdiantamento(AdiantamentosHistorico adiantamentosHistorico, Integer ID, Double Valor, String OBS) {
         getEm().merge(adiantamentosHistorico);        
    }

    public List<AdiantamentosHistorico> listaAdiantamentosHistoricos() {
        return  getEm().createQuery("Select a From AdiantamentosHistorico a GROUP BY a").getResultList();
    }

    public List<AdiantamentosHistorico> pesquisaAdiantamentosHistoricoPorOrcamento(Integer idOrcamento) {
        Query query =  getEm().createQuery("Select a From AdiantamentosHistorico a where a.idOrcamento.idOrcamento = :id");
        query.setParameter("id", idOrcamento);
        return query.getResultList();
    }

    public AdiantamentosHistorico selecionarPorID(Integer id) {
        Query query =  getEm().createQuery("Select a From AdiantamentosHistorico a where a.idAdiantamentoHistorico = :id");
        query.setParameter("id", id);
        return (AdiantamentosHistorico) query.getSingleResult();
    }
}
