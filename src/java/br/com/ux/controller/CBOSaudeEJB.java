/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.CboSaude;
import br.com.ux.model.LogIntegracao;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class CBOSaudeEJB extends Conexao {

    public void Salvar(CboSaude cboSaude, String usuario, Boolean auditoria) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(cboSaude);
        em.flush();
        em.refresh(cboSaude);
        if (auditoria) {
            em.merge(BCRUtils.criarAuditoria("insert", "Nome: " + cboSaude.getDescricao(), usuario, new Date(), "CBO Saude"));
        }
    }

    public void Atualizar(CboSaude cboSaude, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(cboSaude);
        em.merge(BCRUtils.criarAuditoria("update", "Nome: " + cboSaude.getDescricao(), usuario, new Date(), "CBO Saude"));
    }

    public List<CboSaude> listarCboSaude() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select c From CboSaude c ORDER BY c.idCboSaude").getResultList();
    }

    public CboSaude selecionarPorIDCboSaudeTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT c FROM CboSaude c WHERE c.idCboSaudeTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (CboSaude) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }

    public CboSaude retornaCboSaudePorCBO(String cbo) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT c FROM CboSaude c WHERE c.codigoCbo = :cbo");
        query.setParameter("cbo", cbo);
        query.setMaxResults(1);
        try {
            return (CboSaude) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + cbo);
            return null;
        }
    }

    public void VincularCboSaudeTasy(Object[] c, LogIntegracao logPai) {
        CboSaude cs = new CboSaude();
        Date dtInicio = new Date();
        if ((String) c[1] != null) {
            // Busca de CBO Saude por CBO
            cs = retornaCboSaudePorCBO((String) c[1]);
        } else {
            cs = retornaCboSaudePorCBO((String) c[1]);
        }

        if (cs != null) {
            if (cs.getIdCboSaudeTasy() == null) {
                try {
                    // Cria o vinculo atraves do ID_CBO_SAUDE_TASY
                    cs.setIdCboSaudeTasy(Integer.parseInt((String) c[0]));
                    Salvar(cs, "sync", false);
                    //em.merge(BCRUtils.gerarLogIntegracao(cs.getDescricao() + " vinculado.", "VincularCboSaudeTasy", dtInicio, new Date(), logPai));
                } catch (Exception e) {
                    System.out.println(e);
                    //em.merge(BCRUtils.gerarLogIntegracao("Erro ao vincular " + (String) c[1] + ". " + e.getMessage(), "VincularCboSaudeTasy", null, null, logPai));
                }
            }
        } else {
            // Se ele não encontrar o CBO no SMA, salva a novo CBO.  
            cadastrarCboSaude(c, logPai);
        }
    }

    public void VincularListaCboSaudeTasy(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {
            VincularCboSaudeTasy(c, logPai);
        }
    }

    public void cadastrarCboSaude(Object[] c, LogIntegracao logPai) {
        try {
            CboSaude cs = new CboSaude();
            cs.setIdCboSaudeTasy(((BigDecimal) c[0]).intValue());
            cs.setCodigoCbo((String) c[1]);
            cs.setDescricao((String) c[2]);
            cs.setAtivo(Boolean.TRUE);
            Salvar(cs, "sync", false);
            //em.merge(BCRUtils.gerarLogIntegracao(cs.getDescricao() + " cadastrado.", "VincularCboSaudeTasy", dtInicio, new Date(), logPai));
        } catch (Exception e) {
            System.out.println(e);
            //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar " + (String) c[1] + ". " + e.getMessage(), "VincularCboSaudeTasy", null, null, logPai));
        }
    }

    public List<CboSaude> autoCompleteMethodCBOSaude(String var) {
        Query query = getEm().createQuery("Select s From CboSaude s where s.ativo = TRUE AND (s.descricao LIKE :desc OR s.idCboSaude like :id OR s.codigoCbo like :id)");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();
    }

}
