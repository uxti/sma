/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Usuario;
import br.com.ux.util.AgendadorEJB;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

/**
 *
 * @author charl
 */
@Stateless
public class RotinasAutomaticasEJB {

    @EJB
    ChequeEJB chEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    ContaCorrenteEJB ccEJB;
    @EJB
    RepasseEJB rEJB;
    @EJB
    CaixaEJB cxEJB;
    @EJB
    AgendadorEJB aEJB;
    @EJB
    AssociacaoEJB assEJB;
    @EJB
    AuditoriaEJB auEJB;

    @Schedule(hour = "0", minute = "1", second = "0", info = "Rotinas Automatizadas do SMA", persistent = false)
    public void rotinasAutomatizadas() {
        try {
            System.out.println("Iniciando compensação de cheques automáticos ...");
            // Compensa automaticamente os cheque do hospital, realiza as baixas de repasses, caixa, conta corrente. (Proxima atualização, parametrizar esta funcionalidade)
            compensarChequesHospital();

            // Parametro ativo compensa todos os cheques na data de vencimento.
            if (assEJB.carregarAssociacao().getCompensarChequesAutomatico() != null) {
                if (assEJB.carregarAssociacao().getCompensarChequesAutomatico()) {
                    compensarChequesAuto();
                }
            }
            System.out.println("Finalizando compensação de cheques automáticos ...");

            System.out.println("Iniciando backup automatico ...");
            //Backup automatico
            aEJB.backupAutomatico();
            System.out.println("Finalizando backup automatico. Salvo em: " + aEJB.getDestino());
            System.out.println("Reiniciando servidor ...");
            aEJB.reiniciarServidor();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
    //@Schedule(hour = "5", minute = "30", second = "0", info = "Rotinas Automatizadas do SMA", persistent = false)
    public void reiniciarServidor() throws InterruptedException, IOException {
        System.out.println("Reiniciando servidor ...");
        aEJB.reiniciarServidor();
    }

    // Métodos a ser utilizados //    
    public void compensarChequesHospital() {
        Calendar cal = Calendar.getInstance();
        cal.set(2016, Calendar.JANUARY, 1);
        Date dtInicio = cal.getTime();

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date());
        cal2.add(Calendar.DAY_OF_MONTH, -6);
        Date dtFinal = cal2.getTime();

        for (Cheque c : chEJB.listarChequePorTipoPeriodo("H", dtInicio, dtFinal)) {
            //System.out.println("ID: " + c.getIdCheque());
            if ((c.getDtCompensacaoVencimento()).before(dtFinal) || c.getDtCompensacaoVencimento().equals(dtFinal) && c.getCompensado().equals("N") && (c.getSituacao().equals("A Compensar") || c.getSituacao().equals("Recebido"))) {
                receberRepassesHospitalares(retornaRepassesHospitalares(c.getRepasseList()));
                chEJB.compensarSync(c);
            }
        }
    }

    public void compensarChequesAuto() {
        Calendar cal = Calendar.getInstance();
        cal.set(2016, Calendar.JANUARY, 1);
        Date dtInicio = cal.getTime();

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date());
        cal2.add(Calendar.DAY_OF_MONTH, -assEJB.carregarAssociacao().getDiasCompensarChequeAuto());
        Date dtFinal = cal2.getTime();

        for (Cheque c : chEJB.listarChequePorTipoPeriodo("%", dtInicio, dtFinal)) {
            if ((c.getDtCompensacaoVencimento()).before(dtFinal) || c.getDtCompensacaoVencimento().equals(dtFinal) && c.getCompensado().equals("N") && (c.getSituacao().equals("A Compensar") || c.getSituacao().equals("Recebido"))) {
                chEJB.compensarSync(c);
            }
        }
    }

    public List<Repasse> retornaRepassesHospitalares(List<Repasse> listaRepasse) {
        List<Repasse> repasses = new ArrayList<>();
        for (Repasse repasse : listaRepasse) {
            if (repasse.getIdTerceiro().getTerceiroMedHosp()) {
                repasses.add(repasse);
            }
        }
        return repasses;
    }

    public void receberRepassesHospitalares(List<Repasse> lista) {
        Date d = new Date();
        try {
            Usuario u = uEJB.retornaUsuarioPorUsername("root");
            for (Repasse rep : lista) {
                // Atualiza a conta corrente
                ContaCorrente cc = rep.getContaCorrenteList().get(0);
                cc.setSituacao("Liberado");
                cc.setStatus("Retirado");
                cc.setIdUsuario(u);
                cc.setDtRetirada(d);
                cc.setDtLiberacao(d);
                cc.setDtPagamentoMedico(d);
                ccEJB.SalvarSimples(cc, null);
                atualizarDados(rep.getContaCorrenteList());

                // Atualiza o repasse
                rep.setSacado("S");
                rep.setSituacaoTerceiro('L');

                rep.setDtRecebimento(d);
                rEJB.SalvarSimples(rep);
                auEJB.Salvar(BCRUtils.criarAuditoria("pagamento", "Pagamento automatizado do repasse nº" + rep.getIdRepasse() + " no valor de R$ " + rep.getValor(), "root", new Date(), "Cheque"));
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar efetuar o pagamento!" + e);
            System.out.println(e);
        }
    }

    public void atualizarDados(List<ContaCorrente> ccs) {
        String data;
        DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        data = f.format(new Date());
        try {
            for (ContaCorrente corrente : ccs) {
                List<Caixa> listaCaixa = new ArrayList<Caixa>();
                listaCaixa = cxEJB.pegaListaCaixaPorRepasse(corrente.getIdRepasse());
                for (Caixa c : listaCaixa) {
                    c.setStatus("Baixado");
                    c.setObservacao("Repassado em: " + data + ". Usuário: " + corrente.getIdUsuario().getUsuario());
                    cxEJB.atualizarCaixa(c);
                }
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar efetuar o pagamento!" + e);
            System.out.println(e);
        }
    }

}
