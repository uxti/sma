/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Alerta;
import br.com.ux.model.Auditoria;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class AlertaEJB extends Conexao {

    public void Salvar(Alerta al, String usuario) {
         getEm().merge(al);
    }

    public void Excluir(Alerta al, String usuario) {
        Auditoria a = new Auditoria();
        a.setTabela("Alertas");
        a.setDescricao("Exclusão na tabela Alerta no ID: " + al.getIdAlerta());
        a.setDtHora(new Date());
        a.setUsuario(usuario);
         getEm().remove(al);
    }
    
    public void atualizarAlerta(Alerta al, String usuario){
        Auditoria a = new Auditoria();
        a.setTabela("Alertas");
        a.setDescricao("Atualização na tabela Alerta no ID: " + al.getIdAlerta());
        a.setDtHora(new Date());
        a.setUsuario(usuario);
         getEm().merge(al);
    }
    
    public List<Alerta> listaAlertasPorUsuario(String usuario){
        Query query =  getEm().createQuery("Select a FROM Alerta a where a.idUsuarioDestinatario.usuario = :usuario and a.visto = 'N' and a.status = 'A'");
        query.setParameter("usuario", usuario);
        return query.getResultList();
    }
    
    public List<Alerta> listaAlertasPorUsuarioAberto(String usuario){
        Query query =  getEm().createQuery("Select a FROM Alerta a where a.idUsuarioDestinatario.usuario = :usuario and a.status = 'A'");
        query.setParameter("usuario", usuario);
        return query.getResultList();
    }
    
    public List<Alerta> listaAlertasPorUsuarioRecebido(String usuario){
        Query query =  getEm().createQuery("Select a FROM Alerta a where a.idUsuarioDestinatario.usuario = :usuario and a.status = 'R'");
        query.setParameter("usuario", usuario);
        return query.getResultList();
    }
}
