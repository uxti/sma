/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Procedimento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author PokerFace
 */
@Stateless
public class ProcedimentoEJB extends Conexao {

    UsuarioSessao us = new UsuarioSessao();

    public void Salvar(Procedimento procedimento, String usuario) {
        if (procedimento.getIdProcedimento() != null) {
            if (procedimento.getInativo() == null) {
                procedimento.setInativo(Boolean.FALSE);
            }
            getEm().merge(procedimento);
            getEm().merge(BCRUtils.criarAuditoria("update", procedimento.getDescricao(), usuario, new Date(), "Procedimento"));
        } else {
            if (procedimento.getInativo() == null) {
                procedimento.setInativo(Boolean.FALSE);
            }
            getEm().merge(procedimento);
            getEm().merge(BCRUtils.criarAuditoria("insert", procedimento.getDescricao(), usuario, new Date(), "Procedimento"));
        }
    }

    public void atualizar(Procedimento p) {
        getEm().merge(p);
        getEm().merge(BCRUtils.criarAuditoria("update", p.getDescricao(), us.retornaUsuario(), new Date(), "Procedimento"));
    }

    public void Excluir(Procedimento procedimento, String usuario) {
        procedimento = getEm().getReference(Procedimento.class, procedimento.getIdProcedimento());
        getEm().remove(procedimento);
        getEm().merge(BCRUtils.criarAuditoria("delete", procedimento.getDescricao(), usuario, new Date(), "Procedimento"));
    }

    public List<Procedimento> listarProcedimentos() {
        return getEm().createQuery("Select p From Procedimento p WHERE p.inativo = FALSE and p.padrao = 'I'").getResultList();
    }

    public boolean verificarSeProcedimentoExiste(String desc) {
        Query query = getEm().createQuery("Select p From Procedimento p where p.descricao = :descricao");
        query.setParameter("descricao", desc);
        if (query.getResultList().size() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public Number contarProcedimentosRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idProcedimento) From Procedimento p ");
        return (Number) query.getSingleResult();
    }

    public List<Procedimento> listarProcedimentoLazyModeWhere(int primeiro, int qtd, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Procedimento p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }
    
    
    public Procedimento selecionarPorID(Integer id, String usuario) {
        try {
            Query query = getEm().createQuery("Select p From Procedimento p where p.idProcedimento = :id");
            query.setParameter("id", id);
            return (Procedimento) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    
    public Procedimento selecionarPorIDServico(Integer id, String usuario) {
        try {
            Query query = getEm().createQuery("Select p From Procedimento p where p.idServico = :id");
            query.setParameter("id", id);
            return (Procedimento) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public Procedimento selecionarPorPadrao(String s) {
        Query query = getEm().createQuery("Select p From Procedimento p where p.padrao = :padrao");
        query.setParameter("padrao", s);
        return (Procedimento) query.getSingleResult();
    }

    public List<Procedimento> listarProcedimentoLazyMode(int primeiro, int qtd) {
        Query query = getEm().createQuery("Select p From Procedimento p WHERE p.inativo = FALSE");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<Procedimento> listarProcedimentosLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query = getEm().createQuery("Select p From Procedimento p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarProcedimentoRegistro(String clausula) {
        Query query = getEm().createQuery("Select COUNT(p.idProcedimento) From Procedimento p " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<Procedimento> autoCompleteProcedimento(String var) {
        Query query = getEm().createQuery("Select p From Procedimento p where p.inativo = FALSE AND p.descricao LIKE :desc OR p.idProcedimento like :id");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();

    }

    // Auto complete do procedimento sem as Internações.
    public List<Procedimento> autoCompleteProcedimentoAvulso(String var) {
        Query query = getEm().createQuery("Select p From Procedimento p where p.inativo = FALSE AND p.padrao != 'I' AND (p.descricao LIKE :desc OR p.idProcedimento like :id)");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();

    }
}
