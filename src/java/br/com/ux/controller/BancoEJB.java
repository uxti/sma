/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Banco;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author PokerFace
 */
@Stateless
public class BancoEJB extends Conexao {

    public void Salvar(Banco banco, String usuario) {
        Auditoria a = new Auditoria();
        if (banco.getIdBanco() == null) {
             getEm().merge(banco);
             getEm().merge(BCRUtils.criarAuditoria("insert", banco.getNome(), usuario, new Date(), "Banco"));
        } else {
             getEm().merge(banco);
             getEm().merge(BCRUtils.criarAuditoria("update", banco.getNome(), usuario, new Date(), "Banco"));
        }
    }

    public void Excluir(Banco banco, String usuario) {
        banco = getEm().getReference(Banco.class, banco.getIdBanco());
        getEm().remove(banco);
        getEm().merge(BCRUtils.criarAuditoria("delete", banco.getNome(), usuario, new Date(), "Banco"));
    }

    public List<Banco> listaBanco() {
        return  getEm().createQuery("Select b From Banco b WHERE b.inativo = FALSE ").getResultList();
    }

    public List<Banco> listarLazy(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select p From Banco p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(p.idBanco) From Banco p " + clausula);
        return (Number) query.getSingleResult();
    }

    public Banco selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select b From Banco b where b.idBanco = :id");
        query.setParameter("id", id);
        return (Banco) query.getSingleResult();
    }
}
