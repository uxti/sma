/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.controller.integracao.BuscaDadosMySQLEJB;
import br.com.ux.controller.integracao.BuscaDadosTasyEJB;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.Procedimento;
import br.com.ux.model.RegraRepasse;
import br.com.ux.model.Servico;
import br.com.ux.model.Terceiro;
import br.com.ux.model.TipoInternacao;
import br.com.ux.model.tasy.ContaPaciente;
import br.com.ux.model.tasy.ProcedimentoPaciente;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class ContaPacienteEJB extends Conexao {

    @EJB
    BuscaDadosMySQLEJB bdmEJB;

    @EJB
    CirurgiaEJB cEJB;
    public Cirurgia cirurgia;
    private List<Cirurgia> listaFechamentos;
    public CirurgiaItem cirurgiaItem;
    private List<CirurgiaItem> listaCirurgiaItens;

    @EJB
    RegraRepasseEJB rrEJB;

    @EJB
    ConvenioEJB cvEJB;

    @EJB
    PlanoEJB plEJB;

    @EJB
    FormaPagamentoEJB fpEJB;

    @EJB
    ProcedimentoPacienteEJB ppEJB;

    @EJB
    BuscaDadosTasyEJB bdTEJB;

    public ContaPaciente contaPaciente;
    public RegraRepasse regraRepasse;

    private Procedimento procedimento;
    private TipoInternacao tipoInternacao;

    public void Salvar(ContaPaciente cp, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(cp);
        em.flush();
        em.refresh(cp);
        //em.merge(BCRUtils.criarAuditoria("insert", "ID Atendimento: " + cp.getIdAtendimentoPaciente().getIdAtendimentoPaciente(), usuario, new Date(), "Conta Paciente"));
    }

    public void Excluir(ContaPaciente cp) {
        cp = em.getReference(ContaPaciente.class, cp.getIdContaPaciente());
        em.remove(cp);
    }

    public void Atualizar(ContaPaciente cp, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(cp);
        //em.merge(BCRUtils.criarAuditoria("update", "ID Atendimento: " + cp.getIdAtendimentoPaciente().getIdAtendimentoPaciente(), usuario, new Date(), "Conta Paciente"));
    }

    public List<ContaPaciente> listarContaPaciente() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select c From ContaPaciente c ORDER BY c.idContaPaciente").getResultList();
    }

    public List<Cirurgia> listarFechamentosPorConta(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query q = em.createQuery("Select c From Cirurgia c where c.idContaPaciente.idContaPacienteTasy = :id");
        q.setParameter("id", id);
        return q.getResultList();
    }

    public boolean retornaSeFechamentoContaPacienteEstaPago(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query q = em.createQuery("Select c From Cirurgia c where c.idContaPaciente.idContaPacienteTasy = :id");
        q.setParameter("id", id);
        try {
            if (q.getResultList().size() > 0) {
                listaFechamentos = q.getResultList();
                for (Cirurgia c : listaFechamentos) {
                    if (c.isPago()) {
                        return true;
                    }
                }
                return false;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public List<ContaPaciente> listarContaPacientePorAtendimento(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query q = em.createQuery("Select c From ContaPaciente c where c.idAtendimentoPaciente.idAtendimentoPaciente = :id");
        q.setParameter("id", id);
        return q.getResultList();
    }

    public List<ContaPaciente> listarContaPacientePorNumeroAtendimentoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query q = em.createQuery("Select c From ContaPaciente c where c.idAtendimentoPaciente.numeroAtendimentoPacienteTasy = :id");
        q.setParameter("id", id);
        return q.getResultList();
    }

    public ContaPaciente selecionarPorNumeroContaTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT c FROM ContaPaciente c WHERE c.idContaPacienteTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (ContaPaciente) query.getSingleResult();
        } catch (Exception e) {
            //System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }

    public void verificarContaPacienteTasy(Object[] c, LogIntegracao logPai) {
        Date dtInicio = new Date();
        if (c[0] != null) {
            //System.out.println("Iniciando na CC: " + ((BigDecimal) c[0]).intValue());
            contaPaciente = selecionarPorNumeroContaTasy(((BigDecimal) c[0]).intValue());
            if (contaPaciente == null) {
                if (((BigDecimal) c[12]).intValue() == 0) {
                    //System.out.println("CC: " + ((BigDecimal) c[0]).intValue());
                    contaPaciente = cadastrarContaPaciente(c, dtInicio, logPai);
                } else {
                    //System.out.println("Conta paciente com valor zerado:" + ((BigDecimal) c[12]).intValue());
                }
            } else {
                if (contaPaciente.getCancelada() == null) {
                    contaPaciente.setCancelada(Boolean.FALSE);
                }
                if (!retornaSeFechamentoContaPacienteEstaPago(contaPaciente.getIdContaPacienteTasy())) {
                    try {
                        atualizarContaPaciente(contaPaciente, c);
                        //em.merge(BCRUtils.gerarLogIntegracao("Conta Paciente: " + ((BigDecimal) c[0]).intValue() + " editada.", "cadastrarContaPaciente", dtInicio, new Date(), logPai));
                    } catch (NullPointerException e) {
                        //System.out.println("nulo" + e.getMessage());
                        atualizarContaPaciente(contaPaciente, c);
                        //em.merge(BCRUtils.gerarLogIntegracao("Conta Paciente: " + ((BigDecimal) c[0]).intValue() + " editada.", "cadastrarContaPaciente", dtInicio, new Date(), logPai));
                    }
                } else {
                    // Implementar o metodo para estornar conta paciente antiga, estornar valores dos médicos se já estiver sido paga, e gerar novamente a conta. 
                    // em.merge(BCRUtils.gerarLogIntegracao("Conta Paciente: " + ((BigDecimal) c[0]).intValue() + " não atualizada, devido ao pagamento já realizado.", "cadastrarContaPaciente", dtInicio, new Date(), logPai));
                }
            }

            if (contaPaciente != null) {
                //System.out.println("Entrando na CC:" + contaPaciente.getIdContaPacienteTasy());
                if (!contaPaciente.getIdAtendimentoPaciente().getDescricaoTipoAtendimento().equals("Internado")) {
                    // 01112016 - Modificado para pegar todos os atendimentos pacientes exceto os internados, para gerar fechamento externo.
                    // Verifica se é Externo ou Pronto Socorro ou Atendimento Ambulatorial.
                    // Se não houver fechamentos, verifica se o status da Conta está em Definitivo.
                    //System.out.println("Verificando a CC:" + contaPaciente.getIdContaPacienteTasy());
                    if (!contaPaciente.getCancelada() && contaPaciente.getCirurgiaList().isEmpty()) {
                        //System.out.println("Gerando a CC:" + contaPaciente.getIdContaPacienteTasy());
                        gerarFechamentoExternoPorConta(contaPaciente);
                        
                    // CHARLES - 27032018
                    // Desabilitado para gerar as contas paciente mesmo com o valor zerado. Na rotina de verificação dos procedimentos, verifica se existem itens e exclui o registro
                    // da conta, caso necessário.                    
                    /*
                        if (contaPaciente.getValorProcedimento() > 0) {
                        Se todos as verificações estiverem corretas, gerar fechamento a partir dos dados da Conta Paciente. (campo externo = TRUE)
                        gerarFechamentoExternoPorConta(contaPaciente);
                        em.merge(BCRUtils.gerarLogIntegracao("Fechamento gerado para conta Paciente: " + contaPaciente.getIdContaPacienteTasy() + ".", "verificarContaPacienteTasy", dtInicio, new Date(), logPai));
                        }
                     */                    
                    }
                }
//                else if (!retornaSeFechamentoContaPacienteEstaPago(contaPaciente.getIdContaPacienteTasy())) {
//                    if (contaPaciente.getIdAtendimentoPaciente().getDescricaoTipoAtendimento().equals("Externo") || contaPaciente.getIdAtendimentoPaciente().getDescricaoTipoAtendimento().equals("Pronto Socorro")) {
//                        for (Cirurgia ci : contaPaciente.getCirurgiaList()) {
//                            cEJB.Excluir(ci);
//                        }
//                        // Se todos as verificações estiverem corretas, gerar fechamento a partir dos dados da Conta Paciente. (campo externo = TRUE)
//                        gerarFechamentoExternoPorConta(contaPaciente);
//                        em.merge(BCRUtils.gerarLogIntegracao("Fechamento gerado para conta Paciente: " + contaPaciente.getIdContaPacienteTasy() + ".", "verificarContaPacienteTasy", dtInicio, new Date(), logPai));
//                    }
//                }

            }
        }
    }

    public void vincularListaContaPacienteTasy(List<Object[]> resultList, LogIntegracao logPai) {
        // int i = 0;
        for (Object[] c : resultList) {
            //System.out.println("Conta: " + ((BigDecimal) c[0]).intValue());
            verificarContaPacienteTasy(c, logPai);
        }
    }

    public ContaPaciente cadastrarContaPaciente(Object[] c, Date dtInicio, LogIntegracao logPai) {
        try {
            ContaPaciente cp = new ContaPaciente();
            criarContaPaciente(cp, c);
            em.merge(BCRUtils.gerarLogIntegracao("Conta Paciente: " + ((BigDecimal) c[0]).intValue() + " cadastrada.", "cadastrarContaPaciente", dtInicio, new Date(), logPai));
            return cp;
        } catch (Exception e) {
            System.out.println(e);
            //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar conta " + ((BigDecimal) c[0]).intValue() + ". " + e.getMessage(), "cadastrarContaPaciente", null, null, logPai));
            return null;
        }
    }

    public void criarContaPaciente(ContaPaciente cp, Object[] c) {
        try {
            Salvar(retornaContaPaciente(cp, c), "sync");
        } catch (Exception e) {
            System.out.println("Erro ao salvar conta Paciente " + ((BigDecimal) c[0]).intValue());
            System.out.println(e);
        }
    }

    public void atualizarContaPaciente(ContaPaciente cp, Object[] c) {
        try {
            Atualizar(retornaContaPaciente(cp, c), "sync");
        } catch (Exception e) {
            System.out.println("Erro ao atualizar conta Paciente " + ((BigDecimal) c[0]).intValue());
            System.out.println(e);
        }
    }

    public ContaPaciente retornaContaPaciente(ContaPaciente cp, Object[] c) {
        try {
            cp.setIdContaPacienteTasy(((BigDecimal) c[0]).intValue());
            cp.setIdAtendimentoPaciente(bdmEJB.selecionarPorNumeroAtendimentoTasy(((BigDecimal) c[1]).intValue()));
            cp.setNumeroAtendimentoPacienteTasy(((BigDecimal) c[1]).intValue());
            cp.setDtPeriodoInical((Date) c[3]);
            cp.setDtAcerto((Date) c[4]);
            cp.setDtAtualizacao((Date) c[5]);
            cp.setStatus((String) c[6]);
            cp.setValorTotal(((BigDecimal) c[7]).doubleValue());
            cp.setValorProcedimento(((BigDecimal) c[8]).doubleValue());
            cp.setValorMaterial(((BigDecimal) c[9]).doubleValue());
            cp.setValorMedico(((BigDecimal) c[10]).doubleValue());
            cp.setValorDiaria(((BigDecimal) c[11]).doubleValue());
            cp.setCancelada(((BigDecimal) c[12]).intValue() == 0 ? Boolean.FALSE : Boolean.TRUE);
            return cp;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public List<CirurgiaItem> gerarListaDeServicosPorContaPaciente(ContaPaciente cp) {
        List<CirurgiaItem> itens = new ArrayList<>();
        //System.out.println("Lista de proc.: " + cp.getProcedimentoPacienteList().size());
        for (ProcedimentoPaciente pp : cp.getProcedimentoPacienteList()) {
            //System.out.println("Valor: " + pp.getValorProcedimento());
            if (pp.getIdMedico() != null && pp.getCdMotivoExcConta() == null && pp.getValorProcedimento() != null) {
                itens.add(retornaFechamentoItemPorProcedimento(pp));
            }
        }
        return itens;
    }

    public CirurgiaItem retornaFechamentoItemPorProcedimento(ProcedimentoPaciente pp) {
        Double valorAjustado = 0D;
        if (pp.getIdMedico() != null) {
            valorAjustado = retornaValorAjustadoPorRegraRepasse(pp);
        }
        CirurgiaItem ci = new CirurgiaItem();
        ci.setIdServico(pp.getIdServico());
        if (pp.getIdMedico() != null) {
            ci.setIdTerceiro(pp.getIdMedico());
        }
        ci.setOrigem("TASY");

        // Usar este valor para recalcular regra de repasse
        ci.setValorTasy(pp.getValorProcedimento());
        ci.setQuantidade(pp.getQtdProcedimento());
        ci.setIdProcedimentoPaciente(pp);
        ci.setValorUnitario(valorAjustado != 0D ? valorAjustado : 0D);
        ci.setValorTotal(valorAjustado != 0D ? valorAjustado : 0D);
        ci.setValorTotalGeral(valorAjustado + pp.getValorProcedimento());
        ci.setIdProcedimentoPaciente(pp);
        return ci;
    }

    public Double retornaValorAjustadoPorRegraRepasse(ProcedimentoPaciente pp) {
        try {
            Double valorAjustado = 0D;
            if (pp != null) {
                if (pp.getIdContaPaciente() != null) {
                    regraRepasse = rrEJB.retornaRegraRepasse(pp.getIdServico(), pp.getIdMedico(), pp.getIdContaPaciente().getIdAtendimentoPaciente().getCategoria(), pp.getIdContaPaciente().getIdAtendimentoPaciente().getUrgencia() != null ? pp.getIdContaPaciente().getIdAtendimentoPaciente().getUrgencia() : null);
                } else {
                    regraRepasse = null;
                }
            } else {
                regraRepasse = null;
            }

            if (regraRepasse != null) {
                // Se a regra retorna não houver taxa por descuido ou retorno vazio da função, ajustar a 100% para pegar valor integral.
                pp.setIdRegraRepasse(regraRepasse);
                Double taxa = regraRepasse.getFatorProcedimento() == 100 ? 0D : regraRepasse.getFatorProcedimento();
                valorAjustado = (pp.getValorProcedimento() * taxa) / (100 - taxa);
//            if (regraRepasse.getRepasseSecundario()) {
//                Double valorRateioSecundario = ((valorAjustado * regraRepasse.getFatorTerceiroSecundario()) / 100);
//                pp.setValorRateioSecundario(valorRateioSecundario);
//            }
                //System.out.println(regraRepasse.getDescricao() + " - Resultado: " + valorAjustado);
            } else if (pp != null) {
                pp.setIdRegraRepasse(null);
                pp.setValorRateioSecundario(null);
            }
            return BCRUtils.arredondar(valorAjustado);
        } catch (Exception e) {
            System.out.println(e);
        }
        return BCRUtils.arredondar(0D);
    }

    public void excluirRepasseEContasCorrentePorFechamento(Integer idFechamento) {
        //Excluir as contas correntes vinculadas ao repasse
        Query query2 = em.createQuery("Delete from ContaCorrente cc where cc.idCirurgia.idCirurgia = :id");
        query2.setParameter("id", idFechamento);
        query2.executeUpdate();

        Query query = em.createQuery("Delete from Repasse r where r.idCirurgia.idCirurgia = :id ");
        query.setParameter("id", idFechamento);
        query.executeUpdate();
    }

    public Double retornaValorAjustadoPorRegraRepasseTaxa(Servico s, Terceiro t, Double valorTotalGeral, String categoriaConvenio, Boolean urgencia) {
        Double valorAjustado = 0D;

        regraRepasse = rrEJB.retornaRegraRepasse(s, t, categoriaConvenio, urgencia);
        if (regraRepasse != null) {
            // Este metodo calcula o valor do SMA, para atualizar no
            Double taxa = regraRepasse.getFatorProcedimento() == 0D ? 100 : regraRepasse.getFatorProcedimento();
            valorAjustado = (valorTotalGeral * taxa) / 100;
        } else {
            //System.out.println("Não encontrou nada para " + pp.getIdServico().getIdServico());
        }
        return BCRUtils.arredondar(valorAjustado);
    }

    public CirurgiaItem recalcularValoresItem(CirurgiaItem ci, Double valorTotalGeral) {

        valorTotalGeral = valorTotalGeral - ci.getDesconto();
//        System.out.println("fechamento: " + ci.getIdCirurgia().getIdCirurgia());
//        System.out.println("conta: " + ci.getIdCirurgia().getIdContaPaciente().getIdContaPaciente());
//        System.out.println("atendimento: " + ci.getIdCirurgia().getIdContaPaciente().getIdAtendimentoPaciente().getIdAtendimentoPaciente());
//        System.out.println("urgência: " + ci.getIdCirurgia().getIdContaPaciente().getIdAtendimentoPaciente().getUrgencia());
//        
        Double valorAjustado = retornaValorAjustadoPorRegraRepasseTaxa(ci.getIdServico(), ci.getIdTerceiro(), valorTotalGeral, ci.getIdCirurgia().getIdContaPaciente().getIdAtendimentoPaciente().getCategoria(), ci.getIdCirurgia().getIdContaPaciente().getIdAtendimentoPaciente().getUrgencia() != null ? ci.getIdCirurgia().getIdContaPaciente().getIdAtendimentoPaciente().getUrgencia() : null);

        valorAjustado = BCRUtils.arredondar(valorAjustado);

        ci.setValorTotal(valorAjustado);                      // Valor SMA = novo total geral * taxa de repasse
        ci.setValorTotalGeral(valorTotalGeral);               // Valor Total Geral = novo total geral já com desconto
        ci.setValorUnitario(valorTotalGeral);                 // Valor Total Geral = Valor Unitário
        ci.setValorTasy(valorTotalGeral - valorAjustado);     // Valor TASY  = novo total geral - Valor SMA    
        return ci;
    }

    public void zerarCampos() {
        cirurgia = new Cirurgia();
        cirurgiaItem = new CirurgiaItem();
        listaCirurgiaItens = new ArrayList<>();
        procedimento = new Procedimento();
        tipoInternacao = new TipoInternacao();
        cirurgia.setValor(0D);
        cirurgiaItem.setQuantidade(1D);
        cirurgiaItem.setValorUnitario(0D);
        cirurgiaItem.setValorTotal(0D);
        cirurgia.setDtCirurgia(new Date());
    }

    private void gerarFechamentoExternoPorConta(ContaPaciente cp) {
        try {
            zerarCampos();
            cirurgia.setStatus('L');
            cirurgia.setPago(false);
            // Identifador de fechamentos externos
            cirurgia.setExterno(Boolean.TRUE);
            cirurgia.setIdContaPaciente(cp);
            cirurgia.setIdPaciente(cp.getIdAtendimentoPaciente().getIdPaciente());
            cirurgia.setIdConvenio(cvEJB.selecionarConvenioPadrao());
            cirurgia.setIdPlano(plEJB.selecionarPlanoPorNome(cp.getIdAtendimentoPaciente().getCategoria()));
            cirurgia.setIdForma(fpEJB.selecionarPadrao());
            cirurgia.setIdTerceiro(cp.getCdMedicoExecutor());
            cirurgia.setObservacao(cp.getIdAtendimentoPaciente().getObservacao());
            cirurgia.setValor(retornaTotalRealContaPaciente(cp.getProcedimentoPacienteList()));
            cirurgia.setDtInternacao(cp.getDtPeriodoInical());
            // Seta o tipo da Internação com Geral, o mesmo tipo dos lançamentos avulso, porém o que diferencia é o campo externo, neste caso.
            cirurgia.setIdTipoInternacao(cEJB.retornaTipoInternacao("Geral"));

            //listaCirurgiaItens = gerarListaDeServicosPorContaPaciente(cp);
            cEJB.SalvarExterno(cirurgia, listaCirurgiaItens);
            //System.out.println("Fechamento externo gerado para conta " + cp.getIdContaPacienteTasy());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            // System.out.println("Erro ao gerar fechamento externo para conta " + cp.getIdContaPacienteTasy());

        }

    }

    public Double retornaTotalTasyContaPaciente(List<ProcedimentoPaciente> listaProcedimentos) {
        Double total = 0D;
        for (ProcedimentoPaciente pp : listaProcedimentos) {
            total += pp.getValorProcedimento();
        }
        return total;
    }

    public Double retornaTotalRealContaPaciente(List<ProcedimentoPaciente> listaProcedimentos) {
        Double total = 0D;
        for (ProcedimentoPaciente pp : listaProcedimentos) {
            total += retornaValorAjustadoPorRegraRepasse(pp);
        }
        return total;
    }

    public boolean verificaSeContaPacienteFoiExcluida(Integer idContaPaciente) {
        try {
            return bdTEJB.listaContaPacientesTasyPorID(idContaPaciente).isEmpty();
        } catch (Exception e) {
            return false;
        }
    }

    public Cirurgia getCirurgia() {
        return cirurgia;
    }

    public void setCirurgia(Cirurgia cirurgia) {
        this.cirurgia = cirurgia;
    }

    public CirurgiaItem getCirurgiaItem() {
        return cirurgiaItem;
    }

    public void setCirurgiaItem(CirurgiaItem cirurgiaItem) {
        this.cirurgiaItem = cirurgiaItem;
    }

    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public TipoInternacao getTipoInternacao() {
        return tipoInternacao;
    }

    public void setTipoInternacao(TipoInternacao tipoInternacao) {
        this.tipoInternacao = tipoInternacao;
    }

    public ContaPaciente getContaPaciente() {
        return contaPaciente;
    }

    public void setContaPaciente(ContaPaciente contaPaciente) {
        this.contaPaciente = contaPaciente;
    }

    public RegraRepasse getRegraRepasse() {
        return regraRepasse;
    }

    public void setRegraRepasse(RegraRepasse regraRepasse) {
        this.regraRepasse = regraRepasse;
    }

    public List<Cirurgia> getListaFechamentos() {
        return listaFechamentos;
    }

    public void setListaFechamentos(List<Cirurgia> listaFechamentos) {
        this.listaFechamentos = listaFechamentos;
    }

    public List<CirurgiaItem> getListaCirurgiaItens() {
        return listaCirurgiaItens;
    }

    public void setListaCirurgiaItens(List<CirurgiaItem> listaCirurgiaItens) {
        this.listaCirurgiaItens = listaCirurgiaItens;
    }

}
