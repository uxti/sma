/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Cheque;
import br.com.ux.model.Repasse;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.LockModeType;
import javax.persistence.Query;

/**
 *
 * @author PokerFace
 */
@Stateless
public class ChequeEJB extends Conexao {

    private UsuarioSessao us;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    ContaCorrenteEJB ccEJB;

    
    public void Salvar(Cheque cheque, String usuario) {
        if (cheque.getIdCheque() == null) {
            em.persist(cheque);
            em.flush();
            em.refresh(cheque);
            em.merge(BCRUtils.criarAuditoria("insert", "Tipo: " + cheque.getMovimento() + ". Número Cheque: " + cheque.getNumeroCheque() + ". Compensado: " + cheque.getCompensado() + ". Cancelado: " + cheque.getCancelado(), usuario, new Date(), "Cheque"));
        } else {
            em.merge(cheque);
            em.merge(BCRUtils.criarAuditoria("update", "Tipo: " + cheque.getMovimento() + ". Número Cheque: " + cheque.getNumeroCheque() + ". Compensado: " + cheque.getCompensado() + ". Cancelado: " + cheque.getCancelado(), usuario, new Date(), "Cheque"));
        }
    }
    
    public void SalvarSimples(Cheque cheque, String usuario) {
        em.merge(cheque);
        //em.merge(BCRUtils.criarAuditoria("insert", "Tipo: " + cheque.getMovimento() + ". Número Cheque: " + cheque.getNumeroCheque() + ". Compensado: " + cheque.getCompensado() + ". Cancelado: " + cheque.getCancelado(), usuario, new Date(), "Cheque"));
    }

    public void Atualizar(Cheque c, String usuario) {
        em.merge(c);
        em.merge(BCRUtils.criarAuditoria("movimentarCheque", "Tipo: " + c.getMovimento() + "/" + c.getSituacao() + ". Número Cheque: " + c.getNumeroCheque() + ". Compensado: " + c.getCompensado() + ". Cancelado: " + c.getCancelado() + ". Observação." + c.getObservacao(), usuario, new Date(), "Cheque"));

    }

    public void Excluir(Cheque cheque, String usuario) {
        cheque = em.getReference(Cheque.class, cheque.getIdCheque());
        em.remove(cheque);
        em.merge(BCRUtils.criarAuditoria("delete", "Tipo: " + cheque.getMovimento() + ". Número Cheque: " + cheque.getNumeroCheque() + ". Compensado: " + cheque.getCompensado() + ". Cancelado: " + cheque.getCancelado(), usuario, new Date(), "Cheque"));
    }

    public void excluirChequesPorFechamento(Integer idFechamento, String usuario) {
        for (Cheque cheque : listarChequePorFechamento(idFechamento)) {
            em.remove(cheque);
            em.merge(BCRUtils.criarAuditoria("delete", "Tipo: " + cheque.getMovimento() + ". Número Cheque: " + cheque.getNumeroCheque() + ". Compensado: " + cheque.getCompensado() + ". Cancelado: " + cheque.getCancelado(), usuario, new Date(), "Cheque"));
        }
    }

    public void movimentarCheque(Cheque c, String usuario) {
        em.merge(BCRUtils.criarAuditoria("movimentarCheque", "Tipo: " + c.getMovimento() + "/" + c.getSituacao() + ". Número Cheque: " + c.getNumeroCheque() + ". Compensado: " + c.getCompensado() + ". Cancelado: " + c.getCancelado() + ". Observação." + c.getObservacao(), usuario, new Date(), "Cheque"));
    }

    public List<Cheque> listarCheque() {
        Query query = em.createQuery("Select c From Cheque c WHERE c.movimento = :id");
        query.setParameter("id", "C");
        return query.getResultList();
    }

    public List<Cheque> listarChequePorTipoPeriodo(String tipo, Date dtInicio, Date dtFinal) {
        Query query = em.createQuery("Select c From Cheque c WHERE c.movimento = :id and c.dtCompensacaoVencimento between :dtIni and :dtFim and c.tipo like :tip");
        query.setParameter("id", "C");
        query.setParameter("tip", "%" + tipo + "%");
        query.setParameter("dtIni", dtInicio);
        query.setParameter("dtFim", dtFinal);
        return query.getResultList();
    }

    public List<Cheque> listarChequePorMovimentoPeriodo(String movimento, String numeroCheque, Date dtInicio, Date dtFinal) {
        System.out.println(movimento);
        System.out.println(dtInicio);
        System.out.println(dtFinal);
        Query query = em.createQuery("Select c From Cheque c WHERE c.movimento = :id and c.dtCompensacaoVencimento between :dtIni and :dtFim ");
        query.setParameter("id", movimento);
        query.setParameter("dtIni", dtInicio);
        query.setParameter("dtFim", dtFinal);
        return query.getResultList();
    }

    public List<Cheque> listarChequePorNumeroCheque(String id) {
        Query query = em.createQuery("Select c From Cheque c WHERE c.numeroCheque like :id");
        query.setParameter("id", "%" + id + "%");
        return query.getResultList();
    }

    public List<Cheque> listarNotasPromissorias() {
        Query query = em.createQuery("Select c From Cheque c WHERE c.movimento = :id");
        query.setParameter("id", "N");
        return query.getResultList();
    }

    public List<Cheque> listarPagamento() {
        Query query = em.createQuery("Select c From Cheque c");
        return query.getResultList();
    }

    public List<Cheque> listarChequePorFechamento(Integer idFechamento) {
        Query query = em.createQuery("Select c From Cheque c WHERE c.idCirurgia.idCirurgia = :id");
        query.setParameter("id", idFechamento);
        return query.getResultList();
    }

    public List<Cheque> listarChequeCancelados() {
        Query query = em.createQuery("Select c From Cheque c WHERE c.movimento = 'C' AND c.cancelado = 'S'");
        return query.getResultList();
    }

    public Cheque selecionarPorID(Integer id, String usuario) {
        Query query = em.createQuery("Select c From Cheque c where c.idCheque = :id");
        query.setParameter("id", id);
        return (Cheque) query.getSingleResult();
    }

    public Usuario selecionarIDUsuario(String usuario) {
        Query query = em.createQuery("Select u From Usuario u where u.usuario = :usuario");
        query.setParameter("usuario", usuario);
        return (Usuario) query.getSingleResult();
    }

    public Number verificarStatus(Cheque c, String status, String status2) {
        Query query = em.createQuery("Select count(c) From ContaCorrente c where c.idCaixa.idCheque = :id and (c.status = :stat or c.situacao = :stat2)");
        query.setParameter("id", c);
        query.setParameter("stat", status);
        query.setParameter("stat2", status2);
        return (Number) query.getSingleResult();
    }

    public String verificarCompensado(Integer id) {
        Query query = em.createQuery("Select c.compensado From Cheque c where c.idCheque = :id");
        query.setParameter("id", id);
        return (String) query.getSingleResult();
    }

    public void compensar(Cheque cheque, String usuario) {
        cheque.setCompensado("S");
        cheque.setSituacao("Recebido");
        cheque.setDtCompensado(new Date());
        em.merge(cheque);
        em.merge(BCRUtils.criarAuditoria("compensar", "Tipo: " + cheque.getMovimento() + ". Número Cheque: " + cheque.getNumeroCheque() + ". Compensado: " + cheque.getCompensado() + ". Cancelado: " + cheque.getCancelado(), usuario, new Date(), "Cheque"));
    }

    public void compensarSync(Cheque cheque) {
        cheque.setCompensado("S");
        cheque.setDtCompensado(new Date());
        cheque.setSituacao("Recebido");
        em.merge(BCRUtils.criarAuditoria("compensar auto", "Tipo: " + cheque.getMovimento() + ". Número Cheque: " + cheque.getNumeroCheque() + ". Compensado: " + cheque.getCompensado() + ". Cancelado: " + cheque.getCancelado(), "root", new Date(), "Cheque"));
        em.merge(cheque);
    }

    public void trocarCheque(Cheque chqTrocar, Integer id) {
        Query query1 = em.createQuery("Update ContaCorrente c set c.idCheque = :chqTrocar, c.dtLiberacao = :data where c.idCheque = :id");
        query1.setParameter("chqTrocar", chqTrocar.getIdCheque());
        query1.setParameter("data", chqTrocar.getDtCompensacaoVencimento());
        query1.setParameter("id", id);
        query1.executeUpdate();

        Query query2 = em.createQuery("Update Repasse r set r.idCheque = :chqTrocar, r.dtVencimento = :data where r.idCheque.idCheque = :id");
        query2.setParameter("chqTrocar", chqTrocar);
        query2.setParameter("data", chqTrocar.getDtCompensacaoVencimento());
        query2.setParameter("id", id);
        query2.executeUpdate();

        Query query3 = em.createQuery("Update Caixa c set c.idCheque = :chqTrocar where c.idCheque.idCheque = :id");
        query3.setParameter("chqTrocar", chqTrocar);
        query3.setParameter("id", id);
        query3.executeUpdate();
    }

    public Number retornaPagamentosPorFechamento(Integer idCirurgia) {
        Query query = em.createQuery("Select MAX(c.numeroParcela) From Cheque c where c.idCirurgia.idCirurgia = :id");
        query.setParameter("id", idCirurgia);
        return (Number) query.getSingleResult();
    }

    public void deletarChequeDoCaixa(Cheque cheque) {
        Query query = em.createQuery("DELETE FROM Caixa c where c.idCheque.idCheque = :idc");
        query.setParameter("idc", cheque.getIdCheque());
        query.executeUpdate();
    }
}
