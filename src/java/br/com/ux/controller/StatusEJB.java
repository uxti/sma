/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.Status;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class StatusEJB extends Conexao {

    public void Salvar(Status status, String usuario) {
        Auditoria auditoria = new Auditoria();
        if (status.getIdStatus() != null) {
             getEm().merge(status);
            auditoria.setTabela("Status");
            auditoria.setDescricao("Alteração na tabela Status no registro " + status.getDescricao());
            auditoria.setDtHora(new Date());
            auditoria.setUsuario(usuario);
             getEm().merge(auditoria);
        } else {
             getEm().merge(status);
            auditoria.setTabela("Status");
            auditoria.setDescricao("Inserção na tabela Status do registro " + status.getDescricao());
            auditoria.setDtHora(new Date());
            auditoria.setUsuario(usuario);
             getEm().merge(auditoria);
        }
    }

    public void Excluir(Status status, String usuario) {
        status =  getEm().getReference(Status.class, status.getIdStatus());
         getEm().remove(status);
        Auditoria a = new Auditoria();
        a.setTabela("Motivo");
        a.setDescricao("Exclusão na tabela Status" + status.getDescricao() + " código " + status.getIdStatus());
        a.setDtHora(new Date());
        a.setUsuario(usuario);
         getEm().merge(a);
    }

    public List<Status> listarStatus() {
        return  getEm().createQuery("Select m From Status m").getResultList();
    }

    public Status selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select s From Status s where s.idStatus = :id");
        query.setParameter("id", id);
        return (Status) query.getSingleResult();
    }
    
    public Status selecionarPorTipo(Integer tipo) {
        Query query =  getEm().createQuery("Select s From Status s where s.fixo = :tipo");
        query.setParameter("tipo", tipo);
        return (Status) query.getSingleResult();
    }
    
    
     public List<Status> listarLazy(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select s From Status s " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(p.idStatus) From Status p " + clausula);
        return (Number) query.getSingleResult();
    }
    
    

}
