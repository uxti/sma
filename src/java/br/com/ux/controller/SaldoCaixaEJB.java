/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.MovimentoCaixa;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author charl
 */
@Stateless
public class SaldoCaixaEJB extends Conexao {

    UsuarioSessao us = new UsuarioSessao();
    @EJB
    UsuarioEJB uEJB;

    public void Salvar(MovimentoCaixa movimentoCaixa) {
        em.merge(movimentoCaixa);
    }

    public void SalvarPersist(MovimentoCaixa movimentoCaixa) {
        em.persist(movimentoCaixa);
    }

    public MovimentoCaixa saldoInicial(Integer usuario) {
        try {
            Query query = em.createQuery("Select mc From MovimentoCaixa mc where mc.idUsuario.idUsuario = :usu and mc.descricao = 'Saldo Inicial'");
            query.setParameter("usu", usuario);
            query.setMaxResults(1);
            return (MovimentoCaixa) query.getSingleResult();
        } catch (Exception e) {
            MovimentoCaixa mc = new MovimentoCaixa();
            mc.setValor(0D);
            mc.setDtMovimentacao(new Date());
            return mc;
        }
    }

    public List<MovimentoCaixa> listarMovimentoCaixaPorUsuario(Integer usu, Date dtIni, Date dtFim, Boolean saldoInicial) {
        Query query;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String nDtIni = df.format(dtIni);
        String nDtFim = df.format(dtFim);

        if (saldoInicial) {
            query = em.createQuery("Select mc From MovimentoCaixa mc where mc.idUsuario.idUsuario = :usu and mc.dtMovimentacao BETWEEN '" + nDtIni + "' and '" + nDtFim + "' ORDER BY mc.tipoMovimentacao, mc.dtMovimentacao, mc.descricao");
        } else {
            query = em.createQuery("Select mc From MovimentoCaixa mc where mc.idUsuario.idUsuario = :usu and mc.dtMovimentacao BETWEEN '" + nDtIni + "' and '" + nDtFim + "' and mc.descricao != 'Saldo Inicial' ORDER BY mc.tipoMovimentacao, mc.dtMovimentacao, mc.descricao");
        }
        System.out.println("filtrando de: " + nDtIni + " a " + nDtFim);

        query.setParameter("usu", usu);
        return query.getResultList();
    }

}
