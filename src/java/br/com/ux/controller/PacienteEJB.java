/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.controller.integracao.BuscaDadosMySQLEJB;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.Paciente;
import br.com.ux.model.PacienteResponsavel;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class PacienteEJB extends Conexao {

    @EJB
    BuscaDadosMySQLEJB bdmEJB;

    public void Salvar(Paciente paciente, String usuario, List<PacienteResponsavel> responsaveis) {
        em.getEntityManagerFactory().getCache().evictAll();
        if (paciente.getIdPaciente() != null) {
            em.merge(paciente);
            em.merge(BCRUtils.criarAuditoria("update", "Nome: " + paciente.getNome(), usuario, new Date(), "Paciente"));
        } else {
            em.persist(paciente);
            em.flush();
            em.refresh(paciente);
            em.merge(BCRUtils.criarAuditoria("insert", "Nome: " + paciente.getNome(), usuario, new Date(), "Paciente"));

        }
    }

    public void Atualizar(Paciente paciente, String usuario, Boolean auditoria) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(paciente);
        if (auditoria) {
            em.merge(BCRUtils.criarAuditoria("update", "Nome: " + paciente.getNome(), usuario, new Date(), "Paciente"));
        }        
    }
    
    public void AtualizarSimples(Paciente paciente) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(paciente);
    }

    public void Excluir(Paciente paciente, String usuario) {
        paciente = em.getReference(Paciente.class, paciente.getIdPaciente());
        em.remove(paciente);
        em.merge(BCRUtils.criarAuditoria("delete", "Nome: " + paciente.getNome(), usuario, new Date(), "Paciente"));
    }

    public void ExcluirResponsavel(PacienteResponsavel pr, String usuario) {
        pr = em.getReference(PacienteResponsavel.class, pr.getIdPacienteResposanvel());
        em.getEntityManagerFactory().getCache().evictAll();
        em.remove(pr);
        em.merge(BCRUtils.criarAuditoria("delete", "Nome: " + pr.getIdPacienteResponsavel().getNome(), usuario, new Date(), "Paciente Responsável"));

    }

    public List<Paciente> listarPacientes() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select p From Paciente p where p.inativo = FALSE ORDER BY p.nome").getResultList();
    }

    public List<Paciente> listarPacientesByName(String nome) {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select p From Paciente p where where p.inativo = FALSE and p.nome like '%" + nome + "%'").getResultList();
    }

    public List<Paciente> listarPacientesLazyMode(int primeiro, int qtd) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Paciente p where p.inativo = FALSE");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<Paciente> listarPacientesLazyModeWhere(int primeiro, int qtd, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Paciente p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarPacientesRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idPaciente) From Paciente p ");
        return (Number) query.getSingleResult();
    }

    public Paciente selecionarPorID(Integer id, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Paciente p where p.idPaciente = :id");
        query.setParameter("id", id);
        return (Paciente) query.getSingleResult();
    }

    public Number consultarCPF(String cgccpf) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select count(t) From Terceiro t where t.cgccpf = :cgccpf");
        query.setParameter("cgccpf", cgccpf);
        return (Number) query.getSingleResult();
    }

    public List<Paciente> pesquisaPacientePorNome(String nome) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT p FROM Paciente p WHERE p.nome LIKE '%" + nome + "%'");
        return query.getResultList();
    }

    public Paciente pesquisaPacientePorNomeSingle(String nome) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT p FROM Paciente p WHERE p.nome LIKE '%" + nome + "%'");
        return (Paciente) query.getSingleResult();
    }

    public boolean pesquisaSePacienteExistePorNome(String nome) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT p FROM Paciente p WHERE p.nome = '" + nome + "'");
        System.out.println(query.getResultList());
        if (query.getResultList().size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public Long verificaCPF(String cpf) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select count(p.idPaciente) from Paciente p where p.cpf = :cpf");
        query.setParameter("cpf", cpf);
        return (Long) query.getSingleResult();
    }

    public String verificaCPFRetornaNome(String cpf) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p.nome from Paciente p where p.cpf = :cpf");
        query.setParameter("cpf", cpf);
        String nome = null;
        try {
            nome = (String) query.getSingleResult();
        } catch (NoResultException e) {
            nome = null;
        }

        if (nome == null) {
            nome = "Inexistente";
        }
        return nome;
    }

    public Paciente verificaCPFPaciente(String cpf) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p from Paciente p where p.cpf = :cpf");
        query.setParameter("cpf", cpf);
        return (Paciente) query.getSingleResult();
    }

    public Long contarPacienteCPFiD(String cpf, Integer id) {
        Query query = em.createQuery("SELECT COUNT(p.idPaciente) FROM Paciente p WHERE p.idPaciente = " + id + " and p.cpf = '" + cpf + "' ");
        return (Long) query.getSingleResult();
    }

    public List<Paciente> autoCompletePaciente(String var) {
        Query query = em.createQuery("Select p From Paciente p where p.inativo = FALSE AND (p.nome LIKE :nome OR p.cpf LIKE :cpf OR p.idPaciente like :id )");
        query.setParameter("nome", var + "%");
        query.setParameter("cpf", var + "%");
        query.setParameter("id", var + "%");
//        query.setParameter("mae", var + "%");
        return query.getResultList();

    }

    public Paciente verificaPacienteCadastradoNomeCPF(String nome, String CPF) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT p FROM Paciente p WHERE p.nome = :nome and p.cpf = :cpf");
        query.setParameter("nome", nome);
        query.setParameter("cpf", CPF);
        query.setMaxResults(1);
        try {
            return (Paciente) query.getSingleResult();
        } catch (Exception e) {
            //System.out.println("Erro: " + e.getMessage() + "em: " + nome);
            return null;
        }
    }

    public Paciente verificaPacienteCadastradoNomeNomeMaeDtNascimento(String nome, String nomeMae, Date dtNascimento) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT p FROM Paciente p WHERE p.nome = :nome and p.nomeMae = :nomeMae and p.dtNascimento = :nasc");
        query.setParameter("nome", nome);
        query.setParameter("nomeMae", nomeMae);
        query.setParameter("nasc", dtNascimento);
        query.setMaxResults(1);
        try {
            return (Paciente) query.getSingleResult();
        } catch (Exception e) {
            //System.out.println("Erro: " + e.getMessage() + "em: " + nome);
            return null;
        }

    }

    public void VincularPacientesTasy(Object[] c) {
        Paciente p = new Paciente();
        if ((String) c[4] != null) {
            // Busca de Paciente por nome e CPF.
            p = verificaPacienteCadastradoNomeCPF((String) c[1], (String) c[4]);
        } else {
            // Se CPF for nulo, busca por Nome, Nome da Mãe, Data de Nascimento.
            p = verificaPacienteCadastradoNomeNomeMaeDtNascimento((String) c[1], (String) c[2], (Date) c[3]);
        }

        if (p != null) {
            if (p.getIdPacienteTasy() == null) {
                // Cria o vinculo atraves do ID_PACIENTE_TASY
                p.setIdPacienteTasy(Integer.parseInt((String) c[0]));
                Atualizar(p, "sync", false);
                System.out.println(p.getNome() + " vinculado.");
            }
        } else {
            // Se ele não encontrar o paciente no SMA, salva a novo paciente.
            cadastrarPacienteByTasy(c);
            //System.out.println((String) c[1] + " cadastrado.");
        }
    }

    public void VincularListaPacientesTasy(List<Object[]> resultList) {
        for (Object[] c : resultList) {
            VincularPacientesTasy(c);
        }
    }

    public void verificarCadastroPacientesEditados(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {
            
            // Seleciona o paciente do SMA vinculado pelo id_paciente_tasy com o paciente do TASY.
            Paciente pSMA = bdmEJB.selecionarPorIDPacienteTasy(Integer.parseInt((String) c[0]));
            if (pSMA != null) {
                pSMA.setNome((String) c[1]);
                pSMA.setNomeMae((String) c[2]);
                pSMA.setDtNascimento((Date) c[3]);
                pSMA.setCpf((String) c[4]);
                pSMA.setTelefone((String) c[5]);
                pSMA.setTelefoneCelular((String) c[6]);
                //pSMA.setIdPacienteTasy(Integer.parseInt((String) c[0]));
                AtualizarSimples(pSMA);
                //System.out.println((String) c[1] + " atualizado.");
                //em.merge(BCRUtils.gerarLogIntegracao((String) c[1] + " editado.", "sincronizarPacientes", null, null, logPai));
            } else {
                // Se não houver paciente vinculado, executa o processo de verificação para vincular.
                VincularPacientesTasy(c);
            }
        }
    }

    public void cadastrarPacienteByTasy(Object[] c) {
        try {
            // Se ele não encontrar o paciente no SMA, salva a novo paciente.
            Paciente p = new Paciente();
            p.setIdPacienteTasy(Integer.parseInt((String) c[0]));
            p.setNome((String) c[1]);
            p.setNomeMae((String) c[2]);
            p.setDtNascimento((Date) c[3]);
            p.setCpf((String) c[4]);
            p.setTelefone((String) c[5]);
            p.setTelefoneCelular((String) c[6]);
            p.setDtCadastro(new Date());
            Salvar(p, "sync", null);
            System.out.println(p.getNome() + " cadastrado.");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Erro ao cadastrar paciente" + (String) c[1]);
        }
    }

}
