/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.OrcamentoItem;
import br.com.ux.model.Servico;
import br.com.ux.model.ServicoItem;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author BrunoS
 */
@Stateless
public class ServicoEJB extends Conexao {

    @EJB
    GrupoProcedimentoEJB gpEJB;

    UsuarioSessao us = new UsuarioSessao();

    public void Salvar(Servico servico, List<ServicoItem> itens, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        if (servico.getIdServico() != null) {
            em.merge(servico);
            em.merge(BCRUtils.criarAuditoria("update", servico.getDescricao() + " - " + servico.getTipo(), usuario, new Date(), "Serviços"));
            for (ServicoItem si : itens) {
                si.setIdServico(servico);
                em.merge(si);
                em.merge(BCRUtils.criarAuditoria("update", si.getDescritiva() + " - " + si.getPercentual(), usuario, new Date(), "Itens serviços"));
            }

        } else {
            em.merge(servico);
            em.merge(BCRUtils.criarAuditoria("insert", servico.getDescricao() + " - " + servico.getTipo(), usuario, new Date(), "Serviços"));
            for (ServicoItem si : itens) {
                si.setIdServico(servico);
                em.merge(si);
                em.merge(BCRUtils.criarAuditoria("insert", si.getDescritiva() + " - " + si.getPercentual(), usuario, new Date(), "Itens serviços"));
            }
        }
    }

    public void SalvarServico(Servico servico) {
        em.merge(servico);
        if (servico.getIdServico() != null) {
            em.merge(BCRUtils.criarAuditoria("update", servico.getDescricao() + " - " + servico.getTipo(), us.retornaUsuario(), new Date(), "Serviços"));
        } else {
            em.merge(BCRUtils.criarAuditoria("insert", servico.getDescricao() + " - " + servico.getTipo(), us.retornaUsuario(), new Date(), "Serviços"));
        }
    }

    public void SalvarServicoSimples(Servico servico) {
        em.merge(servico);
    }
    
    public void SalvarServicoItemSimples(ServicoItem servico) {
        em.merge(servico);
    }

    public void SalvarServicoItens(List<ServicoItem> itens) {
        for (ServicoItem si : itens) {
            em.merge(si);
            if (si.getIdServicoItem() != null) {
                em.merge(BCRUtils.criarAuditoria("update", si.getDescritiva() + " - " + si.getPercentual(), us.retornaUsuario(), new Date(), "Itens serviços"));
            } else {
                em.merge(BCRUtils.criarAuditoria("insert", si.getDescritiva() + " - " + si.getPercentual(), us.retornaUsuario(), new Date(), "Itens serviços"));
            }
        }
    }

    public void SalvarServicoItensSync(List<ServicoItem> itens) {
        for (ServicoItem si : itens) {
            em.merge(si);
            if (si.getIdServicoItem() != null) {
                em.merge(BCRUtils.criarAuditoria("update", si.getDescritiva() + " - " + si.getPercentual(), "sync", new Date(), "Itens serviços"));
            } else {
                em.merge(BCRUtils.criarAuditoria("insert", si.getDescritiva() + " - " + si.getPercentual(), "sync", new Date(), "Itens serviços"));
            }
        }
    }

    public void Excluir(Servico servico, String usuario, List<ServicoItem> servicoItems) {
        servico = getEm().getReference(Servico.class, servico.getIdServico());
        servicoItems = servico.getServicoItemList();
        for (ServicoItem si : servicoItems) {
            getEm().remove(si);
        }
        getEm().remove(servico);
        getEm().getEntityManagerFactory().getCache().evictAll();
        getEm().merge(BCRUtils.criarAuditoria("delete", servico.getDescricao() + " - " + servico.getTipo(), usuario, new Date(), "Serviços"));
    }

    public void ExcluirItem(ServicoItem servicoItem, String usuario) {
        servicoItem = getEm().getReference(ServicoItem.class, servicoItem.getIdServicoItem());
        getEm().remove(servicoItem);
        getEm().getEntityManagerFactory().getCache().evictAll();
        getEm().merge(BCRUtils.criarAuditoria("delete", servicoItem.getDescritiva() + " - " + servicoItem.getPercentual(), usuario, new Date(), "Itens serviços"));
    }

    public List<Servico> listarServicos() {
        getEm().getEntityManagerFactory().getCache().evictAll();
        Query query = getEm().createQuery("Select s From Servico s WHERE s.inativo = FALSE");
        return query.getResultList();
    }

    public Servico selecionarPorID(Integer id, String usuario) {
        Query query = getEm().createQuery("Select s From Servico s where s.idServico = :id");
        query.setParameter("id", id);
        getEm().getEntityManagerFactory().getCache().evictAll();
        return (Servico) query.getSingleResult();
    }

    public Servico selecionarPorPadrao(String s) {
        Query query = getEm().createQuery("Select s From Servico s where s.tipo = :padrao");
        query.setParameter("padrao", s);
        getEm().getEntityManagerFactory().getCache().evictAll();
        return (Servico) query.getSingleResult();
    }

    public List<Servico> listarServicosLazyMode(int primeiro, int qtd) {
        Query query = getEm().createQuery("Select s From Servico s WHERE s.inativo = FALSE");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<Servico> listarServicosLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query = getEm().createQuery("Select s From Servico s " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Servico retornaServicoPadrao() {
        Query query = getEm().createQuery("Select s From Servico s where s.padrao = :ok");
        query.setParameter("ok", "S");
        return (Servico) query.getResultList().get(0);
    }

    public Servico retornaServicoPadraoAvulso() {
        Query query = getEm().createQuery("Select s From Servico s where s.lancamentoAvulso = true");
        return (Servico) query.getResultList().get(0);
    }

    public ServicoItem retornaServicoItemPadraoAvulso() {
        Query query = getEm().createQuery("Select s From Servico s where s.lancamentoAvulso = true");
        Servico s = new Servico();
        ServicoItem si = new ServicoItem();
        if (query.getResultList().size() > 0) {
            s = (Servico) query.getResultList().get(0);
            si = s.getServicoItemList().get(0);
        }
        return (ServicoItem) si;
    }

    public ServicoItem retornaServicoPadraoPorItemFechamento(CirurgiaItem ci) {
        for (ServicoItem si : ci.getIdServico().getServicoItemList()) {
            if (si.getBaseCalculo().equals('S')) {
                return si;
            }
        }
        return null;
    }

    public Double retornaValorBaseServico(CirurgiaItem ci) {
        for (ServicoItem si : ci.getIdServico().getServicoItemList()) {
            // CHARLES - 05072017 - Solicitado pelo Vera Cruz.
            // Comentado para realizar os calculos de serviços com fator, mas sem base de calculo
            // if (si.getBaseCalculo().equals('S')) {
            //   return ci.getValorTotal() / si.getIdServico().getEscala();
            // }             
            return ci.getValorTotal() / si.getIdServico().getEscala();
        }
        return null;
    }

    public Double retornaValorBaseServicoPorOrcamentoItem(OrcamentoItem oi) {
        for (ServicoItem si : oi.getIdServico().getServicoItemList()) {
            if (si.getBaseCalculo().equals('S')) {
                return oi.getValorTotal() / si.getIdServico().getEscala();
            }
        }
        return null;
    }

    public Number contarServicosRegistro(String clausula) {
        Query query = getEm().createQuery("Select COUNT(s.idServico) From Servico s " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<Servico> autoCompleteServico(String var) {
        Query query = getEm().createQuery("Select s From Servico s where s.inativo = FALSE AND (s.descricao LIKE :desc OR s.idServico like :id OR s.nomeProcedimentoInternoTasy like :id OR s.idProcedimentoTasy like :id)");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();
    }

    public List<Servico> autoCompleteProcedimentoCbhpm(String var) {
        Query query = getEm().createQuery("Select s From Servico s where s.inativo = FALSE AND s.idGrupoProcedimentoCbhpm IS NOT NULL AND (s.descricao LIKE :desc OR s.idServico like :id OR s.idProcedimentoTasy like :id)");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();
    }

    // Sincronização TASY
    public Servico selecionarPorIDProcedimentoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT s FROM Servico s WHERE s.idProcedimentoTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (Servico) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }

    public void VincularServicosProcedimentosTasy(Object[] c, LogIntegracao logPai) {
        Date dtInicio = new Date();
        if (c[0] != null) {
            Servico s = selecionarPorIDProcedimentoTasy(((BigDecimal) c[0]).intValue());
            if (s != null) {                
                if (c[5] != null) {                    
                    s.setNomeProcedimentoInternoTasy((String) c[5]);
                    SalvarServicoSimples(s);
                }

                if (s.getIdProcedimentoTasy() == null) {
                    try {
                        // Cria o vinculo atraves do ID_PROCEDIMENTO_TASY
                        s.setIdProcedimentoTasy(((BigDecimal) c[0]).intValue());
                        if (c[2] != null) {
                            s.setIdGrupoProcedimentoCbhpm(gpEJB.selecionarPorIDGrupoProcedimentoTasy(((BigDecimal) c[3]).intValue()));
                        }
                        s.setNomeProcedimentoInternoTasy((String) c[5]);
                        SalvarServicoSimples(s);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                } else if (retornaListaItensServicoPorServico(s.getIdServico()).isEmpty()) {
                    List<ServicoItem> listaItensServico = criarListaDeServicosItemPadrao((String) c[5], s);
                    SalvarServicoItensSync(listaItensServico);
                }
            } else {
                // Se ele não encontrar o Procedimento no SMA, salva um novo cadastro.
                cadastrarServicoProcedimento(c, dtInicio, logPai);
            }
        }
    }

    public void VincularListaServicosTasy(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {            
            VincularServicosProcedimentosTasy(c, logPai);
        }
    }

    public List<ServicoItem> retornaListaItensServicoPorServico(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT s FROM ServicoItem s WHERE s.idServico.idServico = :id");
        query.setParameter("id", id);
        return query.getResultList();
    }

    public void cadastrarServicoProcedimento(Object[] c, Date dtInicio, LogIntegracao logPai) {
        try {
            //System.out.println("Cadastrando proc. " + ((BigDecimal) c[0]).intValue());
            Servico s = new Servico();
            s.setInativo(Boolean.FALSE);
            s.setTipo("N");
            s.setEscala(1D);
            s.setPadrao("N");
            s.setLancamentoAvulso(Boolean.FALSE);
            s.setDescricaoCbhpm((String) c[1]);
            s.setClassificacao((String) c[3]);
            s.setDescricao((String) c[1]);
            s.setNomeProcedimentoInternoTasy((String) c[5]);
            s.setDtCadastro(new Date());
            s.setDtAtualizacao(new Date());
            s.setIdGrupoProcedimentoCbhpm(gpEJB.selecionarPorIDGrupoProcedimentoTasy(((BigDecimal) c[2]).intValue()));
            s.setIdProcedimentoTasy(((BigDecimal) c[0]).intValue());
            Salvar(s, null, "sync");
            SalvarServicoItens(criarListaDeServicosItemPadrao((String) c[1], s));
        } catch (Exception e) {
            System.out.println(e);
            //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar " + (String) c[1] + ". " + e.getMessage(), "VincularServicosProcedimentosTasy", null, null, logPai));
        }
    }

    public List<ServicoItem> criarListaDeServicosItemPadrao(String descricao, Servico s) {
        List<ServicoItem> lista = new ArrayList<>();
        ServicoItem si = new ServicoItem();
        si.setBaseCalculo('S');
        si.setIdServico(s);
        si.setDescritiva(descricao);
        si.setPercentual(100D);
        lista.add(si);
        return lista;
    }

}
