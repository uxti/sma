/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.TextoEmails;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class TextosEmailsEJB extends Conexao {

    UsuarioSessao us = new UsuarioSessao();

    public void Salvar(TextoEmails textoEmail) {
        String acao = "";
        if (textoEmail.getIdTextoEmail() != null) {
            getEm().merge(textoEmail);
            acao = "insert";
        } else {
            getEm().merge(textoEmail);
            acao = "update";
        }
        getEm().merge(BCRUtils.criarAuditoria(acao, textoEmail.getTexto(), us.retornaUsuario(), new Date(), "Texto Emails"));
    }

    public void atualizar(TextoEmails textoEmail) {
        getEm().merge(textoEmail);
        getEm().merge(BCRUtils.criarAuditoria("update", textoEmail.getTexto(), us.retornaUsuario(), new Date(), "Texto Emails"));
    }

    public void Excluir(TextoEmails textoEmail) {
        textoEmail = getEm().getReference(TextoEmails.class, textoEmail.getIdTextoEmail());
        getEm().remove(textoEmail);
        getEm().merge(BCRUtils.criarAuditoria("delete", textoEmail.getTexto(), us.retornaUsuario(), new Date(), "Texto Emails"));
    }

    public List<TextoEmails> listarTextoEmails() {
        return getEm().createQuery("Select tx From TextoEmails tx ").getResultList();
    }

    public TextoEmails selecionarPorID(Integer id) {
        Query query = getEm().createQuery("Select tx From TextoEmails tx where tx.idTextoEmail = :id");
        query.setParameter("id", id);
        return (TextoEmails) query.getSingleResult();
    }
}
