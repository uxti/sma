/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.controller.integracao.BuscaDadosMySQLEJB;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.Terceiro;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author charles
 */
@Stateless
public class TerceiroEJB extends Conexao {

    @EJB
    CBOSaudeEJB cboEJB;
    @EJB
    BuscaDadosMySQLEJB bdmEJB;
    @EJB
    AssociacaoEJB assEJB;

    public void Salvar(Terceiro terceiro, String usuario, Boolean auditoria) {
        if (terceiro.getIdTerceiro() != null) {
            em.merge(terceiro);
            if (auditoria) {
                em.merge(BCRUtils.criarAuditoria("update", "Nome: " + terceiro.getNome() + " - CPF/CNPJ: " + terceiro.getCgccpf(), usuario, new Date(), "Terceiro"));
            }
        } else {
            em.persist(terceiro);
            em.flush();
            em.refresh(terceiro);
            if (auditoria) {
                em.merge(BCRUtils.criarAuditoria("insert", "Nome: " + terceiro.getNome() + " - CPF/CNPJ: " + terceiro.getCgccpf(), usuario, new Date(), "Terceiro"));
            }            
        }
    }

    public void Excluir(Terceiro terceiro, String usuario) {
        terceiro = em.getReference(Terceiro.class, terceiro.getIdTerceiro());
        em.remove(terceiro);
        em.merge(BCRUtils.criarAuditoria("delete", "Nome: " + terceiro.getNome() + " - CPF/CNPJ: " + terceiro.getCgccpf(), usuario, new Date(), "Terceiro"));
    }

    public List<Terceiro> listarTerceiro() {
        return em.createQuery("Select t From Terceiro t where t.inativo = FALSE ORDER BY t.nome").getResultList();
    }

    public List<Terceiro> listarTerceirosOrdenadoPorNome() {
        return em.createQuery("Select t From Terceiro t where t.inativo = FALSE ORDER BY t.nome").getResultList();
    }

    public Terceiro selecionarPorID(Integer id, String usuario) {
        Query query = em.createQuery("Select t From Terceiro t where t.idTerceiro = :id");
        query.setParameter("id", id);
        return (Terceiro) query.getSingleResult();
    }

    public Terceiro selecionarTerceiroCNPJCPF(String CNPJ, String busca) {
        Query query = em.createQuery("Select t From Terceiro t where t.cgccpf = :ri OR t.nomeFantasia like :hosp");
        query.setParameter("ri", CNPJ);
        query.setParameter("hosp", "%" + busca + "%");
        query.setMaxResults(1);
        return (Terceiro) query.getSingleResult();
    }

    public String verificaCPFRetornaNome(String CGCCPF, String FisicaOuJuridica) {
        em.getEntityManagerFactory().getCache().evictAll();
        if (FisicaOuJuridica.equals("J")) {
            String nome = "";
            try {
                Query query = em.createQuery("Select t.nomeFantasia from Terceiro t where t.cgccpf = :cgccpf");
                query.setParameter("cgccpf", CGCCPF);
                nome = (String) query.getSingleResult();
            } catch (Exception e) {
                if (nome == null) {
                    nome = "Inexistente";
                }
            }
            return nome;
        } else {
            String nome = "";
            try {
                Query query = em.createQuery("Select t.nome from Terceiro t where t.cgccpf = :cgccpf");
                query.setParameter("cgccpf", CGCCPF);
                nome = (String) query.getSingleResult();
            } catch (Exception e) {
                if (nome == null) {
                    nome = "Inexistente";
                }
            }
            return nome;
        }
    }

    public Number consultarCPFCNPJ(String cgccpf) {
        Query query = em.createQuery("Select count(t) From Terceiro t where t.cgccpf = :cgccpf");
        query.setParameter("cgccpf", cgccpf);
        return (Number) query.getSingleResult();
    }

    public List<Terceiro> listarTerceiroLazyMode(int primeiro, int qtd) {
        Query query = em.createQuery("Select t From Terceiro t where t.inativo = FALSE");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<Terceiro> listarTerceirosLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query = em.createQuery("Select t From Terceiro t " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarTerceirosRegistro(String clausula) {
        Query query = em.createQuery("Select COUNT(t.idTerceiro) From Terceiro t " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<Terceiro> autoCompleteTerceiro(String var) {
        Query query = em.createQuery("Select t From Terceiro t where t.inativo = FALSE AND (t.nomeFantasia LIKE :fant OR t.nome LIKE :desc OR t.idTerceiro like :id OR t.cgccpf like :cnpj)");
        query.setParameter("desc", var + "%");
        query.setParameter("fant", var + "%");
        query.setParameter("id", var + "%");
        query.setParameter("cnpj", var + "%");
        return query.getResultList();
    }

    public Terceiro retornaMedicoCadastradoCPF(String CPF) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT t FROM Terceiro t WHERE t.cgccpf = :cpf");
        query.setParameter("cpf", CPF);
        query.setMaxResults(1);
        try {
            return (Terceiro) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + CPF);
            return null;
        }
    }

    public Terceiro retornaMedicoCadastradoNome(String nome) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT t FROM Terceiro t WHERE t.nome LIKE :nome ");
        query.setParameter("nome", "%" + nome + "%");
        query.setMaxResults(1);
        try {
            return (Terceiro) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + nome);
            return null;
        }
    }

    public void VincularMedicosTasy(Object[] c, LogIntegracao logPai) {
        Terceiro t = new Terceiro();
        Date dtInicio = new Date();
        t = retornaMedicoCadastradoNome((String) c[1]);

        if (t != null) {
            if (t.getIdTerceiroTasy() == null) {
                try {
                    // Cria o vinculo atraves do ID_TERCEIRO_TASY
                    t.setIdTerceiroTasy(Integer.parseInt((String) c[0]));
                    if (c[12] != null) {
                        t.setIdCboSaude(cboEJB.selecionarPorIDCboSaudeTasy(((BigDecimal) c[12]).intValue()));
                    }
                    Salvar(t, "sync", false);
                    //em.merge(BCRUtils.gerarLogIntegracao(t.getNome() + " vinculado.", "vincularMedicos", dtInicio, new Date(), logPai));
                } catch (Exception e) {
                    System.out.println(e);
                    //em.merge(BCRUtils.gerarLogIntegracao("Erro ao vincular " + t.getNome() + ". " + e.getMessage(), "vincularMedicos", null, null, logPai));
                }
            }
        } else {
            try {
                // Se ele não encontrar o médico no SMA, salva a novo terceiro.
                t = new Terceiro();
                t.setIdTerceiroTasy(Integer.parseInt((String) c[0]));
                t.setNome((String) c[1]);
                t.setTipoPessoa(((String) c[2]).charAt(0));
                t.setCgccpf((String) c[3]);
                t.setNomeFantasia((String) c[4]);
                t.setRazaoSocial((String) c[5]);
                t.setRg((String) c[6]);
                t.setTelefone((String) c[7]);
                t.setCelular((String) c[8]);
                t.setTipoConselho((String) c[9]);
                t.setNumeroConselho((String) c[10]);
                t.setSexo(((String) c[11]).charAt(0));
                if (c[12] != null) {
                    t.setIdCboSaude(cboEJB.selecionarPorIDCboSaudeTasy(((BigDecimal) c[12]).intValue()));
                }
                t.setInativo(c[13].equals("0") ? Boolean.TRUE : Boolean.FALSE);
                t.setTerceiroMedHosp(Boolean.FALSE);
                Salvar(t, "sync", false);
                //em.merge(BCRUtils.gerarLogIntegracao(t.getNome() + " cadastrado.", "vincularMedicos", dtInicio, new Date(), logPai));
            } catch (Exception e) {
                System.out.println(e);
                //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar " + (String) c[1] + ". " + e.getMessage(), "vincularMedicos", null, null, logPai));
            }
        }
    }

    public void VincularListaMedicosTasy(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {
            VincularMedicosTasy(c, logPai);
        }
    }

    public void verificarCadastroMedicosEditados(List<Object[]> resultList, LogIntegracao logPai) {
        Integer i = 0;
        for (Object[] c : resultList) {
            i++;
            //System.out.println(i + " de: " + resultList.size());
            // c[0] = CD_PESSOA_FISICA            
            // Seleciona o paciente do SMA vinculado pelo id_paciente_tasy com o paciente do TASY.
            Terceiro tSMA = bdmEJB.selecionarPorIDMedicoTasy(Integer.parseInt((String) c[0]));

            // Verifica se existe médico vinculado a esse ID.
            if (tSMA != null) {
                // Cria um objeto com o resultado da pesquisa ao TASY
                Terceiro tTasy = new Terceiro();
                tTasy.setNome((String) c[1]);
                tTasy.setTipoPessoa(((String) c[2]).charAt(0));
                tTasy.setCgccpf((String) c[3]);
                tTasy.setNomeFantasia((String) c[1]);
                tTasy.setRazaoSocial((String) c[5]);
                tTasy.setRg((String) c[6]);
                tTasy.setTelefone((String) c[7]);
                tTasy.setCelular((String) c[8]);
                tTasy.setTipoConselho((String) c[9]);
                tTasy.setNumeroConselho((String) c[10]);
                tTasy.setSexo(((String) c[11]).charAt(0));
                if (c[12] != null) {
                    tTasy.setIdCboSaude(cboEJB.selecionarPorIDCboSaudeTasy(((BigDecimal) c[12]).intValue()));
                }
                Salvar(tSMA, "sync", false);
            } else {
                // Se não houver paciente vinculado, executa o processo de verificação para vincular.
                VincularMedicosTasy(c, logPai);
            }
        }
    }

    public Terceiro retornaCadastroHospital() {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT t FROM Terceiro t WHERE t.terceiroMedHosp = true");
        query.setMaxResults(1);
        try {
            return (Terceiro) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
            return null;
        }
    }

}
