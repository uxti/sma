/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Caixa;
import br.com.ux.model.CaixaPai;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Operacao;
import br.com.ux.model.Repasse;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.ContaCorrenteEstatica;
import br.com.ux.util.UsuarioSessao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class ContaCorrenteEJB extends Conexao {

    @EJB
    PacienteEJB pEJB;
    private UsuarioSessao us = new UsuarioSessao();

    @EJB
    ChequeEJB cEJB;

    @EJB
    ProcedimentoEJB prEJB;

    @EJB
    CirurgiaEJB ciEJB;

    public void Salvar(ContaCorrente contaCorrente, Usuario u, List<ContaCorrente> listaCC, String movimentacao) {
        getEm().getEntityManagerFactory().getCache().evictAll();
        Caixa caixa = new Caixa();
        String historico, tipo;
        if (contaCorrente.getIdContaCorrente() == null) {
            if (contaCorrente.getTipo().equals("S")) {
                historico = "Saque";
                tipo = "D";
            } else {
                historico = "Estorno";
                tipo = "C";
            }
            getEm().persist(contaCorrente);
            getEm().flush();
            getEm().refresh(contaCorrente);

            // Lança no Caixa referenciando o CaixaPai.
            if (contaCorrente.getTipo().equals("S")) {
                Query queryOp = getEm().createQuery("Select o From Operacao o where o.tipo = 'S'");
                queryOp.setMaxResults(1);
                caixa.setIdOperacao((Operacao) queryOp.getSingleResult());
                caixa.setHistorico(historico + " realizado pelo usuário " + contaCorrente.getIdUsuario().getUsuario()
                        + " no valor de: R$ " + contaCorrente.getValor()
                        + ". Favorecido: " + contaCorrente.getIdTerceiro().getNome());
                caixa.setStatus("Baixado");
                caixa.setIdRepasse(contaCorrente.getIdRepasse());
                caixa.setDtCaixa(new Date());
                caixa.setTipoLancamento(tipo.charAt(0));
                caixa.setValor(contaCorrente.getValor());
                caixa.setIdCirurgia(contaCorrente.getIdCirurgia());
                caixa.setIdUsuario(u);
//            caixa.setNumeroParcela(contaCorrente.getIdRepasse().getNumeroParcela());
                getEm().persist(caixa);
                getEm().flush();
                getEm().refresh(caixa);

                // Pega o ID gerado do Caixa e atualiza a Conta Corrente.
                contaCorrente.setIdCaixa(caixa);
                getEm().persist(contaCorrente);
                getEm().flush();
                getEm().refresh(contaCorrente);
            }

        } else {
            getEm().merge(contaCorrente);
        }

        if (movimentacao.equals("S")) {
            for (ContaCorrente cc : listaCC) {
                cc.setIdSaque(contaCorrente.getIdContaCorrente());
                cc.setSituacao("Liberado");
                cc.setStatus("Retirado");
                cc.setIdUsuario(contaCorrente.getIdUsuario());
                cc.setDtRetirada(new Date());
                getEm().merge(cc);
            }
        } else if (movimentacao.equals("E")) {
            for (ContaCorrente cc : listaCC) {
                cc.setSituacao("Liberado");
                cc.setStatus("Não Retirado");
                DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
                String data = f.format(cc.getDtLancamento());
                cc.setObservacao(cc.getObservacao() + ". Estorno realizado em: " + data + ". Nº " + contaCorrente.getIdContaCorrente());
                cc.setIdUsuario(contaCorrente.getIdUsuario());
                cc.setDtEstorno(new Date());
                cc.setDtRetirada(null);
                cc.setDtLiberacao(null);
                getEm().merge(cc);
            }
        }
    }

    public void Excluir(ContaCorrente contaCorrente, String usuario) {
        contaCorrente = getEm().getReference(ContaCorrente.class, contaCorrente.getIdContaCorrente());
        getEm().remove(contaCorrente);
    }

    public void AtualizarCC(ContaCorrente contaCorrente) {
        getEm().merge(contaCorrente);
    }

    public void SalvarLista(List<ContaCorrente> lista, String usuario) {
        for (ContaCorrente cc : lista) {
            getEm().merge(cc);
            getEm().merge(BCRUtils.criarAuditoria("insert", "Conta Corrente: " + cc.getValor() + " - " + cc.getIdTerceiro().getNome(), usuario, new Date(), "Conta Corrente"));
        }
    }

    public void SalvarSimples(ContaCorrente contaCorrente, String usuario) {
        em.merge(contaCorrente);
    }

    public void SalvarEstorno(ContaCorrente contaCorrente, String usuario, boolean total, Integer idEstornada) {
        if (total) {
            getEm().merge(BCRUtils.criarAuditoria("estorno", contaCorrente.getIdContaCorrente().toString() + ". Estorno total do saque.", usuario, new Date(), "Conta Corrente"));
        } else {
            getEm().merge(BCRUtils.criarAuditoria("estorno", contaCorrente.getIdContaCorrente().toString() + ". Valor: " + contaCorrente.getValor() + ". Médico: " + contaCorrente.getIdTerceiro().getNome() + ". Estorno individual do saque.", usuario, new Date(), "Conta Corrente"));
        }
        getEm().merge(contaCorrente);

    }

    public Usuario selecionarIDUsuario(String usuario) {
        Query query = getEm().createQuery("Select u From Usuario u where u.usuario = :usuario");
        query.setParameter("usuario", usuario);
        return (Usuario) query.getSingleResult();
    }

    public ContaCorrente selecionarPorID(Integer id) {
        Query query = getEm().createQuery("Select c From ContaCorrente c where c.idContaCorrente = :id ");
        query.setParameter("id", id);
        return (ContaCorrente) query.getSingleResult();
    }

    // RETIRADO DEVIDO A EXIGENCIA DE RETIRARMOS A DATA DAS PESQUISAS.
    //    public List<ContaCorrente> pesquisaCC_Parametros(Date ini, Date fim, String Terceiro, String clausulas) {
    public List<ContaCorrente> pesquisaCC_Parametros(String Terceiro, String clausulas) {
            
        Query query = em.createQuery("SELECT c FROM ContaCorrente c WHERE c.idTerceiro.idTerceiro LIKE :Terceiro AND c.tipo IN ('C', 'D') "
                + clausulas + " ORDER by c.idRepasse.dtVencimento, c.idRepasse.idRepasse");
        query.setParameter("Terceiro", Terceiro);
        System.out.println("Clausulas: " + clausulas);
        System.out.println(query.getResultList().size());
        return query.getResultList();
    }

    public List<ContaCorrenteEstatica> pesquisaContaCorrenteResumo(String Terceiro, String dataInicio, String dataFim) {
        String sql = "SELECT "
                + "  C.ID_CIRURGIA, " // c[0]
                + "  PAC.ID_PACIENTE, " // c[1]
                + "  R.NUMERO_PARCELA, " // c[2]
                + "  (Select MAX(numero_Parcela) From Cheque where id_cirurgia = cir.id_cirurgia), " // c[3]
                + "  C.STATUS, " // c[4]
                + "  C.SITUACAO, " // c[5]
                + "  CIR.DT_CIRURGIA , " // c[6]
                + "  R.DT_VENCIMENTO , " // c[7]
                + "  C.DT_ESTORNO , " // c[8]
                + "  C.DT_RETIRADA , " // c[9]
                + "  SUM(R.VALOR_DOCUMENTO), " // c[10]
                + "  C.ID_CONTA_CORRENTE,  " // c[11]
                + "  R.ID_CHEQUE,  " // c[12]
                + "  CIR.ID_PROCEDIMENTO, " // c[13]
                + "  r.id_cirurgia_item  " // c[14]
                + "FROM CONTA_CORRENTE C INNER JOIN REPASSE R ON C.ID_REPASSE = R.ID_REPASSE "
                + "  INNER JOIN CIRURGIA CIR ON CIR.ID_CIRURGIA = C.ID_CIRURGIA "
                + "  INNER JOIN PACIENTE PAC ON CIR.ID_PACIENTE = PAC.ID_PACIENTE "
                + "  INNER JOIN TERCEIRO T   ON C.ID_TERCEIRO = T.ID_TERCEIRO "
                + "WHERE C.ID_TERCEIRO = " + Terceiro + " "
                + "  AND (C.VALOR != 0 OR C.SITUACAO != 'Estornado') AND C.SITUACAO != 'Devolvido' AND C.TIPO IN ('C', 'D') "
                + "  AND R.DT_VENCIMENTO BETWEEN '" + dataInicio + "' AND '" + dataFim + "' "
                + "  AND C.DT_PAGAMENTO_MEDICO IS NULL "
                + "GROUP BY "
                + "  CIR.DT_CIRURGIA, "
                + "    C.ID_CIRURGIA, "
                + "  PAC.NOME, "
                + "    R.NUMERO_PARCELA, "
                + "    C.SITUACAO, "
                + "    C.STATUS, "
                + "    R.NUMERO_PARCELA "
                + "ORDER BY R.DT_VENCIMENTO, 1, R.ID_REPASSE";
        System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<ContaCorrenteEstatica> contas = new ArrayList<ContaCorrenteEstatica>();
        for (Object[] c : resultList) {
            ContaCorrenteEstatica cce = new ContaCorrenteEstatica();
            cce.setIdAtendimento(cirurgiaEJB.pesquisarCirurgiaPorID((Integer) c[0]));
            cce.setPaciente(pEJB.selecionarPorID((Integer) c[1], us.retornaUsuario()));
            cce.setNumeroParcela(((Integer) c[2]).toString());
            cce.setStatus((String) c[4]);
            cce.setSituacao((String) c[5]);
            cce.setDtProcedimento((Date) c[6]);
            cce.setDtPrevisao((Date) c[7]);
            cce.setDtEstorno((Date) c[8]);
            cce.setDtRetirada((Date) c[9]);
            cce.setValor((Double) c[10]);
            cce.setContaCorrente(selecionarPorID((Integer) c[11]));
            cce.setPagamento(cEJB.selecionarPorID((Integer) c[12], null));
            if ((Integer) c[13] != null) {
                cce.setProcedimento(prEJB.selecionarPorID((Integer) c[13], null));
            }
            if ((Integer) c[14] != null) {
                cce.setItem(ciEJB.selecionarItemPorID((Integer) c[14]));
            }
            contas.add(cce);
        }
        return contas;
    }

    public List<ContaCorrente> pesquisaSaque_Parametros(Date ini, Date fim, String Terceiro) {
        Query query = getEm().createQuery("SELECT c FROM ContaCorrente c WHERE c.dtLancamento BETWEEN :ini AND :fim AND c.idTerceiro.idTerceiro LIKE :Terceiro AND c.tipo IN ('S','E') ");
        query.setParameter("ini", ini);
        query.setParameter("fim", fim);
        query.setParameter("Terceiro", Terceiro);
        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public List<ContaCorrente> carregaContasParaPagar() {
        Query query = getEm().createQuery("SELECT c FROM ContaCorrente c WHERE  c.tipo IN ('S') and c.dtPagamentoMedico IS NULL ");
        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }
    
    public Number contarContaCorrente(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idContaCorrente) From ContaCorrente p " + clausula);
        return (Number) query.getSingleResult();
    }
    
    public List<ContaCorrente> listarContaCorrenteLazyModeWhere(int first, int pageSize, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From ContaCorrente p " + clausula);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

    public List<ContaCorrente> pesquisaItensSaque(ContaCorrente Saque) {
        Query query = getEm().createQuery("SELECT c FROM ContaCorrente c WHERE c.idSaque = :id");
        query.setParameter("id", Saque.getIdContaCorrente());
        getEm().getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public List<ContaCorrente> retornaContaCorrentePorFechamento(Integer id) {
        Query query = getEm().createQuery("SELECT c FROM ContaCorrente c WHERE c.idCirurgia.idCirurgia = :id");
        query.setParameter("id", id);
        return query.getResultList();
    }

    public void atualizarPagamentoDoMedico(Integer id, Integer idRepasse) {
        Query q = getEm().createQuery("UPDATE ContaCorrente cc set cc.dtPagamentoMedico = :date where cc.idSaque = :id2");
        q.setParameter("date", new Date());
        q.setParameter("id2", id);
        q.executeUpdate();

        Query q2 = getEm().createQuery("UPDATE ContaCorrente cc set cc.dtPagamentoMedico = :date where cc.idContaCorrente = :id2");
        q2.setParameter("date", new Date());
        q2.setParameter("id2", id);
        q2.executeUpdate();

        for (ContaCorrente cc : pesquisarContasCorrentePorSaque(id)) {
            Query q3 = getEm().createQuery("UPDATE Repasse rr set rr.dtRecebimento = :date where rr.idRepasse = :Repasse");
            //System.out.println("Atualizando repasses...");
            q3.setParameter("date", new Date());
            q3.setParameter("Repasse", cc.getIdRepasse().getIdRepasse());
            q3.executeUpdate();
        }

    }

    public Double consultarSaldoAnterior(Date dtIni) {
        DateFormat f = new SimpleDateFormat("dd/MM/yy");
        String dataIniSQL;
        if (dtIni == null) {
            dataIniSQL = f.format(new Date());
        } else {
            dataIniSQL = f.format(dtIni);
        }
        String sql = "SELECT 1000 SALDOATUAL FROM dual";
        Query query = getEm().createNativeQuery(sql);
        return (Double) query.getSingleResult();
    }

    public CaixaPai verificaCaixaAbertoRetornandoObjeto(String usuario, Date dataCaixa) {
        Query query = getEm().createQuery("Select cp From CaixaPai cp where cp.situacao != 'Fechado' and cp.idUsuario.usuario = :id and cp.dtCaixaPai = :data");
        query.setParameter("id", usuario);
        query.setParameter("data", dataCaixa);
        return (CaixaPai) query.getSingleResult();
    }

    public Long verificaCaixaAberto(String usuario, Date dataCaixa) {
        Query query = getEm().createQuery("Select count(cp.idCaixaPai) From CaixaPai cp where cp.situacao != 'Fechado' and cp.idUsuario.usuario = :id and cp.dtCaixaPai = :data");
        query.setParameter("id", usuario);
        query.setParameter("data", dataCaixa);
        return (Long) query.getSingleResult();
    }

    public Operacao selecionarOperacao(String tipo) {
        Query query = getEm().createQuery("Select o FROM Operacao o where o.tipo = :tipo");
        query.setParameter("tipo", tipo);
        query.setMaxResults(1);
        return (Operacao) query.getSingleResult();
    }

    public List<ContaCorrente> listarContaCorrentePorUsuario(Usuario usuario) {
        Query query = getEm().createQuery("Select c From ContaCorrente c where c.idTerceiro.idTerceiro = :idu and c.idCirurgia.status = 'L'");
        query.setParameter("idu", usuario.getIdTerceiro().getIdTerceiro());
        return query.getResultList();
    }

    public List<ContaCorrente> listarContaCorrentePorTerceiro(Integer idTerceiro) {
        Query query = getEm().createQuery("Select c From ContaCorrente c where c.idTerceiro.idTerceiro = :idu and c.idCirurgia.status = 'L'");
        query.setParameter("idu", idTerceiro);
        return query.getResultList();
    }

    public List<ContaCorrente> pesquisarContaCorrentePorTerceiro(Integer idTerceiro, String datas, String idCirurgia,
            String idPaciente, String idProcedimento, String status, String situacao) {
        String sql = "Select c From ContaCorrente c where c.idTerceiro.idTerceiro = :idu and c.idCirurgia.idCirurgia like '" + idCirurgia + "'"
                + " and c.idPaciente.idPaciente like '" + idPaciente + "' "
                + "and c.idRepasse.idCirurgia.idProcedimento.idProcedimento like '" + idProcedimento + "' "
                + "and c.status like '" + status + "' and c.situacao like '" + situacao + "'  " + datas + " ORDER BY c.idRepasse.dtVencimento";
        System.out.println(sql);
        Query query = getEm().createQuery(sql);
        query.setParameter("idu", idTerceiro);
        return query.getResultList();
    }

    public List<ContaCorrente> pesquisarContaCorrentePorPaciente(Integer idPaciente) {
        getEm().getEntityManagerFactory().getCache().evictAll();
        Query query = getEm().createQuery("SELECT c From ContaCorrente c where c.idPaciente.idPaciente = :id ORDER BY c.idRepasse.dtVencimento");
        query.setParameter("id", idPaciente);
        return query.getResultList();
    }

    public ContaCorrente pesquisarContaCorrentePorRepasse(Repasse r) {
        Query query = getEm().createQuery("SELECT r From ContaCorrente r where r.idRepasse =:rr and r.idCirurgia.status = 'L'");
        query.setParameter("rr", r);
        return (ContaCorrente) query.getSingleResult();
    }

    public void atualizarContaCorrente(ContaCorrente contaCorrente) {
        getEm().merge(contaCorrente);
    }

    public List<ContaCorrente> pesquisarContasCorrenteMedico(Date dtini, Date dtFim, Integer idAtendimento, Integer idProcedimento, Integer idPaciente, String funcao,
            String tipo_atendimento) {
        Query query = getEm().createQuery("SELECT c FROM ContaCorrente c where c.idCirurgia.idCirurgia = :idAtendimento and c.idCirurgia.idCirurgia = :idAtendimento and c.idCirurgia.dtCirurgia BETWEEN :dtini and :dtfim and c.idRepasse.idSici.idSici = :idProc");
        return null;
    }

    public List<ContaCorrente> pesquisarContasCorrentePorSaque(Integer idSaque) {
        Query query = getEm().createQuery("SELECT c FROM ContaCorrente c where c.idSaque = :id");
        query.setParameter("id", idSaque);
        return query.getResultList();
    }

    public static Date addAno(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.YEAR, qtd);
        return cal.getTime();
    }
//    ======================================================================================================
//    Movimentacao por Medico

    public List<ContaCorrenteEstatica> pesquisaContaCorrentePorMedico(Integer idTerceiro, Date dtIni, Date dtFim, String idProc, String idPac, String tipo, String idCir, String tipoInternacao) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String datFormat1 = null;
        String datFormat2 = null;
        if (dtIni == null) {
            dtIni = addAno(new Date(), -50);
            datFormat1 = df.format(dtIni);
        } else {
            datFormat1 = df.format(dtIni);
        }
        if (dtFim == null) {
            dtFim = addAno(new Date(), 50);
            datFormat2 = df.format(dtFim);
        } else {
            datFormat2 = df.format(dtFim);
        }

        String sql = "SELECT c.dt_cirurgia, c.id_cirurgia, c.id_paciente, c.id_terceiro, c.id_procedimento, si.descritiva, c.tipo_atendimento, sum(r.valor_documento) valor , s.tipo, ti.descricao, cc.id_conta_corrente, sici.id_sici "
                + "FROM repasse r, terceiro t, cirurgia c, conta_corrente cc, servico_item_cirurgia_item sici, servico_item si, servico s, tipo_internacao ti "
                + "WHERE r.id_terceiro = t.id_terceiro AND "
                + "c.id_cirurgia = r.id_cirurgia AND "
                + "c.id_cirurgia = r.id_cirurgia AND "
                + "r.id_sici     = sici.id_sici  AND "
                + "sici.id_servico_item = si.id_servico_item AND "
                + "ti.id_tipo_internacao = c.id_tipo_internacao AND "
                + "cc.id_repasse = r.id_repasse AND "
                + "s.id_servico  = si.id_servico AND "
                + "r.id_terceiro = " + idTerceiro + " AND "
                + "s.tipo != 'H' AND "
                + "c.id_tipo_internacao like '" + tipoInternacao + "' AND "
                + "c.dt_cirurgia between '" + datFormat1 + "' AND '" + datFormat2 + "' AND  "
                + "c.id_paciente like '" + idPac + "' AND "
                + "c.id_procedimento like '" + idProc + "' AND "
                + "ifnull(c.tipo_atendimento, '') like '" + tipo + "' AND "
                + "c.id_cirurgia like '" + idCir + "' "
                + "GROUP BY c.dt_cirurgia, c.id_cirurgia, c.id_paciente, c.id_terceiro, c.id_procedimento, si.descritiva, c.tipo_atendimento "
                + "ORDER BY c.dt_cirurgia";

        System.out.println("Escrevendo o SQL: " + sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<ContaCorrenteEstatica> contas = new ArrayList<ContaCorrenteEstatica>();
        for (Object[] c : resultList) {
            ContaCorrenteEstatica cce = new ContaCorrenteEstatica();
            cce.setDtProcedimento((Date) c[0]);
            cce.setIdAtendimento(cirurgiaEJB.pesquisarCirurgiaPorID((Integer) c[1]));
            cce.setPaciente(cce.getIdAtendimento().getIdPaciente());
            cce.setProcedimento(cce.getIdAtendimento().getIdProcedimento());
            cce.setFuncao((String) c[5]);
            cce.setTipo((String) c[6]);
            cce.setValor((Double) c[7]);
            //cce.setFuncao((String) c[8]);
            cce.setTipoInternacao((String) c[9]);
            cce.setContaCorrente(cirurgiaEJB.pesquisarContaCorrentePorID((Integer) c[10]));
            cce.setIdFuncao((Integer) c[11]);
            contas.add(cce);
        }
        System.out.println("achou " + contas.size() + " registros!");
        return contas;
    }
    @EJB
    CirurgiaEJB cirurgiaEJB;

    public List<ContaCorrente> listaMovimentoContaCorrentePorMedicoCirurgia(Integer idTerceiro, Integer idCirurgia) {
        Query query = getEm().createQuery("Select c from ContaCorrente c where c.idTerceiro.idTerceiro = :idt and c.idCirurgia.idCirurgia = :idc");
        query.setParameter("idt", idTerceiro);
        query.setParameter("idc", idCirurgia);
        return query.getResultList();
    }

    public boolean retornaSeEAvulso(Integer id) {
//        int qtd = 0;
        boolean retorno = true;
        Query query = getEm().createQuery("Select c.idCirurgia.idTipoInternacao.descricao from ContaCorrente c where c.idSaque = :idc "
                + "GROUP BY  c.idCirurgia.idTipoInternacao.descricao ");
        query.setParameter("idc", id);
        List<String> consulta = query.getResultList();
        if (consulta.isEmpty()) {
            return false;
        } else {
            for (String s : consulta) {
                if (consulta.size() == 1 && s.equals("Internacao")) {
                    retorno = false;
                }
                if (consulta.size() == 1 && !s.equals("Internacao")) {
                    retorno = true;
                }
                if (consulta.size() > 1) {
                    retorno = true;
                }
                System.out.println(s);
            }
            return retorno;
        }
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }
}
