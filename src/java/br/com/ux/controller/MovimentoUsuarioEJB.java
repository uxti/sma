/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.Operacao;
import br.com.ux.model.Repasse;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.UsuarioSessao;
import java.lang.String;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class MovimentoUsuarioEJB extends Conexao {

    UsuarioSessao us = new UsuarioSessao();
    @EJB
    UsuarioEJB uEJB;
    @EJB
    RepasseEJB rEJB;
    @EJB
    CaixaEJB cxEJB;
    @EJB
    ChequeEJB chEJB;

    public void Salvar(Caixa caixa) {
        getEm().merge(caixa);
    }

    public void SalvarPersist(Caixa caixa) {
        getEm().persist(caixa);
    }

    public List<Caixa> listaMovimentoDoDiaPorUsuario(String usuario) {
        Query query = getEm().createQuery("Select c From Caixa c where  c.idUsuario.usuario = :user");
        query.setParameter("user", usuario);
        return query.getResultList();
    }

    public List<Caixa> retornaListaCaixaPorFechamento(Integer idFechamento) {
        Query query = getEm().createQuery("Select c From Caixa c where c.idCirurgia.idCirurgia = :id");
        query.setParameter("id", idFechamento);
        return query.getResultList();
    }

    public List<Caixa> retornaListaCaixaPorFechamentoParaTransferencia(Integer idFechamento) {
        Query query = getEm().createQuery("Select c From Caixa c where c.idCirurgia.idCirurgia = :id and c.idUsuarioDestino is null and c.loteOrigem is null");
        query.setParameter("id", idFechamento);
        return query.getResultList();
    }

    public List<Caixa> listaCaixasPorLote(String loteOrigem) {
        Query query = getEm().createQuery("Select c From Caixa c where c.loteOrigem = :lote AND c.tipoLancamento = 'D'");
        query.setParameter("lote", loteOrigem);
        return query.getResultList();
    }

    public List<Caixa> listarCaixasAgrupadosPorUsuario(Integer usuario, String dtIni, String dtFim) {
        Query query = null;
        String sql = "SELECT c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.num_lote num_lote, "
                + "c.lote_origem,"
                + "c.id_usuario,"
                + "c.id_cheque "
                + "FROM caixa c WHERE c.id_usuario = " + usuario + " and c.historico not like 'Estorno%' and c.situacao is not null and c.lote_origem is not null and c.historico not like 'Saque%' and DATE_FORMAT(c.dt_caixa,'%Y-%m-%d') between '" + dtIni + "' and '" + dtFim + "' GROUP BY c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";
        System.out.println(sql);
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
//            cn.setIdCaixaPai(cp);
            if (c[4] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[4]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query query3 = getEm().createQuery("Select o From Usuario o where o.idUsuario = " + (Integer) c[11]);
                try {
                    cn.setIdUsuario((Usuario) query3.getSingleResult());
                } catch (Exception e) {
                    cn.setIdUsuario(null);
                }
            }
            if (c[12] != null) {
                Query query4 = getEm().createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[12]);
                try {
                    cn.setIdCheque((Cheque) query4.getSingleResult());
                } catch (Exception e) {
                    cn.setIdCheque(null);
                }
            }
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarMovimentoCaixaPorUsuario(Integer usuario, String dtIni, String dtFim) {
        Query query = null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dtAtual = new Date();
        String dataAtual = df.format(dtAtual);
        System.out.println("2.1");
        String sql = "SELECT c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.lote_origem,"
                + "c.id_usuario,"
                + "c.id_cheque "
                + "FROM caixa c WHERE c.id_usuario = " + usuario + " and (c.situacao = 'Aceito' or (c.situacao is null)) and c.historico != 'Saldo Inicial' and DATE_FORMAT(c.dt_caixa,'%Y-%m-%d') between '" + dtIni + "' and '" + dtFim + "' and DATE_FORMAT(c.dt_caixa,'%Y-%m-%d') <= '" + dataAtual + "' GROUP BY c.historico, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY dt_caixa, tipo_lancamento asc";
        System.out.println(sql);
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();
        System.out.println("2.2");

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
            cn.setStatus((String) c[5]);
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            cn.setLoteOrigem((String) c[9]);

////            cn.setIdCaixaPai(cp);
//            if (c[4] != null) {
//                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[4]);
//                cn.setIdOperacao((Operacao) query1.getSingleResult());
//            }
//            if (c[6] != null) {
//                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
//                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
//            }
//
//            if (c[10] != null) {
//                Query query3 = getEm().createQuery("Select o From Usuario o where o.idUsuario = " + (Integer) c[10]);
//                try {
//                    cn.setIdUsuario((Usuario) query3.getSingleResult());
//                } catch (Exception e) {
//                    cn.setIdUsuario(null);
//                }
//            }
//            if (c[11] != null) {
//                Query query4 = getEm().createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[11]);
//                try {
//                    cn.setIdCheque((Cheque) query4.getSingleResult());
//                } catch (Exception e) {
//                    cn.setIdCheque(null);
//                }
//            }
            caixas.add(cn);
        }
        System.out.println("2.3");
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosPorLote(String lotePesquisar) {
        Query query = null;
        String sql = "SELECT c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.num_lote num_lote, "
                + "c.lote_origem,"
                + "c.id_usuario,"
                + "c.id_cheque "
                + "FROM caixa c WHERE c.lote_origem like '%" + lotePesquisar + "%' GROUP BY c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";
        System.out.println(sql);
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
//            cn.setIdCaixaPai(cp);
            if (c[4] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[4]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query query3 = getEm().createQuery("Select o From Usuario o where o.idUsuario = " + (Integer) c[11]);
                try {
                    cn.setIdUsuario((Usuario) query3.getSingleResult());
                } catch (Exception e) {
                    cn.setIdUsuario(null);
                }
            }
            if (c[12] != null) {
                Query query4 = getEm().createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[12]);
                try {
                    cn.setIdCheque((Cheque) query4.getSingleResult());
                } catch (Exception e) {
                    cn.setIdCheque(null);
                }
            }
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosPorFechamentos(String fechamentosPesquisar) {
        Query query = null;
        String sql = "SELECT c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.num_lote num_lote, "
                + "c.lote_origem,"
                + "c.id_usuario,"
                + "c.id_cheque "
                + "FROM caixa c WHERE c.id_cirurgia in (" + fechamentosPesquisar + ") GROUP BY c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";
        System.out.println(sql);
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
//            cn.setIdCaixaPai(cp);
            if (c[4] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[4]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query query3 = getEm().createQuery("Select o From Usuario o where o.idUsuario = " + (Integer) c[11]);
                try {
                    cn.setIdUsuario((Usuario) query3.getSingleResult());
                } catch (Exception e) {
                    cn.setIdUsuario(null);
                }
            }
            if (c[12] != null) {
                Query query4 = getEm().createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[12]);
                try {
                    cn.setIdCheque((Cheque) query4.getSingleResult());
                } catch (Exception e) {
                    cn.setIdCheque(null);
                }
            }
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosTransferenciaPorUsuario(Integer usuario, String dtIni, String dtFim) {
        Query query = null;
        String sql = "SELECT c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.num_lote num_lote, "
                + "c.lote_origem,"
                + "c.id_usuario,"
                + "c.id_cheque "
                + "FROM caixa c WHERE c.id_usuario = " + usuario + " and c.historico not like 'Estorno%' and c.historico not like 'Saque%' and c.tipo_lancamento = 'D' and DATE_FORMAT(c.dt_caixa,'%Y-%m-%d') between '" + dtIni + "' and '" + dtFim + "' GROUP BY c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";
        System.out.println(sql);
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
//            cn.setIdCaixaPai(cp);
//            if (c[4] != null) {
//                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[4]);
//                cn.setIdOperacao((Operacao) query1.getSingleResult());
//            }
            cn.setStatus((String) c[5]);
//            if (c[6] != null) {
//                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
//                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
//            }
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query query3 = getEm().createQuery("Select o From Usuario o where o.idUsuario = " + (Integer) c[11]);
                try {
                    cn.setIdUsuario((Usuario) query3.getSingleResult());
                } catch (Exception e) {
                    cn.setIdUsuario(null);
                }
            }
            if (c[12] != null) {
                Query query4 = getEm().createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[12]);
                try {
                    cn.setIdCheque((Cheque) query4.getSingleResult());
                } catch (Exception e) {
                    cn.setIdCheque(null);
                }
            }
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosPorUsuarioRecebidos(Integer usuario, String dtIni, String dtFim) {
        Query query = null;
        String sql = "SELECT c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.num_lote num_lote, "
                + "c.lote_origem,"
                + "c.id_usuario FROM caixa c WHERE c.id_usuario = " + usuario + " and c.historico not like 'Estorno%' and c.historico not like 'Saque%' and DATE_FORMAT(c.dt_caixa,'%Y-%m-%d') between '" + dtIni + "' and '" + dtFim + "' AND c.situacao = 'Aceito' GROUP BY c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";
        System.out.println(sql);
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
//            cn.setIdCaixaPai(cp);
            if (c[4] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[4]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupados(String dtIni, String dtFim) {
        Query query = null;
        Integer usuario = uEJB.retornaUsuarioRepasseHospital().getIdUsuario();
        String sql = "SELECT c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.num_lote num_lote, "
                + "c.lote_origem,"
                + "c.id_usuario, "
                + "c.id_cheque "
                + "FROM caixa c WHERE c.historico not like 'Estorno%' and c.historico not like 'Saque%' and c.situacao != 'Transferido' and DATE_FORMAT(c.dt_caixa,'%Y-%m-%d') between '" + dtIni + "' and '" + dtFim + "' GROUP BY c.id_usuario, c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";
        System.out.println(sql);
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        System.out.println("aqui");

        List<Caixa> caixas = new ArrayList<>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
//            cn.setIdCaixaPai(cp);
//            if (c[4] != null) {
//                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[4]);
//                cn.setIdOperacao((Operacao) query1.getSingleResult());
//            }
            System.out.println("aqui1");

            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            System.out.println("aqui2");

            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setLoteOrigem((String) c[10]);

            if (c[11] != null) {
                Query query3 = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[11]);
                cn.setIdUsuario((Usuario) query3.getSingleResult());
            }
            System.out.println("aqui3");

            if (c[12] != null) {
                Query query4 = getEm().createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[12]);
                cn.setIdCheque((Cheque) query4.getSingleResult());
            }

            System.out.println("aqui4");

            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosTransferencia(String dtIni, String dtFim) {
        Query query = null;
        Integer usuario = uEJB.retornaUsuarioRepasseHospital().getIdUsuario();
        String sql = "SELECT c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.num_lote num_lote, "
                + "c.lote_origem,"
                + "c.id_usuario, "
                + "c.id_cheque "
                + "FROM caixa c WHERE c.historico not like 'Estorno%' and c.historico not like 'Saque%' and c.situacao != 'Transferido' AND c.situacao = 'Aceito' and DATE_FORMAT(c.dt_caixa,'%Y-%m-%d') between '" + dtIni + "' and '" + dtFim + "' GROUP BY c.id_usuario, c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";
        System.out.println(sql);
        query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        System.out.println("aqui1");
        List<Caixa> caixas = new ArrayList<>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setDtCaixa((Date) c[0]);
            cn.setValor((Double) c[1]);
            cn.setTipoLancamento(c[2].toString().charAt(0));
            cn.setHistorico((String) c[3]);
//            cn.setIdCaixaPai(cp);
//            if (c[4] != null) {
//                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[4]);
//                cn.setIdOperacao((Operacao) query1.getSingleResult());
//            }
            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = em.createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            System.out.println("aqui" + c[6]);
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            cn.setNumLote((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query query3 = em.createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[11]);
                cn.setIdUsuario((Usuario) query3.getSingleResult());
            }
            System.out.println("aqui" + c[11]);
            if (c[12] != null) {
                Query query4 = em.createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[12]);
                cn.setIdCheque((Cheque) query4.getSingleResult());
            }
            System.out.println("aqui" + c[12]);
            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasTransferencia(Integer usuario) {
        Query query = em.createQuery("Select c From Caixa c where c.idUsuario.idUsuario = :user AND c.status = 'Não Baixado' AND c.tipoLancamento = 'C' GROUP BY c.idCirurgia.idCirurgia  ORDER BY c.dtCaixa DESC");
        query.setParameter("user", usuario);
        return query.getResultList();
    }

    public List<Caixa> listarCaixasAgrupadosTransferencia(Integer usuario, String dtIni, String dtFim) {
        System.out.println("Datas: " + dtIni + " - " + dtFim);
        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.id_caixa,"
                + "c.id_cheque "
                + "FROM caixa c WHERE c.id_usuario = " + usuario + " and c.status = 'Não Baixado' and DATE_FORMAT(C.DT_CAIXA,'%Y-%m-%d') between '" + dtIni + "' and '" + dtFim + "' and c.tipo_lancamento = 'C' GROUP BY c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";

        System.out.println(sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            if (c[5] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[5]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[6]);
            if (c[7] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[7]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[8]);
            cn.setLiberado((String) c[9]);
            cn.setIdCaixa((Integer) c[10]);
            if (c[11] != null) {
                Query query3 = getEm().createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[11]);
                cn.setIdCheque((Cheque) query3.getSingleResult());
            }
            caixas.add(cn);
        }

        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosTransferenciaPorPerfil(Integer idPapel, String dtIni, String dtFinal) {
        System.out.println("Datas: " + dtIni + " - " + dtFinal);
        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.id_caixa,"
                + "c.id_cheque "
                + "FROM caixa c, usuario u WHERE c.status = 'Não Baixado' and c.tipo_lancamento = 'C' and u.id_usuario = c.id_usuario and DATE_FORMAT(C.DT_CAIXA,'%Y-%m-%d') BETWEEN '" + dtIni + "' and '" + dtFinal + "' and u.ID_PAPEL = " + idPapel + " GROUP BY c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY num_lote, dt_caixa desc, tipo_lancamento asc";
        System.out.println(sql);

        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            if (c[5] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[5]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[6]);
            if (c[7] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[7]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[8]);
            cn.setLiberado((String) c[9]);
            cn.setIdCaixa((Integer) c[10]);
            if (c[11] != null) {
                Query query3 = getEm().createQuery("Select o From Cheque o where o.idCheque = " + (Integer) c[11]);
                cn.setIdCheque((Cheque) query3.getSingleResult());
            }
            caixas.add(cn);
        }

        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosTransferidos(Integer usuario) {
        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.lote_origem, "
                + "c.id_usuario_destino "
                + "FROM caixa c WHERE c.id_usuario = " + usuario + " and c.situacao = 'Recebido' and c.status = 'Baixado' and c.tipo_lancamento = 'D' GROUP BY c.num_lote, dt_caixa,  id_cirurgia, tipo_lancamento, id_operacao ORDER BY dt_caixa desc";
        //String sql = "select dt_caixa, sum(valor) valor, tipo_lancamento, historico, id_caixa_pai, id_operacao, status, id_cirurgia, numero_parcela, num_lote,  situacao, liberado, lote_origem from caixa where id_usuario = " + usuario + " and dt_caixa between '"+dtIni+"' and '"+dtFim+"' group by dt_caixa,tipo_lancamento, historico, id_caixa_pai, id_operacao, id_cirurgia, numero_parcela, num_lote,situacao, liberado order by dt_caixa , num_lote, tipo_lancamento  asc";
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            if (c[5] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[5]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[6]);
            if (c[7] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[7]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[8]);
            cn.setLiberado((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query q = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[11]);
                cn.setIdUsuarioDestino((Usuario) q.getSingleResult());
            }

            caixas.add(cn);
        }

        return caixas;
    }

    public List<Caixa> listarCaixasAgrupadosTransferidosPorData(Integer usuario, String dtInicial, String dtFinal) {
        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.lote_origem, "
                + "c.id_usuario_destino "
                + "FROM caixa c WHERE c.id_usuario = " + usuario + " and c.situacao = 'Recebido' and c.status = 'Baixado' and c.tipo_lancamento = 'D' and c.dt_caixa between '" + dtInicial + "' and '" + dtFinal + "' GROUP BY c.num_lote, c.lote_origem, c.tipo_lancamento, c.id_operacao ORDER BY dt_caixa desc";
        System.out.println(sql);
        System.out.println("Datas: " + dtInicial + " - " + dtFinal);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();
        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            if (c[5] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[5]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[6]);
            if (c[7] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[7]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[8]);
            cn.setLiberado((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query q = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[11]);
                cn.setIdUsuarioDestino((Usuario) q.getSingleResult());
            }

            caixas.add(cn);
        }

        return caixas;
    }

    public List<Caixa> listarCaixasTransferidos(boolean porUsuario) {
        getEm().getEntityManagerFactory().getCache().evictAll();
        String idUsuario = "";

        if (porUsuario && !uEJB.retornaUsuarioDaSessao().getIdPapel().getDescricao().toLowerCase().contains("admin")) {
            idUsuario = uEJB.retornaUsuarioDaSessao().getIdUsuario().toString();
        } else {
            idUsuario = "%%";
        }

        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.lote_origem, "
                + "c.id_usuario_destino "
                + "FROM caixa c WHERE c.id_usuario LIKE '" + idUsuario + "' and c.situacao = 'Recebido' and c.status = 'Baixado' and c.tipo_lancamento = 'D' GROUP BY c.num_lote, dt_caixa,  id_cirurgia, tipo_lancamento, id_operacao ORDER BY dt_caixa desc";
        //String sql = "select dt_caixa, sum(valor) valor, tipo_lancamento, historico, id_caixa_pai, id_operacao, status, id_cirurgia, numero_parcela, num_lote,  situacao, liberado, lote_origem from caixa where id_usuario = " + usuario + " and dt_caixa between '"+dtIni+"' and '"+dtFim+"' group by dt_caixa,tipo_lancamento, historico, id_caixa_pai, id_operacao, id_cirurgia, numero_parcela, num_lote,situacao, liberado order by dt_caixa , num_lote, tipo_lancamento  asc";
        Query query = getEm().createNativeQuery(sql);
        //query.setFirstResult(primeiro);
        //query.setMaxResults(qtd);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            if (c[5] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[5]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[6]);
            if (c[7] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[7]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[8]);
            cn.setLiberado((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query q = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[11]);
                cn.setIdUsuarioDestino((Usuario) q.getSingleResult());
            }

            caixas.add(cn);
        }
        return caixas;
    }

    public List<Caixa> listarCaixasAgrupados() {
        Integer usuario = uEJB.retornaUsuarioRepasseHospital().getIdUsuario();
        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.id_operacao, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado, "
                + "c.lote_origem, "
                + "c.id_usuario_destino "
                + "FROM caixa c WHERE c.situacao = 'Recebido' and c.status = 'Baixado' and c.tipo_lancamento = 'D' GROUP BY c.id_usuario, c.num_lote, dt_caixa,  id_cirurgia, tipo_lancamento, id_operacao "
                + "ORDER BY dt_caixa desc";
        //String sql = "select dt_caixa, sum(valor) valor, tipo_lancamento, historico, id_caixa_pai, id_operacao, status, id_cirurgia, numero_parcela, num_lote,  situacao, liberado, lote_origem from caixa where id_usuario = " + usuario + " and dt_caixa between '"+dtIni+"' and '"+dtFim+"' group by dt_caixa,tipo_lancamento, historico, id_caixa_pai, id_operacao, id_cirurgia, numero_parcela, num_lote,situacao, liberado order by dt_caixa , num_lote, tipo_lancamento  asc";
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            if (c[5] != null) {
                Query query1 = getEm().createQuery("Select o From Operacao o where o.idOperacao = " + (Integer) c[5]);
                cn.setIdOperacao((Operacao) query1.getSingleResult());
            }
            cn.setStatus((String) c[6]);
            if (c[7] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[7]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[8]);
            cn.setLiberado((String) c[9]);
            cn.setLoteOrigem((String) c[10]);
            if (c[11] != null) {
                Query q = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[11]);
                cn.setIdUsuarioDestino((Usuario) q.getSingleResult());
            }
            caixas.add(cn);
        }
        return caixas;
    }

    public Number contarCaixasRegistro(boolean porUsuario, String clausula) {
        String idUsuario = "";
        if (porUsuario) {
            idUsuario = uEJB.retornaUsuarioDaSessao().getIdUsuario().toString();
        } else {
            idUsuario = "%%";
        }
        getEm().getEntityManagerFactory().getCache().evictAll();
        Query query = getEm().createQuery("Select COUNT(c.idCaixa) From Caixa c WHERE c.idUsuario.idUsuario LIKE '" + idUsuario + "' and "
                + "c.situacao = 'Recebido' and c.status = 'Baixado' and c.tipoLancamento = 'D'");
        return (Number) query.getSingleResult();
    }

    public List<Caixa> listarCaixasTransferencia(String loteOrigem) {
        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado,"
                + "c.id_usuario,"
                + "c.id_usuario_destino,"
                + "c.id_cheque "
                + "FROM caixa c WHERE c.lote_origem = " + loteOrigem + " and c.tipo_lancamento = 'C' GROUP BY c.id_usuario";
        System.out.println(sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            if (c[9] != null) {
                Query query2 = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[9]);
                cn.setIdUsuario((Usuario) query2.getSingleResult());
            }
            if (c[10] != null) {
                Query query2 = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[10]);
                cn.setIdUsuarioDestino((Usuario) query2.getSingleResult());
            }
            if (c[11] != null) {
                Query query2 = getEm().createQuery("Select c From Cheque c where c.idCheque = " + (Integer) c[11]);
                cn.setIdCheque((Cheque) query2.getSingleResult());
            }
            caixas.add(cn);
        }

        return caixas;
    }

    public List<Caixa> listarCaixasTransferenciaDetalhado(String loteOrigem, Integer idUsuario) {
        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado,"
                + "c.id_usuario,"
                + "c.id_usuario_destino,"
                + "c.id_cheque,"
                + "c.id_caixa,"
                + "c.lote_origem"
                + " FROM caixa c WHERE c.lote_origem = " + loteOrigem + " and c.id_usuario = " + idUsuario + " GROUP BY c.id_cheque, c.id_usuario, c.valor";
        System.out.println(sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            if (c[9] != null) {
                Query query2 = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[9]);
                cn.setIdUsuario((Usuario) query2.getSingleResult());
            }
            if (c[10] != null) {
                Query query2 = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[10]);
                cn.setIdUsuarioDestino((Usuario) query2.getSingleResult());
            }
            if (c[11] != null) {
                Query query2 = getEm().createQuery("Select c From Cheque c where c.idCheque = " + (Integer) c[11]);
                cn.setIdCheque((Cheque) query2.getSingleResult());
            }
            cn.setIdCaixa((Integer) c[12]);
            cn.setLoteOrigem((String) c[13]);
            caixas.add(cn);
        }
        return caixas;

    }

    public List<Caixa> listarCaixasTransferenciaDetalhadoPorFechamento(String loteOrigem, Integer idUsuario, Integer idCirurgia) {
        String sql = "SELECT c.num_lote, "
                + "c.dt_caixa, "
                + "sum(c.valor) valor, "
                + "c.tipo_lancamento, "
                + "c.historico historico, "
                + "c.status, "
                + "c.id_cirurgia, "
                + "c.situacao, "
                + "c.liberado,"
                + "c.id_usuario,"
                + "c.id_usuario_destino,"
                + "c.id_cheque,"
                + "c.id_caixa,"
                + "c.lote_origem"
                + " FROM caixa c WHERE c.id_cirurgia = " + idCirurgia + " and c.lote_origem = " + loteOrigem + " and c.id_usuario = " + idUsuario + " GROUP BY c.id_cheque, c.id_usuario, c.valor";
        System.out.println(sql);
        Query query = getEm().createNativeQuery(sql);
        List<Object[]> resultList = query.getResultList();
        List<Caixa> caixas = new ArrayList<Caixa>();

        for (Object[] c : resultList) {
            Caixa cn = new Caixa();
            cn.setNumLote((String) c[0]);
            cn.setDtCaixa((Date) c[1]);
            cn.setValor((Double) c[2]);
            cn.setTipoLancamento(c[3].toString().charAt(0));
            cn.setHistorico((String) c[4]);
//            cn.setIdCaixaPai(cp);
            cn.setStatus((String) c[5]);
            if (c[6] != null) {
                Query query2 = getEm().createQuery("Select o From Cirurgia o where o.idCirurgia = " + (Integer) c[6]);
                cn.setIdCirurgia((Cirurgia) query2.getSingleResult());
            }
            cn.setSituacao((String) c[7]);
            cn.setLiberado((String) c[8]);
            if (c[9] != null) {
                Query query2 = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[9]);
                cn.setIdUsuario((Usuario) query2.getSingleResult());
            }
            if (c[10] != null) {
                Query query2 = getEm().createQuery("Select u From Usuario u where u.idUsuario = " + (Integer) c[10]);
                cn.setIdUsuarioDestino((Usuario) query2.getSingleResult());
            }
            if (c[11] != null) {
                Query query2 = getEm().createQuery("Select c From Cheque c where c.idCheque = " + (Integer) c[11]);
                cn.setIdCheque((Cheque) query2.getSingleResult());
            }
            cn.setIdCaixa((Integer) c[12]);
            cn.setLoteOrigem((String) c[13]);
            caixas.add(cn);
        }
        return caixas;

    }

    public List<Integer> listarCaixasTransferenciaDetalhadoPorLote(String loteOrigem) {
        Query query = getEm().createQuery("SELECT c.idCaixa FROM Caixa c where c.loteOrigem = :loteOrigem and c.tipoLancamento = 'C' GROUP BY c.idUsuario, c.valor, c.idCheque");
        query.setParameter("loteOrigem", loteOrigem);
        return query.getResultList();
    }

    public List<Integer> listarCaixasTransferenciaDetalhado2(String loteOrigem, Integer idUsuario) {
        Query query = getEm().createQuery("SELECT c.idCaixa FROM Caixa c where c.loteOrigem = :loteOrigem and c.idUsuario.idUsuario = :idUsu and c.tipoLancamento = 'C' GROUP BY c.idUsuario, c.idCheque, c.idCaixa");
        query.setParameter("loteOrigem", loteOrigem);
        query.setParameter("idUsu", idUsuario);
        return query.getResultList();
    }

    public List<Integer> listarCaixasTransferenciaMedicos(String loteOrigem, Integer idUsuario) {
        Query query = getEm().createQuery("SELECT c.idCaixa FROM Caixa c where c.loteOrigem = :loteOrigem and c.idUsuario.idUsuario != :idUsu and c.tipoLancamento = 'C' GROUP BY c.idUsuario, c.idCheque, c.idCaixa");
        query.setParameter("loteOrigem", loteOrigem);
        query.setParameter("idUsu", idUsuario);
        return query.getResultList();
    }

    public List<Caixa> listarCaixasTransferenciaMedicosObjeto(String loteOrigem, Integer idUsuario) {
        Query query = getEm().createQuery("SELECT c FROM Caixa c where c.loteOrigem = :loteOrigem and c.idUsuario.idUsuario != :idUsu and c.tipoLancamento = 'C' GROUP BY c.idUsuario, c.idCheque, c.idCaixa");
        query.setParameter("loteOrigem", loteOrigem);
        query.setParameter("idUsu", idUsuario);
        return query.getResultList();
    }

    public List<Integer> listarCaixasTransferenciaPorFechamentoMedicos(List ids, Integer idUsuario) {
        Query query = getEm().createQuery("SELECT c.idCaixa FROM Caixa c where c.idCirurgia.idCirurgia in :ids and c.idUsuario.idUsuario != :idUsu and c.tipoLancamento = 'C' GROUP BY c.idUsuario, c.idCheque, c.idCaixa");
        query.setParameter("ids", ids);
        query.setParameter("idUsu", idUsuario);
        return query.getResultList();
    }

    public List<Caixa> listaMovimentoNaoTransferido(String usuario) {
        Query query = getEm().createQuery("Select c From Caixa c where  c.idUsuario.usuario = :user and c.status = 'Não Baixado' and c.idRepasse.idTerceiro.terceiroMedHosp != TRUE  ");
        query.setParameter("user", usuario);
        return query.getResultList();
    }

    public List<Repasse> listaRepassesPorCaixa(Integer idCaixa) {
        Query query = getEm().createQuery("Select r From Repasse r where r.idCaixa.idCaixa = :idCaixa");
        query.setParameter("idCaixa", idCaixa);
        return query.getResultList();
    }

    public List<Caixa> pesquisaMovimento(Date ini, Date fim, String usuario) {
        Query query = getEm().createQuery("Select c From Caixa c where c.dtCaixa BETWEEN :ini and :fim and c.idUsuario.usuario = :user");
        query.setParameter("ini", ini);
        query.setParameter("fim", fim);
        query.setParameter("user", usuario);
        return query.getResultList();
    }

    public List<Caixa> listaCaixaItensPorLote(String numLote) {
        Query query = getEm().createQuery("Select c From Caixa c where c.loteOrigem = :numLote");
        query.setParameter("numLote", numLote);
        return query.getResultList();
    }

    public List<Caixa> listaCaixaItensRecebidosPorLote(String numLote) {
        Query query = getEm().createQuery("Select c From Caixa c where c.loteOrigem = :numLote and (c.numLote is null or c.numLote = '') and c.situacao = 'Recebido'");
        query.setParameter("numLote", numLote);
        return query.getResultList();
    }

    public void aceitarTransferenciaCaixa(String numLote) {
        Query query = em.createQuery("UPDATE Caixa c set c.situacao = 'Aceito' where c.numLote = :nul");
        query.setParameter("nul", numLote);
        query.executeUpdate();
        em.merge(BCRUtils.criarAuditoria("transferencia", numLote + " aceita.", us.retornaUsuario(), new Date(), "Movimentação"));
    }

    public void atualizarChequeCaixa(Integer idChe, Usuario usu) {
        Query query = getEm().createQuery("UPDATE Cheque c set c.idUsuario = :us where c.idCheque = :id");
        query.setParameter("us", usu);
        query.setParameter("id", idChe);
        query.executeUpdate();
    }

    public void atualizarCheque(Integer idChe, String tipo) {
        Query query = getEm().createQuery("UPDATE Cheque c set c.tipo = :tip where c.idCheque = :id");
        query.setParameter("tip", tipo);
        query.setParameter("id", idChe);
        query.executeUpdate();
    }

    public void recusarTransferenciaCaixa(String numLoteOrigem, Integer idUsuario) {
        List<Repasse> listaRepassePorLote = new ArrayList<>();
        List<Caixa> listaCaixaPorLote = new ArrayList<>();
        listaRepassePorLote = retornaRepassesPorLote(numLoteOrigem);
        listaCaixaPorLote = listaCaixaItensRecebidosPorLote(numLoteOrigem);

        for (Repasse rp : listaRepassePorLote) {
            for (Caixa caixa : listaCaixaPorLote) {
                if (caixa.getIdCheque().getIdCheque().equals(rp.getIdCheque().getIdCheque())) {
                    rp.setIdCaixa(caixa);
                    rEJB.SalvarSimples(rp);
                }
            }
        }

        // Deletar lançamento de debitos referente a transferência.
        String sql1 = "delete from caixa where num_lote = '" + numLoteOrigem + "' and liberado = 'S' and situacao = 'Aguardando Aceite'";
        Query deletarCaixa = getEm().createNativeQuery(sql1);
        deletarCaixa.executeUpdate();
        Query atualizarCreditos = getEm().createQuery("UPDATE Caixa c set c.situacao = '', c.status = 'Não Baixado' where c.loteOrigem = :origem and c.tipoLancamento = 'C'");
        atualizarCreditos.setParameter("origem", numLoteOrigem);
        atualizarCreditos.executeUpdate();
        em.merge(BCRUtils.criarAuditoria("transferencia", numLoteOrigem + " recusada.", us.retornaUsuario(), new Date(), "Movimentação"));
    }

    public void recusarTransferencia(String numLoteOrigem, Integer idUsuario) {
        // Retorna todos os fechamentos deste lote transferido.
        List<Cirurgia> listaFechamentosTransferidos = retornaFechamentosTransferidosPorLote(numLoteOrigem);
        List<Caixa> listaCaixaTransferidos = new ArrayList<>();

        // Retorna todos os caixas referentes aos fechamentos dos lotes transferidos.
        for (Cirurgia c : listaFechamentosTransferidos) {
            listaCaixaTransferidos.addAll(retornaCaixaTransferidosPorFechamento(c.getIdCirurgia()));
        }

        // Atualiza os dados do repasse referente ao caixa que possue os pagamentos vinculados.
        List<Repasse> listaRepassePorLote = retornaRepassesPorLote(numLoteOrigem);
        for (Repasse rp : listaRepassePorLote) {
            for (Caixa caixa : listaCaixaTransferidos) {
                if (caixa.getIdCheque().getIdCheque().equals(rp.getIdCheque().getIdCheque())) {
                    //System.out.println("cx: " + caixa.getIdCaixa() + " no repasse: " + rp.getIdRepasse());
                    rp.setIdCaixa(caixa);
                    rEJB.SalvarSimples(rp);

                }
            }
        }

        try {
            for (Caixa cx : cxEJB.ListarCaixaPorLoteOrigem(numLoteOrigem)) {
                //System.out.println(cx.getIdCaixa());
                cxEJB.Excluir(cx);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

//        // Deletar lançamento de débitos referente a transferência.
//        String sql1 = "delete from caixa where lote_origem = '" + numLoteOrigem + "'";
//        Query deletarCaixa = getEm().createNativeQuery(sql1);
//        deletarCaixa.executeUpdate();
        // Retorna o caixa raiz ao padrão, antes da transferência.
        for (Caixa cx : listaCaixaTransferidos) {
            cx.setSituacao(null);
            cx.setStatus("Não Baixado");
            Salvar(cx);

            Cheque cheque = chEJB.selecionarPorID(cx.getIdCheque().getIdCheque(), null);
            cheque.setIdUsuario(cx.getIdUsuario());
            chEJB.Salvar(cheque, null);
        }
        em.merge(BCRUtils.criarAuditoria("transferencia", numLoteOrigem + " recusada.", us.retornaUsuario(), new Date(), "Movimentação"));
    }

    public List<Repasse> retornaRepassesPorLote(String numLote) {
        Query q = getEm().createQuery("SELECT r FROM Repasse r WHERE r.idCaixa.loteOrigem = :lote");
        q.setParameter("lote", numLote);
        return q.getResultList();
    }

    public List<Caixa> retornaCaixaTransferidosPorFechamento(Integer idFechamento) {
        Query q = getEm().createQuery("SELECT c FROM Caixa c WHERE c.idCirurgia.idCirurgia = :idF and c.situacao = 'Transferido' and c.status = 'Baixado'");
        q.setParameter("idF", idFechamento);
        return q.getResultList();
    }

    public List<Cirurgia> retornaFechamentosTransferidosPorLote(String loteOrigem) {
        Query q = getEm().createQuery("SELECT DISTINCT c.idCirurgia FROM Caixa c WHERE c.loteOrigem = :lote and c.situacao = 'Recebido' and c.status = 'Baixado'");
        q.setParameter("lote", loteOrigem);
        return q.getResultList();
    }

    public Caixa saldoInicial(Integer usu) {
        Query q = em.createQuery("SELECT c FROM Caixa c WHERE c.tipoLancamento = 'C' and c.idUsuario.idUsuario = :us and c.historico = 'Saldo Inicial' and c.status = 'Baixado'");
        q.setParameter("us", usu);
        if (q.getResultList().isEmpty()) {
            return null;
        } else {
            return (Caixa) q.getSingleResult();
        }

    }

    public List<Caixa> listarCaixaPorNumeroLote(String lote) {
        Query q = em.createQuery("SELECT c FROM Caixa c WHERE c.numLote = :lot ");
        q.setParameter("lot", lote);
        return q.getResultList();
    }

}
