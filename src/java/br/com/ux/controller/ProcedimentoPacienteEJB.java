/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.controller.integracao.BuscaDadosMySQLEJB;
import br.com.ux.controller.integracao.BuscaDadosTasyEJB;
import br.com.ux.controller.integracao.SincronizacaoEJB;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.Terceiro;
import br.com.ux.model.tasy.ContaPaciente;
import br.com.ux.model.tasy.ProcedimentoPaciente;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class ProcedimentoPacienteEJB extends Conexao {

    @EJB
    ContaPacienteEJB cpEJB;

    @EJB
    CirurgiaEJB cEJB;

    @EJB
    BuscaDadosMySQLEJB bdmEJB;

    @EJB
    BuscaDadosTasyEJB bdTEJB;

    @EJB
    SincronizacaoEJB syncEJB;

    public void Salvar(ProcedimentoPaciente pp, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(pp);
        em.flush();
        em.refresh(pp);
        //em.merge(BCRUtils.criarAuditoria("insert", "Procedimento Paciente: " + pp.getIdProcedimentoPaciente(), usuario, new Date(), "Procedimento Paciente"));
    }

    public void Atualizar(ProcedimentoPaciente pp, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(pp);
        //em.merge(BCRUtils.criarAuditoria("update", "Procedimento: Paciente: " + pp.getIdProcedimentoPaciente(), usuario, new Date(), "Procedimento Paciente"));
    }

    public void Excluir(ProcedimentoPaciente pp) {
        pp = em.getReference(ProcedimentoPaciente.class, pp.getIdProcedimentoPaciente());
        em.remove(pp);
    }

    public void deletarProcedimentoPacientePorContaPaciente(Integer idContaPaciente) {
        Query query = em.createNativeQuery("Delete from procedimento_paciente where id_conta_paciente = " + idContaPaciente);
        query.executeUpdate();
    }

    public List<ProcedimentoPaciente> listarAtendimentoPaciente() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select p From ProcedimentoPaciente p ORDER BY p.idProcedimentoPaciente").getResultList();
    }

    public ProcedimentoPaciente selecionarPorProcedimentoAtendimentoTasy(Integer id) {
        Query query = em.createQuery("SELECT p FROM ProcedimentoPaciente p WHERE p.idProcedimentoPacienteTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (ProcedimentoPaciente) query.getSingleResult();
        } catch (Exception e) {
            //System.out.println("Erro: " + e.getMessage() + " em: " + id);
            return null;
        }
    }

    public boolean verificaSeProcedimentoPacienteFoiExcluido(Integer idProcedimentoPaciente) {
        try {
            return bdTEJB.listaProcedimentosPacientesTasyPorID(idProcedimentoPaciente).isEmpty();
        } catch (Exception e) {
            return false;
        }
    }

    public List<ProcedimentoPaciente> selecionarListaDeProcedimentosPorContaPaciente(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT p FROM ProcedimentoPaciente p WHERE p.idContaPaciente.idContaPaciente = :id");
        query.setParameter("id", id);
        try {
            return query.getResultList();
        } catch (Exception e) {
            //System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }

    public void VerificarProcedimentoPacienteTasy(Object[] c, LogIntegracao logPai) {
        ProcedimentoPaciente ap;
        Date dtInicio = new Date();

        try {
            if (c[0] != null && c[2] != null && c[4] != null) {
                ap = selecionarPorProcedimentoAtendimentoTasy(((BigDecimal) c[0]).intValue());
                if (ap == null) {
                    // Verifica o numero da conta se é nulo
                    if (c[2] != null) {
                        cadastrarProcedimentoPaciente(c, dtInicio, logPai);
                    }
                } else if (((BigDecimal) c[3]).intValue() > 0) {
                    try {
                        if (c[2] != null) {
                            if (cpEJB.selecionarPorNumeroContaTasy(((BigDecimal) c[2]).intValue()) != null) {
                                ap.setIdContaPaciente(cpEJB.selecionarPorNumeroContaTasy(((BigDecimal) c[2]).intValue()));
                            }
                        }

                        if (((BigDecimal) c[3]).intValue() != 0) {
                            ap.setIdServico(bdmEJB.selecionarPorIDProcedimentoTasy(((BigDecimal) c[3]).intValue()));
                        } else {
                            ap.setIdServico(null);
                        }
                        if (c[4] != null) {
                            ap.setIdMedico(bdmEJB.selecionarPorIDMedicoTasy(Integer.parseInt((String) c[4])));
                        } else {
                            ap.setIdMedico(null);
                        }

                        ap.setDtCadastro((Date) c[5]);
                        ap.setDtAtualizacao((Date) c[6]);
                        ap.setQtdProcedimento(((BigDecimal) c[7]).doubleValue());
                        ap.setValorProcedimento(((BigDecimal) c[8]).doubleValue());
                        //ap.setValorMedico(((BigDecimal) c[9]).doubleValue());
                        if (c[10] != null) {
                            ap.setCdMotivoExcConta(((BigDecimal) c[10]).intValue());
                        } else {
                            ap.setCdMotivoExcConta(null);
                        }

                        if (c[11] != null) {
                            ap.setDtLaudo((Date) c[11]);
                        } else {
                            ap.setDtLaudo(null);
                        }

                        if (c[12] != null) {
                            ap.setIdMedicoLaudo(bdmEJB.selecionarPorIDMedicoTasy(Integer.parseInt((String) c[12])));
                        } else {
                            ap.setIdMedicoLaudo(null);
                        }

                        if (c[13] != null) {
                            ap.setNrSeqProcPacote(((BigDecimal) c[13]).intValue());
                        } else {
                            ap.setNrSeqProcPacote(null);
                        }
                        if (cpEJB.selecionarPorNumeroContaTasy(((BigDecimal) c[2]).intValue()) != null) {
                            Atualizar(ap, "sync");
                        }

                        if (ap.getIdMedico() == null) {
                            Excluir(ap);
                        }
                        //em.merge(BCRUtils.gerarLogIntegracao("Procedimento " + ((BigDecimal) c[0]).toPlainString() + " atualizado.", "cadastrarProcedimentoPaciente", dtInicio, new Date(), logPai));
                    } catch (NullPointerException e) {
                        if (cpEJB.selecionarPorNumeroContaTasy(((BigDecimal) c[2]).intValue()) != null) {
                            Atualizar(ap, "sync");
                        }

                        if (ap.getIdMedico() == null) {
                            Excluir(ap);
                        }
                        //em.merge(BCRUtils.gerarLogIntegracao("Procedimento " + ((BigDecimal) c[0]).toPlainString() + " atualizado.", "cadastrarProcedimentoPaciente", dtInicio, new Date(), logPai));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Erro ao sincronizar Procedimentos Pacientes.");
            System.out.println(e);

        }

    }

    public void VincularListaProcedimentoPacienteTasy(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {
            //System.out.println("Proc: " + ((BigDecimal) c[0]).intValue());
            VerificarProcedimentoPacienteTasy(c, logPai);
        }
    }

    public void cadastrarProcedimentoPaciente(Object[] c, Date dtInicio, LogIntegracao logPai) {
        try {
            //System.out.println("Cadastrando: " + ((BigDecimal) c[0]).intValue());
            if (c[3] != null && c[2] != null && c[4] != null) {
                ProcedimentoPaciente ap = new ProcedimentoPaciente();
                ap.setIdProcedimentoPacienteTasy(((BigDecimal) c[0]).intValue());
                // ap.setIdAtendimentoPaciente(((BigDecimal) c[1]).intValue()));

                if (c[2] != null) {
                    if (((BigDecimal) c[2]).intValue() != 0) {
                        ap.setIdContaPaciente(cpEJB.selecionarPorNumeroContaTasy(((BigDecimal) c[2]).intValue()));
                    }
                }
                if (((BigDecimal) c[3]).intValue() != 0) {
                    ap.setIdServico(bdmEJB.selecionarPorIDProcedimentoTasy(((BigDecimal) c[3]).intValue()));
                }
                if (c[4] != null) {
                    ap.setIdMedico(bdmEJB.selecionarPorIDMedicoTasy(Integer.parseInt((String) c[4])));
                }
                ap.setDtCadastro((Date) c[5]);
                ap.setDtAtualizacao((Date) c[6]);
                ap.setQtdProcedimento(((BigDecimal) c[7]).doubleValue());
                ap.setValorProcedimento(((BigDecimal) c[8]).doubleValue());
                ap.setValorMedico(((BigDecimal) c[9]).doubleValue());
                if (c[10] != null) {
                    ap.setCdMotivoExcConta(((BigDecimal) c[10]).intValue());
                }
                if (c[11] != null) {
                    ap.setDtLaudo((Date) c[11]);
                }
                if (c[12] != null) {
                    ap.setIdMedicoLaudo(bdmEJB.selecionarPorIDMedicoTasy(Integer.parseInt((String) c[12])));
                }
                if (c[13] != null) {
                    ap.setNrSeqProcPacote(((BigDecimal) c[13]).intValue());
                } else {
                    ap.setNrSeqProcPacote(null);
                }
                //System.out.println("Conta: " + ap.getIdContaPaciente());
                //System.out.println("Serviço: " + ap.getIdServico());

                if (ap.getIdContaPaciente() != null && ap.getIdServico() != null) {
                    //System.out.println("Salvando: " + ((BigDecimal) c[0]).intValue());
                    Salvar(ap, "sync");
                    //em.merge(BCRUtils.gerarLogIntegracao("Procedimento " + ((BigDecimal) c[0]).toPlainString() + " cadastrado.", "cadastrarProcedimentoPaciente", dtInicio, new Date(), logPai));
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar Procedimento " + ((BigDecimal) c[0]).toPlainString() + ". " + e.getMessage(), "cadastrarProcedimentoPaciente", null, null, logPai));
        }
    }

}
