/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Banco;
import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.FiltrosRepasse;
import br.com.ux.util.Parametro;
import br.com.ux.util.UsuarioSessao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class RepasseEJB extends Conexao {

    UsuarioSessao us = new UsuarioSessao();

    public void Salvar(Repasse repasse) {
        if (repasse.getIdRepasse() == null) {
            getEm().persist(repasse);
            getEm().flush();
            getEm().refresh(repasse);
            getEm().merge(BCRUtils.criarAuditoria("insert", "Repasse: " + repasse.getIdCirurgia().getIdCirurgia() + " - " + repasse.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Repasse"));
        } else {
            getEm().merge(repasse);
        }
    }

    public void SalvarSimples(Repasse repasse) {
        em.merge(repasse);
    }
    
    public void Excluir(Repasse repasse) {
        repasse = em.getReference(Repasse.class, repasse.getIdRepasse());
        em.remove(repasse);
    }

    public void SalvarAvulsoComAuditoria(Repasse repasse) {
        if (repasse.getIdRepasse() == null) {
            getEm().persist(repasse);
            getEm().flush();
            getEm().refresh(repasse);
            getEm().merge(BCRUtils.criarAuditoria("insert", "Repasse avulso referente ao cheque nº: " + repasse.getIdCheque().getNumeroCheque() + " - " + repasse.getIdTerceiro().getNome() == null ? repasse.getIdTerceiro().getNomeFantasia() : repasse.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Repasse"));
        } else {
            getEm().merge(repasse);
        }
    }

    public void ExcluirRepasseRepetidoAposEstorno(Repasse re) {
        Query query = getEm().createQuery("Delete from Repasse r where r.numeroParcela= :parc and r.idCirurgia.idCirurgia = :cir and r.idTerceiro.idTerceiro = :ter and r.observacao LIKE '%Novo documento gerado do pagamento parcial%'");
        query.setParameter("parc", re.getNumeroParcela());
        query.setParameter("cir", re.getIdCirurgia().getIdCirurgia());
        query.setParameter("ter", re.getIdTerceiro().getIdTerceiro());
        query.executeUpdate();
    }

    

    public void excluirRepasseEContasCorrente(Repasse re) {
        //Excluir as contas correntes vinculadas ao repasse
        Query query2 = em.createQuery("Delete from ContaCorrente cc where cc.idRepasse.idRepasse = :id and cc.idTerceiro.idTerceiro = :ter");
        query2.setParameter("id", re.getIdRepasse());
        query2.setParameter("ter", re.getIdTerceiro().getIdTerceiro());
        query2.executeUpdate();

        Query query = em.createQuery("Delete from Repasse r where r.idRepasse = :id and r.idTerceiro.idTerceiro = :ter");
        query.setParameter("id", re.getIdRepasse());
        query.setParameter("ter", re.getIdTerceiro().getIdTerceiro());
        query.executeUpdate();
    }

    public void ExcluirRepassesRepetido(Repasse re) {
        Query query = getEm().createQuery("Delete from Repasse r where r.idCirurgia.idCirurgia = :cir and r.idTerceiro.idTerceiro = :ter AND r.idCaixa.idCaixa IS NULL");
        query.setParameter("cir", re.getIdCirurgia().getIdCirurgia());
        query.setParameter("ter", re.getIdTerceiro().getIdTerceiro());
        query.executeUpdate();
    }

    public void SalvarRepasses(List<Repasse> lista) {
        for (Repasse repasse : lista) {
            getEm().merge(repasse);
            if (repasse.getIdRepasse() != null) {
                getEm().merge(BCRUtils.criarAuditoria("update", "Repasse: " + repasse.getIdCirurgia().getIdCirurgia() + " - " + repasse.getIdCirurgia().getIdProcedimento().getDescricao() + " - " + repasse.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Repasse"));
            } else {
                getEm().merge(BCRUtils.criarAuditoria("insert", "Repasse: " + repasse.getIdCirurgia().getIdCirurgia() + " - " + repasse.getIdCirurgia().getIdProcedimento().getDescricao() + " - " + repasse.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Repasse"));
            }
        }
    }

    public void SalvarRepassesSimples(List<Repasse> lista) {
        for (Repasse repasse : lista) {
            getEm().merge(repasse);
        }
    }

    public List<Repasse> listaRepasses() {
        return getEm().createQuery("Select r From Repasse r GROUP BY r").getResultList();
    }

    public Paciente pesquisarPaciente(Integer id) {
        Query query = getEm().createQuery("Select p From Paciente p where p.idPaciente = :id");
        query.setParameter("id", id);
        return (Paciente) query.getSingleResult();
    }

    public Cheque pesquisarCheque(Integer id) {
        Query query = getEm().createQuery("Select p From Cheque p where p.idCheque= :id");
        query.setParameter("id", id);
        return (Cheque) query.getSingleResult();
    }

    public Cirurgia pesquisarCirurgia(Integer id) {
        Query query = getEm().createQuery("Select c From Cirurgia c where c.idCirurgia = :id");
        query.setParameter("id", id);
        return (Cirurgia) query.getSingleResult();
    }

    public List<Repasse> pesquisaRepassePorCirurgiaEParcela(Integer idCirurgia, Integer parcela) {
        Query query = getEm().createQuery("Select r From Repasse r where r.idCirurgia.idCirurgia = :cir and r.numeroParcela = :par and r.dtRecebimento IS NULL");
        query.setParameter("cir", idCirurgia);
        query.setParameter("par", parcela);
        return query.getResultList();
    }

    public List<Repasse> pesquisaRepassePorCheque(Integer idCheque) {
        Query query = getEm().createQuery("Select r From Repasse r where r.idCheque.idCheque = :idCheque and r.dtRecebimento IS NULL");
        query.setParameter("idCheque", idCheque);
        return query.getResultList();
    }

    public List<Repasse> pesquisaRepasseHospitalaresPorCheque(Integer idCheque) {
        Query query = getEm().createQuery("Select r From Repasse r where r.idCheque.idCheque = :idCheque and r.idTerceiro.terceiroMedHosp = TRUE");
        query.setParameter("idCheque", idCheque);
        return query.getResultList();
    }

    public Repasse pesquisaRepassePorCirurgiaEParcelaSingle(Integer idCirurgia, Integer parcela, Integer idTerceiro) {
        Query query = getEm().createQuery("Select r From Repasse r where r.idCirurgia.idCirurgia = :cir and r.numeroParcela = :par and r.idTerceiro.idTerceiro = :idTer and r.dtRecebimento IS NULL");
        query.setParameter("cir", idCirurgia);
        query.setParameter("par", parcela);
        query.setParameter("idTer", idTerceiro);
        return (Repasse) query.getSingleResult();
    }

    public Long contarRecebimentosVencidos() {
        return (Long) getEm().createQuery("SELECT count(r.idRepasse) From Repasse r WHERE r.dtVencimento < CURRENT_DATE and r.status = 'A'").getSingleResult();
    }

    public Long contarRecebimentosVencendoHoje() {
        return (Long) getEm().createQuery("SELECT count(r.idRepasse) From Repasse r WHERE r.dtVencimento = CURRENT_DATE and r.status = 'A'").getSingleResult();
    }

    public Long contarRecebimentos() {
        return (Long) getEm().createQuery("SELECT count(r.idRepasse) From Repasse r").getSingleResult();
    }

    public Double totalAReceber() {
        return (Double) getEm().createQuery("select SUM(r.valor) from Repasse r where r.dtVencimento <= CURRENT_DATE and r.status = 'A'").getSingleResult();
    }

    public List<Repasse> listaRepassesRecebidos() {
        return getEm().createQuery("Select r From Repasse r, Caixa c where r.idRepasse = c.idRepasse.idRepasse").getResultList();
    }

    public void ExcluirCaixaEstorno(Integer idRepasse) {
        Query query = getEm().createQuery("Delete from Caixa c where c.idRepasse.idRepasse = :id ");
        query.setParameter("id", idRepasse);

        Integer i = query.executeUpdate();
        System.out.println("Exclusão do caixa extorno " + i);

    }

    public void ExcluirContaCorrenteEstorno(Integer idRepasse) {
        Query query = getEm().createQuery("Delete from ContaCorrente c where c.idRepasse.idRepasse = :id ");
        query.setParameter("id", idRepasse);
        Integer i = query.executeUpdate();
        System.out.println("Exlusao da conta corrente " + i);

    }

    public List<Repasse> pesquisarRepasses(FiltrosRepasse filtros) {
        Query query = null;
        if (filtros.getIdRepasse() == null && filtros.getIdCirurgia() == null && filtros.getIdPaciente() == null) {
            query = getEm().createQuery("Select r From Repasse r");
        } else if ((filtros.getIdRepasse() == null) && (filtros.getIdCirurgia() == null) && (filtros.getIdPaciente() != null)) {
            query = getEm().createQuery("Select r From Repasse r where r.idPaciente.idPaciente = :idPac");
            query.setParameter("idPac", filtros.getIdPaciente());
        } else if (filtros.getIdRepasse() == null && filtros.getIdCirurgia() != null && filtros.getIdPaciente() != null) {
            query = getEm().createQuery("Select r From Repasse r where r.idPaciente.idPaciente = :idPac and r.idCirurgia.idCirurgia = :idCir");
            query.setParameter("idPac", filtros.getIdPaciente());
            query.setParameter("idCir", filtros.getIdCirurgia());
        } else if (filtros.getIdRepasse() != null && filtros.getIdCirurgia() != null && filtros.getIdPaciente() != null) {
            query = getEm().createQuery("Select r From Repasse r where r.idPaciente.idPaciente = :idPac and r.idCirurgia.idCirurgia = :idCir and r.idRepasse = :idRep");
            query.setParameter("idPac", filtros.getIdPaciente());
            query.setParameter("idCir", filtros.getIdCirurgia());
            query.setParameter("idRep", filtros.getIdRepasse());
        } else if (filtros.getIdRepasse() == null && filtros.getIdCirurgia() != null && filtros.getIdPaciente() == null) {
            query = getEm().createQuery("Select r From Repasse r where r.idCirurgia.idCirurgia = :idCir ");
            query.setParameter("idCir", filtros.getIdCirurgia());
        } else if (filtros.getIdRepasse() != null && filtros.getIdCirurgia() != null && filtros.getIdPaciente() == null) {
            query = getEm().createQuery("Select r From Repasse r where r.idCirurgia.idCirurgia = :idCir  and r.idRepasse = :idRep");
            query.setParameter("idCir", filtros.getIdCirurgia());
            query.setParameter("idRep", filtros.getIdRepasse());
        } else if (filtros.getIdRepasse() != null && filtros.getIdCirurgia() == null && filtros.getIdPaciente() == null) {
            query = getEm().createQuery("Select r From Repasse r where r.idRepasse = :idRep");
            query.setParameter("idRep", filtros.getIdRepasse());
        }
        return query.getResultList();
    }

    public void ExcluirContaCorrentestorno(ContaCorrente contaCorrente) {
        contaCorrente = getEm().getReference(ContaCorrente.class, contaCorrente.getIdCaixa());
        getEm().remove(contaCorrente);
    }

    public Repasse selecionarPorID(Integer id) {
        Query query = getEm().createQuery("Select r From Repasse r where r.idRepasse = :id");
        query.setParameter("id", id);
        return (Repasse) query.getSingleResult();
    }

    public Banco selecionarBancoPorID(Integer id) {
        Query query = getEm().createQuery("Select b From Banco b where b.idBanco = :id");
        query.setParameter("id", id);
        return (Banco) query.getSingleResult();
    }

    public List<Repasse> retornaListaTipada(FiltrosRepasse filtrosRepasse) {
        List<Parametro> listaParametros = new ArrayList<Parametro>();
        if (filtrosRepasse.getIdCirurgia() != null) {
            Parametro p = new Parametro();
            p.setCampo("r.idCirurgia.idCirurgia");
            p.setParametro(":cir");
            p.setValorParametroPassado(filtrosRepasse.getIdCirurgia().toString());
            listaParametros.add(p);
        }
        if (filtrosRepasse.getIdPaciente() != null) {
            Parametro p = new Parametro();
            p.setCampo("r.idPaciente.idPaciente");
            p.setParametro(":pac");
            p.setValorParametroPassado(filtrosRepasse.getIdPaciente().toString());
            listaParametros.add(p);
        }
        if (filtrosRepasse.getIdRepasse() != null) {
            Parametro p = new Parametro();
            p.setCampo("r.idRepasse");
            p.setParametro(":rep");
            p.setValorParametroPassado(filtrosRepasse.getIdRepasse().toString());
            listaParametros.add(p);
        }
        String sql = BCRUtils.generateCreateQueryBcr("Select r From Repasse r where r.dtRecebimento is not null and r.status = 'F' ", listaParametros);
        Query qry = getEm().createQuery(sql);
        for (Parametro o : listaParametros) {
            qry.setParameter(o.getParametro().replace(":", ""), Integer.parseInt(o.getValorParametroPassado()));
        }
        return qry.getResultList();
    }

    public List<Repasse> pesquisarRepassePorPacienteCirurgiaParcelaDtRecebimento(Integer cir, Integer parc) {
        Query query = getEm().createQuery("Select r From Repasse r where   r.idCirurgia.idCirurgia = :cir and r.numeroParcela = :parc and r.dtRecebimento IS NOT NULL ");
        query.setParameter("parc", parc);
        query.setParameter("cir", cir);
        System.out.println("Quantidade de retornos " + query.getResultList().size());
        return query.getResultList();
    }

    public List<Object[]> pesquisarRepasseAgrupadosPorCirurgia(Integer cir) {
        Query query = getEm().createQuery("Select SUM(r.valorDocumento), r.idTerceiro.idTerceiro, r.idCheque From Repasse r where r.idCirurgia.idCirurgia = :cir GROUP BY r.idTerceiro.idTerceiro ORDER BY r.idCheque.idCheque");
        query.setParameter("cir", cir);
        return query.getResultList();
    }
    
    public List<Repasse> retornaRepassesPorFechamento(Integer idFechamento) {
        Query query = getEm().createQuery("Select r from Repasse r where r.idCirurgia.idCirurgia = :id");
        query.setParameter("id", idFechamento);
        return query.getResultList();
    }

    public void atualizarRepasse(Integer id, Integer idCir) {
        Query query = getEm().createQuery("UPDATE Repasse r SET r.idCaixa = :id WHERE r.idCirurgia.idCirurgia = :idCir");
        query.setParameter("id", id);
        query.setParameter("idCir", idCir);
        query.executeUpdate();
    }

    public List<Repasse> retornaRepassesPorCaixa(Integer idCaixa) {
        Query query = getEm().createQuery("Select r from Repasse r where r.idCaixa.idCaixa = :id");
        query.setParameter("id", idCaixa);
        return query.getResultList();
    }
}
