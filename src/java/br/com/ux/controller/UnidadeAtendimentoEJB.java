/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.model.UnidadeAtendimento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author BrunoS
 */
@Stateless
public class UnidadeAtendimentoEJB extends Conexao implements Serializable {

    public void Salvar(UnidadeAtendimento unidadeAtendimento, String usuario) {
        if (unidadeAtendimento.getIdUnidade() != null) {
             getEm().merge(unidadeAtendimento);
             getEm().merge(BCRUtils.criarAuditoria("update", unidadeAtendimento.getDescricao(), usuario, new Date(), "Unidade Atendimento"));
        } else {
             getEm().merge(unidadeAtendimento);
             getEm().merge(BCRUtils.criarAuditoria("insert", unidadeAtendimento.getDescricao(), usuario, new Date(), "Unidade Atendimento"));
        }
    }

    public void Excluir(UnidadeAtendimento unidadeAtendimento, String usuario) {
        unidadeAtendimento =  getEm().getReference(UnidadeAtendimento.class, unidadeAtendimento.getIdUnidade());
         getEm().remove(unidadeAtendimento);
         getEm().merge(BCRUtils.criarAuditoria("delete", unidadeAtendimento.getDescricao(), usuario, new Date(), "Unidade Atendimento"));
    }

    public List<UnidadeAtendimento> listarUnidadesAtendimento() {
        return  getEm().createQuery("Select ut From UnidadeAtendimento ut WHERE ut.inativo = FALSE").getResultList();

    }

    public UnidadeAtendimento selecionarPorID(Integer id, String usuario) {

        Query query =  getEm().createQuery("Select ut From UnidadeAtendimento ut where ut.idUnidade = :id");
        query.setParameter("id", id);
        return (UnidadeAtendimento) query.getSingleResult();
    }

    public List<UnidadeAtendimento> listarUndAtendimentoLazyMode(int primeiro, int qtd) {
        Query query =  getEm().createQuery("Select u from UnidadeAtendimento u WHERE u.inativo = FALSE");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<UnidadeAtendimento> listarUndAtendimentoLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select u from UnidadeAtendimento u " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarUndAtendimentoRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(u.idUnidade) From UnidadeAtendimento u " + clausula);
        return (Number) query.getSingleResult();
    }
}
