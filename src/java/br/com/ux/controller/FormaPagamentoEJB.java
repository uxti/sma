/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.FormaPagamento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class FormaPagamentoEJB extends Conexao {

    public void Salvar(FormaPagamento formaPagamento, String usuario) {
        if (formaPagamento.getIdForma() == null) {
             getEm().merge(formaPagamento);
             getEm().merge(BCRUtils.criarAuditoria("insert", formaPagamento.getDescricao(), usuario, new Date(), "Forma Pagamento"));
        } else {
             getEm().merge(formaPagamento);
             getEm().merge(BCRUtils.criarAuditoria("update", formaPagamento.getDescricao(), usuario, new Date(), "Forma Pagamento"));
        }
    }

    public void Excluir(FormaPagamento formaPagamento, String usuario) {
        formaPagamento =  getEm().getReference(FormaPagamento.class, formaPagamento.getIdForma());
         getEm().remove(formaPagamento);
         getEm().merge(BCRUtils.criarAuditoria("delete", formaPagamento.getDescricao(), usuario, new Date(), "Forma Pagamento"));        
    }

    public List<FormaPagamento> listaFormasPagamento() {
        return  getEm().createQuery("Select p From FormaPagamento p WHERE p.inativo = TRUE").getResultList();
    }

    public FormaPagamento selecionarPorID(Integer id, String usuario) {
        Query query =  getEm().createQuery("Select p From FormaPagamento p where p.idForma = :id");
        query.setParameter("id", id);
        return (FormaPagamento) query.getSingleResult();
    }

    public FormaPagamento selecionarPadrao() {
        Query query =  em.createQuery("Select p From FormaPagamento p where p.padrao = 'S'");
        try {
            return (FormaPagamento) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        
    }

    public List<FormaPagamento> listarFormaPagtoLazyMode(int primeiro, int qtd) {
        Query query =  getEm().createQuery("Select f From FormaPagamento f WHERE f.inativo = TRUE");
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<FormaPagamento> listarFormaPagtoLazyModeWhere(int primeiro, int qtd, String clausula) {
        Query query =  getEm().createQuery("Select f From FormaPagamento f " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarFormaPagtoRegistro(String clausula) {
        Query query =  getEm().createQuery("Select COUNT(f.idForma) From FormaPagamento f " + clausula);
        return (Number) query.getSingleResult();
    }
}
