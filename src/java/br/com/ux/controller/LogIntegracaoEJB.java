/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.LogIntegracao;
import br.com.ux.util.Conexao;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Charles
 */
@Stateless
public class LogIntegracaoEJB extends Conexao {

    public void Salvar(LogIntegracao log) {
        // em.getEntityManagerFactory().getCache().evictAll();
//        em.persist(log);
//        em.flush();
//        em.refresh(log);
    }
    
    public void Atualizar(LogIntegracao log) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(log);
    }
    
    public List<LogIntegracao> listarLogs() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select l From LogIntegracao l ORDER BY l.dtFim, l.metodo").getResultList();
    }
}
