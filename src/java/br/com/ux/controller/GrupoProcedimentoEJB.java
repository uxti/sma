/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.tasy.GrupoProcedimentoCbhpm;
import br.com.ux.model.LogIntegracao;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class GrupoProcedimentoEJB extends Conexao {
    
    @EJB
    EspecialidadeProcedimentoEJB eEJB;

    public void Salvar(GrupoProcedimentoCbhpm gp, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(gp);
        em.flush();
        em.refresh(gp);
        em.merge(BCRUtils.criarAuditoria("insert", "Nome: " + gp.getDescricao(), usuario, new Date(), "Grupo de Procedimentos"));
    }

    public void Atualizar(GrupoProcedimentoCbhpm gp, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(gp);
        em.merge(BCRUtils.criarAuditoria("update", "Nome: " + gp.getDescricao(), usuario, new Date(), "Grupo de Procedimentos"));
    }
    
    public GrupoProcedimentoCbhpm selecionarPorIDGrupoProcedimentoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.idGrupoProcedimentoTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (GrupoProcedimentoCbhpm) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }
    
    public GrupoProcedimentoCbhpm retornaGrupoProcedimentoPorID(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.idGrupoProcedimentoCbhpm = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (GrupoProcedimentoCbhpm) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }
    
    public void VincularGrupoProcedimentoTasy(Object[] c, LogIntegracao logPai) {
        GrupoProcedimentoCbhpm gp = new GrupoProcedimentoCbhpm();
        Date dtInicio = new Date();
        if (c[0] != null) {
            gp = selecionarPorIDGrupoProcedimentoTasy(((BigDecimal) c[0]).intValue());
        }

        if (gp != null) {
            if (gp.getIdGrupoProcedimentoTasy() == null) {
                try {
                    // Cria o vinculo atraves do ID_ESPECIALIDADE_PROCEDIMENTO_TASY
                    gp.setIdGrupoProcedimentoTasy(((BigDecimal) c[0]).intValue());
                    if (c[2] != null) {
                        gp.setIdEspecialidadeProcedimento(eEJB.selecionarPorIDEspecialidadeProcedimentoTasy(((BigDecimal) c[2]).intValue()));
                    }                    
                    Salvar(gp, "sync");
                    //em.merge(BCRUtils.gerarLogIntegracao(gp.getDescricao() + " vinculado.", "VincularGrupoProcedimentoTasy", dtInicio, new Date(), logPai));
                } catch (Exception e) {
                    System.out.println(e);
                    //em.merge(BCRUtils.gerarLogIntegracao("Erro ao vincular " + (String) c[1] + ". " + e.getMessage(), "VincularCboSaudeTasy", null, null, logPai));
                }
            }
        } else {
            // Se ele não encontrar a Especialidade Procedimento no SMA, salva um novo cadastro.
            cadastrarGrupoProcedimento(c, dtInicio, logPai);
        }
    }
    
    public void VincularListaGrupoProcedimentoTasy(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {
            VincularGrupoProcedimentoTasy(c, logPai);
        }
    }

    public void cadastrarGrupoProcedimento(Object[] c, Date dtInicio, LogIntegracao logPai) {
        try {
            GrupoProcedimentoCbhpm gp = new GrupoProcedimentoCbhpm();
            gp.setAtivo(Boolean.TRUE);
            gp.setDescricao((String) c[1]);
            gp.setDtCadastro(new Date());
            gp.setIdEspecialidadeProcedimento(eEJB.selecionarPorIDEspecialidadeProcedimentoTasy(((BigDecimal) c[2]).intValue()));
            gp.setIdGrupoProcedimentoTasy(((BigDecimal) c[0]).intValue());
            Salvar(gp, "sync");
            //em.merge(BCRUtils.gerarLogIntegracao(gp.getDescricao() + " cadastrado.", "VincularGrupoProcedimentoTasy", dtInicio, new Date(), logPai));
        } catch (Exception e) {
            System.out.println(e);
            //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar " + (String) c[1] + ". " + e.getMessage(), "VincularGrupoProcedimentoTasy", null, null, logPai));
        }
    }

    public List<GrupoProcedimentoCbhpm> autoCompleteGrupoProcedimento(String var) {
        Query query = getEm().createQuery("Select s From GrupoProcedimentoCbhpm s where s.ativo = TRUE AND (s.descricao LIKE :desc OR s.idGrupoProcedimentoCbhpm like :id OR s.idGrupoProcedimentoTasy like :id)");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();
    }
}
