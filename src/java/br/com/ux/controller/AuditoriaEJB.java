/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.Auditoria;
import br.com.ux.util.Conexao;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class AuditoriaEJB extends Conexao {

    public List<Auditoria> listarPorData(Date dtIni, Date dtFim) {
        Query query =  getEm().createQuery("Select a From Auditoria a where a.dtHora BETWEEN :ini and :fim");
        query.setParameter("ini", dtIni);
        query.setParameter("fim", dtFim);
        return query.getResultList();
    }
    
     public List<Auditoria> listarAuditoriaLazyModeWhere(int primeiro, int qtd, String clausula, Date dtIni, Date dtFim) {
        Query query =  getEm().createQuery("Select a From Auditoria a where a.dtHora BETWEEN :ini and :fim" + clausula);
         query.setParameter("ini", dtIni);
        query.setParameter("fim", dtFim);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistro(String clausula, Date dtIni, Date dtFim) {
        Query query =  getEm().createQuery("Select COUNT(a.idAuditoria) From Auditoria a where a.dtHora BETWEEN :ini and :fim "+clausula);
        query.setParameter("ini", dtIni);
        query.setParameter("fim", dtFim);
        return (Number) query.getSingleResult();
    }
    
    public void Salvar(Auditoria auditoria){
         em.merge(auditoria);                
    }
}
