/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.controller;

import br.com.ux.model.LogIntegracao;
import br.com.ux.model.tasy.AreaProcedimento;
import br.com.ux.model.tasy.EspecialidadeProcedimento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class EspecialidadeProcedimentoEJB extends Conexao {
    
    @EJB
    AreaProcedimentoEJB aEJB;

    public void Salvar(EspecialidadeProcedimento ep, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.persist(ep);
        em.flush();
        em.refresh(ep);
        em.merge(BCRUtils.criarAuditoria("insert", "Nome: " + ep.getDescricao(), usuario, new Date(), "Especialidade Procedimento"));
    }

    public void Atualizar(EspecialidadeProcedimento ep, String usuario) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(ep);
        em.merge(BCRUtils.criarAuditoria("update", "Nome: " + ep.getDescricao(), usuario, new Date(), "Especialidade Procedimento"));
    }

    public List<EspecialidadeProcedimento> listarEspecialidadeProcedimento() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createQuery("Select e From EspecialidadeProcedimento e ORDER BY e.idEspecialidadeProcedimento").getResultList();
    }

    public EspecialidadeProcedimento selecionarPorIDEspecialidadeProcedimentoTasy(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT e FROM EspecialidadeProcedimento e WHERE e.idEspecialidadeProcedimentoTasy = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (EspecialidadeProcedimento) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }
    
    public EspecialidadeProcedimento retornaEspecialidadeProcedimentoPorID(Integer id) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("SELECT e FROM EspecialidadeProcedimento e WHERE e.idEspecialidadeProcedimento = :id");
        query.setParameter("id", id);
        query.setMaxResults(1);
        try {
            return (EspecialidadeProcedimento) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + "em: " + id);
            return null;
        }
    }
    
    public void VincularEspecialidadeProcedimentoTasy(Object[] c, LogIntegracao logPai) {
        EspecialidadeProcedimento ep = new EspecialidadeProcedimento();
        Date dtInicio = new Date();
        if (c[0] != null) {
            ep = selecionarPorIDEspecialidadeProcedimentoTasy(((BigDecimal) c[0]).intValue());
        }

        if (ep != null) {
            if (ep.getIdEspecialidadeProcedimentoTasy() == null) {
                try {
                    // Cria o vinculo atraves do ID_ESPECIALIDADE_PROCEDIMENTO_TASY
                    ep.setIdEspecialidadeProcedimentoTasy(((BigDecimal) c[0]).intValue());
                    if (c[2] != null) {
                        ep.setIdAreaProcedimento(aEJB.selecionarPorIDAreaProcedimentoTasy(((BigDecimal) c[2]).intValue()));
                    }                    
                    Salvar(ep, "sync");
                    //em.merge(BCRUtils.gerarLogIntegracao(ep.getDescricao() + " vinculado.", "VincularAreaProcedimentoTasy", dtInicio, new Date(), logPai));
                } catch (Exception e) {
                    System.out.println(e);
                    //em.merge(BCRUtils.gerarLogIntegracao("Erro ao vincular " + (String) c[1] + ". " + e.getMessage(), "VincularCboSaudeTasy", null, null, logPai));
                }
            }
        } else {
            // Se ele não encontrar a Especialidade Procedimento no SMA, salva um novo cadastro.
            cadastrarEspecialidadeProcedimento(c, dtInicio, logPai);
        }
    }
    
    public void VincularListaEspecialidadeProcedimentoTasy(List<Object[]> resultList, LogIntegracao logPai) {
        for (Object[] c : resultList) {
            VincularEspecialidadeProcedimentoTasy(c, logPai);
        }
    }

    public void cadastrarEspecialidadeProcedimento(Object[] c, Date dtInicio, LogIntegracao logPai) {
        try {
            EspecialidadeProcedimento ep = new EspecialidadeProcedimento();
            ep.setIdEspecialidadeProcedimentoTasy(((BigDecimal) c[0]).intValue());
            ep.setDescricao((String) c[1]);
            ep.setIdAreaProcedimento(aEJB.selecionarPorIDAreaProcedimentoTasy(((BigDecimal) c[2]).intValue()));
            ep.setDtCadastro(new Date());
            ep.setAtivo(Boolean.TRUE);
            Salvar(ep, "sync");
            //em.merge(BCRUtils.gerarLogIntegracao(ep.getDescricao() + " cadastrado.", "VincularEspecialidadeProcedimentoTasy", dtInicio, new Date(), logPai));
        } catch (Exception e) {
            System.out.println(e);
            //em.merge(BCRUtils.gerarLogIntegracao("Erro ao cadastrar " + (String) c[1] + ". " + e.getMessage(), "VincularEspecialidadeProcedimentoTasy", null, null, logPai));
        }
    }

    public List<EspecialidadeProcedimento> autoCompleteEspecialidadeProcedimento(String var) {
        Query query = getEm().createQuery("Select s From EspecialidadeProcedimento s where s.ativo = TRUE AND (s.descricao LIKE :desc OR s.idEspecialidadeProcedimentoTasy like :id OR s.idEspecialidadeProcedimento like :id)");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        return query.getResultList();
    }
}
