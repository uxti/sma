/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.ConvenioEJB;
import br.com.ux.controller.PlanoEJB;
import br.com.ux.model.Convenio;
import br.com.ux.model.Plano;
import br.com.ux.model.PlanoConvenio;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class ConvenioMB implements Serializable {

    @EJB
    ConvenioEJB cEJB;
    private Convenio convenio;
    @EJB
    PlanoEJB pEJB;
    private Plano plano;
    private List<PlanoConvenio> listaPlanosASalvar;
    private PlanoConvenio planoConvenio;
    UsuarioSessao us = new UsuarioSessao();
    private List<Convenio> listaConvenios;
    LazyDataModel<Convenio> model;

    /**
     * Creates a new instance of ConvenioMB
     */
    public ConvenioMB() {
        novo();
        carregarModeloLazy();
    }

    public void novo() {
        convenio = new Convenio();
        listaConvenios = new ArrayList<Convenio>();
        plano = new Plano();
        listaPlanosASalvar = new ArrayList<PlanoConvenio>();
        planoConvenio = new PlanoConvenio();
    }

    public void Salvar() {
        if (convenio.getIdConvenio() == null) {
            try {
                cEJB.Salvar(convenio, us.retornaUsuario(), listaPlanosASalvar);
                Mensagem.addMensagem(1, "Convenio salvo com sucesso!");
                novo();
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Algo inesperado aconteceu ao tentar salvar o convenio!" + e);
            }
        } else {
            try {
                cEJB.Salvar(convenio, us.retornaUsuario(), listaPlanosASalvar);
                Mensagem.addMensagem(1, "Convenio alterado com sucesso!");
                novo();
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Algo inesperado aconteceu ao tentar salvar o convenio!" + e);
            }
        }
    }

    public void carregarModeloLazy() {
        model = new LazyDataModel<Convenio>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Convenio> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" AND p.inativo = FALSE and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%' AND p.inativo = FALSE");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%' AND p.inativo = FALSE");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();

                setRowCount(Integer.parseInt(cEJB.contarRegistro(Clausula).toString()));

                listaConvenios = cEJB.listarLazy(first, pageSize, Clausula);
                return listaConvenios;
            }
        };
    }

    public List<Convenio> listarConvenios() {
        return cEJB.listarConvenios();
    }

    public void Excluir() {
        try {
            if (convenio.getCirurgiaList().isEmpty()) {
                cEJB.Excluir(convenio, us.retornaUsuario());
                Mensagem.addMensagem(1, "Convenio excluído com sucesso!");
                novo();
                BCRUtils.ResetarDatatableFiltros("frmConvenio:dtl");
            } else {
                Mensagem.addMensagem(3, "Este cadastro já foi utilizado e não pode ser excluído. Se quiser desabilitar, clique em Editar e desmarque a opção 'Ativo'.");

            }

        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar excluir!" + e);
        }
    }

    public Convenio selecionarPorID(Integer id) {
        convenio = cEJB.selecionarPorID(id, us.retornaUsuario());
        listaPlanosASalvar = convenio.getPlanoConvenioList();
        return convenio;
    }

    public void addPlano(Plano plano) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, new FacesMessage("O plano " + plano.getDescricao() + " foi adicionado com sucesso!"));
        planoConvenio.setIdPlano(plano);
        listaPlanosASalvar.add(planoConvenio);
        planoConvenio = new PlanoConvenio();
    }

    public void removerPlano(PlanoConvenio plano) {
        if (plano.getIdPlanoConvenio() != null) {
            try {
                cEJB.ExcluirOrcamentoPlano(plano, us.retornaUsuario());
                listaPlanosASalvar.remove(plano);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, new FacesMessage("O plano " + plano.getIdPlano().getDescricao() + " foi removido com sucesso!"));
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("excluir");
            }

        } else {
            try {
                listaPlanosASalvar.remove(plano);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, new FacesMessage("O plano " + plano.getIdPlano().getDescricao() + " foi removido com sucesso!"));
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("excluir");
            }

        }

    }

    public Convenio getConvenio() {
        return convenio;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

    public Plano getPlano() {
        return plano;
    }

    public List<PlanoConvenio> getListaPlanosASalvar() {
        return listaPlanosASalvar;
    }

    public List<Plano> listaPlanos() {
        return pEJB.listarPlanos();
    }

    public List<Convenio> getListaConvenios() {
        return listaConvenios;
    }

    public void setListaConvenios(List<Convenio> listaConvenios) {
        this.listaConvenios = listaConvenios;
    }

    public LazyDataModel<Convenio> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Convenio> model) {
        this.model = model;
    }

}
