/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.EmailsEJB;
import br.com.ux.model.Emails;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class EmailsMB {

    @EJB
    EmailsEJB eEJB;
    Emails emails;
    List<Emails> listaEmails;

    UsuarioSessao us = new UsuarioSessao();

    public EmailsMB() {
        novo();
    }

    public void novo() {
        emails = new Emails();
    }

    public void salvar() {
        try {
            eEJB.Salvar(emails);
            Mensagem.addMensagemPadraoSucesso("salvar");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }
    
    public void excluir(Emails emails) {
        try {
            eEJB.Excluir(emails);
            Mensagem.addMensagemPadraoSucesso("excluir");
            novo();
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }
    
    public Emails selecionarPorID(Integer id) {
        emails = eEJB.selecionarPorID(id);
        return emails;
    }

    public List<Emails> listaEmails() {
        return eEJB.listarEmails();
    }

    public Emails getEmails() {
        return emails;
    }

    public void setEmails(Emails emails) {
        this.emails = emails;
    }

    public List<Emails> getListaEmails() {
        return listaEmails;
    }

    public void setListaEmails(List<Emails> listaEmails) {
        this.listaEmails = listaEmails;
    }

}
