/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.TextosEmailsEJB;
import br.com.ux.model.TextoEmails;
import br.com.ux.util.Mensagem;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class TextoEmailsMB implements Serializable {

    @EJB
    TextosEmailsEJB teEJB;
    TextoEmails textoEmails;
    List<TextoEmails> listaTextoEmails;

    public TextoEmailsMB() {
        Novo();
    }

    public void Novo() {
        textoEmails = new TextoEmails();
    }

    public void Salvar() {
        try {
            teEJB.Salvar(textoEmails);
            Mensagem.addMensagemPadraoSucesso("salvar");
            Novo();
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    @PostConstruct
    public void carregarListaTextoEmails() {
        listaTextoEmails = teEJB.listarTextoEmails();
    }

    public List<TextoEmails> listarTextoEmails() {
        listaTextoEmails = teEJB.listarTextoEmails();
        return listaTextoEmails;
    }

    public void Excluir(TextoEmails te) {
        try {
            teEJB.Excluir(te);
            Mensagem.addMensagemPadraoSucesso("excluir");
            Novo();
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }

    public void selecionarPorID(Integer id) {
        textoEmails = teEJB.selecionarPorID(id);
    }

    public TextoEmails getTextoEmails() {
        return textoEmails;
    }

    public void setTextoEmails(TextoEmails textoEmails) {
        this.textoEmails = textoEmails;
    }

    public List<TextoEmails> getListaTextoEmails() {
        return listaTextoEmails;
    }

    public void setListaTextoEmails(List<TextoEmails> listaTextoEmails) {
        this.listaTextoEmails = listaTextoEmails;
    }
    
    

}
