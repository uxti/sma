/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.PlanoEJB;
import br.com.ux.model.Plano;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class PlanoMB implements Serializable {

    @EJB
    PlanoEJB pEJB;
    private Plano plano;
    UsuarioSessao us = new UsuarioSessao();
    List<Plano> listaaCarregarLazy;
    LazyDataModel<Plano> model;

    public PlanoMB() {
        novo();
    }

    public void novo() {
        plano = new Plano();
        model = new LazyDataModel<Plano>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Plano> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                          if(sf.toString().contains("where")){
                               sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                          }else{
                               sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                          }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(pEJB.contarPlanosRegistro(Clausula).toString()));


                listaaCarregarLazy = pEJB.listarPlanosLazyModeWhere(first, pageSize, Clausula);
//                listaPacientesLazy = pEJB.listarPacientesLazyMode(first, pageSize);
                return listaaCarregarLazy;
            }
        };
    }

    public void salvar() {
        if (plano.getIdPlano() == null) {
            try {
                pEJB.Salvar(plano, us.retornaUsuario());
                Mensagem.addMensagem(1, "Plano salvo com sucesso!");
                novo();
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Algo inesperado aconteceu ao tentar salvar o plano!" + e);
            }
        } else {
            try {

                pEJB.Salvar(plano, us.retornaUsuario());
                Mensagem.addMensagem(1, "Plano alterado com sucesso!");
                novo();
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Algo inesperado aconteceu ao tentar salvar o plano!" + e);
            }
        }
    }

    public List<Plano> listarPlanos() {
        return pEJB.listarPlanos();
    }

    public void Excluir() {
        try {
            pEJB.Excluir(plano, us.retornaUsuario());
            Mensagem.addMensagem(1, "Plano excluído com sucesso!");
            novo();
            BCRUtils.ResetarDatatableFiltros("frmPlano:dtlPlanos");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar excluir!" + e);
        }
    }
    
    public Plano selecionarPlanoPorNome(String busca) {
        return pEJB.selecionarPlanoPorNome(busca);
    }

    public List<Plano> getListaaCarregarLazy() {
        return listaaCarregarLazy;
    }

    public void setListaaCarregarLazy(List<Plano> listaaCarregarLazy) {
        this.listaaCarregarLazy = listaaCarregarLazy;
    }

    public LazyDataModel<Plano> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Plano> model) {
        this.model = model;
    }

    public Plano selecionarPorID(Integer id) {
        return plano = pEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public Plano getPlano() {
        return plano;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }

    public PlanoEJB getpEJB() {
        return pEJB;
    }

    public void setpEJB(PlanoEJB pEJB) {
        this.pEJB = pEJB;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }
}
