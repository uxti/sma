/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Charles
 */
@Named(value = "fotoMB")
@RequestScoped
public class fotoMB {

    private UploadedFile logo;
    private File foto;

    public fotoMB() {
        getFoto();       
    }

    public void setLogo(UploadedFile logo) {
        this.logo = logo;
    }

    public UploadedFile getLogo() {
        return logo;
    }

    public void handleFileUpload(FileUploadEvent event) {
        setLogo(event.getFile());
        InputStream is = null;
        OutputStream os = null;
        byte[] buffer;
        boolean success = true;
        try {
            is = event.getFile().getInputstream();
            os = new FileOutputStream("C:\\SMA\\logo.png");
            buffer = new byte[is.available()];
            is.read(buffer);
            os.write(buffer);
        } catch (IOException e) {
            success = false;
        } catch (OutOfMemoryError e) {
            success = false;
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
                if (os != null) {
                    os.close();

                }
            } catch (IOException e) {
            }
        }
        System.out.println(success);
        if (success) {
            System.out.println("OK");
        }
        FacesMessage message = new FacesMessage("Upload feito com sucesso.", event.getFile().getFileName() + " salvo em C:\\SMA\\logo.png");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public StreamedContent getFoto() {
        File foto = new File("C:\\SMA\\logo.png");
        DefaultStreamedContent content = null;
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(foto));
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();
            content = new DefaultStreamedContent(new ByteArrayInputStream(bytes), "image/png");
        } catch (Exception e) {
            System.out.println(e);
        }
        return content;
    }
}
