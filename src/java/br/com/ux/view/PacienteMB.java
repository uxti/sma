/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.PacienteEJB;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.Paciente;
import br.com.ux.model.PacienteResponsavel;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.RelatorioFactory;
import br.com.ux.util.UsuarioSessao;
import br.com.ux.util.ValidarCPF;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PacienteMB implements Serializable {

    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;
    private List<Paciente> listaPacientes;
    private List<Paciente> listaPacientesLazy;
    UsuarioSessao us = new UsuarioSessao();
    private LazyDataModel<Paciente> model;
    private PacienteResponsavel pacienteResponsavel;
    List<PacienteResponsavel> listaResponsaveisSalvar;
    private Paciente pacienteTemp;
    private Paciente pacienteSegundoPlano;

    public PacienteMB() {
        novo();

    }

    public void novo() {

        paciente = new Paciente();
        pacienteSegundoPlano = new Paciente();
        pacienteResponsavel = new PacienteResponsavel();
        listaPacientes = new ArrayList<Paciente>();
        listaResponsaveisSalvar = new ArrayList<PacienteResponsavel>();
        listaPacientesLazy = new ArrayList<Paciente>();
        setIdade(0);
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String action = params.get("pacienteNaoCadastrado");
        if (action != null) {
            if (!action.equals("null")) {
                System.out.println(action);
                paciente.setNome(action);
            }
        }
        pacienteTemp = new Paciente();
        model = new LazyDataModel<Paciente>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Paciente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;

                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append("and p.inativo = FALSE and p." + filterProperty + " like'" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'" + filterValue + "%' and p.inativo = FALSE");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'" + filterValue + "%' and p.inativo = FALSE");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }

                setRowCount(Integer.parseInt(pEJB.contarPacientesRegistro(Clausula).toString()));
                listaPacientesLazy = pEJB.listarPacientesLazyModeWhere(first, pageSize, Clausula);
                return listaPacientesLazy;
            }
        };
        BCRUtils.ResetarDatatableFiltros("consultaPrincipal:dt2");

    }

    public void novoResponsavel() {
        pacienteSegundoPlano = new Paciente();
        setIdade(0);
    }

    public void carregarListaPacientes() {
    }

    public static String captalizar(String a) {
        String aa = a.substring(0, 1).toUpperCase();
        String ab = a.substring(1);
        String f = aa + ab;
        System.out.println(f);
        return f;
    }

    public void addResponsavel(Paciente paciente) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, new FacesMessage("Responsável " + paciente.getNome() + " foi adicionado com sucesso!"));
        pacienteResponsavel.setIdPacienteResponsavel(paciente);
        listaResponsaveisSalvar.add(pacienteResponsavel);
        pacienteResponsavel = new PacienteResponsavel();
    }

    public void removerResponsavel(PacienteResponsavel pacienteResponsavel) {
        if (pacienteResponsavel.getIdPacienteResposanvel() != null) {
            try {
                pEJB.ExcluirResponsavel(pacienteResponsavel, us.retornaUsuario());
                listaResponsaveisSalvar.remove(pacienteResponsavel);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, new FacesMessage("O responsavel " + pacienteResponsavel.getIdPacienteResponsavel().getNome() + " foi removido com sucesso!"));
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("excluir");
            }
        } else {
            try {
                listaResponsaveisSalvar.remove(pacienteResponsavel);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, new FacesMessage("O responsavel " + pacienteResponsavel.getIdPacienteResponsavel().getNome() + " foi removido com sucesso!"));
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("excluir");
            }
        }
    }
    private Integer idade;

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public Paciente getPacienteTemp() {
        return pacienteTemp;
    }

    public void setPacienteTemp(Paciente pacienteTemp) {
        this.pacienteTemp = pacienteTemp;
    }

    public void calculaIdade() {
        try {
            Calendar cn = Calendar.getInstance();
            if (cn != null) {
                cn.setTime(paciente.getDtNascimento());
                Date dataAtual = new Date();
                Calendar ca = Calendar.getInstance();
                ca.setTime(dataAtual);

                if (cn.get(Calendar.YEAR) > ca.get(Calendar.YEAR)) {
                    paciente.setDtNascimento(null);
                    setIdade(0);
                } else {
                    int idade = ca.get(Calendar.YEAR) - cn.get(Calendar.YEAR);
                    if ((idade < 0) || (idade > 130)) {
                        paciente.setDtNascimento(null);
                        setIdade(0);
                    } else {
                        setIdade(idade);
                    }
                }
            } else {
                setIdade(0);
                paciente.setDtNascimento(null);
            }
        } catch (Exception e) {
            setIdade(0);
            paciente.setDtNascimento(null);

        }
    }
    Integer idadeTotal;

    public Integer getIdadeTotal() {
        return idadeTotal;
    }

    public void setIdadeTotal(Integer idadeTotal) {
        this.idadeTotal = idadeTotal;
    }

    public Date dtNascimento(Integer idade) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.YEAR, -idade);
        return cal.getTime();
    }

    public Integer calculaIdadeDatatable(Date dt_nasc) {
        try {
            Calendar cn = Calendar.getInstance();
            cn.setTime(dt_nasc);
            Date dataAtual = new Date(System.currentTimeMillis());
            Calendar ca = Calendar.getInstance();
            ca.setTime(dataAtual);
            int idade = ca.get(Calendar.YEAR) - cn.get(Calendar.YEAR);
            if (ca.get(Calendar.MONTH) < cn.get(Calendar.MONTH)) {
                idade--;
            } else if (ca.get(Calendar.MONTH) == cn.get(Calendar.MONTH)) {
                if (ca.get(Calendar.DAY_OF_MONTH) < cn.get(Calendar.DAY_OF_MONTH)) {
                    idade--;
                }
            }
            setIdade(idade);
            return getIdade();
        } catch (Exception e) {
            setIdade(0);
            return getIdade();
        }
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public List<Paciente> getListaPacientes() {
        return listaPacientes;
    }

    public void carregarPacientes() {
        listaPacientes = pEJB.listarPacientes();
    }

    public LazyDataModel<Paciente> getModel() {
        return model;
    }

    public List<Paciente> getListaPacientesLazy() {
        return listaPacientesLazy;
    }

    public void setListaPacientesLazy(List<Paciente> listaPacientesLazy) {
        this.listaPacientesLazy = listaPacientesLazy;
    }

    public Long validarCPF() {
        ValidarCPF vCPF = new ValidarCPF();
        Long t;
        Long retorno;
        t = (Long) pEJB.consultarCPF(paciente.getCpf());
        if (vCPF.isCPF(paciente.getCpf())) {
            if (t > 0) {
                Mensagem.addMensagem(3, "CPF já está em uso!");
                retorno = 0L;
//                RequestContext.getCurrentInstance().execute("document.getElementById('tblPrincipal:cpf').value=''");
//                RequestContext.getCurrentInstance().execute("document.getElementById('tblPrincipal:cpf').focus();");
            } else {
                retorno = 1L;
            }
        } else {
            Mensagem.addMensagem(3, "CPF inválido");
            retorno = 0L;
//            RequestContext.getCurrentInstance().execute("document.getElementById('tblPrincipal:cpf').focus();");
        }
        return retorno;
    }

    public void retornoDialogo() {
        if (paciente.getIdPaciente() != null) {
            if ((!paciente.getCpf().isEmpty() && (!listaResponsaveisSalvar.isEmpty())) || (idade.intValue() > 18)) {
                System.out.println("Aqui");
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('dlgCadastrarPaciente').hide();");
            }
        }
    }

    public void salvarDialogo(Paciente pacienteSP) {
        ValidarCPF vcpf = new ValidarCPF();
        if (pacienteSP.getIdPaciente() == null) {
            try {
                if ((pacienteSP.getCpf().isEmpty() || pacienteSP.getCpf() == null)) {
                    Mensagem.addMensagem(3, "Paciente com CPF em branco. ");
                } else if (pacienteSP.getCpf() == null || pacienteSP.getCpf().isEmpty()) {
                    if (testarNome(pacienteSP.getNome())) {
                        pEJB.Atualizar(pacienteSP, us.retornaUsuario(), false);
                        Mensagem.addMensagemPadraoSucesso("salvar");
                    } else {
                        Mensagem.addMensagem(3, "Atenção! O Nome informado não é válido!");
                    }
                } else if (vcpf.isCPF(pacienteSP.getCpf())) {
                    pacienteSP.setDtCadastro(new Date());
                    if (!verificaCPFExiste(pacienteSP.getCpf())) {
                        if (testarNome(pacienteSP.getNome())) {
                            pEJB.Atualizar(pacienteSP, us.retornaUsuario(), false);
                            Mensagem.addMensagemPadraoSucesso("salvar");
                        } else {
                            Mensagem.addMensagem(3, "Atenção! O Nome informado não é válido!");
                        }

                    } else {
                        pacienteTemp = pEJB.verificaCPFPaciente(pacienteSP.getCpf());
                        Mensagem.addMensagem(3, "CPF já existe no paciente " + pacienteTemp.getNome() + "!");
                        pacienteTemp = new Paciente();
                    }
                } else {
                    Mensagem.addMensagem(3, "Atenção! O CPF informado não é válido!");
                }
            } catch (Exception e) {
                System.out.println("O erro é: " + e.toString() + " termina aqui o erro");
                if (e.toString().contains("MySQLIntegrityConstraintViolationException")) {
                    Mensagem.addMensagem(3, "CPF já existe!");
                } else {
                    Mensagem.addMensagem(3, "Erro ao tentar Salvar!!" + e);
                }
            }

        }
    }

    public boolean verificaCPFExiste() {
        try {
            if (pEJB.verificaCPF(paciente.getCpf()) > 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public boolean verificaCPFExiste(String CPF) {
        try {
            if (pEJB.verificaCPF(CPF) > 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public void verificaCPF() {
        System.out.println(paciente.getCpf());
        if (!paciente.getCpf().equals("___.___.___-__")) {
            ValidarCPF vcpf = new ValidarCPF();
            if (vcpf.isCPF(paciente.getCpf())) {
                String nome = pEJB.verificaCPFRetornaNome(paciente.getCpf());
                if (!nome.equals("Inexistente")) {
                    Mensagem.addMensagem(3, "CPF já foi cadastrado para " + nome);
                }
            } else {
                Mensagem.addMensagem(3, "Atenção! O CPF informado não é válido!");
            }
        }
    }

    public void salvar2() {
        if (paciente.getInativo() == null) {
            paciente.setInativo(false);
        }
        if (paciente.getInativo()) {
            pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
            Mensagem.addMensagemPadraoSucesso("salvar");
            novo();
        } else {
            ValidarCPF vcpf = new ValidarCPF();
            if (paciente.getIdPaciente() == null) {
                try {
                    if (paciente.getCpf() == null || paciente.getCpf().isEmpty()) {
                        if (testarNome()) {
                            pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                            Mensagem.addMensagemPadraoSucesso("salvar");
                            novo();
                        } else {
                            Mensagem.addMensagem(3, "Atenção! O Nome informado não é válido!");
                        }
                    } else if (vcpf.isCPF(paciente.getCpf())) {
                        paciente.setDtCadastro(new Date());
                        if (!verificaCPFExiste()) {
                            if (testarNome()) {
                                pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                                Mensagem.addMensagemPadraoSucesso("salvar");
                                novo();
                            } else {
                                Mensagem.addMensagem(3, "Atenção! O Nome informado não é válido!");
                            }

                        } else {
                            pacienteTemp = pEJB.verificaCPFPaciente(paciente.getCpf());
                            Mensagem.addMensagem(3, "CPF já existe no paciente " + pacienteTemp.getNome() + "!");
                            pacienteTemp = new Paciente();
                        }
                    } else {
                        Mensagem.addMensagem(3, "Atenção! O CPF informado não é válido!");
                    }
                } catch (Exception e) {
                    System.out.println("O erro é: " + e.toString() + " termina aqui o erro");
                    if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                        Mensagem.addMensagem(3, "CPF já existe!");
                        RequestContext.getCurrentInstance().execute("document.getElementByID('tblPrincipal:cpf').focus()");
                    } else {
                        Mensagem.addMensagem(3, "Erro ao tentar Salvar!!" + e);
                    }
                }
            } else if ((paciente.getCpf() == null || paciente.getCpf().equals("___.___.___-__")) && paciente.getPacienteResponsavelList().size() < 1) {
                Mensagem.addMensagem(3, "Informe um responsável para este paciente!");
            } else {

                try {
                    System.out.println("Maior com CPF");
                    if (paciente.getCpf() == null || paciente.getCpf().isEmpty()) {
                        if (testarNome()) {
                            pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                            Mensagem.addMensagemPadraoSucesso("editar");
                        } else {
                            Mensagem.addMensagem(3, "Atenção! O Nome informado não é válido!");
                        }
                    } else if (vcpf.isCPF(paciente.getCpf())) {
                        if (!verificaCPFExiste()) {
                            if (testarNome()) {
                                pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                                Mensagem.addMensagemPadraoSucesso("editar");
                            } else {
                                Mensagem.addMensagem(3, "Atenção! O Nome informado não é válido!");
                            }

                        } else if (pEJB.contarPacienteCPFiD(paciente.getCpf(), paciente.getIdPaciente()) > 0) {
                            if (testarNome()) {
                                pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                                Mensagem.addMensagemPadraoSucesso("editar");
                            } else {
                                Mensagem.addMensagem(3, "Atenção! O Nome informado não é válido!");
                            }
                        } else {
                            pacienteTemp = pEJB.verificaCPFPaciente(paciente.getCpf());
                            Mensagem.addMensagem(3, "CPF já existe no paciente " + pacienteTemp.getNome() + "!");
                            pacienteTemp = new Paciente();
                        }
                    } else {
                        Mensagem.addMensagem(3, "Atenção! O CPF informado não é válido!");
                    }
                } catch (Exception e) {
                    if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                        Mensagem.addMensagem(3, "CPF já existe!");
                    } else {
                        Mensagem.addMensagem(3, "Erro ao tentar Editar!!" + e);
                    }
                }
            }
        }
    }

    public void salvar() {
        ValidarCPF vCPF = new ValidarCPF();
        if (paciente.getIdPaciente() == null) {
            try {
                if (idade < 18 && paciente.getCpf().isEmpty()) {
                    paciente.setCpf(null);
                    paciente.setDtCadastro(new Date());
                    pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                    Mensagem.addMensagemPadraoSucesso("salvar");
                } else if (vCPF.isCPF(paciente.getCpf())) {
                    paciente.setDtCadastro(new Date());
                    pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                    Mensagem.addMensagemPadraoSucesso("salvar");
                } else {
                    Mensagem.addMensagem(3, "CPF inválido!");
                }
            } catch (Exception e) {
                if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                    Mensagem.addMensagem(3, "CPF já existe!");
                } else {
                    Mensagem.addMensagem(3, "Erro ao tentar Salvar!!" + e);
                }
            }
        } else {
            try {
                if (idade < 18 && paciente.getCpf().isEmpty()) {
                    pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                    Mensagem.addMensagemPadraoSucesso("editar");
                } else if (vCPF.isCPF(paciente.getCpf())) {
                    pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                    Mensagem.addMensagemPadraoSucesso("editar");
                } else {
                    Mensagem.addMensagem(3, "CPF inválido!");
                }
            } catch (Exception e) {
                System.out.println("O erro é: " + e.getCause().toString() + " termina aqui o erro");
                if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                    Mensagem.addMensagem(3, "CPF já existe!");
                } else {
                    Mensagem.addMensagem(3, "Erro ao tentar Editar!!" + e);
                }
            }
        }
    }

    public void SalvarDialogo() {
        if (paciente.getIdPaciente() == null) {
            try {
                paciente.setDtCadastro(new Date());
                pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");

            } catch (Exception e) {
                if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                    Mensagem.addMensagem(3, "CPF já existe!");
                } else {
                    Mensagem.addMensagem(3, "Erro ao tentar Salvar!!" + e);
                }
            }
        } else {
            try {
                pEJB.Salvar(paciente, us.retornaUsuario(), listaResponsaveisSalvar);
                novo();
                Mensagem.addMensagemPadraoSucesso("editar");

            } catch (Exception e) {
                if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                    Mensagem.addMensagem(3, "CPF já existe!");
                } else {
                    Mensagem.addMensagem(3, "Erro ao tentar Editar!!" + e);
                }
            }
        }
    }

    public List<Paciente> listarPacientes() {
        return pEJB.listarPacientes();
    }

    public Paciente selecionarPorID(Integer id) {
        paciente = pEJB.selecionarPorID(id, us.retornaUsuario());
        listaResponsaveisSalvar = paciente.getPacienteResponsavelList();
        List<Cirurgia> listaTeste = new ArrayList<Cirurgia>();
        listaTeste = paciente.getCirurgiaList();
        calculaIdade();
        return paciente;
    }

    public void selecionarPorIDResponsavel(Integer id) {
        paciente = pEJB.selecionarPorID(id, us.retornaUsuario());
        listaResponsaveisSalvar = paciente.getPacienteResponsavelList();
        List<Cirurgia> listaTeste = new ArrayList<Cirurgia>();
        listaTeste = paciente.getCirurgiaList();
        calculaIdade();
    }

    public void Excluir() {
        try {
            pEJB.Excluir(paciente, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("excluir");
            BCRUtils.ResetarDatatableFiltros("consultaPrincipal:dt2");
            novo();
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar excluir!" + e);
        }
    }

    public boolean testarNome() {
        if (paciente.getNome().isEmpty()) {
            Mensagem.addMensagem(3, "O campo não pode ficar em branco!");
            return false;
        } else {
            Pattern pattern = Pattern.compile("[0-9]");
            Matcher matcher = pattern.matcher(paciente.getNome());
            if (matcher.find()) {
                Mensagem.addMensagem(3, "O Nome não deve conter números!");
                return false;
            } else if (!paciente.getNome().contains(" ")) {
                Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                return false;
            } else if (paciente.getNome().length() < 2) {
                Mensagem.addMensagem(3, "Verifique o nome informado. Min. 2 letras.");
                return false;
            } else {
                String[] listaNomes = paciente.getNome().split(" ");
                for (String t : listaNomes) {
                    if (t.length() < 2) {
                        Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                        return false;
                    }
                }
                if (listaNomes.length <= 1) {
                    Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                    return false;
                } else {
//                            if (pEJB.pesquisaSePacienteExistePorNome(paciente.getNome())) {
//                                Mensagem.addMensagem(2, "Atenção! Nome já existente no cadastro.");
//                                return false;
//                            } else {
                    return true;
                }
            }
        }
    }
    //}

    public boolean testarNome(String Nome) {
        if (Nome.isEmpty()) {
            Mensagem.addMensagem(3, "O campo não pode ficar em branco!");
            return false;
        } else {
            Pattern pattern = Pattern.compile("[0-9]");
            Matcher matcher = pattern.matcher(Nome);
            if (matcher.find()) {
                Mensagem.addMensagem(3, "O Nome não deve conter números!");
                return false;
            } else if (!Nome.contains(" ")) {
                Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                return false;
            } else if (Nome.length() < 2) {
                Mensagem.addMensagem(3, "Verifique o nome informado. Min. 2 letras.");
                return false;
            } else {
                String[] listaNomes = Nome.split(" ");
                for (String t : listaNomes) {
                    if (t.length() < 2) {
                        Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                        return false;
                    }
                }
                if (listaNomes.length <= 1) {
                    Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                    return false;
                } else {
//                            if (pEJB.pesquisaSePacienteExistePorNome(Nome)) {
//                                Mensagem.addMensagem(2, "Atenção! Nome já existente no cadastro.");
//                                return false;
//                            } else {
                    return true;
                }
            }
        }
    }
    // }

    public void ConsultaCEP() {
        if (paciente.getCep().length() == 9) {
            try {
                URL url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep=" + paciente.getCep() + "&formato=xml");
                Document document = getDocumento(url);
                Element root = document.getRootElement();
                for (Iterator i = root.elementIterator(); i.hasNext();) {
                    Element element = (Element) i.next();
                    if (element.getText().isEmpty()) {
                        Mensagem.addMensagem(1, "Não foi possivel encontrar este CEP!");
                    } else {
                        if (element.getQualifiedName().equals("cidade")) {
                            paciente.setCidade(element.getText());

                        }
                        if (element.getQualifiedName().equals("bairro")) {
                            paciente.setBairro(element.getText());
                        }
                        if (element.getQualifiedName().equals("logradouro")) {
                            paciente.setLogradouro(element.getText());
                        }
                        if (element.getQualifiedName().equals("uf")) {
                            paciente.setUf(element.getText());
                        }
                        if (element.getQualifiedName().equals("tipo_logradouro")) {
                            paciente.setTipoLogradouro(element.getText().toUpperCase());
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }

    public PacienteEJB getpEJB() {
        return pEJB;
    }

    public void setpEJB(PacienteEJB pEJB) {
        this.pEJB = pEJB;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public PacienteResponsavel getPacienteResponsavel() {
        return pacienteResponsavel;
    }

    public void setPacienteResponsavel(PacienteResponsavel pacienteResponsavel) {
        this.pacienteResponsavel = pacienteResponsavel;
    }

    public List<PacienteResponsavel> getListaResponsaveisSalvar() {
        return listaResponsaveisSalvar;
    }

    public void setListaResponsaveisSalvar(List<PacienteResponsavel> listaResponsaveisSalvar) {
        this.listaResponsaveisSalvar = listaResponsaveisSalvar;
    }
//    macete para gerar cpf
    ArrayList<Integer> listaAleatoria = new ArrayList();
    ArrayList<Integer> listaNumerosMultiplicados = null;

    public int gerarNumeroAleatorio() {
        int num = (int) (Math.random() * 10);
        return num;
    }

    public boolean verificarCPF(String val) {
        if (val.equals("___.___.___-__") || val.isEmpty() || val.equals(null)) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList geraCPFParcial() {
        listaAleatoria = new ArrayList();
        for (int i = 0; i < 9; i++) {
            listaAleatoria.add(gerarNumeroAleatorio());
        }
        return listaAleatoria;
    }

    public ArrayList geraPrimeiroDigito() {
        listaNumerosMultiplicados = new ArrayList();
        int primeiroDigito;
        int total = 0;
        int resto = 0;
        int peso = 10;

        for (int item : listaAleatoria) {
            listaNumerosMultiplicados.add(item * peso);
            peso--;
        }
        for (int item : listaNumerosMultiplicados) {
            total += item;
        }
        resto = (total % 11);
        if (resto < 2) {
            primeiroDigito = 0;
        } else {
            primeiroDigito = 11 - resto;
        }
        listaAleatoria.add(primeiroDigito);
        return listaAleatoria;
    }

    public ArrayList geraSegundoDigito() {
        listaNumerosMultiplicados = new ArrayList();
        int SegundoDigito;
        int total = 0;
        int resto;
        int peso = 11;

        for (int item : listaAleatoria) {
            listaNumerosMultiplicados.add(item * peso);
            peso--;
        }
        for (int item : listaNumerosMultiplicados) {
            total += item;
        }
        resto = (total % 11);
        if (resto < 2) {
            SegundoDigito = 0;
        } else {
            SegundoDigito = 11 - resto;
        }
        listaAleatoria.add(SegundoDigito);
        return listaAleatoria;
    }

    public void geraCPF() {
        geraCPFParcial();
        geraPrimeiroDigito();
        geraSegundoDigito();
        String texto = "";

        for (int item : listaAleatoria) {
            texto += item;
        }
        paciente.setCpf(texto);
        System.out.println(paciente.getCpf());
    }
    Date data_inicio_cadastro;
    Date data_fim_cadastro = new Date();
    Date data_inicio_nasc;
    Date date_fim_nasc = new Date();

    public Date getData_inicio_cadastro() {
        return data_inicio_cadastro;
    }

    public void setData_inicio_cadastro(Date data_inicio_cadastro) {
        this.data_inicio_cadastro = data_inicio_cadastro;
    }

    public Date getData_fim_cadastro() {
        return data_fim_cadastro;
    }

    public void setData_fim_cadastro(Date data_fim_cadastro) {
        this.data_fim_cadastro = data_fim_cadastro;
    }

    public Date getData_inicio_nasc() {
        return data_inicio_nasc;
    }

    public void setData_inicio_nasc(Date data_inicio_nasc) {
        this.data_inicio_nasc = data_inicio_nasc;
    }

    public Date getDate_fim_nasc() {
        return date_fim_nasc;
    }

    public void setDate_fim_nasc(Date date_fim_nasc) {
        this.date_fim_nasc = date_fim_nasc;
    }

    public void imprimirListagemPaciente() {

        if (getData_inicio_cadastro() == null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.add(gc.YEAR, -100);
            Date d2 = gc.getTime();
            setData_inicio_cadastro(d2);
        }
        if (getData_fim_cadastro() == null) {
            setData_fim_cadastro(new Date());
        }
        if (getData_inicio_nasc() == null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.add(gc.YEAR, -200);
            Date d2 = gc.getTime();
            setData_inicio_nasc(d2);
        }
        if (getDate_fim_nasc() == null) {
            setDate_fim_nasc(new Date());
        }
        System.out.println(data_inicio_cadastro + "ini cad");
        System.out.println(data_fim_cadastro + "fim cad");
        System.out.println(data_inicio_nasc + "ini nasc");
        System.out.println(date_fim_nasc + "fim nasc");

        RelatorioFactory.ListagemPaciente("/WEB-INF/Relatorios/reportListagemPaciente.jasper", us.retornaUsuario(), data_inicio_cadastro, data_fim_cadastro, data_inicio_nasc, date_fim_nasc);
    }

    public Paciente getPacienteSegundoPlano() {
        return pacienteSegundoPlano;
    }

    public void setPacienteSegundoPlano(Paciente pacienteSegundoPlano) {
        this.pacienteSegundoPlano = pacienteSegundoPlano;
    }
}
