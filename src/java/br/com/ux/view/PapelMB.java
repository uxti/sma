/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.PapelEJB;
import br.com.ux.model.Papel;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PapelMB implements Serializable {

    @EJB
    PapelEJB pEJB;
    private Papel papel;
    UsuarioSessao us = new UsuarioSessao();

    public PapelMB() {
        papel = new Papel();
    }

    public void novo() {
        papel = new Papel();
    }

    public Papel getPapel() {
        return papel;
    }

    public void Salvar() {
        if (papel.getIdPapel() != null) {
            try {
                if (papel.getRole() == null || papel.getRole().equals("")) {
                    papel.setRole("ROLE_ROOT");
                }
                pEJB.Salvar(papel, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("editar");
                papel = new Papel();
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        } else {
            try {
                if (papel.getRole() == null || papel.getRole().equals("")) {
                    papel.setRole("ROLE_ROOT");
                }
                pEJB.Salvar(papel, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("salvar");
                papel = new Papel();
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        }
    }

    public List<Papel> listarPermissoes() {
        return pEJB.listarPapeis();
    }

    public Papel selecionarPorID(Integer id) {
        return papel = pEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public void Excluir(Papel papel) {
        try {
            pEJB.Excluir(papel, us.retornaUsuario());
            Mensagem.addMensagem(1, "Papel excluído com sucesso!");
            BCRUtils.ResetarDatatableFiltros("formParametro:tbl");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar excluir!" + e);
        }
    }
}
