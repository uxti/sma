/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.TipoInternacaoEJB;
import br.com.ux.model.TipoInternacao;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author BrunoS
 */
@ManagedBean
@ViewScoped
public class TipoInternacaoMB implements Serializable {

    @EJB
    TipoInternacaoEJB tpEJB;
    TipoInternacao tipoInternacao;
    UsuarioSessao us = new UsuarioSessao();
    List<TipoInternacao> listaaCarregarLazy;
    LazyDataModel<TipoInternacao> model;

    public TipoInternacaoMB() {
        novo();
    }

    public void novo() {
        tipoInternacao = new TipoInternacao();
        model = new LazyDataModel<TipoInternacao>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<TipoInternacao> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(tpEJB.contarTipoInternacoesRegistro(Clausula).toString()));


                listaaCarregarLazy = tpEJB.listarTipoInternacoesLazyModeWhere(first, pageSize, Clausula);
//                listaPacientesLazy = pEJB.listarPacientesLazyMode(first, pageSize);
                return listaaCarregarLazy;
            }
        };
    }

    public void salvar() {
        if (tipoInternacao.getIdTipoInternacao() == null) {
            try {
                tpEJB.Salvar(tipoInternacao, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        } else {
            try {
                tpEJB.Salvar(tipoInternacao, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("editar");
            }
        }
    }

    public void excluir() {
        try {
            tpEJB.Excluir(tipoInternacao, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("excluir");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }

    public TipoInternacao selecionarPorID(Integer id) {
        return tipoInternacao = tpEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public List<TipoInternacao> listarTodos() {
        return tpEJB.listarTipoInternacoes();
    }
    //==========================================================================

    public TipoInternacaoEJB getTpEJB() {
        return tpEJB;
    }

    public void setTpEJB(TipoInternacaoEJB tpEJB) {
        this.tpEJB = tpEJB;
    }

    public TipoInternacao getTipoInternacao() {
        return tipoInternacao;
    }

    public void setTipoInternacao(TipoInternacao tipoInternacao) {
        this.tipoInternacao = tipoInternacao;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public List<TipoInternacao> getListaaCarregarLazy() {
        return listaaCarregarLazy;
    }

    public void setListaaCarregarLazy(List<TipoInternacao> listaaCarregarLazy) {
        this.listaaCarregarLazy = listaaCarregarLazy;
    }

    public LazyDataModel<TipoInternacao> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<TipoInternacao> model) {
        this.model = model;
    }
}
