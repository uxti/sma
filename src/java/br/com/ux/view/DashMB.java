/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.OrcamentoEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.model.Orcamento;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class DashMB implements Serializable {

    @EJB
    OrcamentoEJB oejb;
    private BarChartModel barOrcamentos;
    @EJB
    RepasseEJB rEJB;

    public DashMB() {
    }

    public BarChartModel getBarOrcamentos() {
        return barOrcamentos;
    }

    @PostConstruct
    public void init() {
        criarChart();
    }

    public void criarChart() {
        barOrcamentos = initBarModel();
        barOrcamentos.setTitle("Orçamentos");
        barOrcamentos.setAnimate(true);
        barOrcamentos.setLegendPosition("ne");
    }

    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
        List<Orcamento> lista = new ArrayList<Orcamento>();
        lista = oejb.listaOrcamentos();
        int contAbertos = 0;
        int contVencidos = 0;
        int contNaoRealizados = 0;
        int contRealizados = 0;
        int contCancelados = 0;
        int contAndamento = 0;

        for (Orcamento o : lista) {
            if ("Aberto".equals(o.getSituacao())) {
                contAbertos = contAbertos + 1;
            }
            if ("Vencido".equals(o.getSituacao())) {
                contVencidos = contVencidos + 1;
            }
            if ("Não Realizado".equals(o.getSituacao())) {
                contNaoRealizados = contNaoRealizados + 1;
            }
            if ("Realizado".equals(o.getSituacao())) {
                contRealizados = contRealizados + 1;
            }
            if ("Cancelado".equals(o.getSituacao())) {
                contCancelados = contCancelados + 1;
            }
            if ("Andamento".equals(o.getSituacao())) {
                contAndamento = contAndamento + 1;
            }
        }

        ChartSeries abertos = new ChartSeries();
        abertos.setLabel("Abertos");
        abertos.set("Aberto", contAbertos);

        ChartSeries Vencido = new ChartSeries();
        Vencido.setLabel("Vencido");
        Vencido.set("Vencido", contVencidos);

        ChartSeries nRealizado = new ChartSeries();
        nRealizado.setLabel("Não Realizados");
        nRealizado.set("Não Realizados", contNaoRealizados);

        ChartSeries Realizado = new ChartSeries();
        Realizado.setLabel("Realizado");
        Realizado.set("Realizado", contRealizados);


        ChartSeries Cancelado = new ChartSeries();
        Cancelado.setLabel("Cancelado");
        Cancelado.set("Cancelado", contCancelados);

        ChartSeries Andamento = new ChartSeries();
        Andamento.setLabel("Andamento");
        Andamento.set("Andamento", contAndamento);

        model.addSeries(abertos);
        model.addSeries(Vencido);
        model.addSeries(nRealizado);
        model.addSeries(Realizado);
        model.addSeries(Cancelado);
        model.addSeries(Andamento);

        return model;
    }

    public Long recebimentosVencidos() {
        return rEJB.contarRecebimentosVencidos();
    }

    public Long recebimentosVencendoHoje() {
        return rEJB.contarRecebimentosVencendoHoje();
    }

    public Long contaRepasses() {
        return rEJB.contarRecebimentos();
    }

    public Double totalReceber() {
        return rEJB.totalAReceber();
    }

    public Double arredondar(Double val) {
        return Math.ceil(val);
    }
}
