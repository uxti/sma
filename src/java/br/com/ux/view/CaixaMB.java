/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.OperacaoEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Caixa;
import br.com.ux.model.CaixaPai;
import br.com.ux.model.Operacao;
import br.com.ux.model.Usuario;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class CaixaMB implements Serializable {

    @EJB
    CaixaEJB cEJB;
    private List<Caixa> listaTipada;
    UsuarioSessao us = new UsuarioSessao();
    @EJB
    OperacaoEJB oEJB;
    @EJB
    UsuarioEJB uEJB;
    private Caixa caixa;
    private CaixaPai caixaPai;
    private CaixaPai caixaPaiTransferencia;
    private Operacao operacao;
    private Operacao operacaoTransferencia;
    private Usuario usuario;
    private double valorTotal = 0D;

    public CaixaMB() {
        novo();
    }

    public void novo() {
        caixa = new Caixa();
        operacao = new Operacao();
        operacaoTransferencia = new Operacao();
        usuario = new Usuario();
        caixaPai = new CaixaPai();
        caixaPaiTransferencia = new CaixaPai();
        listaTipada = new ArrayList<Caixa>();
        caixaPai.setDtCaixaPai(new Date());
    }

   

   
}
