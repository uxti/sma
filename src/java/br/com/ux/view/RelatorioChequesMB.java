/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.ChequeEJB;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.RelatorioFactory;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class RelatorioChequesMB {

    @EJB
    ChequeEJB cEJB;
    private Date dtIni, dtFim, dtCompIni, dtCompFim;
    private String status, situacao, tipo, movimento;
    private Integer idProcedimento;

    public RelatorioChequesMB() {
        dtIni = BCRUtils.primeiroDiaDoMes(new Date());
        dtFim = BCRUtils.ultimoDiaDoMes(new Date());
        dtCompIni = BCRUtils.primeiroDiaDoMes(new Date());
        dtCompFim = BCRUtils.ultimoDiaDoMes(new Date());
    }

    public void imprimirRelatorio() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("logo", "C://SMA//logo.png");
        map.put("dtIni", dtIni);
        map.put("dtFim", dtFim);
        map.put("dtCompIni", dtCompIni);
        map.put("dtCompFim", dtCompFim);
        map.put("pTipo", tipo);
        map.put("pMovimento", movimento);
//        if (idProcedimento == 0) {
//            map.put("idProcedimento", null);
//        } else {
//            map.put("idProcedimento", idProcedimento);
//        }        
        //System.out.println(idProcedimento);
        map.put("parametro", status);
        map.put("pSituacao", situacao);        
     
        RelatorioFactory.Relatorio("/WEB-INF/Relatorios/reportListagemCheques.jasper", map);
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        this.dtIni = dtIni;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDtCompIni() {
        return dtCompIni;
    }

    public void setDtCompIni(Date dtCompIni) {
        this.dtCompIni = dtCompIni;
    }

    public Date getDtCompFim() {
        return dtCompFim;
    }

    public void setDtCompFim(Date dtCompFim) {
        this.dtCompFim = dtCompFim;
    }

    public Integer getIdProcedimento() {
        return idProcedimento;
    }

    public void setIdProcedimento(Integer idProcedimento) {
        this.idProcedimento = idProcedimento;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMovimento() {
        return movimento;
    }

    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }
    
    
}
