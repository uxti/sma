/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.model.Associacao;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class AssociacaoMB implements Serializable {

    @EJB
    AssociacaoEJB aEJB;
    private Associacao associacao;
    UsuarioSessao us = new UsuarioSessao();

    public AssociacaoMB() {
        associacao = new Associacao();
    }

    @PostConstruct
    public void carregarAssociacao() {
        associacao = aEJB.carregarAssociacao();
    }

    public Associacao getAssociacao() {
        return associacao;
    }

    public void setAssociacao(Associacao associacao) {
        this.associacao = associacao;
    }
    
     public Integer retornaPrazoCompensacaoCheque(){
         carregarAssociacao();
         return associacao.getPrazoCompensacaoCheque();
    }
     
     public Integer retornaDiasCompensacaoChequeHospital(){
         carregarAssociacao();
         return associacao.getDiasCompensarChequeHospital();
    }


    public void Salvar() {
        try {
            if (associacao.getDiferencaTolerada() > 1) {
                Mensagem.addMensagem(3, "Diferença tolerada deve ser inferior a R$ 1,00");
            } else {
                aEJB.Salvar(associacao, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("salvar");
                associacao = getAssociacao();
            }
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
        }
    }
}
