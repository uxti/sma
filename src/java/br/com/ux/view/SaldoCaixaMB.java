/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.SaldoCaixaEJB;
import br.com.ux.controller.UsuarioEJB;
import java.io.Serializable;
import br.com.ux.model.MovimentoCaixa;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import static br.com.ux.util.BCRUtils.addMes;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.Map;

/**
 *
 * @author charl
 */
@ViewScoped
@ManagedBean
public class SaldoCaixaMB implements Serializable {

    @EJB
    ContaCorrenteEJB cEJB;

    @EJB
    UsuarioEJB uEJB;
    private Usuario usuario;
    private UsuarioSessao us = new UsuarioSessao();

    @EJB
    SaldoCaixaEJB scEJB;
    private MovimentoCaixa movimentoCaixa;
    private List<MovimentoCaixa> listaMovimentoCaixa;

    private Date dataAtual, PDtIni, PDtFim, dtSaldoInicial;
    private Double saldoInicial, totalEntrada, totalSaida;
    private Boolean SaldoDiario;
    private Double valorSaldoInicial;
    private String tipoMovimentacao, descricaoMovimentacao;

    public SaldoCaixaMB() {
        novo();
    }

    public void novo() {
        movimentoCaixa = new MovimentoCaixa();
        dataAtual = new Date();
        PDtIni = dataAtual;
        PDtFim = dataAtual;
        saldoInicial = 0D;
        totalEntrada = 0D;
        totalSaida = 0D;
        tipoMovimentacao = "";
        descricaoMovimentacao = "";

    }

    public void setarDatasPesquisaCaixa(String periodo) {
        if (periodo.equals("D")) {
            PDtIni = BCRUtils.addDia(dataAtual, 0);
            PDtFim = BCRUtils.addDia(dataAtual, 0);
            SaldoDiario = Boolean.TRUE;
        }
        if (periodo.equals("M")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(dataAtual);
            PDtFim = BCRUtils.ultimoDiaDoMes(dataAtual);
            SaldoDiario = Boolean.FALSE;
        }
        if (periodo.equals("A")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual, -1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual, -1));
            SaldoDiario = Boolean.FALSE;
        }
        if (periodo.equals("P")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual, 1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual, 1));
            SaldoDiario = Boolean.FALSE;
        }

        usuario = uEJB.selecionarPorUser(us.retornaUsuario());
        listaMovimentoCaixa = new ArrayList<>();

        pesquisaMovimentoCaixa(usuario.getIdUsuario());
    }

    public void pesquisaMovimentoCaixa(Integer usu) {
        listaMovimentoCaixa = new ArrayList<>();
        totalEntrada = 0D;
        totalSaida = 0D;

        MovimentoCaixa cxSaldoInicial = scEJB.saldoInicial(usu);
        MovimentoCaixa mcSaldoInicial = calcularSaldo(usu, cxSaldoInicial.getValor(), "Inicial", PDtIni, PDtFim);

        listaMovimentoCaixa.add(mcSaldoInicial);
        listaMovimentoCaixa.addAll(scEJB.listarMovimentoCaixaPorUsuario(usu, PDtIni, PDtFim, false));

        saldoInicial = mcSaldoInicial.getValor();
        dtSaldoInicial = mcSaldoInicial.getDtMovimentacao();

        if (!listaMovimentoCaixa.isEmpty()) {
            for (MovimentoCaixa c : listaMovimentoCaixa) {
                if (c.getTipoMovimentacao().equals("C")) {
                    totalEntrada = totalEntrada + c.getValor();
                } else if (c.getTipoMovimentacao().equals("D")) {
                    totalSaida = totalSaida + c.getValor();
                }
            }
            totalEntrada = totalEntrada - mcSaldoInicial.getValor();
        } else {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public MovimentoCaixa calcularSaldo(Integer usu, Double saldoAberturaCaixa, String inicialOuFinal, Date dtIni, Date dtFim) {

        Double totalEntradaTemp, totalSaidaTemp;
        List<MovimentoCaixa> listaTempMC = new ArrayList<>();
        MovimentoCaixa mc = new MovimentoCaixa();

        totalEntradaTemp = 0D;
        totalSaidaTemp = 0D;

        if (inicialOuFinal.equals("Inicial")) {
            Date newDtIni, newDtFim;
            newDtIni = BCRUtils.addAno(new Date(), -7);
            newDtFim = BCRUtils.addDia(dtIni, -1);
            listaTempMC = scEJB.listarMovimentoCaixaPorUsuario(usu, newDtIni, newDtFim, false);
        } else {
            Date newDtIni, newDtFim;
            newDtIni = BCRUtils.addAno(new Date(), -7);
            newDtFim = BCRUtils.addDia(dtFim, 0);
            listaTempMC = scEJB.listarMovimentoCaixaPorUsuario(usu, newDtIni, newDtFim, false);
        }

        for (MovimentoCaixa mc2 : listaTempMC) {
            if (mc2.getTipoMovimentacao().equals("C")) {
                totalEntradaTemp += mc2.getValor();
            } else {
                totalSaidaTemp += mc2.getValor();
            }
        }

        if (inicialOuFinal.equals("Inicial")) {
            mc.setDescricao("Saldo Inicial");
            mc.setValor((saldoAberturaCaixa + totalEntradaTemp) - totalSaidaTemp);
            mc.setDtMovimentacao(dtIni);
            mc.setTipoMovimentacao("C");
        } else {
            mc.setDescricao("Saldo Final");
            mc.setValor((saldoAberturaCaixa + totalEntradaTemp) - totalSaidaTemp);
            mc.setDtMovimentacao(dtFim);
            mc.setTipoMovimentacao("T");
        }
        return mc;
    }

    public void salvarValorSaldoInicial() {
        if (valorSaldoInicial != null) {
            Usuario u = uEJB.retornaUsuarioDaSessao();
            MovimentoCaixa cxSaldoInicial = new MovimentoCaixa();
            if (tipoMovimentacao.equals("S")) {
                cxSaldoInicial = scEJB.saldoInicial(u.getIdUsuario());
                cxSaldoInicial.setTipoMovimentacao("C");
                cxSaldoInicial.setDescricao("Saldo Inicial");
            } else {
                cxSaldoInicial.setTipoMovimentacao(tipoMovimentacao);
                if (descricaoMovimentacao.isEmpty() || descricaoMovimentacao.equals("") ) {                    
                    descricaoMovimentacao = tipoMovimentacao.equals("D") ? "Saída avulsa" : "Entrada avulsa";
                }
                cxSaldoInicial.setDescricao(descricaoMovimentacao);
            }
            cxSaldoInicial.setDtMovimentacao(new Date());
            cxSaldoInicial.setIdUsuario(u);
            cxSaldoInicial.setValor(valorSaldoInicial);
            scEJB.Salvar(cxSaldoInicial);
            pesquisaMovimentoCaixa(u.getIdUsuario());
            Mensagem.addMensagem(1, "Saldo de abertura do caixa atualizado com sucesso.");
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public MovimentoCaixa getMovimentoCaixa() {
        return movimentoCaixa;
    }

    public void setMovimentoCaixa(MovimentoCaixa movimentoCaixa) {
        this.movimentoCaixa = movimentoCaixa;
    }    

    public Date getPDtIni() {
        return PDtIni;
    }

    public Date getDataAtual() {
        return dataAtual;
    }

    public void setDataAtual(Date dataAtual) {
        this.dataAtual = dataAtual;
    }

    public void setPDtIni(Date PDtIni) {
        this.PDtIni = PDtIni;
    }

    public Date getPDtFim() {
        return PDtFim;
    }

    public void setPDtFim(Date PDtFim) {
        this.PDtFim = PDtFim;
    }

    public List<MovimentoCaixa> getListaMovimentoCaixa() {
        return listaMovimentoCaixa;
    }

    public void setListaMovimentoCaixa(List<MovimentoCaixa> listaMovimentoCaixa) {
        this.listaMovimentoCaixa = listaMovimentoCaixa;
    }

    public Date getDtSaldoInicial() {
        return dtSaldoInicial;
    }

    public void setDtSaldoInicial(Date dtSaldoInicial) {
        this.dtSaldoInicial = dtSaldoInicial;
    }

    public Double getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(Double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public Double getTotalEntrada() {
        return totalEntrada;
    }

    public void setTotalEntrada(Double totalEntrada) {
        this.totalEntrada = totalEntrada;
    }

    public Double getTotalSaida() {
        return totalSaida;
    }

    public void setTotalSaida(Double totalSaida) {
        this.totalSaida = totalSaida;
    }

    public Boolean getSaldoDiario() {
        return SaldoDiario;
    }

    public void setSaldoDiario(Boolean SaldoDiario) {
        this.SaldoDiario = SaldoDiario;
    }

    public Double getValorSaldoInicial() {
        return valorSaldoInicial;
    }

    public void setValorSaldoInicial(Double valorSaldoInicial) {
        this.valorSaldoInicial = valorSaldoInicial;
    }

    public String getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(String tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public String getDescricaoMovimentacao() {
        return descricaoMovimentacao;
    }

    public void setDescricaoMovimentacao(String descricaoMovimentacao) {
        this.descricaoMovimentacao = descricaoMovimentacao;
    }

}
