/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.MovimentoUsuarioEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.SaldoCaixaEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.Extenso;
import br.com.ux.util.Mensagem;
import br.com.ux.util.RelatorioFactory;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class MovimentoUsuarioMB extends Conexao implements Serializable {

    @EJB
    MovimentoUsuarioEJB muEJB;
    @EJB
    AuditoriaEJB aEJB;
    @EJB
    SaldoCaixaEJB scEJB;

    private Caixa caixa;
    private List<Caixa> listaCaixas;
    private List<Caixa> itensParaSeremTransferidos;
    private List<Caixa> listaCaixasTransferir;
    private List<Caixa> listaCaixasDetalhados;
    private Usuario usuario;
    private Usuario usuarioTransferencia;
    private Usuario usuarioHospital;
    private Paciente paciente;
    private UsuarioSessao us = new UsuarioSessao();
    private Calendar dataAtual, primeiroDia, ultimoDia;
    private String obs, lotePesquisar = "", fechamentoPesquisar, cpf;
    private Long numAleatorio;
    private String tipoTransferencia;
    private String transferenciaSelecionadaPorId;
    private boolean SaldoDiario;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    RepasseEJB rEJB;
    @EJB
    CaixaEJB cxEJB;
    @EJB
    AssociacaoEJB assEJB;
    @EJB
    ContaCorrenteEJB ccEJB;
    @EJB
    TerceiroEJB tEJB;

    // Controle de Transferencia //
    private Double valorTotalListaTransferir, valorSelecionadoTransferir, valorRestanteTransferir, valorSaldoInicial;
    // Controle de Honorários //
    private Double valorHonorariosMedicos, valorHonorariosHospital, valorHonorariosTotal;
    private int qdeHonorariosHospitalares, qdeHonorariosMedicos;
    private String loteImprimir;
    private LazyDataModel<Caixa> model;

    private Double saldoInicial, saldoEntradas, saldoSaidas, saldoFinal;
    public Double valorTaxaDeposito;
    ContaCorrente conta;
    private String emitirRecibo;
    private Date dtSaldoInicial;
    private Double valorHospital;
    private Double valorMedico;

    /**
     * Creates a new instance of MovimentoUsuarioMB
     */
    public MovimentoUsuarioMB() {
        novo();
    }

    public void novo() {
        caixa = new Caixa();
        paciente = new Paciente();
        listaCaixas = new ArrayList<>();
        itensParaSeremTransferidos = new ArrayList<>();
        listaCaixasTransferir = new ArrayList<>();
        usuario = new Usuario();
        dataAtual = Calendar.getInstance();
        primeiroDia = Calendar.getInstance();
        ultimoDia = Calendar.getInstance();
        PDtIni = dataAtual.getTime();
        PDtFim = dataAtual.getTime();
        usuarioHospital = new Usuario();
        transferenciaSelecionadaPorId = "";
        valorTaxaDeposito = 0D;

//        model = new LazyDataModel<Caixa>() {
//            private static final long serialVersionUID = 1L;
//
//            @Override
//            public List<Caixa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
//                String Clausula = "";
//                StringBuffer sf = new StringBuffer();
//                int contar = 0;
//
//                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
//                    String filterProperty = it.next(); // table column name = field name  
//                    Object filterValue = filters.get(filterProperty);
//                    System.out.println(filterProperty);
//                    if (contar == 0) {
//                        if (sf.toString().contains("where")) {
//                            sf.append("and c." + filterProperty + " like'%" + filterValue + "%'");
//                        } else {
//                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
//                        }
//                    } else {
//                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
//                    }
//                    contar = contar + 1;
//                }
//                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
//                    sf.append(" order by c." + sortField + " asc");
//                }
//                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
//                    sf.append(" order by c." + sortField + " desc");
//                }
//                Clausula = sf.toString();
//
//                if (Clausula.contains("_")) {
//                    Clausula = Clausula.replace("_", "");
//                }
//                if (Clausula.contains("() -")) {
//                    Clausula = Clausula.replace("() -", "");
//                }
//                if (Clausula.contains("..")) {
//                    Clausula = Clausula.replace("..", "");
//                }
//
//
//                setRowCount(Integer.parseInt(muEJB.contarCaixasRegistro(!uEJB.retornaUsuarioDaSessao().getIdPapel().getVerTodasTransferencias(), Clausula).toString()));
//                
//                return listaCaixas;
//            }
//
//            @Override
//            public Object getRowKey(Caixa cx) {
//                return cx.getIdCirurgia().getIdCirurgia();
//            }
//
//            @Override
//            public Caixa getRowData(String rowKey) {
//                Integer id = Integer.valueOf(rowKey);
//
//                for (Caixa cx : listaCaixas) {
//                    if (id.equals(cx.getIdCirurgia().getIdCirurgia())) {
//                        return cx;
//                    }
//                }
//                return null;
//            }
//        };
    }

    public void retornaTransferencias() {
        listaCaixas = muEJB.listarCaixasTransferidos(!uEJB.retornaUsuarioDaSessao().getIdPapel().getVerTodasTransferencias());
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }
    private Double totalEntrada;
    private Double totalSaida;

    public List<Caixa> getListaCaixas() {
        totalEntrada = 0D;
        totalSaida = 0D;
        if (!listaCaixas.isEmpty()) {
            for (Caixa c : listaCaixas) {
                if (c.getTipoLancamento() == 'C') {
                    totalEntrada = totalEntrada + c.getValor();
                } else {
                    totalSaida = totalSaida + c.getValor();
                }

            }
        }
        return listaCaixas;
    }

    public Double getTotalEntrada() {
        return totalEntrada;
    }

    public void setTotalEntrada(Double totalEntrada) {
        this.totalEntrada = totalEntrada;
    }

    public Double getTotalSaida() {
        return totalSaida;
    }

    public void setTotalSaida(Double totalSaida) {
        this.totalSaida = totalSaida;
    }

    public void setListaCaixas(List<Caixa> listaCaixas) {
        this.listaCaixas = listaCaixas;
    }

    public List<Caixa> getItensParaSeremTransferidos() {
        return itensParaSeremTransferidos;
    }

    public void setItensParaSeremTransferidos(List<Caixa> itensParaSeremTransferidos) {
        this.itensParaSeremTransferidos = itensParaSeremTransferidos;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    //parametros
    private Date PDtIni;
    private Date PDtFim;

    public Date getPDtIni() {
        return PDtIni;
    }

    public void setPDtIni(Date PDtIni) {
        this.PDtIni = PDtIni;
    }

    public Date getPDtFim() {
        return PDtFim;
    }

    public void setPDtFim(Date PDtFim) {
        this.PDtFim = PDtFim;
    }

    //
    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public static Date addAno(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public void setarDatasPesquisa(String periodo) {
        if (periodo.equals("D")) {
            PDtIni = dataAtual.getTime();
            PDtFim = dataAtual.getTime();
        }
        if (periodo.equals("M")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(dataAtual.getTime());
            PDtFim = BCRUtils.ultimoDiaDoMes(dataAtual.getTime());
        }
        if (periodo.equals("A")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual.getTime(), -1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual.getTime(), -1));
        }
        if (periodo.equals("P")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual.getTime(), 1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual.getTime(), 1));
        }
        usuario = uEJB.selecionarPorUser(us.retornaUsuario());
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String tipoPesquisa = params.get("tipo");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        listaCaixas = new ArrayList<Caixa>();

        if (tipoPesquisa != null) {
            pesquisaMovimentoTransferencia(usuario.getIdUsuario(), df.format(PDtIni), df.format(PDtFim));
        } else {
            pesquisaMovimento(usuario.getIdUsuario());
        }

    }

    public void setarDatasPesquisaCaixa(String periodo) {
        if (periodo.equals("D")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(BCRUtils.addAno(dataAtual.getTime(), -7));
            PDtFim = BCRUtils.addDia(dataAtual.getTime(), 1);
            SaldoDiario = Boolean.TRUE;
        }
        if (periodo.equals("M")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(dataAtual.getTime());
            PDtFim = BCRUtils.ultimoDiaDoMes(dataAtual.getTime());
            SaldoDiario = Boolean.FALSE;
        }
        if (periodo.equals("A")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual.getTime(), -1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual.getTime(), -1));
            SaldoDiario = Boolean.FALSE;
        }
        if (periodo.equals("P")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual.getTime(), 1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual.getTime(), 1));
            SaldoDiario = Boolean.FALSE;
        }
        usuario = uEJB.selecionarPorUser(us.retornaUsuario());
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String tipoPesquisa = params.get("tipo");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        listaCaixas = new ArrayList<Caixa>();

        System.out.println("Aqui 1");
        pesquisaMovimentoCaixa(usuario.getIdUsuario());

    }

    public void pesquisaMovimento(Integer usu) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dtIni = df.format(PDtIni);
        String dtFim = df.format(PDtFim);
        System.out.println(lotePesquisar);
        if (lotePesquisar == null || lotePesquisar.equals("")) {
            if (uEJB.selecionarPorID(usu).getVerTransferencias()) {
                System.out.println("entrou");
                listaCaixas = muEJB.listarCaixasAgrupados(dtIni, dtFim);
                System.out.println("saiu");
            } else {
                listaCaixas = muEJB.listarCaixasAgrupadosPorUsuario(usu, dtIni, dtFim);
            }
        } else {
            listaCaixas = muEJB.listarCaixasAgrupadosPorLote(lotePesquisar);
        }

        if (listaCaixas.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public void salvarValorSaldoInicial() {
        if (valorSaldoInicial != null) {
            Usuario u = uEJB.retornaUsuarioDaSessao();
            Caixa cxSaldoInicial = muEJB.saldoInicial(u.getIdUsuario());
            if (cxSaldoInicial == null) {
                cxSaldoInicial = new Caixa();
                cxSaldoInicial.setDtCaixa(BCRUtils.addAno(new Date(), -7));
                cxSaldoInicial.setTipoLancamento('C');
                cxSaldoInicial.setHistorico("Saldo Inicial");
                cxSaldoInicial.setStatus("Baixado");
                cxSaldoInicial.setIdUsuario(u);
            }
            cxSaldoInicial.setValor(valorSaldoInicial);
            cxEJB.Salvar(cxSaldoInicial);
            pesquisaMovimentoCaixa(u.getIdUsuario());
            Mensagem.addMensagem(1, "Saldo inicial atualizado com sucesso");
        }
    }

    public void pesquisaMovimentoCaixa(Integer usu) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dtIni = df.format(PDtIni);
        String dtFim = df.format(PDtFim);

        listaCaixas = new ArrayList<>();
        Caixa cxSaldoInicial = muEJB.saldoInicial(usu);
        listaCaixas.add(cxSaldoInicial);
        listaCaixas.addAll(muEJB.listarMovimentoCaixaPorUsuario(usu, dtIni, dtFim));

        // CHARLES 07072017
        // Seta saldo inicial de acordo com o lançamento de um registro de saldo inicial.
        if (cxSaldoInicial != null) {
            saldoInicial = cxSaldoInicial.getValor();
            dtSaldoInicial = cxSaldoInicial.getDtCaixa();
        } else {
            saldoInicial = 0D;
            dtSaldoInicial = new Date();
        }
        totalEntrada = 0D;
        totalSaida = 0D;

        if (listaCaixas.size() > 0) {
            for (Caixa c : listaCaixas) {
                if (c.getTipoLancamento().equals('C')) {
                    totalEntrada = totalEntrada + c.getValor();
                } else {
                    totalSaida = totalSaida + c.getValor();
                }
            }
        } else {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public void pesquisaMovimentoTransferencia(Integer usu, String dtIni, String dtFim) {
        if (uEJB.selecionarPorID(usu).getVerTransferencias()) {
            System.out.println("aqui");
            listaCaixas = muEJB.listarCaixasAgrupadosTransferencia(dtIni, dtFim);
            System.out.println("saiu");
        } else {
            System.out.println("aqui2");

            listaCaixas = muEJB.listarCaixasAgrupadosTransferenciaPorUsuario(usu, dtIni, dtFim);
        }

        if (listaCaixas.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public List<Caixa> listaItensTransferidos(Integer usuario) {
        if (uEJB.selecionarPorID(usuario).getVerTransferencias()) {
            return muEJB.listarCaixasAgrupados();
        } else {
            return muEJB.listarCaixasAgrupadosTransferidos(usuario);

        }
    }

    public boolean verificarSeTemRepasseMedicos(String loteOrigem) {
        List<Usuario> listaUsuarios = new ArrayList<>();
        listaUsuarios = uEJB.retornaUsuariosPorTransferencia(loteOrigem);
        for (Usuario u : listaUsuarios) {
            if (u.getReceberRepasse()) {
                return true;
            }
        }
        return false;

    }

    public boolean verificarSeTemRepasseHospitalares(String loteOrigem) {
        List<Usuario> listaUsuarios = new ArrayList<>();
        listaUsuarios = uEJB.retornaUsuariosPorTransferencia(loteOrigem);
        //System.out.println("Lote: " + loteOrigem);
        for (Usuario usuario : listaUsuarios) {
            if (usuario.getReceberRepasseHospital()) {
                return true;
            }
        }
        return false;

    }

    public List<Caixa> listaItensCaixaTransferidos(String loteOrigem) {
        if (!loteOrigem.isEmpty() || !loteOrigem.equals("")) {
            return muEJB.listarCaixasTransferencia(loteOrigem);
        } else {
            return null;
        }

    }

    public List<Caixa> listaItensCaixaTransferidosDetalhados(String loteOrigem, Integer idUsuario) {
        listaCaixasDetalhados = new ArrayList<Caixa>();
        listaCaixasDetalhados = muEJB.listarCaixasTransferenciaDetalhado(loteOrigem, idUsuario);
        return listaCaixasDetalhados;
    }

    public List<Caixa> listaItensCaixaTransferidosDetalhadosPorFechamento(String loteOrigem, Integer idUsuario, Integer idCirurgia) {
        listaCaixasDetalhados = new ArrayList<Caixa>();
        listaCaixasDetalhados = muEJB.listarCaixasTransferenciaDetalhadoPorFechamento(loteOrigem, idUsuario, idCirurgia);
        return listaCaixasDetalhados;
    }

    public List<Repasse> listaRepassesPorCaixa(Integer idCaixa) {
        return muEJB.listaRepassesPorCaixa(idCaixa);
    }

    public List<Repasse> listaRepassesItensTransferidos(List<Repasse> listaRepasse, boolean Hospital) {
        List<Repasse> listaRepasses = new ArrayList<Repasse>();
        if (!Hospital) {
            for (Repasse repasse : listaRepasse) {
                if (!repasse.getIdTerceiro().getTerceiroMedHosp()) {
                    listaRepasses.add(repasse);
                }
            }
        } else {
            for (Repasse repasse : listaRepasse) {
                if (repasse.getIdTerceiro().getTerceiroMedHosp()) {
                    listaRepasses.add(repasse);
                }
            }
        }
        return listaRepasses;
    }
    private Double valorTotalTransferido;
    private Double valorTotalBaixado;

    public Double getValorTotalTransferido() {
        return valorTotalTransferido;
    }

    public void setValorTotalTransferido(Double valorTotalTransferido) {
        this.valorTotalTransferido = valorTotalTransferido;
    }

    public Double getValorTotalBaixado() {
        return valorTotalBaixado;
    }

    public void setValorTotalBaixado(Double valorTotalBaixado) {
        this.valorTotalBaixado = valorTotalBaixado;
    }

    public List<Caixa> verificarItensParaTransferir() {
        List<Caixa> itens = new ArrayList<Caixa>();
        for (Caixa c : itensParaSeremTransferidos) {
            itens.addAll(muEJB.listaCaixaItensPorLote(c.getNumLote()));
        }
        valorTotalBaixado = 0D;
        valorTotalTransferido = 0D;
        for (Caixa c : itens) {
            if (c.getIdCirurgia() != null) {
                valorTotalTransferido = valorTotalTransferido + c.getValor();
            }

        }
        return itens;
    }

    public void descontarTaxas(ContaCorrente cc) {
        // CHARLES 1107217
        // DEDUZIR TAXAS QUANDO FOR DEPOSITO BANCÁRIO

        if (assEJB.carregarAssociacao().getTaxaDescontoRepasseDeposito() != null) {
            if (assEJB.carregarAssociacao().getTaxaDescontoRepasseDeposito() != 0D) {

                Double taxa = assEJB.carregarAssociacao().getTaxaDescontoRepasseDeposito();
                Double valorDescontado = (cc.getValor() * taxa) / 100;
                Double novoValor = cc.getValor() - ((cc.getValor() * taxa) / 100);

                // CHARLES 28072017
                // Gerar novo repasse ao hospital do valor descontado da taxa
                Repasse rep = new Repasse();
                rep.setIdCirurgia(cc.getIdCirurgia());
                rep.setNumeroParcela(cc.getIdRepasse().getIdCheque().getNumeroParcela());
                rep.setIdCheque(cc.getIdRepasse().getIdCheque());
                rep.setIdPaciente(cc.getIdPaciente());
                rep.setValor(BCRUtils.arredondar(valorDescontado));
                rep.setDtLancamento(cc.getIdRepasse().getIdCheque().getDtRecebimentoEmissao());
                rep.setDtRecebimento(cc.getIdRepasse().getIdCheque().getDtRecebimentoEmissao());
                rep.setDtVencimento(cc.getIdRepasse().getIdCheque().getDtCompensacaoVencimento());
                rep.setIdTerceiro(tEJB.retornaCadastroHospital());
                rep.setTipoPagamento('P');
                rep.setPercentualRecebido(100D);
                rep.setSituacaoPaciente('P');
                rep.setStatus('F');
                rep.setValor(0D);
                rep.setValorRecebido(BCRUtils.arredondar(valorDescontado));
                rep.setValorDocumento(BCRUtils.arredondar(valorDescontado));
                rep.setFormaPagamento("Depósito Bancário");
                rep.setSituacaoTerceiro('N');
                // CHARLES 14112019
                // Desabiliado por causar duplicidade
                //rEJB.SalvarSimples(rep);

                // CHARLES 28072017
                // Gerar nova conta corrente ao hospital do valor descontado da taxa
                ContaCorrente cce = new ContaCorrente();
                cce.setIdContaCorrente(null);
                cce.setIdTerceiro(rep.getIdTerceiro());
                cce.setIdRepasse(rep);
                cce.setIdCirurgia(cc.getIdCirurgia());
                cce.setIdPaciente(cc.getIdPaciente());
                cce.setIdCheque(rep.getIdCheque().getIdCheque());
                cce.setTipo("C");
                cce.setDtLancamento(new Date());
                cce.setSituacao("Não Liberado");
                cce.setStatus("Não Retirado");
                cce.setTaxaDeposito(Boolean.TRUE);
                cce.setValor(BCRUtils.arredondar(valorDescontado));
                cce.setObservacao("Repasse gerado pela taxa de depósito referente ao fechamento nº " + cc.getIdCirurgia().getIdCirurgia() + " do paciente " + rep.getIdPaciente().getNome() + ". Origem: " + cc.getIdRepasse().getIdRepasse());
                ccEJB.SalvarSimples(cce, null);

                // CHARLES 28072017
                // Atualiza o valor na conta corrente e no repasse atual
                cc.setValor(novoValor);
                cc.getIdRepasse().setValorRecebido(novoValor);
                cc.getIdRepasse().setValorDocumento(novoValor);
                rEJB.SalvarSimples(cc.getIdRepasse());
                cc.setTaxado(Boolean.TRUE);
                ccEJB.AtualizarCC(cc);
            }
        }
    }

    public void novaTransferencia(Date dtInicial, Date dtFinal) {
        numAleatorio = 0L;
        numAleatorio = new Date().getTime();
        usuarioTransferencia = new Usuario();
        listaCaixasTransferir = new ArrayList<Caixa>();
        usuario = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dtIni = df.format(dtInicial);
        String dtFim = df.format(dtFinal);

        if (assEJB.carregarAssociacao().getTransferirFechamentoPerfil()) {
            listaCaixasTransferir = muEJB.listarCaixasAgrupadosTransferenciaPorPerfil(usuario.getIdPapel().getIdPapel(), dtIni, dtFim);
        } else {
            listaCaixasTransferir = muEJB.listarCaixasAgrupadosTransferencia(usuario.getIdUsuario(), dtIni, dtFim);
        }

        valorTotalListaTransferir = 0D;
        for (Caixa cx : listaCaixasTransferir) {
            valorTotalListaTransferir = valorTotalListaTransferir + cx.getValor();
        }
        calcularValores();
    }

    public void setarLoteEmSelecionados() {
        valorSelecionadoTransferir = 0D;
        valorRestanteTransferir = 0D;

        valorHospital = 0D;
        valorMedico = 0D;

        for (Caixa cx : itensParaSeremTransferidos) {
            cx.setNumLote(numAleatorio.toString());
            valorSelecionadoTransferir = valorSelecionadoTransferir + cx.getValor();
            valorRestanteTransferir = valorTotalListaTransferir - valorSelecionadoTransferir;
        }
    }

    public void calcularValores() {
        valorSelecionadoTransferir = 0D;
        valorRestanteTransferir = 0D;

        for (Caixa cx : itensParaSeremTransferidos) {
            valorSelecionadoTransferir = valorSelecionadoTransferir + cx.getValor();
            valorRestanteTransferir = valorTotalListaTransferir - valorSelecionadoTransferir;
        }
    }

    public void transferenciaEntreUsuarios() {
        List<String> lotes = new ArrayList<String>();
        try {
            for (Caixa cx : itensParaSeremTransferidos) {
                Date d = new Date();
                List<Caixa> itens = new ArrayList<Caixa>();
                itens.addAll(muEJB.retornaListaCaixaPorFechamentoParaTransferencia(cx.getIdCirurgia().getIdCirurgia()));
                for (Caixa c : itens) {
                    System.out.println("Transferindo caixa id" + c.getIdCaixa());
                    String loteOrigem = cx.getNumLote();
                    loteImprimir = loteOrigem;
                    int Contem = 0;
                    for (String l : lotes) {
                        if (l.equals(loteOrigem)) {
                            Contem = 1;
                        }
                    }
                    if (Contem != 1) {
                        lotes.add(loteOrigem);
                    }
                    if (c.getIdCheque().getTipo() == null) {
                        muEJB.atualizarCheque(c.getIdCheque().getIdCheque(), retornaTipoCheque(c.getIdCheque()));
                    }

                    if (c.getIdCirurgia() != null) {
                        if ((c.getIdCheque().getTipo().equals("M")) && (c.getIdCheque().getMovimento().equals("C"))) {
                            //System.out.println("Entrou no Misto e Cheque");
                            // Se o tipo do cheque for misto (Hospital e médicos) ou Honorários hospitalares.
                            usuarioHospital = uEJB.retornaUsuarioRepasseHospital();
                            muEJB.atualizarChequeCaixa(c.getIdCheque().getIdCheque(), usuarioHospital);
                            c.setStatus("Baixado");
                            c.setSituacao("Transferido");
                            muEJB.Salvar(c);

                            // lançou os debitos no caixa no valor integral do Cheque para usuario padrão Hospital
                            Caixa cDebito = new Caixa();
                            cDebito = c;
                            cDebito.setIdCaixa(null);
                            cDebito.setTipoLancamento('D');
                            cDebito.setDtCaixa(d);
                            cDebito.setSituacao("Recebido");
                            cDebito.setLoteOrigem(loteOrigem);
                            cDebito.setNumLote("");
                            cDebito.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                            cDebito.setIdUsuarioDestino(usuarioHospital);
                            cDebito.setHistorico(c.getHistorico());
                            muEJB.SalvarPersist(cDebito);

                            // Lança o crédito no valor integral do Cheque para usuario padrão Hospital
                            Caixa cTransferencia = new Caixa();
                            cTransferencia = c;
                            cTransferencia.setIdCaixa(null);
                            cTransferencia.setTipoLancamento('C');
                            cTransferencia.setStatus("Não Baixado");
                            cTransferencia.setSituacao("Aguardando Aceite");
                            cTransferencia.setLoteOrigem(loteOrigem);
                            cTransferencia.setNumLote(numAleatorio.toString());
                            cTransferencia.setLiberado("S");
                            cTransferencia.setIdUsuario(usuarioHospital);
                            cTransferencia.setIdUsuarioDestino(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                            cTransferencia.setDtCaixa(d);
                            cTransferencia.getIdCheque().setIdUsuario(usuarioHospital);
                            cTransferencia.setHistorico(c.getHistorico());
                            muEJB.SalvarPersist(cTransferencia);
                            for (Repasse r : cTransferencia.getIdCheque().getRepasseList()) {
                                r.setIdCaixa(cTransferencia);
                                rEJB.Salvar(r);
                            }
                            aEJB.Salvar(BCRUtils.criarAuditoria("transferencia", c.getNumLote() + " - Pagamento ID: " + c.getIdCheque().getIdCheque().toString() + "para o usuário: " + usuarioHospital.getUsuario(), us.retornaUsuario(), new Date(), "Movimentação"));
                        } else {
                            //System.out.println("Entrou no 'não é Cheque'");

                            // Se o tipo do cheque for nulo ou individual (Tipo = "N" ou "H")
                            muEJB.atualizarChequeCaixa(c.getIdCheque().getIdCheque(), usuarioTransferencia);
                            c.setStatus("Baixado");
                            c.setSituacao("Transferido");
                            muEJB.Salvar(c);

                            if (c.getIdCheque().getTipo().equals("M")) {
                                usuarioHospital = uEJB.retornaUsuarioRepasseHospital();
                                valorHonorariosHospital = 0D;
                                valorHonorariosMedicos = 0D;
                                valorHonorariosTotal = 0D;
                                for (Repasse r : c.getIdCheque().getRepasseList()) {
                                    String tipoServico = "";
                                    try {
                                        if (r.getIdSici().getIdServicoItem().getIdServico().getTipo() == null) {
                                            tipoServico = "N";
                                        } else {
                                            tipoServico = r.getIdSici().getIdServicoItem().getIdServico().getTipo();
                                        }
                                    } catch (NullPointerException e) {
                                        tipoServico = "N";
                                    }

                                    if ("H".equals(tipoServico)) {
                                        valorHonorariosHospital = valorHonorariosHospital + r.getValorDocumento();
                                    } else if ("N".equals(tipoServico)) {
                                        valorHonorariosMedicos = valorHonorariosMedicos + r.getValorDocumento();
                                    }
                                    valorHonorariosTotal = valorHonorariosTotal + r.getValorDocumento();
                                }
                                // lançou os debitos no caixa referente ao hospital
                                Caixa cDebitoHospital = new Caixa();
                                cDebitoHospital = c;
                                cDebitoHospital.setIdCaixa(null);
                                cDebitoHospital.setTipoLancamento('D');
                                cDebitoHospital.setDtCaixa(d);
                                cDebitoHospital.setSituacao("Recebido");
                                cDebitoHospital.setLoteOrigem(loteOrigem);
                                cDebitoHospital.setNumLote("");
                                cDebitoHospital.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                                cDebitoHospital.setIdUsuarioDestino(usuarioHospital);
                                cDebitoHospital.setHistorico(c.getHistorico());
                                cDebitoHospital.setValor(valorHonorariosHospital);
                                muEJB.SalvarPersist(cDebitoHospital);
                                //System.out.println("Débito de: " + valorHonorariosHospital + " para o hospital");
                                //System.out.println("Do usuário: " + cDebitoHospital.getIdUsuario().getUsuario() + " para o usuário " + cDebitoHospital.getIdUsuarioDestino().getUsuario());

                                // Lança créditos no caixa referente ao hospital
                                Caixa cTransferenciaHospital = new Caixa();
                                cTransferenciaHospital = c;
                                cTransferenciaHospital.setIdCaixa(null);
                                cTransferenciaHospital.setTipoLancamento('C');
                                cTransferenciaHospital.setStatus("Não Baixado");
                                cTransferenciaHospital.setSituacao("Aguardando Aceite");
                                cTransferenciaHospital.setLoteOrigem(loteOrigem);
                                cTransferenciaHospital.setNumLote(numAleatorio.toString());
                                cTransferenciaHospital.setLiberado("S");
                                cTransferenciaHospital.setIdUsuario(usuarioHospital);
                                cTransferenciaHospital.setIdUsuarioDestino(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                                cTransferenciaHospital.setDtCaixa(d);
                                cTransferenciaHospital.getIdCheque().setIdUsuario(usuarioHospital);
                                cTransferenciaHospital.setHistorico(c.getHistorico());
                                cTransferenciaHospital.setValor(valorHonorariosHospital);
                                muEJB.SalvarPersist(cTransferenciaHospital);
                                for (Repasse r : cTransferenciaHospital.getIdCheque().getRepasseList()) {
                                    if (r.getIdTerceiro().getTerceiroMedHosp()) {
                                        r.setIdCaixa(cTransferenciaHospital);
                                        rEJB.Salvar(r);
                                    }
                                }
                                aEJB.Salvar(BCRUtils.criarAuditoria("transferencia", cTransferenciaHospital.getNumLote() + " - Pagamento ID: " + cTransferenciaHospital.getIdCheque().getIdCheque().toString() + "para o usuário: " + usuarioHospital.getUsuario(), us.retornaUsuario(), new Date(), "Movimentação"));
                                //System.out.println("Crédito de: " + valorHonorariosHospital + " para o hospital");
                                //System.out.println("Do usuário: " + cTransferenciaHospital.getIdUsuario().getUsuario() + " para o usuário " + cTransferenciaHospital.getIdUsuarioDestino().getUsuario());

                                // lançou os debitos no caixa
                                Caixa cDebitoMedicos = new Caixa();
                                cDebitoMedicos = c;
                                cDebitoMedicos.setIdCaixa(null);
                                cDebitoMedicos.setTipoLancamento('D');
                                cDebitoMedicos.setDtCaixa(d);
                                cDebitoMedicos.setSituacao("Recebido");
                                cDebitoMedicos.setStatus("Baixado");
                                cDebitoMedicos.setLiberado(null);
                                cDebitoMedicos.setLoteOrigem(loteOrigem);
                                cDebitoMedicos.setNumLote("");
                                cDebitoMedicos.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                                cDebitoMedicos.setIdUsuarioDestino(usuarioTransferencia);
                                cDebitoMedicos.setHistorico(c.getHistorico());
                                cDebitoMedicos.setValor(valorHonorariosMedicos);
                                muEJB.SalvarPersist(cDebitoMedicos);
                                //System.out.println("Débito de: " + valorHonorariosMedicos + " para os médicos");
                                //System.out.println("Do usuário: " + cDebitoHospital.getIdUsuarioDestino().getUsuario() + " para o usuário " + cDebitoHospital.getIdUsuario().getUsuario());

                                // Lança créditos no caixa referente aos médicos
                                Caixa cTransferenciaMedicos = new Caixa();
                                cTransferenciaMedicos = c;
                                cTransferenciaMedicos.setIdCaixa(null);
                                cTransferenciaMedicos.setTipoLancamento('C');
                                cTransferenciaMedicos.setStatus("Não Baixado");
                                cTransferenciaMedicos.setSituacao("Aguardando Aceite");
                                cTransferenciaMedicos.setLoteOrigem(loteOrigem);
                                cTransferenciaMedicos.setNumLote(numAleatorio.toString());
                                cTransferenciaMedicos.setLiberado("S");
                                cTransferenciaMedicos.setIdUsuario(usuarioTransferencia);
                                cTransferenciaMedicos.setIdUsuarioDestino(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                                cTransferenciaMedicos.setDtCaixa(d);
                                cTransferenciaMedicos.getIdCheque().setIdUsuario(usuarioTransferencia);
                                cTransferenciaMedicos.setHistorico(c.getHistorico());
                                cTransferenciaMedicos.setValor(valorHonorariosMedicos);
                                muEJB.SalvarPersist(cTransferenciaMedicos);
                                for (Repasse r : cTransferenciaMedicos.getIdCheque().getRepasseList()) {
                                    if (!r.getIdTerceiro().getTerceiroMedHosp()) {
                                        r.setIdCaixa(cTransferenciaMedicos);
                                        rEJB.Salvar(r);
                                    }
                                }
                                aEJB.Salvar(BCRUtils.criarAuditoria("transferencia", cTransferenciaMedicos.getNumLote() + " - Pagamento ID: " + cTransferenciaMedicos.getIdCheque().getIdCheque().toString() + "para o usuário: " + usuarioHospital.getUsuario(), us.retornaUsuario(), new Date(), "Movimentação"));
                                //System.out.println("Crédito de: " + valorHonorariosHospital + " para o hospital");
                                //System.out.println("Do usuário: " + cTransferenciaHospital.getIdUsuario().getUsuario() + " para o usuário " + cTransferenciaHospital.getIdUsuarioDestino().getUsuario());
                            } else {
                                //System.out.println("Exclusivo");
                                if (c.getIdCheque().getTipo().equals("H")) {
                                    usuarioHospital = uEJB.retornaUsuarioRepasseHospital();
                                }
                                // lançou os debitos no caixa
                                Caixa cDebito = new Caixa();
                                cDebito = c;
                                cDebito.setIdCaixa(null);
                                cDebito.setTipoLancamento('D');
                                cDebito.setDtCaixa(d);
                                cDebito.setSituacao("Recebido");
                                cDebito.setLoteOrigem(loteOrigem);
                                cDebito.setNumLote("");
                                cDebito.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                                cDebito.setIdUsuarioDestino(c.getIdCheque().getTipo().equals("H") ? usuarioHospital : usuarioTransferencia);
                                cDebito.setHistorico(c.getHistorico());
                                muEJB.SalvarPersist(cDebito);

                                // Lança transferencia
                                Caixa cTransferencia = new Caixa();
                                cTransferencia = c;
                                cTransferencia.setIdCaixa(null);
                                cTransferencia.setTipoLancamento('C');
                                cTransferencia.setStatus("Não Baixado");
                                cTransferencia.setSituacao("Aguardando Aceite");
                                cTransferencia.setLoteOrigem(loteOrigem);
                                cTransferencia.setNumLote(numAleatorio.toString());
                                cTransferencia.setLiberado("S");
                                cTransferencia.setIdUsuario(c.getIdCheque().getTipo().equals("H") ? usuarioHospital : usuarioTransferencia);
                                cTransferencia.setIdUsuarioDestino(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                                cTransferencia.setDtCaixa(d);
                                cTransferencia.getIdCheque().setIdUsuario(c.getIdCheque().getTipo().equals("H") ? usuarioHospital : usuarioTransferencia);
                                cTransferencia.setHistorico(c.getHistorico());
                                muEJB.SalvarPersist(cTransferencia);
                                for (Repasse r : cTransferencia.getIdCheque().getRepasseList()) {
                                    r.setIdCaixa(cTransferencia);
                                    rEJB.Salvar(r);
                                }
                                aEJB.Salvar(BCRUtils.criarAuditoria("transferencia", cTransferencia.getNumLote() + " - Pagamento ID: " + cTransferencia.getIdCheque().getIdCheque().toString() + "para o usuário: " + cTransferencia.getIdUsuario().getUsuario(), us.retornaUsuario(), new Date(), "Movimentação"));

                            }
                        }
                    }
                }
            }
            verificarItensParaTransferir();
            setarDatasPesquisa("D");
            Mensagem.addMensagem(1, "Transferência efetuada com sucesso!");
            RequestContext.getCurrentInstance().execute("PF('dlgImprimir').show();");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar transferir! Entre em contato com o suporte!");
            System.out.println(e);
        }

    }

    public String retornaTipoCheque(Cheque chq) {
        qdeHonorariosHospitalares = 0;
        qdeHonorariosMedicos = 0;
        for (Repasse rep : chq.getRepasseList()) {
            if ("N".equals(rep.getIdSici().getIdServicoItem().getIdServico().getTipo())) {
                qdeHonorariosMedicos++;
            } else if ("H".equals(rep.getIdSici().getIdServicoItem().getIdServico().getTipo())) {
                qdeHonorariosHospitalares++;
            }
        }
        if (qdeHonorariosHospitalares > 0 && qdeHonorariosMedicos > 0) {
            return "M";
        } else if (qdeHonorariosHospitalares > 0) {
            return "H";
        } else if (qdeHonorariosMedicos > 0) {
            return "M";
        } else {
            return "";
        }
    }

    public void testarImpressao() {
        List<String> lotes = new ArrayList<String>();
        try {
            for (Caixa cc : itensParaSeremTransferidos) {
                List<Caixa> itens = new ArrayList<Caixa>();
                itens.addAll(muEJB.listaCaixaItensPorLote(cc.getNumLote()));
                for (Caixa c : itens) {
                    String loteOrigem = c.getNumLote();
                    int Contem = 0;
                    for (String l : lotes) {
                        if (l.equals(loteOrigem)) {
                            Contem = 1;
                        }
                    }
                    if (Contem != 1) {
                        lotes.add(loteOrigem);
                    }

                }

            }
            RelatorioFactory.ComprovanteTransferencia("/WEB-INF/Relatorios/Comprovante_Transferencia.jasper", lotes);
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar transferir! Tente novamente!");
            System.out.println(e);
        }
    }

    public void imprimir(String lote) {
        loteImprimir = lote;
    }

    public void imprimirReciboPaciente(ContaCorrente cc) {
        conta = cc;
    }

    public void imprimirReciboPagamento() {
        Extenso e = new Extenso();
        String valorExtenso = e.write(new BigDecimal(conta.getValor()));
        String pagador = "";
        String cpfRecibo = "";
        switch (emitirRecibo) {
            case "P":
                pagador = conta.getIdPaciente().getNome();
                cpfRecibo = conta.getIdPaciente().getCpf();
                break;
            case "A":
                pagador = conta.getIdRepasse().getIdCheque().getEmitente() == null ? conta.getIdPaciente().getNome() : conta.getIdRepasse().getIdCheque().getEmitente();
                cpfRecibo = conta.getIdRepasse().getIdCheque().getEmitente() == null ? conta.getIdPaciente().getCpf() : conta.getIdRepasse().getIdCheque().getCpf();
                break;
            default:
                pagador = obs;
                cpfRecibo = cpf;
                break;
        }
        RelatorioFactory.reciboPagamento("/WEB-INF/Relatorios/reportReciboPagamento.jasper", "C:/SMA/logo.png", conta.getValor(), valorExtenso, pagador, conta.getIdTerceiro().getNome(), conta.getIdTerceiro().getCgccpf(), cpfRecibo);
    }

    public void imprimirTransferencia(String lote) {
        List<Integer> listaCaixa;
        listaCaixa = muEJB.listarCaixasTransferenciaDetalhadoPorLote(lote);
        Double valorHosp = 0D;
        Double valorMed = 0D;
        RelatorioFactory.ComprovanteTransferenciaIndividual("/WEB-INF/Relatorios/Comprovante_Transferencia.jasper", listaCaixa, valorMed, valorHosp, "C:/SMA/logo.png");
    }

    public Double retornaValorComOuSemTaxaDeposito(ContaCorrente cc) {
        if (cc.getTaxaDeposito() != null) {
            return cc.getTaxaDeposito() ? 0D : cc.getIdRepasse().getValorDocumento();
        }

        if (cc.getTaxado() != null) {
            return cc.getTaxado() ? ((cc.getIdRepasse().getValorDocumento() * 100) / (100 - assEJB.carregarAssociacao().getTaxaDescontoRepasseDeposito())) : cc.getIdRepasse().getValorDocumento();
        }

        return cc.getIdRepasse().getValorDocumento();
    }

    public void imprimirTransferenciaMedicos() {
        List<Integer> listaCaixa;
        Integer idUsuario = 0;
        idUsuario = uEJB.retornaUsuarioRepasseHospital().getIdUsuario();
        listaCaixa = muEJB.listarCaixasTransferenciaMedicos(loteImprimir, idUsuario);
        List<Caixa> listCaixas = muEJB.listarCaixasTransferenciaMedicosObjeto(loteImprimir, idUsuario);

        Double valorHosp = 0D;
        Double valorMed = 0D;

        for (Caixa cx : listCaixas) {
            for (Repasse r : cx.getRepasseList()) {
                for (ContaCorrente cc : r.getContaCorrenteList()) {
                    if (cc.getIdTerceiro().getTerceiroMedHosp()) {
                        valorHosp += retornaValorComOuSemTaxaDeposito(cc);
                    } else {
                        valorMed += retornaValorComOuSemTaxaDeposito(cc);
                    }
                }
            }
        }

        System.out.println(valorHosp);
        System.out.println(valorMed);

        RelatorioFactory.ComprovanteTransferenciaIndividual(
                "/WEB-INF/Relatorios/Comprovante_Transferencia_Individual.jasper", listaCaixa, valorMed, valorHosp, "C:/SMA/logo.png");
    }

    public void imprimirTransferenciaMedicosAgrupados() {
        List<Integer> listaCaixa;
        Integer idUsuario = 0;
        idUsuario = uEJB.retornaUsuarioRepasseHospital().getIdUsuario();
        listaCaixa = muEJB.listarCaixasTransferenciaMedicos(loteImprimir, idUsuario);

        Double valorHosp = 0D;
        Double valorMed = 0D;
        RelatorioFactory.ComprovanteTransferenciaIndividual("/WEB-INF/Relatorios/Comprovante_Transferencia_Individual_Agrupado.jasper", listaCaixa, valorMed, valorHosp, "C:/SMA/logo.png");
    }

    public void imprimirTransferenciaSelecionadas() {
        List<Integer> listaCaixa = new ArrayList<>();
        List<Integer> listaDetalhada = new ArrayList<>();
        for (Caixa c : listaCaixasTransferir) {
            listaDetalhada.add(c.getIdCirurgia().getIdCirurgia());
        }
        Integer idUsuario = 0;
        idUsuario = uEJB.retornaUsuarioRepasseHospital().getIdUsuario();
        listaCaixa = muEJB.listarCaixasTransferenciaPorFechamentoMedicos(listaDetalhada, idUsuario);
        List<Caixa> listCaixas = muEJB.listarCaixasTransferenciaMedicosObjeto(loteImprimir, idUsuario);

        Double valorHosp = 0D;
        Double valorMed = 0D;

        for (Caixa cx : listCaixas) {
            for (Repasse r : cx.getRepasseList()) {
                for (ContaCorrente cc : r.getContaCorrenteList()) {
                    if (cc.getIdTerceiro().getTerceiroMedHosp()) {
                        valorHosp += retornaValorComOuSemTaxaDeposito(cc);
                    } else {
                        valorMed += retornaValorComOuSemTaxaDeposito(cc);
                    }
                }
            }
        }
        RelatorioFactory.ComprovanteTransferenciaIndividual("/WEB-INF/Relatorios/Comprovante_Transferencia_Individual.jasper", listaCaixa, valorMed, valorHosp, "C:/SMA/logo.png");
    }

    public void selecionaTransferencia(String id) {
        transferenciaSelecionadaPorId = id;
    }

    public String gerarHistorico(String loteOrigem) {
        Integer idUsuario = uEJB.retornaUsuarioRepasseHospital().getIdUsuario();
        List<Caixa> listCaixas = muEJB.listarCaixasTransferenciaMedicosObjeto(loteOrigem, idUsuario);

        Double valorHosp = 0D;
        Double valorMed = 0D;

        StringBuilder fechamentos = new StringBuilder();

        for (Caixa cx : listCaixas) {
            for (Repasse r : cx.getRepasseList()) {
                for (ContaCorrente cc : r.getContaCorrenteList()) {
                    if (cc.getIdTerceiro().getTerceiroMedHosp()) {
                        valorHosp += retornaValorComOuSemTaxaDeposito(cc);
                    } else {
                        valorMed += retornaValorComOuSemTaxaDeposito(cc);
                    }
                }
            }
        }
        return fechamentos.append("Transferência - Total: R$ ").append(String.format("%1$,.2f", valorMed)).append(" (Médicos) : R$ ").append(String.format("%1$,.2f", valorHosp)).append(" (Hospital).").toString();
    }

    public Double retornaValorTransferenciaPorTipo(String lote, Integer idUsuario, String tipo) {
        List<Caixa> listCaixas = muEJB.listarCaixasTransferenciaMedicosObjeto(lote, idUsuario);

        Double valorHosp = 0D;
        Double valorMed = 0D;

        for (Caixa cx : listCaixas) {
            for (Repasse r : cx.getRepasseList()) {
                for (ContaCorrente cc : r.getContaCorrenteList()) {
                    if (cc.getIdTerceiro().getTerceiroMedHosp()) {
                        valorHosp += retornaValorComOuSemTaxaDeposito(cc);
                    } else {
                        valorMed += retornaValorComOuSemTaxaDeposito(cc);
                    }
                }
            }
        }
        return tipo.equals("M") ? valorMed : valorHosp;
    }

    public void imprimirTransferenciaPorUsuario(String loteOrigem, Integer idUsuario) {
        List<Integer> listaCaixa = new ArrayList<Integer>();
        listaCaixa = muEJB.listarCaixasTransferenciaDetalhado2(loteOrigem, idUsuario);
        List<Caixa> listCaixas = muEJB.listarCaixasTransferenciaMedicosObjeto(loteImprimir, idUsuario);

        Double valorHosp = 0D;
        Double valorMed = 0D;

        for (Caixa cx : listCaixas) {
            for (Repasse r : cx.getRepasseList()) {
                for (ContaCorrente cc : r.getContaCorrenteList()) {
                    if (cc.getIdTerceiro().getTerceiroMedHosp()) {
                        valorHosp += retornaValorComOuSemTaxaDeposito(cc);
                    } else {
                        valorMed += retornaValorComOuSemTaxaDeposito(cc);
                    }
                }
            }
        }
        RelatorioFactory.ComprovanteTransferenciaIndividual("/WEB-INF/Relatorios/Comprovante_Transferencia_Individual.jasper", listaCaixa, valorMed, valorHosp, "C:/SMA/logo.png");
    }

    public void aceitarTransferencia(String numLote, Integer idUser) {
        try {
            muEJB.aceitarTransferenciaCaixa(numLote);
            Integer idUsuario = uEJB.retornaUsuarioRepasseHospital().getIdUsuario();

            Double valor = retornaValorTransferenciaPorTipo(numLote, idUsuario, "M");
            scEJB.Salvar(BCRUtils.criarMovimentoCaixa("C", "Transferência", new Date(), valor, numLote, null, uEJB.selecionarPorID(idUser)));
            pesquisaMovimento(idUser);
            Mensagem.addMensagem(1, "Transferência aceito com sucesso!");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar aceitar a transferência. Tente novamente!");
            System.out.println(e);
        }
    }

    public void recusarTransferencia(String numLote, Integer idUser) {
        try {
            muEJB.recusarTransferencia(numLote, idUser);
            Mensagem.addMensagem(1, "Transferência recusada.");
            //pesquisaMovimento(idUser);
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar recusar a transferência. Tente novamente!");
            System.out.println(e);
        }
    }

    public String retornaUsuario() {
        String usuarioLog = "";
        Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (usuarioLogado instanceof UserDetails) {
            return usuarioLog = ((UserDetails) usuarioLogado).getUsername();
        } else {
            return usuarioLog = usuarioLogado.toString().toUpperCase();
        }
    }

    public Usuario RetornaLogadoUsuario() {
        uEJB.verificaUsuarioAdministradorExiste();
        return usuario = uEJB.retornaUsuarioPorUsername(retornaUsuario());
    }

    public boolean permiteReverterTransferencia(String lote) {
        List<Caixa> caixas = muEJB.listarCaixaPorNumeroLote(lote);
        for (Caixa cx : caixas) {
            if (cx.getSituacao().equals("Aceito")) {
                return false;
            }
        }
        return true;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Long getNumAleatorio() {
        return numAleatorio;
    }

    public void setNumAleatorio(Long numAleatorio) {
        this.numAleatorio = numAleatorio;
    }

    public List<Caixa> getListaCaixasTransferir() {
        return listaCaixasTransferir;
    }

    public void setListaCaixasTransferir(List<Caixa> listaCaixasTransferir) {
        this.listaCaixasTransferir = listaCaixasTransferir;
    }

    public Double getValorTotalListaTransferir() {
        return valorTotalListaTransferir;
    }

    public void setValorTotalListaTransferir(Double valorTotalListaTransferir) {
        this.valorTotalListaTransferir = valorTotalListaTransferir;
    }

    public Double getValorSelecionadoTransferir() {
        return valorSelecionadoTransferir;
    }

    public void setValorSelecionadoTransferir(Double valorSelecionadoTransferir) {
        this.valorSelecionadoTransferir = valorSelecionadoTransferir;
    }

    public Double getValorRestanteTransferir() {
        return valorRestanteTransferir;
    }

    public void setValorRestanteTransferir(Double valorRestanteTransferir) {
        this.valorRestanteTransferir = valorRestanteTransferir;
    }

    public Usuario getUsuarioTransferencia() {
        return usuarioTransferencia;
    }

    public void setUsuarioTransferencia(Usuario usuarioTransferencia) {
        this.usuarioTransferencia = usuarioTransferencia;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public String getTipoTransferencia() {
        return tipoTransferencia;
    }

    public void setTipoTransferencia(String tipoTransferencia) {
        this.tipoTransferencia = tipoTransferencia;
    }

    public Usuario getUsuarioHospital() {
        return usuarioHospital;
    }

    public void setUsuarioHospital(Usuario usuarioHospital) {
        this.usuarioHospital = usuarioHospital;
    }

    public List<Caixa> getListaCaixasDetalhados() {
        return listaCaixasDetalhados;
    }

    public void setListaCaixasDetalhados(List<Caixa> listaCaixasDetalhados) {
        this.listaCaixasDetalhados = listaCaixasDetalhados;
    }

    public String getLoteImprimir() {
        return loteImprimir;
    }

    public void setLoteImprimir(String loteImprimir) {
        this.loteImprimir = loteImprimir;
    }

    public String getTransferenciaSelecionadaPorId() {
        return transferenciaSelecionadaPorId;
    }

    public void setTransferenciaSelecionadaPorId(String transferenciaSelecionadaPorId) {
        this.transferenciaSelecionadaPorId = transferenciaSelecionadaPorId;
    }

    public LazyDataModel<Caixa> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Caixa> model) {
        this.model = model;
    }

    public String getLotePesquisar() {
        return lotePesquisar;
    }

    public void setLotePesquisar(String lotePesquisar) {
        this.lotePesquisar = lotePesquisar;
    }

    public String getFechamentoPesquisar() {
        return fechamentoPesquisar;
    }

    public void setFechamentoPesquisar(String fechamentoPesquisar) {
        this.fechamentoPesquisar = fechamentoPesquisar;
    }

    public ContaCorrente getConta() {
        return conta;
    }

    public void setConta(ContaCorrente conta) {
        this.conta = conta;
    }

    public String getEmitirRecibo() {
        return emitirRecibo;
    }

    public void setEmitirRecibo(String emitirRecibo) {
        this.emitirRecibo = emitirRecibo;
    }

    public Double getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(Double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public Double getSaldoEntradas() {
        return saldoEntradas;
    }

    public void setSaldoEntradas(Double saldoEntradas) {
        this.saldoEntradas = saldoEntradas;
    }

    public Double getSaldoSaidas() {
        return saldoSaidas;
    }

    public void setSaldoSaidas(Double saldoSaidas) {
        this.saldoSaidas = saldoSaidas;
    }

    public Double getSaldoFinal() {
        return saldoFinal;
    }

    public void setSaldoFinal(Double saldoFinal) {
        this.saldoFinal = saldoFinal;
    }

    public Date getDtSaldoInicial() {
        return dtSaldoInicial;
    }

    public void setDtSaldoInicial(Date dtSaldoInicial) {
        this.dtSaldoInicial = dtSaldoInicial;
    }

    public boolean isSaldoDiario() {
        return SaldoDiario;
    }

    public void setSaldoDiario(boolean SaldoDiario) {
        this.SaldoDiario = SaldoDiario;
    }

    public Calendar getDataAtual() {
        return dataAtual;
    }

    public void setDataAtual(Calendar dataAtual) {
        this.dataAtual = dataAtual;
    }

    public Double getValorSaldoInicial() {
        return valorSaldoInicial;
    }

    public void setValorSaldoInicial(Double valorSaldoInicial) {
        this.valorSaldoInicial = valorSaldoInicial;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Double getValorTaxaDeposito() {
        return valorTaxaDeposito;
    }

    public void setValorTaxaDeposito(Double valorTaxaDeposito) {
        this.valorTaxaDeposito = valorTaxaDeposito;
    }

    public Double getValorHospital() {
        return valorHospital;
    }

    public void setValorHospital(Double valorHospital) {
        this.valorHospital = valorHospital;
    }

    public Double getValorMedico() {
        return valorMedico;
    }

    public void setValorMedico(Double valorMedico) {
        this.valorMedico = valorMedico;
    }

}
