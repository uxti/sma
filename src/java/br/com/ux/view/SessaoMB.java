/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.controller.integracao.SincronizacaoEJB;
import br.com.ux.model.Associacao;
import br.com.ux.model.Usuario;
import br.com.ux.util.AgendadorEJB;
import br.com.ux.util.Mensagem;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author charl
 */
@ManagedBean
@ViewScoped
public class SessaoMB implements Serializable {

    @EJB
    UsuarioEJB uEJB;
    @EJB
    AssociacaoEJB aEJB;
    @EJB
    AgendadorEJB agEJB;

    private Usuario usuario;
    private List<Usuario> listaUsuario;
    private Associacao associacao;
    private String log, senhaAntiga, novaSenha;
    private UIInput bindingSenha;

    public SessaoMB() {
        usuario = new Usuario();
    }

    public boolean verificarConfiguracoes() throws FileNotFoundException {
        boolean baseTeste = false;
        if (aEJB.carregarAssociacao().getBaseTeste() != null) {
            baseTeste = aEJB.carregarAssociacao().getBaseTeste();
        }
        return baseTeste;
    }

    public String retornaUsuario() {
        String usuarioLog = "";
        Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        if (usuarioLogado instanceof UserDetails) {
            return usuarioLog = ((UserDetails) usuarioLogado).getUsername();
        } else {
            return usuarioLog = usuarioLogado.toString().toUpperCase();
        }
    }

    @Autowired
    private SessionRegistryImpl sessionRegistry;

    public void listarUsuariosLogados() {
        System.out.println("Obj: " + getSessionRegistry());

        List<Object> principals = sessionRegistry.getAllPrincipals();
        List<String> usersNamesList = new ArrayList<String>();

        for (Object principal : principals) {
            if (principal instanceof Usuario) {
                usersNamesList.add(((Usuario) principal).getUsuario());
            }
        }
    }

    public void rotinasAutomaticas() throws InterruptedException, IOException {
        agEJB.reiniciarServidor();
    }

    public Usuario getUsuario() {
        try {
            return usuario = uEJB.retornaUsuarioPorUsername(retornaUsuario());
        } catch (Exception e) {
            System.out.println(e);
            return usuario = uEJB.retornaUsuarioPorUsername(retornaUsuario());
        }

    }

    public boolean verificaTipoUsuario() {
        if (usuario.getIdPapel().getDescricao().equals("Medico")) {
            return false;
        } else {
            return true;
        }
    }

    public String montaMenuConfiguracao() {
        if (getUsuario().getIdPapel().getMenuusuario() == false && getUsuario().getIdPapel().getMenupermissao() == false
                && getUsuario().getIdPapel().getMenuauditoria() == false && getUsuario().getIdPapel().getMenuassociacao() == false
                && getUsuario().getIdPapel().getMenubackup() == false) {
            return "display: none;";
        } else {
            return "";
        }
    }

    public String montaMenuMovimentacao() {
        if (getUsuario().getIdPapel().getMenuorcamento() == false) {
            return "display: none;";
        } else {
            return "";
        }
    }

    public String montaMenuRelatorios() {
        if (!getUsuario().getIdPapel().getMenulistagemcirurgia() && !getUsuario().getIdPapel().getMenulistagemorcamento() && !getUsuario().getIdPapel().getRelatorioLancamentoAvulso()) {
            return "display: none;";
        } else {
            return "";
        }
    }

    public String montaMenuTesouraria() {
        if (getUsuario().getIdPapel().getMenucontrolecaixa() == false && getUsuario().getIdPapel().getMenulancarcaixa() == false
                && getUsuario().getIdPapel().getMenucontrolecaixa() == false && getUsuario().getIdPapel().getMenuestornorecebimento() == false && getUsuario().getIdPapel().getMenucirurgia() == false
                && getUsuario().getIdPapel().getMenulancarexame() == false && getUsuario().getIdPapel().getDashboard() == false) {
            return "display: none;";
        } else {
            return "";
        }
    }

    public String montaMenuCadastro() {
        if (getUsuario().getIdPapel().getMenupaciente() == false
                && getUsuario().getIdPapel().getMenuterceiro() == false
                && getUsuario().getIdPapel().getMenuitensorcto() == false
                && getUsuario().getIdPapel().getMenuexame() == false
                && getUsuario().getIdPapel().getMenumotivo() == false
                && getUsuario().getIdPapel().getMenubanco() == false
                && getUsuario().getIdPapel().getMenuconvenio() == false
                && getUsuario().getIdPapel().getMenuconvenio() == false
                && getUsuario().getIdPapel().getMenuprocedimento() == false
                && getUsuario().getIdPapel().getMenuespecialidade() == false
                && getUsuario().getIdPapel().getMenuunidadeatendimento() == false
                && getUsuario().getIdPapel().getMenuformapagto() == false
                && getUsuario().getIdPapel().getMenuoperacaocaixa() == false) {
            return "display: none;";
        } else {
            return "";
        }
    }

    public boolean Administrador() {
        if (getUsuario().getIdPapel().getDescricao().toLowerCase().contains("admin")) {
            return true;
        } else {
            return false;
        }
    }

    public String montaMenuCadastroGerais() {
        if (getUsuario().getIdPapel().getMenuexame() == false
                && getUsuario().getIdPapel().getMenumotivo() == false && getUsuario().getIdPapel().getMenubanco() == false
                && getUsuario().getIdPapel().getMenuconvenio() == false && getUsuario().getIdPapel().getMenuconvenio() == false
                && getUsuario().getIdPapel().getMenuprocedimento() == false && getUsuario().getIdPapel().getMenuespecialidade() == false
                && getUsuario().getIdPapel().getMenuunidadeatendimento() == false && getUsuario().getIdPapel().getMenuformapagto() == false
                && getUsuario().getIdPapel().getMenuoperacaocaixa() == false) {
            return "display: none;";
        } else {
            return "";
        }
    }

    public String tipoApp() {
        if (getApplicationUri().toLowerCase().contains("teste")) {
            return "Teste";
        } else {
            return "Produção";
        }
    }

    public static String getApplicationUri() {
        try {
            FacesContext ctxt = FacesContext.getCurrentInstance();
            ExternalContext ext = ctxt.getExternalContext();
            URI uri = new URI(ext.getRequestScheme(),
                    null, ext.getRequestServerName(), ext.getRequestServerPort(),
                    ext.getRequestContextPath(), null, null);
//            System.out.println(uri.toASCIIString());
            return uri.toASCIIString();
        } catch (URISyntaxException e) {
            throw new FacesException(e);
        }
    }

    public void verlog() {
        log = "";
        try {
            File diretorio = new File("C:\\glassfish3\\glassfish\\domains\\sma\\logs\\server.log");
            BufferedReader br;
            if (diretorio.exists()) {
                br = new BufferedReader(new FileReader("C:\\glassfish3\\glassfish\\domains\\sma\\logs\\server.log"));
            } else {
                br = new BufferedReader(new FileReader("C:\\glassfish3\\glassfish\\domains\\domain1\\logs\\server.log"));
            }
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            log = sb.toString();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void validateConfirmPassword(FacesContext context, UIComponent toValidate,
            Object value) throws ValidatorException {

        String passwordStr = (String) toValidate.getAttributes().get("oriPassword");
        String confirmPasswordStr = (String) value;
    }

    public static boolean validatePassword(final String password) {
        Pattern p = Pattern.compile("^(?=.[0-9])(?=.[a-z])");
        Matcher m = p.matcher(password);
        return m.matches();
    }

    public void atualizarSenha() {
        if (senhaAntiga.equals(usuario.getSenha())) {
            if (usuario.getTrocarSenha()) {
                usuario.setTrocarSenha(Boolean.FALSE);
            }
            try {
                uEJB.atualizarSenha(novaSenha, usuario.getIdUsuario());
                Mensagem.addMensagem(1, "Senha atualizada.");
                RequestContext.getCurrentInstance().execute("$('#modalPerfil').modal('hide')");
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Não foi possível atualizar sua senha. Entre em contato com o Suporte.");
                System.out.println(e);
            }
        } else {
            Mensagem.addMensagem(3, "Senha atual não confere.");
        }

    }

    public void limparLog() {
        log = "";
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getSenhaAntiga() {
        return senhaAntiga;
    }

    public void setSenhaAntiga(String senhaAntiga) {
        this.senhaAntiga = senhaAntiga;
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }

    public Associacao getAssociacao() {
        return associacao;
    }

    public void setAssociacao(Associacao associacao) {
        this.associacao = associacao;
    }

    public UIInput getBindingSenha() {
        return bindingSenha;
    }

    public void setBindingSenha(UIInput bindingSenha) {
        this.bindingSenha = bindingSenha;
    }

    public List<Usuario> getListaUsuario() {
        return listaUsuario;
    }

    public void setListaUsuario(List<Usuario> listaUsuario) {
        this.listaUsuario = listaUsuario;
    }

    public SessionRegistryImpl getSessionRegistry() {
        return sessionRegistry;
    }

    public void setSessionRegistry(SessionRegistryImpl sessionRegistry) {
        this.sessionRegistry = sessionRegistry;
    }

}
