/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.CirurgiaEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.DescricaoExameEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.ProcedimentoEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.TipoInternacaoEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Associacao;
import br.com.ux.model.Banco;
import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.DescricaoExame;
import br.com.ux.model.Paciente;
import br.com.ux.model.Procedimento;
import br.com.ux.model.Repasse;
import br.com.ux.model.ServicoItemCirurgiaItem;
import br.com.ux.model.TipoInternacao;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.RelatorioFactory;
import br.com.ux.util.UsuarioSessao;
import br.com.ux.util.ValidarCNPJ;
import br.com.ux.util.ValidarCPF;
import static br.com.ux.view.MovimentoUsuarioMB.addMes;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class LancamentoAvulsoMB {
    // EJB's

    @EJB
    PacienteEJB pEJB;
    @EJB
    TerceiroEJB tEJB;
    @EJB
    DescricaoExameEJB deEJB;
    @EJB
    RepasseEJB rEJB;
    @EJB
    ProcedimentoEJB proEJB;
    @EJB
    ChequeEJB chEJB;
    @EJB
    CirurgiaEJB cEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    AuditoriaEJB aEJB;
    @EJB
    CaixaEJB cxEJB;
    @EJB
    ContaCorrenteEJB ccEJB;
    @EJB
    TipoInternacaoEJB tiEJB;
    @EJB
    AssociacaoEJB asEJB;

    // Objetos
    private Cirurgia cirurgia;
    private CirurgiaItem cirurgiaItem;
    private DescricaoExame descricaoExame;
    private Repasse repasse;
    private Procedimento procedimento;
    private ContaCorrente contaCorrente;
    private Cheque cheque;
    private TipoInternacao tipoInternacao;
    private Caixa caixa;
    private Banco banco;
    private ServicoItemCirurgiaItem servicoItemCirurgiaItem;
    private UsuarioSessao us;
    // Listas
    private ArrayList<Paciente> listaPacientes;
    private List<Cirurgia> listaLancamentos;
    private ArrayList<CirurgiaItem> listaCirurgiaItens;
    private List<ServicoItemCirurgiaItem> listaItensDosItens;
    private ArrayList<DescricaoExame> listaDescricaoExames;
    // Variaveis
    private boolean CPFCPNJ, validacaoCGC;
    private String movimento;
    private Calendar dataAtual, primeiroDia, ultimoDia;
    private Date PDtInicial, PDtFinal;

    public LancamentoAvulsoMB() {
        novo();
    }

    public void novo() {
        cirurgia = new Cirurgia();
        dataAtual = new GregorianCalendar();
        PDtInicial = new Date();
        PDtFinal = new Date();
        cirurgiaItem = new CirurgiaItem();
        CPFCPNJ = false;
        listaPacientes = new ArrayList<>();
        listaCirurgiaItens = new ArrayList<>();
        listaDescricaoExames = new ArrayList<>();
        listaLancamentos = new ArrayList<>();
        repasse = new Repasse();
        contaCorrente = new ContaCorrente();
        procedimento = new Procedimento();
        cheque = new Cheque();
        banco = new Banco();
        caixa = new Caixa();
        tipoInternacao = new TipoInternacao();
        contaCorrente = new ContaCorrente();
        cirurgia.setValor(0D);
        cirurgiaItem.setQuantidade(1D);
        cirurgiaItem.setValorUnitario(0D);
        cirurgiaItem.setValorTotal(0D);
        cirurgia.setDtCirurgia(new Date());
        us = new UsuarioSessao();
    }

    public void zerarCampos() {
        cirurgia = new Cirurgia();
        cirurgiaItem = new CirurgiaItem();
        listaCirurgiaItens = new ArrayList<>();
        cheque = new Cheque();
        CPFCPNJ = false;
        repasse = new Repasse();
        contaCorrente = new ContaCorrente();
        procedimento = new Procedimento();
        cheque = new Cheque();
        banco = new Banco();
        caixa = new Caixa();
        tipoInternacao = new TipoInternacao();
        contaCorrente = new ContaCorrente();
        cirurgia.setValor(0D);
        cirurgiaItem.setQuantidade(1D);
        cirurgiaItem.setValorUnitario(0D);
        cirurgiaItem.setValorTotal(0D);
        cirurgia.setDtCirurgia(new Date());
    }

    @PostConstruct
    public void carregarListaLancamentos() {
        pesquisarMovimento(BCRUtils.primeiroDiaDoAno(new Date()), BCRUtils.ultimoDiaDoMes(new Date()));
    }

    public void pesquisarMovimento(Date PDtIni, Date PDtFim) {
        listaLancamentos = new ArrayList<>();
        tipoInternacao = cEJB.retornaTipoInternacao("Internacao");
        listaLancamentos = cEJB.pesquisarLancamentosAvulsos(PDtIni, PDtFim, tipoInternacao.getIdTipoInternacao());

        if (listaLancamentos.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public void setarDatasPesquisa(String periodo) {
        Date PDtIni = new Date();
        Date PDtFim = new Date();
        if (periodo.equals("D")) {
            PDtIni = dataAtual.getTime();
            PDtFim = dataAtual.getTime();
        }
        if (periodo.equals("M")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(dataAtual.getTime());
            PDtFim = BCRUtils.ultimoDiaDoMes(dataAtual.getTime());
        }
        if (periodo.equals("A")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual.getTime(), -1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual.getTime(), -1));
        }
        if (periodo.equals("P")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual.getTime(), 1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual.getTime(), 1));
        }

        pesquisarMovimento(PDtIni, PDtFim);

    }

    public void addItem() {
        if (validarCamposItem()) {
            cirurgiaItem = new CirurgiaItem();
            cirurgiaItem.setObservacao(procedimento.getDescricao());
            if (procedimento.getPadrao().equals("D")) {
                cirurgiaItem.setValorTotal(-procedimento.getValor());
            } else {
                cirurgiaItem.setValorTotal(procedimento.getValor());
            }
            listaCirurgiaItens.add(cirurgiaItem);
            cirurgia.setIdProcedimento(procedimento);
            calcularTotalLancamento();
            procedimento = new Procedimento();
            Salvar();
        }
    }

    public void setarDatas() {
        if ((movimento.equals("D")) || (movimento.equals("R"))) {
            cheque.setDtCompensacaoVencimento(new Date());
            cheque.setDtCompensado(new Date());
        }
    }

    public void mudarMascara() {
        if (CPFCPNJ) {
            CPFCPNJ = false;
        } else {
            CPFCPNJ = true;
        }
        cheque.setCpf(null);
        validacaoCGC = false;
    }

    public void validarCPF() {
        ValidarCPF vCPF = new ValidarCPF();
        ValidarCNPJ vCNPJ = new ValidarCNPJ();
        validacaoCGC = false;
        if ((!cheque.getCpf().equals("___.___.___-__")) || (!cheque.getCpf().equals("___.___.___/____-__"))) {
            if (CPFCPNJ) {
                validacaoCGC = vCNPJ.isCNPJ(cheque.getCpf());
            } else {
                validacaoCGC = vCPF.isCPF(cheque.getCpf());
            }
        }
    }

//    public void carregarPagamento() {
//        limparCampos();
//        movimento = "C";
//        cheque.setMovimento("C");
//        cheque.setCompensado("N");
//        cheque.setValor(cirurgia.getValor());
//        cheque.setIdPaciente(cirurgia.getIdPaciente());
//        cheque.setDtRecebimentoEmissao(new Date());
//        cheque.setDtCompensacaoVencimento(new Date());
//        cheque.setCpf(cheque.getIdPaciente().getCpf());
//        cheque.setEmitente(cheque.getIdPaciente().getNome());
//        validacaoCGC = true;
//    }
    public void carregarPagamento(String tipo) {
        limparCampos();
        if (cirurgia.getValor() != null) {
            movimento = "D";
            cheque.setMovimento("D");
            cheque.setCompensado("S");
            cheque.setValor(cirurgia.getValor());
            cheque.setIdPaciente(cirurgia.getIdPaciente());
            cheque.setDtRecebimentoEmissao(new Date());
            cheque.setDtCompensacaoVencimento(new Date());
            cheque.setDtCompensado(new Date());
            if (cheque.getIdPaciente().getCpf() != null) {
                cheque.setCpf(cheque.getIdPaciente().getCpf());
            }
            cheque.setEmitente(cheque.getIdPaciente().getNome());
            validacaoCGC = true;
            salvarPagamento();
        }
    }

    public void salvarPagamento() {
        cheque.setMovimento("D");
        cheque.setCompensado("S");
        cheque.setDtCompensado(new Date());
        cheque.setNumeroParcela(1);
        cheque.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
        if (cirurgia.getIdTerceiro().getTerceiroMedHosp()) {
            cheque.setTipo("H");
        } else {
            cheque.setTipo("N");
        }
        chEJB.Salvar(cheque, us.retornaUsuario());
        lancarRepasse();
        limparCampos();
        RequestContext.getCurrentInstance().execute("PF('dlgLancarCheque').hide();");
    }

    public void limparCampos() {
        validacaoCGC = false;
        CPFCPNJ = false;
        movimento = "C";
    }

    public boolean validarCamposObrigatoriosCheque() {
        if (!cheque.getValor().equals(cirurgia.getValor())) {
            Mensagem.addMensagem(3, "Valor pago não representa o total do lançamento. Total a pagar: R$ " + cirurgia.getValor());
            return false;
        } else if (movimento.equals("C")) {
            if (cheque.getNumeroCheque() == null) {
                Mensagem.addMensagem(3, "Informe o número do cheque!");
                return false;
            } else if (banco.getIdBanco() == null) {
                Mensagem.addMensagem(3, "Informe o banco.");
                return false;
            } else if (cheque.getValor().equals(0D)) {
                Mensagem.addMensagem(3, "Informe o valor do pagamento.");
                return false;
            } else {
                return true;
            }
        } else if (cheque.getValor().equals(0D)) {
            Mensagem.addMensagem(3, "Informe o valor do pagamento.");
            return false;
        } else {
            return true;
        }
    }

    public boolean validarCamposItem() {
        if (procedimento.getDescricao() == null) {
            Mensagem.addMensagem(3, "Informe o item!");
            return false;
        } else if (procedimento.getValor() == null) {
            Mensagem.addMensagem(3, "Informe o valor do item.");
            return false;
        } else {
            return true;
        }
    }

    public boolean verificarCamposObrigatorios() {
        if (listaCirurgiaItens.isEmpty()) {
            Mensagem.addMensagem(3, "Informe o procedimento.");
            return false;
        }
        return true;
    }

    public void Salvar() {
        try {
            cirurgia.setIdUsuario(uEJB.retornaUsuarioDaSessao());
            cirurgia.setPago(Boolean.FALSE);
            cirurgia.setIdTipoInternacao(cEJB.retornaTipoInternacao(procedimento.getPadrao() == null ? "G%" : procedimento.getPadrao() + "%"));

            // 23052018 - Charles
            // Adicionando forma de repasse "Débito" para retirar valores da conta do médico.
            if (!uEJB.retornaUsuarioDaSessao().getIdPapel().getProntoAtendimento()) {
                if (cirurgia.getIdProcedimento().getPadrao().equals("D")) {
                    carregarPagamento("D");
                } else {
                    carregarPagamento("C");
                }
                cirurgia.setPago(true);
            }

            cirurgia.setStatus('L');
            cEJB.SalvarAvulso(cirurgia, listaCirurgiaItens, repasse);

            if (!uEJB.retornaUsuarioDaSessao().getIdPapel().getProntoAtendimento()) {
                cheque.setIdCirurgia(cirurgia);
                chEJB.Salvar(cheque, us.retornaUsuario());
                if (!cirurgia.getIdProcedimento().getPadrao().equals("D")) {
                    Receber("C");
                } else {
                    Receber("D");
                }
            }
            Mensagem.addMensagem(1, "Salvo com sucesso.");
            aEJB.Salvar(BCRUtils.criarAuditoria("avulso", cirurgia.getIdCirurgia().toString(), us.retornaUsuario(), new Date(), "Lançamento Avulso"));
            zerarCampos();
            carregarListaLancamentos();
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar salvar!" + e);
            System.out.println(e);
        }
    }

    public void pagarAvulso(Cirurgia c) {
        try {
            cirurgia = c;
            cirurgia.setPago(true);
            cEJB.atualizarCirurgia(cirurgia);
            carregarPagamento(cirurgia.getIdProcedimento().getPadrao().equals("D") ? "D" : "C");
            lancarContaCorrente();
            //rEJB.SalvarSimples(repasse);
            cheque.setIdCirurgia(cirurgia);
            chEJB.Salvar(cheque, us.retornaUsuario());
            Receber(cirurgia.getIdProcedimento().getPadrao().equals("D") ? "D" : "C");
            c.setRepasseList(cirurgia.getRepasseList());
            Mensagem.addMensagem(1, "Pagamento salvo com sucesso.");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar salvar!" + e);
            System.out.println(e);
        }
    }

    public void pagarAvulsoEmCheque(String mov, String cpf, String emitenteCheque, Banco banco, Date dtVencimento, String numeroCheque, String observacao, String conta, Double valor, Cirurgia c) {
        try {
            Associacao a = asEJB.carregarAssociacao();
            cirurgia = c;            
            carregarPagamentoCheque(mov, emitenteCheque, cpf, numeroCheque, banco, dtVencimento, conta, observacao, valor, a.getPrazoCompensacaoCheque());
            if (!mov.equals("A") && !mov.equals("R")) {
                lancarRepasse();
                lancarContaCorrente();
                Receber(cirurgia.getIdProcedimento().getPadrao().equals("D") ? "D" : "C");
                c.setRepasseList(cirurgia.getRepasseList());
            }
            cheque.setIdCirurgia(cirurgia);
            cirurgia.setPago(true);
            cEJB.atualizarCirurgia(cirurgia);            
            Mensagem.addMensagem(1, "Pagamento salvo com sucesso.");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar salvar!" + e);
            System.out.println(e);
        }
    }

    private void carregarPagamentoCheque(String mov, String emitenteCheque, String cpf, String numeroCheque, Banco banco, Date dtVencimento, String conta, String observacao, Double valor, Integer prazoCompensacao) {
        String tipoMovimento;
        tipoMovimento = "D".equals(mov) ? "Dinheiro" : mov.equals("C") ? "Cheque" : "N".equals(mov) ? "Nota Promissória" : "B".equals(mov) ? "Depósito Bancário" : "A".equals(mov) ? "Cartão - Débito" : "R".equals(mov) ? "Cartão - Crédito" : "";

        cheque = new Cheque();
        cheque.setIdPaciente(cirurgia.getIdPaciente());
        cheque.setIdCirurgia(cirurgia);
        if (mov.equals("C")) {
            cheque.setEmitente(emitenteCheque);
            cheque.setCpf(cpf);
            cheque.setNumeroCheque(numeroCheque);
            cheque.setIdBanco(banco);
        }

        cheque.setMovimento(mov);
        cheque.setDtCompensacaoVencimento(dtVencimento);
        cheque.setDtRecebimentoEmissao(new Date());

        cheque.setObservacao(observacao);
        cheque.setConta(conta);
        cheque.setValor(valor);
        cheque.setNumeroParcela(1);
        cheque.setIdUsuario(uEJB.retornaUsuarioDaSessao());

        cheque.setIdCirurgia(cirurgia);
        cheque.setTipo("N");
        cheque.setDtCompensado(BCRUtils.addDia(new Date(), prazoCompensacao));
        cheque.setSituacao("Recebido");
        cheque.setCompensado("S");
        chEJB.Salvar(cheque, us.retornaUsuario());
    }

    public void lancarRepasse() {
        try {
            repasse = new Repasse();
            repasse.setIdCirurgia(cirurgia);
            repasse.setNumeroParcela(cheque.getNumeroParcela());
            repasse.setIdCheque(cheque);
            repasse.setIdPaciente(cheque.getIdPaciente());
            repasse.setValor(cheque.getValor());
            repasse.setDtLancamento(cheque.getDtRecebimentoEmissao());
            repasse.setDtRecebimento(cheque.getDtRecebimentoEmissao());
            repasse.setDtVencimento(cheque.getDtCompensacaoVencimento());
            repasse.setIdTerceiro(cirurgia.getIdTerceiro());
            repasse.setTipoPagamento('P');
            repasse.setPercentualRecebido(100D);
            repasse.setSituacaoPaciente('P');
            repasse.setStatus('F');
            repasse.setValor(0D);
            repasse.setValorRecebido(cheque.getValor());
            repasse.setValorDocumento(cheque.getValor());
            if (repasse.getIdCheque().getMovimento().equals("C")) {
                repasse.setFormaPagamento("Cheque");
                repasse.setSituacaoTerceiro('N');
            } else {
                repasse.setFormaPagamento("Dinheiro");
                repasse.setSituacaoTerceiro('L');
            }

        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    private void lancarContaCorrente() {
        ContaCorrente cc = new ContaCorrente();
        cc.setIdContaCorrente(null);
        cc.setIdTerceiro(repasse.getIdTerceiro());
        cc.setIdRepasse(repasse);
        cc.setIdCirurgia(cirurgia);
        cc.setIdPaciente(repasse.getIdPaciente());
        cc.setIdCheque(repasse.getIdCheque().getIdCheque());
        if (cirurgia.getIdProcedimento().getPadrao().equals("D")) {
            cc.setTipo("D");
            cc.setObservacao("Débito gerado pelo lançamento avulso nº " + cc.getIdCirurgia().getIdCirurgia() + " do paciente " + repasse.getIdPaciente().getNome() + ".");
        } else {
            cc.setTipo("C");
            cc.setObservacao("Repasse gerado pelo lançamento avulso nº " + cc.getIdCirurgia().getIdCirurgia() + " do paciente " + repasse.getIdPaciente().getNome() + ".");
        }
        cc.setDtLancamento(new Date());
        cc.setSituacao("Não Liberado");
        cc.setStatus("Não Retirado");
        cc.setValor(repasse.getValorDocumento());
        ccEJB.SalvarSimples(cc, null);
    }

    public void Receber(String tipo) {
        Usuario u = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
        try {
            Date d = new Date();
            Long numAleatorio = d.getTime();
            String forma, terceiro = "";

            // Lançar pagamentos no Caixa
            caixa = new Caixa();
            caixa.setNumLote(numAleatorio.toString());
            caixa.setIdCheque(cheque);
            caixa.setIdUsuario(u);
            caixa.setDtCaixa(cheque.getDtRecebimentoEmissao());
            caixa.setTipoLancamento('C');
            caixa.setValor(cheque.getValor());
            caixa.setStatus("Não Baixado");
            caixa.setIdCirurgia(cirurgia);
            if (cirurgia.getIdTerceiro().getTipoPessoa() == 'J') {
                terceiro = cirurgia.getIdTerceiro().getNomeFantasia();
            } else {
                terceiro = cirurgia.getIdTerceiro().getNome();
            }
            caixa.setHistorico("Lançamento Avulso " + (tipo.equals("D") ? "(Débito)" : "") + " nº " + cirurgia.getIdCirurgia() + ". Méd. Resp.:" + terceiro + ". Paciente: " + cirurgia.getIdPaciente().getNome());
            caixa.setNumeroParcela(cheque.getNumeroParcela());
            cxEJB.Salvar(caixa);

        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algum erro ao tentar receber!");
            System.out.println(e);
        }
    }

    public List<Repasse> pesquisarRepassesPorFechamento(Cirurgia c) {
        return cEJB.pesquisarRepassesPorCirurgia(c);
    }

    public void ExcluirItem(CirurgiaItem ci) {
        int indice = 0;
        List<CirurgiaItem> listaTemp = new ArrayList<>();
        listaTemp = listaCirurgiaItens;
        for (CirurgiaItem cit : listaTemp) {
            if (cit == ci) {
                listaCirurgiaItens.remove(indice);
                Mensagem.addMensagemGrowl(1, "Item Removido com sucesso!");
                calcularTotalLancamento();
                repasse = new Repasse();
                break;
            } else {
                indice++;
            }
        }
    }

    public void excluirAvulso(Cirurgia c) {
        if (c != null) {
            cEJB.Excluir(c);
            carregarListaLancamentos();
            Mensagem.addMensagemPadraoSucesso("excluir");
        }
    }

    public void ExcluirPagamento() {
        try {
            chEJB.Excluir(cheque, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("excluir");
            calcularTotalLancamento();
            repasse = new Repasse();
            cheque = new Cheque();
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
        }

    }

    public void calcularTotalLancamento() {
        Double valorTotal = 0D;
        for (CirurgiaItem ci : listaCirurgiaItens) {
            valorTotal = valorTotal + ci.getValorTotal();
        }
        cirurgia.setValor(valorTotal);
    }

    public List<DescricaoExame> completeMethodDescricaoExame(String var) {
        return deEJB.completeMethodDescricao(var);
    }

    public List<Paciente> completeMethodPaciente(String var) {
        return pEJB.autoCompletePaciente(var);
    }

    public List<Procedimento> completeMethodProcedimento(String var) {
        return proEJB.autoCompleteProcedimentoAvulso(var);
    }

    public Cirurgia getCirurgia() {
        return cirurgia;
    }

    public void setCirurgia(Cirurgia cirurgia) {
        this.cirurgia = cirurgia;
    }

    public CirurgiaItem getCirurgiaItem() {
        return cirurgiaItem;
    }

    public void setCirurgiaItem(CirurgiaItem cirurgiaItem) {
        this.cirurgiaItem = cirurgiaItem;
    }

    public DescricaoExame getDescricaoExame() {
        return descricaoExame;
    }

    public void setDescricaoExame(DescricaoExame descricaoExame) {
        this.descricaoExame = descricaoExame;
    }

    public ArrayList<Paciente> getListaPacientes() {
        return listaPacientes;
    }

    public void setListaPacientes(ArrayList<Paciente> listaPacientes) {
        this.listaPacientes = listaPacientes;
    }

    public ArrayList<CirurgiaItem> getListaCirurgiaItens() {
        return listaCirurgiaItens;
    }

    public void setListaCirurgiaItens(ArrayList<CirurgiaItem> listaCirurgiaItens) {
        this.listaCirurgiaItens = listaCirurgiaItens;
    }

    public ArrayList<DescricaoExame> getListaDescricaoExames() {
        return listaDescricaoExames;
    }

    public void setListaDescricaoExames(ArrayList<DescricaoExame> listaDescricaoExames) {
        this.listaDescricaoExames = listaDescricaoExames;
    }

    public boolean isCPFCPNJ() {
        return CPFCPNJ;
    }

    public void setCPFCPNJ(boolean CPFCPNJ) {
        this.CPFCPNJ = CPFCPNJ;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Repasse getRepasse() {
        return repasse;
    }

    public void setRepasse(Repasse repasse) {
        this.repasse = repasse;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public TipoInternacao getTipoInternacao() {
        return tipoInternacao;
    }

    public void setTipoInternacao(TipoInternacao tipoInternacao) {
        this.tipoInternacao = tipoInternacao;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public List<ServicoItemCirurgiaItem> getListaItensDosItens() {
        return listaItensDosItens;
    }

    public void setListaItensDosItens(List<ServicoItemCirurgiaItem> listaItensDosItens) {
        this.listaItensDosItens = listaItensDosItens;
    }

    public String getMovimento() {
        return movimento;
    }

    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    public ServicoItemCirurgiaItem getServicoItemCirurgiaItem() {
        return servicoItemCirurgiaItem;
    }

    public void setServicoItemCirurgiaItem(ServicoItemCirurgiaItem servicoItemCirurgiaItem) {
        this.servicoItemCirurgiaItem = servicoItemCirurgiaItem;
    }

    public boolean isValidacaoCGC() {
        return validacaoCGC;
    }

    public void setValidacaoCGC(boolean validacaoCGC) {
        this.validacaoCGC = validacaoCGC;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public List<Cirurgia> getListaLancamentos() {
        return listaLancamentos;
    }

    public void setListaLancamentos(List<Cirurgia> listaLancamentos) {
        this.listaLancamentos = listaLancamentos;
    }

    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public void imprimirLancamento(Integer idCirurgia) {
        RelatorioFactory.internacao("/WEB-INF/Relatorios/reportReciboConsulta_MeiaFolha.jasper", idCirurgia, "C://SMA//logo.png");
    }

    public Calendar getDataAtual() {
        return dataAtual;
    }

    public void setDataAtual(Calendar dataAtual) {
        this.dataAtual = dataAtual;
    }

    public Calendar getPrimeiroDia() {
        return primeiroDia;
    }

    public void setPrimeiroDia(Calendar primeiroDia) {
        this.primeiroDia = primeiroDia;
    }

    public Calendar getUltimoDia() {
        return ultimoDia;
    }

    public void setUltimoDia(Calendar ultimoDia) {
        this.ultimoDia = ultimoDia;
    }

    public Date getPDtInicial() {
        return PDtInicial;
    }

    public void setPDtInicial(Date PDtInicial) {
        this.PDtInicial = PDtInicial;
    }

    public Date getPDtFinal() {
        return PDtFinal;
    }

    public void setPDtFinal(Date PDtFinal) {
        this.PDtFinal = PDtFinal;
    }

    private void carregarDebito() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void ReceberDebito() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
