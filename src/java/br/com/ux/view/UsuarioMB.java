/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.PapelEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Auditoria;
import br.com.ux.model.Papel;
import br.com.ux.model.Terceiro;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import br.com.ux.util.ValidarCPF;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author BrunoS
 */
@ManagedBean
@ViewScoped
public class UsuarioMB implements Serializable {

    @EJB
    UsuarioEJB uEJB;
    @EJB
    PapelEJB pEJB;
    @EJB
    TerceiroEJB tEJB;
    @EJB
    PacienteEJB pacienteEJB;
    Usuario usuario;
    Papel papel;
    Papel papel2;
    Terceiro terceiro;
    List<Usuario> listaaCarregar = new ArrayList<Usuario>();
    UsuarioSessao us = new UsuarioSessao();
    LazyDataModel<Usuario> model;

    public UsuarioMB() {
        novo();
        carregarModeloLazy();
    }

    public void novo() {
        usuario = new Usuario();
        papel = new Papel();
        papel2 = new Papel();
        terceiro = new Terceiro();
        usuario.setAtivo(Boolean.TRUE);
        usuario.setTrocarSenha(Boolean.TRUE);
    }

    public void salvar() {
        ValidarCPF vcpf = new ValidarCPF();
        if (usuario.getIdUsuario() == null) {
            try {
                if (papel.getIdPapel() != null) {
                    usuario.setIdPapel(papel);
                }
                if (terceiro.getIdTerceiro() != null) {
                    usuario.setIdTerceiro(terceiro);
                } else {
                    usuario.setIdTerceiro(null);
                }

                uEJB.Salvar(usuario, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
                RequestContext.getCurrentInstance().execute("PF('dlgNovo').hide();");

            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("editar");
                System.out.println(e);
            }
        } else {
            try {
                if (papel.getIdPapel() != null) {
                    usuario.setIdPapel(papel);
                }
                if (terceiro.getIdTerceiro() != null) {
                    usuario.setIdTerceiro(terceiro);
                } else {
                    usuario.setIdTerceiro(null);
                }
                uEJB.Salvar(usuario, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("editar");
                RequestContext.getCurrentInstance().execute("PF('dlgNovo').hide();");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("editar");
                System.out.println(e);
            }
        }

    }

    public void excluir() {
        try {
            uEJB.Excluir(usuario, us.retornaUsuario());
            novo();
            Mensagem.addMensagem(1, "Excluido com sucesso!");
            BCRUtils.ResetarDatatableFiltros("frmUsuario:dtlUsuarios"); // passar a datatable do usuario
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar excluir!" + e);
        }
    }

    public void verificaCPF() {
        if (!usuario.getCpf().equals("___.___.___-__")) {
            System.out.println(usuario.getCpf());
            ValidarCPF vcpf = new ValidarCPF();
            if (vcpf.isCPF(usuario.getCpf())) {
                String nome = uEJB.verificaCPFRetornaNome(usuario.getCpf());
                if (!nome.equals("Inexistente")) {
                    Mensagem.addMensagem(3, "CPF já foi cadastrado para " + nome);
                }
            } else {
                Mensagem.addMensagem(3, "Atenção! O CPF informado não é válido!");
            }
        }
    }

    public boolean retornaSeUsuarioJaTemMovimentacao() {
        boolean retorno = false;
        if (usuario.getIdUsuario() != null) {
            if (!usuario.getCaixaList().isEmpty() || !usuario.getCaixaList1().isEmpty() || !usuario.getChequeList().isEmpty()
                    || !usuario.getTerceiroList().isEmpty() || !usuario.getContaCorrenteList().isEmpty() || usuario.getIdTerceiro() == null || !usuario.getIdTerceiro().getRepasseList().isEmpty()) {
                retorno = true;
            }
        }
        return retorno;
    }

    public void carregarModeloLazy() {
        model = new LazyDataModel<Usuario>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Usuario> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();

                setRowCount(Integer.parseInt(uEJB.contarRegistro(Clausula).toString()));

                listaaCarregar = uEJB.listarLazy(first, pageSize, Clausula);
                return listaaCarregar;
            }
        };
    }
    
    public Usuario retornaUsuarioLogado() {
        return uEJB.retornaUsuarioDaSessao();
    }

    public List<Terceiro> listaTerceiros() {
        return tEJB.listarTerceiro();
    }

    public Usuario selecionaPorID(Integer id) {
        usuario = uEJB.selecionarPorID(id);
        papel = usuario.getIdPapel();
        if (usuario.getIdTerceiro() != null) {
            terceiro = usuario.getIdTerceiro();
        }
        return usuario;
    }

    public Usuario selecionaPorUser(String s) {
        usuario = uEJB.selecionarPorUser(s);
        papel = usuario.getIdPapel();
        if (usuario.getIdTerceiro() != null) {
            terceiro = usuario.getIdTerceiro();
        }
        return usuario;
    }

    public List<Usuario> listaUsuarioRecebimento() {
        List<Usuario> usuarioPorPapel = new ArrayList<Usuario>();
        usuarioPorPapel = uEJB.usuariosPorRecebimento();
        return usuarioPorPapel;
    }

    public void carregaConsulta() {
        listaaCarregar = uEJB.listarUsuarios();
    }

    public List<Usuario> listaUsuarios() {
        return uEJB.listarUsuarios();
    }
    
    public List<Usuario> listaUsuariosAtivos() {
        return uEJB.listarUsuariosAtivos();
    }
    
    public List<Usuario> listaUsuariosEmail() {
        return uEJB.listarUsuariosEmail();
    }
    
    public List<Usuario> listaUsuariosProntoAtendimento() {
        return uEJB.listarUsuariosProntoAtendimento();
    }

    public List<Usuario> listaUsuariosAptosTransferencia() {
        return uEJB.listarUsuariosTransferencia();
    }

    public List<Papel> listaPepeis() {
        return pEJB.listarPapeis();
    }

    //=========================================================================
    public UsuarioEJB getuEJB() {
        return uEJB;
    }

    public void setuEJB(UsuarioEJB uEJB) {
        this.uEJB = uEJB;
    }

    public PapelEJB getpEJB() {
        return pEJB;
    }

    public void setpEJB(PapelEJB pEJB) {
        this.pEJB = pEJB;
    }

    public TerceiroEJB gettEJB() {
        return tEJB;
    }

    public void settEJB(TerceiroEJB tEJB) {
        this.tEJB = tEJB;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Papel getPapel() {
        return papel;
    }

    public void setPapel(Papel papel) {
        this.papel = papel;
    }

    public Terceiro getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(Terceiro terceiro) {
        this.terceiro = terceiro;
    }

    public List<Usuario> getListaaCarregar() {
        return listaaCarregar;
    }

    public void setListaaCarregar(List<Usuario> listaaCarregar) {
        this.listaaCarregar = listaaCarregar;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public LazyDataModel<Usuario> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Usuario> model) {
        this.model = model;
    }

    public Papel getPapel2() {
        return papel2;
    }

    public void setPapel2(Papel papel2) {
        this.papel2 = papel2;
    }
}
