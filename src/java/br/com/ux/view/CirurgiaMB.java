package br.com.ux.view;

import br.com.ux.controller.AdiantamentosHistoricoEJB;
import br.com.ux.controller.integracao.SincronizacaoEJB;
import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.AtendimentoPacienteEJB;
import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.CirurgiaEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.ContaPacienteEJB;
import br.com.ux.controller.ConvenioEJB;
import br.com.ux.controller.FormaPagamentoEJB;
import br.com.ux.controller.MotivoEJB;
import br.com.ux.controller.OrcamentoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.PlanoEJB;
import br.com.ux.controller.ProcedimentoEJB;
import br.com.ux.controller.ProcedimentoPacienteEJB;
import br.com.ux.controller.RegraRepasseEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.ServicoEJB;
import br.com.ux.controller.StatusEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.TipoInternacaoEJB;
import br.com.ux.controller.UnidadeAtendimentoEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.AdiantamentosHistorico;
import br.com.ux.model.Banco;
import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Convenio;
import br.com.ux.model.FormaPagamento;
import br.com.ux.model.Motivo;
import br.com.ux.model.Orcamento;
import br.com.ux.model.OrcamentoItem;
import br.com.ux.model.Paciente;
import br.com.ux.model.Plano;
import br.com.ux.model.Procedimento;
import br.com.ux.model.RegraRepasse;
import br.com.ux.model.RegraRepasseMedicos;
import br.com.ux.model.Repasse;
import br.com.ux.model.Servico;
import br.com.ux.model.ServicoItem;
import br.com.ux.model.ServicoItemCirurgiaItem;
import br.com.ux.model.Terceiro;
import br.com.ux.model.TipoInternacao;
import br.com.ux.model.UnidadeAtendimento;
import br.com.ux.model.Usuario;
import br.com.ux.model.tasy.AtendimentoPaciente;
import br.com.ux.model.tasy.ContaPaciente;
import br.com.ux.model.tasy.ProcedimentoPaciente;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Conexao;
import br.com.ux.util.Extenso;
import br.com.ux.util.Mensagem;
import br.com.ux.util.Parcela;
import br.com.ux.util.RelatorioFactory;
import br.com.ux.util.UsuarioSessao;
import br.com.ux.util.ValidarCNPJ;
import br.com.ux.util.ValidarCPF;
import br.com.ux.util.ValoresTerceiros;
import com.sun.xml.ws.util.StringUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.persistence.Query;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class CirurgiaMB implements Serializable {

    private String status;
    @EJB
    StatusEJB stEJB;
    @EJB
    AssociacaoEJB assEJB;
    @EJB
    OrcamentoEJB oEJB;
    private Orcamento orcamento;
    @EJB
    AuditoriaEJB aEJB;
    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;
    private List<Paciente> listaPacientesACarregar;
    @EJB
    ProcedimentoEJB proEJB;
    private List<Procedimento> listaProcedimentosACarregar;
    Procedimento procedimento;
    @EJB
    ConvenioEJB cEJB;
    private Convenio convenio;
    @EJB
    PlanoEJB plaEJB;
    private Plano plano;
    @EJB
    TerceiroEJB tEJB;
    private Terceiro terceiro;
    private List<Terceiro> listaTerceirosACarregar;
    @EJB
    UnidadeAtendimentoEJB uaEJB;
    private UnidadeAtendimento unidadeAtendimento;
    @EJB
    FormaPagamentoEJB fpEJB;
    private FormaPagamento formaPagamento;
    private OrcamentoItem orcamentoItem;
    @EJB
    ServicoEJB sEJB;
    private List<Servico> listaDeServicos;
    private LazyDataModel<Servico> listaDeServicosLazy;

    private List<OrcamentoItem> itens;
    private UsuarioSessao us = new UsuarioSessao();
    @EJB
    MotivoEJB mEJB;
    private Motivo motivo;
    private String acao;
    @EJB
    CirurgiaEJB cirEJB;
    private Cirurgia cirurgia;
    private CirurgiaItem cirurgiaItem;
    private List<CirurgiaItem> cirurgiaItens;
    private List<ServicoItemCirurgiaItem> itensDosItens;
    private List<ServicoItemCirurgiaItem> listadeItensDosItens;
    private LazyDataModel<Paciente> model;
    private List<Orcamento> orctosAPesquisar;
    private Paciente responsavel;
    private Banco banco;
    private Integer paramIdCirurgia;
    @EJB
    ChequeEJB chequeEJB;
    private Cheque cheque;
    private List<Cheque> listaCheque;
    private List<Cheque> listaChequeTemporaria;
    private Parcela parcelaPaga;
    @EJB
    RepasseEJB repasseEJB;
    @EJB
    TipoInternacaoEJB tiEJB;
    private TipoInternacao tipoInternacao;
    private List<TipoInternacao> listaTipoInternacao;
    @EJB
    AdiantamentosHistoricoEJB ahEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    CaixaEJB caixaEJB;
    private Caixa caixa;
    @EJB
    SincronizacaoEJB syncEJB;

    @EJB
    AtendimentoPacienteEJB apEJB;
    private AtendimentoPaciente atendimentoPaciente;
    private ContaPaciente contaPaciente;
    private List<AtendimentoPaciente> listaAtendimentos;
    private LazyDataModel<AtendimentoPaciente> listaAtendimentosLazy;

    @EJB
    RegraRepasseEJB rrEJB;
    private RegraRepasse regraRepasse;

    @EJB
    ContaPacienteEJB cpEJB;
    private List<ContaPaciente> listaContasPacientePorAtendimento;

    @EJB
    ProcedimentoPacienteEJB ppEJB;

    @EJB
    ContaCorrenteEJB ccEJB;

    //  Controle das Telas    //
    private String tipoInter, liberar, nomePacientePesquisa;
    private boolean exceto, CPFCPNJ, validacaoCGC, finalizarParciais, emitirRecibo;
    //  Controle de Cheques  //
    private String tipo, compensado, observacao, situacao, pagamento, emitenteCheque, movimento, CPF, conta, numeroCheque;
    private Double valorCheque, valorTotal, valorRecebido, valorTotalPgto, valorHonorariosMedicos, valorResumoRepasses, valorReciboMed, valorReciboHosp;
    private Double valorHonorariosHospitalares, valorTotalHonorario, valorTotalPago, valorInfoCheques, valorRestante, valorTotalCirurgia;
    private Double valorTotalItensHospital, widgetValorPago, widgetValorRestante, widgetValorTotal;
    private Integer numeroParcela, qdeHonorariosMedicos, qdeHonorariosHospitalares, numeroAtendimento;
    private Date dtEmissao, dtCompensado, dtVencimento;
    boolean diferenca, manual;
    // Controle de Repasses por Tipo de Honorários
    private List<Repasse> listaRepassesHonorariosMedicos;
    private List<Repasse> listaRepassesHonorariosHospitalares;
    private List<Repasse> listaRepassesHonorariosTotal;
    // Gerar itens para hospital //
    private Servico servico;
    private Servico servicoExterno;
    // Lista Repasses consolidados // 
    private Double valorTotalaRepassar, valorRepassado, valoresPagos, valorDesconto;
    private List<Repasse> repassesConsolidados;
    private ValoresTerceiros valoresTerceiros;
    private List<ValoresTerceiros> listVt = new ArrayList<>();
    private ServicoItemCirurgiaItem servicoItemCirurgiaItem;
    private List<ServicoItemCirurgiaItem> listaServicosPorTerceiros;
    private List<ServicoItemCirurgiaItem> listaServicosPorTerceirosTemporaria;
    private Integer paramProcedimento = null;
    private String paramServico = "%";
    private Integer paramTerceiro = null;
    private Integer paramTerceiroLaudo = null;
    private Integer paramUsuario = null, paramPago = null;
    private LazyDataModel<Procedimento> modelProcedimento;
    private Boolean desabilitarAutomatico, recibo;

    public CirurgiaMB() {
        setStatus("Consultando");
        novo();
        setAcao("inserindo");
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public void novo() {
        responsavel = new Paciente();
        orcamento = new Orcamento();
        CPFCPNJ = false;
        pagamento = "";
        orcamento.setDtOrcamento(new Date());
        listaPacientesACarregar = new ArrayList<>();
        listaProcedimentosACarregar = new ArrayList<>();
        listaTerceirosACarregar = new ArrayList<>();
        //listaDeServicos = new ArrayList<>();
        listaCirurgiaAPesquisar = new ArrayList<>();
        convenio = new Convenio();
        plano = new Plano();
        cirurgiaItem = new CirurgiaItem();
        unidadeAtendimento = new UnidadeAtendimento();
        formaPagamento = new FormaPagamento();
        motivo = new Motivo();
        cirurgia = new Cirurgia();
        cirurgiaItens = new ArrayList<>();
        cirurgia.setValor(0D);
        cirurgiaItem.setQuantidade(1D);
        cirurgiaItem.setValorUnitario(0D);
        cirurgiaItem.setValorTotal(0D);
        itens = new ArrayList<>();
        setAcao("inserindo");
        itensDosItens = new ArrayList<>();
        listadeItensDosItens = new ArrayList<>();
        cirurgia.setDtCirurgia(new Date());
        //cirurgia.setDtInternacao(new Date());
        repasses = new ArrayList<>();
        repassesConsolidados = new ArrayList<>();
        orctosAPesquisar = new ArrayList<>();

        totalParcela = 0D;
        valorCirur = 0D;
        valorDif = 0D;
        valorFinal = 0D;
        valorInfoCheques = 0D;
        valorTotalCirurgia = 0D;
        valorReciboMed = 0D;
        valorReciboHosp = 0D;
        numeroAtendimento = 0;
        valorInfoCheques = 0D;

        listaCheque = new ArrayList<Cheque>();
        listaChequeTemporaria = new ArrayList<Cheque>();
        cheque = new Cheque();
        paciente = new Paciente();
        terceiro = new Terceiro();
        banco = new Banco();
        tipoInternacao = new TipoInternacao();
        caixa = new Caixa();
        valoresTerceiros = new ValoresTerceiros();
        servicoItemCirurgiaItem = new ServicoItemCirurgiaItem();
        listaServicosPorTerceiros = new ArrayList<ServicoItemCirurgiaItem>();
        listaServicosPorTerceirosTemporaria = new ArrayList<ServicoItemCirurgiaItem>();
        dtInicialPesquisa = BCRUtils.primeiroDiaDoMes(BCRUtils.addMes(new Date(), -1));
        dtFinalPesquisa = BCRUtils.ultimoDiaDoMes(new Date());
        listVt = new ArrayList<>();
        contaPaciente = new ContaPaciente();
        desabilitarAutomatico = Boolean.FALSE;
        recibo = Boolean.TRUE;
    }

    public void carregarPacientesLazyModel() {
        model = new LazyDataModel<Paciente>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Paciente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'" + filterValue + "%'");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(pEJB.contarPacientesRegistro(Clausula).toString()));
                listaPacientesACarregar = pEJB.listarPacientesLazyModeWhere(first, pageSize, Clausula);
                return listaPacientesACarregar;
            }
        };
    }

    public void carregarProcedimentoLazyModel() {
        modelProcedimento = new LazyDataModel<Procedimento>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Procedimento> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'" + filterValue + "%' and p.inativo = FALSE");
                        } else {
                            sf.append(" where p." + filterProperty + " like'" + filterValue + "%' and p.inativo = FALSE");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'" + filterValue + "%' and p.inativo = FALSE");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(proEJB.contarProcedimentosRegistro(Clausula).toString()));
                listaProcedimentosACarregar = proEJB.listarProcedimentosLazyModeWhere(first, pageSize, Clausula);
                return listaProcedimentosACarregar;
            }
        };
    }

    public List<Servico> completeMethodServico(String var) {
        return sEJB.autoCompleteServico(var);
    }

    public void zerarPaciente() {
        nomePacientePesquisa = null;
        pacientePesquisa = null;
    }

    public LazyDataModel<Paciente> getModel() {
        return model;
    }

    public Orcamento getOrcamento() {
        return orcamento;
    }

    public Convenio getConvenio() {
        return convenio;
    }

    public Plano getPlano() {
        return plano;
    }

    public void setOrcamento(Orcamento orcamento) {
        this.orcamento = orcamento;
    }

    public List<Terceiro> listaTerceiros() {
        return tEJB.listarTerceiro();
    }

    public void setListaPacientesACarregar(List<Paciente> listaPacientesACarregar) {
        this.listaPacientesACarregar = listaPacientesACarregar;
    }

    public void setListaProcedimentosACarregar(List<Procedimento> listaProcedimentosACarregar) {
        this.listaProcedimentosACarregar = listaProcedimentosACarregar;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }

    public void setListaTerceirosACarregar(List<Terceiro> listaTerceirosACarregar) {
        this.listaTerceirosACarregar = listaTerceirosACarregar;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public Paciente getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Paciente responsavel) {
        this.responsavel = responsavel;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public OrcamentoItem getOrcamentoItem() {
        return orcamentoItem;
    }

    public void setOrcamentoItem(OrcamentoItem orcamentoItem) {
        this.orcamentoItem = orcamentoItem;
    }

    public List<OrcamentoItem> getItens() {
        return itens;
    }

    public void setItens(List<OrcamentoItem> itens) {
        this.itens = itens;
    }

    public void carregarListaPacientes() {
        listaPacientesACarregar = pEJB.listarPacientes();
    }

    public List<Orcamento> getOrctosAPesquisar() {
        return orctosAPesquisar;
    }

    public void setOrctosAPesquisar(List<Orcamento> orctosAPesquisar) {
        this.orctosAPesquisar = orctosAPesquisar;
    }

    public void pesquisarOrctosParaImportacao() {
        orctosAPesquisar = new ArrayList<Orcamento>();
        BCRUtils.ResetarDatatableFiltros("frmConsultarOrcamentos:tblConOrcto");
        orctosAPesquisar = cirEJB.pesquisarOrcamentosParaImportacao();
    }

    public void pesquisarAtendimentoParaImportacao() {

        // Somente atendimentos internos e Ambulatoriais
        listaAtendimentosLazy = new LazyDataModel<AtendimentoPaciente>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<AtendimentoPaciente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuilder sf = new StringBuilder();
                int contar = 0;
                for (String filterProperty : filters.keySet()) {
                    // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p.").append(filterProperty).append(" like '").append(filterValue).append("%' ");
                        } else {
                            sf.append(" where p.").append(filterProperty).append(" like '").append(filterValue).append("%' ");
                        }
                    } else {
                        sf.append(" and p.").append(filterProperty).append(" like '").append(filterValue).append("%' ");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p.").append(sortField).append(" asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p.").append(sortField).append(" desc");
                }

                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(apEJB.contarAtendimentosPacienteRegistro(Clausula).toString()));
                listaAtendimentos = apEJB.listarAtendimentosPacienteModeWhere(first, pageSize, Clausula);
                return listaAtendimentos;
            }
        };
        BCRUtils.ResetarDatatableFiltros("frmConsultarAtendimentos:atend");
    }

    public List<Paciente> getListaPacientesACarregar() {
        return listaPacientesACarregar;
    }

    public List<Procedimento> getListaProcedimentosACarregar() {
        return listaProcedimentosACarregar;
    }

    public List<Servico> getListaDeServicos() {
        return listaDeServicos;
    }

    public void setListaDeServicos(List<Servico> listaDeServicos) {
        this.listaDeServicos = listaDeServicos;
    }

    public String getStatus() {
        return status;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Cirurgia getCirurgia() {
        return cirurgia;
    }

    public void setCirurgia(Cirurgia cirurgia) {
        this.cirurgia = cirurgia;
    }

    public CirurgiaItem getCirurgiaItem() {
        return cirurgiaItem;
    }

    public void setCirurgiaItem(CirurgiaItem cirurgiaItem) {
        this.cirurgiaItem = cirurgiaItem;
    }

    public List<CirurgiaItem> getCirurgiaItens() {
        return cirurgiaItens;
    }

    public void setCirurgiaItens(List<CirurgiaItem> cirurgiaItens) {
        this.cirurgiaItens = cirurgiaItens;
    }

    public List<ServicoItemCirurgiaItem> getItensDosItens() {
        return itensDosItens;
    }

    public void setItensDosItens(List<ServicoItemCirurgiaItem> itensDosItens) {
        this.itensDosItens = itensDosItens;
    }

    public List<Repasse> getRepasses() {
        return repasses;
    }

    public void setRepasses(List<Repasse> repasses) {
        this.repasses = repasses;
    }

    public List<Parcela> getPs() {
        return ps;
    }

    public void setPs(List<Parcela> ps) {
        this.ps = ps;
    }

//    Metodos de Listagem geral
    public List<Convenio> listaConvenios() {
        return cEJB.listarConvenios();
    }

    public List<Plano> listaPlanosPorConvenio() {
        return plaEJB.listaPlanosPorConvenio(convenio.getIdConvenio());
    }

    public List<Paciente> listaDePacientes() {
        return pEJB.listarPacientes();
    }

    public List<Terceiro> getListaTerceirosACarregar() {
        return listaTerceirosACarregar;
    }

    public List<UnidadeAtendimento> listaUnidadesDeATendimento() {
        return uaEJB.listarUnidadesAtendimento();
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public List<FormaPagamento> listaFormasDePagamento() {
        return fpEJB.listaFormasPagamento();
    }

    //Metodos de carregamento
    public void carregarListaProcedimentos() {
        listaProcedimentosACarregar = getListaProcedimentosACarregar();
    }

    public void carregarTerceiros() {
        listaTerceirosACarregar = tEJB.listarTerceiro();
    }

    public void carregarServicos() {
        listaDeServicosLazy = new LazyDataModel<Servico>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Servico> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE");
                        } else {
                            sf.append(" where s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE ");
                        }
                    } else {
                        sf.append(" and s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by s." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by s." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(sEJB.contarServicosRegistro(Clausula).toString()));
                listaDeServicos = sEJB.listarServicosLazyModeWhere(first, pageSize, Clausula);
                return listaDeServicos;
            }
        };
    }

    // metodos de selecionamento
    public void selecionarPaciente(Paciente paciente) {
        cirurgia.setIdPaciente(paciente);
    }

    public void selecionarPacientePesquisa(Paciente paciente) {
        pacientePesquisa = paciente.getIdPaciente();
        nomePacientePesquisa = paciente.getNome();
    }

    public void selecionarProcedimento(Procedimento proc) {
        procedimento = proc;
        cirurgia.setIdProcedimento(proc);
    }

    public void selecionaTerceiroResponsavel(Terceiro terceiro) {
        cirurgia.setIdTerceiro(terceiro);
    }

    public void selecionaTerceiroItem(Terceiro terceiro) {
        cirurgiaItem.setIdTerceiro(terceiro);
    }

    public void selecionarServico(Servico servico) {
        cirurgiaItem.setIdServico(servico);
    }

    public void selecionarFormaDePagamento(FormaPagamento formaPagamento1) {
        formaPagamento = formaPagamento1;
        cirurgia.setIdForma(formaPagamento1);
    }

    public void selecionarTipoInternacao(String s) {
        tipoInternacao = cirEJB.retornaTipoInternacao(s);
    }

    public void ReceberExterno(Cheque ch) {
        Usuario u = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
        try {
            Date d = new Date();
            Long numAleatorio = d.getTime();

            // Lançar pagamentos no Caixa
            Caixa caixa = new Caixa();
            caixa.setNumLote(numAleatorio.toString());
            caixa.setIdCheque(ch);
            caixa.setIdUsuario(u);
            caixa.setDtCaixa(ch.getDtRecebimentoEmissao());
            caixa.setTipoLancamento('C');
            caixa.setValor(ch.getValor());

            caixa.setStatus("Não Baixado");
            caixa.setIdCirurgia(cirurgia);
            caixa.setHistorico("Atendimento Externo nº " + cirurgia.getIdCirurgia() + ". Paciente: " + cirurgia.getIdPaciente().getNome());
            caixa.setNumeroParcela(ch.getNumeroParcela());
            caixaEJB.Salvar(caixa);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void concluirFechamentoExternoParcelado() {
        try {
            boolean pagamentosEmAberto = false;
            for (Cheque ch : listaChequeTemporaria) {
                if (ch.getIdCheque() == null) {
                    pagamentosEmAberto = true;
                }
            }
            if (pagamentosEmAberto) {
                Mensagem.addMensagem(3, "Ainda há pagamentos sem repasses lançados. Verifique os pagamentos novamente.");
            } else {
                cirurgia.setPago(true);
                cirEJB.atualizarFechamento(cirurgia);
                Mensagem.addMensagem(1, "Pagamentos salvos com sucesso.");
                RequestContext.getCurrentInstance().execute("PF('dlgLancarParcelado').hide()");
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar salvar fechamento." + e);
            System.out.println(e);
        }

    }

    public void pagarRepasseProporcional(Cheque ch) {
        try {
            percentualTotalPagamento = 0D;
            percentualTotalPagamento = (ch.getValor() * 100) / (cirurgia.getValor());
            String tipoMovimento = "D".equals(ch.getMovimento()) ? "Dinheiro" : ch.getMovimento().equals("C") ? "Cheque" : "N".equals(ch.getMovimento()) ? "Nota Promissória" : "B".equals(ch.getMovimento()) ? "Depósito Bancário" : "A".equals(ch.getMovimento()) ? "Cartão - Débito" : "R".equals(ch.getMovimento()) ? "Cartão - Crédito" : "";
            ch.setTipo("N");
            chequeEJB.Salvar(ch, us.retornaUsuario());
            if (!ch.getMovimento().equals("A") && !ch.getMovimento().equals("R")) {
                lancarRepassesContaCorrenteProporcional(ch, cirurgia.getCirurgiaItemList(), tipoMovimento, percentualTotalPagamento);
                ReceberExterno(ch);
            }
            Mensagem.addMensagem(1, "Pagamento salvo e repasses lançados com sucesso.");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar salvar pagamento!" + e);
            System.out.println(e);
        }

    }

    public void lancarRepassesContaCorrenteProporcional(Cheque ch, List<CirurgiaItem> listaItens, String tipoPagamento, Double percentualTotalPagamento) {
        try {
            for (CirurgiaItem ci : listaItens) {
                if (ci.getIdProcedimentoPaciente().getIdRegraRepasse().getRepasseSecundario()) {
                    // Lança o novo valor do repasse após o rateio
                    if (ci.getIdProcedimentoPaciente().getIdRegraRepasse().getRegraRepasseMedicosList().isEmpty()) {
                        Mensagem.addMensagem(3, "Verifique a regra de repasse nº ".concat(ci.getIdProcedimentoPaciente().getIdRegraRepasse().getIdRegraRepasse().toString()).concat(". Lista de médicos relacionados não encontrado."));
                    } else {
                        try {
                            for (RegraRepasseMedicos rrm : ci.getIdProcedimentoPaciente().getIdRegraRepasse().getRegraRepasseMedicosList()) {
                                Double valorRateioRepasse = (ci.getValorTotalGeral() * rrm.getFatorRepasseSecundario() / 100);
                                Double valorProporcional = (valorRateioRepasse * percentualTotalPagamento) / 100;
                                Repasse repasse = new Repasse();
                                repasse.setIdCirurgia(cirurgia);
                                repasse.setNumeroParcela(ch.getNumeroParcela());
                                repasse.setIdCheque(ch);
                                repasse.setIdPaciente(ch.getIdPaciente());
                                repasse.setValor(valorProporcional);
                                repasse.setDtLancamento(cheque.getDtRecebimentoEmissao());
                                repasse.setDtRecebimento(cheque.getDtRecebimentoEmissao());
                                repasse.setDtVencimento(cheque.getDtCompensacaoVencimento());
                                repasse.setIdTerceiro(rrm.getIdTerceiro());
                                repasse.setTipoPagamento('P');
                                repasse.setPercentualRecebido(100D);
                                repasse.setSituacaoPaciente('P');
                                repasse.setStatus('F');
                                repasse.setValor(0D);
                                repasse.setValorRecebido(valorProporcional);
                                repasse.setValorDocumento(valorProporcional);
                                repasse.setIdCirurgiaItem(ci);
                                repasse.setFormaPagamento(tipoPagamento);
                                repasse.setSituacaoTerceiro('L');
                                lancarContaCorrente(repasse);
                            }
                        } catch (Exception e) {
                            Mensagem.addMensagem(3, "Verifique a regra de repasse nº ".concat(ci.getIdProcedimentoPaciente().getIdRegraRepasse().getIdRegraRepasse().toString()).concat(". Erro ao realizar os repasses relacionados."));
                            System.out.println(e);
                        }

                    }
                    // CHARLES - 23022018
                    // Só lançar os repasses se o valor for diferente de zero. :-/
                } else if (!ci.getValorTotalGeral().equals(0D)) {
                    // CHARLES - 26062019
                    // Permitir parcelamento de atendimentos externos.
                    Double valorProporcional = (ci.getValorTotal() * percentualTotalPagamento) / 100;

                    Repasse repasse = new Repasse();
                    repasse.setIdCirurgia(cirurgia);
                    repasse.setNumeroParcela(ch.getNumeroParcela());
                    repasse.setIdCheque(ch);
                    repasse.setIdPaciente(ch.getIdPaciente());
                    repasse.setValor(valorProporcional);
                    repasse.setDtLancamento(ch.getDtRecebimentoEmissao());
                    repasse.setDtRecebimento(ch.getDtRecebimentoEmissao());
                    repasse.setDtVencimento(ch.getDtCompensacaoVencimento());
                    repasse.setIdTerceiro(ci.getIdTerceiro());
                    repasse.setTipoPagamento('P');
                    repasse.setPercentualRecebido(100D);
                    repasse.setSituacaoPaciente('P');
                    repasse.setStatus('F');
                    repasse.setValor(0D);
                    repasse.setValorRecebido(valorProporcional);
                    repasse.setValorDocumento(valorProporcional);
                    repasse.setIdCirurgiaItem(ci);
                    repasse.setFormaPagamento(tipoPagamento);
                    repasse.setSituacaoTerceiro('L');
                    lancarContaCorrente(repasse);
                }
            }
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void lancarContaCorrente(Repasse repasse) {
        try {
            ContaCorrente cc = new ContaCorrente();
            cc.setIdTerceiro(repasse.getIdTerceiro());
            cc.setIdRepasse(repasse);
            cc.setIdCirurgia(cirurgia);
            cc.setIdPaciente(repasse.getIdPaciente());
            cc.setIdCheque(repasse.getIdCheque().getIdCheque());
            cc.setTipo("C");
            cc.setDtLancamento(new Date());
            cc.setSituacao("Não Liberado");
            cc.setStatus("Não Retirado");
            cc.setValor(repasse.getValorDocumento());
            if (repasse.getIdCirurgiaItem().getIdProcedimentoPaciente().getIdRegraRepasse().getRepasseSecundario()) {
                cc.setObservacao("Rateio referente ao atendimento externo nº " + cc.getIdCirurgia().getIdCirurgia() + ", paciente " + repasse.getIdPaciente().getNome() + ".");
            } else {
                cc.setObservacao("Atendimento externo avulso nº " + cc.getIdCirurgia().getIdCirurgia() + " referente ao paciente " + repasse.getIdPaciente().getNome() + ".");
            }

            ccEJB.SalvarSimples(cc, null);
            //System.out.println(repasse.getIdCirurgiaItem().getIdCirurgiaItem());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void selecionarCheque(Cheque ch) {
        cheque = ch;
        servicoItemCirurgiaItem = new ServicoItemCirurgiaItem();
        listaServicosPorTerceirosTemporaria = new ArrayList<>();
        for (Repasse r : repassesConsolidados) {
            if (r.getIdCheque() == cheque) {
                ServicoItemCirurgiaItem si = r.getIdSici();
                listaServicosPorTerceirosTemporaria.add(si);
            }
        }
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String automatico = params.get("automatico");

        desabilitarAutomatico = Boolean.TRUE;
        if (automatico.equals("sim")) {
            if (assEJB.carregarAssociacao().getGerarRepassesSugeridos()) {
                // CHARLES 07072017
                // CALCULAR PROPORCIONALMENTE O PAGAMENTO AOS REPASSE (SUGESTÃO)
                if (listaServicosPorTerceirosTemporaria.isEmpty()) {
                    // Acha a proporção do pagamento ao valor total do fechamento.

                    Double valorTotal = cirurgia.getValor();
                    percentualTotalPagamento = 0D;
                    percentualTotalPagamento = (ch.getValor() * 100) / (valorTotal);
                    List<ServicoItemCirurgiaItem> listTemp = new ArrayList<>();

                    if (repassesConsolidados.isEmpty()) {
                        listTemp = itensDosItens;
                    } else {
                        for (ValoresTerceiros v : vltAgrupados) {
                            listTemp.addAll(v.getItensPorTerceiros());
                            //System.out.println("lista: " + listTemp.size());
                        }
                    }

                    for (ServicoItemCirurgiaItem sici : listTemp) {
                        Double valor = 0D;
                        valor = (sici.getValorTaxado() * percentualTotalPagamento) / 100;
                        ServicoItemCirurgiaItem it = new ServicoItemCirurgiaItem();
                        it.setIdCirurgia(sici.getIdCirurgia());
                        it.setIdCirurgiaItem(sici.getIdCirurgiaItem());
                        it.setIdServicoItem(sici.getIdServicoItem());
                        it.setIdTerceiro(sici.getIdTerceiro());
                        it.setValor(arredondar_double(sici.getValor()));
                        it.setValorPago(valor + sici.getValorPago());
                        it.setTaxa(sici.getTaxa());
                        it.setValorTaxado(sici.getValorTaxado());
                        it.setTaxaValor(sici.getTaxaValor());
                        it.setPrincipal("S");                                             // POG autêntica            
                        lancarRepasseAutomatico(cheque, it, valor);
                        listaServicosPorTerceirosTemporaria.add(it);
                    }
                    valoresTerceiros.setItensPorTerceiros(listaServicosPorTerceirosTemporaria);
                    System.out.println(valoresTerceiros.getItensPorTerceiros().size());
                    calcularRestanteValoresTerceiros();
                    calcularValoresTerceirosAutomatico(listaServicosPorTerceirosTemporaria);
                    Mensagem.addMensagem(1, "Repasses lançados com sucesso.");

                } else {
                    zerarCamposRepasse();
                    valorTotalaRepassar = 0D;
                    valorRepassado = 0D;
                    calcularRestanteCheque();
                    dtVencimento = cheque.getDtCompensacaoVencimento();
                }
            } else {
                zerarCamposRepasse();
                valorTotalaRepassar = 0D;
                valorRepassado = 0D;
                calcularRestanteCheque();
                dtVencimento = cheque.getDtCompensacaoVencimento();
            }
        } else {
            zerarCamposRepasse();
            valorTotalaRepassar = 0D;
            valorRepassado = 0D;
            calcularRestanteCheque();
            dtVencimento = cheque.getDtCompensacaoVencimento();
            manual = true;
        }

    }

    public void selecionarServicoItemCirurgiaItem(ServicoItemCirurgiaItem sici) {
        valorRecebido = 0D;
        servicoItemCirurgiaItem = sici;
        if (servicoItemCirurgiaItem != null) {
            valorRecebido = (servicoItemCirurgiaItem.getValor() - servicoItemCirurgiaItem.getTaxaValor()) - (servicoItemCirurgiaItem.getValorPago() != null ? servicoItemCirurgiaItem.getValorPago() : 0D);
            valorRecebido = arredondar_double(valorRecebido);
        }

        if (widgetValorRestante <= valorRecebido) {
            setValorRecebido(widgetValorRestante);
        }
    }

    public Boolean verificarLancamentoManuais() {
        Boolean flag = false;
        for (Cheque chq1 : listaCheque) {
            if (chq1.getLancamentoManual() != null) {
                if (chq1.getLancamentoManual().equals("S")) {
                    flag = true;
                    manual = true;
                }
            }
        }
        System.out.println("retorno: " + flag);
        return flag;
    }

    public void recalcularCheque(Cheque c) {
        Double valorPago = 0D;
        Double valorTotal = 0D;
        Double valorRestante = 0D;
        for (Repasse rep : repassesConsolidados) {
            if (rep.getIdCheque() == c) {
                valorPago = valorPago + rep.getValor();
            }
        }
        if (valorPago == 0) {
            valorTotal = c.getValor();
        }
        valorRestante = valorTotal - valorPago;
        widgetValorTotal = valorTotal;
        widgetValorPago = valorPago;
        widgetValorRestante = valorRestante;
        dtVencimento = c.getDtCompensacaoVencimento();
    }

    public void calcularRestanteCheque() {
        Double valorPago = 0D;
        Double valorTotal = 0D;
        Double valorRestante = 0D;
        for (Repasse rep : repassesConsolidados) {
            if (rep.getIdCheque() == cheque) {
                valorPago = valorPago + rep.getValor();
            }
        }
        valorTotal = cheque.getValor();
        valorRestante = valorTotal - valorPago;
        widgetValorTotal = valorTotal;
        widgetValorPago = valorPago;
        widgetValorRestante = valorRestante;

    }

    public void selecionarValoresTerceiro(ValoresTerceiros vt) {
        valoresTerceiros = vt;

    }

    public void calcularTotalItem() {
        if (cirurgiaItem.getValorUnitario() == null) {
            cirurgiaItem.setValorUnitario(0D);
        }
        if (cirurgiaItem.getQuantidade() == null) {
            cirurgiaItem.setQuantidade(0D);
        }
        cirurgiaItem.setValorTotal(cirurgiaItem.getValorUnitario() * cirurgiaItem.getQuantidade());
    }

    public void calcularTotalItem(CirurgiaItem ci) {
        if (ci.getValorUnitario() == null) {
            ci.setValorUnitario(0D);
        }
        if (ci.getQuantidade() == null) {
            ci.setQuantidade(0D);
        }
        ci.setValorTotal(ci.getValorUnitario() * ci.getQuantidade());
    }

    public void calcularTotalItemIndex(CirurgiaItem ci, int index) {
        Double valorAntigo = 0D;
        if (ci.getValorUnitario() == null) {
            ci.setValorUnitario(0D);
        }
        if (ci.getQuantidade() == null) {
            ci.setQuantidade(0D);
        }
        valorAntigo = ci.getValorTotal();
        ci.setValorTotal(ci.getValorUnitario() * ci.getQuantidade());
        cirurgiaItens.remove(index);
        cirurgiaItens.add(index, ci);
        if (!valorAntigo.equals(ci.getValorTotal())) {
            calcularTotalItensGeral();
        }
    }

    public void calcularTotalItensGeral() {
        if (cirurgia.getIdForma() != null && cirurgia.getIdProcedimento() != null && cirurgia.getIdTerceiro() != null) {
            if (cirurgiaItens.isEmpty()) {
                Mensagem.addMensagemGrowl(2, "Adicione itens para calcular o fechamento.");
            } else {
                boolean itensZerado = false;
                for (CirurgiaItem cii : cirurgiaItens) {
                    if (cii.getValorTotal() == 0D || cii.getValorTotal() == null) {
                        Mensagem.addMensagemGrowl(2, "Existem itens com valor zerado. Verifique os valores.");
                        itensZerado = true;
                    }
                }

                if (!itensZerado) {
                    List<CirurgiaItem> listaTemporaria;
                    listaTemporaria = cirurgiaItens;
                    cirurgiaItens = new ArrayList<CirurgiaItem>();
                    for (CirurgiaItem ci : listaTemporaria) {
                        if (ci.getValorUnitario() == null) {
                            ci.setValorUnitario(0D);
                        }
                        if (ci.getQuantidade() == null) {
                            ci.setQuantidade(0D);
                        }
                        ci.setValorTotal(ci.getValorUnitario() * ci.getQuantidade());
                        cirurgiaItem = ci;
                        addItem("N");
                        carregarRepasses();
                        calcularValores();
                    }
                    Mensagem.addMensagemGrowl(1, "Cálculos realizado com sucesso!");
                }
            }
        } else {
            Mensagem.addMensagemGrowl(3, "Informe os campos obrigatórios.");
        }

    }

    public void carregarRepasses() {
        BCRUtils.ResetarDatatableFiltros(":frmInserir:tblRepasses");
        itensDosItens = new ArrayList<>();
        valorTotalItensHospital = 0D;

        for (CirurgiaItem ci : cirurgiaItens) {
            Double valorBase;
            ServicoItem itemB;
            itemB = sEJB.retornaServicoPadraoPorItemFechamento(ci);
            valorBase = arredondar_double(sEJB.retornaValorBaseServico(ci));

            for (ServicoItem sii : ci.getIdServico().getServicoItemList()) {
                ServicoItemCirurgiaItem sicii = new ServicoItemCirurgiaItem();

                if (sii.getIdServico().getTipo().equals("H")) {
                    valorBase = ci.getValorTotal() / sii.getIdServico().getEscala();
                    valorTotalItensHospital = valorTotalItensHospital + valorBase;
                    sicii.setIdServicoItem(sii);
                    sicii.setIdTerceiro(ci.getIdTerceiro());
                    sicii.setValorTaxado(0D);
                    sicii.setTaxa(0D);
                    sicii.setValor(arredondar_double(valorTotalItensHospital));
                    sicii.setIdCirurgiaItem(ci);
                    itensDosItens.add(0, sicii);
                }

                if (sii.getIdServico().getTipo().equals("N")) {
//                    sicii.setIdTerceiro(null);
                    sicii.setIdServicoItem(sii);
                    sicii.setValorTaxado(0D);
                    sicii.setTaxa(0D);
                    sicii.setIdTerceiro(null);
                    sicii.setIdCirurgiaItem(ci);

                    if (itemB == null) {
                        sicii.setValor(arredondar_double((ci.getValorTotal() * sii.getPercentual()) / 100));
                    } else if (sii.equals(itemB)) {
                        sicii.setValor(arredondar_double(valorBase));
                    } else {
                        sicii.setValor(arredondar_double((valorBase * sii.getPercentual()) / 100));
                    }

                    if (ci.getIdTerceiro() != null) {
                        sicii.setIdTerceiro(ci.getIdTerceiro());
                    }
                    itensDosItens.add(sicii);
                }
            }
        }

//        if ((valorTotalItensHospital > 0)) {
//            ServicoItemCirurgiaItem sicii = new ServicoItemCirurgiaItem();
//            List<ServicoItemCirurgiaItem> itensDosItensTemp;
//            servico = retornaServicoPadrao();
//            ServicoItem item = servico.getServicoItemList().get(0);
//            if (sicii.getIdTerceiro() == null) {
//                Terceiro t = new Terceiro();
//                t = tEJB.selecionarTerceiroCNPJCPF("61.635.808/0001-30", "Hospital");
//                sicii.setIdTerceiro(t);
//                sicii.setIdServicoItem(item);
//                sicii.setValorTaxado(0D);
//                sicii.setTaxa(0D);
//                sicii.setValor(arredondar_double(valorTotalItensHospital));
//                itensDosItensTemp = itensDosItens;
//                itensDosItens = new ArrayList<>();
//                itensDosItens.add(0, sicii);
//                itensDosItens.addAll(itensDosItensTemp);
//            }
//        }
        // Recalcular sem ajax
        Integer i = 0;
        if (!itensDosItens.isEmpty()) {
            for (ServicoItemCirurgiaItem sici : itensDosItens) {
                calcularDescontoPercentual(sici, i);
                i++;
            }
        }

    }

    public Servico retornaServicoPadrao() {
        return sEJB.retornaServicoPadrao();
    }

    public void addItem(String flag) {
        if (validarItensCamposObrigatorios()) {
            if (cirurgiaItem.getIdServico() == null) {
                Mensagem.addMensagem(3, "Informe um item antes de adicionar!");
            } else {
                if (cirurgiaItem.getIdServico().getTipo().equals("H")) {
                    cirurgiaItem.setIdTerceiro(tEJB.selecionarTerceiroCNPJCPF("18.170.894/0001-23", "Hospital"));
                }
                cirurgiaItens.add(cirurgiaItem);
                Double tot = 0D;
                cirurgiaItem.setValorTotal(arredondar_double(cirurgiaItem.getValorTotal()));
                tot = cirurgiaItem.getValorTotal();
                Double valorAnterior = cirurgia.getValor();
                cirurgia.setValor(valorAnterior + tot);

                calcularValorInternacao();
                zerarListas();
                limparItem();
                if (flag.equals("S")) {
                    Mensagem.addMensagemGrowl(1, "Item adicionado com sucesso!");
                }

            }
        }
    }

    public void verificarUsuarioLogado() {
        if (uEJB.retornaUsuarioDaSessao().getIdPapel().getProntoAtendimento()) {
            paramUsuario = uEJB.retornaUsuarioDaSessao().getIdUsuario();
        }
    }

    public void limparItem() {
        cirurgiaItem = new CirurgiaItem();
        cirurgiaItem.setValorTotal(0D);
        cirurgiaItem.setValorUnitario(0D);
        cirurgiaItem.setQuantidade(1D);
    }

    public void zerarListas() {
        itensDosItens = new ArrayList<>();
        repasses = new ArrayList<>();
        repassesConsolidados = new ArrayList<>();
        psAgrupado = new ArrayList<>();

        valorFinal = 0D;
        valorFinalHospital = 0D;
        valorFinalMedico = 0D;
        valorCirur = 0D;
        valorCirurHospital = 0D;
        valorCirurMedico = 0D;
        valorDif = 0D;
        valorDifMedico = 0D;
        valorDifHospital = 0D;
        cirEJB.deletarContaCorrente(cirurgia);
        cirEJB.deletarRepasse(cirurgia);
        if (!cirurgia.isCancelado()) {
            listaCheque = new ArrayList<>();
            cirEJB.deletarPagamentos(cirurgia);
            cirurgia.setChequeList(null);
        }
        cirurgia.setRepasseList(null);
        cirurgia.setContaCorrenteList(null);

    }
    List<Repasse> repasses = new ArrayList<Repasse>();

    public boolean verificarValoresZerados() {
        Integer i = 0;
        for (ServicoItemCirurgiaItem siii2 : itensDosItens) {
            if (siii2.getValor() == 0D) {
                i = 1;
                break;
            }
        }
        if (i == 1) {
            return true;
        } else {
            return false;
        }

    }

    public void carregaOrcamentos() {
        orctosAPesquisar = oEJB.listaOrcamentos();
    }

    public void processarItensDosItens() {
        if (itensDosItens.isEmpty()) {
            Mensagem.addMensagemGrowl(2, "Clique em Calcular na tabela acima. Depois clique em Processar novamente.");
        } else if (validarCamposObrigatorios()) {
            if (verificarValoresZerados()) {
                Mensagem.addMensagem(3, "Atenção existem valores zerados! Informe o valor dos itens corretamente.");
            } else {
                List<ServicoItemCirurgiaItem> siv = new ArrayList<ServicoItemCirurgiaItem>();

                if (!listadeItensDosItens.isEmpty()) {
                    for (ServicoItemCirurgiaItem si : listadeItensDosItens) {
                        if (si.getIdSici() != null) {
                            cirEJB.deletarRepaases(si);
                        }
                    }
                }

                listadeItensDosItens = siv;
                for (ServicoItemCirurgiaItem siii : itensDosItens) {
                    listadeItensDosItens.add(siii);
                }

                if (formaPagamento.getIdForma() == null) {
                    Mensagem.addMensagem(3, "Informe a forma de pagamento!");
                    RequestContext.getCurrentInstance().execute("PF('dlgConsultarFormas').show()");
                } else {
                    Double valorCirurgia = 0D;
                    for (ServicoItemCirurgiaItem siiii : itensDosItens) {
                        valorCirurgia = siiii.getValorTaxado() + valorCirurgia;
                    }
                    cirurgia.setValor(valorCirurgia);
                    if (!cirurgia.isCancelado()) {
                        listaCheque = new ArrayList<>();
                        listaChequeTemporaria = new ArrayList<>();
                        chequeEJB.excluirChequesPorFechamento(cirurgia.getIdCirurgia(), us.retornaUsuario());
                    }
                    //gerarParcelas();
                    calcularTotalRepasse();
                    calcularValoresTerceiros();
                    if (assEJB.carregarAssociacao().getImprimirReciboAtendimentoInterno()) {
                        Salvar();
                    } else {
                        Mensagem.addMensagemGrowl(1, "Os itens foram processados com sucesso!");
                    }

                }

            }
        }
    }

    public void mudaAba() {
        RequestContext.getCurrentInstance().execute("$( '#colTree' ).removeClass( 'collapsed' );");
        RequestContext.getCurrentInstance().execute("document.getElementById('colTree').className = '';");
    }

    public void mudarMascara() {
        if (CPFCPNJ) {
            CPFCPNJ = false;
        } else {
            CPFCPNJ = true;
        }
        CPF = "";
        validacaoCGC = false;
    }
    private Double totalParcela;

    public Double getTotalParcela() {
        return totalParcela;
    }

    public void setTotalParcela(Double totalParcela) {
        this.totalParcela = totalParcela;
    }

    public void calcularTotalRepasse() {
        Double val = 0D;
        for (Repasse rr : repassesConsolidados) {
            if (rr.getValor().equals(0D)) {
                val = rr.getValorDocumento() + val;
            } else {
                val = rr.getValor() + val;
            }
        }
        totalParcela = arredondar_double(val);
        //System.out.println("Valor total dos repasses:" + totalParcela);
    }

    public void calcularValoresTerceiros() {
        ArrayList<Integer> idterceiros = new ArrayList<>();
        ArrayList<Integer> idterceirosSemRepeticao = new ArrayList<>();
        ArrayList<ServicoItemCirurgiaItem> itensPorTerceirosTemporaria = new ArrayList<>();
        vltAgrupados = new ArrayList<ValoresTerceiros>();
        for (ServicoItemCirurgiaItem r : itensDosItens) {
            if (!idterceiros.contains(r.getIdTerceiro().getIdTerceiro())) {
                idterceiros.add(r.getIdTerceiro().getIdTerceiro());
            }
        }
        idterceirosSemRepeticao.addAll(idterceiros);
        idterceirosSemRepeticao.remove(null);
        idterceiros = new ArrayList<Integer>();
        idterceiros.addAll(idterceirosSemRepeticao);

        for (Integer id : idterceiros) {
            Double val = 0D;
            ValoresTerceiros vt = new ValoresTerceiros();
            itensPorTerceirosTemporaria = new ArrayList<>();
            for (ServicoItemCirurgiaItem ter : itensDosItens) {
                if (ter.getIdTerceiro().getIdTerceiro() == id) {
                    val = val + (ter.getValor() - ter.getTaxaValor());
                    val = arredondar_double(val);
                    if (ter.getValorPago() == null) {
                        ter.setValorPago(0D);
                    }
                    itensPorTerceirosTemporaria.add(ter);
                }
            }
            vt.setIdTerceiro(id);
            vt.setValor(val);
            vt.setValorPago(0D);
            vt.setValorRestante(val);
            vt.setTerceiro(cirEJB.pesquisaNomePorID(id));
            vt.setItensPorTerceiros(itensPorTerceirosTemporaria);
            vltAgrupados.add(vt);
            calcularValoresPagos();
        }
    }

    public void calcularValoresTerceirosEditar() {
        ArrayList<Integer> idterceiros = new ArrayList<Integer>();
        ArrayList<Integer> idterceirosSemRepeticao = new ArrayList<Integer>();
        ArrayList<ServicoItemCirurgiaItem> itensPorTerceirosTemporaria = new ArrayList<ServicoItemCirurgiaItem>();
        vltAgrupados = new ArrayList<ValoresTerceiros>();
        for (ServicoItemCirurgiaItem r : itensDosItens) {
            if (!idterceiros.contains(r.getIdTerceiro().getIdTerceiro())) {
                idterceiros.add(r.getIdTerceiro().getIdTerceiro());
            }
        }
        idterceirosSemRepeticao.addAll(idterceiros);
        idterceirosSemRepeticao.remove(null);
        idterceiros = new ArrayList<Integer>();
        idterceiros.addAll(idterceirosSemRepeticao);

        for (Integer id : idterceiros) {
            Double val = 0D;
            Double valPago = 0D;
            ValoresTerceiros vt = new ValoresTerceiros();
            itensPorTerceirosTemporaria = new ArrayList<ServicoItemCirurgiaItem>();
            for (ServicoItemCirurgiaItem ter : itensDosItens) {
                if (ter.getIdTerceiro().getIdTerceiro() == id) {
                    val = val + (ter.getValor() - ter.getTaxaValor());
                    val = arredondar_double(val);
                    if (ter.getValorPago() == null) {
                        ter.setValorPago(0D);
                    }
                    valPago = valPago + ter.getValorPago();
                    itensPorTerceirosTemporaria.add(ter);
                }
            }
            vt.setIdTerceiro(id);
            vt.setValor(val);
            vt.setValorPago(valPago);
            vt.setValorRestante(val);
            vt.setTerceiro(cirEJB.pesquisaNomePorID(id));
            vt.setItensPorTerceiros(itensPorTerceirosTemporaria);
            vltAgrupados.add(vt);
            calcularValoresPagos();
        }
    }

    public void calcularValoresTerceirosAutomatico(List<ServicoItemCirurgiaItem> listaJaModificada) {
        ArrayList<Integer> idterceiros = new ArrayList<>();
        ArrayList<Integer> idterceirosSemRepeticao = new ArrayList<>();
        ArrayList<ServicoItemCirurgiaItem> itensPorTerceirosTemporaria = new ArrayList<>();
        vltAgrupados = new ArrayList<>();
        for (ServicoItemCirurgiaItem r : listaJaModificada) {
            if (!idterceiros.contains(r.getIdTerceiro().getIdTerceiro())) {
                idterceiros.add(r.getIdTerceiro().getIdTerceiro());
            }
        }
        idterceirosSemRepeticao.addAll(idterceiros);
        idterceirosSemRepeticao.remove(null);
        idterceiros = new ArrayList<>();
        idterceiros.addAll(idterceirosSemRepeticao);

        for (Integer id : idterceiros) {
            Double val = 0D;
            Double valPago = 0D;
            ValoresTerceiros vt = new ValoresTerceiros();
            itensPorTerceirosTemporaria = new ArrayList<>();
            for (ServicoItemCirurgiaItem ter : listaJaModificada) {
                if (ter.getIdTerceiro().getIdTerceiro() == id) {
                    val = val + (ter.getValor() - ter.getTaxaValor());
                    val = arredondar_double(val);
                    if (ter.getValorPago() == null) {
                        ter.setValorPago(0D);
                    }
                    valPago = valPago + ter.getValorPago();
                    itensPorTerceirosTemporaria.add(ter);
                }
            }
            vt.setIdTerceiro(id);
            vt.setValor(val);
            vt.setValorPago(valPago);
            vt.setValorRestante(val);
            vt.setTerceiro(cirEJB.pesquisaNomePorID(id));
            vt.setItensPorTerceiros(itensPorTerceirosTemporaria);
            vltAgrupados.add(vt);
            calcularValoresPagos();
        }
    }

    public void calcularValoresPagos() {
        valorInfoCheques = 0D;
        valorTotalCirurgia = 0D;
        for (Cheque chq : listaCheque) {
            valorInfoCheques = valorInfoCheques + chq.getValor();
        }
        valorTotalCirurgia = cirurgia.getValor();
        if (listaCheque.isEmpty()) {
            desabilitarAutomatico = Boolean.FALSE;
        }
    }

    // Seta objetos pelo tipo da Internação
    public void retornaTipoLancamento(AjaxBehaviorEvent event) {
        Integer teste = Integer.parseInt(event.getComponent().getAttributes().get("value").toString());
        String padrao;
        TipoInternacao ti = tiEJB.selecionarPorID(teste, us.retornaUsuario());

        if (ti.getDescricao().equals("Exame")) {
            padrao = "E";
        } else {
            padrao = "C";
        }
        cirurgia.setIdProcedimento(proEJB.selecionarPorPadrao(padrao));
        if (padrao.equals("C")) {
            cirurgiaItem.setIdServico(sEJB.selecionarPorPadrao(padrao));
        }
    }

    public void reiniciarLancamentoExternoParcelado(Cirurgia c) {
        cirurgia = c;
        System.out.println("aqui");
        zeraVariaveisCheque();
        listaChequeTemporaria = new ArrayList<>();
        listaCheque = new ArrayList<>();
        movimento = "C";
        pagamento = "Cheque";
        compensado = "N";
        valorTotalPago = 0D;
        valorCheque = cirurgia.getValor();
        banco = new Banco();
        paciente = cirurgia.getIdPaciente();
        if (cirurgia.getIdResponsavel() != null) {
            emitenteCheque = cirurgia.getIdResponsavel().getNome();
            CPF = cirurgia.getIdResponsavel().getCpf();
        } else {

            emitenteCheque = paciente.getNome();
            if (paciente.getCpf() != null) {
                CPF = paciente.getCpf();
            } else {
                CPF = "";
            }

        }

        dtEmissao = new Date();
        dtCompensado = new Date();
        situacao = "";
        valorTotal = 0D;
        validacaoCGC = true;

    }

    public void carregarValoresExterno(Cirurgia c) {
        cirurgia = c;
        carregaValoresCheque();
    }

    public void carregaValoresCheque() {
        zeraVariaveisCheque();
        verificarSaldoRestante();
        listaChequeTemporaria = new ArrayList<>();
        movimento = "C";
        pagamento = "Cheque";
        compensado = "N";
        valorCheque = valorRestante;
        banco = new Banco();
        paciente = cirurgia.getIdPaciente();
        if (cirurgia.getIdResponsavel() != null) {
            emitenteCheque = cirurgia.getIdResponsavel().getNome();
            CPF = cirurgia.getIdResponsavel().getCpf();
        } else {

            emitenteCheque = paciente.getNome();
            if (paciente.getCpf() != null) {
                CPF = paciente.getCpf();
            } else {
                CPF = "";
            }

        }

        dtEmissao = new Date();
        dtCompensado = new Date();
        situacao = "";
        valorTotal = 0D;
        validacaoCGC = true;
    }

    public void setarItensTerceiro() {
        valorRecebido = 0D;
        if (valoresTerceiros != null) {
            System.out.println("1");
            if (valoresTerceiros.getItensPorTerceiros().size() == 1) {
                System.out.println("2");
                servicoItemCirurgiaItem = valoresTerceiros.getItensPorTerceiros().get(0);
                if (servicoItemCirurgiaItem != null) {
                    valorRecebido = arredondar_double((servicoItemCirurgiaItem.getValor() - servicoItemCirurgiaItem.getTaxaValor()) - (servicoItemCirurgiaItem.getValorPago() != null ? servicoItemCirurgiaItem.getValorPago() : 0D));
                }
                System.out.println("3");
            } else {
                servicoItemCirurgiaItem = new ServicoItemCirurgiaItem();
                valorRecebido = 0D;
                listaServicosPorTerceiros();
                Mensagem.addMensagem(2, "Selecione o item a repassar.");
                RequestContext.getCurrentInstance().execute("PF('dlgItensTerceiro').show();");
            }
            System.out.println("4");

            if (widgetValorRestante <= valorRecebido) {
                setValorRecebido(widgetValorRestante);
            }

        }
    }

    public void listaServicosPorTerceiros() {
        listaServicosPorTerceiros = valoresTerceiros.getItensPorTerceiros();
    }

    public void zerarCamposRepasse() {
        servicoItemCirurgiaItem = new ServicoItemCirurgiaItem();
        valorRecebido = null;
        dtVencimento = cheque.getDtCompensacaoVencimento();
        observacao = null;
    }

    public void salvarItensServicos() {
        if (verificarCampos()) {
            if ((valorRecebido.equals(0D))) {
                Mensagem.addMensagem(3, "Valor restante neste pagamento: R$ " + (arredondar_double(valorRestante)));
            } else if (valorRecebido > arredondar_double(widgetValorRestante)) {
                Mensagem.addMensagem(3, "Valor excedente. Total disponível neste pagamento: R$ " + (arredondar_double(widgetValorRestante)));
            } else if (valorRecebido > (arredondar_double(servicoItemCirurgiaItem.getValor() - (servicoItemCirurgiaItem.getValorPago() != null ? servicoItemCirurgiaItem.getValorPago() : 0D)))) {
                Mensagem.addMensagem(3, "Valor excedente. Saldo a Pagar: R$ " + (arredondar_double(servicoItemCirurgiaItem.getValor() - (servicoItemCirurgiaItem.getValorPago() != null ? servicoItemCirurgiaItem.getValorPago() : 0D))));
            } else {
                ServicoItemCirurgiaItem item = new ServicoItemCirurgiaItem();
                item.setIdCirurgia(servicoItemCirurgiaItem.getIdCirurgia());
                item.setIdCirurgiaItem(servicoItemCirurgiaItem.getIdCirurgiaItem());
                item.setIdServicoItem(servicoItemCirurgiaItem.getIdServicoItem());
                item.setIdTerceiro(servicoItemCirurgiaItem.getIdTerceiro());
                item.setValor(arredondar_double(servicoItemCirurgiaItem.getValor()));
                item.setValorPago(servicoItemCirurgiaItem.getValorPago() != null ? (arredondar_double(servicoItemCirurgiaItem.getValorPago() + valorRecebido)) : arredondar_double(valorRecebido));
                item.setTaxa(servicoItemCirurgiaItem.getTaxa());
                item.setValorTaxado(servicoItemCirurgiaItem.getValorTaxado());
                item.setTaxaValor(servicoItemCirurgiaItem.getTaxaValor());
                item.setPrincipal("S");                                             // POG autêntica
                lancarRepasse(item, valorRecebido, dtVencimento);
                listaServicosPorTerceirosTemporaria.add(item);

                for (ServicoItemCirurgiaItem sici : valoresTerceiros.getItensPorTerceiros()) {
                    if (sici.getIdServicoItem() == item.getIdServicoItem() && sici.getIdCirurgiaItem() == item.getIdCirurgiaItem()) {
                        sici.setValorPago(arredondar_double(sici.getValorPago() != null ? (sici.getValorPago()) + valorRecebido : valorRecebido));
                    }
                }
                calcularRestanteCheque();
                zerarCamposRepasse();
                calcularRestanteValoresTerceiros();
                calcularTotalRepasse();
                valoresTerceiros = new ValoresTerceiros();
            }
        } else {
            Mensagem.addMensagem(3, "Informe os campos necessários.");
        }
    }

    public boolean verificarCampos() {
        if (servicoItemCirurgiaItem == null) {
            Mensagem.addMensagem(3, "Informe o item a ser repassado!");
            return false;
        } else {
            return true;
        }
    }

    public void calcularRestanteValoresTerceiros() {
        int i = 0;
        int i2 = 1;
        Double valorTotal, valorPago, valorRestante;
        valorTotal = 0D;
        valorPago = 0D;
        valorRestante = 0D;
        i = valoresTerceiros.getItensPorTerceiros().size();
        for (ServicoItemCirurgiaItem seici : valoresTerceiros.getItensPorTerceiros()) {
            valorTotal += seici.getValorTaxado();
            //System.out.println("Terceiro: " + seici.getIdTerceiro().getIdTerceiro() + "Valor taxado" + seici.getValorTaxado());
//            valorTotal = valorTotal + (seici.getValor() - seici.getTaxaValor());
            valorPago = valorPago + seici.getValorPago();
            valorRestante = valorTotal - valorPago;
            if (i == i2) {
                valoresTerceiros.setValorPago(arredondar_double(valorPago));
                valoresTerceiros.setValorRestante(arredondar_double(valorRestante));
                break;
            } else {
                i2++;
            }
        }
    }

    public void calcularRestanteValoresTerceirosTotais() {
        int i;
        int i2;
        Double valorTotal, valorPago, valorRestante;
        valorTotal = 0D;
        valorPago = 0D;
        valorRestante = 0D;
        for (ValoresTerceiros vt : vltAgrupados) {
            i = 0;
            i2 = 1;
            valorTotal = 0D;
            valorPago = 0D;
            valorRestante = 0D;
            i = vt.getItensPorTerceiros().size();
            for (ServicoItemCirurgiaItem seici : vt.getItensPorTerceiros()) {
                valorTotal = valorTotal + seici.getValor();
                valorPago = valorPago + seici.getValorPago();
                valorRestante = valorTotal - valorPago;
                if (i == i2) {
                    vt.setValorPago(arredondar_double(valorPago));
                    vt.setValorRestante(arredondar_double(valorRestante));
                    break;
                } else {
                    i2++;
                }
            }
        }

    }

    public void lancarRepasse(ServicoItemCirurgiaItem item, Double valorLancamento, Date datatVencimento) {
        Double valor = 0D;
        try {
            Repasse r = new Repasse();
            valor = valorLancamento;
            r.setNumeroParcela(cheque.getNumeroParcela());
            r.setIdCheque(cheque);
            r.setValor(valor);
            r.setDtLancamento(cirurgia.getDtCirurgia());
            r.setDtVencimento(datatVencimento);
            r.setIdTerceiro(tEJB.selecionarPorID(valoresTerceiros.getIdTerceiro(), us.retornaUsuario()));
            r.setIdSici(item);
            r.setStatus('A');
            r.setSituacaoPaciente('A');
            r.setSituacaoTerceiro('N');
            r.setTipoPagamento('P');
            r.setIdForma(formaPagamento);
            r.setValorDocumento(valor);
            repassesConsolidados.add(r);
            item.setIdRepasse(r);
            Mensagem.addMensagemPadraoSucesso("salvar");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
        }
    }

    public void lancarRepasseAutomatico(Cheque ch, ServicoItemCirurgiaItem item, Double valor) {
        try {
            Repasse r = new Repasse();
            r.setNumeroParcela(ch.getNumeroParcela());
            r.setIdCheque(ch);
            r.setValor(valor);
            r.setValorDocumento(valor);
            r.setDtLancamento(cirurgia.getDtCirurgia());
            r.setDtVencimento(ch.getDtCompensacaoVencimento());
            r.setIdTerceiro(item.getIdTerceiro());
            r.setIdSici(item);
            r.setStatus('A');
            r.setSituacaoPaciente('A');
            r.setSituacaoTerceiro('N');
            r.setTipoPagamento('P');
            r.setIdForma(formaPagamento);
            System.out.println("gerado repasse de: " + r.getIdCheque().getIdCheque() + " para: " + r.getIdTerceiro().getIdTerceiro());
            item.setIdRepasse(r);
            repassesConsolidados.add(r);
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
        }
    }

    public void selecionarParcela(Parcela pa) {
        parcelaPaga = pa;
    }

    public void ExcluirRepasseIterator(Repasse repasse) {
        for (Iterator<Repasse> rep = repassesConsolidados.iterator(); rep.hasNext();) {
            Repasse repTemp = rep.next();
            if (repasse.getNumeroParcela() == repTemp.getNumeroParcela()
                    && repasse.getIdTerceiro() == repTemp.getIdTerceiro()
                    && repasse.getDtVencimento() == repTemp.getDtVencimento()) {
                rep.remove();
                break;
            }
        }
    }

    public void excluirPagamento(ServicoItemCirurgiaItem item) {
        int indice = 0;
        Double valorAEstornar = 0D;
        valorAEstornar = item.getIdRepasse().getValor();
        try {
            ExcluirRepasseIterator(item.getIdRepasse());
            for (ServicoItemCirurgiaItem sici : listaServicosPorTerceirosTemporaria) {
                if (item == sici) {
                    for (ValoresTerceiros vl : vltAgrupados) {
                        for (ServicoItemCirurgiaItem itemCirurgiaItem : vl.getItensPorTerceiros()) {
                            if (itemCirurgiaItem.getIdServicoItem() == item.getIdServicoItem()) {
                                itemCirurgiaItem.setValorPago(itemCirurgiaItem.getValorPago() - valorAEstornar);
                            }
                        }
                    }
                    listaServicosPorTerceirosTemporaria.remove(indice);
                    break;
                } else {
                    indice++;
                }

            }
            calcularRestanteCheque();
            calcularTotalRepasse();
            calcularRestanteValoresTerceirosTotais();
            Mensagem.addMensagemGrowl(1, "Item removido com sucesso!");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
            System.out.println(e);
        }

    }

    public void mudarValores() {
        if (tipo.equals("N")) {
            valorCheque = valorHonorariosMedicos;
        } else if (tipo.equals("H")) {
            valorCheque = valorHonorariosHospitalares;
        } else {
            valorCheque = valorTotalHonorario;
        }
    }

    public void zeraVariaveisCheque() {
        if (cirurgia.getIdCirurgia() != null) {
            valorCirur = cirurgia.getValor();
        }
        numeroCheque = null;
        pagamento = "";
        numeroParcela = null;
        tipo = "";
        compensado = "";
        conta = null;
        valorCheque = null;
        dtEmissao = null;
        diferenca = true;
        cheque = new Cheque();
        CPF = "";
        emitenteCheque = "";
        movimento = "";
    }

    public void cancelarPagamentosExternos() {
        zeraCheque();
        listaChequeTemporaria = new ArrayList<>();
        estornarPagamentoExterno(cirurgia);
        carregaValoresCheque();
        RequestContext.getCurrentInstance().execute("PF('dlgLancarParcelado').hide()");
    }

    public void estornarPagamentoExterno(Cirurgia c) {
        try {
            for (ContaCorrente cc : c.getContaCorrenteList()) {
                ccEJB.Excluir(cc, null);
                repasseEJB.Excluir(cc.getIdRepasse());
            }
            caixaEJB.excluirCaixaPorFechamento(c.getIdCirurgia());
            chequeEJB.excluirChequesPorFechamento(c.getIdCirurgia(), us.retornaUsuario());

            c.setPago(Boolean.FALSE);
            // Zerar as listas vinculadas.
            c.setRepasseList(null);
            c.setContaCorrenteList(null);
            c.setChequeList(null);

            cirEJB.atualizarFechamento(c);
            Mensagem.addMensagem(1, "Estorno realizado com sucesso");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao estornar.");
        }
    }

    public void zeraCheque() {
        numeroCheque = null;
        pagamento = "";
        numeroParcela = null;
        tipo = "";
        compensado = "";
        conta = null;
        diferenca = true;
        cheque = new Cheque();
        movimento = "C";
        parcelaPaga = new Parcela();
        recibo = Boolean.TRUE;
    }

    public void PagarCirurgiaCheque() throws ParseException {
        try {
            // Somente faça a validação do CPF quando for cheque.
            if (movimento.equals("C") || movimento.equals("N")) {
                if (CPFCPNJ) {
                    ValidarCNPJ vCNPJ = new ValidarCNPJ();
                    validacaoCGC = vCNPJ.isCNPJ(CPF);
                } else {
                    ValidarCPF vCPF = new ValidarCPF();
                    validacaoCGC = vCPF.isCPF(CPF);
                }
            } else {
                validacaoCGC = true;
            }

            if (validacaoCGC) {
                cheque = new Cheque();
                if (validarCamposObrigatoriosCheque()) {
                    if (movimento.equals("C") || movimento.equals("N") || movimento.equals("R")) {

                        if (movimento.equals("C") || movimento.equals("N")) {
                            cheque.setCpf(CPF);
                            cheque.setEmitente(emitenteCheque);
                            if (dtCompensado.equals(new Date())) {
                                cheque.setCompensado("S");
                                cheque.setSituacao("Enviado ao Banco");
                                cheque.setDtCompensado(dtCompensado);
                            } else {
                                cheque.setCompensado("N");
                                cheque.setSituacao("A Compensar");
                            }

                            if (movimento.equals("C")) {
                                if (banco.getIdBanco() != null) {
                                    cheque.setIdBanco(banco);
                                }
                            } else {
                                cheque.setSituacao("A Receber");
                            }
                        } else {
                            cheque.setCompensado("S");
                            cheque.setSituacao("Recebido");
                            cheque.setDtCompensado(dtCompensado);
                        }
                    } else {
                        cheque.setCompensado("S");
                        cheque.setSituacao("Recebido");
                        cheque.setDtCompensado(new Date());
                    }

                    if (cirurgia.getIdResponsavel() != null) {
                        cheque.setIdPaciente(cirurgia.getIdResponsavel());
                    } else {
                        cheque.setIdPaciente(paciente);
                    }

                    cheque.setMovimento(movimento);
                    cheque.setDtCompensacaoVencimento(dtCompensado);
                    cheque.setDtRecebimentoEmissao(dtEmissao);
                    cheque.setNumeroCheque(numeroCheque);
                    cheque.setObservacao(observacao);
                    cheque.setConta(conta);
                    cheque.setValor(valorCheque);

                    //System.out.println("Recibo: " + recibo);
                    cheque.setEmitirRecibo(recibo ? "Sim" : "Não");
                    //cheque.setEmitirRecibo();

                    int qtdParcelas = 0;
                    String inc = "";
                    for (Cheque chq1 : listaCheque) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String date1 = sdf.format(chq1.getDtCompensacaoVencimento());
                        String date2 = sdf.format(cheque.getDtCompensacaoVencimento());
                        if (date1.equals(date2)) {
                            qtdParcelas = chq1.getNumeroParcela();
                            inc = "N";
                            break;
                        } else if (chq1.getNumeroParcela() > qtdParcelas) {
                            qtdParcelas = chq1.getNumeroParcela();
                            inc = "S";
                        }
                    }

                    if ((inc.equals("S")) || (qtdParcelas == 0)) {
                        cheque.setNumeroParcela(qtdParcelas + 1);
                    } else {
                        cheque.setNumeroParcela(qtdParcelas);
                    }
                    cheque.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));

                    // Somente adiciona os pagamentos. Não persiste a informação.
                    listaCheque.add(cheque);
                    listaChequeTemporaria.add(cheque);
                    valorCheque = 0D;
                    valorTotal = 0D;
                    for (Cheque ch : listaChequeTemporaria) {
                        valorTotal = valorTotal + ch.getValor();
                    }
                    calcularValoresPagos();
                    zeraCheque();
                    if (verificarSaldoRestante()) {
                        valorCheque = valorRestante;
                        //dtCompensado = BCRUtils.addMes(dtCompensado, 1);
                    }
                    Mensagem.addMensagem(1, "Pagamento salvo com sucesso.");
                }
            } else {
                validacaoCGC = false;
            }
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }

    }

    public void validarCPF() {
        ValidarCPF vCPF = new ValidarCPF();
        ValidarCNPJ vCNPJ = new ValidarCNPJ();
        validacaoCGC = false;
        if ((!CPF.equals("___.___.___-__")) || (!CPF.equals("___.___.___/____-__"))) {
            if (CPFCPNJ) {
                validacaoCGC = vCNPJ.isCNPJ(CPF);
            } else {
                validacaoCGC = vCPF.isCPF(CPF);
            }
        }
    }

    public boolean verificarSaldo(Cheque chq) {
        Double valorTotal, valorPago, valorRestante;
        valorTotal = 0D;
        valorPago = 0D;
        valorRestante = 0D;
        if (cirurgia != null) {
            for (Repasse rep : repassesConsolidados) {
                if (rep.getIdCheque() == chq) {
                    valorPago = valorPago + rep.getValor();
                }
            }
            if (valorPago > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean verificarSaldoRestante() {
        if (cirurgia != null) {
            valorTotalPago = 0D;
            valorRestante = 0D;
            for (Cheque c : listaCheque) {
                valorTotalPago = valorTotalPago + c.getValor();
            }
            if (valorTotalPago == null) {
                valorTotalPago = 0D;
            }
            if (valorTotalPago.equals(0D)) {
                valorRestante = cirurgia.getValor();
            } else {
                valorRestante = cirurgia.getValor() - valorTotalPago;
            }
            if (valorRestante > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void Receber() {
        Usuario u = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
        try {
            // Somente um numero de lote para toda a cirurgia
            Date d = new Date();
            Long numAleatorio = d.getTime();
            String forma, terceiro = "";
            for (Repasse rr : repassesConsolidados) {

                // CHARLES 20112019
                // Cartão de crédito e débito não geram repasse e nem conta_corrente.
                if (rr.getIdCheque() != null && !rr.getIdCheque().getMovimento().equals("R") && !rr.getIdCheque().getMovimento().equals("A")) {
                    //System.out.println("gerando repasse referente: id cheque: " + rr.getIdCheque().getIdCheque() + " movimento: " + rr.getIdCheque().getMovimento());
                    if (rr.getIdCheque().getMovimento().equals("C")) {
                        rr.setFormaPagamento("Cheque");
                        rr.setSituacaoTerceiro('N');
                    } else if (rr.getIdCheque().getMovimento().equals("N")) {
                        rr.setFormaPagamento("Nota Promissória");
                        rr.setSituacaoTerceiro('N');
                    } else {
                        rr.setFormaPagamento("Dinheiro");
                        rr.setSituacaoTerceiro('L');
                    }

                    rr.setDtRecebimento(rr.getIdCheque().getDtRecebimentoEmissao());
                    rr.setObservacao(rr.getIdCheque().getObservacao());
                    rr.setPercentualRecebido(100D);
                    rr.setValorRecebido(rr.getValorDocumento());
                    rr.setSituacaoPaciente('P');
                    rr.setStatus('F');
                    rr.setValor(0D);
                    rr.setTipoPagamento('P');
                    List<Repasse> r2 = repasseEJB.pesquisaRepassePorCheque(rr.getIdCheque().getIdCheque());

                    for (Repasse r : r2) {
                        // CHARLES 20112019
                        // Cartão de crédito e débito não geram repasse e nem conta_corrente.
                        if (!r.getIdCheque().getMovimento().equals("R") && !r.getIdCheque().getMovimento().equals("A")) {
                            System.out.println("gerando repasse referente: id cheque: " + rr.getIdCheque().getIdCheque() + " movimento: " + rr.getIdCheque().getMovimento());
                            r.setDtRecebimento(rr.getDtRecebimento());
                            r.setObservacao(rr.getObservacao());
                            r.setDesconto(rr.getDesconto());
                            r.setAcrescimo(rr.getAcrescimo());
                            r.setJuros(rr.getJuros());
                            r.setSituacaoPaciente(rr.getSituacaoPaciente());
                            r.setStatus(rr.getStatus());
                            r.setSituacaoTerceiro(rr.getSituacaoTerceiro());
                            r.setDtRecebimento(rr.getDtRecebimento());
                            r.setTipoPagamento(rr.getTipoPagamento());
                            r.setPercentualRecebido(rr.getPercentualRecebido());
                            r.setFormaPagamento(rr.getFormaPagamento());
                            if (rr.getPercentualRecebido() == 100) {
                                r.setValorRecebido(r.getValorDocumento());
                                r.setValor(0D);
                                r.setSituacaoPaciente('P');
                                repasseEJB.Salvar(r);
                            }
                        }
                    }
                }
            }

            // Lançar pagamentos no Caixa
            for (Cheque ch : listaCheque) {

                // CHARLES 20112019
                // Cartão de crédito e débito não geram lançamentos de caixa.
                if (!ch.getMovimento().equals("R") && !ch.getMovimento().equals("A")) {
                    System.out.println("gerando caixa do pagamento: " + ch.getIdCheque());
                    caixa.setNumLote(numAleatorio.toString());
                    caixa.setIdCheque(ch);
                    caixa.setIdUsuario(u);
                    caixa.setDtCaixa(ch.getDtRecebimentoEmissao());
                    caixa.setTipoLancamento('C');
                    caixa.setValor(ch.getValor());
                    caixa.setStatus("Não Baixado");
                    if (cirurgia.getIdTerceiro().getTipoPessoa() == 'J') {
                        terceiro = cirurgia.getIdTerceiro().getNomeFantasia();
                    } else {
                        terceiro = cirurgia.getIdTerceiro().getNome();
                    }
                    caixa.setHistorico("Fechamento nº " + cirurgia.getIdCirurgia() + ". " + cirurgia.getIdProcedimento().getDescricao() + " ." + " Méd. Resp.:" + terceiro + ". Paciente: " + cirurgia.getIdPaciente().getNome());
                    caixa.setIdCirurgia(cirurgia);
                    caixa.setNumeroParcela(ch.getNumeroParcela());
                    caixaEJB.LancarRecebimentoNoCaixa(caixa);
                }
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algum erro ao tentar receber!");
            System.out.println(e);
        }

    }

    public void excluirCheque() {
        int indice = 0;
        List<Cheque> listaTemp = listaCheque;
        for (Cheque ch : listaTemp) {
            if (ch == cheque) {
                listaCheque.remove(indice);
                Mensagem.addMensagemGrowl(1, "Pagamento excluído com sucesso!");
                break;
            } else {
                indice++;
            }
        }
        calcularValoresPagos();
    }

    public void excluirCheque2(Cheque c) {
        int indice = 0;
        List<Cheque> listaTemp = new ArrayList<Cheque>();
        listaTemp = listaChequeTemporaria;
        for (Cheque ch : listaTemp) {
            if (ch == c) {
                listaChequeTemporaria.remove(indice);
                listaCheque.remove(indice);
//                chequeEJB.Excluir(ch, us.retornaUsuario());
                Mensagem.addMensagemGrowl(1, "Item Removido com sucesso!");
                break;
            } else {
                indice++;
            }
        }
        calcularValoresPagos();
    }
    List<Parcela> ps = new ArrayList<Parcela>();
    ArrayList<Parcela> psAgrupado = new ArrayList<Parcela>();
    List<ValoresTerceiros> vlt = new ArrayList<ValoresTerceiros>();
    List<ValoresTerceiros> vltAgrupados = new ArrayList<ValoresTerceiros>();

    public List<ValoresTerceiros> getVlt() {
        return vlt;
    }

    public void setVlt(List<ValoresTerceiros> vlt) {
        this.vlt = vlt;
    }

    public List<ValoresTerceiros> getVltAgrupados() {
        return vltAgrupados;
    }

    public void setVltAgrupados(List<ValoresTerceiros> vltAgrupados) {
        this.vltAgrupados = vltAgrupados;
    }

    public ArrayList<Parcela> getPsAgrupado() {
        return psAgrupado;
    }

    public void setPsAgrupado(ArrayList<Parcela> psAgrupado) {
        this.psAgrupado = psAgrupado;
    }

    public Double retornaValorOrcadoPorServico(ServicoItemCirurgiaItem sici) {
        Double valorBaseCi = sEJB.retornaValorBaseServico(sici.getIdCirurgiaItem());
        Double valorBaseOi = 0D;
        for (OrcamentoItem oi : sici.getIdCirurgia().getIdOrcamento().getOrcamentoItemList()) {
            if (oi.getIdServico().getIdServico().equals(sici.getIdServicoItem().getIdServico().getIdServico())) {
                valorBaseOi = sEJB.retornaValorBaseServicoPorOrcamentoItem(oi);
            }
        }
        return 0D;

    }

    public void gerarParcelas() {
        repasses = new ArrayList<Repasse>();
        ps = new ArrayList<Parcela>();
        Parcela p = new Parcela();

        if (formaPagamento.getTipo() == 'P') {
            int numParcelas = formaPagamento.getNumMeses();
            for (int i = 0; i < numParcelas; i++) {
                Double valorRestante = 0D;
                int repasseAtual = 0;
                int QtdRepasse = 0;
                QtdRepasse = listadeItensDosItens.size();
                for (ServicoItemCirurgiaItem sici : listadeItensDosItens) {
                    repasseAtual++;
                    Double valor = 0D;
                    Repasse r = new Repasse();
                    valor = sici.getValorTaxado() / numParcelas;
                    valorRestante = valorRestante + (valor - (sici.getValorTaxado() / numParcelas));
                    r.setNumeroParcela(i + 1);
                    if (repasseAtual == QtdRepasse) {
                        valor = valor - valorRestante;
                    }
                    r.setValor(valor);
                    r.setDtLancamento(cirurgia.getDtCirurgia());
                    r.setDtVencimento(addMes(cirurgia.getDtCirurgia(), i));
                    r.setIdTerceiro(sici.getIdTerceiro());
                    r.setIdSici(sici);
                    r.setStatus('A');
                    r.setSituacaoPaciente('A');
                    r.setSituacaoTerceiro('N');
                    r.setTipoPagamento('P');
                    r.setIdForma(formaPagamento);
                    r.setValorDocumento(valor);
                    repasses.add(r);
                }
                p.setNumParcela(i);
                p.setValorParcela(cirurgia.getValor() / formaPagamento.getNumMeses());
                ps.add(p);
                p = new Parcela();
            }
        } else {
            int numParcelas = formaPagamento.getNumMeses();
            for (int i = 0; i < numParcelas; i++) {
                Double valorRestante = 0D;
                int repasseAtual = 0;
                int QtdRepasse = 0;
                QtdRepasse = listadeItensDosItens.size();
                for (ServicoItemCirurgiaItem sici : listadeItensDosItens) {
                    Double valor = 0D;
                    Repasse r = new Repasse();
                    valor = sici.getValorTaxado() / numParcelas;
                    r.setNumeroParcela(i + 1);
                    if (repasseAtual == QtdRepasse) {
                        valor = valor - valorRestante;
                    }
                    r.setValor(valor);
                    valorRestante = valorRestante + (valor - (sici.getValorTaxado() / numParcelas));
                    r.setValorDocumento(valor);
                    r.setDtLancamento(cirurgia.getDtCirurgia());
                    r.setDtVencimento(new Date());
                    r.setIdTerceiro(sici.getIdTerceiro());
                    r.setIdSici(sici);
                    r.setStatus('A');
                    r.setSituacaoPaciente('A');
                    r.setSituacaoTerceiro('N');
                    r.setTipoPagamento('P');
                    r.setAcrescimo(0D);
                    r.setDesconto(0D);
//                    r.setDtRepasse(cirurgia.getDtCirurgia());
                    repasses.add(r);
                }
                p.setNumParcela(i);
                p.setValorParcela(cirurgia.getValor() / formaPagamento.getNumMeses());
                ps.add(p);
                p = new Parcela();
            }
        }
    }

    public String formataMoeda(Double valor) {
        return NumberFormat.getCurrencyInstance().format(valor);
    }

    public void processaTaxa(ServicoItemCirurgiaItem item, Integer index) {
        Double valor = item.getValor();
        Double taxa = item.getTaxa();
        Double taxaMaxima = 100D;
        Double resultado;
        resultado = (valor * taxa) / taxaMaxima;
        item.setValorTaxado(resultado);
        itensDosItens.set(index, item);
        calcularValores();
    }

    public void calcularValorDesconto() {
        if (cirurgia.getDescontoTotal() != null) {
            if (valorDesconto != null) {
                if (!valorDesconto.equals(cirurgia.getDescontoTotal())) {
                    //Double novoValorCirurgia = cirurgia.getValor() - ((cirurgia.getValor() * valorDesconto) / 100);
                    Double antigoValorCirurgia = (cirurgia.getValor() * 100) / (100 - cirurgia.getDescontoTotal());
                    Double novoValorCirurgia = antigoValorCirurgia - ((antigoValorCirurgia * valorDesconto) / 100);
                    cirurgia.setValor(novoValorCirurgia);
                }
            }
        } else {
            Double novoValorCirurgia = cirurgia.getValor() - ((cirurgia.getValor() * valorDesconto) / 100);
            cirurgia.setValor(novoValorCirurgia);
        }
        calcularValoresItensComDesconto();
        cirurgia.setDescontoTotal(valorDesconto);
        Mensagem.addMensagem(1, "Desconto calculado com sucesso.");
    }

    public void calcularValoresItensComDesconto() {
        for (CirurgiaItem ci : cirurgiaItens) {
            Double novoValorItemUnitario;
            if (cirurgia.getDescontoTotal() == null) {
                novoValorItemUnitario = ci.getValorUnitario() - ((ci.getValorUnitario() * valorDesconto) / 100);
            } else {
                Double antigoValorCirurgia = (ci.getValorUnitario() * 100) / (100 - cirurgia.getDescontoTotal());
                novoValorItemUnitario = antigoValorCirurgia - ((antigoValorCirurgia * valorDesconto) / 100);
            }
            ci.setValorUnitario(novoValorItemUnitario);
            calcularTotalItem(ci);
        }
    }

    public void calcularDescontoPercentual(ServicoItemCirurgiaItem item, Integer index) {
        Double valor = item.getValor();
        Double descontoPercent = item.getTaxa();
        Double descontoValor = item.getTaxaValor();
        Double resultado;

        if (descontoPercent == null || descontoPercent == 0) {
            resultado = valor;
        } else {
            resultado = valor - ((valor * descontoPercent) / 100);
        }
        descontoValor = valor - resultado;
        item.setValorTaxado(resultado);
        item.setTaxaValor(descontoValor);
        itensDosItens.set(index, item);
        calcularValores();
    }

    public void calcularDescontoDinheiro(ServicoItemCirurgiaItem item, Integer index) {
        Double valor = item.getValor();
        Double descontoPercent = item.getTaxa();
        Double descontoValor = item.getTaxaValor();
        Double resultado;
        resultado = ((descontoValor * 100) / valor);
        item.setTaxa(arredondar_double(resultado));
        item.setValorTaxado(valor - descontoValor);
        itensDosItens.set(index, item);
        calcularValores();
    }

    public void processaTaxaInvertida(ServicoItemCirurgiaItem item, Integer index) {
        Double valor = item.getValor();
        Double ValorTaxado = 0D;
        if (item.getValorTaxado() != null) {
            ValorTaxado = item.getValorTaxado();
        }
        Double taxaPercentual = null;
        taxaPercentual = (ValorTaxado * 100) / valor;
        item.setTaxa(taxaPercentual);
        itensDosItens.set(index, item);
        calcularValores();
    }

    //defasado
    public void processarTaxa(ServicoItemCirurgiaItem item) {
        int index = 0;
        for (ServicoItemCirurgiaItem ii : itensDosItens) {
            if (item == ii) {
                Double valor = item.getValor();
                Double taxa = item.getTaxa();
                Double taxaMaxima = 100D;
                Double resultado;
                if (taxa != 0) {
                    resultado = (valor * taxa) / taxaMaxima;
                } else {
                    resultado = 100D;
                }

                item.setValorTaxado(resultado);
                itensDosItens.set(index, item);
                break;
            }
            index++;
        }
    }

    //defasado
    public void processarTaxaIterator(ServicoItemCirurgiaItem item) {
        int index = 0;
        Iterator<ServicoItemCirurgiaItem> itensDosItensTemp = itensDosItens.iterator();
        while (itensDosItensTemp.hasNext()) {
            ServicoItemCirurgiaItem obj = itensDosItensTemp.next();
            if (item == obj) {
                Double valor = item.getValor();
                Double taxa = item.getTaxa();
                Double taxaMaxima = 100D;
                Double resultado;
                if (taxa != null) {
                    resultado = (valor * taxa) / taxaMaxima;
                    obj.setValorTaxado(resultado);
                }
                itensDosItens.set(index, obj);
                break;
            } else {
                index++;
            }
        }
        calcularValores();
    }
//defasado

    public void processarTaxaInvertida(ServicoItemCirurgiaItem item) {
        int index = 0;
        for (ServicoItemCirurgiaItem ii : itensDosItens) {
            if (item == ii) {
                Double valor = item.getValor();
                Double ValorTaxado = item.getValorTaxado();
                Double taxaPercentual;
                if (ValorTaxado != 0) {
                    taxaPercentual = (ValorTaxado * 100) / valor;
                } else {
                    taxaPercentual = 100D;
                }
                item.setTaxa(taxaPercentual);
                itensDosItens.set(index, item);
                break;
            } else {
                index++;
            }
        }
    }
//defasado

    public void processarTaxaInvertidaInterator(ServicoItemCirurgiaItem item) {
        int index = 0;
        Iterator<ServicoItemCirurgiaItem> itensDosItensTemp = itensDosItens.iterator();
        while (itensDosItensTemp.hasNext()) {
            ServicoItemCirurgiaItem obj = itensDosItensTemp.next();
            if (item == obj) {
                Double valor = item.getValor();
                Double ValorTaxado = item.getValorTaxado();
                Double taxaPercentual;
                if (ValorTaxado != null) {
                    taxaPercentual = (ValorTaxado * 100) / valor;
                } else {
                    taxaPercentual = 0D;
                }
                item.setTaxa(taxaPercentual);
                itensDosItens.set(index, item);
                break;
            } else {
                index++;
            }
        }
        calcularValores();

    }
    private Double valorFinal;
    private Double valorFinalMedico;
    private Double valorFinalHospital;
    private Double valorCirur;
    private Double valorCirurMedico;
    private Double valorCirurHospital;
    private Double valorDif;
    private Double valorDifMedico;
    private Double valorDifHospital;
    private Double percentualTotalPagamento;

    public Double getValorFinalMedico() {
        return valorFinalMedico;
    }

    public void setValorFinalMedico(Double valorFinalMedico) {
        this.valorFinalMedico = valorFinalMedico;
    }

    public Double getValorFinalHospital() {
        return valorFinalHospital;
    }

    public void setValorFinalHospital(Double valorFinalHospital) {
        this.valorFinalHospital = valorFinalHospital;
    }

    public Double getValorCirurMedico() {
        return valorCirurMedico;
    }

    public void setValorCirurMedico(Double valorCirurMedico) {
        this.valorCirurMedico = valorCirurMedico;
    }

    public Double getValorCirurHospital() {
        return valorCirurHospital;
    }

    public void setValorCirurHospital(Double valorCirurHospital) {
        this.valorCirurHospital = valorCirurHospital;
    }

    public Double getValorDifMedico() {
        return valorDifMedico;
    }

    public void setValorDifMedico(Double valorDifMedico) {
        this.valorDifMedico = valorDifMedico;
    }

    public Double getValorDifDesconto() {
        return valorDifHospital;
    }

    public void setValorDifDesconto(Double valorDifDesconto) {
        this.valorDifHospital = valorDifDesconto;
    }

    public Double totalItens() {
        Double total = 0D;
        Iterator<CirurgiaItem> ccIterator = cirurgiaItens.iterator();
        while (ccIterator.hasNext()) {
            CirurgiaItem obj = ccIterator.next();
            total = obj.getValorTotal() + total;
        }
        return total;
    }

    public void valores() {
        Double valorTotal = 0D;
        valorTotal = totalItens();
        //System.out.println("Valor total dos itens: " + valorTotal);
        //System.out.println("Valor total do fechamento: " + valorFinal);
    }

    public Double totalItensHospital() {
        Double total = 0D;
        Iterator<CirurgiaItem> ccIterator = cirurgiaItens.iterator();
        while (ccIterator.hasNext()) {
            CirurgiaItem obj = ccIterator.next();
            if (!"N".equals(obj.getIdServico().getTipo())) {
                total = obj.getValorTotal() + total;

            }
        }
        return total;
    }

    public Double totalItensMedico() {
        Double total = 0D;
        Iterator<CirurgiaItem> ccIterator = cirurgiaItens.iterator();
        while (ccIterator.hasNext()) {
            CirurgiaItem obj = ccIterator.next();
            if ("N".equals(obj.getIdServico().getTipo())) {
                total = obj.getValorTotal() + total;
            }
        }
        return total;
    }

    public void calcularValores() {
        valorFinal = 0D;
        valorFinalHospital = 0D;
        valorFinalMedico = 0D;
        valorCirur = 0D;
        valorCirurHospital = 0D;
        valorCirurMedico = 0D;
        valorDif = 0D;
        valorDifMedico = 0D;
        valorDifHospital = 0D;
        for (ServicoItemCirurgiaItem ii : itensDosItens) {
            valorCirur = ii.getValor() + valorCirur;
            valorFinal = ii.getValorTaxado() + valorFinal;
            if ("N".equals(ii.getIdServicoItem().getIdServico().getTipo())) {
                valorCirurMedico = ii.getValor() + valorCirurMedico;
                valorFinalMedico = ii.getValorTaxado() + valorFinalMedico;

            }
            if ("H".equals(ii.getIdServicoItem().getIdServico().getTipo())) {
                valorCirurHospital = ii.getValor() + valorCirurHospital;
                valorFinalHospital = ii.getValorTaxado() + valorFinalHospital;

            }
        }
        valorDif = valorCirur - valorFinal;
        valorDifHospital = valorCirurHospital - valorFinalHospital;
        valorDifMedico = valorCirurMedico - valorFinalMedico;
        cirurgia.setValor(this.valorFinal);
    }

    public Double getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(Double valorFinal) {
        this.valorFinal = valorFinal;
    }

    public Double getValorCirur() {
        return valorCirur;
    }

    public void setValorCirur(Double valorCirur) {
        this.valorCirur = valorCirur;
    }

    public Double getValorDif() {
        return valorDif;
    }

    public void setValorDif(Double valorDif) {
        this.valorDif = valorDif;
    }

    public void ExcluirCirurgia() {
        try {
            if (cirurgia.getIdOrcamento() != null) {
                Orcamento oupdate = new Orcamento();
                oupdate = cirurgia.getIdOrcamento();
                oupdate.setSituacao("Lançado");
                oupdate.setDtExecucao(null);
                oupdate.setIdStatus(stEJB.selecionarPorTipo(1));
                oEJB.atualizarOrcamento(oupdate);
            }
            cirEJB.Excluir(cirurgia);
            Mensagem.addMensagemPadraoSucesso("excluir");
            cirurgia = new Cirurgia();
            BCRUtils.ResetarDatatableFiltros("frmConsultar:tblPesq");
            listaCirurgiaAPesquisar = cirEJB.pesquisarCirurgias(getDtInicialPesquisa(), getDtFinalPesquisa(), getIdPacientePesquisa(), getNumeroCirurgia(), getIdProcedimentoPesquisa(), getIdTerceiroPesquisa(), tipoInternacao.getIdTipoInternacao(), exceto);
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }

    public int pegarNumeroDeParcelas() {

        int parcelaCorrente = 1;
        for (Repasse r : repasses) {
            if (!r.getNumeroParcela().equals(parcelaCorrente)) {
                parcelaCorrente++;
            }
        }
        return parcelaCorrente;
    }

    public void ExcluirRepasse(Repasse repasse) {
        pegarNumeroDeParcelas();
        try {
            int indice = 0;
            for (Repasse r : repasses) {
                if (repasse.getNumeroParcela() == r.getNumeroParcela()
                        && repasse.getIdTerceiro() == r.getIdTerceiro()
                        && repasse.getDtVencimento() == r.getDtVencimento()) {
                    if (r.getIdRepasse() != null) {
                        cirEJB.excluirRepasse(r);
                        repasses.remove(r);
                    } else {
                        repasses.remove(r);
                    }
                    break;
                } else {
                    indice++;
                }
            }
            calcularTotalRepasse();
        } catch (Exception e) {
        }
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public void deletarItem(CirurgiaItem oi) {

        if (oi.getIdCirurgiaItem() != null) {
            Double valorParaRepassar = oi.getValorTotal();
            int indice = 0;
            List<CirurgiaItem> listaTemporaria = new ArrayList<CirurgiaItem>();
            listaTemporaria = cirurgiaItens;
            for (CirurgiaItem ci : listaTemporaria) {
                if (ci == oi) {
                    cirurgiaItens.remove(indice);
                    cirEJB.ExcluirItem(oi);
//                    for (ServicoItemCirurgiaItem sii : oi.getServicoItemCirurgiaItemList()) {
//                        itensDosItens.remove(sii);
//                    }
                    break;

                } else {
                    indice++;
                }
            }
            Double valor = 0D;
            for (CirurgiaItem oo : cirurgiaItens) {
                valor = valor + oo.getValorTotal();
            }
            cirurgia.setValor(valor);
            calcularValorInternacao();
            calcularTotalItensGeral();
            Mensagem.addMensagemGrowl(1, "Item removido com sucesso!");
        } else {
            int indice = 0;
            List<CirurgiaItem> listaTemporaria = new ArrayList<CirurgiaItem>();
            listaTemporaria = cirurgiaItens;
            for (CirurgiaItem ci : listaTemporaria) {
                if (ci == oi) {
                    cirurgiaItens.remove(indice);
                    break;
                } else {
                    indice++;
                }
            }
            Double valor = 0D;
            valorInternacao = 0D;
            for (CirurgiaItem oo : cirurgiaItens) {
                valor = valor + oo.getValorTotal();
                valorInternacao = valorInternacao + cirurgiaItem.getValorTotal();
            }

            cirurgia.setValor(valor);
            calcularValorInternacao();
            calcularTotalItensGeral();
            zerarListas();
            Mensagem.addMensagemGrowl(1, "Item Removido com sucesso!");

        }
    }
    Double valorInternacao = 0D;

    public void calcularValorInternacao() {
        valorInternacao = 0D;
        for (CirurgiaItem ci : cirurgiaItens) {
            valorInternacao = valorInternacao + ci.getValorTotal();
        }
        //System.out.println(valorInternacao);
    }

    public void setarDatas() {
        if ((movimento.equals("D")) || movimento.equals("R") || (movimento.equals("A"))) {
            dtCompensado = new Date();
        }
    }

    public void mesclarContaPacienteOrcamento(Orcamento orc, ContaPaciente cp) {
        try {
            cirurgia.setIdContaPaciente(cp);
            gerarFechamentoPorOrcamento(orc);
            cirurgiaItens.addAll(cpEJB.gerarListaDeServicosPorContaPaciente(cp));
            calcularValorInternacao();
            cirurgia.setDtInternacao(cp.getIdAtendimentoPaciente().getDtEntrada());
            Mensagem.addMensagemGrowl(1, "Fechamento criado com sucesso!");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagemGrowl(3, "Erro ao gerar fechamento.");
        }

    }

    public void selecionarOrcamento(Orcamento o) {
        try {
            novo();
            orcamento = o;
            // Se o orçamento estiver vinculado
            if (o.getIdAtendimentoPaciente() != null) {
                pesquisarContasPacientePorAtendimento(o.getIdAtendimentoPaciente().getIdAtendimentoPaciente());
                RequestContext.getCurrentInstance().execute("PF('dlgConsultarContasPacientePorAtendimento').show()");
            } else {
                // Se o orçamento não estiver vinculado a um atendimento
                gerarFechamentoPorOrcamento(orcamento);
                try {
                    calcularValorInternacao();
                    Mensagem.addMensagemGrowl(1, "Fechamento criado com sucesso!");
                } catch (Exception e) {
                    Mensagem.addMensagemGrowl(3, "Erro. Contate o suporte.");
                }

            }
        } catch (Exception e) {
            Mensagem.addMensagemGrowl(3, "Este numero de orçamento não foi encontrado ou já está em uso!");
        }
    }

    private void gerarFechamentoPorOrcamento(Orcamento orc) {
        valorTotalItensHospital = 0D;

        cirurgia.setIdOrcamento(orc);
        cirurgia.setIdPaciente(orc.getIdPaciente());
        cirurgia.setIdProcedimento(orc.getIdProcedimento());
        if (orc.getIdResponsavel() != null) {
            cirurgia.setResponsavel("S");
            cirurgia.setIdResponsavel(orc.getIdResponsavel());
        }
        if (orc.getIdConvenio() != null) {
            convenio = orc.getIdConvenio();
        }
        if (orc.getIdPlano() != null) {
            plano = orc.getIdPlano();
        }

        formaPagamento = orc.getIdForma();
        cirurgia.setIdForma(orc.getIdForma());
        cirurgia.setDtInternacao(new Date());
        cirurgia.setIdTerceiro(orc.getIdTerceiro());
        cirurgia.setObservacao(orc.getObservacao());
        unidadeAtendimento = orc.getIdUnidade();
        cirurgia.setValor(orc.getValor());

        for (OrcamentoItem oi : orc.getOrcamentoItemList()) {
            CirurgiaItem ci = new CirurgiaItem();
            if (oi.getIdServico().getTipo().equals("H")) {
                Double valorBase = oi.getValorTotal() / oi.getIdServico().getEscala();
                valorTotalItensHospital = valorTotalItensHospital + valorBase;
            } else {
                ci.setOrigem("SMA");
                ci.setIdOrcamentoItem(oi);
                ci.setIdServico(oi.getIdServico());
                ci.setObservacao(oi.getObservacao());
                ci.setQuantidade(oi.getQuantidade());
                ci.setValorUnitario(oi.getValorUnitario());
                ci.setValorTotal(arredondar_double(oi.getValorTotal()));
                cirurgiaItens.add(ci);
            }
        }

        // Gerar item único com valor do hospital
        CirurgiaItem ci = new CirurgiaItem();
        ci.setIdServico(retornaServicoPadrao());
        ci.setIdTerceiro(tEJB.selecionarTerceiroCNPJCPF("18.170.894/0001-23", "Hospital"));
        ci.setQuantidade(1D);
        ci.setValorUnitario(valorTotalItensHospital);
        ci.setValorTotal(arredondar_double(valorTotalItensHospital));
        cirurgiaItens.add(ci);
    }

    public void selecionarContaPaciente(ContaPaciente cp) {
        try {
            novo();
            contaPaciente = cp;
            cirurgia.setIdContaPaciente(cp);
            cirurgia.setIdPaciente(cp.getIdAtendimentoPaciente().getIdPaciente());
            convenio = cEJB.selecionarConvenioPadrao();
            //plano = plaEJB.selecionarPlanoPorNome("Particular");

            formaPagamento = fpEJB.selecionarPadrao();
            cirurgia.setIdForma(formaPagamento);
            cirurgia.setIdProcedimento(proEJB.selecionarPorIDServico(cp.getIdAtendimentoPaciente().getIdServico().getIdServico(), null));
            cirurgia.setIdTerceiro(cp.getCdMedicoExecutor() == null ? cp.getIdAtendimentoPaciente().getIdMedicoResp() : cp.getCdMedicoExecutor());
            cirurgia.setObservacao(cp.getIdAtendimentoPaciente().getObservacao());
            //unidadeAtendimento = orcamento.getIdUnidade();
            //cirurgia.setIdOrcamento(orcamento);

            cirurgia.setValor(cpEJB.retornaTotalRealContaPaciente(cp.getProcedimentoPacienteList()));
            cirurgia.setDtInternacao(cp.getIdAtendimentoPaciente().getDtEntrada());

            System.out.println("Data CC: " + cp.getIdAtendimentoPaciente().getDtEntrada());

            cirurgiaItens.addAll(cpEJB.gerarListaDeServicosPorContaPaciente(cp));
            try {
                calcularValorInternacao();
                Mensagem.addMensagemGrowl(1, "Fechamento criado com sucesso!");
            } catch (Exception e) {
                Mensagem.addMensagemGrowl(3, "Erro ao calcular.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public Double retornaValorAjustadoPorRegraRepasse(ProcedimentoPaciente pp) {
        return cpEJB.retornaValorAjustadoPorRegraRepasse(pp);
    }

    public void atualizarAtendimento(Cirurgia c) {
        try {
            // CHARLES - 23022018
            // Para evitar possíveis erros de lançamento, duplicados, cancelados, etc.
            ppEJB.deletarProcedimentoPacientePorContaPaciente(c.getIdContaPaciente().getIdContaPaciente());
            for (CirurgiaItem ci : c.getCirurgiaItemList()) {
                cirEJB.ExcluirItem(ci);
            }

            // Renova os lançamentos, de acordo com o TASY.
            syncEJB.sincronizarAtendimentoPacientePorID(c.getIdContaPaciente().getIdAtendimentoPaciente().getNumeroAtendimentoPacienteTasy());
            Mensagem.addMensagem(1, "Dados atualizados.");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Não foi possível atualizar. Erro " + e.getMessage());
        }
    }

    public void Salvar() {
//        if (verificaValoresRepasseComOrcamento()) {
        cirurgia.setIdUsuario(uEJB.retornaUsuarioDaSessao());
        if (validarCamposObrigatorios()) {
            if (cirurgia.getIdCirurgia() == null) {
                try {
                    servicoItemCirurgiaItem = new ServicoItemCirurgiaItem();
                    if (convenio.getIdConvenio() != null) {
                        cirurgia.setIdConvenio(convenio);
                    }
                    if (orcamento.getIdOrcamento() != null) {
                        cirurgia.setIdOrcamento(orcamento);
                    }

                    if (plano.getIdPlano() != null) {
                        cirurgia.setIdPlano(plano);
                    }

                    if (responsavel.getIdPaciente() != null) {
                        cirurgia.setIdResponsavel(responsavel);
                    }

                    if (tipoInternacao.getIdTipoInternacao() != null) {
                        cirurgia.setIdTipoInternacao(tipoInternacao);
                    } else {
                        tipoInternacao = cirEJB.retornaTipoInternacao("Internacao");
                        cirurgia.setIdTipoInternacao(tipoInternacao);
                    }
                    Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
                    if (params != null) {
                        String finalizarr = params.get("finalizar");
                        finalizarParciais = finalizarr.equals("Sim");
                    }

                    // Verifica se pode finalizar o orçamento para não permitir ser utilizado novamente.
                    if (finalizarParciais) {
                        // Muda o status do Orçamento para não ser mais utilizado
                        if (orcamento.getIdOrcamento() != null) {
                            orcamento.setIdStatus(stEJB.selecionarPorTipo(4));
                            orcamento.setDtExecucao(cirurgia.getDtCirurgia());
                            oEJB.atualizarOrcamento(orcamento);
                        }

                        // Seta data de finalização dos fechamentos.
                        cirurgia.setDtFinalizacao(new Date());
                    } else {
                        // Muda o status do Orçamento para continuar sendo utilizado
                        if (orcamento.getIdOrcamento() != null) {
                            orcamento.setIdStatus(stEJB.selecionarPorTipo(6));
                            orcamento.setDtExecucao(null);
                            oEJB.atualizarOrcamento(orcamento);
                        }

                        // Seta nula a data de finalização dos fechamentos.
                        cirurgia.setDtFinalizacao(null);
                    }

                    String action = "N";
                    // Se retorna true, pode liberar.
                    if (retornarSePodeLiberarRepasseFechamento()) {
                        action = "S";
                    }

                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                    String dataFormatada = df.format(cirurgia.getDtCirurgia());

                    if (action.equals("S")) {
                        cirurgia.setStatus('L');
                        if (orcamento.getIdOrcamento() != null) {
                            if (!orcamento.getAdiantamentosHistoricoList().isEmpty() && finalizarParciais) {
                                for (AdiantamentosHistorico ah : orcamento.getAdiantamentosHistoricoList()) {
                                    ah.setStatus("D");
                                    if (!ah.getTipo().equals("S")) {
                                        ah.setObservacao(ah.getObservacao() + ". Adiantamento pago em: " + dataFormatada + " - " + us.retornaUsuario());
                                    }
                                    ahEJB.atualizarAdiantamento(ah, orcamento.getIdOrcamento(), ah.getValor(), ah.getObservacao());
                                }
                            }
                        }

                    } else if (action.equals("N")) {
                        cirurgia.setStatus('N');
                    }

                    // Salvar os pagamentos lançados
                    for (Cheque ch : listaCheque) {
                        chequeEJB.Salvar(ch, us.retornaUsuario());
                    }

//                    for (Cheque ch : listaCheque) {
//                        for (Repasse r : repassesConsolidados) {
//                            if (ch.getIdCheque().equals(r.getIdCheque().getIdCheque())) {
//                                r.setIdCheque(ch);
//                            }
//                        }
//                    }

                    cirEJB.Salvar(cirurgia, cirurgiaItens, itensDosItens, repassesConsolidados);

                    for (Cheque ch : listaCheque) {
                        if (!repassesConsolidados.isEmpty()) {
                            calcularValoresChequePorHonorarios(ch);
                            if (qdeHonorariosHospitalares > 0 && qdeHonorariosMedicos > 0) {
                                // CHARLES 06072017
                                // Se o parametro abaixo for verdadeiro, seta o tipo do CHEQUE (movimento = C) para não misto (tipo = N) para ser transferido para o caixa honorarios no momento da transfêrencia
                                if (assEJB.carregarAssociacao().getTransferirChequesCaixaHonorarios() == null) {
                                    ch.setTipo("M");
                                } else if (assEJB.carregarAssociacao().getTransferirChequesCaixaHonorarios() && ch.getMovimento().equals("C")) {
                                    ch.setTipo("N");
                                } else {
                                    ch.setTipo("M");
                                }
                            } else if (qdeHonorariosHospitalares > 0) {
                                ch.setTipo("H");
                            } else if (qdeHonorariosMedicos > 0) {
                                ch.setTipo("N");
                            } else {
                                ch.setTipo(null);
                            }
                        }
                        ch.setIdCirurgia(cirurgia);
                        chequeEJB.Salvar(ch, us.retornaUsuario());
                    }

                    if (cirurgiaItens.isEmpty()) {
                        RequestContext.getCurrentInstance().execute("alertify.error('Atenção! Você salvou o fechamento sem nenhum item adicionado!')");
                    }

                    if (action.equals("S")) {
                        // Se for dinheiro, deve cair no caixa no dia da movimentação
                        Receber();
                        aEJB.Salvar(BCRUtils.criarAuditoria("liberarfechamento", cirurgia.getIdCirurgia().toString(), us.retornaUsuario(), new Date(), "Fechamento"));
                    }
                    Mensagem.addMensagemGrowl(1, "Fechamento salvo com sucesso.");
                    if (cirurgia.getIdUsuario().getIdPapel().getPermitirSomenteLancarPagamentosAtendimentoInterno()) {
                        mudaStatusConsultando();
                    }

                } catch (Exception e) {
                    Mensagem.addMensagem(3, "Erro ao tentar salvar!" + e);
                    System.out.println(e);
                }
            } else {
                try {
                    servicoItemCirurgiaItem = new ServicoItemCirurgiaItem();
                    if (convenio.getIdConvenio() != null) {
                        cirurgia.setIdConvenio(convenio);
                    }
                    if (orcamento.getIdOrcamento() != null) {
                        cirurgia.setIdOrcamento(orcamento);

                    }
                    if (plano.getIdPlano() != null) {
                        cirurgia.setIdPlano(plano);
                    }
                    // CHARLES - 06072017
                    // desabilitado por não haver uso por parte dos usuários                    

                    // if (unidadeAtendimento.getIdUnidade() != null) {
                    //   cirurgia.setIdUnidade(unidadeAtendimento);
                    // }                   
                    // if (responsavel.getIdPaciente() != null) {
                    //   cirurgia.setIdResponsavel(responsavel);
                    // }
                    String action = "N";
                    if (retornarSePodeLiberarRepasseFechamento()) {
                        action = "S";
                    }
                    if (action.equals("S")) {
                        cirurgia.setStatus('L');
                    } else if (action.equals("N")) {
                        cirurgia.setStatus('N');
                    }

                    for (Cheque ch : listaCheque) {
                        chequeEJB.Salvar(ch, us.retornaUsuario());
                    }

//                    for (Cheque ch : listaCheque) {
//                        for (Repasse r : repassesConsolidados) {
//                            if (ch.getIdCheque().equals(r.getIdCheque().getIdCheque())) {
//                                r.setIdCheque(ch);
//                            }
//                        }
//                    }
                    cirEJB.deletarContaCorrente(cirurgia);
                    cirEJB.deletaRepasses(cirurgia);
                    cirEJB.deleteSicii(cirurgia);
                    cirEJB.deleteCirurgiaItem(cirurgia);

                    cirEJB.Salvar(cirurgia, cirurgiaItens, itensDosItens, repassesConsolidados);

                    for (Cheque ch : listaCheque) {
                        if (!repassesConsolidados.isEmpty()) {
                            calcularValoresChequePorHonorarios(ch);
                            if (qdeHonorariosHospitalares > 0 && qdeHonorariosMedicos > 0) {
                                // CHARLES 06072017
                                // Se o parametro abaixo for verdadeiro, seta o tipo do CHEQUE (movimento = C) para não misto (tipo = N) para ser transferido para o caixa honorarios no momento da transfêrencia
                                if (assEJB.carregarAssociacao().getTransferirChequesCaixaHonorarios() == null) {
                                    ch.setTipo("M");
                                } else if (assEJB.carregarAssociacao().getTransferirChequesCaixaHonorarios() && ch.getMovimento().equals("C")) {
                                    ch.setTipo("N");
                                } else {
                                    ch.setTipo("M");
                                }
                            } else if (qdeHonorariosHospitalares > 0) {
                                ch.setTipo("H");
                            } else if (qdeHonorariosMedicos > 0) {
                                ch.setTipo("N");
                            } else {
                                ch.setTipo(null);
                            }
                        }
                        ch.setIdCirurgia(cirurgia);
                        chequeEJB.Salvar(ch, us.retornaUsuario());
                    }

                    if (cirurgiaItens.isEmpty()) {
                        RequestContext.getCurrentInstance().execute("alertify.error('Atenção! Você salvou o fechamento sem nenhum item adicionado!')");
                    }

                    if (action.equals("S")) {
                        // Se for dinheiro, deve cair no caixa no dia da movimentação
                        Receber();
                        aEJB.Salvar(BCRUtils.criarAuditoria("liberarfechamento", cirurgia.getIdCirurgia().toString(), us.retornaUsuario(), new Date(), "Fechamento"));
                    }
                    Mensagem.addMensagemGrowl(1, "Fechamento editado com sucesso.");
                    if (cirurgia.getIdUsuario().getIdPapel().getPermitirSomenteLancarPagamentosAtendimentoInterno()) {
                        mudaStatusConsultando();
                    }
                } catch (EJBException e) {
                    Mensagem.addMensagemPadraoErro("editar");
                    System.out.println(e);
                }
            }
            paramIdCirurgia = cirurgia.getIdCirurgia();
            //System.out.println("fechamento salvo " + paramIdCirurgia);
//            RequestContext.getCurrentInstance().execute("PF('dlgImprimir').show();");
        }

    }

    public void actionTeste() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String action = params.get("finalizar");

        //System.out.println("Mostrando a action: " + action);
    }

    public void imprimirFechamento(Integer idCirurgia) {
        RelatorioFactory.internacao("/WEB-INF/Relatorios/reportInternacao.jasper", idCirurgia, "C://SMA//logo.png");
        //System.out.println("Id da cirirgia: " + cirurgia.getIdCirurgia());
        novo();
    }

    public void imprimirListagemFechamento() {
        if (pacientePesquisa == null) {
//            System.out.println(dtInicialPesquisa);
//            System.out.println(dtFinalPesquisa);
//            System.out.println(statusPesquisa);
//            System.out.println(paramProcedimento);
//            System.out.println(paramTerceiro);
//            System.out.println(pacientePesquisa);
//            System.out.println(paramServico);
            RelatorioFactory.RelatorioListagemFechamento("/WEB-INF/Relatorios/reportFechamentosAgrupados.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, statusPesquisa, paramProcedimento, paramTerceiro, pacientePesquisa, paramServico);
        } else {
            RelatorioFactory.RelatorioListagemFechamento("/WEB-INF/Relatorios/reportResumoFechamentosPorPaciente.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, statusPesquisa, paramProcedimento, paramTerceiro, pacientePesquisa, paramServico);
        }

    }

    public void imprimirListagemFechamentoSintetico() {
        RelatorioFactory.RelatorioListagemFechamento("/WEB-INF/Relatorios/reportFechamentosSintetico.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, statusPesquisa, paramProcedimento, paramTerceiro, pacientePesquisa, paramServico);
    }

    public void imprimirListagemFechamentoSinteticoResumo() {
        RelatorioFactory.RelatorioListagemFechamentoResumo("/WEB-INF/Relatorios/reportFechamentoSinteticoResumo.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, paramProcedimento, paramTerceiro);
    }

    public void imprimirOrcadoXRealizado() {
        RelatorioFactory.RelatorioListagemFechamento("/WEB-INF/Relatorios/reportOrcadoXRealizado.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, statusPesquisa, paramProcedimento, paramTerceiro, pacientePesquisa, paramServico);
    }

    public void imprimirOrcadoXRealizadoPeriodo() {
        RelatorioFactory.RelatorioListagemFechamento("/WEB-INF/Relatorios/reportOrcadoXRealizadoPeriodo.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, statusPesquisa, paramProcedimento, paramTerceiro, pacientePesquisa, paramServico);
    }

    public void imprimirListagemAvulso() {
        List<Usuario> listaUsuarios = new ArrayList<>();
        List<String> lista = new ArrayList<>();
        if (paramUsuario == 9999) {
            listaUsuarios = uEJB.listarUsuariosProntoAtendimento();
            for (Usuario usu : listaUsuarios) {
                lista.add(usu.getIdUsuario().toString());
                //System.out.println(usu.getIdUsuario().toString());
            }
        }
        RelatorioFactory.RelatorioListagemAvulsos("/WEB-INF/Relatorios/reportAvulsosAgrupados.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, "%", paramProcedimento, paramTerceiro, paramUsuario, paramPago, lista, exceto);
    }

    public void imprimirListagemLaudos() {
        RelatorioFactory.RelatorioListagemLaudos("/WEB-INF/Relatorios/reportLaudos.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, paramProcedimento, paramTerceiro, paramTerceiroLaudo);
    }

    public Double getSomaValores(List<Cirurgia> listaFechamento) {
        Double total = new Double(0);
        for (Cirurgia c : listaFechamento) {
            total += c.getValor();
        }
        return total;
    }

    public Double retornaTotalTasyContaPaciente(List<ProcedimentoPaciente> listaProcedimentos) {
        Double total = 0D;
        for (ProcedimentoPaciente pp : listaProcedimentos) {
            total += pp.getValorProcedimento();
        }
        return total;
    }

    public Double retornaTotalRealContaPaciente(List<ProcedimentoPaciente> listaProcedimentos) {
        Double total = 0D;
        for (ProcedimentoPaciente pp : listaProcedimentos) {
            total += retornaValorAjustadoPorRegraRepasse(pp);
        }
        return total;
    }

    public Double retornaTotalGeralContaPacientePorItens(List<CirurgiaItem> listaItens, Integer campo) {
        Double total = 0D;
        for (CirurgiaItem ci : listaItens) {
            switch (campo) {
                case 1:
                    total += ci.getValorTotal();
                    break;
                case 2:
                    total += ci.getValorTasy();
                    break;
                case 3:
                    total += ci.getValorTotalGeral();
                    break;
                case 4:
                    total += ci.getDesconto();
                    break;
            }
        }
        return total;
    }

    public boolean retornaStatusRegraRepasseItens(List<CirurgiaItem> lista) {
        boolean valoresRegras = false;
        for (CirurgiaItem cii : lista) {
            // NR_SEQ_PROC_PACOTE é o campo a ser monitorado, no caso de procedimentos em pacote.
            if (cii.getValorTotal().equals(0D) && (cii.getIdProcedimentoPaciente().getNrSeqProcPacote() == null)) {
                valoresRegras = true;
            }
            return valoresRegras;
        }
        return valoresRegras;
    }

    public void calcularValoresChequePorHonorarios(Cheque chq) {
        valorHonorariosHospitalares = 0D;
        valorHonorariosMedicos = 0D;
        valorTotalHonorario = 0D;
        qdeHonorariosHospitalares = 0;
        qdeHonorariosMedicos = 0;
        for (Repasse rep : repassesConsolidados) {
            if (rep.getIdCheque() == chq) {
                if ("N".equals(rep.getIdSici().getIdServicoItem().getIdServico().getTipo())) {
                    valorHonorariosMedicos = valorHonorariosMedicos + rep.getValorDocumento();
                    qdeHonorariosMedicos++;
                } else if ("H".equals(rep.getIdSici().getIdServicoItem().getIdServico().getTipo())) {
                    valorHonorariosHospitalares = valorHonorariosHospitalares + rep.getValorDocumento();
                    qdeHonorariosHospitalares++;
                }
                valorTotalHonorario = valorTotalHonorario + rep.getValorDocumento();
            }
        }
        if (qdeHonorariosHospitalares > 0 && qdeHonorariosMedicos > 0) {
        } else if (qdeHonorariosHospitalares > 0) {
        } else if (qdeHonorariosMedicos > 0) {
        } else {
            System.out.println("Vazio");
        }
    }

    public List<ContaPaciente> pesquisarContasPacientePorAtendimento(Integer idAtendimento) {
        return cpEJB.listarContaPacientePorAtendimento(idAtendimento);
    }

    public List<ProcedimentoPaciente> pesquisarProcedimentosPacientePorContaPaciente(Integer idContaPaciente) {
        return ppEJB.selecionarListaDeProcedimentosPorContaPaciente(idContaPaciente);
    }

    public String retornaTipoCheque(Cheque chq) {
        qdeHonorariosHospitalares = 0;
        qdeHonorariosMedicos = 0;
        for (Repasse rep : chq.getRepasseList()) {
            if ("N".equals(rep.getIdSici().getIdServicoItem().getIdServico().getTipo())) {
                qdeHonorariosMedicos++;
            } else if ("H".equals(rep.getIdSici().getIdServicoItem().getIdServico().getTipo())) {
                qdeHonorariosHospitalares++;
            }
        }
        if (qdeHonorariosHospitalares > 0 && qdeHonorariosMedicos > 0) {
            return "Misto";
        } else if (qdeHonorariosHospitalares > 0) {
            return "Hospital";
        } else if (qdeHonorariosMedicos > 0) {
            return "Médicos";
        } else {
            return "Vazio";
        }
    }

    public void liberarFechamento(String status) {
        liberar = status;
    }

    public String verificaRepassesOnComplete() {
        int i = 0;
        i = repassesConsolidados.size();
        //System.out.println("2" + i);
        if (i > 0) {
            return "PF('dlgLiberacao').show()";
        } else {
            return "#{cirurgiaMB.Salvar()}";
        }
    }

    public boolean validarCamposObrigatorios() {
        if (cirurgia.getIdPaciente() == null) {
            Mensagem.addMensagem(3, "Informe o paciente!");
            return false;
        } else if (cirurgia.getIdProcedimento() == null) {
            Mensagem.addMensagem(3, "Informe o procedimento!");
            return false;
        } else if (cirurgia.getIdTerceiro() == null) {
            Mensagem.addMensagem(3, "Informe o médico responsável!");
            return false;
        } else if (cirurgia.getIdForma() == null) {
            Mensagem.addMensagem(3, "Informe o forma de pagamento");
            return false;
        } else if (cirurgia.getDtInternacao() == null) {
            Mensagem.addMensagem(3, "Informe a data de internação");
            return false;
        } else {
            return true;
        }
    }

    public boolean validarItensCamposObrigatorios() {
        if (cirurgiaItem.getValorUnitario() == null || cirurgiaItem.getValorUnitario().equals(0D)) {
            Mensagem.addMensagem(3, "Informe o valor unitário do item.");
            return false;
        } else if (cirurgiaItem.getQuantidade() == null || cirurgiaItem.getQuantidade().equals(0D)) {
            Mensagem.addMensagem(3, "Informe a quantidade.");
            return false;
        } else {
            return true;
        }
    }

    public boolean validarCamposObrigatoriosCheque() {
        if (arredondar_double(valorTotalPago + valorCheque) > arredondar_double((valorTotalPago + valorRestante))) {
            Mensagem.addMensagem(3, "Valor pago excede ao valor total do fechamento. Valor restante a pagar: R$ " + arredondar_double(valorRestante));
            return false;
        } else if (movimento.equals("C")) {
            if (numeroCheque == null) {
                Mensagem.addMensagem(3, "Informe o número do cheque!");
                return false;
            } else if (banco == null) {
                Mensagem.addMensagem(3, "Informe o banco.");
                return false;
            } else if (valorCheque.equals(0)) {
                Mensagem.addMensagem(3, "Informe o valor do pagamento.");
                return false;
            } else {
                return true;
            }
        } else if (valorCheque.equals(0)) {
            Mensagem.addMensagem(3, "Informe o valor do pagamento.");
            return false;
        } else {
            return true;
        }
    }

    public boolean verificaValoresRepasseComOrcamento() {
        Double valor = 0D;
        for (Repasse r : repasses) {
            valor = r.getValor() + valor;
        }
        if (valor < cirurgia.getValor()) {
            return false;
        } else if (valor > cirurgia.getValor()) {
            return false;
        } else {
            return true;
        }
    }

    public Cirurgia selecionarPorID(Integer id) {
        mudaStatusInserindo();
        novo();
        repasses = new ArrayList<Repasse>();
        repassesConsolidados = new ArrayList<Repasse>();
        psAgrupado = new ArrayList<Parcela>();
        cirurgia = cirEJB.selecionarPorID(id);
        paramIdCirurgia = cirurgia.getIdCirurgia();
        if (cirurgia.getIdOrcamento() != null) {
            orcamento = cirurgia.getIdOrcamento();
        }
        if (cirurgia.getIdPlano() != null) {
            plano = cirurgia.getIdPlano();
        }
        if (cirurgia.getIdForma() != null) {
            formaPagamento = cirurgia.getIdForma();
        }
        if (cirurgia.getIdUnidade() != null) {
            unidadeAtendimento = cirurgia.getIdUnidade();
        }

        if (cirurgia.getIdResponsavel() != null) {
            responsavel = cirurgia.getIdResponsavel();
        }

        if (cirurgia.getCirurgiaItemList() != null) {
            cirurgiaItens = new ArrayList<CirurgiaItem>();
            cirurgiaItens = cirurgia.getCirurgiaItemList();
        }
        if (cirurgia.getServicoItemCirurgiaItemList() != null) {
            itensDosItens = new ArrayList<ServicoItemCirurgiaItem>();
            itensDosItens = cirurgia.getServicoItemCirurgiaItemList();
        }
        if (cirurgia.getRepasseList() != null) {
            repassesConsolidados = new ArrayList<>();
            repassesConsolidados = cirurgia.getRepasseList();
        }
        if (cirurgia.getChequeList() != null) {
            listaCheque = new ArrayList<Cheque>();
            listaCheque = cirurgia.getChequeList();
        }

        calcularTotalRepasse();
        calcularValores();
        calcularValoresTerceiros();
        calcularRestanteValoresTerceirosTotais();
        calcularValorInternacao();
        return cirurgia;
    }

    public String maiuscula(String a) {
        return StringUtils.capitalize(a);

    }

    public Cirurgia selecionarPorIDExclusao(Integer id) {
        cirurgia = cirEJB.selecionarPorID(id);
        return cirurgia;
    }

    public void LiberarCirurgia(Cirurgia cirurgia) {
        selecionarPorID(cirurgia.getIdCirurgia());
        if (!repassesConsolidados.isEmpty() && !listaCheque.isEmpty()) {
            Receber();
        }
        if (cirurgia.getIdOrcamento() != null) {
            orcamento = cirurgia.getIdOrcamento();
        }
        cirurgia.setStatus('L');
        cirEJB.atualizarCirurgia(cirurgia);
        Mensagem.addMensagem(1, "Fechamento e Pagamentos liberados!");
    }

    public void CancelarFechamento() {
        try {
            cirurgia.setStatus('N');
            cirurgia.setCancelado(true);
            if (cirurgia.getIdOrcamento() != null) {
                Orcamento oupdate = new Orcamento();
                oupdate = cirurgia.getIdOrcamento();
                oupdate.setSituacao("Lançado");
                oupdate.setDtExecucao(null);
                oupdate.setIdStatus(stEJB.selecionarPorTipo(1));
                oEJB.atualizarOrcamento(oupdate);
            }
            cirEJB.Cancelar(cirurgia);
            Mensagem.addMensagem(1, "Fechamento cancelado!");
            listaCirurgiaAPesquisar = new ArrayList<>();
            BCRUtils.ResetarDatatableFiltros("frmConsultar:tblPesq");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
            System.out.println(e);
        }
    }

    public String retornaLoteOrigemPorFechamento(Integer id) {
        return cirEJB.retornaLoteOrigemPorFechamento(id);
    }

    public void processarListaCheque(String id) throws ParseException {
        if (id == null) {
            id = "%";
        }
        listaCheque = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = Conexao.conexaoJDBC().createStatement();
            try {
                String sql = "SELECT c.id_paciente idPaciente, c.id_cheque idCheque,        "
                        + "c.valor Valor, c.numero_cheque numeroCheque,           "
                        + "ifNull(c.id_banco,0) idBanco, c.compensado compensado,           "
                        + "c.conta Conta, c.dt_recebimento_emissao dtEmissao,     "
                        + "c.dt_compensacao_vencimento dtCompensado, c.tipo Tipo, "
                        + "c.movimento MOV, c.numero_parcela NUMP, c.observacao OBS "
                        + "FROM cheque c, repasse r "
                        + "WHERE r.id_cirurgia = " + id
                        + " AND r.id_cheque = c.id_cheque"
                        + " GROUP BY c.id_cheque, c.numero_cheque";
                //System.out.println(sql);
                resultSet = statement.executeQuery(sql);
                Cheque c = new Cheque();
                while (resultSet.next()) {
                    c = new Cheque();
                    c.setIdPaciente(repasseEJB.pesquisarPaciente(resultSet.getInt("idPaciente")));
                    c.setIdCheque(resultSet.getInt("idCheque"));
                    c.setValor(resultSet.getDouble("Valor"));
                    c.setNumeroCheque(resultSet.getString("numeroCheque"));
                    if (resultSet.getInt("idBanco") != 0) {
                        c.setIdBanco(repasseEJB.selecionarBancoPorID(resultSet.getInt("idBanco")));
                    }
                    c.setConta(resultSet.getString("Conta"));
                    c.setDtRecebimentoEmissao(resultSet.getDate("dtEmissao"));
                    c.setDtCompensacaoVencimento(resultSet.getDate("dtCompensado"));
                    c.setTipo(resultSet.getString("Tipo"));
                    c.setObservacao(resultSet.getString("OBS"));
                    c.setCompensado(resultSet.getString("Compensado"));
                    c.setMovimento(resultSet.getString("MOV"));
                    c.setNumeroParcela(resultSet.getInt("NUMP"));
                    listaCheque.add(c);
                }
                calcularValoresPagos();

                resultSet.close();
                statement.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (Exception e) {
        }

    }

    public void calcularParcelasAposAlteracao() {
        ArrayList<Integer> parcelas = new ArrayList<Integer>();
        Set<Integer> parcelasSemRepeticao = new LinkedHashSet<Integer>();
        psAgrupado = new ArrayList<Parcela>();
        for (Repasse r : repasses) {
            parcelas.add(r.getNumeroParcela());
        }
        parcelasSemRepeticao.addAll(parcelas);
        parcelasSemRepeticao.remove(null);
        parcelas = new ArrayList<Integer>();
        parcelas.addAll(parcelasSemRepeticao);

        for (Integer i : parcelas) {
            Double val = 0D, valorPago = 0D;
            Date dtVenc = null;
            String pago = "";
            Cirurgia c1 = new Cirurgia();
            List<Cheque> listaTemporaria = new ArrayList<Cheque>();
            List<Repasse> listaTemporariaRepasse = new ArrayList<Repasse>();
            Parcela parAdd = new Parcela();
            for (Repasse par : repasses) {
                if (par.getNumeroParcela() == i) {
                    val = val + par.getValor();
                    dtVenc = par.getDtVencimento();
                    c1 = par.getIdCirurgia();
                    listaTemporariaRepasse.add(par);
                    if (par.getIdCheque() != null) {
                        cheque = par.getIdCheque();
                        if (!listaTemporaria.contains(cheque)) {
                            listaTemporaria.add(cheque);
                            valorPago = valorPago + cheque.getValor();
                        }
                    }
                }
            }
            if ((val > valorPago) && (valorPago > 0)) {
                pago = "P";
            } else if (val.equals(valorPago)) {
                pago = "S";
            } else {
                pago = "N";
            }
            parAdd.setValorParcela(val);
            parAdd.setNumParcela(i);
            parAdd.setDataVencimento(dtVenc);
            parAdd.setPago(pago);
            parAdd.setForma_pagamento(pagamento);
            parAdd.setListaChequeParcelas(listaTemporaria);
            parAdd.setListaRepasses(listaTemporariaRepasse);
            psAgrupado.add(parAdd);
        }

        ps.addAll(psAgrupado);
    }

    public Cirurgia selecionarPorIDInfo(Integer id) {
        cirurgia = new Cirurgia();
        cirurgiaItens = new ArrayList<CirurgiaItem>();
        itensDosItens = new ArrayList<ServicoItemCirurgiaItem>();
        repasses = new ArrayList<Repasse>();
        cirurgia = cirEJB.selecionarPorID(id);
        cirurgiaItens = cirurgia.getCirurgiaItemList();
        itensDosItens = cirurgia.getServicoItemCirurgiaItemList();
        repassesConsolidados = cirurgia.getRepasseList();
        listaCheque = cirurgia.getChequeList();
        calcularValoresPagos();
        calcularTotalRepasse();
        calcularRepassesAgrupados();
        return cirurgia;
    }

    public void calcularRepassesAgrupados() {
        listVt = new ArrayList<>();
        List<Object[]> listRepasses = new ArrayList<>();
        listRepasses = repasseEJB.pesquisarRepasseAgrupadosPorCirurgia(cirurgia.getIdCirurgia());
        valorResumoRepasses = 0d;
        valorHonorariosHospitalares = 0D;
        valorHonorariosMedicos = 0D;
        for (Object[] c : listRepasses) {
            ValoresTerceiros vt = new ValoresTerceiros();
            vt.setIdTerceiro((Integer) c[1]);
            vt.setTerceiro(tEJB.selecionarPorID(vt.getIdTerceiro(), us.retornaUsuario()).getNome() == null ? tEJB.selecionarPorID(vt.getIdTerceiro(), us.retornaUsuario()).getNomeFantasia() : tEJB.selecionarPorID(vt.getIdTerceiro(), us.retornaUsuario()).getNome());
            vt.setValor((Double) c[0]);
            listVt.add(vt);
        }
        for (ValoresTerceiros vt : listVt) {
            if (vt.getTerceiro().contains("HOSPITAL")) {
                valorHonorariosHospitalares += vt.getValor();
            } else {
                valorHonorariosMedicos += vt.getValor();
            }
            valorResumoRepasses += vt.getValor();
        }

    }

    public boolean retornarSePodeLiberarRepasseFechamento() {
        // Calcular o valor das parcelas já inseridas no repasse.
        calcularTotalRepasse();
        Double diff = 0D;
        // Pega o valor do Fechamento.
        diff = arredondar_double(cirurgia.getValor() - totalParcela);
        // Compara com o parametro. Se a diferença for maior ou igual do que o permitido, retorna False, ou seja, não pode fazer a liberação.
        //System.out.println(diff.toString());
        if (diff >= assEJB.retornaDiferencaPermitida()) {
            return false;
        } else {
            // Se retorna true, faça a liberação, pois o valor da diferença é inexistente ou está dentro do permitido.
            return true;
        }
    }

    public void limpaObjeto() {
        cirurgia = new Cirurgia();
        mudaStatusConsultando();
    }

    public void mudaStatusInserindo() {
        setStatus("Inserindo");
    }

    public void mudaStatusConsultando() {
        novo();
        setStatus("Consultando");
    }
    //Consultas //
    private Date dtInicialPesquisa;
    private Date dtFinalPesquisa;
    private Integer idPacientePesquisa, pacientePesquisa = null;
    private String situacaoPesquisa;
    private String statusPesquisa;
    private Integer numeroCirurgia;
    private Integer idProcedimentoPesquisa;
    private Integer idTerceiroPesquisa;

    public Integer getNumeroCirurgia() {
        return numeroCirurgia;
    }

    public void setNumeroCirurgia(Integer numeroCirurgia) {
        this.numeroCirurgia = numeroCirurgia;
    }

    public void setDtInicialPesquisa(Date dtInicialPesquisa) {
        this.dtInicialPesquisa = dtInicialPesquisa;
    }

    public void setDtFinalPesquisa(Date dtFinalPesquisa) {
        this.dtFinalPesquisa = dtFinalPesquisa;
    }

    public void setIdPacientePesquisa(Integer idPacientePesquisa) {
        this.idPacientePesquisa = idPacientePesquisa;
    }

    public void setSituacaoPesquisa(String situacaoPesquisa) {
        this.situacaoPesquisa = situacaoPesquisa;
    }

    public void setStatusPesquisa(String statusPesquisa) {
        this.statusPesquisa = statusPesquisa;
    }

    public Date getDtInicialPesquisa() {
        return dtInicialPesquisa;
    }

    public Date getDtFinalPesquisa() {
        return dtFinalPesquisa;
    }

    public Integer getIdPacientePesquisa() {
        return idPacientePesquisa;
    }

    public String getSituacaoPesquisa() {
        return situacaoPesquisa;
    }

    public String getStatusPesquisa() {
        return statusPesquisa;
    }

    public Integer getIdProcedimentoPesquisa() {
        return idProcedimentoPesquisa;
    }

    public void setIdProcedimentoPesquisa(Integer idProcedimentoPesquisa) {
        this.idProcedimentoPesquisa = idProcedimentoPesquisa;
    }

    public Integer getIdTerceiroPesquisa() {
        return idTerceiroPesquisa;
    }

    public void setIdTerceiroPesquisa(Integer idTerceiroPesquisa) {
        this.idTerceiroPesquisa = idTerceiroPesquisa;
    }
    private List<Cirurgia> listaCirurgiaAPesquisar;

    public List<Cirurgia> getListaOrcamentoAPesquisar() {
        return listaCirurgiaAPesquisar;
    }

    public void pesquisarCirurgias(String ti) {
        Map<String, String> params
                = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String action = params.get("form");

        if (ti.isEmpty()) {
            ti = action;
        }
        if ((action.equals("EC") && ti.equals("EC"))) {
            ti = "Internacao";
            exceto = true;
            tipoInternacao = cirEJB.retornaTipoInternacao(ti);
        } else {
            exceto = false;
            tipoInternacao = cirEJB.retornaTipoInternacao(ti);
        }

        listaCirurgiaAPesquisar = new ArrayList<Cirurgia>();
        listaCirurgiaAPesquisar = cirEJB.pesquisarCirurgias(dtInicialPesquisa, dtFinalPesquisa, pacientePesquisa, getNumeroCirurgia(), procedimento == null ? null : procedimento.getIdProcedimento(), getIdTerceiroPesquisa(), tipoInternacao.getIdTipoInternacao(), exceto);
        if (listaCirurgiaAPesquisar.isEmpty()) {
            Mensagem.addMensagem(3, "Não foi encontrado nenhum registro!");
        }
    }

    public List<ServicoItemCirurgiaItem> getListadeItensDosItens() {
        return listadeItensDosItens;
    }

    public void setListadeItensDosItens(List<ServicoItemCirurgiaItem> listadeItensDosItens) {
        this.listadeItensDosItens = listadeItensDosItens;
    }

    public List<Cirurgia> getListaCirurgiaAPesquisar() {
        return listaCirurgiaAPesquisar;
    }

    public void setListaCirurgiaAPesquisar(List<Cirurgia> listaCirurgiaAPesquisar) {
        this.listaCirurgiaAPesquisar = listaCirurgiaAPesquisar;
    }

    public List<Motivo> listarMotivos() {
        return mEJB.listarMotivos();
    }

    public Boolean verificaPagamentoDeCirurgia(Cirurgia cirurgia) {
        Long total = cirEJB.contarPagamentos(cirurgia.getIdCirurgia());
        if (total == 0) {
            return false;
        } else {
            return true;
        }
    }

    public List<TipoInternacao> listaTiposInternacoes() {
        return tiEJB.listarTipoInternacoes();
    }

    public List<Procedimento> listaDeProcedimentos() {
        return proEJB.listarProcedimentos();
    }

    public List<Terceiro> listaDeTerceiros() {
        return tEJB.listarTerceiro();
    }

    public String geraCorAcordoComParcela(Integer parcela) {
        if (parcela == 1) {
            return "classRow1";
        } else if (parcela == 2) {
            return "classRow2";
        } else if (parcela == 3) {
            return "classRow3";
        } else if (parcela == 4) {
            return "classRow4";
        } else if (parcela == 5) {
            return "classRow5";
        } else if (parcela == 6) {
            return "classRow6";
        } else if (parcela == 7) {
            return "classRow7";
        } else if (parcela == 8) {
            return "classRow8";
        } else if (parcela == 9) {
            return "classRow9";
        } else if (parcela == 9) {
            return "classRow9";
        } else {
            return null;
        }
    }

    public boolean verificaSePodeCancelar(Cirurgia cirurgia) {
        if (cirEJB.verificaSePodeCancelar(cirurgia) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public String getCompensado() {
        return compensado;
    }

    public void setCompensado(String compensado) {
        this.compensado = compensado;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Double getValorCheque() {
        return valorCheque;
    }

    public void setValorCheque(Double valorCheque) {
        this.valorCheque = valorCheque;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Date getDtCompensado() {
        return dtCompensado;
    }

    public void setDtCompensado(Date dtCompensado) {
        this.dtCompensado = dtCompensado;
    }

    public boolean isDiferenca() {
        return diferenca;
    }

    public void setDiferenca(boolean diferenca) {
        this.diferenca = diferenca;
    }

    public Double getValorDifHospital() {
        return valorDifHospital;
    }

    public void setValorDifHospital(Double valorDifHospital) {
        this.valorDifHospital = valorDifHospital;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Double getValorRecebido() {
        return valorRecebido;
    }

    public void setValorRecebido(Double valorRecebido) {
        this.valorRecebido = valorRecebido;
    }

    public Double getValorTotalPgto() {
        return valorTotalPgto;
    }

    public void setValorTotalPgto(Double valorTotalPgto) {
        this.valorTotalPgto = valorTotalPgto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double arredondar(Double d) {
        return Math.ceil(d);
    }

    public Double arredondar_double(Double d) {
        return Math.round(d * 100.0) / 100.0;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public List<Cheque> getListaCheque() {
        return listaCheque;
    }

    public void setListaCheque(List<Cheque> listaCheque) {
        this.listaCheque = listaCheque;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Terceiro getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(Terceiro terceiro) {
        this.terceiro = terceiro;
    }

    public Parcela getParcelaPaga() {
        return parcelaPaga;
    }

    public void setParcelaPaga(Parcela parcelaPaga) {
        this.parcelaPaga = parcelaPaga;
    }

    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    public Integer getNumeroParcela() {
        return numeroParcela;
    }

    public void setNumeroParcela(Integer numeroParcela) {
        this.numeroParcela = numeroParcela;
    }

    public String getTipoInter() {
        return tipoInter;
    }

    public void setTipoInter(String tipoInter) {
        this.tipoInter = tipoInter;
    }

    public boolean isExceto() {
        return exceto;
    }

    public void setExceto(boolean exceto) {
        this.exceto = exceto;
    }

    public TipoInternacao getTipoInternacao() {
        return tipoInternacao;
    }

    public void setTipoInternacao(TipoInternacao tipoInternacao) {
        this.tipoInternacao = tipoInternacao;
    }

    public List<TipoInternacao> getListaTipoInternacao() {
        return listaTipoInternacao;
    }

    public void setListaTipoInternacao(List<TipoInternacao> listaTipoInternacao) {
        this.listaTipoInternacao = listaTipoInternacao;
    }

    public Double getValorInternacao() {
        return valorInternacao;
    }

    public void setValorInternacao(Double valorInternacao) {
        this.valorInternacao = valorInternacao;
    }

    public String getEmitenteCheque() {
        return emitenteCheque;
    }

    public void setEmitenteCheque(String emitenteCheque) {
        this.emitenteCheque = emitenteCheque;
    }

    public String getMovimento() {
        return movimento;
    }

    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public Double getValorHonorariosMedicos() {
        return valorHonorariosMedicos;
    }

    public void setValorHonorariosMedicos(Double valorHonorariosMedicos) {
        this.valorHonorariosMedicos = valorHonorariosMedicos;
    }

    public Double getValorHonorariosHospitalares() {
        return valorHonorariosHospitalares;
    }

    public void setValorHonorariosHospitalares(Double valorHonorariosHospitalares) {
        this.valorHonorariosHospitalares = valorHonorariosHospitalares;
    }

    public Double getValorTotalHonorario() {
        return valorTotalHonorario;
    }

    public void setValorTotalHonorario(Double valorTotalHonorario) {
        this.valorTotalHonorario = valorTotalHonorario;
    }

    public Double getValorTotalPago() {
        return valorTotalPago;
    }

    public void setValorTotalPago(Double valorTotalPago) {
        this.valorTotalPago = valorTotalPago;
    }

    public List<Cheque> getListaChequeTemporaria() {
        return listaChequeTemporaria;
    }

    public void setListaChequeTemporaria(List<Cheque> listaChequeTemporaria) {
        this.listaChequeTemporaria = listaChequeTemporaria;
    }

    public List<Repasse> getListaRepassesHonorariosMedicos() {
        return listaRepassesHonorariosMedicos;
    }

    public void setListaRepassesHonorariosMedicos(List<Repasse> listaRepassesHonorariosMedicos) {
        this.listaRepassesHonorariosMedicos = listaRepassesHonorariosMedicos;
    }

    public List<Repasse> getListaRepassesHonorariosHospitalares() {
        return listaRepassesHonorariosHospitalares;
    }

    public void setListaRepassesHonorariosHospitalares(List<Repasse> listaRepassesHonorariosHospitalares) {
        this.listaRepassesHonorariosHospitalares = listaRepassesHonorariosHospitalares;
    }

    public List<Repasse> getListaRepassesHonorariosTotal() {
        return listaRepassesHonorariosTotal;
    }

    public void setListaRepassesHonorariosTotal(List<Repasse> listaRepassesHonorariosTotal) {
        this.listaRepassesHonorariosTotal = listaRepassesHonorariosTotal;
    }

    public Double getValorInfoCheques() {
        return valorInfoCheques;
    }

    public void setValorInfoCheques(Double valorInfoCheques) {
        this.valorInfoCheques = valorInfoCheques;
    }

    public Double getValorTotalItensHospital() {
        return valorTotalItensHospital;
    }

    public void setValorTotalItensHospital(Double valorTotalItensHospital) {
        this.valorTotalItensHospital = valorTotalItensHospital;
    }

    public Double getValorRestante() {
        return valorRestante;
    }

    public void setValorRestante(Double valorRestante) {
        this.valorRestante = valorRestante;
    }

    public void imprimirInternacao(Integer idCirurgia) {
        //System.out.println("id da cirurgia antes do relatorio: " + paramTerceiro);
        RelatorioFactory.internacao("/WEB-INF/Relatorios/reportReciboAtendimentoExterno_MeiaFolha.jasper", idCirurgia, "C://SMA//logo.png");
        //System.out.println("id da cirurgia depois do relatorio: " + cirurgia.getIdCirurgia());
    }

    public void imprimirInternacaoAnalitico(Integer idCirurgia) {
        //System.out.println("id da cirurgia antes do relatorio: " + paramTerceiro);
        RelatorioFactory.internacao("/WEB-INF/Relatorios/reportInternacao.jasper", idCirurgia, "C://SMA//logo.png");
        //System.out.println("id da cirurgia depois do relatorio: " + cirurgia.getIdCirurgia());
    }

    public void imprimirAtendimentoExterno(Cirurgia cir) {

        // IF desnecessário temporariamente, pois com a lista zerada não aciona a função, porem, por preguiça de reescrever o codigo, estou deixando.
        if (cir.getCirurgiaItemList().isEmpty()) {
            cirurgia = cir;
            RequestContext.getCurrentInstance().execute("PF('dlgInformarValores').show();");
        } else {
            //System.out.println("id da cirurgia antes do relatorio: " + paramTerceiro);
            RelatorioFactory.internacao("/WEB-INF/Relatorios/reportReciboAtendimentoExterno_MeiaFolha.jasper", cir.getIdCirurgia(), "C://SMA//logo.png");
            //System.out.println("id da cirurgia depois do relatorio: " + cirurgia.getIdCirurgia()); 
        }

    }

    public void imprimirAtendimentoExternoValores() {
        if (numeroAtendimento == null || numeroAtendimento.equals(0)) {
            RelatorioFactory.reciboAvulso("/WEB-INF/Relatorios/reportReciboAvulso_MeiaFolha.jasper", paciente.getIdPaciente(), terceiro.getIdTerceiro(), observacao, valorReciboMed, valorReciboHosp, servicoExterno.getNomeProcedimentoInternoTasy() == null ? servicoExterno.getDescricao() : servicoExterno.getNomeProcedimentoInternoTasy(), "C://SMA//logo.png");
        } else {
            RelatorioFactory.reciboExterno("/WEB-INF/Relatorios/reportReciboAtendimentoExterno_Avulso_MeiaFolha.jasper", numeroAtendimento, observacao, valorReciboMed, valorReciboHosp, servicoExterno.getNomeProcedimentoInternoTasy() == null ? servicoExterno.getDescricao() : servicoExterno.getNomeProcedimentoInternoTasy(), "C://SMA//logo.png");
        }

        //System.out.println("id da cirurgia depois do relatorio: " + cirurgia.getIdCirurgia()); 
    }

    public void imprimirAtendimentoInterno() {
        RelatorioFactory.internacao("/WEB-INF/Relatorios/reportReciboAtendimentoInterno_MeiaFolha.jasper", cirurgia.getIdCirurgia(), "C://SMA//logo.png");
    }

    public void imprimirFechamentoSintetico(Integer idCirurgia) {
        RelatorioFactory.internacao("/WEB-INF/Relatorios/reportFechamentoSintetico.jasper", idCirurgia, "C://SMA//logo.png");
    }

    public List<Repasse> getRepassesConsolidados() {
        return repassesConsolidados;
    }

    public void setRepassesConsolidados(List<Repasse> repassesConsolidados) {
        this.repassesConsolidados = repassesConsolidados;
    }

    public ValoresTerceiros getValoresTerceiros() {
        return valoresTerceiros;
    }

    public void setValoresTerceiros(ValoresTerceiros valoresTerceiros) {
        this.valoresTerceiros = valoresTerceiros;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Double getWidgetValorPago() {
        return widgetValorPago;
    }

    public void setWidgetValorPago(Double widgetValorPago) {
        this.widgetValorPago = widgetValorPago;
    }

    public Double getWidgetValorRestante() {
        return widgetValorRestante;
    }

    public void setWidgetValorRestante(Double widgetValorRestante) {
        this.widgetValorRestante = widgetValorRestante;
    }

    public Double getWidgetValorTotal() {
        return widgetValorTotal;
    }

    public void setWidgetValorTotal(Double widgetValorTotal) {
        this.widgetValorTotal = widgetValorTotal;
    }

    public ServicoItemCirurgiaItem getServicoItemCirurgiaItem() {
        return servicoItemCirurgiaItem;
    }

    public void setServicoItemCirurgiaItem(ServicoItemCirurgiaItem servicoItemCirurgiaItem) {
        this.servicoItemCirurgiaItem = servicoItemCirurgiaItem;
    }

    public List<ServicoItemCirurgiaItem> getListaServicosPorTerceiros() {
        return listaServicosPorTerceiros;
    }

    public void setListaServicosPorTerceiros(List<ServicoItemCirurgiaItem> listaServicosPorTerceiros) {
        this.listaServicosPorTerceiros = listaServicosPorTerceiros;
    }

    public List<ServicoItemCirurgiaItem> getListaServicosPorTerceirosTemporaria() {
        return listaServicosPorTerceirosTemporaria;
    }

    public void setListaServicosPorTerceirosTemporaria(List<ServicoItemCirurgiaItem> listaServicosPorTerceirosTemporaria) {
        this.listaServicosPorTerceirosTemporaria = listaServicosPorTerceirosTemporaria;
    }

    public Double getValorTotalaRepassar() {
        return valorTotalaRepassar;
    }

    public void setValorTotalaRepassar(Double valorTotalaRepassar) {
        this.valorTotalaRepassar = valorTotalaRepassar;
    }

    public Double getValorRepassado() {
        return valorRepassado;
    }

    public void setValorRepassado(Double valorRepassado) {
        this.valorRepassado = valorRepassado;
    }

    public Double getValorTotalCirurgia() {
        return valorTotalCirurgia;
    }

    public void setValorTotalCirurgia(Double valorTotalCirurgia) {
        this.valorTotalCirurgia = valorTotalCirurgia;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public boolean isCPFCPNJ() {
        return CPFCPNJ;
    }

    public void setCPFCPNJ(boolean CPFCPNJ) {
        this.CPFCPNJ = CPFCPNJ;
    }

    public Double getValoresPagos() {
        return valoresPagos;
    }

    public void setValoresPagos(Double valoresPagos) {
        this.valoresPagos = valoresPagos;
    }

    public boolean isValidacaoCGC() {
        return validacaoCGC;
    }

    public void setValidacaoCGC(boolean validacaoCGC) {
        this.validacaoCGC = validacaoCGC;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public String getLiberar() {
        return liberar;
    }

    public void setLiberar(String liberar) {
        this.liberar = liberar;
    }

    public Integer getQdeHonorariosMedicos() {
        return qdeHonorariosMedicos;
    }

    public void setQdeHonorariosMedicos(Integer qdeHonorariosMedicos) {
        this.qdeHonorariosMedicos = qdeHonorariosMedicos;
    }

    public Integer getQdeHonorariosHospitalares() {
        return qdeHonorariosHospitalares;
    }

    public void setQdeHonorariosHospitalares(Integer qdeHonorariosHospitalares) {
        this.qdeHonorariosHospitalares = qdeHonorariosHospitalares;
    }

    public Integer getParamProcedimento() {
        return paramProcedimento;
    }

    public void setParamProcedimento(Integer paramProcedimento) {
        this.paramProcedimento = paramProcedimento;
    }

    public Integer getParamTerceiro() {
        return paramTerceiro;
    }

    public void setParamTerceiro(Integer paramTerceiro) {
        this.paramTerceiro = paramTerceiro;
    }

    public Integer getParamUsuario() {
        return paramUsuario;
    }

    public void setParamUsuario(Integer paramUsuario) {
        this.paramUsuario = paramUsuario;
    }

    public boolean isFinalizarParciais() {
        return finalizarParciais;
    }

    public void setFinalizarParciais(boolean finalizarParciais) {
        this.finalizarParciais = finalizarParciais;
    }

    public List<ValoresTerceiros> getListVt() {
        return listVt;
    }

    public void setListVt(List<ValoresTerceiros> listVt) {
        this.listVt = listVt;
    }

    public Double getValorResumoRepasses() {
        return valorResumoRepasses;
    }

    public void setValorResumoRepasses(Double valorResumoRepasses) {
        this.valorResumoRepasses = valorResumoRepasses;
    }

    public Integer getPacientePesquisa() {
        return pacientePesquisa;
    }

    public void setPacientePesquisa(Integer pacientePesquisa) {
        this.pacientePesquisa = pacientePesquisa;
    }

    public String getNomePacientePesquisa() {
        return nomePacientePesquisa;
    }

    public void setNomePacientePesquisa(String nomePacientePesquisa) {
        this.nomePacientePesquisa = nomePacientePesquisa;
    }

    public Integer getParamIdCirurgia() {
        return paramIdCirurgia;
    }

    public void setParamIdCirurgia(Integer paramIdCirurgia) {
        this.paramIdCirurgia = paramIdCirurgia;
    }

    public String getParamServico() {
        return paramServico;
    }

    public void setParamServico(String paramServico) {
        this.paramServico = paramServico;
    }

    public Integer getParamPago() {
        return paramPago;
    }

    public void setParamPago(Integer paramPago) {
        this.paramPago = paramPago;
    }

    public ContaPaciente getContaPaciente() {
        return contaPaciente;
    }

    public void setContaPaciente(ContaPaciente contaPaciente) {
        this.contaPaciente = contaPaciente;
    }

    public AtendimentoPaciente getAtendimentoPaciente() {
        return atendimentoPaciente;
    }

    public void setAtendimentoPaciente(AtendimentoPaciente atendimentoPaciente) {
        this.atendimentoPaciente = atendimentoPaciente;
    }

    public List<AtendimentoPaciente> getListaAtendimentos() {
        return listaAtendimentos;
    }

    public void setListaAtendimentos(List<AtendimentoPaciente> listaAtendimentos) {
        this.listaAtendimentos = listaAtendimentos;
    }

    public LazyDataModel<AtendimentoPaciente> getListaAtendimentosLazy() {
        return listaAtendimentosLazy;
    }

    public void setListaAtendimentosLazy(LazyDataModel<AtendimentoPaciente> listaAtendimentosLazy) {
        this.listaAtendimentosLazy = listaAtendimentosLazy;
    }

    public LazyDataModel<Servico> getListaDeServicosLazy() {
        return listaDeServicosLazy;
    }

    public void setListaDeServicosLazy(LazyDataModel<Servico> listaDeServicosLazy) {
        this.listaDeServicosLazy = listaDeServicosLazy;
    }

    public RegraRepasse getRegraRepasse() {
        return regraRepasse;
    }

    public void setRegraRepasse(RegraRepasse regraRepasse) {
        this.regraRepasse = regraRepasse;
    }

    public List<ContaPaciente> getListaContasPacientePorAtendimento() {
        return listaContasPacientePorAtendimento;
    }

    public void setListaContasPacientePorAtendimento(List<ContaPaciente> listaContasPacientePorAtendimento) {
        this.listaContasPacientePorAtendimento = listaContasPacientePorAtendimento;
    }

    public Integer getParamTerceiroLaudo() {
        return paramTerceiroLaudo;
    }

    public void setParamTerceiroLaudo(Integer paramTerceiroLaudo) {
        this.paramTerceiroLaudo = paramTerceiroLaudo;
    }

    public Double getValorReciboMed() {
        return valorReciboMed;
    }

    public void setValorReciboMed(Double valorReciboMed) {
        this.valorReciboMed = valorReciboMed;
    }

    public Double getValorReciboHosp() {
        return valorReciboHosp;
    }

    public void setValorReciboHosp(Double valorReciboHosp) {
        this.valorReciboHosp = valorReciboHosp;
    }

    public Servico getServicoExterno() {
        return servicoExterno;
    }

    public void setServicoExterno(Servico servicoExterno) {
        this.servicoExterno = servicoExterno;
    }

    public Integer getNumeroAtendimento() {
        return numeroAtendimento;
    }

    public void setNumeroAtendimento(Integer numeroAtendimento) {
        this.numeroAtendimento = numeroAtendimento;
    }

    public Double getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(Double valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public Double getPercentualTotalPagamento() {
        return percentualTotalPagamento;
    }

    public void setPercentualTotalPagamento(Double percentualTotalPagamento) {
        this.percentualTotalPagamento = percentualTotalPagamento;
    }

    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public LazyDataModel<Procedimento> getModelProcedimento() {
        return modelProcedimento;
    }

    public void setModelProcedimento(LazyDataModel<Procedimento> modelProcedimento) {
        this.modelProcedimento = modelProcedimento;
    }

    public Boolean getDesabilitarAutomatico() {
        return desabilitarAutomatico;
    }

    public void setDesabilitarAutomatico(Boolean desabilitarAutomatico) {
        this.desabilitarAutomatico = desabilitarAutomatico;
    }

    public boolean isEmitirRecibo() {
        return emitirRecibo;
    }

    public void setEmitirRecibo(boolean emitirRecibo) {
        this.emitirRecibo = emitirRecibo;
    }

    public Boolean getRecibo() {
        return recibo;
    }

    public void setRecibo(Boolean recibo) {
        this.recibo = recibo;
    }

    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }

}
