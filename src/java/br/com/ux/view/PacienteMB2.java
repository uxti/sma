/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.PacienteEJB;
import br.com.ux.model.Paciente;
import br.com.ux.util.Mensagem;
import br.com.ux.util.ValidarCPF;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PacienteMB2 {

    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;

    public PacienteMB2() {
        novo();
    }

    public boolean validarNome() {
        if (paciente.getNome() == null) {
            Mensagem.addMensagem(3, "O campo não pode ficar em branco!");
            return false;
        } else {
            paciente.setNome(paciente.getNome().toUpperCase());
            Pattern pattern = Pattern.compile("[0-9]");
            Matcher matcher = pattern.matcher(paciente.getNome());
            if (matcher.find()) {
                Mensagem.addMensagem(3, "O Nome não deve conter números!");
                return false;
            } else {
                if (!paciente.getNome().contains(" ")) {
                    Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                    return false;
                } else {
                    if (paciente.getNome().length() < 2) {
                        Mensagem.addMensagem(3, "Verifique o nome informado. Min. 2 letras.");
                        return false;
                    } else {
                        String[] listaNomes = paciente.getNome().split(" ");
                        for (String t : listaNomes) {
                            if (t.length() < 2) {
                                Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                                return false;
                            }
                        }
                        if (listaNomes.length <= 1) {
                            Mensagem.addMensagem(3, "Verifique o nome informado. Ele deve conter Nome e Sobrenome do Paciente.");
                            return false;
                        } else {
                            return true;
                        }
                    }
                }
            }
        }
    }

    public void verificaCPF() {
        System.out.println(paciente.getCpf());
        if (paciente.getCpf() != null) {
            if (!paciente.getCpf().equals("___.___.___-__")) {
                System.out.println(paciente.getCpf());
                ValidarCPF vcpf = new ValidarCPF();
                if (vcpf.isCPF(paciente.getCpf())) {
                    String nome = pEJB.verificaCPFRetornaNome(paciente.getCpf());
                    if (!nome.equals("Inexistente")) {
                        Mensagem.addMensagem(3, "CPF já foi cadastrado para " + nome);
                    }
                } else {
                    Mensagem.addMensagem(3, "Atenção! O CPF informado não é válido!");
                }
            }
            RequestContext rc = RequestContext.getCurrentInstance();
            rc.execute(null);
        }
    }

    public void salvar() {
        if ((paciente.getCpf() == null || paciente.getCpf().equals("___.___.___-__")) && paciente.getPacienteResponsavelList().size() < 1) {
            Mensagem.addMensagem(3, "Informe um responsável para este paciente!");
        } else {
            if (paciente.getIdPaciente() == null) {
            } else {
            }
        }

    }

    public void novo() {
        paciente = new Paciente();
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
}
