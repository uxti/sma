/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.CirurgiaEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.OrcamentoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Orcamento;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import br.com.ux.util.ValoresTerceiros;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class ResumoPacienteMB implements Serializable {

    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;
    @EJB
    OrcamentoEJB oEJB;
    private Orcamento orcamento;
    private List<Orcamento> listaOrcamentos;
    @EJB
    CirurgiaEJB cEJB;
    private Cirurgia fechamento;
    private List<Cirurgia> listaFechamentos;
    @EJB 
    ContaCorrenteEJB ccEJB;
    @EJB
    RepasseEJB rEJB;
    @EJB
    TerceiroEJB tEJB;
    private UsuarioSessao us = new UsuarioSessao();
    private List<ContaCorrente> extratos = new ArrayList<ContaCorrente>();
    
    private Date dtInicio, dtFinal;
    private ArrayList<ValoresTerceiros> listVt;
    private double valorResumoRepasses;
    private double valorHonorariosHospitalares;
    private double valorHonorariosMedicos;

    public ResumoPacienteMB() {
        paciente = new Paciente();
        orcamento = new Orcamento();
        fechamento = new Cirurgia();
        listaFechamentos = new ArrayList<>();
        listaOrcamentos = new ArrayList<>();
    }

    public void gerarResumo() {
        pesquisarFechamentos();
    }
    
    public void pesquisarExtratos() {
        extratos = ccEJB.pesquisarContaCorrentePorPaciente(paciente.getIdPaciente());
        if (extratos.isEmpty()) {
            Mensagem.addMensagem(3, "Sem movimentação neste período");
        }
    }

    public void pesquisarOrcamentos() {
        listaOrcamentos = new ArrayList<>();
        listaOrcamentos = oEJB.listaOrcamentosPorPaciente(paciente.getIdPaciente());
        if (listaOrcamentos.isEmpty()) {
            System.out.println("Sem orçamentos.");
            Mensagem.addMensagem(3, "Não existe orçamentos para este paciente.");
        }
    }
    
    public void pesquisarFechamentos() {
        listaFechamentos = new ArrayList<>();
        listaFechamentos = cEJB.pesquisarAtendimentosPorPaciente(paciente.getIdPaciente());
        if (listaFechamentos.isEmpty()) {
            Mensagem.addMensagem(3, "Não existe movimentação deste paciente.");
        }
    }
    
    public void calcularRepassesAgrupados() {
        listVt = new ArrayList<>();
        List<Object[]> listRepasses = new ArrayList<>();
        listRepasses = rEJB.pesquisarRepasseAgrupadosPorCirurgia(fechamento.getIdCirurgia());
        valorResumoRepasses = 0d;
        valorHonorariosHospitalares = 0D;
        valorHonorariosMedicos = 0D;
        for (Object[] c : listRepasses) {
            ValoresTerceiros vt = new ValoresTerceiros();
            vt.setIdTerceiro((Integer) c[1]);
            vt.setTerceiro(tEJB.selecionarPorID(vt.getIdTerceiro(), us.retornaUsuario()).getNome() == null ? tEJB.selecionarPorID(vt.getIdTerceiro(), us.retornaUsuario()).getNomeFantasia() : tEJB.selecionarPorID(vt.getIdTerceiro(), us.retornaUsuario()).getNome());
            vt.setValor((Double) c[0]);
            listVt.add(vt);
        }
        for (ValoresTerceiros vt : listVt) {
            if (vt.getTerceiro().contains("HOSPITAL")) {
                valorHonorariosHospitalares += vt.getValor();
            } else {
                valorHonorariosMedicos += vt.getValor();
            }
            valorResumoRepasses += vt.getValor();
        }

    }
    
    public Double calcularValorTotal(List<Cheque> lista) {
        Double valorInfoCheques = 0D;
        if (lista != null) {
            for (Cheque chq : lista) {
                valorInfoCheques += chq.getValor();
            }
        }
        return valorInfoCheques;
    }

    public List<Paciente> completeMethodPaciente(String var) {
        return pEJB.autoCompletePaciente(var);
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public List<Orcamento> getListaOrcamentos() {
        return listaOrcamentos;
    }

    public void setListaOrcamentos(List<Orcamento> listaOrcamentos) {
        this.listaOrcamentos = listaOrcamentos;
    }

    public Date getDtInicio() {
        return dtInicio;
    }

    public void setDtInicio(Date dtInicio) {
        this.dtInicio = dtInicio;
    }

    public Date getDtFinal() {
        return dtFinal;
    }

    public void setDtFinal(Date dtFinal) {
        this.dtFinal = dtFinal;
    }

    public Orcamento getOrcamento() {
        return orcamento;
    }

    public void setOrcamento(Orcamento orcamento) {
        this.orcamento = orcamento;
    }

    public Cirurgia getFechamento() {
        return fechamento;
    }

    public void setFechamento(Cirurgia fechamento) {
        this.fechamento = fechamento;
        calcularRepassesAgrupados();
    }

    public List<Cirurgia> getListaFechamentos() {
        return listaFechamentos;
    }

    public void setListaFechamentos(List<Cirurgia> listaFechamentos) {
        this.listaFechamentos = listaFechamentos;
    }

    public List<ContaCorrente> getExtratos() {
        return extratos;
    }

    public void setExtratos(List<ContaCorrente> extratos) {
        this.extratos = extratos;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public ArrayList<ValoresTerceiros> getListVt() {
        return listVt;
    }

    public void setListVt(ArrayList<ValoresTerceiros> listVt) {
        this.listVt = listVt;
    }

    public double getValorResumoRepasses() {
        return valorResumoRepasses;
    }

    public void setValorResumoRepasses(double valorResumoRepasses) {
        this.valorResumoRepasses = valorResumoRepasses;
    }

    public double getValorHonorariosHospitalares() {
        return valorHonorariosHospitalares;
    }

    public void setValorHonorariosHospitalares(double valorHonorariosHospitalares) {
        this.valorHonorariosHospitalares = valorHonorariosHospitalares;
    }

    public double getValorHonorariosMedicos() {
        return valorHonorariosMedicos;
    }

    public void setValorHonorariosMedicos(double valorHonorariosMedicos) {
        this.valorHonorariosMedicos = valorHonorariosMedicos;
    }
    
    
    
    
    
}
