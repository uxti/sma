/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.BancoEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.FormaPagamentoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.RotinasAutomaticasEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Banco;
import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.FormaPagamento;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Terceiro;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.RelatorioFactory;
import br.com.ux.util.UsuarioSessao;
import br.com.ux.util.ValidarCNPJ;
import br.com.ux.util.ValidarCPF;
import static br.com.ux.view.MovimentoUsuarioMB.addMes;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author PokerFace
 */
@ManagedBean
@ViewScoped
public final class ChequeMB implements Serializable {

    @EJB
    ChequeEJB chequeEJB;
    private Cheque cheque;
    private Cheque chequeTrocar;
    UsuarioSessao us = new UsuarioSessao();
    private List<Cheque> listaCheque, listaNotasPromissorias;
    private List<Cheque> listaChequeSelecionados, listaNotasPromissoriasSelecionadas;

    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;
    private List<Paciente> pacientesAcarregar;
    @EJB
    BancoEJB bancoEJB;
    private Banco banco;
    @EJB
    TerceiroEJB terceiroEJB;
    private Terceiro terceiro;
    private List<Terceiro> terceirosAcarregar;
    private List<Repasse> repasses, repassesConsolidados;
    private Double valorTotal = 0D;
    private Double valorTotalCheques = 0D;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    RepasseEJB rEJB;
    @EJB
    CaixaEJB cEJB;
    private Caixa caixa;
    private Date dataAdiantamento;
    @EJB
    ContaCorrenteEJB ccEJB;
    private boolean lancando, CPFCPNJ, validacaoCGC = false;
    private Double valorTotalSelecionado, valorRepassado;
    @EJB
    FormaPagamentoEJB fEJB;
    @EJB
    AssociacaoEJB assEJB;

    @EJB
    AuditoriaEJB aEJB;
    @EJB
    CaixaEJB caixaEJB;
    public Repasse repasse;
    @EJB
    RotinasAutomaticasEJB rtEJB;

    private Calendar dataAtual, primeiroDia, ultimoDia;
    private Date PDtInicial, PDtFinal;
    public String PNumeroCheque;

    public ChequeMB() {
        novo();
    }

    public void novo() {
        cheque = new Cheque();
        repasse = new Repasse();
        repassesConsolidados = new ArrayList<>();
        paciente = new Paciente();
        banco = new Banco();
        terceiro = new Terceiro();
        cheque.setDtCompensacaoVencimento(new Date());
        cheque.setDtRecebimentoEmissao(new Date());
        listaChequeSelecionados = new ArrayList<>();
        pacientesAcarregar = new ArrayList<>();
        caixa = new Caixa();
        dataAdiantamento = new Date();
        chequeTrocar = new Cheque();
        lancando = false;
        PNumeroCheque = null;
        dataAtual = new GregorianCalendar();

    }

    public void atualizar() {
        try {
            movimentarCheque(cheque);
            chequeEJB.Atualizar(cheque, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("editar");
            novo();
            carregarCheques();
            BCRUtils.ResetarDatatableFiltros(":consultaCheques:dtl");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("editar");
            System.out.println(e);
        }
    }

    public void salvar() {
        try {
            if (paciente.getIdPaciente() != null) {
                cheque.setIdPaciente(paciente);
            }
            if (terceiro.getIdTerceiro() != null) {
                cheque.setIdTerceiro(terceiro);
            }
            if (banco.getIdBanco() != null) {
                cheque.setIdBanco(banco);
            }
            if (cheque.getMovimento() == null) {
                cheque.setMovimento("C");
            }

            chequeEJB.Salvar(cheque, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("salvar");
            if (cheque.getCompensado().equals("S")) {
                cheque.setDtCompensado(new Date());
                CompensarAvulso();

            }
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
        }
    }

    public void selecionarPaciente() {
        cheque.setEmitente(cheque.getIdPaciente().getNome());
        cheque.setCpf(cheque.getIdPaciente().getCpf());
    }

    public boolean validarCPF(String cpf) {
        ValidarCPF vCPF = new ValidarCPF();
        if (vCPF.isCPF(cpf)) {
            return true;
        } else {
            Mensagem.addMensagem(3, "CPF inválido");
            return false;
        }
    }

    public String chequeVencido(Cheque ch) {
        String resultado = "";
        if (ch != null) {
            if (ch.getCompensado().equals("N")) {
                if (ch.getDtCompensacaoVencimento().before(new Date()) || ch.getDtCompensacaoVencimento().equals(new Date())) {
                    resultado = "alert alert-danger";
                }
            } else {
                resultado = "";
            }
        }
        return resultado;
    }

    public void listagemCheques() {
        rtEJB.compensarChequesHospital();
    }

    public void salvarAvulso() {
        if (cheque.getIdCheque() != null) {
            Mensagem.addMensagem(1, "Cheque avulso lançado com sucesso!");
            carregarCheques();
        } else {
            try {
                cheque.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
                cheque.setTipo("N");
                cheque.setCompensado("N");
                cheque.setMovimento("C");
                cheque.setMovimento("C");
                cheque.setNumeroParcela(1);
                cheque.setAvulso(true);
                if (cheque.getObservacao() == null || cheque.getObservacao().equals("")) {
                    cheque.setObservacao("Cheque Avulso");
                }
                chequeEJB.Salvar(cheque, us.retornaUsuario());
                CompensarAvulso(cheque);
                Mensagem.addMensagem(1, "Cheque avulso lançado com sucesso!");
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Algo inesperado aconteceu! Tente novamente.");
                System.out.println(e);
            }
        }
    }

    public Double calcularValorTotal(List<Cheque> lista) {
        Double valorInfoCheques = 0D;
        if (lista != null) {
            for (Cheque chq : lista) {
                valorInfoCheques += chq.getValor();
            }
        }
        return valorInfoCheques;
    }

    public void carregarValoresRepasses() {
        salvarAvulso();
        repasse = new Repasse();
        repasses = new ArrayList<>();
        valorRepassado = 0D;
        if (!repassesConsolidados.isEmpty()) {
            repasses = repassesConsolidados;
            for (Repasse r : repasses) {
                valorRepassado = valorRepassado + r.getValorDocumento();
            }
        }
    }

    public void lancarRepasse() {
        FormaPagamento formaPagamento = fEJB.selecionarPorID(3, null);
        if (repasse.getValorDocumento().equals(0D) || repasse.getValorDocumento() == null) {
            Mensagem.addMensagem(3, "Valor informado do repasse inválido.");
        } else if (repasse.getValorDocumento() > cheque.getValor()) {
            Mensagem.addMensagem(3, "Valor informado é superior ao valor total do cheque.");
        } else if (repasse.getValorDocumento() > (cheque.getValor() - valorRepassado)) {
            Mensagem.addMensagem(3, "Valor informado é superior ao valor restante a ser repassado.");
        } else {
            try {
                repasse.setIdPaciente(cheque.getIdPaciente());
                repasse.setNumeroParcela(1);
                repasse.setIdCheque(cheque);
                repasse.setValor(0D);
                repasse.setDtLancamento(cheque.getDtRecebimentoEmissao());
                repasse.setDtRecebimento(cheque.getDtRecebimentoEmissao());
                repasse.setDtVencimento(cheque.getDtCompensacaoVencimento());
                repasse.setPercentualRecebido(100D);
                repasse.setIdTerceiro(repasse.getIdTerceiro());
                repasse.setStatus('F');
                repasse.setSituacaoPaciente('P');
                repasse.setSituacaoTerceiro('N');
                repasse.setTipoPagamento('P');
                repasse.setFormaPagamento("Cheque");
                repasse.setIdForma(formaPagamento);
                repasse.setValorRecebido(repasse.getValorDocumento());
                repasses.add(repasse);
                repassesConsolidados.add(repasse);

                rEJB.SalvarAvulsoComAuditoria(repasse);
                gerarContaCorrente(repasse.getIdTerceiro(), repasse, cheque);

                valorRepassado = valorRepassado + repasse.getValorDocumento();
                repasse = new Repasse();
                Mensagem.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        }
    }

    public void gerarContaCorrente(Terceiro terc, Repasse r, Cheque c) {
        ContaCorrente ccc = new ContaCorrente();
        ccc.setIdTerceiro(terc);
        ccc.setIdUsuario(uEJB.retornaUsuarioDaSessao());
        ccc.setIdCheque(c.getIdCheque());
        ccc.setTipo("C");
        ccc.setObservacao("Repasse referente ao cheque nº " + c.getNumeroCheque() + ". Valor total do cheque: " + c.getValor()
                + " .Paciente " + c.getIdPaciente().getNome());
        ccc.setDtLancamento(c.getDtRecebimentoEmissao());
        ccc.setSituacao("Não Liberado");
        ccc.setStatus("Não Retirado");
        ccc.setValor(r.getValorDocumento());
        ccc.setIdRepasse(r);
        ccc.setIdPaciente(r.getIdPaciente());
        ccEJB.atualizarContaCorrente(ccc);
    }

    public void excluirRepasse(Repasse rep) {
        try {
            rEJB.excluirRepasseEContasCorrente(rep);
            if (!repasses.isEmpty()) {
                repasses.remove(rep);
            }
            repassesConsolidados.remove(rep);
            valorRepassado = valorRepassado - rep.getValorDocumento();
            Mensagem.addMensagemPadraoSucesso("excluir");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
            System.out.println(e);
        }
    }

    public List<Paciente> completeMethodPaciente(String var) {
        return pEJB.autoCompletePaciente(var);
    }

    @PostConstruct
    public void carregarListaLancamentos() {
        PDtInicial = BCRUtils.primeiroDiaDoMes(BCRUtils.addMes(new Date(), -1));
        PDtFinal = BCRUtils.ultimoDiaDoMes(new Date());
        listaCheque = chequeEJB.listarChequePorMovimentoPeriodo("C", null, PDtInicial, PDtFinal);
        listaNotasPromissorias = chequeEJB.listarChequePorMovimentoPeriodo("N", null, PDtInicial, PDtFinal);
    }

    public void carregarCheques() {
        listaCheque = chequeEJB.listarCheque();
        listaNotasPromissorias = chequeEJB.listarNotasPromissorias();
    }

    public void pesquisarCheque() {
        listaCheque = chequeEJB.listarChequePorMovimentoPeriodo("C", null, PDtInicial, PDtFinal);
    }

    public void carregarChequesPorPeriodo(String numeroCheque, String movimento, Date dtInicial, Date dtFinal) {
        if (movimento.equals("C")) {
            listaCheque = new ArrayList<>();
            listaCheque = chequeEJB.listarChequePorMovimentoPeriodo("C", numeroCheque, dtInicial, dtFinal);
            if (listaCheque.isEmpty()) {
                Mensagem.addMensagem(1, "Não foram encontrados registros");
            }
        }

        if (movimento.equals("N")) {
            listaNotasPromissorias = new ArrayList<>();
            listaNotasPromissorias = chequeEJB.listarChequePorMovimentoPeriodo("N", numeroCheque, dtInicial, dtFinal);
            if (listaNotasPromissorias.isEmpty()) {
                Mensagem.addMensagem(1, "Não foram encontrados registros");
            }
        }

    }

    public void setarDatasPesquisa(String movimento, String periodo) {
        Date PDtIni = new Date();
        Date PDtFim = new Date();

        if (periodo.equals("D")) {
            PDtIni = dataAtual.getTime();
            PDtFim = dataAtual.getTime();
        }
        if (periodo.equals("M")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(dataAtual.getTime());
            PDtFim = BCRUtils.ultimoDiaDoMes(dataAtual.getTime());
        }
        if (periodo.equals("A")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual.getTime(), -1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual.getTime(), -1));
        }
        if (periodo.equals("P")) {
            PDtIni = BCRUtils.primeiroDiaDoMes(addMes(dataAtual.getTime(), 1));
            PDtFim = BCRUtils.ultimoDiaDoMes(addMes(dataAtual.getTime(), 1));
        }

        carregarChequesPorPeriodo(PNumeroCheque, movimento, PDtIni, PDtFim);

    }

    public Double calcularTotalCheques(List<Cheque> listaCh) {
        valorTotalCheques = 0D;
        for (Cheque ch : listaCh) {
            valorTotalCheques += ch.getValor();
        }
        return valorTotalCheques;
    }

    public void cancelar(Cheque c) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            c.setCancelado("S");
            c.setObservacao(c.getObservacao() + " - Cancelado em: " + sdf.format(new Date()));
            chequeEJB.Salvar(c, us.retornaUsuario());
            Mensagem.addMensagem(1, "Cheque cancelado com sucesso.");
            novo();
            carregarCheques();
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Não foi possível cancelar este cheque. Entre em contato com o suporte.");
            System.out.println(e);
        }
    }

    public void movimentarCheque(Cheque c) {
        try {
            if (c.getSituacao().equals("Representado") || c.getSituacao().equals("Jurídico") || c.getSituacao().equals("Devolvido")) {
                //devolverCheque();
                c.setCompensado("N");
            }

        } catch (Exception e) {
            Mensagem.addMensagem(3, "Não foi possível cancelar este cheque. Entre em contato com o suporte.");
            System.out.println(e);
        }
    }

    public String retornaSeEstaCancelado(Cheque c) {
        try {
            if (c.getCancelado().equals("S")) {
                return "cancelado";
            } else if (c.getCancelado().equals("T")) {
                return "trocado";
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }

    }

    public List<Cheque> listaChequesCancelados() {
        return chequeEJB.listarChequeCancelados();
    }

    public Cheque selecionarPorID(Integer id) {
        cheque = chequeEJB.selecionarPorID(id, us.retornaUsuario());
        if (cheque.getIdTerceiro() != null) {
            terceiro = cheque.getIdTerceiro();
        }
        if (cheque.getIdPaciente() != null) {
            paciente = cheque.getIdPaciente();
        }
        if (cheque.getIdBanco() != null) {
            banco = cheque.getIdBanco();
        }
        repassesConsolidados = cheque.getRepasseList();
        return cheque;
    }

    private void CompensarAvulso() {
        Usuario u = uEJB.selecionarPorUser(us.retornaUsuario());
        Date d = new Date();
        Long numAleatorio = d.getTime();
        caixa = new Caixa();
        caixa.setNumLote(numAleatorio.toString());
        caixa.setIdUsuario(u);
//                            caixa.setIdCaixaPai(getCaixaAtual());
        caixa.setDtCaixa(new Date());
        caixa.setTipoLancamento('C');
        caixa.setValor(cheque.getValor());
        caixa.setIdCheque(cheque);
        caixa.setStatus("Não Baixado");
        caixa.setHistorico("Cheque avulso compensado nº " + cheque.getIdCheque() + ". Paciente " + cheque.getIdPaciente().getIdPaciente() + ", " + cheque.getIdPaciente().getNome() + ". Recebido por " + us.retornaUsuario());
        cEJB.LancarRecebimentoNoCaixa(caixa);
    }

    private void CompensarAvulso(Cheque cheque) {
        Usuario u = uEJB.selecionarPorUser(us.retornaUsuario());
        Date d = new Date();
        Long numAleatorio = d.getTime();
        caixa = new Caixa();
        caixa.setNumLote(numAleatorio.toString());
        caixa.setIdUsuario(u);
//                            caixa.setIdCaixaPai(getCaixaAtual());
        caixa.setDtCaixa(new Date());
        caixa.setTipoLancamento('C');
        caixa.setValor(cheque.getValor());
        caixa.setIdCheque(cheque);
        caixa.setStatus("Não Baixado");
        caixa.setHistorico("Cheque avulso compensado nº " + cheque.getIdCheque() + ". Emitente " + cheque.getEmitente() + ". Recebido por " + us.retornaUsuario());
        cEJB.LancarRecebimentoNoCaixa(caixa);
    }

    public void compensar() {
//        List<Repasse> listaRepasses = cheque.getRepasseList();
//        Usuario u = uEJB.selecionarPorUser(us.retornaUsuario());
//        Date d = new Date();
//        if (listaRepasses.isEmpty()) {
//            CompensarAvulso();
//        }        

//        if (assEJB.carregarAssociacao().getCompensarRepassesHospitalAoCompensarCheque()) {
//            receberRepassesHospitalares(retornaRepassesHospitalares(cheque.getRepasseList()));
//        }
        compensarCheque(cheque);

        // Dar baixa nos repasses hospitalares.
        novo();
        carregarCheques();
        BCRUtils.ResetarDatatableFiltros(":consultaCheques:dtl");

    }

    public void receberRepassesHospitalaresAntigos() {
        for (Cheque c : chequeEJB.listarPagamento()) {
            List<Repasse> lista = new ArrayList<>();
            lista = rEJB.pesquisaRepasseHospitalaresPorCheque(c.getIdCheque());
            receberRepassesHospitalares(lista);
        }

    }

    public void receberRepassesHospitalares(List<Repasse> lista) {
        Usuario u = uEJB.retornaUsuarioDaSessao();
        Date d = new Date();
        try {
            for (Repasse rep : lista) {
                // Atualiza a conta corrente
                ContaCorrente cc = rep.getContaCorrenteList().get(0);
                cc.setSituacao("Liberado");
                cc.setStatus("Retirado");
                cc.setIdUsuario(u);
                cc.setDtRetirada(d);
                cc.setDtLiberacao(d);
                cc.setDtPagamentoMedico(d);
                ccEJB.SalvarSimples(cc, u.getUsuario());
                atualizarDados(rep.getContaCorrenteList());

                // Atualiza o repasse
                rep.setSacado("S");
                rep.setSituacaoTerceiro('L');

                rep.setDtRecebimento(d);
                rEJB.SalvarSimples(rep);
                System.out.println("ID: " + rep.getIdRepasse());
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar efetuar o pagamento!" + e);
            System.out.println(e);
        }

    }

    public void atualizarDados(List<ContaCorrente> ccs) {
        String data;
        DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        data = f.format(new Date());
        try {
            for (ContaCorrente corrente : ccs) {
                List<Caixa> listaCaixa = new ArrayList<Caixa>();
                listaCaixa = caixaEJB.pegaListaCaixaPorRepasse(corrente.getIdRepasse());
                for (Caixa c : listaCaixa) {
                    c.setStatus("Baixado");
                    c.setObservacao("Repassado em: " + data + ". Usuário: " + corrente.getIdUsuario().getUsuario());
                    caixaEJB.atualizarCaixa(c);
                }
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar efetuar o pagamento!" + e);
            System.out.println(e);
        }
    }

    public List<Repasse> retornaRepassesHospitalares(List<Repasse> listaRepasse) {
        List<Repasse> repasses = new ArrayList<>();
        for (Repasse repasse : listaRepasse) {
            if (repasse.getIdTerceiro().getTerceiroMedHosp()) {
                repasses.add(repasse);
            }
        }
        return repasses;
    }

    public void compensarSelecionados() {
        try {
            String mov = "";
            for (Cheque ch : listaChequeSelecionados) {
                mov = ch.getMovimento();
                chequeEJB.compensar(ch, us.retornaUsuario());
            }
            Mensagem.addMensagem(1, "Recebidos com sucesso");
            carregarCheques();
            novo();
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Ocorreu um erro ao tentar receber.");
            System.out.println(e);
        }
    }

    public void calcularTotalSelecionado() {
        valorTotalSelecionado = 0D;
        for (Cheque ch : listaChequeSelecionados) {
            valorTotalSelecionado += ch.getValor();
        }
        System.out.println(valorTotalSelecionado);
    }

    public boolean statusRetirado(Cheque c) {
        Number n = chequeEJB.verificarStatus(c, "Retirado", "Compensado");
        if (n.intValue() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificaCompensado(Cheque c) {
        String s = chequeEJB.verificarCompensado(c.getIdCheque());
        if (s == null) {
            return false;
        } else if (s.equals("S")) {
            return true;
        } else {
            return false;
        }
    }

    public void compensarCheque(Cheque c) {
        try {
            chequeEJB.compensar(c, us.retornaUsuario());
            Mensagem.addMensagem(1, "Cheque compensado com sucesso!");
            carregarCheques();
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar excluir!" + e);
        }
    }

    public void devolverCheque() {
        try {
            if (cheque.getIdCheque() != null) {
//                System.out.println(cheque.getDevolvido());
                chequeEJB.deletarChequeDoCaixa(cheque);
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar devolver!" + e);
            System.out.println(e);
        }
    }

    public void adiar() {
        try {
            if (cheque.getIdCheque() != null) {
                for (Repasse r : cheque.getRepasseList()) {
                    if (r.getIdCheque().getIdCheque().equals(cheque.getIdCheque())) {
                        r.setDtVencimento(cheque.getDtCompensacaoVencimento());
                        r.setObservacao(cheque.getObservacao());
                        rEJB.Salvar(r);
                    }
                    for (ContaCorrente cc : r.getContaCorrenteList()) {
                        cc.setDtLiberacao(cheque.getDtCompensacaoVencimento());
                        cc.setObservacao(cheque.getObservacao());
                        ccEJB.SalvarSimples(cc, us.retornaUsuario());
                    }
                }
                chequeEJB.Salvar(cheque, us.retornaUsuario());
                Mensagem.addMensagem(1, "Cheque e repasses adiados com sucesso!");
                novo();
                carregarCheques();
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar adiar!" + e);
            System.out.println(e);
        }
    }

    public void trocarCheque() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            if (chequeTrocar.getMovimento().equals("C")) {
                if (chequeTrocar.getDtCompensado().equals(new Date())) {
                    chequeTrocar.setCompensado("S");
                } else {
                    chequeTrocar.setCompensado("N");
                }
            } else {
                chequeTrocar.setIdBanco(null);
                chequeTrocar.setNumeroCheque(null);
                chequeTrocar.setConta(null);
                chequeTrocar.setCpf(null);
                chequeTrocar.setEmitente(null);
                chequeTrocar.setCompensado("S");
            }

            chequeEJB.Salvar(chequeTrocar, us.retornaUsuario());
            chequeEJB.trocarCheque(chequeTrocar, cheque.getIdCheque());
            if (cheque.getIdCheque() != null) {
                cheque.setCancelado("T");
                cheque.setRepasseList(null);
                cheque.setCaixaList(null);
                cheque.setObservacao("Cheque trocado em: " + df.format(new Date()) + ". Novo pagamento: " + chequeTrocar.getIdCheque() + "Usuário: " + us.retornaUsuario());
                chequeEJB.Salvar(cheque, us.retornaUsuario());
            }
            Mensagem.addMensagem(1, "Cheque trocado com sucesso.");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Não foi possível trocar o cheque com este lançamento. Entre em contato com o suporte.");
            System.out.println(e);
        }
    }

    public void validarCPF() {
        ValidarCPF vCPF = new ValidarCPF();
        ValidarCNPJ vCNPJ = new ValidarCNPJ();
        validacaoCGC = false;
        if ((!chequeTrocar.getCpf().equals("___.___.___-__")) || (!chequeTrocar.getCpf().equals("___.___.___/____-__"))) {
            if (CPFCPNJ) {
                validacaoCGC = vCNPJ.isCNPJ(chequeTrocar.getCpf());
            } else {
                validacaoCGC = vCPF.isCPF(chequeTrocar.getCpf());
            }
        }

    }

    public void setarCheque() {
        if (cheque != null) {
            chequeTrocar = new Cheque();
            chequeTrocar = cheque;
            if (chequeTrocar.getCpf() == null || cheque.getEmitente() == null) {
                if (chequeTrocar.getIdCirurgia().getIdResponsavel() != null) {
                    chequeTrocar.setEmitente(chequeTrocar.getIdCirurgia().getIdResponsavel().getNome());
                    chequeTrocar.setCpf(chequeTrocar.getIdCirurgia().getIdResponsavel().getCpf());
                } else {
                    chequeTrocar.setEmitente(chequeTrocar.getIdPaciente().getNome());
                    chequeTrocar.setCpf(chequeTrocar.getIdPaciente().getCpf());
                }
            }
            chequeTrocar.setIdCheque(null);
            chequeTrocar.setCancelado("N");
            chequeTrocar.setDtCompensacaoVencimento(new Date());
            chequeTrocar.setDtRecebimentoEmissao(new Date());
            lancando = true;
        } else {
            novo();
            carregarCheques();
        }

    }

    public void setarDatas() {
        if ((!cheque.getMovimento().equals("C"))) {
            chequeTrocar.setDtCompensacaoVencimento(new Date());
        }
    }

    public void mudarMascara() {
        if (CPFCPNJ) {
            CPFCPNJ = false;
        } else {
            CPFCPNJ = true;
        }
        chequeTrocar.setCpf(null);
        validacaoCGC = false;
    }

    public void selecionarPaciente(Paciente paciente1) {
        paciente = new Paciente();
        paciente = paciente1;
    }

    public List<Terceiro> listaTerceiros() {
        return terceiroEJB.listarTerceiro();
    }

    public Cheque getCheque() {
        return cheque;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public List<Cheque> getListaCheque() {
        return listaCheque;
    }

    public List<Banco> listaBancos() {
        return bancoEJB.listaBanco();
    }

    public void setListaCheque(List<Cheque> listaCheque) {
        this.listaCheque = listaCheque;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void carregarPacientes() {
        pacientesAcarregar = pEJB.listarPacientes();
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Terceiro getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(Terceiro terceiro) {
        this.terceiro = terceiro;
    }

    public List<Terceiro> getTerceirosAcarregar() {
        return terceirosAcarregar;
    }

    public void setTerceirosAcarregar(List<Terceiro> terceirosAcarregar) {
        this.terceirosAcarregar = terceirosAcarregar;
    }

    public List<Paciente> getPacientesAcarregar() {
        return pacientesAcarregar;
    }

    public void setPacientesAcarregar(List<Paciente> pacientesAcarregar) {
        this.pacientesAcarregar = pacientesAcarregar;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }
    Date dt_ini_emissao;
    Date dt_fim_emissao = new Date();
    Date dt_ini_venc;
    Date dt_fim_venc = new Date();
    String id_paciente, id_terceiro, tipo, id_banco, compensado;

    public void imprimirChques() {

        RelatorioFactory.listagemCheques("/WEB-INF/Relatorios/reportListagemCheque.jasper", us.retornaUsuario(), null, null, null, null, null, null, null, null, null, "C://SMA//logo.png");
    }

    public Date getDt_ini_emissao() {
        return dt_ini_emissao;
    }

    public void setDt_ini_emissao(Date dt_ini_emissao) {
        this.dt_ini_emissao = dt_ini_emissao;
    }

    public Date getDt_fim_emissao() {
        return dt_fim_emissao;
    }

    public void setDt_fim_emissao(Date dt_fim_emissao) {
        this.dt_fim_emissao = dt_fim_emissao;
    }

    public Date getDt_ini_venc() {
        return dt_ini_venc;
    }

    public void setDt_ini_venc(Date dt_ini_venc) {
        this.dt_ini_venc = dt_ini_venc;
    }

    public Date getDt_fim_venc() {
        return dt_fim_venc;
    }

    public void setDt_fim_venc(Date dt_fim_venc) {
        this.dt_fim_venc = dt_fim_venc;
    }

    public String getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(String id_paciente) {
        this.id_paciente = id_paciente;
    }

    public String getId_terceiro() {
        return id_terceiro;
    }

    public void setId_terceiro(String id_terceiro) {
        this.id_terceiro = id_terceiro;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getId_banco() {
        return id_banco;
    }

    public void setId_banco(String id_banco) {
        this.id_banco = id_banco;
    }

    public String getCompensado() {
        return compensado;
    }

    public void setCompensado(String compensado) {
        this.compensado = compensado;
    }

    public Cheque getChequeTrocar() {
        return chequeTrocar;
    }

    public void setChequeTrocar(Cheque chequeTrocar) {
        this.chequeTrocar = chequeTrocar;
    }

    public boolean isLancando() {
        return lancando;
    }

    public void setLancando(boolean lancando) {
        this.lancando = lancando;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public Date getDataAdiantamento() {
        return dataAdiantamento;
    }

    public void setDataAdiantamento(Date dataAdiantamento) {
        this.dataAdiantamento = dataAdiantamento;
    }

    public boolean isCPFCPNJ() {
        return CPFCPNJ;
    }

    public void setCPFCPNJ(boolean CPFCPNJ) {
        this.CPFCPNJ = CPFCPNJ;
    }

    public boolean isValidacaoCGC() {
        return validacaoCGC;
    }

    public void setValidacaoCGC(boolean validacaoCGC) {
        this.validacaoCGC = validacaoCGC;
    }

    public List<Cheque> getListaChequeSelecionados() {
        return listaChequeSelecionados;
    }

    public void setListaChequeSelecionados(List<Cheque> listaChequeSelecionados) {
        this.listaChequeSelecionados = listaChequeSelecionados;
    }

    public Double getValorTotalSelecionado() {
        return valorTotalSelecionado;
    }

    public void setValorTotalSelecionado(Double valorTotalSelecionado) {
        this.valorTotalSelecionado = valorTotalSelecionado;
    }

    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Cheque ", ((Cheque) event.getObject()).getIdCheque().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Cheque ", ((Cheque) event.getObject()).getIdCheque().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public List<Repasse> getRepasses() {
        return repasses;
    }

    public void setRepasses(List<Repasse> repasses) {
        this.repasses = repasses;
    }

    public Double getValorRepassado() {
        return valorRepassado;
    }

    public void setValorRepassado(Double valorRepassado) {
        this.valorRepassado = valorRepassado;
    }

    public Repasse getRepasse() {
        return repasse;
    }

    public void setRepasse(Repasse repasse) {
        this.repasse = repasse;
    }

    public List<Repasse> getRepassesConsolidados() {
        return repassesConsolidados;
    }

    public void setRepassesConsolidados(List<Repasse> repassesConsolidados) {
        this.repassesConsolidados = repassesConsolidados;
    }

    public Double getValorTotalCheques() {
        return valorTotalCheques;
    }

    public void setValorTotalCheques(Double valorTotalCheques) {
        this.valorTotalCheques = valorTotalCheques;
    }

    public List<Cheque> getListaNotasPromissorias() {
        return listaNotasPromissorias;
    }

    public void setListaNotasPromissorias(List<Cheque> listaNotasPromissorias) {
        this.listaNotasPromissorias = listaNotasPromissorias;
    }

    public List<Cheque> getListaNotasPromissoriasSelecionadas() {
        return listaNotasPromissoriasSelecionadas;
    }

    public void setListaNotasPromissoriasSelecionadas(List<Cheque> listaNotasPromissoriasSelecionadas) {
        this.listaNotasPromissoriasSelecionadas = listaNotasPromissoriasSelecionadas;
    }

    public Calendar getDataAtual() {
        return dataAtual;
    }

    public void setDataAtual(Calendar dataAtual) {
        this.dataAtual = dataAtual;
    }

    public Calendar getPrimeiroDia() {
        return primeiroDia;
    }

    public void setPrimeiroDia(Calendar primeiroDia) {
        this.primeiroDia = primeiroDia;
    }

    public Calendar getUltimoDia() {
        return ultimoDia;
    }

    public void setUltimoDia(Calendar ultimoDia) {
        this.ultimoDia = ultimoDia;
    }

    public Date getPDtInicial() {
        return PDtInicial;
    }

    public void setPDtInicial(Date PDtInicial) {
        this.PDtInicial = PDtInicial;
    }

    public Date getPDtFinal() {
        return PDtFinal;
    }

    public void setPDtFinal(Date PDtFinal) {
        this.PDtFinal = PDtFinal;
    }

    public String getPNumeroCheque() {
        return PNumeroCheque;
    }

    public void setPNumeroCheque(String PNumeroCheque) {
        this.PNumeroCheque = PNumeroCheque;
    }

}
