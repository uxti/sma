/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.CirurgiaEJB;
import br.com.ux.controller.OrcamentoEJB;
import br.com.ux.pojo.Grafico;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.RelatorioFactory;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DonutChartModel;
import org.primefaces.model.chart.LegendPlacement;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author BrunoS
 */
@ManagedBean
@ViewScoped
public class GraficoOrcamentoMB implements Serializable {

    @EJB
    OrcamentoEJB oEJB;
    private Calendar dataAtual, primeiroDia, ultimoDia;
    private Date PDtIni, PDtFim;
    private int totalOrcamento, totalOrcamentoRealizados;
    private Double percentual, maxValor;
    private PieChartModel pieModel2, pieModel3;
    private DonutChartModel graficoDonut;
    private String tipo;
    private BarChartModel graficoBarra;
    @EJB
    CirurgiaEJB cEJB;

    public GraficoOrcamentoMB() {
        dataAtual = Calendar.getInstance();
        primeiroDia = Calendar.getInstance();
        ultimoDia = Calendar.getInstance();
        PDtIni = BCRUtils.primeiroDiaDoMes(dataAtual.getTime());
        PDtFim = BCRUtils.ultimoDiaDoMes(dataAtual.getTime());
        totalOrcamento = 0;
        tipo = "M";
        maxValor = 0D;
        percentual = 0D;
    }

    @PostConstruct
    public void init() {
        createPieModels();
    }

    public PieChartModel getPieModel2() {
        return pieModel2;
    }

    private void createPieModels() {
        //criarGrafico();
        gerarGraficoOrcamentoPorCompetencia();
        createBarModel();
        criarGraficoOrcamentos();
    }

    private void gerarGraficoOrcamentoPorCompetencia() {
        pieModel2 = new PieChartModel();
        totalOrcamento = 0;

        List<Grafico> list = cEJB.verificarOrcadoXRealizadoPorPeriodo(PDtIni, PDtFim);
        if (list.isEmpty()) {
            pieModel2.set("0", 0);
            totalOrcamento = 0;
        } else {
            for (Grafico grafico : list) {
                pieModel2.set(grafico.getPeriodo() + " - " + grafico.getQuantidade(), grafico.getQuantidade());
                totalOrcamento += grafico.getQuantidade();
            }
        }
        pieModel2.setTitle("Mês de criação dos orçamentos");
        pieModel2.setLegendPosition("w");
        pieModel2.setLegendPlacement(LegendPlacement.INSIDE);
        pieModel2.setFill(true);
        pieModel2.setShowDataLabels(true);
    }

//    private void criarGrafico() {
//        graficoDonut = new DonutChartModel();
//        Map<String, Number> circle1 = new LinkedHashMap<String, Number>();
//        Number liberados = (Number) oEJB.contarRegistroParametroStatus("Liberado", PDtIni, PDtFim);
//        Number naoLiberados = (Number) oEJB.contarRegistroParametroStatus("Não Liberado", PDtIni, PDtFim);
//        //totalOrcamento = (Number) oEJB.contarRegistroParametro("%", PDtIni, PDtFim);
//        circle1.put("Liberados:" + liberados, liberados);
//        circle1.put("Não Liberados: " + naoLiberados, naoLiberados);
//        graficoDonut.addCircle(circle1);
//        graficoDonut.setShowDataLabels(true);
//        graficoDonut.setLegendPlacement(LegendPlacement.INSIDE);
//        graficoDonut.setTitle("Por Status.");
//        graficoDonut.setLegendPosition("w");
//        //graficoDonut.setLegendPlacement(LegendPlacement.OUTSIDE);
//    }
    public void setarDatas(String periodo) {
        if (periodo.equals("D")) {
            PDtIni = dataAtual.getTime();
            PDtFim = dataAtual.getTime();
            tipo = "D";
        } else if (periodo.equals("S")) {
            tipo = "S";
        } else {
            PDtIni = BCRUtils.primeiroDiaDoMes(dataAtual.getTime());
            PDtFim = BCRUtils.ultimoDiaDoMes(dataAtual.getTime());
            tipo = "M";
        }
        createPieModels();

    }

    private BarChartModel gerarGraficoOrcadoRealizado() {
        BarChartModel model = new BarChartModel();
        List<Grafico> list = cEJB.verificarOrcadoXRealizado(PDtIni, PDtFim);
        Double totalOrcamentos = 0D;
        Double totalFechamentos = 0D;
        maxValor = 0D;
        percentual = 0D;

        ChartSeries orcamentos = new ChartSeries();
        orcamentos.setLabel("Orçamentos - R$ " + BCRUtils.arredondar(totalOrcamentos));
        if (list.isEmpty()) {
            orcamentos.set("0", 0);
            totalOrcamentos = 0D;
        } else {
            for (Grafico grafico : list) {
                orcamentos.set(grafico.getPeriodo(), grafico.getValorOrcado());
                totalOrcamentos += grafico.getValorOrcado();
            }
        }

        ChartSeries fechamentos = new ChartSeries();
        fechamentos.setLabel("Fechamentos - R$ " + BCRUtils.arredondar(totalFechamentos));

        if (list.isEmpty()) {
            fechamentos.set("0", 0);
            totalFechamentos = 0D;
            percentual = 0D;
            maxValor = 0D;
        } else {
            for (Grafico grafico : list) {
                fechamentos.set(grafico.getPeriodo(), grafico.getValorFechamento());
                totalFechamentos += grafico.getValorFechamento();

                percentual += grafico.getPercentual();
                maxValor += grafico.getValorFechamento();
            }
        }

        if (list.size() > 1) {
            maxValor = (maxValor / 2);
            percentual = BCRUtils.arredondar(((totalFechamentos * 100) / totalOrcamentos) - 100);
        } else {
            percentual = BCRUtils.arredondar(percentual);
        }
        model.addSeries(orcamentos);
        model.addSeries(fechamentos);

        return model;
    }

    private void createBarModel() {
        graficoBarra = gerarGraficoOrcadoRealizado();
        graficoBarra.setTitle("Valor Orçado x Valor Realizado: " + percentual + "%");

        graficoBarra.setLegendPosition("ne");
        graficoBarra.setMouseoverHighlight(true);
        graficoBarra.setShowDatatip(true);
        graficoBarra.setAnimate(true);

        Axis yAxis = graficoBarra.getAxis(AxisType.Y);
        yAxis.setLabel("Valor Total");
        yAxis.setMin(0);

        if (maxValor.intValue() < 500000) {
            maxValor = 500000D;
        } else if (maxValor.intValue() < 1000000) {
            maxValor = 1000000D;
        } else if (maxValor.intValue() < 1500000) {
            maxValor = 1500000D;
        } else if (maxValor.intValue() < 2000000) {
            maxValor = 2000000D;
        } else {
            maxValor = maxValor * 3;
        }
        yAxis.setMax(maxValor);
    }

    private void criarGraficoOrcamentos() {
        pieModel3 = new PieChartModel();
        totalOrcamentoRealizados = 0;
        Double totalOrcamentosRealizados = 0D;

        List<Grafico> list = oEJB.verificarOrcamentosPorPeriodo(PDtIni, PDtFim);
        if (list.isEmpty()) {
            pieModel3.set("0", 0);
            totalOrcamentosRealizados = 0D;
            totalOrcamentoRealizados = 0;
        } else {
            for (Grafico grafico : list) {
                pieModel3.set(grafico.getDescricao() + " - " + grafico.getQuantidade() + " - R$ " + String.format("%.2f", grafico.getValorOrcado()), grafico.getQuantidade());
                totalOrcamentosRealizados += grafico.getValorOrcado();
                totalOrcamentoRealizados += grafico.getQuantidade();
            }
        }
        DecimalFormat formato = new DecimalFormat("#.##");
        pieModel3.setTitle("Orçamentos lançados: " + totalOrcamentoRealizados + " - Total: R$ " + String.format("%.2f", totalOrcamentosRealizados));
        pieModel3.setLegendPosition("w");
        pieModel3.setLegendPlacement(LegendPlacement.INSIDE);
        pieModel3.setFill(true);
        pieModel3.setShowDataLabels(true);
    }

    public void itemSelect(ItemSelectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
                "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public void imprimir() {
        RelatorioFactory.RelatorioDashboard("/WEB-INF/Relatorios/reportDashboard.jasper", "C://SMA//logo.png", PDtIni, PDtFim);
    }

    public Calendar getDataAtual() {
        return dataAtual;
    }

    public void setDataAtual(Calendar dataAtual) {
        this.dataAtual = dataAtual;
    }

    public Calendar getPrimeiroDia() {
        return primeiroDia;
    }

    public void setPrimeiroDia(Calendar primeiroDia) {
        this.primeiroDia = primeiroDia;
    }

    public Calendar getUltimoDia() {
        return ultimoDia;
    }

    public void setUltimoDia(Calendar ultimoDia) {
        this.ultimoDia = ultimoDia;
    }

    public Date getPDtIni() {
        return PDtIni;
    }

    public void setPDtIni(Date PDtIni) {
        this.PDtIni = PDtIni;
    }

    public Date getPDtFim() {
        return PDtFim;
    }

    public void setPDtFim(Date PDtFim) {
        this.PDtFim = PDtFim;
    }

    public DonutChartModel getGraficoDonut() {
        return graficoDonut;
    }

    public void setGraficoDonut(DonutChartModel graficoDonut) {
        this.graficoDonut = graficoDonut;
    }

    public int getTotalOrcamento() {
        return totalOrcamento;
    }

    public void setTotalOrcamento(int totalOrcamento) {
        this.totalOrcamento = totalOrcamento;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public BarChartModel getGraficoBarra() {
        return graficoBarra;
    }

    public void setGraficoBarra(BarChartModel graficoBarra) {
        this.graficoBarra = graficoBarra;
    }

    public Double getPercentual() {
        return percentual;
    }

    public void setPercentual(Double percentual) {
        this.percentual = percentual;
    }

    public Double getMaxValor() {
        return maxValor;
    }

    public void setMaxValor(Double maxValor) {
        this.maxValor = maxValor;
    }

    public PieChartModel getPieModel3() {
        return pieModel3;
    }

    public void setPieModel3(PieChartModel pieModel3) {
        this.pieModel3 = pieModel3;
    }

    public int getTotalOrcamentoRealizados() {
        return totalOrcamentoRealizados;
    }

    public void setTotalOrcamentoRealizados(int totalOrcamentoRealizados) {
        this.totalOrcamentoRealizados = totalOrcamentoRealizados;
    }
}
