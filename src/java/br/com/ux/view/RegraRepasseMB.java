/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AreaProcedimentoEJB;
import br.com.ux.controller.CBOSaudeEJB;
import br.com.ux.controller.EspecialidadeProcedimentoEJB;
import br.com.ux.controller.GrupoProcedimentoEJB;
import br.com.ux.controller.RegraRepasseEJB;
import br.com.ux.controller.ServicoEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.model.CboSaude;
import br.com.ux.model.RegraRepasse;
import br.com.ux.model.RegraRepasseMedicos;
import br.com.ux.model.Servico;
import br.com.ux.model.Terceiro;
import br.com.ux.model.tasy.AreaProcedimento;
import br.com.ux.model.tasy.EspecialidadeProcedimento;
import br.com.ux.model.tasy.GrupoProcedimentoCbhpm;
import br.com.ux.model.tasy.ProcedimentoPaciente;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class RegraRepasseMB implements Serializable {

    @EJB
    RegraRepasseEJB rrEJB;
    private RegraRepasse regraRepasse;
    private RegraRepasseMedicos regraRepasseMedicos;
    private List<RegraRepasseMedicos> listaMedicosSecundarios;

    @EJB
    TerceiroEJB tEJB;
    @EJB
    AreaProcedimentoEJB apEJB;
    @EJB
    EspecialidadeProcedimentoEJB epEJB;
    @EJB
    GrupoProcedimentoEJB gpEJB;
    @EJB
    ServicoEJB sEJB;
    @EJB
    CBOSaudeEJB csEJB;

    private List<RegraRepasse> listaRegraRepasses;

    private Double valorTotalAtend;
    private Double valorRepasseMedico;
    private Double valorRepasseHospital;

    UsuarioSessao us = new UsuarioSessao();

    public RegraRepasseMB() {
        novo();
    }

    public void novo() {
        regraRepasse = new RegraRepasse();
        regraRepasse.setAtivo(Boolean.TRUE);
        listaRegraRepasses = new ArrayList<>();

        regraRepasseMedicos = new RegraRepasseMedicos();
        listaMedicosSecundarios = new ArrayList<>();
    }

    public void salvar(RegraRepasse rr) {
        try {
            if (rr.getIdRegraRepasse() == null) {
                rr.setDtCadastro(new Date());
                rr.setDtAtualizacao(new Date());
                if (listaMedicosSecundarios.isEmpty()) {
                    regraRepasse.setRepasseSecundario(Boolean.FALSE);
                }
                rrEJB.Salvar(rr, us.retornaUsuario());

                if (!listaMedicosSecundarios.isEmpty()) {
                    for (RegraRepasseMedicos rrm : listaMedicosSecundarios) {
                        rrm.setIdRegraRepasse(regraRepasse);
                        rrEJB.SalvarRepasseSecundario(rrm);
                    }
                }
                //novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
            } else {
                if (listaMedicosSecundarios.isEmpty()) {
                    regraRepasse.setRepasseSecundario(Boolean.FALSE);
                }
                rr.setDtAtualizacao(new Date());
                rrEJB.Atualizar(rr, us.retornaUsuario());
                //novo();

                if (!listaMedicosSecundarios.isEmpty()) {
                    // Primeiro, exclui todos os antigos registros
                    for (RegraRepasseMedicos rrm : rrEJB.listarMedicosSecundariosPorRegraRepasse(regraRepasse.getIdRegraRepasse())) {
                        rrEJB.ExcluirMedicoSecundario(rrm);
                    }

                    for (RegraRepasseMedicos rrm : listaMedicosSecundarios) {
                        rrm.setIdRegraRepasse(regraRepasse);
                        rrEJB.SalvarRepasseSecundario(rrm);
                    }
                }
                Mensagem.addMensagemPadraoSucesso("editar");
            }
            listarRegraRepasse();
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    @PostConstruct
    public void listarRegraRepasse() {
        listaRegraRepasses = rrEJB.listarRegraRepasses();
    }

    public List<RegraRepasse> listaRegraRepasses() {
        return rrEJB.listarRegraRepasses();
    }

    public void excluir(RegraRepasse rr) {
        try {
            if (rr.getRepasseSecundario()) {
                for (RegraRepasseMedicos rrm : rr.getRegraRepasseMedicosList()) {
                    rrEJB.ExcluirMedicoSecundario(rrm);
                }
            }
            rrEJB.Excluir(rr, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("excluir");
            novo();
            listarRegraRepasse();
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Este cadastro já foi utilizado e não pode ser excluído. Se quiser desabilitar, clique em Editar e desmarque a opção 'Ativo'.");
        }
    }

    public void copiarRegra(RegraRepasse rr) {
        try {
            rr.setIdRegraRepasse(null);
            rr.setRepasseSecundario(Boolean.FALSE);
            rr.setIdTerceiroSecundario(null);
            rr.setFatorTerceiroSecundario(null);
            rr.setDtCadastro(new Date());
            rr.setDtAtualizacao(new Date());
            regraRepasse = rr;
            if (regraRepasse.getRegraRepasseMedicosList() != null) {
                listaMedicosSecundarios = regraRepasse.getRegraRepasseMedicosList();
            }
            Mensagem.addMensagem(1, "Regra duplicada com sucesso.");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao copiar regra.");
        }
    }

    public void testarRegraRepasse(ProcedimentoPaciente pp) {
        Double valorAjustado = 0D;
        regraRepasse = rrEJB.retornaRegraRepasse(pp.getIdServico(), pp.getIdMedico(), pp.getIdContaPaciente().getIdAtendimentoPaciente().getCategoria(), false);
        if (regraRepasse != null) {
            valorAjustado = realizarCalculoTaxaRegraRepasse(regraRepasse, pp.getValorProcedimento());
        } else {
            System.out.println("Não encontrou nada para " + pp.getIdServico().getIdServico());
        }
    }

    public void zerarTesteRegra() {
        valorRepasseMedico = 0D;
        valorTotalAtend = 0D;
        valorRepasseHospital = 0D;
    }

    public void testarRegra() {
        valorRepasseMedico = testarCalculoTaxaRegraRepasse(regraRepasse, valorTotalAtend);
        valorRepasseHospital = valorTotalAtend - valorRepasseMedico;
    }

    public Double realizarCalculoTaxaRegraRepasse(RegraRepasse rr, Double valorASerAjustado) {
        Double valorReal = 0D;
        if (rr != null) {
            Double taxa = regraRepasse.getFatorProcedimento();
            valorReal = (valorASerAjustado * taxa) / (100 - taxa);
            System.out.println(regraRepasse.getDescricao() + " - Resultado: " + valorReal);
        }
        return valorReal;
    }

    public Double testarCalculoTaxaRegraRepasse(RegraRepasse rr, Double valorTotalAtendimentoExterno) {
        Double valorSMA = 0D;
        if (rr != null) {
            Double taxa = regraRepasse.getFatorProcedimento();
            valorSMA = (valorTotalAtendimentoExterno * taxa) / 100;
        }
        return valorSMA;
    }

    public void adicionarMedicoSecundario() {
        listaMedicosSecundarios.add(regraRepasseMedicos);
        regraRepasseMedicos = new RegraRepasseMedicos();
        Mensagem.addMensagemGrowl(1, "Médico adicionado com sucesso!");
    }

    public void excluirMedicoSecundario(RegraRepasseMedicos rm) {
        int indice = 0;
        List<RegraRepasseMedicos> listaTemporaria;
        listaTemporaria = listaMedicosSecundarios;
        for (RegraRepasseMedicos rpm : listaTemporaria) {
            if (rpm == rm) {
                listaMedicosSecundarios.remove(indice);
                if (regraRepasse.getIdRegraRepasse() != null) {
                    rrEJB.ExcluirMedicoSecundario(rpm);
                }
                break;
            } else {
                indice++;
            }
        }
        Mensagem.addMensagemGrowl(1, "Médico removido com sucesso!");
    }

    public RegraRepasse retornaRegraRepasse(Servico s, Terceiro t, String categoriaConvenio, Boolean urgencia) {
        return rrEJB.retornaRegraRepasse(s, t, categoriaConvenio, urgencia);
    }

    public List<Terceiro> completeMethodTerceiro(String var) {
        return tEJB.autoCompleteTerceiro(var);
    }

    public List<AreaProcedimento> completeMethodAreaProcedimento(String var) {
        return apEJB.autoCompleteAreaProcedimento(var);
    }

    public List<EspecialidadeProcedimento> completeMethodEspecialidadeProcedimento(String var) {
        return epEJB.autoCompleteEspecialidadeProcedimento(var);
    }

    public List<GrupoProcedimentoCbhpm> completeMethodGrupoProcedimento(String var) {
        return gpEJB.autoCompleteGrupoProcedimento(var);
    }

    public List<CboSaude> completeMethodCBOSaude(String var) {
        return csEJB.autoCompleteMethodCBOSaude(var);
    }

    public List<Servico> completeMethodProcedimentosCbhpm(String var) {
        return sEJB.autoCompleteProcedimentoCbhpm(var);
    }

    public void zerarMedicoSecundario() {
        if (regraRepasse.getIdMedico() != null) {
            regraRepasse.setIdTerceiroSecundario(null);
            regraRepasse.setFatorTerceiroSecundario(null);
        }
    }

    public RegraRepasse selecionarPorID(Integer id) {
        return rrEJB.selecionarPorID(id);
    }

    public void selecionarRegraRepasse(RegraRepasse rr) {
        regraRepasse = rr;
        listaMedicosSecundarios = rr.getRegraRepasseMedicosList();
    }

    public RegraRepasse getRegraRepasse() {
        return regraRepasse;
    }

    public void setRegraRepasse(RegraRepasse regraRepasse) {
        this.regraRepasse = regraRepasse;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public RegraRepasseMedicos getRegraRepasseMedicos() {
        return regraRepasseMedicos;
    }

    public void setRegraRepasseMedicos(RegraRepasseMedicos regraRepasseMedicos) {
        this.regraRepasseMedicos = regraRepasseMedicos;
    }

    public List<RegraRepasseMedicos> getListaMedicosSecundarios() {
        return listaMedicosSecundarios;
    }

    public void setListaMedicosSecundarios(List<RegraRepasseMedicos> listaMedicosSecundarios) {
        this.listaMedicosSecundarios = listaMedicosSecundarios;
    }

    public Double getValorTotalAtend() {
        return valorTotalAtend;
    }

    public void setValorTotalAtend(Double valorTotalAtend) {
        this.valorTotalAtend = valorTotalAtend;
    }

    public Double getValorRepasseMedico() {
        return valorRepasseMedico;
    }

    public void setValorRepasseMedico(Double valorRepasseMedico) {
        this.valorRepasseMedico = valorRepasseMedico;
    }

    public Double getValorRepasseHospital() {
        return valorRepasseHospital;
    }

    public void setValorRepasseHospital(Double valorRepasseHospital) {
        this.valorRepasseHospital = valorRepasseHospital;
    }

    public List<RegraRepasse> getListaRegraRepasses() {
        return listaRegraRepasses;
    }

    public void setListaRegraRepasses(List<RegraRepasse> listaRegraRepasses) {
        this.listaRegraRepasses = listaRegraRepasses;
    }

}
