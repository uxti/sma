/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.UnidadeAtendimentoEJB;
import br.com.ux.model.UnidadeAtendimento;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author BrunoS
 */
@ManagedBean
@ViewScoped
public class UnidadeAtendimentoMB implements Serializable {

    @EJB
    UnidadeAtendimentoEJB undEJB;
    UnidadeAtendimento unidadeAtendimento;
    List<UnidadeAtendimento> listaaCarregar = new ArrayList<UnidadeAtendimento>();
    List<UnidadeAtendimento> listaaCarregarLazy;
    LazyDataModel<UnidadeAtendimento> model;
    UsuarioSessao us = new UsuarioSessao();

    public UnidadeAtendimentoMB() {
        novo();
        carregaModelLazy();
    }

    public void novo() {
        unidadeAtendimento = new UnidadeAtendimento();
    }

    public void salvar() {
        if (unidadeAtendimento.getIdUnidade() == null) {
            try {
                undEJB.Salvar(unidadeAtendimento, us.retornaUsuario());
                novo();;
                Mensagem.addMensagem(1, "Salvo com sucesso!");
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Erro ao tentar salvar!");
            }

        } else {
            try {
                undEJB.Salvar(unidadeAtendimento, us.retornaUsuario());
                novo();;
                Mensagem.addMensagem(1, "Editado com sucesso!");
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Erro ao tentar editar!");
            }
        }
    }

    public void excluir() {
        try {
            undEJB.Excluir(unidadeAtendimento, us.retornaUsuario());
            novo();
            Mensagem.addMensagem(1, "Excluido com sucesso!");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar excluir!");
        }
    }

    public UnidadeAtendimento selecionarporid(Integer id) {
        return unidadeAtendimento = undEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public List<UnidadeAtendimento> listaUnidadesAtendimento() {
        return undEJB.listarUnidadesAtendimento();
    }

    public void carregarLista() {
        listaaCarregar = undEJB.listarUnidadesAtendimento();
    }

    //==========================================================================
    public UnidadeAtendimentoEJB getUndEJB() {
        return undEJB;
    }

    public void setUndEJB(UnidadeAtendimentoEJB undEJB) {
        this.undEJB = undEJB;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public List<UnidadeAtendimento> getListaaCarregar() {
        return listaaCarregar;
    }

    public void setListaaCarregar(List<UnidadeAtendimento> listaaCarregar) {
        this.listaaCarregar = listaaCarregar;
    }

    public List<UnidadeAtendimento> getListaaCarregarLazy() {
        return listaaCarregarLazy;
    }

    public void setListaaCarregarLazy(List<UnidadeAtendimento> listaaCarregarLazy) {
        this.listaaCarregarLazy = listaaCarregarLazy;
    }

    public LazyDataModel<UnidadeAtendimento> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<UnidadeAtendimento> model) {
        this.model = model;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    private void carregaModelLazy() {
        model = new LazyDataModel<UnidadeAtendimento>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<UnidadeAtendimento> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        sf.append(" where u." + filterProperty + " like'%" + filterValue + "%' AND u.inativo = FALSE ");
                    } else {
                        sf.append(" and u." + filterProperty + " like'%" + filterValue + "%' AND u.inativo = FALSE ");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by u." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by u." + sortField + " desc");
                }
                Clausula = sf.toString();
                setRowCount(Integer.parseInt(undEJB.contarUndAtendimentoRegistro(Clausula).toString()));


                listaaCarregarLazy = undEJB.listarUndAtendimentoLazyModeWhere(first, pageSize, Clausula);
//                listaPacientesLazy = pEJB.listarPacientesLazyMode(first, pageSize);
                return listaaCarregarLazy;
            }
        };
    }
}
