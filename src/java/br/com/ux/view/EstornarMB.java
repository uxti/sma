/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.OperacaoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Caixa;
import br.com.ux.model.CaixaPai;
import br.com.ux.model.Operacao;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Usuario;
import br.com.ux.util.Conexao;
import br.com.ux.util.FiltrosRepasse;
import br.com.ux.util.Mensagem;
import br.com.ux.util.Parametro;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class EstornarMB implements Serializable {

    @EJB
    RepasseEJB rEJB;
    private Repasse repasse;
    private List<Repasse> repasseSelecionados;
    private Double valorTotalPgto;
    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;
    @EJB
    CaixaEJB cEJB;
    private Caixa caixa;
    private CaixaPai caixaPai;
    UsuarioSessao us = new UsuarioSessao();
    @EJB
    UsuarioEJB uEJB;
    private Usuario usuario;
    @EJB
    OperacaoEJB opEJB;
    private Operacao operacao = new Operacao();

    public EstornarMB() {
        repasse = new Repasse();
        repasseSelecionados = new ArrayList<Repasse>();
        valorTotalPgto = 0D;
        paciente = new Paciente();
        usuario = new Usuario();
        operacao = new Operacao();
        caixa = new Caixa();
        caixaPai = new CaixaPai();
    }
    FiltrosRepasse filtros = new FiltrosRepasse();

    public FiltrosRepasse getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosRepasse filtros) {
        this.filtros = filtros;
    }

    public void listarRepassesAEstornar() {
        try {
            StringBuilder sb = new StringBuilder();
            
            repasseSelecionados = new ArrayList<Repasse>();
            Statement statement = null;
            ResultSet resultSet = null;
            
            if (filtros.getIdCirurgia() != null) {
                sb.append("AND id_cirurgia like '" + filtros.getIdCirurgia() + "' ");
            } else {
                sb.append("AND id_cirurgia like '%%' ");
            }

            if (filtros.getIdRepasse() != null) {
                sb.append("AND id_repasse LIKE '" + filtros.getIdRepasse() + "' ");
            } else {
                sb.append("AND id_repasse like '%%' ");
            }

            statement = Conexao.conexaoJDBC().createStatement();
            resultSet = statement.executeQuery("select numero_parcela, id_paciente, dt_recebimento, sum(valor_documento)valor_documento, sum(valor_recebido) valor_recebido, id_cirurgia, sum(valor) valor from repasse where dt_recebimento is not null and status = 'F' and id_paciente  like '%%' " + sb + " group by id_paciente, dt_recebimento, id_cirurgia, numero_parcela");
            Repasse r = new Repasse();
            while (resultSet.next()) {
                r = new Repasse();
//              r = rEJB.selecionarPorID(resultSet.getInt("id_repasse"));
                r.setIdPaciente(rEJB.pesquisarPaciente(resultSet.getInt("id_paciente")));
                r.setIdCirurgia(rEJB.pesquisarCirurgia(resultSet.getInt("id_cirurgia")));
                r.setValorRecebido(resultSet.getDouble("valor"));
                r.setValor(resultSet.getDouble("valor"));
                r.setDtRecebimento(resultSet.getDate("dt_recebimento"));
                r.setValorRecebido(resultSet.getDouble("valor_recebido"));
                r.setNumeroParcela(resultSet.getInt("numero_parcela"));
                r.setValorDocumento(resultSet.getDouble("valor_documento"));
                repasseSelecionados.add(r);
            }
            if (repasseSelecionados.isEmpty()) {
                Mensagem.addMensagem(3, "Atenção! Nenhum recebimento para estorno foi encontrado!");
            }
        } catch (SQLException ex) {
            Logger.getLogger(EstornarMB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<Operacao> listarOperacoes() {
        return opEJB.listarOperacoes();
    }

    public CaixaPai getCaixaAtual() {
        return caixaPai = cEJB.retornaCaixaAberto(us.retornaUsuario());
    }

    public void ConfirmarEstorno() {
        List<Repasse> repassesAEstornar = new ArrayList<Repasse>();
        Date dt_estorno = new Date();
        try {
            for (Repasse rr : repasseSelecionados) {
                if (rr.isPagar()) {
                    repassesAEstornar = rEJB.pesquisarRepassePorPacienteCirurgiaParcelaDtRecebimento(rr.getIdCirurgia().getIdCirurgia(), rr.getNumeroParcela());
                    for (Repasse re : repassesAEstornar) {
                        try {
                            rEJB.ExcluirContaCorrenteEstorno(re.getIdRepasse());
                        } catch (Exception e) {
                            Mensagem.addMensagem(3, "Atenção! Erro ao tentar estornar da conta corrente do terceiro!");
                        }
                        try {
                            caixa.setDtCaixa(dt_estorno);
                            caixa.setIdRepasse(re);
                            caixa.setTipoLancamento('D');
                            caixa.setValor(re.getValorRecebido());
                            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            caixa.setHistorico("Estorno do recebimento da cirurgia " + re.getIdCirurgia().getIdCirurgia() + ", Parcela " + re.getNumeroParcela() + ". Paciente " + re.getIdPaciente().getIdPaciente() + ", " + re.getIdPaciente().getNome() + ". Estornado por " + us.retornaUsuario());
                            caixa.setIdOperacao(operacao);
//                            caixa.setIdCaixaPai(getCaixaAtual());
                            caixa.setStatus("Baixado");
                            caixa.setIdCirurgia(re.getIdCirurgia());
                            caixa.setNumeroParcela(re.getNumeroParcela());
                            cEJB.LancarRecebimentoNoCaixa(caixa);
//                            cEJB.atualizarSaldoCaixaPai(getCaixaAtual(), caixa.getValor(), caixa.getTipoLancamento());
                            Caixa cc = cEJB.pegaCaixaPorRepasse(re);
                            cc.setStatus("Baixado");
                            cEJB.LancarRecebimentoNoCaixa(cc);
                        } catch (Exception e) {
                            Mensagem.addMensagem(3, "Atenção! Erro ao tentar estornar do caixa!");
                        }
                        try {
                            rEJB.ExcluirRepasseRepetidoAposEstorno(re);
                        } catch (Exception e) {
                            System.out.println("erro ao tentar excluir os repasses repetidos");
                            System.out.println(e);
                        }
                        try {

                            re.setAcrescimo(null);
                            re.setJuros(null);
                            re.setValor(re.getValorDocumento());
                            re.setSituacaoPaciente('A');
                            re.setStatus('A');
                            re.setSituacaoTerceiro('N');
                            re.setTipoPagamento('P');
                            re.setPercentualRecebido(null);
                            re.setValorRecebido(null);
                            re.setDesconto(null);
                            re.setAcrescimo(null);
                            re.setJuros(null);
                            re.setDtRecebimento(null);
                            rEJB.Salvar(re);
                        } catch (Exception e) {
                            Mensagem.addMensagem(3, "Atenção erro ao tentar estornar!" + e);
                        }
                    }
                }
            }
            Mensagem.addMensagem(1, "Estorno efetuado com sucesso!");
        } catch (Exception e) {
            System.out.println(e);
        }
        listarRepassesAEstornar();
    }

    public void pesquisarRepasses() {
        if (filtros.getIdCirurgia() == null && filtros.getIdPaciente() == null && filtros.getIdRepasse() == null) {
            filtros = new FiltrosRepasse();
        }
        repasseSelecionados = new ArrayList<Repasse>();
        try {
            repasseSelecionados = rEJB.retornaListaTipada(filtros);
            if (repasseSelecionados.isEmpty()) {
                Mensagem.addMensagem(4, "Atenção nenhuma conta para estorno foi encontrado!");
            }
        } catch (Exception e) {
        }

    }

    public Repasse getRepasse() {
        return repasse;
    }

    public void setRepasse(Repasse repasse) {
        this.repasse = repasse;
    }

    public List<Repasse> getRepasseSelecionados() {
        return repasseSelecionados;
    }

    public void setRepasseSelecionados(List<Repasse> repasseSelecionados) {
        this.repasseSelecionados = repasseSelecionados;
    }

    public Double getValorTotalPgto() {
        return valorTotalPgto;
    }

    public void setValorTotalPgto(Double valorTotalPgto) {
        this.valorTotalPgto = valorTotalPgto;
    }

    public List<Paciente> listarPacientes() {
        return pEJB.listarPacientes();
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public CaixaPai getCaixaPai() {
        return caixaPai;
    }

    public void setCaixaPai(CaixaPai caixaPai) {
        this.caixaPai = caixaPai;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Operacao getOperacao() {
        return operacao;
    }

    public void setOperacao(Operacao operacao) {
        this.operacao = operacao;
    }
}
