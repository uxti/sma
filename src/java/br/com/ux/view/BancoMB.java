/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.BancoEJB;
import br.com.ux.model.Banco;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author PokerFace
 */
@ManagedBean
@ViewScoped
public class BancoMB implements Serializable {

    @EJB
    BancoEJB bancoEJB;
    UsuarioSessao us = new UsuarioSessao();
    List<Banco> listaBanco = new ArrayList<Banco>();
    private Banco banco;
    LazyDataModel<Banco> model;

    public BancoMB() {
        novo();
        carregarModeloLazy();
    }

    public void novo() {
        banco = new Banco();
    }

    public void carregarModeloLazy() {
        model = new LazyDataModel<Banco>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Banco> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append("and p.inativo = FALSE and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%' and p.inativo = FALSE");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%' and p.inativo = FALSE");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                setRowCount(Integer.parseInt(bancoEJB.contarRegistro(Clausula).toString()));
                listaBanco = bancoEJB.listarLazy(first, pageSize, Clausula);
                return listaBanco;
            }
        };
    }

    public void salvar() {
        if (banco.getIdBanco() == null) {
            try {
                bancoEJB.Salvar(banco, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        } else {
            try {
                bancoEJB.Salvar(banco, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("editar");
            }
        }
    }

    public void excluir(Banco b) {
        try {
            bancoEJB.Excluir(b, us.retornaUsuario());
            novo();
            Mensagem.addMensagemPadraoSucesso("excluir");
            DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
                    .findComponent("formBanco:dtl");
            if (dataTable != null) {
                dataTable.reset();
            }
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }

    public Banco selecionaPorID(Integer id) {
        return banco = bancoEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public List<Banco> listaBancos() {
        listaBanco = new ArrayList<Banco>();
        listaBanco = bancoEJB.listaBanco();
        return listaBanco;
    }

    public List<Banco> getListaBanco() {
        return listaBanco;
    }

    public void setListaBanco(List<Banco> listaBanco) {
        this.listaBanco = listaBanco;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public LazyDataModel<Banco> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Banco> model) {
        this.model = model;
    }
}
