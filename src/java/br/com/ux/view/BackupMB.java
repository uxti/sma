/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.util.AgendadorEJB;
import br.com.ux.util.Mensagem;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class BackupMB implements Serializable{

    private Date dtUltimoBkp = new Date();
    int cont = 0;
    @EJB
    AssociacaoEJB assEJB;
    @EJB
    AgendadorEJB aEJB;
            
    public BackupMB() {
    }

    public void GerarBackup() {        
        try {
            aEJB.backupAutomatico();
            Mensagem.addMensagem(1, "Backup concluído com sucesso. Arquivo salvo em " + aEJB.getDestino() + "Backups");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao tentar fazer o backup!");
        }        
    }
//    @PostConstruct
//    public void carregarDados() {
//        //caminhoBat = "D:/SMA/BKPPARAMETRIZADO.bat ";
//        //caminhoDump = "D:/xampp/mysql/bin/mysqldump ";
//        caminhoMySQL = "C:\\Program Files (x86)\\MySQL\\MySQL Server 5.5\\bin\\";
//        destino = "C:\\SMA\\";
//        if (assEJB.retornaDataUltimoBkp() != null) {
//            dtUltimoBkp = assEJB.retornaDataUltimoBkp();
//        }
//        botao = false;
//    }

//    public void handleFileUpload(FileUploadEvent event) throws IOException {
//        InputStream is = null;
//        OutputStream os = null;
//        byte[] buffer;
//        boolean success = true;
//        criarPastas();
//        try {
//            is = event.getFile().getInputstream();
//            os = new FileOutputStream(destino + "Temp\\Teste.csv");
//            buffer = new byte[is.available()];
//            is.read(buffer);
//            os.write(buffer);
//        } catch (IOException e) {
//            success = false;
//        } catch (OutOfMemoryError e) {
//            success = false;
//        } finally {
//            try {
//                if (is != null) {
//                    is.close();
//                }
//                if (os != null) {
//                    os.close();
//
//                }
//            } catch (IOException e) {
//            }
//        }
//        System.out.println(success);
//        if (success) {
//            setCaminhoCSV(destino + "Temp\\Teste.csv");
//            System.out.println(caminhoCSV);
//        }
//        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Arquivo CSV importado." + event.getFile().getFileName() + " importado para" + caminhoCSV, null));
//    }

    public Date getDtUltimoBkp() {
        return dtUltimoBkp;
    }

    public void setDtUltimoBkp(Date dtUltimoBkp) {
        this.dtUltimoBkp = dtUltimoBkp;
    }

    
}
