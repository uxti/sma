/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.CirurgiaEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.MovimentoUsuarioEJB;
import br.com.ux.controller.OrcamentoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.RepasseEstaticoEJB;
import br.com.ux.controller.SaldoCaixaEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Associacao;
import br.com.ux.model.Caixa;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Orcamento;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Terceiro;
import br.com.ux.model.TipoInternacao;
import br.com.ux.model.Usuario;
import br.com.ux.pojo.Orcto;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.ContaCorrenteEstatica;
import br.com.ux.util.Mensagem;
import br.com.ux.util.RelatorioFactory;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class ContaCorrenteMB implements Serializable {

    @EJB
    AuditoriaEJB aEJB;
    @EJB
    ContaCorrenteEJB ccEJB;
    @EJB
    SaldoCaixaEJB scEJB;

    private ContaCorrente contaCorrente, contaCorrenteEstorno, contaCorrenteSaque;
    private List<ContaCorrente> listaSaque, listaItensSaque, listaContaCorrente, listaContaSelecionados, listaContaSelecionadosPorMedico, listaItensSelecionadosEstornar, listaItensAEstornar;
    private List<ContaCorrente> listaContaCorrente2;
    private LazyDataModel<ContaCorrente> listaContaCorrenteLazy;
    UsuarioSessao us = new UsuarioSessao();
    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;
    private List<Paciente> pacientesAcarregar;
    private List<Paciente> listaPacientesACarregar;
    private LazyDataModel<Paciente> model;
    @EJB
    TerceiroEJB tEJB;
    private Terceiro terceiro;
    private List<Terceiro> terceirosAcarregar;
    @EJB
    UsuarioEJB uEJB;
    private Usuario usuario;
    @EJB
    CaixaEJB caixaEJB;
    private Caixa caixa;
    private List<Caixa> listaCaixa;
    @EJB
    RepasseEJB repasseEJB;
    private Repasse repasse;
    @EJB
    CirurgiaEJB cirEJB;
    private TipoInternacao tipoInternacao;
    @EJB
    RepasseEstaticoEJB reEJB;
    @EJB
    ChequeEJB chEJB;
    @EJB
    OrcamentoEJB oEJB;
    @EJB
    AssociacaoEJB asEJB;
    @EJB
    MovimentoUsuarioEJB muEJB;

    //  =================================================================  // 
    private Date pdtINILancamento, pdtINIEstorno, pdtINIRetirada, pdtINIPrevisao, dtInicioResumo, dtFinalResumo;
    private Date pdtFIMLancamento, pdtFIMRetirada, pdtFIMEstorno, pdtFIMPrevisao;
    private String pstatus, psituacao, CredDeb, tipo, nomePacientePesquisa, loteOrigemPesquisa;
    private Caixa lotePesquisa;
    private String pidTerceiro, pidPaciente, Filtros, saldoFormatado;
    private Calendar dataAtual;
    private Calendar primeiroDia;
    private Calendar ultimoDia;
    private Double saldoAnterior, saldo, credito, debito, saldoInicial, saldoFinal;     // totais
    private Double liberado, naoLiberado, vencido;                                      // situacao
    private Double pago, retirado, naoRetirado, saque, estorno;                         // status
    private Double totalSelecionado, totalItensSaque;                                   // checkbox
    private boolean selecionado, manterItensSelecionados;
    private Integer idSaqueImprimir = 0;
    SessaoMB sessao = new SessaoMB();
    AssociacaoMB associacao = new AssociacaoMB();
    private List<ContaCorrenteEstatica> listaResumo;
    private List<Orcamento> listaDeOrctos;
    private String pacientePesquisa;
    private Associacao ass;
    private PieChartModel gfTipos, gfValores;
    private Date dtAtual;
    private List<Cirurgia> listaCirurgiaAPesquisar;
    private List<Caixa> listaCaixaTransferencia;
    private List<ContaCorrente> listaSaques;
    private Boolean selecaoLimpa;

    public ContaCorrenteMB() {
        novo();
    }

    public void novo() {
        contaCorrente = new ContaCorrente();
        contaCorrenteEstorno = new ContaCorrente();
        paciente = new Paciente();
        terceiro = new Terceiro();
        usuario = new Usuario();
        listaContaCorrente = new ArrayList<>();
        listaSaque = new ArrayList<>();
        listaItensSaque = new ArrayList<>();
        listaContaSelecionados = new ArrayList<>();
        listaItensAEstornar = new ArrayList<>();
        pacientesAcarregar = new ArrayList<>();
        terceirosAcarregar = new ArrayList<>();
        tipoInternacao = new TipoInternacao();
        Filtros = "A";
        setarVariaveis();
        psituacao = "Não Liberado";
        dtAtual = new Date();
        dtInicioResumo = BCRUtils.primeiroDiaDoMes(new Date());
        dtFinalResumo = BCRUtils.ultimoDiaDoMes(new Date());
        padtIni = BCRUtils.primeiroDiaDoMes(new Date());
        padtFim = BCRUtils.ultimoDiaDoMes(new Date());
        pdtINILancamento = BCRUtils.primeiroDiaDoMes(BCRUtils.addMes(new Date(), -1));
        pdtFIMLancamento = BCRUtils.ultimoDiaDoMes(new Date());
        listaDeOrctos = new ArrayList<>();
        ass = new Associacao();
        loteOrigemPesquisa = null;
        manterItensSelecionados = Boolean.FALSE;
        selecaoLimpa = Boolean.FALSE;
    }

    @PostConstruct
    public void setarUsuarioCorrente() {
        usuario = uEJB.retornaUsuarioDaSessao();
        //carregarTransferenciasParaPagamentos();
        carregaContasParaPagarLazy();
    }

    public Date addDia(Date data, Integer qtdsDias) {
        return BCRUtils.addDia(data, qtdsDias);
    }

    public void zerarPaciente() {
        paciente = new Paciente();
        nomePacientePesquisa = null;
        pacientePesquisa = null;
        paIdPaciente = null;
    }

    public void limparPaciente() {
        nomePacientePesquisa = null;
        pacientePesquisa = null;
    }

    public void limparTerceiro() {
        terceiro = new Terceiro();
    }

    public void pesquisarContaCorrenteResumo(Terceiro t) {
        listaContaCorrente = new ArrayList<>();
        ass = asEJB.carregarAssociacao();
        StringBuilder sb = new StringBuilder();
        setarVariaveis();
        pidTerceiro = t.getIdTerceiro().toString();
        String dataInicio, dataFim;
        DateFormat df = new SimpleDateFormat("y-M-d");

        // Para pegar antigos já vencidos automaticamente.
        dataInicio = "2000-01-01"; //df.format(getDtInicioResumo());
        dataFim = df.format(getDtFinalResumo());
        listaResumo = ccEJB.pesquisaContaCorrenteResumo(getPidTerceiro(), dataInicio, dataFim);
        credito = 0D;
        liberado = 0D;
        vencido = 0D;
        naoLiberado = 0D;
        for (ContaCorrenteEstatica cc : listaResumo) {
            credito = credito + cc.getValor();
            if ((cc.getSituacao().equals("Liberado")) && cc.getStatus().equals("Retirado")) {
                liberado += +cc.getValor();
            } else if ((addDia(cc.getDtPrevisao(), ass.getPrazoCompensacaoCheque())).before(dtAtual)) {
                vencido += cc.getValor();
            } else {
                naoLiberado += +cc.getValor();
            }
        }
        if (listaResumo.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public void atualizarRepassesLiberacao() {
        listaSaques = listarContasParaPagar();
    }

    public List<ContaCorrente> listarContasParaPagar() {
        return ccEJB.carregaContasParaPagar();
    }

    public void carregaContasParaPagarLazy() {
        listaContaCorrenteLazy = new LazyDataModel<ContaCorrente>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<ContaCorrente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuilder sf = new StringBuilder();
                int contar = 0;
                for (String filterProperty : filters.keySet()) {
                    // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p.").append(filterProperty).append(" like '").append(filterValue).append("%' AND  p.tipo IN ('S') and p.dtPagamentoMedico IS NULL ");
                        } else {
                            sf.append(" where p.").append(filterProperty).append(" like '").append(filterValue).append("%' AND p.tipo IN ('S') and p.dtPagamentoMedico IS NULL ");
                        }
                    } else {
                        sf.append(" and p.").append(filterProperty).append(" like '").append(filterValue).append("%' AND p.tipo IN ('S') and p.dtPagamentoMedico IS NULL");
                    }
                    contar++;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    if (sf.length() > 0) {
                        sf.append(" order by p.").append(sortField).append(" asc");
                    } else {
                        sf.append(" where p.tipo IN ('S') and p.dtPagamentoMedico IS NULL order by p.").append(sortField).append(" asc");
                    }
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    if (sf.length() > 0) {
                        sf.append(" order by p.").append(sortField).append(" desc");
                    } else {
                        sf.append(" where p.tipo IN ('S') and p.dtPagamentoMedico IS NULL order by p.").append(sortField).append(" desc");
                    }

                }
                Clausula = sf.toString();
                //System.out.println(Clausula);
                setRowCount(Integer.parseInt(ccEJB.contarContaCorrente(Clausula).toString()));
                listaContaCorrente2 = ccEJB.listarContaCorrenteLazyModeWhere(first, pageSize, Clausula);
                return listaContaCorrente2;
            }
        };
    }

    public void pagarMedico(Integer id, Integer idRepasse) {
        try {
            ccEJB.atualizarPagamentoDoMedico(id, idRepasse);
            BCRUtils.ResetarDatatableFiltros("graficos:frmLiberar:dtlSaque");
            Mensagem.addMensagemGrowl2(1, "Pagamento efetuado com sucesso!");
        } catch (Exception e) {
            Mensagem.addMensagemGrowl2(4, "Não foi possível efetuar o pagamento!");
            System.out.println(e);
        }
    }

    public void verUsuario() {
        System.out.println(usuario.getNomeCompleto());
    }

    public void pesquisarContaCorrente() {
        listaContaCorrente = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        setarVariaveis();
        pidTerceiro = terceiro.getIdTerceiro().toString();
        String dataInicio, dataFim = "";
        DateFormat df = new SimpleDateFormat("y-M-d");

        if (paIdPaciente != null) {
            sb.append("AND c.idPaciente.idPaciente = '").append(paIdPaciente).append("' ");
        }
        if (getCredDeb() != null) {
            sb.append("AND c.tipo LIKE '").append(getCredDeb()).append("' ");
        }

        System.out.println(loteOrigemPesquisa);
        if (loteOrigemPesquisa.length() >= 3) {
            sb.append("AND c.idRepasse.idCaixa.loteOrigem like '%").append(loteOrigemPesquisa).append("%' ");
        }

        if (getPsituacao() != null) {
            switch (getPsituacao()) {
                case "Não Compensado":
                    sb.append("AND c.idRepasse.idCheque.compensado = 'N' AND (c.situacao = 'Não Liberado' or (c.situacao = 'Estornado' and c.valor != 0)) AND c.situacao != 'Devolvido'");
                    break;
                case "Liberado":
                    sb.append("AND c.situacao LIKE '").append(getPsituacao()).append("' ");
                    break;
                case "Taxa Depósito":
                    sb.append("AND c.taxaDeposito = TRUE ");
                    break;
                default:
                    sb.append("AND (c.situacao LIKE '").append(getPsituacao()).append("' or (c.situacao = 'Estornado' and c.valor != 0)) AND c.situacao != 'Devolvido'");
                    break;
            }

        }

        if (getPstatus() != null) {
            sb.append("AND c.status LIKE '").append(getPstatus()).append("' ");
        }

        Date teste = pdtINILancamento;
        Date teste2 = pdtFIMLancamento;

        dataInicio = df.format(teste);
        dataFim = df.format(teste2);

        sb.append("AND c.idRepasse.dtVencimento" + " BETWEEN '").append(dataInicio).append("' AND '").append(dataFim).append("' ");

        listaContaCorrente = ccEJB.pesquisaCC_Parametros(getPidTerceiro(), sb.toString());
        credito = 0D;
        liberado = 0D;
        naoLiberado = 0D;

        for (ContaCorrente cc : listaContaCorrente) {
            credito = credito + cc.getValor();
            if (cc.getTipo().equals("C")) {
                if (cc.getIdRepasse().getIdCheque() == null) {
                    naoLiberado += cc.getValor();
                } else if (cc.getIdRepasse().getIdCheque().getCompensado().equals("S")) {
                    liberado += cc.getValor();
                } else {
                    naoLiberado += cc.getValor();
                }
            }
        }

//        DESABILITADO POIS NÃO HÁ MAIS NECESSIDADE DE CALCULAR O SALDO RETIRADO E NÃO RETIRADO.
//        LISTA 13072015    
//        for (ContaCorrente cc : listaContaCorrente) {
//            if (cc.getTipo().equals("C")) {
//                credito = credito + cc.getValor();
//                if (cc.getSituacao().equals("Liberado")) {
//                    liberado = liberado + cc.getValor();
//                    if (cc.getStatus().equals("Retirado")) {
//                        retirado = retirado + cc.getValor();
//                    } else if (cc.getStatus().equals("Não Retirado")) {
//                        naoRetirado = naoRetirado + cc.getValor();
//                    }
//                } else if (cc.getSituacao().equals("Não Liberado")) {
//                    naoLiberado = naoLiberado + cc.getValor();
//                }
//
//            } else if (cc.getTipo().equals("D")) {
//                debito = debito + cc.getValor();
//                if (cc.getSituacao().equals("Pago")) {
//                    pago = pago + cc.getValor();
//                }
//            }
//        }
//
//        saldo = credito - debito;
//        saldoFinal = (saldoAnterior + credito) - debito;
        if (listaContaCorrente.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public void selecionarPaciente(Paciente paciente1) {
        paciente = paciente1;
    }

    public void selecionarPacientePesquisa(Paciente paciente) {
        paIdPaciente = paciente.getIdPaciente().toString();
        nomePacientePesquisa = paciente.getNome();
    }

    public void pesquisarSaque() {
        listaSaque = new ArrayList<ContaCorrente>();
        setarVariaveis();
        pidTerceiro = terceiro.getIdTerceiro().toString();
        listaSaque = ccEJB.pesquisaSaque_Parametros(getPdtINIRetirada(), getPdtFIMRetirada(), getPidTerceiro());
        for (ContaCorrente cc : listaSaque) {
            if (cc.getTipo().equals("S")) {
                saque = saque + cc.getValor();
            }
            if (cc.getTipo().equals("E")) {
                estorno = estorno + cc.getValor();
            }
        }
        if (listaSaque.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public void pesquisarSaqueTerceiro(Terceiro terc) {
        listaSaque = new ArrayList<ContaCorrente>();
        setarVariaveis();
        pidTerceiro = terc.getIdTerceiro().toString();
        System.out.println(pidTerceiro);
        listaSaque = ccEJB.pesquisaSaque_Parametros(getDtInicioResumo(), getDtFinalResumo(), getPidTerceiro());
        for (ContaCorrente cc : listaSaque) {
            if (cc.getTipo().equals("S")) {
                saque = saque + cc.getValor();
            }
            if (cc.getTipo().equals("E")) {
                estorno = estorno + cc.getValor();
            }
        }
        if (listaSaque.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public void pesquisarItensSaque(ContaCorrente cc, String tipo) {
        if (tipo.equals("estorno")) {
            contaCorrenteEstorno = cc;
        } else {
            contaCorrenteSaque = cc;
        }
        totalItensSaque = 0D;
        listaItensSaque = new ArrayList<>();
        listaItensSaque = ccEJB.pesquisaItensSaque(cc);
        if (!listaItensSaque.isEmpty()) {
            for (ContaCorrente cc2 : listaItensSaque) {
                totalItensSaque = totalItensSaque + cc2.getValor();
                idSaqueImprimir = cc2.getIdSaque();
            }
        } else {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }
    NumberFormat formataDinheiro = NumberFormat.getCurrencyInstance();

    public void retornaValorAPagar() {
        totalSelecionado = 0D;

        int x = 0;
        Terceiro t = new Terceiro();

        if (listaContaSelecionadosPorMedico != null) {
            if (!listaContaSelecionadosPorMedico.isEmpty()) {
                for (ContaCorrente cc3 : listaContaSelecionadosPorMedico) {
                    t = cc3.getIdTerceiro();
                }
            }

            for (ContaCorrente cc2 : listaContaSelecionados) {
                if ((!listaContaSelecionadosPorMedico.contains(cc2) && terceiro.equals(cc2.getIdTerceiro()) && cc2.getIdTerceiro().equals(t)) || listaContaSelecionadosPorMedico.isEmpty()) {
                    listaContaSelecionadosPorMedico.add(cc2);
                } else {
                    x++;
                    break;
                }
            }

            if (listaContaSelecionados.isEmpty() && !listaContaSelecionadosPorMedico.isEmpty()) {
                listaContaSelecionados.addAll(listaContaSelecionadosPorMedico);
                selecaoLimpa = Boolean.TRUE;
            }

            for (ContaCorrente cc2 : listaContaSelecionados) {
                if (!cc2.getIdTerceiro().equals(t)) {
                    listaContaSelecionadosPorMedico = null;
                    Mensagem.addMensagem(2, "Foram encontrados médicos diferentes entre os repasses selecionados.");
                }
            }
        }

        if (!listaContaSelecionados.isEmpty()) {
            for (ContaCorrente cc : listaContaSelecionados) {
                totalSelecionado = totalSelecionado + cc.getValor();
            }
        } else {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }

//        System.out.println("tamanho lista 1: " + listaContaSelecionados.size());
//        System.out.println("tamanho lista 2: " + listaContaSelecionadosPorMedico.size());
    }

    public void selecionarContasParaPagamentoEmLote() {
        if (listaContaSelecionadosPorMedico == null) {
            listaContaSelecionadosPorMedico = new ArrayList<>();
        }

        int x = 0;
        Terceiro t = new Terceiro();

        if (!listaContaSelecionadosPorMedico.isEmpty()) {
            for (ContaCorrente cc3 : listaContaSelecionadosPorMedico) {
                t = cc3.getIdTerceiro();
            }
        }

        for (ContaCorrente cc2 : listaContaSelecionados) {
            System.out.println("testando o " + cc2.getIdContaCorrente());
            System.out.println("medico selecionado " + terceiro.getNome());
            System.out.println("medico do repasse " + t.getNome());
            System.out.println("se tem " + (listaContaSelecionadosPorMedico.contains(cc2) ? "Tem" : "Não tem"));
            if ((!listaContaSelecionadosPorMedico.contains(cc2) && terceiro.equals(cc2.getIdTerceiro()) && (cc2.getIdTerceiro().equals(t) || t.getIdTerceiro() == null)) || listaContaSelecionadosPorMedico.isEmpty()) {
                listaContaSelecionadosPorMedico.add(cc2);
            } else {
                x++;
                break;
            }
        }

        if (x == 0) {
            Mensagem.addMensagem(1, listaContaSelecionados.size() + " repasse(s) selecionado(s).");
            t = terceiro;
            novo();
            terceiro = t;
        } else {
            Mensagem.addMensagem(2, "Os repasses não podem ser adicionados aos selecionados. Verifique o médico nos itens marcados.");
        }

    }

    public void limparListaSelecionados() {
        if (listaContaSelecionadosPorMedico != null) {
            listaContaSelecionadosPorMedico.clear();
        }
        Mensagem.addMensagem(1, "Repasses selecionados anteriormente foram desmarcados.");
    }

    public Boolean verificarSeJaFoiSelecionado(ContaCorrente cc) {
        if (listaContaSelecionadosPorMedico != null) {
            return listaContaSelecionadosPorMedico.contains(cc);
        }
        return false;
    }

    public Integer retornaNumeroParcela(Integer idRepasse, Integer idTerceiro, Integer idCirurgia) {
        return reEJB.retornaRepassePorMedicoFechamento(idRepasse, idTerceiro, idCirurgia);
    }

    public Long retornaMaxNumeroParcela(Integer idTerceiro, Integer idCirurgia) {
        return reEJB.retornaMaxParcelaPorMedicoFechamento(idTerceiro, idCirurgia);
    }

    public void estornarSaque() {
        try {
            String data;
            DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
            usuario = ccEJB.selecionarIDUsuario(us.retornaUsuario());
            Double valor = 0D;
            if (listaItensSelecionadosEstornar.isEmpty()) {
                Mensagem.addMensagem(3, "É necessário selecionar ao menos um repasse.");
            } else {
                //Editar o registro selecionado e coloca como estornado.
                contaCorrenteEstorno.setSituacao("Estornado");
                contaCorrenteEstorno.setStatus("Retirado");
                contaCorrenteEstorno.setTipo("E");

                contaCorrenteEstorno.setDtEstorno(new Date());
                data = f.format(contaCorrenteEstorno.getDtLancamento());
                contaCorrenteEstorno.setObservacao(contaCorrenteEstorno.getObservacao() + ". Estorno realizado em: " + data + " .");
                for (ContaCorrente cc3 : listaItensSelecionadosEstornar) {
                    valor = cc3.getValor() + valor;
                    System.out.println(valor);
                }
                ccEJB.SalvarEstorno(contaCorrenteEstorno, us.retornaUsuario(), valor.equals(contaCorrenteEstorno.getValor()), null);

                for (ContaCorrente conta : listaItensSelecionadosEstornar) {
                    conta.setDtEstorno(new Date());
                    conta.setDtRetirada(null);
                    data = f.format(new Date());
                    conta.setIdSaque(null);
                    conta.setSituacao("Não Liberado");
                    conta.setStatus("Estornado");
                    conta.setObservacao(conta.getObservacao() + ". Estorno realizado em: " + data + ". Usuário: " + us.retornaUsuario());
                    conta.getIdRepasse().setSacado("N");
                    ccEJB.AtualizarCC(conta);
                }
                atualizarDados(listaItensSelecionadosEstornar, "E");
                Mensagem.addMensagem(1, "Estorno efetuado com sucesso! ");
                novo();
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar estornar!" + e);
            System.out.println(e);
        }
    }

    public void limparCampos() {
        contaCorrente = new ContaCorrente();
        contaCorrenteEstorno = new ContaCorrente();
        listaItensSelecionadosEstornar = new ArrayList<>();
        listaItensSaque = new ArrayList<>();
    }

    public void estornarSaqueSelecionados() {
        try {
            String data;
            DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
            usuario = ccEJB.selecionarIDUsuario(us.retornaUsuario());
            if (!listaItensAEstornar.isEmpty()) {
                for (ContaCorrente cc : listaItensAEstornar) {
                    // Editar o registro selecionado e coloca como estornado.
                    cc.setSituacao("Estornado");
                    cc.setStatus("Retirado");
                    cc.setDtEstorno(new Date());
                    data = f.format(cc.getDtLancamento());
                    cc.setObservacao(cc.getObservacao() + ". Estorno realizado em: " + data + " .");
                    ccEJB.Salvar(cc, usuario, listaItensSaque, " ");
                    //Começa a trabalhar com os itens do saque.
                    ContaCorrente corrente = new ContaCorrente();
                    usuario = ccEJB.selecionarIDUsuario(us.retornaUsuario());
                    corrente.setIdTerceiro(cc.getIdTerceiro());
                    if (usuario.getIdUsuario() != null) {
                        corrente.setIdUsuario(usuario);
                    }
                    corrente.setTipo("E");
                    corrente.setValor(cc.getValor());
                    corrente.setObservacao("Estorno realizado por " + corrente.getIdUsuario().getUsuario() + " no valor de: R$" + corrente.getValor() + ". Favorecido: " + corrente.getIdTerceiro().getNome());
                    corrente.setDtLancamento(new Date());
                    corrente.setSituacao("Liberado");
                    corrente.setStatus("Retirado");
                    ccEJB.Salvar(corrente, usuario, listaItensSaque, "E");
                    atualizarDados(listaContaCorrente, "E");
                    Mensagem.addMensagem(1, "Estorno efetuado com sucesso! "
                            + "Todas os itens do saque já foram restaurados.");
                    aEJB.Salvar(BCRUtils.criarAuditoria("estorno", corrente.getIdContaCorrente() + " - Valor: " + corrente.getValor() + " para o médico: " + corrente.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Pagamentos"));
                }
            } else {
                Mensagem.addMensagem(3, "Não foram encontrados registros.");
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar estornar!" + e);
            System.out.println(e);
        }

    }

    public void pagarSelecionados() {
        try {
            if (listaContaSelecionados.isEmpty() && !listaContaSelecionadosPorMedico.isEmpty()) {
                listaContaSelecionados = listaContaSelecionadosPorMedico;
            }

            if (listaContaSelecionados.size() > 0) {
                usuario = ccEJB.selecionarIDUsuario(us.retornaUsuario());
                if (terceiro.getIdTerceiro() != null) {
                    contaCorrente.setIdTerceiro(terceiro);
                }
                if (usuario.getIdUsuario() != null) {
                    contaCorrente.setIdUsuario(usuario);
                }
                contaCorrente.setTipo("S");
                contaCorrente.setValor(totalSelecionado);
                contaCorrente.setObservacao("Liberação realizado por " + contaCorrente.getIdUsuario().getUsuario() + " no valor de: R$" + contaCorrente.getValor() + ". Favorecido: " + contaCorrente.getIdTerceiro().getNome());
                contaCorrente.setDtRetirada(new Date());
                contaCorrente.setDtLancamento(new Date());
                contaCorrente.setSituacao("Liberado");
                contaCorrente.setStatus("Retirado");

                ccEJB.Salvar(contaCorrente, usuario, listaContaSelecionados, "S");
                atualizarDados(listaContaSelecionados, "S");
                idSaqueImprimir = contaCorrente.getIdContaCorrente();
                Mensagem.addMensagem(1, "Liberação efetuada com sucesso!");

                aEJB.Salvar(BCRUtils.criarAuditoria("liberar", contaCorrente.getIdContaCorrente() + " - Valor: " + contaCorrente.getValor() + " para o médico: " + contaCorrente.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Pagamentos"));
                scEJB.Salvar(BCRUtils.criarMovimentoCaixa("D", "Pagamento", new Date(), contaCorrente.getValor(), null, contaCorrente, usuario));
                RequestContext.getCurrentInstance().execute("PF('dlgImprimirSaques').show();");
                //System.out.println("pagamento ao medico gerado");
                novo();
                listaContaSelecionadosPorMedico = new ArrayList<>();
            } else {
                Mensagem.addMensagem(3, "Não foram encontrados registros.");
                novo();
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar efetuar a liberação!" + e);
            System.out.println(e);
        }
    }

    public void imprimirComprovantePagamentoMedico(Integer idSaque) {
        idSaqueImprimir = idSaque;
        RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Geral.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        if (retornaSeEChequeAvulso(idSaqueImprimir)) {
//            System.out.println("Cheques Avulsos");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Cheque_Avulso.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        } else if (ccEJB.retornaSeEAvulso(idSaqueImprimir)) {
//            System.out.println("Avulsos");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Avulso.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        } else {
//            System.out.println("Internações");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        }
    }

    public void imprimirComprovantePagamentoMedicoValorProcedimento(Integer idSaque) {
        idSaqueImprimir = idSaque;
        RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Geral_Valor_Procedimento.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        if (retornaSeEChequeAvulso(idSaqueImprimir)) {
//            System.out.println("Cheques Avulsos");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Cheque_Avulso.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        } else if (ccEJB.retornaSeEAvulso(idSaqueImprimir)) {
//            System.out.println("Avulsos");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Avulso.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        } else {
//            System.out.println("Internações");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        }
    }

    public void imprimirComprovantePagamentoMedicoValorProcedimentoDtProcedimento(Integer idSaque) {
        idSaqueImprimir = idSaque;
        RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Geral_Valor_Procedimento_DT_Procedimento.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        if (retornaSeEChequeAvulso(idSaqueImprimir)) {
//            System.out.println("Cheques Avulsos");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Cheque_Avulso.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        } else if (ccEJB.retornaSeEAvulso(idSaqueImprimir)) {
//            System.out.println("Avulsos");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico_Avulso.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        } else {
//            System.out.println("Internações");
//            RelatorioFactory.ComprovantePagamentoMedico("/WEB-INF/Relatorios/Comprovante_Recebimento_Medico.jasper", idSaqueImprimir, "C:/SMA/logo.png");
//        }
    }

    public boolean retornaSeEChequeAvulso(int idSaque) {
        List<ContaCorrente> lista = new ArrayList<>();
        lista = ccEJB.pesquisaItensSaque(ccEJB.selecionarPorID(idSaque));
        for (ContaCorrente cc : lista) {
            if (cc.getIdRepasse().getIdCheque().getIdProcedimento() != null) {
                return true;
            }
        }
        return false;
    }

    public Double retornaValorTotalListaContas(List<ContaCorrente> listaContas) {
        Double total = 0D;
        for (ContaCorrente cc : listaContas) {
            total += cc.getValor();
        }
        return total;
    }

    public void imprimir(Integer saque) {
        idSaqueImprimir = saque;
    }

    public boolean verificarCaixaAberto(Date dataCaixa) {
        Long l = ccEJB.verificaCaixaAberto(us.retornaUsuario(), dataCaixa);
        if (l > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificarTiposRepasses() {
        List<ContaCorrente> contasFechamento = new ArrayList<>();
        List<ContaCorrente> contasAvulso = new ArrayList<>();
        for (ContaCorrente cc : listaContaSelecionados) {
            if (cc.getIdCirurgia() != null) {
                contasFechamento.add(cc);
            } else if (cc.getIdRepasse().getIdCheque().getIdProcedimento() != null) {
                contasAvulso.add(cc);
            }
        }
        if (!contasFechamento.isEmpty() && !contasAvulso.isEmpty()) {
            return true;
        }
        return false;
    }

    public String verificarVencido(Date data) {
        String resultado = "";

        if (data != null) {
            if (data.before(new Date()) || data.equals(BCRUtils.addDia(new Date(), 2))) {
                resultado = "alert alert-danger";
            } else if (data.after(new Date())) {
                resultado = "alert alert-success";
            }
        }
        return resultado;
    }

    public boolean verificarChequeEstaNoCaixaCorrente(Repasse r) {

        if (!asEJB.carregarAssociacao().getPermitirPagamentoSemTransferir()) {
            usuario = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
           
            if (r.getIdCheque().getIdUsuario().getIdUsuario().equals(usuario.getIdUsuario())) {
                if (r.getIdCheque().getCompensado().equals("S")) {
                    if (r.getIdCaixa() != null) {
                        return r.getIdCaixa().getSituacao().equals("Aceito");
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        if (r.getIdTerceiro().getTerceiroMedHosp()) {
            return true;
        } else if (r.getIdCheque() != null) {
            return r.getIdCheque().getCompensado().equals("S");
        } else {
            return false;
        }
    }

    public void liberarRegistroContaCorrente(ContaCorrente cc) {
        try {
            cc.setSituacao("Liberado");
            ccEJB.AtualizarCC(cc);
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Não foi possível realizar a liberação. Entre em contato com o suporte." + e);
        }
    }

    public void atualizarDados(List<ContaCorrente> ccs, String tipo) {
        String data;
        DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        data = f.format(new Date());
        if (tipo.equals("S")) {
            try {
                for (ContaCorrente corrente : ccs) {
                    // Atualiza os dados do Caixa.

                    listaCaixa = new ArrayList<Caixa>();
                    listaCaixa = caixaEJB.pegaListaCaixaPorRepasse(corrente.getIdRepasse());
                    for (Caixa c : listaCaixa) {
                        c.setStatus("Baixado");
                        c.setObservacao("Repassado em: " + data + ". Usuário: " + contaCorrente.getIdUsuario().getUsuario());
                        caixaEJB.atualizarCaixa(c);
                    }

                    // Atualiza os dados do Repasse.
                    repasse = new Repasse();
                    repasse = repasseEJB.selecionarPorID(corrente.getIdRepasse().getIdRepasse());
                    repasse.setSacado("S");
                    repasseEJB.Salvar(repasse);

                    liberarRegistroContaCorrente(corrente);
                }

            } catch (Exception e) {
                Mensagem.addMensagem(3, "Erro ao tentar efetuar o pagamento!" + e);
                System.out.println(e);
            }
        } else {
            try {
                // CHARLES 07072017
                // Gera estorno no CAIXA do usuário

                for (ContaCorrente corrente : ccs) {
                    caixa = new Caixa();
                    caixa.setHistorico("Estorno realizado pelo usuário " + corrente.getIdUsuario().getUsuario() + " no valor de: R$ " + corrente.getValor()
                            + ". Favorecido: " + corrente.getIdTerceiro().getNome());
                    caixa.setStatus("Baixado");
                    caixa.setIdRepasse(corrente.getIdRepasse());
                    caixa.setDtCaixa(new Date());
                    caixa.setTipoLancamento('C');
                    caixa.setValor(corrente.getValor());
                    caixa.setIdCirurgia(corrente.getIdCirurgia());
                    caixa.setIdUsuario(uEJB.retornaUsuarioDaSessao());
                    caixaEJB.atualizarCaixa(caixa);

//                    for (Caixa c : listaCaixa) {
//                        c.setStatus("Aceito");
//                        c.setObservacao("Estornado em: " + data + ". Usuário: " + contaCorrente.getIdUsuario().getUsuario());
//                        caixaEJB.atualizarCaixa(c);
//                    }
                    // Atualiza os dados do Repasse.
                    repasse = new Repasse();
                    repasse = repasseEJB.selecionarPorID(corrente.getIdRepasse().getIdRepasse());
                    repasse.setSacado(null);
                    repasseEJB.Salvar(repasse);
                }
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Erro ao tentar efetuar o estorno!" + e);
                System.out.println(e);
            }

        }
    }

    public void carregarTransferenciasParaPagamentos() {
        listaCaixaTransferencia = retornaListadeTransferenciaPorData();
    }

    public List<Caixa> retornaListadeTransferenciaPorData() {
        List<Caixa> listaCaixasPorUsuario = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dataInicio;
        String dataFim;
        dataInicio = df.format(padtIni);
        dataFim = df.format(padtFim);
        listaCaixasPorUsuario = muEJB.listarCaixasAgrupadosPorUsuarioRecebidos(uEJB.retornaUsuarioDaSessao().getIdUsuario(), dataInicio, dataFim);
        return listaCaixasPorUsuario;
    }

    public String formataMoney(Double valor) {
        return formataDinheiro.format(valor);
    }

    public String getSaldoFormatado() {
        return saldoFormatado;
    }

    public void setSaldoFormatado(String saldoFormatado) {
        this.saldoFormatado = saldoFormatado;
    }

    public Double getSaldoInicial() {
        return saldoInicial;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public Terceiro getTerceiro() {
        return terceiro;
    }

    public List<ContaCorrente> getListaItensSaque() {
        return listaItensSaque;
    }

    public void setListaItensSaque(List<ContaCorrente> listaItensSaque) {
        this.listaItensSaque = listaItensSaque;
    }

    public void setTerceiro(Terceiro terceiro) {
        this.terceiro = terceiro;
    }

    public List<Terceiro> getTerceirosAcarregar() {
        return terceirosAcarregar;
    }

    public void setTerceirosAcarregar(List<Terceiro> terceirosAcarregar) {
        this.terceirosAcarregar = terceirosAcarregar;
    }

    public List<ContaCorrente> getListaSaque() {
        return listaSaque;
    }

    public void setListaSaque(List<ContaCorrente> listaSaque) {
        this.listaSaque = listaSaque;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void setSaldoInicial(Double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public NumberFormat getFormataDinheiro() {
        return formataDinheiro;
    }

    public void setFormataDinheiro(NumberFormat formataDinheiro) {
        this.formataDinheiro = formataDinheiro;
    }

    public List<Terceiro> listaTerceiros() {
        return tEJB.listarTerceiro();
    }

    public List<Terceiro> listaTerceirosOrdenados() {
        return tEJB.listarTerceirosOrdenadoPorNome();
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public List<Paciente> getPacientesAcarregar() {
        return pacientesAcarregar;
    }

    public void setPacientesAcarregar(List<Paciente> pacientesAcarregar) {
        this.pacientesAcarregar = pacientesAcarregar;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public List<ContaCorrente> getListaContaCorrente() {
        return listaContaCorrente;
    }

    public void setListaContaCorrente(List<ContaCorrente> listaContaCorrente) {
        this.listaContaCorrente = listaContaCorrente;
    }

    public Date getPdtINILancamento() {
        return pdtINILancamento;
    }

    public void setPdtINILancamento(Date pdtINILancamento) {
        this.pdtINILancamento = pdtINILancamento;
    }

    public Date getPdtINIEstorno() {
        return pdtINIEstorno;
    }

    public void setPdtINIEstorno(Date pdtINIEstorno) {
        this.pdtINIEstorno = pdtINIEstorno;
    }

    public Date getPdtINIRetirada() {
        return pdtINIRetirada;
    }

    public void setPdtINIRetirada(Date pdtINIRetirada) {
        this.pdtINIRetirada = pdtINIRetirada;
    }

    public Date getPdtFIMLancamento() {
        return pdtFIMLancamento;
    }

    public void setPdtFIMLancamento(Date pdtFIMLancamento) {
        this.pdtFIMLancamento = pdtFIMLancamento;
    }

    public Date getPdtFIMRetirada() {
        return pdtFIMRetirada;
    }

    public void setPdtFIMRetirada(Date pdtFIMRetirada) {
        this.pdtFIMRetirada = pdtFIMRetirada;
    }

    public Date getPdtFIMEstorno() {
        return pdtFIMEstorno;
    }

    public void setPdtFIMEstorno(Date pdtFIMEstorno) {
        this.pdtFIMEstorno = pdtFIMEstorno;
    }

    public String getPstatus() {
        return pstatus;
    }

    public void setPstatus(String pstatus) {
        this.pstatus = pstatus;
    }

    public String getPsituacao() {
        return psituacao;
    }

    public void setPsituacao(String psituacao) {
        this.psituacao = psituacao;
    }

    public String getPidTerceiro() {
        return pidTerceiro;
    }

    public void setPidTerceiro(String pidTerceiro) {
        this.pidTerceiro = pidTerceiro;
    }

    public String getPidPaciente() {
        return pidPaciente;
    }

    public void setPidPaciente(String pidPaciente) {
        this.pidPaciente = pidPaciente;
    }

    public String getCredDeb() {
        return CredDeb;
    }

    public void setCredDeb(String CredDeb) {
        this.CredDeb = CredDeb;
    }

    public Double getSaldoAnterior() {
        return saldoAnterior;
    }

    public Double getCredito() {
        return credito;
    }

    public void setCredito(Double credito) {
        this.credito = credito;
    }

    public Double getDebito() {
        return debito;
    }

    public void setDebito(Double debito) {
        this.debito = debito;
    }

    public void setSaldoAnterior(Double SaldoAnterior) {
        this.saldoAnterior = SaldoAnterior;
    }

    public String getFiltros() {
        return Filtros;
    }

    public void setFiltros(String Filtros) {
        this.Filtros = Filtros;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Double getSaldoFinal() {
        return saldoFinal;
    }

    public void setSaldoFinal(Double saldoFinal) {
        this.saldoFinal = saldoFinal;
    }

    public Double getLiberado() {
        return liberado;
    }

    public void setLiberado(Double liberado) {
        this.liberado = liberado;
    }

    public Double getNaoLiberado() {
        return naoLiberado;
    }

    public void setNaoLiberado(Double naoLiberado) {
        this.naoLiberado = naoLiberado;
    }

    public Double getRetirado() {
        return retirado;
    }

    public void setRetirado(Double retirado) {
        this.retirado = retirado;
    }

    public Double getNaoRetirado() {
        return naoRetirado;
    }

    public void setNaoRetirado(Double naoRetirado) {
        this.naoRetirado = naoRetirado;
    }

    public List<ContaCorrente> getListaContaSelecionados() {
        return listaContaSelecionados;
    }

    public void setListaContaSelecionados(List<ContaCorrente> listaContaSelecionados) {
        this.listaContaSelecionados = listaContaSelecionados;
    }

    public Double getTotalSelecionado() {
        return totalSelecionado;
    }

    public void setTotalSelecionado(Double totalSelecionado) {
        this.totalSelecionado = totalSelecionado;
    }

    public boolean isSelecionado() {
        return selecionado;
    }

    public void setSelecionado(boolean selecionado) {
        this.selecionado = selecionado;
    }

    public Double getSaque() {
        return saque;
    }

    public void setSaque(Double saque) {
        this.saque = saque;
    }

    public Double getPago() {
        return pago;
    }

    public void setPago(Double pago) {
        this.pago = pago;
    }

    public void setarVariaveis() {
        liberado = 0D;
        vencido = 0D;
        naoLiberado = 0D;
        retirado = 0D;
        naoRetirado = 0D;
        saque = 0D;
        pago = 0D;
        estorno = 0D;
        credito = 0D;
        debito = 0D;
        saldo = 0D;
        saldoFinal = 0D;
        saldoAnterior = 0D;
    }

    public Double getSomaValores(List<ContaCorrente> listaContas) {
        Double total = new Double(0);
        for (ContaCorrente c : listaContas) {
            total += c.getValor();
        }
        return total;
    }

    public void setarDatas(String campo) {
        dataAtual = Calendar.getInstance();
        primeiroDia = Calendar.getInstance();
        ultimoDia = Calendar.getInstance();
        //1º dia do mês atual  
        primeiroDia.add(Calendar.DAY_OF_MONTH, -dataAtual.get(Calendar.DAY_OF_MONTH));
        primeiroDia.add(Calendar.DAY_OF_YEAR, 1);
        //Ultimo dia do mês atual  
        ultimoDia.add(Calendar.MONTH, 1);
        ultimoDia.add(Calendar.DAY_OF_MONTH, -dataAtual.get(Calendar.DAY_OF_MONTH));
        if (campo.equals("Lancamento")) {
            pdtINILancamento = primeiroDia.getTime();
            pdtFIMLancamento = ultimoDia.getTime();
        } else if ((campo.equals("Retirada"))) {
            pdtINILancamento = null;
            pdtFIMLancamento = null;
            pdtINIRetirada = primeiroDia.getTime();
            pdtFIMRetirada = ultimoDia.getTime();
        }
    }

    public Double getTotalItensSaque() {
        return totalItensSaque;
    }

    public void setTotalItensSaque(Double totalItensSaque) {
        this.totalItensSaque = totalItensSaque;
    }

    public Double getEstorno() {
        return estorno;
    }

    public void setEstorno(Double estorno) {
        this.estorno = estorno;
    }

    public Usuario pesquisarUsuario() {
        return uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
    }

    public List<ContaCorrente> listaMovimentacaoPorUsuario() {
        return ccEJB.listarContaCorrentePorUsuario(pesquisarUsuario());
    }
//    ============================================================================
//    tela de medico
    private List<ContaCorrenteEstatica> contasCorrentesMedicos = new ArrayList<ContaCorrenteEstatica>();
    private Date padtIni;
    private Date padtFim;
    private String paIdPaciente;
    private String paidCirurgia;
    private String paidprocedimentofiltro;
    private String paTipo;
    private List<ContaCorrente> extratos = new ArrayList<ContaCorrente>();
    private String paTipoData;
    private String paStatus;
    private String paSituacao;

    public List<ContaCorrenteEstatica> getContasCorrentesMedicos() {
        return contasCorrentesMedicos;
    }

    public void setContasCorrentesMedicos(List<ContaCorrenteEstatica> contasCorrentesMedicos) {
        this.contasCorrentesMedicos = contasCorrentesMedicos;
    }

    public Date getPadtIni() {
        return padtIni;
    }

    public void setPadtIni(Date padtIni) {
        this.padtIni = padtIni;
    }

    public Date getPadtFim() {
        return padtFim;
    }

    public void setPadtFim(Date padtFim) {
        this.padtFim = padtFim;
    }

    public String getPaIdPaciente() {
        return paIdPaciente;
    }

    public void setPaIdPaciente(String paIdPaciente) {
        this.paIdPaciente = paIdPaciente;
    }

    public String getPaidCirurgia() {
        return paidCirurgia;
    }

    public void setPaidCirurgia(String paidCirurgia) {
        this.paidCirurgia = paidCirurgia;
    }

    public String getPaidprocedimentofiltro() {
        if (paidprocedimentofiltro == null) {
            paidprocedimentofiltro = "";
        }
        return paidprocedimentofiltro;
    }

    public void setPaidprocedimentofiltro(String paidprocedimentofiltro) {
        this.paidprocedimentofiltro = paidprocedimentofiltro;
    }

    public String getPaTipo() {
        return paTipo;
    }

    public void setPaTipo(String paTipo) {
        this.paTipo = paTipo;
    }

    public List<ContaCorrente> getExtratos() {
        return extratos;
    }

    public void setExtratos(List<ContaCorrente> extratos) {
        this.extratos = extratos;
    }

    public String getPaTipoData() {
        return paTipoData;
    }

    public void setPaTipoData(String paTipoData) {
        this.paTipoData = paTipoData;
    }

    public String getPaStatus() {
        return paStatus;
    }

    public void setPaStatus(String paStatus) {
        this.paStatus = paStatus;
    }

    public String getPaSituacao() {
        return paSituacao;
    }

    public void setPaSituacao(String paSituacao) {
        this.paSituacao = paSituacao;
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public static Date addAno(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.YEAR, qtd);
        return cal.getTime();
    }

    public TipoInternacao selecionarTipoInternacao(String s) {
        return (TipoInternacao) cirEJB.retornaTipoInternacao(s);
    }

    public void pesquisarContaCorrentePorMedico(Integer id) {
        if (paTipo == null || paTipo.equals("null") || paTipo.isEmpty()) {
            paTipo = "%";
        }
        if (tipo == null || tipo.equals("null") || tipo.isEmpty()) {
            tipo = "%";
        } else {
            System.out.println(tipo);
            tipoInternacao = selecionarTipoInternacao(tipo);
            System.out.println(tipoInternacao.getDescricao());
            tipo = tipoInternacao.getIdTipoInternacao().toString();
        }

        if (paidCirurgia == null || paidCirurgia.equals("null") || paidCirurgia.isEmpty()) {
            paidCirurgia = "%";
        }
        if (paIdPaciente == null || paIdPaciente.equals("null") || paIdPaciente.isEmpty()) {
            paIdPaciente = "%";
        }
        if (paidprocedimentofiltro == null || paidprocedimentofiltro.equals("null") || paidprocedimentofiltro.isEmpty()) {
            paidprocedimentofiltro = "%";
        }
        contasCorrentesMedicos = new ArrayList<ContaCorrenteEstatica>();
        contasCorrentesMedicos = ccEJB.pesquisaContaCorrentePorMedico(id, padtIni, padtFim, paidprocedimentofiltro, paIdPaciente, paTipo, paidCirurgia, tipo);
        if (contasCorrentesMedicos.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrado registros.");
        }
        calcularValores();
    }
    // Totalizadores Demostrativo médico.
    Double valorHospital, valorTerceiro, valorTotal;
    Integer qntRegistros;

    public void calcularValores() {
        credito = 0D;
        naoLiberado = 0D;
        liberado = 0D;
        retirado = 0D;
        naoRetirado = 0D;
        valorTerceiro = 0D;
        valorTotal = 0D;
        qntRegistros = 0;
        for (ContaCorrenteEstatica cce : contasCorrentesMedicos) {
            credito = credito + cce.getValor();
            if (cce.getContaCorrente().getStatus().equals("Não Retirado")) {
                naoRetirado = naoRetirado + cce.getValor();
            }
            if (cce.getContaCorrente().getStatus().equals("Retirado")) {
                retirado = retirado + cce.getValor();
            }
            if (cce.getContaCorrente().getSituacao().equals("Liberado")) {
                liberado = liberado + cce.getValor();
            }
            if (cce.getContaCorrente().getSituacao().equals("Não Liberado")) {
                naoLiberado = naoLiberado + cce.getValor();
            }
            qntRegistros++;
        }
        System.out.println(naoRetirado);
        System.out.println(retirado);
        System.out.println(naoLiberado);
        System.out.println(liberado);
        System.out.println(qntRegistros);
    }

    public List<ContaCorrente> pesquisarMovimentoDeContaCorrente(Integer idTerceiro, Integer idCirurgia) {
        return ccEJB.listaMovimentoContaCorrentePorMedicoCirurgia(idTerceiro, idCirurgia);
    }

    public Double calcularValoresTotais(List<ContaCorrente> lista) {
        Double total = 0D;
        for (ContaCorrente cc : lista) {
            total += cc.getValor();
        }
        return total;
    }

    public Number retornaPagamentosPorFechamento(Integer idFechamento) {
        return chEJB.retornaPagamentosPorFechamento(idFechamento);
    }

    public void carregarPacientesLazyModel() {
        model = new LazyDataModel<Paciente>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Paciente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'" + filterValue + "%'");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(pEJB.contarPacientesRegistro(Clausula).toString()));
                listaPacientesACarregar = pEJB.listarPacientesLazyModeWhere(first, pageSize, Clausula);
                return listaPacientesACarregar;
            }
        };
    }

    public void pesquisarExtratos(Integer idTerceiro) {
        String clausula = "";
        String idCirurgia = "";
        Integer idPacienteInt, idCirurgiaInt, idProcedimentoInt;
//        06082016
//        Código comentado devido ao uso da logica que "Repasses/Contas Correntes são derivados de fechamentos, por isso, 
//        não podem ser mostrados avulsos. Além disso, o desconto aplicado no item de repasse de cada médico é para todos os itens, não somente a um repasse/conta corrente demonstrado"

//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//        String dataIni;
//        String dataFim;
//        if (padtIni == null) {
//            dataIni = df.format(addAno(new Date(), -50));
//        } else {
//            dataIni = df.format(padtIni);
//        }
//        if (padtFim == null) {
//            dataFim = df.format(addAno(new Date(), +50));
//        } else {
//            dataFim = df.format(padtFim);
//        }
//        clausula = " and c.idRepasse.dtVencimento BETWEEN '" + dataIni + "' and '" + dataFim + "'";
        if (paidCirurgia == null || "".equals(paidCirurgia)) {
            idCirurgia = "%";
            idCirurgiaInt = null;
        } else {
            idCirurgia = paidCirurgia;
            idCirurgiaInt = Integer.parseInt(paidCirurgia);
        }

        if ("".equals(paIdPaciente) || paIdPaciente == null || paIdPaciente.equals("%")) {
            paIdPaciente = "%";
            idPacienteInt = null;
        } else {
            idPacienteInt = Integer.parseInt(paIdPaciente);
        }

        if ("".equals(paidprocedimentofiltro) || paidprocedimentofiltro == null) {
            paidprocedimentofiltro = "%";
            idProcedimentoInt = null;
        } else {
            idProcedimentoInt = Integer.parseInt(paidprocedimentofiltro);

        }
//        if (paStatus == "" || paStatus == null) {
//            paStatus = "%";
//        }
//        if (paSituacao == "" || paSituacao == null) {
//            paSituacao = "%";
//        }
        //extratos = ccEJB.pesquisarContaCorrentePorTerceiro(idTerceiro, clausula, idCirurgia, paIdPaciente, paidprocedimentofiltro, paStatus, paSituacao);
        listaCirurgiaAPesquisar = new ArrayList<>();
        listaCirurgiaAPesquisar = cirEJB.pesquisarFechamentosQuePossuemRepassesMedico(padtIni, padtFim, idPacienteInt, idCirurgiaInt, idProcedimentoInt, null, null, true);
        if (listaCirurgiaAPesquisar.isEmpty()) {
            Mensagem.addMensagem(3, "Não foi encontrado nenhum registro!");
        }
    }

    public List<ContaCorrente> pesquisarExtratoPorFechamento(Integer idCirurgia, Integer idTerceiro) {
        return ccEJB.listaMovimentoContaCorrentePorMedicoCirurgia(idTerceiro, idCirurgia);
    }

    public void pesquisarOrcamentos(Terceiro terceiro) {
        Integer sai = 0;
        Integer stat = 0;
        Integer cir = 0;

        if (paidprocedimentofiltro != null) {
            sai = Integer.parseInt(paidprocedimentofiltro);
        }
        if (pstatus != null) {
            stat = Integer.parseInt(pstatus);
        }
        if (!paidCirurgia.isEmpty()) {
            cir = Integer.parseInt(paidCirurgia);
        }
        listaDeOrctos = oEJB.pesquisarOrcamentos2(padtIni, padtFim, nomePacientePesquisa, sai, terceiro.getIdTerceiro(), stat, cir);
        if (listaDeOrctos.isEmpty()) {
            Mensagem.addMensagem(3, "Não foram encontrados registros.");
        }
    }

    public void imprimirOrcamentos(Terceiro terceiro) {
        List<Orcto> orcs = new ArrayList<Orcto>();
        for (Orcamento o : listaDeOrctos) {
            Orcto oo = new Orcto();
            oo.setId_orcto(o.getIdOrcamento());
            oo.setPaciente(o.getIdPaciente().getNome());
            oo.setProcedimento(o.getIdProcedimento().getDescricao());
            oo.setTerceiro(o.getIdTerceiro().getNome());
            oo.setDT_ORCAMENTO(o.getDtOrcamento());
            oo.setDT_PREVISAO(o.getDtPrevisao());
            oo.setSITUACAO(o.getIdStatus().getDescricao());
            oo.setTotal(o.getValor());
            oo.setDescricao("");
            orcs.add(oo);
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("logo", "C:/SMA/logo.png");
        map.put("pData_Inicio", padtIni);
        map.put("pData_Fim", padtFim);
        map.put("pStatus", pstatus);
        map.put("pProcedimento", paidprocedimentofiltro);
        RelatorioFactory.imprimirOrctosMedicos("/WEB-INF/Relatorios/reportOrcamentosMedicos.jasper", map, orcs);
    }

    public void imprimirAtendimentoPorCirurgia(Integer idTerceiro, Integer idCirurgia) {
        RelatorioFactory.atendimentosPorTerceiro("/WEB-INF/Relatorios/reportAtendimentosRealizadosPorTerceiro.jasper", idCirurgia, idTerceiro, "C:/SMA/logo.png");
    }

    public void imprimirAtendimentos(Integer idTerceiro, Date dtInicio, Date dtFinal) {
        RelatorioFactory.atendimentos("/WEB-INF/Relatorios/reportAtendimentosRealizados.jasper", idTerceiro, dtInicio, dtFinal, "C:/SMA/logo.png");
    }

    public void imprimirAtendimentosResumo(Integer idTerceiro, Date dtInicio, Date dtFinal, String status) {
        String paciente;
        if (paStatus == null || paStatus.equals("") || paStatus.isEmpty()) {
            paStatus = "%";
        }
        paciente = nomePacientePesquisa == null ? "%" : paIdPaciente;
        //RelatorioFactory.atendimentosResumo("/WEB-INF/Relatorios/reportAtendimentosRealizadosResumo.jasper", idTerceiro, paciente, dtInicio, dtFinal, "C:/SMA/logo.png", status);
        if (System.getProperty("os.name").equals("Linux")) {
            RelatorioFactory.atendimentosResumo("/WEB-INF/Relatorios/reportAtendimentosRealizadosResumo.jasper", idTerceiro, paciente, dtInicio, dtFinal, "/media/charles/D48C82D38C82B012/SMA/logo.png", status);
        } else {
            RelatorioFactory.atendimentosResumo("/WEB-INF/Relatorios/reportAtendimentosRealizadosResumo.jasper", idTerceiro, paciente, dtInicio, dtFinal, "C:/SMA/logo.png", status);
        }

    }

    public void imprimirExtratoPorTerceiro(Integer idTerceiro) {
        if (paidCirurgia == null || paidCirurgia.equals("") || paidCirurgia.isEmpty()) {
            paidCirurgia = "%";
        }
        if (paIdPaciente == null || paIdPaciente.equals("") || paIdPaciente.isEmpty()) {
            paIdPaciente = "%";
        }
        if (paidprocedimentofiltro == null || paidprocedimentofiltro.equals("") || paidprocedimentofiltro.isEmpty()) {
            paidprocedimentofiltro = "%";
        }
        if (paStatus == null || paStatus.equals("") || paStatus.isEmpty()) {
            paStatus = "%";
        }
        if (paSituacao == null || paSituacao.equals("") || paSituacao.isEmpty()) {
            paSituacao = "%";
        }

        if (padtIni == null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.add(gc.MONTH, -1);
            Date d2 = gc.getTime();
            padtIni = d2;
        }
        if (padtFim == null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.add(gc.MONTH, 1);
            Date d1 = gc.getTime();
            padtFim = d1;
        }

        RelatorioFactory.extratosPorTerceiro("/WEB-INF/Relatorios/reportExtratoPorTerceiro.jasper", paidCirurgia, paIdPaciente, idTerceiro.toString(), "%", getPadtIni(), getPadtFim(), paidprocedimentofiltro, "%", paStatus, paSituacao, "C:/SMA/logo.png");
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getPdtINIPrevisao() {
        return pdtINIPrevisao;
    }

    public void setPdtINIPrevisao(Date pdtINIPrevisao) {
        this.pdtINIPrevisao = pdtINIPrevisao;
    }

    public Date getPdtFIMPrevisao() {
        return pdtFIMPrevisao;
    }

    public void setPdtFIMPrevisao(Date pdtFIMPrevisao) {
        this.pdtFIMPrevisao = pdtFIMPrevisao;
    }

    public List<Paciente> getListaPacientesACarregar() {
        return listaPacientesACarregar;
    }

    public void setListaPacientesACarregar(List<Paciente> listaPacientesACarregar) {
        this.listaPacientesACarregar = listaPacientesACarregar;
    }

    public LazyDataModel<Paciente> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Paciente> model) {
        this.model = model;
    }

    public Double getValorHospital() {
        return valorHospital;
    }

    public void setValorHospital(Double valorHospital) {
        this.valorHospital = valorHospital;
    }

    public Double getValorTerceiro() {
        return valorTerceiro;
    }

    public void setValorTerceiro(Double valorTerceiro) {
        this.valorTerceiro = valorTerceiro;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Integer getIdSaqueImprimir() {
        return idSaqueImprimir;
    }

    public void setIdSaqueImprimir(Integer idSaqueImprimir) {
        this.idSaqueImprimir = idSaqueImprimir;
    }

    public List<ContaCorrente> getListaItensSelecionadosEstornar() {
        return listaItensSelecionadosEstornar;
    }

    public void setListaItensSelecionadosEstornar(List<ContaCorrente> listaItensSelecionadosEstornar) {
        this.listaItensSelecionadosEstornar = listaItensSelecionadosEstornar;
    }

    public List<ContaCorrente> getListaItensAEstornar() {
        return listaItensAEstornar;
    }

    public void setListaItensAEstornar(List<ContaCorrente> listaItensAEstornar) {
        this.listaItensAEstornar = listaItensAEstornar;
    }

    public ContaCorrente getContaCorrenteEstorno() {
        return contaCorrenteEstorno;
    }

    public void setContaCorrenteEstorno(ContaCorrente contaCorrenteEstorno) {
        this.contaCorrenteEstorno = contaCorrenteEstorno;
    }

    public Date getDtInicioResumo() {
        return dtInicioResumo;
    }

    public void setDtInicioResumo(Date dtInicioResumo) {
        this.dtInicioResumo = dtInicioResumo;
    }

    public Date getDtFinalResumo() {
        return dtFinalResumo;
    }

    public void setDtFinalResumo(Date dtFinalResumo) {
        this.dtFinalResumo = dtFinalResumo;
    }

    public String getNomePacientePesquisa() {
        return nomePacientePesquisa;
    }

    public void setNomePacientePesquisa(String nomePacientePesquisa) {
        this.nomePacientePesquisa = nomePacientePesquisa;
    }

    public List<ContaCorrenteEstatica> getListaResumo() {
        return listaResumo;
    }

    public void setListaResumo(List<ContaCorrenteEstatica> listaResumo) {
        this.listaResumo = listaResumo;
    }

    public List<Orcamento> getListaDeOrctos() {
        return listaDeOrctos;
    }

    public void setListaDeOrctos(List<Orcamento> listaDeOrctos) {
        this.listaDeOrctos = listaDeOrctos;
    }

    public String getPacientePesquisa() {
        return pacientePesquisa;
    }

    public void setPacientePesquisa(String pacientePesquisa) {
        this.pacientePesquisa = pacientePesquisa;
    }

    public PieChartModel getGfTipos() {
        return gfTipos;
    }

    public void setGfTipos(PieChartModel gfTipos) {
        this.gfTipos = gfTipos;
    }

    public PieChartModel getGfValores() {
        return gfValores;
    }

    public void setGfValores(PieChartModel gfValores) {
        this.gfValores = gfValores;
    }

    public Date getDtAtual() {
        return dtAtual;
    }

    public void setDtAtual(Date dtAtual) {
        this.dtAtual = dtAtual;
    }

    public Double getVencido() {
        return vencido;
    }

    public void setVencido(Double vencido) {
        this.vencido = vencido;
    }

    public Associacao getAss() {
        return ass;
    }

    public void setAss(Associacao ass) {
        this.ass = ass;
    }

    public List<Cirurgia> getListaCirurgiaAPesquisar() {
        return listaCirurgiaAPesquisar;
    }

    public void setListaCirurgiaAPesquisar(List<Cirurgia> listaCirurgiaAPesquisar) {
        this.listaCirurgiaAPesquisar = listaCirurgiaAPesquisar;
    }

    public ContaCorrente getContaCorrenteSaque() {
        return contaCorrenteSaque;
    }

    public void setContaCorrenteSaque(ContaCorrente contaCorrenteSaque) {
        this.contaCorrenteSaque = contaCorrenteSaque;
    }

    public Caixa getLotePesquisa() {
        return lotePesquisa;
    }

    public void setLotePesquisa(Caixa lotePesquisa) {
        this.lotePesquisa = lotePesquisa;
    }

    public List<Caixa> getListaCaixa() {
        return listaCaixa;
    }

    public void setListaCaixa(List<Caixa> listaCaixa) {
        this.listaCaixa = listaCaixa;
    }

    public List<Caixa> getListaCaixaTransferencia() {
        return listaCaixaTransferencia;
    }

    public void setListaCaixaTransferencia(List<Caixa> listaCaixaTransferencia) {
        this.listaCaixaTransferencia = listaCaixaTransferencia;
    }

    public List<ContaCorrente> getListaSaques() {
        return listaSaques;
    }

    public void setListaSaques(List<ContaCorrente> listaSaques) {
        this.listaSaques = listaSaques;
    }

    public List<ContaCorrente> getListaContaCorrente2() {
        return listaContaCorrente2;
    }

    public void setListaContaCorrente2(List<ContaCorrente> listaContaCorrente2) {
        this.listaContaCorrente2 = listaContaCorrente2;
    }

    public LazyDataModel<ContaCorrente> getListaContaCorrenteLazy() {
        return listaContaCorrenteLazy;
    }

    public void setListaContaCorrenteLazy(LazyDataModel<ContaCorrente> listaContaCorrenteLazy) {
        this.listaContaCorrenteLazy = listaContaCorrenteLazy;
    }

    public String getLoteOrigemPesquisa() {
        return loteOrigemPesquisa;
    }

    public void setLoteOrigemPesquisa(String loteOrigemPesquisa) {
        this.loteOrigemPesquisa = loteOrigemPesquisa;
    }

    public boolean isManterItensSelecionados() {
        return manterItensSelecionados;
    }

    public void setManterItensSelecionados(boolean manterItensSelecionados) {
        this.manterItensSelecionados = manterItensSelecionados;
    }

    public List<ContaCorrente> getListaContaSelecionadosPorMedico() {
        return listaContaSelecionadosPorMedico;
    }

    public void setListaContaSelecionadosPorMedico(List<ContaCorrente> listaContaSelecionadosPorMedico) {
        this.listaContaSelecionadosPorMedico = listaContaSelecionadosPorMedico;
    }

}
