/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AlertaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.DescricaoExameEJB;
import br.com.ux.controller.ExameEJB;
import br.com.ux.controller.FormaPagamentoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Alerta;
import br.com.ux.model.Cheque;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.DescricaoExame;
import br.com.ux.model.Exame;
import br.com.ux.model.FormaPagamento;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Terceiro;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author PokerFace
 */
@ManagedBean
@ViewScoped
public class ExameMB implements Serializable {

    @EJB
    ExameEJB eEJB;
    private Exame exame;
    UsuarioSessao us = new UsuarioSessao();
    private List<Exame> listaExames;
    private List<ContaCorrente> listaContaCorrente;
    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;
    private List<Paciente> pacientesAcarregar;
    @EJB
    TerceiroEJB tEJB;
    private Terceiro terceiro;
    private Terceiro terceiroItem;
    @EJB
    DescricaoExameEJB dEJB;
    private DescricaoExame descricaoExame;
    private List<DescricaoExame> listaDescricaoExames;
    @EJB
    FormaPagamentoEJB fpEJB;
    private FormaPagamento formaPagamento;
    private List<FormaPagamento> listaFormaPagamentos;
    @EJB
    ChequeEJB chequeEJB;
    private Cheque cheque;
    LazyDataModel<Exame> model;
    @EJB
    RepasseEJB repasseEJB;
    private Repasse repasse;
    private List<Repasse> parcelas;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    AlertaEJB alertaEJB;
    // Controle de tipo de lançamento //
    private String tipo;

    public ExameMB() {
        novo();
        carregarModeloLazy();
    }

    public void novo() {
        exame = new Exame();
        paciente = new Paciente();
        terceiro = new Terceiro();
        descricaoExame = new DescricaoExame();
        formaPagamento = new FormaPagamento();
        listaExames = new ArrayList<Exame>();
        pacientesAcarregar = new ArrayList<Paciente>();
        listaDescricaoExames = new ArrayList<DescricaoExame>();
        listaFormaPagamentos = new ArrayList<FormaPagamento>();
        zeraCampos();
        tipo = "E";
    }

    public void zeraCampos() {
        totalDesconto = 0D;
        totalParcela = 0D;

    }

    public void salvar() {
        if (validarCamposObrigatorios()) {
            try {
                if (paciente.getIdPaciente() != null) {
                    exame.setIdPaciente(paciente);
                }
                if (terceiro.getIdTerceiro() != null) {
                    exame.setIdTerceiro(terceiro);
                }
                if (descricaoExame.getIdDescricaoExame() != null) {
                    exame.setIdDescricaoExame(descricaoExame);
                }

                exame.setTipoLancamento(tipo);
                eEJB.Salvar(exame, us.retornaUsuario());
                gerarAlerta();
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
                
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        }
    }

    public List<Exame> carregarExames() {
        return eEJB.listarExames();
    }

    public void gerarAlerta() {
        List<Usuario> usuariosPorPapel = new ArrayList<Usuario>();
        usuariosPorPapel = uEJB.usuariosPorRecebimento();
        for (Usuario usu : usuariosPorPapel) {
            System.out.println(usu.getUsuario());
            String descricao = tipo == "E" ? "Receber exame nº: " + exame.getIdExame() + ". Paciente: " + exame.getIdPaciente().getNome() + ". Valor: R$ " + exame.getValor() + " " : "Receber consulta nº: " + exame.getIdExame() + "Paciente: " + exame.getIdPaciente().getNome() + ". Valor: R$" + exame.getValor() + ".";
            Usuario usuario = new Usuario();
            usuario = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
            Alerta a = new Alerta();
            a.setData(new Date());
            a.setDescricao(descricao);
            a.setIdExame(exame);
            a.setIdUsuario(usuario);
            a.setIdUsuarioDestinatario(usu);
            a.setStatus("A");
            a.setVisto("N");
            a.setTipo(tipo);
            alertaEJB.Salvar(a, usuario.getUsuario());
        }
    }

    public void excluir(Exame exame) {
        try {
            eEJB.Excluir(exame, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("excluir");
            novo();
            BCRUtils.ResetarDatatableFiltros("formDescricaoExame:dtl");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar excluir!" + e);
        }
    }
   
    public void cancelar(Exame exame) {
        try {
            exame.setSituacao("Cancelado");
            eEJB.Salvar(exame, us.retornaUsuario());
            Mensagem.addMensagem(1, "Exame cancelado!");
            novo();
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar cancelar!" + e);
        }
    }

    public void addMessage() {
        String mensagem = tipo.equals("E") ? "Informe os dados do exame." : "Informe os dados da consulta.";
        Mensagem.addMensagem(1, mensagem);
    }

    public Exame selecionarPorID(Integer id) {
        exame = eEJB.selecionarPorID(id, us.retornaUsuario());
        paciente = exame.getIdPaciente();
        formaPagamento = exame.getIdForma();
        terceiro = exame.getIdTerceiro();
        descricaoExame = exame.getIdDescricaoExame();
        parcelas = exame.getRepasseList();
        calcularTotalRepasse();
        return exame;
    }

    public void definirData() {
        if (exame.getTipoPagamento().equals("V")) {
            exame.setDtRecebimento(new Date());
            try {
                formaPagamento = fpEJB.selecionarPadrao();
            } catch (Exception e) {
                Mensagem.addMensagem(1, "Não foram encontradas formas de pagamento cadastradas.");
            }
        } else {
            formaPagamento = fpEJB.selecionarPorID(exame.getIdForma().getIdForma(), us.retornaUsuario());
            exame.setDtRecebimento(null);
        }
    }

    public String retornaDialogo(String Tipo) {
        if (Tipo.equals("Cheque")) {
            return "PF('dlgLancarCheque').show()";
        } else {
            return "";
        }
    }

    public void carregarModeloLazy() {
        model = new LazyDataModel<Exame>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Exame> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(eEJB.contarExamesRegistro(Clausula).toString()));

                listaExames = eEJB.listarExamesLazy(first, pageSize, Clausula);
                return listaExames;
            }
        };
    }

    public void lancarCheque(String Tipo) {
        if (Tipo.equals("Cheque")) {
            cheque = new Cheque();
            cheque.setIdPaciente(paciente);
            cheque.setIdTerceiro(terceiro);
            cheque.setTipo("R");
            cheque.setValor(exame.getValor());
        }
    }

    public void gerarParcelas() {
        if (exame.getValor() == 0D) {
            Mensagem.addMensagem(3, "Informe um valor.");
        } else {
            if (formaPagamento.getIdForma() == null) {
                Mensagem.addMensagem(3, "Informe a forma de pagamento!");
                RequestContext.getCurrentInstance().execute("PF('dlgConsultarFormas').show()");
            } else {
                if (exame.getTipoPagamento().equals("P")) {
                    parcelas.clear();
                    Integer num_Parcelas = exame.getIdForma().getNumMeses();
                    for (int i = 0; i < num_Parcelas; i++) {
                        Double valor = 0D;
                        repasse = new Repasse();
                        valor = exame.getValor() / num_Parcelas;
                        repasse.setIdPaciente(paciente);
                        repasse.setNumeroParcela(i + 1);
                        repasse.setIdExame(exame);
                        repasse.setDesconto(0D);
                        repasse.setValor(valor);
                        repasse.setValorDocumento(valor);
                        repasse.setDtLancamento(new Date());
                        repasse.setDtVencimento(addMes(exame.getDtRecebimento(), i));
                        repasse.setIdTerceiro(terceiro);
                        repasse.setStatus('A');
                        repasse.setSituacaoPaciente('A');
                        repasse.setSituacaoTerceiro('N');
                        repasse.setTipoPagamento('P');
                        repasse.setIdForma(formaPagamento);
                        parcelas.add(repasse);
                    }
                } else {
                    parcelas.clear();
                    repasse = new Repasse();
                    repasse.setNumeroParcela(1);
                    repasse.setValor(exame.getValor());
                    repasse.setIdPaciente(paciente);
                    repasse.setIdExame(exame);
                    repasse.setDesconto(0D);
                    repasse.setDtLancamento(new Date());
                    repasse.setDtVencimento(exame.getDtRecebimento());
                    repasse.setIdTerceiro(terceiro);
                    repasse.setStatus('A');
                    repasse.setSituacaoPaciente('A');
                    repasse.setSituacaoTerceiro('N');
                    repasse.setTipoPagamento('P');
                    repasse.setIdForma(formaPagamento);
                    repasse.setValorDocumento(exame.getValor());
                    parcelas.add(repasse);
                }
            }
            calcularTotalRepasse();
        }
    }
    private Double totalParcela;
    private Double totalDesconto;

    public void calcularTotalRepasse() {
        Double vlrTotal = 0D;
        Double descTotal = 0D;
        Double vlrDif = 0D;
        totalParcela = vlrDif;
        for (Repasse rr : parcelas) {
            vlrTotal = rr.getValor() + vlrTotal;
            descTotal = rr.getDesconto() + descTotal;
        }
        totalParcela = vlrTotal;
        totalDesconto = descTotal;
    }

    public void calcularParcelaDesconto(Repasse r, Integer index) {
        for (Repasse r1 : parcelas) {
            if (r1.getNumeroParcela().equals(r.getNumeroParcela())) {
                Double resultado = 0D;
                resultado = (r1.getValorDocumento() - r.getDesconto());
                r.setValor(resultado);
                parcelas.set(index, r);
                calcularTotalRepasse();
                resultado = 0D;
                break;
            }
        }
    }
    private Double valorDif;

    public void calcularValores() {
        Double valorFinal = 0D;
        Double valorExame = 0D;
        Double valorDif = 0D;
        for (Repasse r : parcelas) {
            valorFinal = r.getValor() + valorFinal;
        }
        valorDif = totalParcela - valorFinal;
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public void selecionarFormaDePagamento(FormaPagamento formaPagamento1) {
        formaPagamento = formaPagamento1;
        exame.setIdForma(formaPagamento1);
    }

    public boolean validarCamposObrigatorios() {
        if (paciente.getIdPaciente() == null) {
            Mensagem.addMensagem(3, "Informe o paciente!");
            return false;
        } else if (descricaoExame.getIdDescricaoExame() == null) {
            Mensagem.addMensagem(3, "Informe o exame!");
            return false;
        } else {
            return true;
        }
    }

    public ExameEJB geteEJB() {
        return eEJB;
    }

    public void seteEJB(ExameEJB eEJB) {
        this.eEJB = eEJB;
    }

    public Exame getExame() {
        return exame;
    }

    public void setExame(Exame exame) {
        this.exame = exame;
    }

    public List<Exame> getListaExames() {
        return listaExames;
    }

    public void setListaExames(List<Exame> listaExames) {
        this.listaExames = listaExames;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public List<Paciente> getPacientesAcarregar() {
        return pacientesAcarregar;
    }

    public void setPacientesAcarregar(List<Paciente> pacientesAcarregar) {
        this.pacientesAcarregar = pacientesAcarregar;
    }

    public Terceiro getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(Terceiro terceiro) {
        this.terceiro = terceiro;
    }

    public Terceiro getTerceiroItem() {
        return terceiroItem;
    }

    public void setTerceiroItem(Terceiro terceiroItem) {
        this.terceiroItem = terceiroItem;
    }

    public void selecionarPaciente(Paciente paciente1) {

        paciente = new Paciente();
        paciente = paciente1;
    }

    public void selecionarDescricaoExames(DescricaoExame descricaoExame1) {
        descricaoExame = new DescricaoExame();
        descricaoExame = descricaoExame1;
    }

    public void setarDataAtual() {
        exame.setDtExame(new Date());
    }

    public List<Terceiro> listaTerceiros() {
        return tEJB.listarTerceiro();
    }

    public List<ContaCorrente> listaContaCorrente() {
        return eEJB.listarContaCorrente();
    }

    public List<FormaPagamento> listaFormaPagamentos() {
        return fpEJB.listaFormasPagamento();
    }

    public void carregarPacientes() {
        pacientesAcarregar = pEJB.listarPacientes();
    }

    public void carregarDescricaoExames() {
        listaDescricaoExames = dEJB.listaDescricaoExame(tipo);
    }

    public DescricaoExame getDescricaoExame() {
        return descricaoExame;
    }

    public void setDescricaoExame(DescricaoExame descricaoExame) {
        this.descricaoExame = descricaoExame;
    }

    public List<DescricaoExame> getListaDescricaoExames() {
        return listaDescricaoExames;
    }

    public void setListaDescricaoExames(List<DescricaoExame> listaDescricaoExames) {
        this.listaDescricaoExames = listaDescricaoExames;
    }

    public List<ContaCorrente> getListaContaCorrente() {
        return listaContaCorrente;
    }

    public void setListaContaCorrente(List<ContaCorrente> listaContaCorrente) {
        this.listaContaCorrente = listaContaCorrente;
    }

    public FormaPagamentoEJB getFpEJB() {
        return fpEJB;
    }

    public void setFpEJB(FormaPagamentoEJB fpEJB) {
        this.fpEJB = fpEJB;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public List<FormaPagamento> getListaFormaPagamentos() {
        return listaFormaPagamentos;
    }

    public void setListaFormaPagamentos(List<FormaPagamento> listaFormaPagamentos) {
        this.listaFormaPagamentos = listaFormaPagamentos;
    }

    public LazyDataModel<Exame> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Exame> model) {
        this.model = model;
    }

    public List<Repasse> getParcelas() {
        return parcelas;
    }

    public void setParcelas(List<Repasse> parcelas) {
        this.parcelas = parcelas;
    }

    public Repasse getRepasse() {
        return repasse;
    }

    public void setRepasse(Repasse repasse) {
        this.repasse = repasse;
    }

    public Double getTotalParcela() {
        return totalParcela;
    }

    public void setTotalParcela(Double totalParcela) {
        this.totalParcela = totalParcela;
    }

    public Double getValorDif() {
        return valorDif;
    }

    public void setValorDif(Double valorDif) {
        this.valorDif = valorDif;
    }

    public Double getTotalDesconto() {
        return totalDesconto;
    }

    public void setTotalDesconto(Double totalDesconto) {
        this.totalDesconto = totalDesconto;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
