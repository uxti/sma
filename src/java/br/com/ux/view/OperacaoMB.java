/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.OperacaoEJB;
import br.com.ux.model.Operacao;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class OperacaoMB {

    @EJB
    OperacaoEJB oEJB;
    private Operacao operacao;
    UsuarioSessao us = new UsuarioSessao();

    /**
     * Creates a new instance of OperacaoMB
     */
    public OperacaoMB() {
        novo();
    }

    public void novo() {
        operacao = new Operacao();
    }

    public void Salvar() {
        if (operacao.getIdOperacao() != null) {
            try {
                oEJB.salvar(operacao, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        } else {
            try {
                oEJB.salvar(operacao, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("editar");
            }
        }
    }

    public void selecionarPorID(Integer id) {
        novo();
        operacao = oEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public void Excluir() {
        try {
            oEJB.excluir(operacao, us.retornaUsuario());
            novo();
            Mensagem.addMensagemPadraoSucesso("excluir");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }

    
    public List<Operacao> listarOperacoes(){
        return oEJB.listarOperacoes();
    }
    
    
    public Operacao getOperacao() {
        return operacao;
    }

    public void setOperacao(Operacao operacao) {
        this.operacao = operacao;
    }
}
