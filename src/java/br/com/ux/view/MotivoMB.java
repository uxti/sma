/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.MotivoEJB;
import br.com.ux.model.Banco;
import br.com.ux.model.Motivo;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author BrunoS
 */
@ManagedBean
@ViewScoped
public class MotivoMB implements Serializable {

    @EJB
    MotivoEJB mEJB;
    Motivo motivo;
    List<Motivo> listaaCarregar;
    UsuarioSessao us = new UsuarioSessao();
    LazyDataModel<Motivo> model;

    public MotivoMB() {
        novo();
    }

    public void novo() {
        motivo = new Motivo();
        listaaCarregar = new ArrayList<Motivo>();
        carregarModeloLazy();
    }

    public void salvar() {
        if (motivo.getIdMotivo() == null) {
            try {
                mEJB.Salvar(motivo, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        } else {
            try {
                mEJB.Salvar(motivo, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("editar" + e);
            }
        }
    }

    public void carregarModeloLazy() {
        model = new LazyDataModel<Motivo>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Motivo> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();

                setRowCount(Integer.parseInt(mEJB.contarRegistro(Clausula).toString()));

                listaaCarregar = mEJB.listarLazy(first, pageSize, Clausula);
                return listaaCarregar;
            }
        };
    }

    public void excluir(Motivo motivo) {
        try {
            mEJB.Excluir(motivo, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("excluir");
            novo();
            BCRUtils.ResetarDatatableFiltros("formMotivo:dtl");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir" + e);
        }
    }

    public List<Motivo> listaMotivos() {
        return mEJB.listarMotivos();
    }
    
    public List<Motivo> listaMotivosPorTipo(String tipo) {
        return mEJB.listarMotivosPorTipo(tipo);
    }

    public Motivo selecionaPorID(Integer id) {
        return motivo = mEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public void carregaMotivos() {
        listaaCarregar = mEJB.listarMotivos();
    }

    //==========================================================================
    public MotivoEJB getmEJB() {
        return mEJB;
    }

    public void setmEJB(MotivoEJB mEJB) {
        this.mEJB = mEJB;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public List<Motivo> getListaaCarregar() {
        return listaaCarregar;
    }

    public void setListaaCarregar(List<Motivo> listaaCarregar) {
        this.listaaCarregar = listaaCarregar;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public LazyDataModel<Motivo> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Motivo> model) {
        this.model = model;
    }
    
    
}
