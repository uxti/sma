/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.StatusEJB;
import br.com.ux.model.Status;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class StatusMB {

    @EJB
    StatusEJB stEJB;
    public Status status;
    public List<Status> listaaCarregar;
    public UsuarioSessao us = new UsuarioSessao();

    public StatusMB() {
        novo();
    }

    public void novo() {
        status = new Status();
        listaaCarregar = new ArrayList<>();
    }

    public void salvar() {        
        System.out.println("Salvando status.");
        if (status.getIdStatus() == null) {
            try {    
                System.out.println("Salvando status.");
                stEJB.Salvar(status, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                System.out.println(e);
                Mensagem.addMensagemPadraoErro("salvar" + e);
            }
        } else {
            try {
                System.out.println("Editando status.");
                stEJB.Salvar(status, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                System.out.println(e);
                Mensagem.addMensagemPadraoErro("editar" + e);
            }
        }
    }

    public void excluir(Status status) {
        try {
            stEJB.Excluir(status, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("excluir");
            novo();
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir" + e);
            
        }
    }

    public List<Status> listaStatus() {
        return stEJB.listarStatus();
    }

    public Status selecionaPorID(Integer id) {
        return status = stEJB.selecionarPorID(id, us.retornaUsuario());
    }
    
    public Status retornaStatusPorTipo(Integer tipo){
        return status = stEJB.selecionarPorTipo(tipo);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Status> getListaaCarregar() {
        return listaaCarregar;
    }

    public void setListaaCarregar(List<Status> listaaCarregar) {
        this.listaaCarregar = listaaCarregar;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }
    
    
}
