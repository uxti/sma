/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AlertaEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.ExameEJB;
import br.com.ux.controller.FormaPagamentoEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Alerta;
import br.com.ux.model.Banco;
import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Exame;
import br.com.ux.model.FormaPagamento;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Usuario;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class AlertaMB implements Serializable {

    @EJB
    AlertaEJB alertaEJB;
    UsuarioSessao us = new UsuarioSessao();
    @EJB
    UsuarioEJB uEJB;
    @EJB
    ChequeEJB chequeEJB;
    @EJB
    ExameEJB exameEJB;
    @EJB
    TerceiroEJB tEJB;
    @EJB
    FormaPagamentoEJB fEJB;
    @EJB
    RepasseEJB repasseEJB;
    @EJB
    CaixaEJB caixaEJB;
    @EJB
    ContaCorrenteEJB ccEJB;
    // Controle de alertas //
    private Integer contador;
    private Date dtCompensado;
    private String movimento, numeroCheque, emitenteCheque, compensado, observacao, formaPag, hospital;
    private Double valorCheque, valorRestante, valorPago, valorTotal;
    private Paciente paciente;
    private Banco banco;
    private Exame exame;
    private Alerta alerta;
    private List<Cheque> listaCheque;
    private Cheque cheque;
    private Caixa caixa;
    private FormaPagamento formaPagamento;

    public AlertaMB() {
        novo();
        zerarCampos();
    }

    public void novo() {
        alerta = new Alerta();
        exame = new Exame();
        banco = new Banco();
        paciente = new Paciente();
        listaCheque = new ArrayList<Cheque>();
        formaPagamento = new FormaPagamento();
        dtCompensado = new Date();
        valorRestante = 0D;
        valorCheque = 0D;
        valorPago = 0D;
        valorTotal = 0D;
    }

    public void zerarCampos() {
        movimento = "";
        emitenteCheque = "";
        compensado = "";
        numeroCheque = null;
        observacao = "";
        if (listaCheque.size() > 0) {
            for (Cheque ch : listaCheque) {
                chequeEJB.Excluir(cheque, us.retornaUsuario());
            }
        }
        listaCheque = new ArrayList<Cheque>();
    }

    public void Salvar() {
        try {
            alertaEJB.Salvar(alerta, us.retornaUsuario());
        } catch (Exception e) {
            Mensagem.addMensagemErro("Aconteceu algo inesperado" + e, 10000);
        }
    }

    public void carregarDados(Alerta a) {
        novo();
        zerarCampos();
        alerta = a;
        if (a.getIdExame() != null) {
            exame = alerta.getIdExame();
        }
        selecionarFormaPagamentoPadrao();
        valorCheque = 0D;
        valorRestante = 0D;
        emitenteCheque = "";
        paciente = a.getTipo().equals("E") ? a.getIdExame().getIdPaciente() : a.getTipo().equals("C") ? a.getIdCirurgia().getIdPaciente() : a.getTipo().equals("O") ? a.getIdOrcamento().getIdPaciente() : null;
        valorCheque = a.getTipo().equals("E") ? a.getIdExame().getValor() : a.getTipo().equals("C") ? a.getIdCirurgia().getValor() : a.getTipo().equals("O") ? a.getIdOrcamento().getValor() : 0D;
        valorTotal = valorCheque;
        valorRestante = valorTotal;
        emitenteCheque = paciente.getNome();

    }

    public void Pagamento() {
        cheque = new Cheque();
        if (validarCamposObrigatoriosCheque()) {
            if (formaPagamento.getForma().equals("C")) {
                cheque.setCompensado(compensado);
            } else {
                cheque.setCompensado("S");
            }
            if (!paciente.getNome().equals(emitenteCheque)) {
                cheque.setEmitente(emitenteCheque);
            }
            cheque.setMovimento(formaPagamento.getForma());
            cheque.setDtCompensado(dtCompensado);
            cheque.setDtRecebimentoEmissao(new Date());

            if (banco.getIdBanco() != null) {
                cheque.setIdBanco(banco);
            }
            cheque.setIdTerceiro(hospital.equals("N") ? exame.getIdTerceiro() : tEJB.selecionarTerceiroCNPJCPF("61.635.808/0001-30", "Hospital"));
            cheque.setIdPaciente(paciente);
            cheque.setNumeroCheque(numeroCheque);
            cheque.setObservacao(observacao);
            cheque.setTipo(hospital.equals("N") ? "N" : "H");
            cheque.setValor(valorCheque);
            cheque.setNumeroParcela(1);
            cheque.setIdExame(exame);
            cheque.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
            chequeEJB.Salvar(cheque, us.retornaUsuario());
            listaCheque.add(cheque);
            calcularValores();

            if (exame.getIdExame() != null) {
                atualizarExame(exame);
            }
            zerarCampos();
        }
    }

    public boolean validarCamposObrigatoriosCheque() {
        if (formaPagamento.getIdForma() != null) {
            if (formaPagamento.getForma().equals("C")) {
                if (numeroCheque == null) {
                    Mensagem.addMensagem(3, "Informe o numero do Cheque!");
                    return false;
                } else if (banco == null) {
                    Mensagem.addMensagem(3, "Informe o banco.");
                    return false;
                } else {
                    return true;
                }
            } else {
                if (formaPagamento.getForma().equals("D")) {
                    if (valorCheque == 0D) {
                        Mensagem.addMensagem(3, "O valor do pagamento não pode ser zero.");
                        return false;
                    } else if (formaPagamento == null) {
                        Mensagem.addMensagem(3, "Informe a forma de Pagamento.");
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        } else {
            Mensagem.addMensagem(1, "Informe a forma de pagamento.");
            return false;
        }

    }

    public void atualizarExame(Exame e) {
        exameEJB.Atualizar(e, us.retornaUsuario());
    }

    public void calcularValores() {
        valorPago = 0D;
        for (Cheque ch : listaCheque) {
            valorPago = valorPago + ch.getValor();
        }
        valorRestante = valorTotal - valorPago;
        valorCheque = valorRestante;
    }

    public boolean confirmarPagamento(Alerta a) {
        if (listaCheque.size() > 0) {
            Receber();
            Mensagem.addMensagem(1, "Lançamentos confirmados. Recebimentos efetuados com sucesso.");
            return true;
        } else {
            Mensagem.addMensagem(1, "Não há pagamento(s) para este lançamento. ");
            return false;

        }
    }

    public void selecionarFormaDePagamento(FormaPagamento formaPagamento1) {
        formaPagamento = formaPagamento1;
    }

    public void selecionarFormaPagamentoPadrao() {
        formaPagamento = fEJB.selecionarPadrao();
    }

    public List<Alerta> alertasPorUsuario() {
        List<Alerta> alertas = new ArrayList<Alerta>();
        contador = 0;
        alertas = alertaEJB.listaAlertasPorUsuario(us.retornaUsuario());
        for (Alerta al : alertas) {
            contador = contador + 1;
        }
        return alertas;
    }

    public void Receber() {
        Usuario u = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
        try {
            // Somente um numero de lote para toda a cirurgia
            Date d = new Date();
            Long numAleatorio = d.getTime();
            String forma, terceiro = "";
            List<Repasse> repasses = new ArrayList<Repasse>();
            // Gerar repasses
            for (Cheque ch : listaCheque) {
                Repasse rr = new Repasse();
                if (ch.getMovimento().equals("C")) {
                    rr.setFormaPagamento("Cheque");
                    rr.setSituacaoTerceiro('N');
                } else {
                    rr.setFormaPagamento("Dinheiro");
                    rr.setSituacaoTerceiro('L');
                }

                rr.setDtRecebimento(ch.getDtRecebimentoEmissao());
                rr.setObservacao(ch.getObservacao());
                rr.setPercentualRecebido(100D);
                rr.setValorRecebido(ch.getValor());
                rr.setSituacaoPaciente('P');
                rr.setStatus('F');
                rr.setValor(0D);
                rr.setIdExame(ch.getIdExame());
                rr.setTipoPagamento('P');
                repasseEJB.Salvar(rr);
                repasses.add(rr);
            }
            // Gerar conta corrente
            List<ContaCorrente> contas = new ArrayList<ContaCorrente>();
            for (Repasse r : repasses) {
                String tipo = r.getIdExame().getTipoLancamento().equals("E") ? "Exame" : "Consulta";
                ContaCorrente cc = new ContaCorrente();
                cc.setIdContaCorrente(null);
                cc.setIdTerceiro(r.getIdTerceiro());
                cc.setIdRepasse(r);
                cc.setIdPaciente(r.getIdPaciente());
                cc.setTipo("C");
                cc.setDtLancamento(new Date());
                cc.setSituacao("Não Liberado");
                cc.setStatus("Não Retirado");
                cc.setValor(r.getValor());
                cc.setObservacao("Repasse gerado " + tipo + " nº " + r.getIdExame() + " .Parcela " + r.getNumeroParcela() + " Paciente: " + r.getIdPaciente().getNome() + ".");
                contas.add(cc);
            }
            // Salva conta corrente
            for (ContaCorrente cc : contas) {
                ccEJB.SalvarSimples(cc, us.retornaUsuario());
            }

            // Lançar pagamentos no Caixa
            for (Cheque ch : listaCheque) {
                String tipo = ch.getIdExame().getTipoLancamento().equals("E") ? "Exame" : "Consulta";
                caixa = new Caixa();
                caixa.setNumLote(numAleatorio.toString());
                caixa.setIdCheque(ch);
                caixa.setIdUsuario(u);
                caixa.setDtCaixa(ch.getDtRecebimentoEmissao());
                caixa.setTipoLancamento('C');
                caixa.setValor(ch.getValor());
                caixa.setStatus("Não Baixado");
                if (ch.getIdTerceiro().getTipoPessoa() == 'J') {
                    terceiro = ch.getIdTerceiro().getNomeFantasia();
                } else {
                    terceiro = ch.getIdTerceiro().getNome();
                }
                caixa.setHistorico(tipo + " nº " + ch.getIdExame().getIdExame() + ". " + ch.getIdExame().getIdDescricaoExame().getDescricao() + " ." + " Méd. Resp.:" + terceiro + ". Paciente: " + ch.getIdExame().getIdPaciente().getNome());
                caixa.setNumeroParcela(ch.getNumeroParcela());
                caixaEJB.LancarRecebimentoNoCaixa(caixa);
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algum erro ao tentar receber!");
            System.out.println(e);
        }

    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public Alerta getAlerta() {
        return alerta;
    }

    public void setAlerta(Alerta alerta) {
        this.alerta = alerta;
    }

    public Integer getContador() {
        return contador;
    }

    public void setContador(Integer contador) {
        this.contador = contador;
    }

    public Date getDtCompensado() {
        return dtCompensado;
    }

    public void setDtCompensado(Date dtCompensado) {
        this.dtCompensado = dtCompensado;
    }

    public String getMovimento() {
        return movimento;
    }

    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    public String getEmitenteCheque() {
        return emitenteCheque;
    }

    public void setEmitenteCheque(String emitenteCheque) {
        this.emitenteCheque = emitenteCheque;
    }

    public String getCompensado() {
        return compensado;
    }

    public void setCompensado(String compensado) {
        this.compensado = compensado;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Double getValorCheque() {
        return valorCheque;
    }

    public void setValorCheque(Double valorCheque) {
        this.valorCheque = valorCheque;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public List<Cheque> getListaCheque() {
        return listaCheque;
    }

    public void setListaCheque(List<Cheque> listaChequeTemporaria) {
        this.listaCheque = listaChequeTemporaria;
    }

    public String getFormaPag() {
        return formaPag;
    }

    public void setFormaPag(String formaPag) {
        this.formaPag = formaPag;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Exame getExame() {
        return exame;
    }

    public void setExame(Exame exame) {
        this.exame = exame;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public Double getValorRestante() {
        return valorRestante;
    }

    public void setValorRestante(Double valorRestante) {
        this.valorRestante = valorRestante;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public void teste() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String url = req.getRequestURL().toString();
        System.out.println(url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath() + "/") ;
    }
    
    
}
