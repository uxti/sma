/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.FormaPagamentoEJB;
import br.com.ux.model.FormaPagamento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class FormaPagamentoMB implements Serializable {

    @EJB
    FormaPagamentoEJB fpEJB;
    public FormaPagamento formaPagamento;
    UsuarioSessao us = new UsuarioSessao();
    List<FormaPagamento> listaaCarregarLazy;
    LazyDataModel<FormaPagamento> model;

    public FormaPagamentoMB() {
        novo();
        carregarModelLazy();
    }

    public void novo() {
        formaPagamento = new FormaPagamento();
        formaPagamento.setInativo(Boolean.TRUE);
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void Salvar() {
        System.out.println("Forma de Pagamento: " + formaPagamento.getDescricao());
        try {
            fpEJB.Salvar(formaPagamento, us.retornaUsuario());
            novo();
            Mensagem.addMensagem(1, formaPagamento.getIdForma() == null ? "Forma de Pagamento Salva com sucesso!" : "Forma de Pagamento alterada com sucesso!");            
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algo inesperado ao tentar salvar a forma de Pagamento!" + e);
        }
    }

    public List<FormaPagamento> listaFormas() {
        return fpEJB.listaFormasPagamento();
    }

    public FormaPagamento selecionarPorID(Integer id) {
        novo();

        return formaPagamento = fpEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public void Excluir(FormaPagamento formaPagamento) {
        try {
            if (formaPagamento.getCirurgiaList().isEmpty() && formaPagamento.getOrcamentoList().isEmpty()) {
                fpEJB.Excluir(formaPagamento, us.retornaUsuario());
                novo();
                Mensagem.addMensagem(1, "Forma de Pagamento Excluida com sucesso!");
                BCRUtils.ResetarDatatableFiltros("formPagamento:dtl");
            } else {
                Mensagem.addMensagem(3, "Este cadastro já foi utilizado e não pode ser excluído. Se quiser desabilitar, clique em Editar e desmarque a opção 'Ativo'.");

            }

        } catch (Exception e) {
            Mensagem.addMensagem(3, "Algo inesperado aconteceu ao tentar excluir!" + e);
        }

    }

    public FormaPagamentoEJB getFpEJB() {
        return fpEJB;
    }

    public void setFpEJB(FormaPagamentoEJB fpEJB) {
        this.fpEJB = fpEJB;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public List<FormaPagamento> getListaaCarregarLazy() {
        return listaaCarregarLazy;
    }

    public void setListaaCarregarLazy(List<FormaPagamento> listaaCarregarLazy) {
        this.listaaCarregarLazy = listaaCarregarLazy;
    }

    public LazyDataModel<FormaPagamento> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<FormaPagamento> model) {
        this.model = model;
    }

    private void carregarModelLazy() {
        model = new LazyDataModel<FormaPagamento>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<FormaPagamento> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        sf.append(" where f." + filterProperty + " like'%" + filterValue + "%' AND f.inativo = TRUE");
                    } else {
                        sf.append(" and f." + filterProperty + " like'%" + filterValue + "%' AND f.inativo = TRUE");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by f." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by f." + sortField + " desc");
                }
                Clausula = sf.toString();
                setRowCount(Integer.parseInt(fpEJB.contarFormaPagtoRegistro(Clausula).toString()));

                listaaCarregarLazy = fpEJB.listarFormaPagtoLazyModeWhere(first, pageSize, Clausula);
//                listaPacientesLazy = pEJB.listarPacientesLazyMode(first, pageSize);
                return listaaCarregarLazy;
            }
        };
    }
}
