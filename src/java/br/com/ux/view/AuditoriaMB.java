/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.model.Auditoria;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.BCRUtilsMB;
import br.com.ux.util.Mensagem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class AuditoriaMB implements Serializable {

    @EJB
    AuditoriaEJB aEJB;
    private Date dataInicial;
    private Date dataFinal;
    private String nomeUsuario, acao, pesquisar;
    private List<Auditoria> listaAuditoria;
    private LazyDataModel<Auditoria> model;

    public AuditoriaMB() {
        listaAuditoria = new ArrayList<Auditoria>();
        dataInicial = BCRUtils.primeiroDiaDoMes(new Date());
        dataFinal = BCRUtils.ultimoDiaDoMes(new Date());
        nomeUsuario = "%";
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public List<Auditoria> getListaAuditoria() {
        return listaAuditoria;
    }

    public void pesquisarAuditoria() {
//        listaAuditoria = new ArrayList<Auditoria>();
//        listaAuditoria = aEJB.listarPorData(getDataInicial(), getDataFinal());
//        if (listaAuditoria.isEmpty()) {
////            Mensagem.addMensagem(3, "Atenção! Não foi encontrada auditoria nesta data!");
//            Mensagem.addMensagemAviso("Atenção! Não foi encontrada auditoria nesta data!", 6000);
//        }
        model = new LazyDataModel<Auditoria>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Auditoria> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        sf.append(" and a." + filterProperty + " like'%" + filterValue + "%'");
                    } else {
                        sf.append(" and a." + filterProperty + " like'%" + filterValue + "%'");
                    }

                }
                if (!nomeUsuario.equals("") || !nomeUsuario.isEmpty()) {
                    sf.append(" and a.usuario like '" + nomeUsuario.toLowerCase() + "%'");
                }

                if (!acao.equals("") || !acao.isEmpty()) {
                    sf.append(" and a.acao like '" + acao.toLowerCase() + "%'");
                }
                
                if (!pesquisar.equals("") || !pesquisar.isEmpty()) {
                    sf.append(" and a.descricao like '%" + pesquisar.toLowerCase() + "%'");
                }
                
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by a." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by a." + sortField + " desc");
                }

                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(aEJB.contarRegistro(Clausula, dataInicial, dataFinal).toString()));
                listaAuditoria = aEJB.listarAuditoriaLazyModeWhere(first, pageSize, Clausula, dataInicial, dataFinal);
                return listaAuditoria;
            }
        };

    }

    public LazyDataModel<Auditoria> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Auditoria> model) {
        this.model = model;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getPesquisar() {
        return pesquisar;
    }

    public void setPesquisar(String pesquisar) {
        this.pesquisar = pesquisar;
    }
    
}
