/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.BancoEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.OperacaoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Banco;
import br.com.ux.model.Caixa;
import br.com.ux.model.CaixaPai;
import br.com.ux.model.Cheque;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Operacao;
import br.com.ux.model.Paciente;
import br.com.ux.model.Repasse;
import br.com.ux.model.Terceiro;
import br.com.ux.model.Usuario;
import br.com.ux.util.Conexao;
import br.com.ux.util.Mensagem;
import br.com.ux.util.Recebimento;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Renato
 */
@ViewScoped
@ManagedBean
public class RepasseMB implements Serializable {

    @EJB
    RepasseEJB rEJB;
    @EJB
    PacienteEJB pEJB;
    private Paciente paciente;
    private Terceiro terceiro;
    private Repasse repasse;
    private List<Repasse> listaRepasses;
    private List<Repasse> listaRepassesSelecionados;
    private Recebimento recebimento = new Recebimento();
    @EJB
    CaixaEJB cEJB;
    private CaixaPai caixaPai;
    private Caixa caixa;
    @EJB
    UsuarioEJB uEJB;
    private Usuario usuario;
    private UsuarioSessao us = new UsuarioSessao();
    @EJB
    ContaCorrenteEJB contaEJB;
    private boolean exibirBotao;
    @EJB
    ChequeEJB chequeEJB;
    private Cheque cheque;
    private List<Cheque> listaCheque;
    private Double valorTotal = 0D;
    @EJB
    BancoEJB bancoEJB;
    private Banco banco;
    //  Controle de Cheques  //
    private String tipo, compensado, observacao, conta, numeroCheque;
    private Double valorCheque;
    private Date dtEmissao, dtCompensado;
    boolean diferenca;

    /**
     * Creates a new instance of RepasseMB
     */
    public RepasseMB() {
        novo();
    }

    public void novo() {
        repasse = new Repasse();
        listaRepasses = new ArrayList<Repasse>();
        listaRepassesSelecionados = new ArrayList<Repasse>();
        setDt_recebimento(new Date());
        setAcrescimo(0D);
        setDesconto(0D);
        setJuros(0D);
        setValor_recebido(0D);
        setObs("");
        setSituacao("");
        setStatus("");
        caixaPai = new CaixaPai();
        caixa = new Caixa();
        usuario = new Usuario();
        operacao = new Operacao();
        cheque = new Cheque();
        listaCheque = new ArrayList<Cheque>();
        banco = new Banco();
        paciente = new Paciente();
        terceiro = new Terceiro();
    }
    private Date dt_recebimento;
    private String obs;
    private String situacao;
    private String status;
    private Double desconto;
    private Double acrescimo;
    private Double juros;
    private Double valor_recebido;
    private Double percentualTotal;
    private Operacao operacao;
    private String formaPagamento;
    @EJB
    OperacaoEJB opEJB;

    public void verificarMarcados() {
        Double val = 0D;
        try {
            for (Repasse rr : listaRepasses) {
                if (rr.isPagar()) {
                    val = rr.getValor() + val;
                }
            }
        } catch (Exception e) {
            System.out.println("Excessão no metodo verificarMarcados " + e);
        }

        valorTotalPgto = val;
//        setValor_recebido(valorTotalPgto);
    }

    public void calcularPercentual() {
        Double percentualValor = 0D;
        percentualValor = ((valor_recebido * 100) / valorAPagar);
        percentualTotal = percentualValor;
        valorTotalPgto = valor_recebido;
    }
    private List<Repasse> listaTemporaria = new ArrayList<Repasse>();

    public List<Repasse> getListaTemporaria() {
        return listaTemporaria;
    }

    public void setListaTemporaria(List<Repasse> listaTemporaria) {
        this.listaTemporaria = listaTemporaria;
    }

//<p:ajax event="rowSelect" listener="#{repasseMB.marcaTodasParcelasDoOrcamento}" update=":frmConsultarRecebimentos:tblPesq" />
    public void marcaTodasParcelasDoOrcamento(SelectEvent event) {
        try {
            Repasse r = new Repasse();
            r = (Repasse) event.getObject();
            System.out.println(r.getIdCirurgia().getIdCirurgia());
            listaRepassesSelecionados = new ArrayList<Repasse>();
            for (Repasse rep : listaRepasses) {
                if (rep.getIdCirurgia() == r.getIdCirurgia() && rep.getNumeroParcela() == r.getNumeroParcela()) {
                    listaRepassesSelecionados.add(rep);
                    System.out.println("adiciounou");
                }
            }
            calcularTotaisRecebimento();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }
    String dataInicioFormatada;
    String dataFimFormatada;

    public void processarListaRecebimentos() throws ParseException {
        DateFormat df = new SimpleDateFormat("y-M-d");
        if (dtIni == null) {
            dtIni = addMes(new Date(), -100);
            dataInicioFormatada = df.format(dtIni);
        } else {
            dataInicioFormatada = df.format(dtIni);
        }
        if (dtFim == null) {
            dtFim = addMes(new Date(), 50);
            dataFimFormatada = df.format(dtFim);
        } else {
            dataFimFormatada = df.format(dtFim);
        }
        if (idPac == null) {
            idPac = "%";
        }
        if (numCirurgia == null) {
            numCirurgia = "%";
        }
        listaRepasses = new ArrayList<Repasse>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = Conexao.conexaoJDBC().createStatement();

//            System.out.println("select sum(valor) valor, numero_parcela ,  id_paciente, id_cirurgia,  dt_vencimento, dt_lancamento, situacao_paciente, tipo_pagamento from repasse "
//                    + "where situacao_paciente != 'P' and status != 'F' "
//                    + "and id_paciente like '%" + getIdPac() + "' and dt_lancamento BETWEEN '" + dataInicioFormatada + "' and '" + dataFimFormatada + "' AND ID_CIRURGIA LIKE '%" + getNumCirurgia() + "'  "
//                    + "group by numero_parcela ,  id_paciente, id_cirurgia,   dt_vencimento,dt_lancamento,situacao_paciente, tipo_pagamento");
            try {
                resultSet = statement.executeQuery("select sum(valor) valor, numero_parcela ,  id_paciente, id_cirurgia,  dt_vencimento, dt_lancamento, situacao_paciente, id_cheque from repasse "
                        + "where situacao_paciente != 'P' and status != 'F' "
                        + "and id_paciente like '%" + getIdPac() + "' and dt_lancamento BETWEEN '" + dataInicioFormatada + "' and '" + dataFimFormatada + "' AND ID_CIRURGIA LIKE '%" + getNumCirurgia() + "'  "
                        + "group by numero_parcela ,  id_paciente, id_cirurgia,   dt_vencimento,dt_lancamento,situacao_paciente, tipo_pagamento");
                Repasse r = new Repasse();
                while (resultSet.next()) {
                    r = new Repasse();
                    r.setIdPaciente(rEJB.pesquisarPaciente(resultSet.getInt("id_paciente")));
                    r.setIdCirurgia(rEJB.pesquisarCirurgia(resultSet.getInt("id_cirurgia")));
                    r.setNumeroParcela(resultSet.getInt("numero_parcela"));
                    r.setValor(resultSet.getDouble("valor"));
                    r.setDtLancamento(resultSet.getDate("dt_lancamento"));
                    r.setDtVencimento(resultSet.getDate("dt_vencimento"));
                    r.setSituacaoPaciente(resultSet.getString("situacao_paciente").charAt(0));
                    //                    r.setIdCheque(rEJB.pesquisarCheque(resultSet.getInt("id_cheque")));
                    if (r.getIdCirurgia().getStatus() == 'L') {
                        listaRepasses.add(r);
                    }
                }
                resultSet.close();
                statement.close();
            } catch (Exception e) {
                System.out.println(e);
            }

            if (listaRepasses.isEmpty()) {
                Mensagem.addMensagem(3, "Atenção! Nenhum recebimento foi encontrado!");
            }
        } catch (Exception e) {
        }

    }

    public void calcularTotaisRecebimento() {
        Double val = 0D;
        for (Repasse rep : listaRepassesSelecionados) {
            val = rep.getValor() + val;
        }
        valorTotalPgto = val;
    }

    public void carregaValores(Double valor) {
        valor_recebido = 0D;
        desconto = 0D;
        acrescimo = 0D;
        juros = 0D;
        percentualTotal = 0D;
        dt_recebimento = new Date();
        formaPagamento = "";
        obs = "";
        valorTotalPgto = 0D;
        if (valorTotalPgto != null) {
            setValor_recebido(getValorTotalPgto());
            zeraVariaveisCheque();
            setPercentualTotal(100D);
        }
        valorTotal = valor;
    }

    public void carregaValoresCheque() {
        zeraVariaveisCheque();

        if (valorTotalPgto != null) {
            for (Repasse rp : listaRepasses) {
                if (rp.isPagar()) {
                    tipo = "E";
                    compensado = "N";
                    paciente = rp.getIdPaciente();
                    terceiro = rp.getIdCirurgia().getIdTerceiro();
                    repasse = rp;
                    valorCheque = valorTotalPgto;
                    dtEmissao = new Date();
                    dtCompensado = new Date();
                }

            }
        }
    }

    public void salvarEmRepasse(List<Cheque> list) {
        try {
            for (Cheque ch : list) {
                chequeEJB.Salvar(ch, us.retornaUsuario());
            }
            Mensagem.addMensagemPadraoSucesso("salvar");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
        }

    }

    public void incluirVarios() {
        cheque = new Cheque();
        cheque.setCompensado(compensado);
        cheque.setConta(conta);
        cheque.setDtCompensacaoVencimento(dtCompensado);
        cheque.setDtRecebimentoEmissao(dtEmissao);
        if (banco.getIdBanco() != null) {
            cheque.setIdBanco(banco);
        }
        cheque.setIdPaciente(paciente);
        cheque.setIdTerceiro(terceiro);
        cheque.setNumeroCheque(numeroCheque);
        cheque.setObservacao(situacao);
        cheque.setTipo(tipo);
        cheque.setValor(valorCheque);
//        repasse.setIdCheque(cheque);
        listaCheque.add(cheque);
        valorTotal = 0D;
        for (Cheque ch : listaCheque) {
            valorTotal = valorTotal + ch.getValor();
        }

        if (valor_recebido.equals(valorTotal)) {
            diferenca = false;
        } else {
            diferenca = true;
        }
        zeraVariaveisCheque();
        cheque = new Cheque();
    }

    public Date retornaDataPagamentoMedico(Repasse r) {
        System.out.println(contaEJB.pesquisarContaCorrentePorRepasse(r).getDtPagamentoMedico());
        return contaEJB.pesquisarContaCorrentePorRepasse(r).getDtPagamentoMedico();

    }

    public void excluirCheque(Cheque c) {
        try {
            int indice = 0;
            List<Cheque> listaTemporaria = new ArrayList<Cheque>();
            listaTemporaria = listaCheque;
            for (Cheque ch : listaTemporaria) {
                if (ch == c) {
                    listaCheque.remove(indice);
                    break;
                } else {
                    indice++;
                }
            }
            valorTotal = 0D;
            for (Cheque ch : listaCheque) {
                valorTotal = valorTotal + ch.getValor();
            }
            Mensagem.addMensagem(1, "Item Removido com sucesso!");
        } catch (Exception e) {
            Mensagem.addMensagem(1, "Erro ao remover");
        }
    }

    public void processarListaCheque(String id) throws ParseException {
        if (id == null) {
            id = "%";
        }
        listaCheque = new ArrayList<Cheque>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = Conexao.conexaoJDBC().createStatement();

            System.out.println("select sum(valor) valor, numero_parcela , id_paciente, id_cirurgia,  dt_vencimento, dt_lancamento, situacao_paciente, tipo_pagamento from repasse "
                    + "where situacao_paciente != 'P' and status != 'F' "
                    + "and id_paciente like '%" + getIdPac() + "' and dt_lancamento BETWEEN '" + dataInicioFormatada + "' and '" + dataFimFormatada + "' AND ID_CIRURGIA LIKE '%" + getNumCirurgia() + "'  "
                    + "group by numero_parcela ,  id_paciente, id_cirurgia,   dt_vencimento,dt_lancamento,situacao_paciente, tipo_pagamento");

            try {
                resultSet
                        = statement.executeQuery("SELECT c.id_paciente idPaciente, c.id_cheque idCheque,        "
                                + "c.valor Valor, c.numero_cheque numeroCheque,           "
                                + "c.id_banco idBanco, c.compensado compensado            "
                                + "c.conta Conta, c.dt_recebimento_emissao dtEmissao,     "
                                + "c.dt_compensacao_vencimento dtCompensado, c.tipo Tipo, "
                                + "c.observacao OBS "
                                + "FROM cheque c, repasse r "
                                + "WHERE r.id_cirurgia = " + id
                                + "AND r.id_cheque = c.id_cheque"
                                + "GROUP BY c.id_cheque, c.numero_cheque");

                Cheque c = new Cheque();
                while (resultSet.next()) {
                    System.out.println("entrou no while");
                    c = new Cheque();

                    c.setIdPaciente(rEJB.pesquisarPaciente(resultSet.getInt("idPaciente")));
                    c.setIdCheque(resultSet.getInt("idCheque"));
                    c.setValor(resultSet.getDouble("Valor"));
                    c.setNumeroCheque(resultSet.getString(numeroCheque));
                    c.setIdBanco(rEJB.selecionarBancoPorID(resultSet.getInt("idBanco")));
                    c.setConta(resultSet.getString("Conta"));
                    c.setDtRecebimentoEmissao(resultSet.getDate("dtEmissao"));
                    c.setDtCompensacaoVencimento(resultSet.getDate("dtCompensado"));
                    c.setTipo(resultSet.getString("Tipo"));
                    c.setObservacao(resultSet.getString("OBS"));
                    c.setCompensado(resultSet.getString("Compensado"));
                    System.out.println("AddListaCheque");
                    listaCheque.add(c);
                }
                resultSet.close();
                statement.close();
            } catch (Exception e) {
                System.out.println(e);
            }

            if (listaCheque.isEmpty()) {
                Mensagem.addMensagem(3, "Atenção! Nenhum cheque foi encontrado!");
            }
        } catch (Exception e) {
        }

    }

    public List<Operacao> listaDeOperacoes() {
        return opEJB.listarOperacoes();
    }

    public Operacao getOperacao() {
        return operacao;
    }

    public void setOperacao(Operacao operacao) {
        this.operacao = operacao;
    }

    public Date getDt_recebimento() {
        return dt_recebimento;
    }

    public void setDt_recebimento(Date dt_recebimento) {
        this.dt_recebimento = dt_recebimento;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Double getAcrescimo() {
        return acrescimo;
    }

    public void setAcrescimo(Double acrescimo) {
        this.acrescimo = acrescimo;
    }

    public Double getJuros() {
        return juros;
    }

    public void setJuros(Double juros) {
        this.juros = juros;
    }

    public Double getValor_recebido() {
        return valor_recebido;
    }

    public void setValor_recebido(Double valor_recebido) {
        this.valor_recebido = valor_recebido;
    }

    public Double getPercentualTotal() {
        return percentualTotal;
    }

    public void setPercentualTotal(Double percentualTotal) {
        this.percentualTotal = percentualTotal;
    }

    public CaixaPai getCaixaPai() {
        return caixaPai;
    }

    public void setCaixaPai(CaixaPai caixaPai) {
        this.caixaPai = caixaPai;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public Usuario getUsuario() {
        return usuario = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public List<Paciente> listaPacientes() {
        return pEJB.listarPacientes();
    }
    private String numCirurgia;
    private Date dtIni;
    private Date dtFim;
    private String idPac;

    public String getNumCirurgia() {
        return numCirurgia;
    }

    public void setNumCirurgia(String numCirurgia) {
        this.numCirurgia = numCirurgia;
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        this.dtIni = dtIni;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    public String getIdPac() {
        return idPac;
    }

    public void setIdPac(String idPac) {
        this.idPac = idPac;
    }

    public String formataMoeda(Double valor) {
        return NumberFormat.getCurrencyInstance().format(valor);
    }
    private Double valorTotalPgto;

    public Double getValorTotalPgto() {
        return valorTotalPgto;
    }

    public void setValorTotalPgto(Double valorTotalPgto) {
        this.valorTotalPgto = valorTotalPgto;
    }

    public void pesquisar() {
        listaRepasses = rEJB.listaRepasses();
    }

    public Repasse getRepasse() {
        return repasse;
    }

    public void setRepasse(Repasse repasse) {
        this.repasse = repasse;
    }

    public List<Repasse> getListaRepasses() {
        return listaRepasses;
    }

    public void setListaRepasses(List<Repasse> listaRepasses) {
        this.listaRepasses = listaRepasses;
    }

    public List<Repasse> getListaRepassesSelecionados() {
        return listaRepassesSelecionados;
    }

    public void setListaRepassesSelecionados(List<Repasse> listaRepassesSelecionados) {
        this.listaRepassesSelecionados = listaRepassesSelecionados;
    }

    public Recebimento getRecebimento() {
        return recebimento;
    }

    public void setRecebimento(Recebimento recebimento) {
        this.recebimento = recebimento;
    }

    public List<Repasse> listarRepassesAEstornar() {
        return rEJB.listaRepassesRecebidos();
    }

    public List<Cheque> getListaCheque() {
        return listaCheque;
    }

    public void setListaCheque(List<Cheque> listaCheque) {
        this.listaCheque = listaCheque;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getDataInicioFormatada() {
        return dataInicioFormatada;
    }

    public void setDataInicioFormatada(String dataInicioFormatada) {
        this.dataInicioFormatada = dataInicioFormatada;
    }

    public String getDataFimFormatada() {
        return dataFimFormatada;
    }

    public void setDataFimFormatada(String dataFimFormatada) {
        this.dataFimFormatada = dataFimFormatada;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCompensado() {
        return compensado;
    }

    public void setCompensado(String compensado) {
        this.compensado = compensado;
    }

    public Double getValorCheque() {
        return valorCheque;
    }

    public void setValorCheque(Double valorCheque) {
        this.valorCheque = valorCheque;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Date getDtCompensado() {
        return dtCompensado;
    }

    public void setDtCompensado(Date dtCompensado) {
        this.dtCompensado = dtCompensado;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public List<Banco> listaBancos() {
        return bancoEJB.listaBanco();
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    private void zeraVariaveisCheque() {
        numeroCheque = null;
        tipo = "";
        compensado = "";
        conta = null;
        valorCheque = 0D;
        dtEmissao = null;
        diferenca = true;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Terceiro getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(Terceiro terceiro) {
        this.terceiro = terceiro;
    }

    public boolean isDiferenca() {
        return diferenca;
    }

    public void setDiferenca(boolean diferenca) {
        this.diferenca = diferenca;
    }

    public void ConfirmarRecebimento() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        Integer idUser = Integer.parseInt(request.getParameter("idUser"));
        Usuario u = uEJB.selecionarPorID(idUser);
        try {
            for (Repasse rr : listaRepasses) {
                if (rr.isPagar()) {
                    rr.setFormaPagamento(getFormaPagamento());
                    rr.setDtRecebimento(getDt_recebimento());
                    rr.setObservacao(getObs());
                    rr.setDesconto(getDesconto());
                    rr.setAcrescimo(getAcrescimo());
                    rr.setJuros(getJuros());
                    rr.setPercentualRecebido(percentualTotal);
                    verificarMarcados();
                    if (getValor_recebido() - valorTotalPgto == 0) {
                        Double valorRestante = valorTotalPgto - getValor_recebido();
                        rr.setValorRecebido(getValor_recebido());
                        rr.setSituacaoPaciente('P');
                        rr.setSituacaoTerceiro('L');
                        rr.setStatus('F');
                        rr.setValor(valorRestante);
                        rr.setTipoPagamento('V');
                    } else {
                        Double valorRestante = valorTotalPgto - getValor_recebido();
                        rr.setValorRecebido(getValor_recebido());
                        rr.setSituacaoPaciente('X');
                        rr.setSituacaoTerceiro('L');
                        rr.setStatus('F');
                        rr.setValor(valorRestante);
                        rr.setDtRecebimento(getDt_recebimento());
                        rr.setTipoPagamento('P');
                    }
                    List<Repasse> r2 = rEJB.pesquisaRepassePorCirurgiaEParcela(rr.getIdCirurgia().getIdCirurgia(), rr.getNumeroParcela());
                    Date d = new Date();
                    Long numAleatorio = d.getTime();
                    for (Repasse r : r2) {
                        r.setDtRecebimento(rr.getDtRecebimento());
                        r.setObservacao(rr.getObservacao());
                        r.setDesconto(rr.getDesconto());
                        r.setAcrescimo(rr.getAcrescimo());
                        r.setJuros(rr.getJuros());
                        r.setSituacaoPaciente(rr.getSituacaoPaciente());
                        r.setStatus(rr.getStatus());
                        r.setSituacaoTerceiro(rr.getSituacaoTerceiro());
                        r.setDtRecebimento(rr.getDtRecebimento());
                        r.setTipoPagamento(rr.getTipoPagamento());
                        r.setPercentualRecebido(rr.getPercentualRecebido());
                        r.setFormaPagamento(rr.getFormaPagamento());
                        if (rr.getPercentualRecebido() == 100) {
                            r.setValorRecebido(r.getValorDocumento());
                            r.setValor(0D);
                            r.setSituacaoPaciente('P');
                            rEJB.Salvar(r);
                            caixa.setNumLote(numAleatorio.toString());
                            caixa.setIdUsuario(u);
//                            caixa.setIdCaixaPai(getCaixaAtual());
                            caixa.setDtCaixa(getDt_recebimento());
                            caixa.setIdRepasse(r);
                            caixa.setTipoLancamento('C');
                            caixa.setValor(r.getValorRecebido());
                            if (operacao.getIdOperacao() != null) {
                                caixa.setIdOperacao(operacao);
                            }
                            caixa.setStatus("Não Baixado");
                            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            caixa.setHistorico("Recebimento da cirurgia " + r.getIdCirurgia().getIdCirurgia() + ", Parcela " + r.getNumeroParcela() + ". Paciente " + r.getIdPaciente().getIdPaciente() + ", " + r.getIdPaciente().getNome() + ". Recebido por " + us.retornaUsuario());
                            caixa.setIdCirurgia(r.getIdCirurgia());
                            caixa.setNumeroParcela(r.getNumeroParcela());
                            cEJB.LancarRecebimentoNoCaixa(caixa);
//                            cEJB.atualizarSaldoCaixaPai(getCaixaAtual(), caixa.getValor(), caixa.getTipoLancamento());
                        } else {
                            //Cria novo documento caso o pagamento não seja efetuado totalmente
                            r.setValorRecebido((r.getValorDocumento() * rr.getPercentualRecebido()) / 100);
                            r.setValor(r.getValorDocumento() - r.getValorRecebido());
                            Repasse novoRepasse = new Repasse();
                            novoRepasse.setDtLancamento(getDt_recebimento());
                            novoRepasse.setValor(r.getValor());
                            novoRepasse.setIdCirurgia(r.getIdCirurgia());
                            novoRepasse.setNumeroParcela(r.getNumeroParcela());
                            novoRepasse.setValorDocumento(r.getValor());
                            novoRepasse.setIdForma(r.getIdForma());
                            novoRepasse.setIdPaciente(r.getIdPaciente());
                            novoRepasse.setIdTerceiro(r.getIdTerceiro());
                            novoRepasse.setIdSici(r.getIdSici());
                            novoRepasse.setDtVencimento(addMes(r.getDtRecebimento(), 1));
                            novoRepasse.setSituacaoPaciente('A');
                            novoRepasse.setSituacaoTerceiro('N');
                            novoRepasse.setStatus('A');
                            novoRepasse.setTipoPagamento(r.getTipoPagamento());
                            novoRepasse.setObservacao("Novo documento gerado do pagamento parcial da cirurgia " + rr.getIdCirurgia().getIdCirurgia() + " da parcela " + rr.getNumeroParcela() + ". ");

                            rEJB.Salvar(novoRepasse);
                            rEJB.Salvar(r);
                            //Atualizamos a conta corrente antiga
                            ContaCorrente cc = contaEJB.pesquisarContaCorrentePorRepasse(r);
                            cc.setValor(r.getValorRecebido());
                            contaEJB.atualizarContaCorrente(cc);
                            //Inserimo um registro do restante da conta corrente
                            ContaCorrente ccc = new ContaCorrente();
                            ccc.setIdTerceiro(r.getIdTerceiro());
                            ccc.setIdExame(r.getIdExame());
                            ccc.setTipo("C");
                            ccc.setObservacao("Repasse nº " + novoRepasse.getIdRepasse() + ".Parcela: " + novoRepasse.getNumeroParcela() + " .Paciente " + novoRepasse.getIdPaciente().getNome() + ". Restante do pagamento parcial do repasse " + r.getIdRepasse());
                            ccc.setDtLancamento(dt_recebimento);
                            ccc.setSituacao("Não Liberado");
                            ccc.setStatus("Não Retirado");
                            ccc.setValor(novoRepasse.getValorDocumento());
                            ccc.setIdCirurgiaItem(novoRepasse.getIdSici().getIdCirurgiaItem());
                            ccc.setIdRepasse(novoRepasse);
                            ccc.setIdPaciente(novoRepasse.getIdPaciente());
                            ccc.setIdCirurgia(novoRepasse.getIdCirurgia());
                            contaEJB.atualizarContaCorrente(ccc);
                            caixa.setNumLote(numAleatorio.toString());
                            caixa.setDtCaixa(getDt_recebimento());
                            caixa.setIdRepasse(r);
                            caixa.setTipoLancamento('C');
                            caixa.setValor(r.getValorRecebido());
                            caixa.setIdUsuario(u);
                            if (operacao.getIdOperacao() != null) {
                                caixa.setIdOperacao(operacao);
                            }
                            caixa.setStatus("Não Baixado");
                            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            caixa.setHistorico("Recebimento da cirurgiaº " + r.getIdCirurgia().getIdCirurgia() + ", Parcela " + r.getNumeroParcela() + ". Paciente " + r.getIdPaciente().getIdPaciente() + ", " + r.getIdPaciente().getNome() + ". Recebido por " + us.retornaUsuario());
                            caixa.setIdCirurgia(r.getIdCirurgia());
                            caixa.setNumeroParcela(r.getNumeroParcela());
                            cEJB.LancarRecebimentoNoCaixa(caixa);
//                            cEJB.atualizarSaldoCaixaPai(getCaixaAtual(), caixa.getValor(), caixa.getTipoLancamento());
                        }
                    }
                }
            }
            Mensagem.addMensagem(1, "Recebimento Efetuado com sucesso!");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Atenção erro ao tentar receber!" + e);
        }
        novo();
        valorTotalPgto = 0D;
        valor_recebido = 0D;
    }

    public void Receber() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        Integer idUser = Integer.parseInt(request.getParameter("idUser"));
        Usuario u = uEJB.selecionarPorID(idUser);
        try {
            for (Repasse rr : listaRepasses) {
                if (rr.isPagar()) {
                    rr.setFormaPagamento(getFormaPagamento());
                    rr.setDtRecebimento(getDt_recebimento());
                    rr.setObservacao(getObs());
                    rr.setDesconto(getDesconto());
                    rr.setAcrescimo(getAcrescimo());
                    rr.setJuros(getJuros());
                    rr.setPercentualRecebido(percentualTotal);
                    verificarMarcados();
                    if (getValor_recebido() - valorTotalPgto == 0) {
                        Double valorRestante = valorTotalPgto - getValor_recebido();
                        rr.setValorRecebido(getValor_recebido());
                        rr.setSituacaoPaciente('P');
                        rr.setSituacaoTerceiro('L');
                        rr.setStatus('F');
                        rr.setValor(valorRestante);
                        rr.setTipoPagamento('V');
                    } else {
                        Double valorRestante = valorTotalPgto - getValor_recebido();
                        rr.setValorRecebido(getValor_recebido());
                        rr.setSituacaoPaciente('X');
                        rr.setSituacaoTerceiro('L');
                        rr.setStatus('F');
                        rr.setValor(valorRestante);
                        rr.setDtRecebimento(getDt_recebimento());
                        rr.setTipoPagamento('P');
                    }
                    List<Repasse> r2 = rEJB.pesquisaRepassePorCirurgiaEParcela(rr.getIdCirurgia().getIdCirurgia(), rr.getNumeroParcela());
                    Date d = new Date();
                    Long numAleatorio = d.getTime();
                    for (Repasse r : r2) {
                        r.setDtRecebimento(rr.getDtRecebimento());
                        r.setObservacao(rr.getObservacao());
                        r.setDesconto(rr.getDesconto());
                        r.setAcrescimo(rr.getAcrescimo());
                        r.setJuros(rr.getJuros());
                        r.setSituacaoPaciente(rr.getSituacaoPaciente());
                        r.setStatus(rr.getStatus());
                        r.setSituacaoTerceiro(rr.getSituacaoTerceiro());
                        r.setDtRecebimento(rr.getDtRecebimento());
                        r.setTipoPagamento(rr.getTipoPagamento());
                        r.setPercentualRecebido(rr.getPercentualRecebido());
                        r.setFormaPagamento(rr.getFormaPagamento());
                        if (rr.getPercentualRecebido() == 100) {
                            r.setValorRecebido(r.getValorDocumento());
                            r.setValor(0D);
                            r.setSituacaoPaciente('P');
                            rEJB.Salvar(r);
                            caixa.setNumLote(numAleatorio.toString());
                            caixa.setIdUsuario(u);
//                            caixa.setIdCaixaPai(getCaixaAtual());
                            caixa.setDtCaixa(getDt_recebimento());
                            caixa.setIdRepasse(r);
                            caixa.setTipoLancamento('C');
                            caixa.setValor(r.getValorRecebido());
                            if (operacao.getIdOperacao() != null) {
                                caixa.setIdOperacao(operacao);
                            }
                            caixa.setStatus("Não Baixado");
                            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            caixa.setHistorico("Recebimento da cirurgia " + r.getIdCirurgia().getIdCirurgia() + ", Parcela " + r.getNumeroParcela() + ". Paciente " + r.getIdPaciente().getIdPaciente() + ", " + r.getIdPaciente().getNome() + ". Recebido por " + us.retornaUsuario());
                            caixa.setIdCirurgia(r.getIdCirurgia());
                            caixa.setNumeroParcela(r.getNumeroParcela());
                            cEJB.LancarRecebimentoNoCaixa(caixa);
                        } else {
                            //Cria novo documento caso o pagamento não seja efetuado totalmente
                            r.setValorRecebido((r.getValorDocumento() * rr.getPercentualRecebido()) / 100);
                            r.setValor(r.getValorDocumento() - r.getValorRecebido());
                            Repasse novoRepasse = new Repasse();
                            novoRepasse.setDtLancamento(getDt_recebimento());
                            novoRepasse.setValor(r.getValor());
                            novoRepasse.setIdCirurgia(r.getIdCirurgia());
                            novoRepasse.setNumeroParcela(r.getNumeroParcela());
                            novoRepasse.setValorDocumento(r.getValor());
                            novoRepasse.setIdForma(r.getIdForma());
                            novoRepasse.setIdPaciente(r.getIdPaciente());
                            novoRepasse.setIdTerceiro(r.getIdTerceiro());
                            novoRepasse.setIdSici(r.getIdSici());
                            novoRepasse.setDtVencimento(addMes(r.getDtRecebimento(), 1));
                            novoRepasse.setSituacaoPaciente('A');
                            novoRepasse.setSituacaoTerceiro('N');
                            novoRepasse.setStatus('A');
                            novoRepasse.setTipoPagamento(r.getTipoPagamento());
                            novoRepasse.setObservacao("Novo documento gerado do pagamento parcial da cirurgia " + rr.getIdCirurgia().getIdCirurgia() + " da parcela " + rr.getNumeroParcela() + ". ");

                            rEJB.Salvar(novoRepasse);
                            rEJB.Salvar(r);
                            //Atualizamos a conta corrente antiga
                            ContaCorrente cc = contaEJB.pesquisarContaCorrentePorRepasse(r);
                            cc.setValor(r.getValorRecebido());
                            contaEJB.atualizarContaCorrente(cc);
                            //Inserimo um registro do restante da conta corrente
                            ContaCorrente ccc = new ContaCorrente();
                            ccc.setIdTerceiro(r.getIdTerceiro());
                            ccc.setIdExame(r.getIdExame());
                            ccc.setTipo("C");
                            ccc.setObservacao("Repasse gerado pela cirurgia " + novoRepasse.getIdCirurgia().getIdCirurgia() + " da parcela " + novoRepasse.getNumeroParcela() + " do paciente " + novoRepasse.getIdPaciente().getNome() + ". Restante do pagamento parcial do repasse " + r.getIdRepasse());
                            ccc.setDtLancamento(dt_recebimento);
                            ccc.setSituacao("Não Liberado");
                            ccc.setStatus("Não Retirado");
                            ccc.setValor(novoRepasse.getValorDocumento());
                            ccc.setIdCirurgiaItem(novoRepasse.getIdSici().getIdCirurgiaItem());
                            ccc.setIdRepasse(novoRepasse);
                            ccc.setIdPaciente(novoRepasse.getIdPaciente());
                            ccc.setIdCirurgia(novoRepasse.getIdCirurgia());
                            contaEJB.atualizarContaCorrente(ccc);
                            caixa.setNumLote(numAleatorio.toString());
                            caixa.setDtCaixa(getDt_recebimento());
                            caixa.setIdRepasse(r);
                            caixa.setTipoLancamento('C');
                            caixa.setValor(r.getValorRecebido());
                            caixa.setIdUsuario(u);
                            if (operacao.getIdOperacao() != null) {
                                caixa.setIdOperacao(operacao);
                            }
                            caixa.setStatus("Não Baixado");
                            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            caixa.setHistorico("Recebimento da cirurgiaº " + r.getIdCirurgia().getIdCirurgia() + ", Parcela " + r.getNumeroParcela() + ". Paciente " + r.getIdPaciente().getIdPaciente() + ", " + r.getIdPaciente().getNome() + ". Recebido por " + us.retornaUsuario());
                            caixa.setIdCirurgia(r.getIdCirurgia());
                            caixa.setNumeroParcela(r.getNumeroParcela());
                            cEJB.LancarRecebimentoNoCaixa(caixa);
//                            cEJB.atualizarSaldoCaixaPai(getCaixaAtual(), caixa.getValor(), caixa.getTipoLancamento());
                        }
                    }
                }
            }
            processarListaRecebimentos();
            Mensagem.addMensagem(1, "Recebimento Efetuado com sucesso!");

        } catch (Exception e) {
            Mensagem.addMensagem(3, "Aconteceu algum erro ao tentar receber!");
            System.out.println(e);
        }

    }

    

    private Double valorAPagar;
    List<Repasse> repassesProcessados;

    public Double getValorAPagar() {
        return valorAPagar;
    }

    public void setValorAPagar(Double valorAPagar) {
        this.valorAPagar = valorAPagar;
    }

    public List<Repasse> getRepassesProcessados() {
        return repassesProcessados;
    }

    public void setRepassesProcessados(List<Repasse> repassesProcessados) {
        this.repassesProcessados = repassesProcessados;
    }

    public void processarItensAReceber() {
        repassesProcessados = new ArrayList<Repasse>();
        for (Repasse rr : listaRepasses) {
            if (rr.isPagar()) {
                repassesProcessados.addAll(rEJB.pesquisaRepassePorCirurgiaEParcela(rr.getIdCirurgia().getIdCirurgia(), rr.getNumeroParcela()));
            }
        }
        valorAPagar = 0D;

        for (Repasse r : repassesProcessados) {
            valorAPagar = valorAPagar + r.getValor();
        }
        valor_recebido = valorAPagar;
    }
}
