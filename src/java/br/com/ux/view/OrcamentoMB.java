/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AdiantamentosHistoricoEJB;
import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.ConvenioEJB;
import br.com.ux.controller.EmailsEJB;
import br.com.ux.controller.FormaPagamentoEJB;
import br.com.ux.controller.MotivoEJB;
import br.com.ux.controller.OrcamentoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.PlanoEJB;
import br.com.ux.controller.ProcedimentoEJB;
import br.com.ux.controller.ServicoEJB;
import br.com.ux.controller.StatusEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.UnidadeAtendimentoEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.AdiantamentosHistorico;
import br.com.ux.model.Associacao;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.Convenio;
import br.com.ux.model.Emails;
import br.com.ux.model.FormaPagamento;
import br.com.ux.model.Motivo;
import br.com.ux.model.Orcamento;
import br.com.ux.model.OrcamentoItem;
import br.com.ux.model.Paciente;
import br.com.ux.model.Plano;
import br.com.ux.model.Procedimento;
import br.com.ux.model.Servico;
import br.com.ux.model.Status;
import br.com.ux.model.Terceiro;
import br.com.ux.model.UnidadeAtendimento;
import br.com.ux.pojo.OrcamentoItemDuplicado;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Extenso;
import br.com.ux.util.Mensagem;
import br.com.ux.util.RelatorioFactory;
import br.com.ux.util.UsuarioSessao;
import com.sun.xml.ws.util.StringUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.mail.EmailException;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class OrcamentoMB implements Serializable {

    private String status;
    @EJB
    OrcamentoEJB oEJB;
    private Orcamento orcamento;
    private Orcamento orcamentoDuplicado;
    @EJB
    PacienteEJB pEJB;
    private List<Paciente> listaPacientesACarregar;
    @EJB
    ProcedimentoEJB proEJB;
    private List<Procedimento> listaProcedimentosACarregar;
    Procedimento procedimento;
    private Integer paramProcedimento = null;
    private Integer paramTerceiro = null;
    @EJB
    ConvenioEJB cEJB;
    private Convenio convenio;
    @EJB
    PlanoEJB plaEJB;
    private Plano plano;
    @EJB
    TerceiroEJB tEJB;
    private List<Terceiro> listaTerceirosACarregar;
    @EJB
    UnidadeAtendimentoEJB uaEJB;
    private UnidadeAtendimento unidadeAtendimento;
    @EJB
    FormaPagamentoEJB fpEJB;
    private FormaPagamento formaPagamento;
    private OrcamentoItem orcamentoItem, itemDuplicado;
    @EJB
    ServicoEJB sEJB;
    private List<Servico> listaDeServicos;
    private List<OrcamentoItem> itens;
    private UsuarioSessao us = new UsuarioSessao();
    @EJB
    MotivoEJB mEJB;
    private Motivo motivo;
    private String acao;
    private Paciente responsavel;
    private List<Orcamento> orcamentosFiltrados;
    private LazyDataModel<Paciente> model;
    @EJB
    AdiantamentosHistoricoEJB ahEJB;
    private AdiantamentosHistorico ah;
    private String paramAdiantamento = null;
    private String paramSemFechamentos = null;
    private String paramTipoData;
    private Date horarioInicialPesquisa;
    private Date horarioFinalPesquisa;
    @EJB
    AuditoriaEJB aEJB;
    @EJB
    AssociacaoEJB assEJB;
    @EJB
    StatusEJB stEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    EmailsEJB eEJB;
    private Emails email;
    private String textoModelo;
    private List<AdiantamentosHistorico> listaAdiantamentos;
    private Double valorTotal, valorPago, valorRestante;
    //  Controle de Adiantamentos  //
    private String tipo, numeroCheque, observacao, pagadorAdiantamento;
    private Double valorAdiantamento;
    //  Consultas  //
    private Integer idOrcamentoPesquisa;
    private Date dtInicialPesquisa;
    private Date dtFinalPesquisa;
    private Integer idPacientePesquisa;
    private String situacaoPesquisa;
    private String statusPesquisa;
    private Integer idTerceiroPesquisa;
    private Integer idProcedimentoPesquisa;
    private Integer idStatusPesquisa;
    private String statusAdiantamento, nomePacientePesquisa;

    private boolean recebimentoEmail;
    private LazyDataModel<Procedimento> modelProcedimento;

    public OrcamentoMB() {
        setStatus("Consultando");
        novo();
        setAcao("inserindo");
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public void novo() {
        orcamento = new Orcamento();
        convenio = new Convenio();
        email = new Emails();
        plano = new Plano();
        unidadeAtendimento = new UnidadeAtendimento();
        formaPagamento = new FormaPagamento();
        motivo = new Motivo(1);
        ah = new AdiantamentosHistorico();
        listaAdiantamentos = new ArrayList<AdiantamentosHistorico>();
        listaPacientesACarregar = new ArrayList<Paciente>();
        listaProcedimentosACarregar = new ArrayList<Procedimento>();
        listaTerceirosACarregar = new ArrayList<Terceiro>();
        listaDeServicos = new ArrayList<Servico>();
        listaOrcamentoAPesquisar = new ArrayList<Orcamento>();
        orcamentoItem = new OrcamentoItem();
        itens = new ArrayList<OrcamentoItem>();
        orcamento.setDtOrcamento(new Date());
        orcamento.setHoraCadastro(new Date());
        //======================================================================
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(gc.MONTH, 1);
        Date d1 = gc.getTime();
        orcamento.setDtValidade(d1);
        //======================================================================

        orcamento.setValor(0D);
        orcamentoItem.setQuantidade(1D);
        orcamentoItem.setValorUnitario(0D);
        orcamentoItem.setValorTotal(0D);
        orcamento.setValor(0D);
        setAcao("Inserindo");
        responsavel = new Paciente();
        valorTotal = 0D;
        dtInicialPesquisa = BCRUtils.primeiroDiaDoMes(BCRUtils.addMes(new Date(), -1));
        dtFinalPesquisa = BCRUtils.ultimoDiaDoMes(new Date());
        recebimentoEmail = false;
        procedimento = new Procedimento();
    }

    public void carregarPacientesLazyModel() {
        model = new LazyDataModel<Paciente>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Paciente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'" + filterValue + "%'");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                System.out.println("passei");
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(pEJB.contarPacientesRegistro(Clausula).toString()));
                listaPacientesACarregar = pEJB.listarPacientesLazyModeWhere(first, pageSize, Clausula);
                return listaPacientesACarregar;
            }
        };
    }

    public void carregarProcedimentoLazyModel() {
        modelProcedimento = new LazyDataModel<Procedimento>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Procedimento> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'" + filterValue + "%' and p.inativo = FALSE");
                        } else {
                            sf.append(" where p." + filterProperty + " like'" + filterValue + "%' and p.inativo = FALSE");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'" + filterValue + "%' and p.inativo = FALSE");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(proEJB.contarProcedimentosRegistro(Clausula).toString()));
                listaProcedimentosACarregar = proEJB.listarProcedimentosLazyModeWhere(first, pageSize, Clausula);
                return listaProcedimentosACarregar;
            }
        };
    }

    public void selecionarPacientePesquisa(Paciente paciente) {
        idPacientePesquisa = paciente.getIdPaciente();
        nomePacientePesquisa = paciente.getNome();
    }

    public void zerarPaciente() {
        idPacientePesquisa = null;
        nomePacientePesquisa = "";
    }

    public void somentePacienteNaoCadastrados() {
        idPacientePesquisa = 0;
        nomePacientePesquisa = "Somente paciente não cadastrados";
    }

    public Orcamento getOrcamento() {
        return orcamento;
    }

    public Convenio getConvenio() {
        return convenio;
    }

    public Plano getPlano() {
        return plano;
    }

    public void setOrcamento(Orcamento orcamento) {
        this.orcamento = orcamento;
    }

    public void setListaPacientesACarregar(List<Paciente> listaPacientesACarregar) {
        this.listaPacientesACarregar = listaPacientesACarregar;
    }

    public void setListaProcedimentosACarregar(List<Procedimento> listaProcedimentosACarregar) {
        this.listaProcedimentosACarregar = listaProcedimentosACarregar;
    }

    public List<Orcamento> getOrcamentosFiltrados() {
        return orcamentosFiltrados;
    }

    public void setOrcamentosFiltrados(List<Orcamento> orcamentosFiltrados) {
        this.orcamentosFiltrados = orcamentosFiltrados;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }

    public void setListaTerceirosACarregar(List<Terceiro> listaTerceirosACarregar) {
        this.listaTerceirosACarregar = listaTerceirosACarregar;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public OrcamentoItem getOrcamentoItem() {
        return orcamentoItem;
    }

    public void setOrcamentoItem(OrcamentoItem orcamentoItem) {
        this.orcamentoItem = orcamentoItem;
    }

    public List<OrcamentoItem> getItens() {
        return itens;
    }

    public void setItens(List<OrcamentoItem> itens) {
        this.itens = itens;
    }

    public void carregarListaPacientes() {
        listaPacientesACarregar = pEJB.listarPacientes();
        BCRUtils.ResetarDatatableFiltros("tblconsulta:dtl");
    }

    public List<Paciente> getListaPacientesACarregar() {
        return listaPacientesACarregar;
    }

    public void selecionarPaciente(Paciente paciente) {
        orcamento.setIdPaciente(paciente);
    }

    public List<Procedimento> getListaProcedimentosACarregar() {
        return listaProcedimentosACarregar;
    }

    public void carregarListaProcedimentos() {
        BCRUtils.ResetarDatatableFiltros("frmConsultarProcedimentos:tblConsultaProc");
        listaProcedimentosACarregar = proEJB.listarProcedimentos();
    }

    public void selecionarProcedimento(Procedimento proc) {
        procedimento = proc;
        orcamento.setIdProcedimento(proc);
    }

    public List<Servico> getListaDeServicos() {
        return listaDeServicos;
    }

    public void setListaDeServicos(List<Servico> listaDeServicos) {
        this.listaDeServicos = listaDeServicos;
    }

    public List<Convenio> listaConvenios() {
        return cEJB.listarConvenios();
    }

    public List<Status> listaStatus() {
        return stEJB.listarStatus();
    }

    public List<Plano> listaPlanosPorConvenio() {
        return plaEJB.listaPlanosPorConvenio(orcamento.getIdConvenio().getIdConvenio());
    }

    public Paciente getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Paciente responsavel) {
        this.responsavel = responsavel;
    }

    public void selecionaTerceiroResponsavel(Terceiro terceiro) {
        orcamento.setIdTerceiro(terceiro);
    }

    public void carregarTerceiros() {
        listaTerceirosACarregar = tEJB.listarTerceiro();
    }

    public List<Paciente> listaDePacientes() {
        return pEJB.listarPacientes();
    }

    public List<Terceiro> getListaTerceirosACarregar() {
        return listaTerceirosACarregar;
    }

    public List<UnidadeAtendimento> listaUnidadesDeATendimento() {
        return uaEJB.listarUnidadesAtendimento();
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public List<FormaPagamento> listaFormasDePagamento() {
        return fpEJB.listaFormasPagamento();
    }

    public void carregarServicos() {
        listaDeServicos = new ArrayList<Servico>();
        //listaDeServicos = sEJB.listarServicos();
    }

    public void verificarServicoSetado() {
        orcamentoItem.setValorUnitario(0D);
        //System.out.println(orcamentoItem.getIdServico().getDescricao());
    }

    public String pegarGrupo(Servico s) {
        String grupo = "";
        grupo = s.getTipo().charAt(0) == 'N' ? "Honorários" : "Hospitalares";
        return grupo;
    }

    public String getStatus() {
        return status;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void calcularTotalItem() {
        if (orcamentoItem.getValorUnitario() == null) {
            orcamentoItem.setValorUnitario(0D);
        }
        if (orcamentoItem.getQuantidade() == null) {
            orcamentoItem.setQuantidade(0D);
        }
        orcamentoItem.setValorTotal(orcamentoItem.getValorUnitario() * orcamentoItem.getQuantidade());
//        if (orcamentoItem.getValorTotal() > 0D && orcamentoItem.getIdServico() != null) {
//            addItem();
//        }
    }

    public List<Servico> completeMethodServico(String var) {
        return sEJB.autoCompleteServico(var);
    }

    public List<Procedimento> completeMethodProcedimento(String var) {
        return proEJB.autoCompleteProcedimento(var);
    }

    public List<Terceiro> completeMethodTerceiro(String var) {
        return tEJB.autoCompleteTerceiro(var);
    }

    public boolean verificarDuplicados(List<OrcamentoItem> listaItens) {
        for (OrcamentoItem oi : itens) {
            if (orcamentoItem.getIdServico().getIdServico() == oi.getIdServico().getIdServico()) {
                itemDuplicado = new OrcamentoItem();
                itemDuplicado = oi;
                return true;
            }
        }
        return false;
    }

    public void addItem() {
        if (orcamentoItem.getIdServico() == null) {
            Mensagem.addMensagem(3, "Informe um item antes de adicionar!");
        } else if (!verificarDuplicados(itens)) {
            calcularTotalItem();
            itens.add(orcamentoItem);
            Double tot = 0D;
            tot = orcamentoItem.getValorTotal();
            Double valorAnterior = orcamento.getValor();
            orcamento.setValor(valorAnterior + tot);
            limparItem();
            Mensagem.addMensagemGrowl(1, "Item adicionado com sucesso!");
        } else {
            Mensagem.addMensagem(3, "Atenção este serviço já está adicionado!");
        }
        //System.out.println("valor do orçamento: " + orcamento.getValor());
    }

    public Double getSomaValores(List<Orcamento> listaOrcamento) {
        Double total = new Double(0);
        for (Orcamento o : listaOrcamento) {
            total += o.getValor();
        }
        return total;
    }

    public void limparItem() {
        orcamentoItem = new OrcamentoItem();
        orcamentoItem.setValorTotal(0D);
        orcamentoItem.setValorUnitario(0D);
        orcamentoItem.setQuantidade(1D);
        RequestContext.getCurrentInstance().execute("zerafield();");
    }

    public void calcularTotalItemIndex(OrcamentoItem oi, int index) {
        Double valorAntigo = 0D;
        if (oi.getValorUnitario() == null) {
            oi.setValorUnitario(0D);
        }
        if (oi.getQuantidade() == null) {
            oi.setQuantidade(0D);
        }
        valorAntigo = oi.getValorTotal();
        oi.setValorTotal(oi.getValorUnitario() * oi.getQuantidade());
        itens.remove(index);
        itens.add(index, oi);
        valorTotal = 0D;
        for (OrcamentoItem oi1 : itens) {
            valorTotal = valorTotal + oi1.getValorTotal();
        }
        orcamento.setValor(valorTotal);
        Mensagem.addMensagemGrowl(1, "Cálculos realizado com sucesso!");

    }

    public void deletarItem(OrcamentoItem oi, int indice) {
        try {
            if (oi.getIdOrcamentoItem() != null) {
                oEJB.ExcluirItem(oi);
            }
            itens.remove(indice);
            calcularTotalOrcamento();
            Mensagem.addMensagem(1, "Item Removido com sucesso!");
            BCRUtils.ResetarDatatableFiltros("frmConsultar:tblPesq");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar remover o item!" + e);
        }

    }

    private void calcularTotalOrcamento() {
        Double valor = 0D;
        for (OrcamentoItem oo : itens) {
            valor = valor + oo.getValorTotal();

        }
        orcamento.setValor(valor);
    }

    private void calcularTotalOrcamentoSalvar(Integer idOrcamento) {
        Double valor = 0D;
        Orcamento orc = oEJB.selecionarPorID(idOrcamento);
        if (orc != null) {
            for (OrcamentoItem oo : orc.getOrcamentoItemList()) {
                valor = valor + oo.getValorTotal();
            }
            orc.setValor(valor);
            oEJB.SalvarSimples(orc);
        }
    }

    public Date pegaDtFinalizacao(List<Cirurgia> cirurgiaList) {
        if (cirurgiaList == null) {
            return null;
        } else {
            for (Cirurgia c : cirurgiaList) {
                if (c.getDtFinalizacao() != null) {
                    return c.getDtFinalizacao();
                }
            }
        }
        return null;
    }

    public Date pegaDtInternacao(List<Cirurgia> cirurgiaList) {
        if (cirurgiaList == null) {
            return null;
        } else {
            for (Cirurgia c : cirurgiaList) {
                if (c.getDtFinalizacao() != null) {
                    return c.getDtInternacao();
                }
            }
        }
        return null;
    }

    public void verificarRegistrosDuplicados() {
        if (oEJB.verificarRegistrosDuplicadosOrcamento().size() > 0) {
            for (OrcamentoItemDuplicado oid : oEJB.verificarRegistrosDuplicadosOrcamento()) {
                System.out.println("Orçamento: " + oid.getIdOrcamento());
                OrcamentoItem oiTemp = oEJB.selecionaItemPorID(oid.getIdOrcamentoItem());
                oEJB.ExcluirItensPorServico(oid.getIdServico(), oid.getIdOrcamento(), oiTemp.getIdOrcamentoItem());
            }
        }
    }

    public void Salvar() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String action = params.get("liberado") == null ? "" : params.get("liberado");
        if (validarCamposObrigatorios(action)) {
            if (orcamento.getIdOrcamento() == null) {
                try {
                    if (orcamento.getDtPrevisao() == null) {
                        orcamento.setDtPrevisao(BCRUtils.addDia(new Date(), 15));
                    }
//                    if (convenio.getIdConvenio() != null) {
//                        orcamento.setIdConvenio(convenio);
//                    }
                    if (formaPagamento.getIdForma() != null) {
                        orcamento.setIdForma(formaPagamento);
                    }
                    if (motivo.getIdMotivo() != null) {
                        orcamento.setIdMotivo(motivo);
                    }
                    if (plano.getIdPlano() != null) {
                        orcamento.setIdPlano(plano);
                    }
                    if (unidadeAtendimento.getIdUnidade() != null) {
                        orcamento.setIdUnidade(unidadeAtendimento);
                    }
                    if (responsavel.getIdPaciente() != null) {
                        orcamento.setIdResponsavel(responsavel);
                    }

                    orcamento.setIdStatus(stEJB.selecionarPorTipo(1));
                    orcamento.setSituacao("Lançado");

                    if (action.equals("S")) {
                        orcamento.setStatus("Liberado");
                    } else {
                        orcamento.setStatus("Não Liberado");
                    }

                    calcularTotalOrcamento();
                    // Seta Usuário de Criação.
                    orcamento.setIdUsuarioCriacao(uEJB.retornaUsuarioDaSessao());
                    orcamento.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());

                    if (orcamento.getDtOrcamento() != null) {
                        oEJB.Salvar(orcamento, itens, us.retornaUsuario(), getAcao());
                    }

                    if (action.equals("S")) {
                        Mensagem.addMensagemGrowl(1, "Liberado e salvo com sucesso!");
                        aEJB.Salvar(BCRUtils.criarAuditoria("liberarorcamento", orcamento.getIdOrcamento().toString(), us.retornaUsuario(), new Date(), "Orçamento"));
                    } else {
                        Mensagem.addMensagemGrowl(1, "Salvo com sucesso! Não liberado.");
                    }
                    System.out.println("Orçamento salvo ");

                } catch (Exception e) {
                    Mensagem.addMensagemPadraoErro("salvar");
                    System.out.println(e);
                }
            } else {
                try {
//                    if (convenio.getIdConvenio() != null) {
//                        orcamento.setIdConvenio(convenio);
//                    }
                    if (formaPagamento.getIdForma() != null) {
                        orcamento.setIdForma(formaPagamento);
                    }
                    if (motivo.getIdMotivo() != null) {
                        orcamento.setIdMotivo(motivo);
                    }
                    if (plano.getIdPlano() != null) {
                        orcamento.setIdPlano(plano);
                    }
                    if (unidadeAtendimento.getIdUnidade() != null) {
                        orcamento.setIdUnidade(unidadeAtendimento);
                    }
                    if (responsavel.getIdPaciente() != null) {
                        orcamento.setIdResponsavel(responsavel);
                    }

                    if (orcamento.getAdiantamentosHistoricoList() == null) {
                        orcamento.setIdStatus(stEJB.selecionarPorTipo(1));
                        orcamento.setSituacao("Lançado");
                    }

                    if (action.equals("S")) {
                        orcamento.setStatus("Liberado");
                    } else {
                        orcamento.setStatus("Não Liberado");
                    }

                    if (orcamento.getDtOrcamento() == null) {
                        orcamento.setDtOrcamento(new Date());
                    }

                    calcularTotalOrcamento();

                    // Seta Usuário da Últ. Alteração.
                    orcamento.setIdUsuarioUltimaAlteracao(uEJB.retornaUsuarioDaSessao());
                    oEJB.Salvar(orcamento, itens, us.retornaUsuario(), getAcao());

                    if (action.equals("S")) {
                        Mensagem.addMensagemGrowl(1, "Liberado e editado com sucesso!");
                        aEJB.Salvar(BCRUtils.criarAuditoria("liberarorcamento", orcamento.getIdOrcamento().toString(), us.retornaUsuario(), new Date(), "Orçamento"));
                    } else {
                        Mensagem.addMensagemGrowl(1, "Editado com sucesso! Não liberado.");
                    }

                    System.out.println("Orçamento editado ");

                } catch (Exception e) {
                    Mensagem.addMensagemPadraoErro("editar");
                    System.out.println(e);
                }
            }
        }
        // Verifica se há registros duplicados lançados no itens do Orçamento.
        verificarRegistrosDuplicados();
    }

    public boolean validaDuplicidade(Integer idPaciente, Integer idTerceiro, Integer idProcedimento, Double valor) {
        orcamentoDuplicado = new Orcamento();
        if (idTerceiro != null && idProcedimento != null && valor != null && valor != 0 && idTerceiro != 0 && idProcedimento != 0) {
            orcamentoDuplicado = oEJB.listaOrcamentoParaVerificarDuplicidade(idPaciente, idTerceiro, idProcedimento, valor);
            System.out.println("ORC2: " + orcamentoDuplicado);
            if (orcamentoDuplicado.getIdOrcamento() != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean validarCamposObrigatorios(String parametro) {
        boolean retorno = false;
        if (itens.isEmpty()) {
            Mensagem.addMensagem(3, "Este orçamento não possui itens adicionados.");
            retorno = false;
            return retorno;
        }
        if (orcamento.getIdProcedimento() == null) {
            Mensagem.addMensagem(3, "Informe o procedimento!");
            retorno = false;
            return retorno;
        } else if (orcamento.getIdTerceiro() == null) {
            Mensagem.addMensagem(3, "Informe o médico responsável!");
            retorno = false;
            return retorno;
        } else {
            retorno = true;
        }
        return retorno;
    }

    public Orcamento selecionarPorID(Integer id) {
        mudaStatusInserindo();
        calcularTotalOrcamentoSalvar(id);
        orcamento = oEJB.selecionarPorID(id);
        if (!orcamento.getOrcamentoItemList().isEmpty()) {
            itens = new ArrayList<>();
            itens.addAll(orcamento.getOrcamentoItemList());
        }

        listaAdiantamentos = new ArrayList<>();
        if (!orcamento.getAdiantamentosHistoricoList().isEmpty()) {
            listaAdiantamentos.addAll(orcamento.getAdiantamentosHistoricoList());
        }

        if (orcamento.getIdResponsavel() != null) {
            System.out.println(orcamento.getIdResponsavel());
            responsavel = orcamento.getIdResponsavel();
        }
        if (orcamento.getIdPlano() != null) {
            plano = orcamento.getIdPlano();
        }
        if (orcamento.getIdForma() != null) {
            formaPagamento = orcamento.getIdForma();
        }
        if (orcamento.getIdMotivo() != null) {
            motivo = orcamento.getIdMotivo();
        }
        if (orcamento.getIdUnidade() != null) {
            unidadeAtendimento = orcamento.getIdUnidade();
        }

        return orcamento;

    }

    public Orcamento selecionarPorIDInfo(Integer id) {
        calcularTotalOrcamentoSalvar(id);
        orcamento = new Orcamento();
        orcamento = oEJB.selecionarPorID(id);
        listaAdiantamentos = new ArrayList<>();
        if (!orcamento.getAdiantamentosHistoricoList().isEmpty()) {
            listaAdiantamentos.addAll(orcamento.getAdiantamentosHistoricoList());
        }
        itens = new ArrayList<>();
        itens = orcamento.getOrcamentoItemList();
        return orcamento;
    }

    public void setarDataAgendamento() {
        orcamento.setDtAgendado(BCRUtils.addDia(new Date(), 30));
    }

    public void limparPaciente() {
        Integer id = orcamento.getIdPaciente().getIdPaciente();
        orcamento.setIdPaciente(pEJB.selecionarPorID(id, null));
    }

    public void limpaObjeto() {
        orcamento = new Orcamento();
        mudaStatusConsultando();
    }

    public String maiuscula(String a) {
        return StringUtils.capitalize(a);

    }

    public Orcamento copiar(Integer id) {
        try {
            mudaStatusInserindo();
            calcularTotalOrcamentoSalvar(id);
            Orcamento o = new Orcamento();
            List<OrcamentoItem> itens2 = new ArrayList<>();
            o = oEJB.selecionarPorID(id);
            o.setIdPaciente(null);
            o.setPacienteNaoCadastradado(null);
            o.setIdTerceiro(null);
            o.setDtOrcamento(new Date());
            o.setDtPrevisao(BCRUtils.addDia(new Date(), 15));
            o.setIdStatus(stEJB.selecionarPorTipo(1));
            o.setSubMotivo(null);
            o.setHoraCadastro(new Date());
            o.setDtEnvioEmail(null);
            o.setDtRecebimentoEmailOpme(null);
            // =====================================================================
            GregorianCalendar gc = new GregorianCalendar();
            gc.add(gc.MONTH, 1);
            o.setDtValidade(gc.getTime());
            //======================================================================
            o.setIdOrcamento(null);
            o.setDtCancelamento(null);
            o.setDtExecucao(null);
            itens2 = new ArrayList<OrcamentoItem>();
            if (!o.getOrcamentoItemList().isEmpty()) {
                for (OrcamentoItem oi : o.getOrcamentoItemList()) {
                    oi.setIdOrcamento(new Orcamento());
                    oi.setIdOrcamentoItem(null);
                    itens2.add(oi);
                }
            }
//            if (o.getIdConvenio() != null) {
//                convenio = o.getIdConvenio();
//            }
//            if (o.getIdPlano() != null) {
//                plano = o.getIdPlano();
//            }
//            if (o.getIdForma() != null) {
//                formaPagamento = o.getIdForma();
//            }
//            if (orcamento.getIdUnidade() != null) {
//                unidadeAtendimento = o.getIdUnidade();
//            }
//            if (o.getIdMotivo() != null) {
//                motivo = o.getIdMotivo();
//            }

            orcamento = new Orcamento();
            itens = new ArrayList<OrcamentoItem>();
            orcamento = o;
            itens = itens2;
            setAcao("Copiar");
            Mensagem.addMensagem(1, "Orçamento copiado com sucesso!");
            aEJB.Salvar(BCRUtils.criarAuditoria("copiar", id.toString(), us.retornaUsuario(), new Date(), "Orçamento"));
            return orcamento;
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar copiar o orçamento!");
            return null;
        }

    }

    public Orcamento selecionarPorIDExclusao(Integer id) {
        orcamento = oEJB.selecionarPorID(id);
        if (!orcamento.getOrcamentoItemList().isEmpty()) {
            itens = orcamento.getOrcamentoItemList();
        }
        if (orcamento.getIdConvenio() != null) {
            convenio = orcamento.getIdConvenio();
        }
        if (orcamento.getIdPlano() != null) {
            plano = orcamento.getIdPlano();
        }
        if (orcamento.getIdForma() != null) {
            formaPagamento = orcamento.getIdForma();
        }
        if (orcamento.getIdMotivo() != null) {
            motivo = orcamento.getIdMotivo();
        }
        if (orcamento.getIdUnidade() != null) {
            unidadeAtendimento = orcamento.getIdUnidade();
        }
        orcamento.setSubMotivo(null);
        orcamento.setIdMotivo(null);
        return orcamento;
    }

    public void liberar(Integer id) {
        try {
            orcamento = oEJB.selecionarPorID(id);
            orcamento.setStatus("Liberado");
            orcamento.setSituacao("Andamento");
            orcamento.setIdStatus(stEJB.selecionarPorTipo(1));
            oEJB.Liberar(orcamento);
            listaOrcamentoAPesquisar = oEJB.pesquisarOrcamentos(dtInicialPesquisa, dtFinalPesquisa, idPacientePesquisa, situacaoPesquisa, idOrcamentoPesquisa, procedimento == null ? null : procedimento.getIdProcedimento(), getIdTerceiroPesquisa(), getIdStatusPesquisa(), getStatusAdiantamento(), getParamSemFechamentos(), "Orçamento");
            Mensagem.addMensagem(1, "Orçamento foi liberado com sucesso!");
            aEJB.Salvar(BCRUtils.criarAuditoria("liberarorcamento", id.toString(), us.retornaUsuario(), new Date(), "Orçamento"));
            BCRUtils.ResetarDatatableFiltros("frmConsultar:tblPesq");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar liberar o orçamento!");
        }

    }

    public void adicionarAdiantamento(Integer id) {
        calcularTotalOrcamentoSalvar(id);
        orcamento = oEJB.selecionarPorID(id);
        if (orcamento.getIdPaciente() == null) {
            Mensagem.addMensagem(3, "Informe um paciente cadastrado.");
        } else if (!orcamento.getAdiantamentosHistoricoList().isEmpty()) {
            listaAdiantamentos = orcamento.getAdiantamentosHistoricoList();
            calcularValoresAdiantamento();
            zeraVariaveisAdiantamento();
            if (valorRestante != 0) {
                valorAdiantamento = valorRestante;
            }
            pagadorAdiantamento = orcamento.getIdPaciente().getNome();
        } else {
            valorTotal = 0D;
            zeraVariaveisAdiantamento();
            listaAdiantamentos = new ArrayList<>();
            valorAdiantamento = orcamento.getValor();
        }
    }

    public void incluir() {
        try {
            ah = new AdiantamentosHistorico();
            if (orcamento.getIdOrcamento() != null) {
                ah.setIdOrcamento(orcamento);
            }

            orcamento.setIdUsuarioAdiantamento(uEJB.retornaUsuarioDaSessao());
            orcamento.setDtAdiantamento(new Date());
            orcamento.setIdStatus(stEJB.selecionarPorTipo(2));
            orcamento.setStatus("Liberado");
            oEJB.atualizarOrcamento(orcamento);

            ah.setNumeroCheque(numeroCheque);
            ah.setObservacao(observacao);
            if (!pagadorAdiantamento.equals("") || pagadorAdiantamento != null) {
                ah.setPagador(pagadorAdiantamento);
            } else {
                if (orcamento.getIdPaciente() == null) {
                    ah.setPagador(orcamento.getPacienteNaoCadastradado());
                } else {
                    ah.setPagador(orcamento.getIdPaciente().getNome());
                };
            }
            ah.setStatus("L");
            ah.setTipo(tipo);
            ah.setValor(valorAdiantamento);
            ahEJB.Salvar(ah, us.retornaUsuario());
            listaAdiantamentos.add(ah);
            calcularValoresAdiantamento();
            zeraVariaveisAdiantamento();
            if (valorRestante != 0) {
                valorAdiantamento = valorRestante;
            }
            Mensagem.addMensagemPadraoSucesso("salvar");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagemPadraoErro("salvar");
        }
    }

    public void calcularValoresAdiantamento() {
        valorTotal = 0D;
        valorPago = 0D;
        valorRestante = 0D;
        valorTotal = orcamento.getValor();
        for (AdiantamentosHistorico ah : listaAdiantamentos) {
            valorPago = valorPago + ah.getValor();
        }
        if (valorPago > valorTotal) {
            valorRestante = valorTotal;
        } else {
            valorRestante = valorTotal - valorPago;
        }

    }

    public void zeraVariaveisAdiantamento() {
        ah = new AdiantamentosHistorico();
        numeroCheque = "";
        observacao = "";
        pagadorAdiantamento = "";
        tipo = "C";
        valorAdiantamento = 0D;
        RequestContext.getCurrentInstance().execute("limparCampos();");
    }

    public void salvarEmOrcamento(List<AdiantamentosHistorico> list) {
        try {
            for (AdiantamentosHistorico historico : list) {
                ahEJB.Salvar(historico, us.retornaUsuario());
            }
            Mensagem.addMensagemPadraoSucesso("salvar");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
        }

    }

    public void atualizarOrcamentoStatus(Orcamento o) {
        try {
            if (o.getIdStatus().getMotivoObrigatorio() && (o.getSubMotivo().isEmpty() || o.getSubMotivo().equals("") || o.getSubMotivo() == null)) {
                Mensagem.addMensagem(1, "Informe o motivo.");
            } else {
                if (o.getIdStatus().getFixo() != null) {
                    if (o.getIdStatus().getFixo().equals(3) || o.getIdStatus().getFixo().equals(5)) {
                        o.setDtCancelamento(new Date());
                        o.setIdUsuarioCancelamento(uEJB.retornaUsuarioDaSessao());
                        oEJB.atualizarOrcamento(o);
                        Mensagem.addMensagem(1, "Status atualizado com sucesso!");
                    } else {
                        if (o.getIdStatus().getFixo().equals(4)) {
                            // Muda o status do Orçamento para não ser mais utilizado
                            o.setIdStatus(stEJB.selecionarPorTipo(4));
                            o.setDtExecucao(new Date());
                        }
                        o.setDtCancelamento(null);
                        oEJB.atualizarOrcamento(o);
                        Mensagem.addMensagem(1, "Status atualizado com sucesso!");
                    }

                    /* 
                       26052017 - Carla - Via grupo WhatsApp ()
                       Retirada a verificação se o paciente está cadastrado para permitir a mudança de status no orçamento, sem paciente vinculado.    
                        
                       // } else if (o.getIdPaciente() == null && (!o.getIdStatus().getFixo().equals(1) || !o.getIdStatus().getFixo().equals(7))) {
                       // Mensagem.addMensagem(3, "Informe um paciente cadastrado.");
                     */
                } else {
                    oEJB.atualizarOrcamento(o);
                    Mensagem.addMensagem(1, "Status atualizado com sucesso!");
                }

                listaOrcamentoAPesquisar = oEJB.pesquisarOrcamentos(dtInicialPesquisa, dtFinalPesquisa, idPacientePesquisa, situacaoPesquisa, idOrcamentoPesquisa, getIdProcedimentoPesquisa(), getIdTerceiroPesquisa(), getIdStatusPesquisa(), getStatusAdiantamento(), getParamSemFechamentos(), "Orçamento");
            }

        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao atualizar. Tente novamente.");
            System.out.println(e);
        }
    }

    public void setarDataRecebimentoEmailOPME() {
        if (recebimentoEmail) {
            orcamento.setDtRecebimentoEmailOpme(new Date());
        }
    }

    public void zerarEmail() {
        orcamento = new Orcamento();
        email = new Emails();
    }

    public String criarTextoMensagem(String obs) {
        String nome = "";
        System.out.println(orcamento.getIdPaciente());
        if (orcamento.getIdPaciente() == null) {
            nome = orcamento.getPacienteNaoCadastradado();
        } else {
            nome = orcamento.getIdPaciente().getNome();
        }
        return "<div style=\"font-weight: normal; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal;\">Olá,</div><div style=\"font-weight: normal; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal;\"><br></div><div style=\"font-weight: normal; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal;\">Solicito o orçamento dos materiais OPME para o seguinte procedimento:</div><div><ul><li style=\"font-size: 12px; font-weight: normal;\"><span style=\"font-size: 13.3333px;\"><span style=\"font-weight: bold;\">Médico</span>: " + orcamento.getIdTerceiro().getNome() + "</span></li><li style=\"font-size: 12px; font-weight: normal;\"><span style=\"font-size: 13.3333px;\"><span style=\"font-weight: bold;\">Paciente</span>: " + nome + "</span></li><li style=\"font-size: 12px; font-weight: normal;\"><span style=\"font-size: 13.3333px;\"><span style=\"font-weight: bold;\">Convênio</span>: " + orcamento.getIdConvenio().getDescricao() + "</span></li><li style=\"font-size: 12px; font-weight: normal;\"><span style=\" font-size: 13.3333px;\"><span style=\"font-weight: bold;\">Procedimento</span>: " + orcamento.getIdProcedimento().getDescricao() + ".</span></li><li><span style=\"font-size: 13.3333px; font-weight: bold;\">Materiais: </span><span style=\"font-size: 13.3333px;\">" + obs + "</span></li></ul><div style=\"font-weight: normal;\"><span style=\"font-size: 13.3333px;\"></span></div><div style=\"font-weight: normal; font-size: 12px;\"><span style=\"font-size: 13.3333px;\">Aguardo retorno,</span></div></div><div style=\"font-weight: normal; font-size: 10pt;\"><span style=\"font-size: 13.3333px;\"><br></span></div><div style=\"font-weight: normal; font-size: 10pt;\"><span style=\"font-size: 13.3333px;\">Att.,</span></div><div><span style=\"font-size: 13.3333px; font-weight: bold;\">" + uEJB.retornaUsuarioDaSessao().getNomeCompleto() + "</span></div><div style=\"font-weight: normal;\"><span style=\"font-size: 13.3333px;\">" + uEJB.retornaUsuarioDaSessao().getEmail() + "</span></div>";
    }

    public boolean verificarCamposEnvioEmail() {
        // Verificar campos de configuração do servidor de Email
        if ((assEJB.carregarAssociacao().getSmtpServer() == null || assEJB.carregarAssociacao().getSmtpServer().equals("")) || (assEJB.carregarAssociacao().getSenhaEmailPadrao() == null || assEJB.carregarAssociacao().getSenhaEmailPadrao().equals(""))
                || (assEJB.carregarAssociacao().getPortaSmtp() == 0) || assEJB.carregarAssociacao().getEmailPadrao() == null) {
            return true;
        } else // Verificar campos de email do usuário emitente.
         if (uEJB.retornaUsuarioDaSessao().getEmail() == null || uEJB.retornaUsuarioDaSessao().getEmail().equals("")) {
                return true;
            } else {
                return false;
            }
    }

    public void carregarValoresEmail(Orcamento o) {
        orcamento = o;
        if (verificarCamposEnvioEmail()) {
            RequestContext.getCurrentInstance().execute("PF('dlgSolicitarOPME').hide();");
            Mensagem.addMensagem(3, "Informe as configurações de emails corretamente e/ou o email no seu usuário.");
        } else {
            email = new Emails();
            String nome = orcamento.getIdPaciente() != null ? orcamento.getIdPaciente().getNome() : orcamento.getPacienteNaoCadastradado();
            email.setAssunto("Solicitação Orçamento OPME - Paciente: " + nome);
            email.setCategoria("Solicitação OPME");
            email.setDtEnvio(new Date());
            email.setIdOrcamento(o);
            email.setMensagem(criarTextoMensagem(""));
            email.setIdUsuarioEmitente(uEJB.retornaUsuarioDaSessao());
            RequestContext.getCurrentInstance().execute("PF('dlgSolicitarOPME').show();");
        }

    }

    public void enviarEmail() throws EmailException {
        try {
            Associacao a = assEJB.carregarAssociacao();
            String destinatario = "";
            if (email.getIdUsuarioDestinatario() == null) {
                destinatario = email.getDestinatario();
            } else {
                destinatario = email.getIdUsuarioDestinatario().getUsuario();
            }

            BCRUtils.enviarEmailHtml(a.getSmtpServer(), a.getPortaSmtp(), a.getEmailPadrao(), a.getSenhaEmailPadrao(),
                    email.getAssunto(), "", email.getMensagem(),
                    email.getIdUsuarioEmitente().getNomeCompleto(), email.getIdUsuarioEmitente().getEmail(), destinatario, email.getDestinatario(), email.getCc());

            eEJB.Salvar(email);
            orcamento.setDtEnvioEmail(email.getDtEnvio());
            oEJB.atualizarOrcamento(orcamento);
            Mensagem.addMensagem(1, "Email enviado para " + destinatario + " com sucesso.");
            novo();
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao enviar email.");
        }

        //BCRUtils.enviarEmail("smtp.gmail.com", "charlesfg7421@gmail.com", "@@@charles0109", "charles7421@hotmail.com", "Teste de Email SMA", "");        
    }

    public void atualizarOrcamento(Orcamento o, Integer status) {
        oEJB.atualizarOrcamento(o);
    }

    public void excluirAdiantamento(AdiantamentosHistorico adiantamentosHistorico) {
        try {
            int indice = 0;
            List<AdiantamentosHistorico> listaTemporaria = new ArrayList<AdiantamentosHistorico>();
            listaTemporaria = listaAdiantamentos;
            for (AdiantamentosHistorico ah : listaTemporaria) {
                if (ah == adiantamentosHistorico) {
                    listaAdiantamentos.remove(indice);
                    try {
                        ahEJB.Excluir(adiantamentosHistorico, us.retornaUsuario());

                        // Colocar o status do Orçamento em Lançado.
                        orcamento.setIdStatus(stEJB.selecionarPorTipo(1));
                        oEJB.atualizarOrcamento(orcamento);
                        Mensagem.addMensagemPadraoSucesso("excluir");
                    } catch (Exception e) {
                        Mensagem.addMensagemPadraoErro("excluir");
                    }
                    break;
                } else {
                    indice++;
                }
            }
            zeraVariaveisAdiantamento();
            calcularValoresAdiantamento();
            if (valorRestante != 0) {
                valorAdiantamento = valorRestante;
            }
        } catch (Exception e) {
            Mensagem.addMensagem(1, "Erro ao remover");
        }
    }

    public void Excluir(Orcamento orcamento) {
        try {
            oEJB.ExcluirOrcamento(orcamento);
            listaOrcamentoAPesquisar.remove(orcamento);
            Mensagem.addMensagemPadraoSucesso("excluir");
            BCRUtils.ResetarDatatableFiltros("frmConsultar:tblPesq");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar excluir o orcamento!" + e);
        }
    }

//    @PostPersist
//    @PostUpdate
//    public void setaID(Orcamento orcamento) {
//        this.orcamento.setIdOrcamento(orcamento.getIdOrcamento());
//    }
    public void mudaStatusInserindo() {
        novo();
        if (orcamento.getIdOrcamento() == null) {
            orcamento.setIdStatus(stEJB.selecionarPorTipo(1));
            orcamento.setIdConvenio(cEJB.selecionarConvenioPadrao());
            orcamento.setDtPrevisao(BCRUtils.addDia(new Date(), 15));
            orcamento.setDtValidade(BCRUtils.addDia(new Date(), 30));
        }
        setStatus("Inserindo");
    }

    public void mudaStatusConsultando() {
        //novo();
        setStatus("Consultando");
    }
    //Consultas //

    public Integer getIdOrcamentoPesquisa() {
        return idOrcamentoPesquisa;
    }

    public void setIdOrcamentoPesquisa(Integer idOrcamentoPesquisa) {
        this.idOrcamentoPesquisa = idOrcamentoPesquisa;
    }

    public void setDtInicialPesquisa(Date dtInicialPesquisa) {
        this.dtInicialPesquisa = dtInicialPesquisa;
    }

    public void setDtFinalPesquisa(Date dtFinalPesquisa) {
        this.dtFinalPesquisa = dtFinalPesquisa;
    }

    public void setIdPacientePesquisa(Integer idPacientePesquisa) {
        this.idPacientePesquisa = idPacientePesquisa;
    }

    public void setSituacaoPesquisa(String situacaoPesquisa) {
        this.situacaoPesquisa = situacaoPesquisa;
    }

    public void setStatusPesquisa(String statusPesquisa) {
        this.statusPesquisa = statusPesquisa;
    }

    public Date getDtInicialPesquisa() {
        return dtInicialPesquisa;
    }

    public Date getDtFinalPesquisa() {
        return dtFinalPesquisa;
    }

    public Integer getIdPacientePesquisa() {
        return idPacientePesquisa;
    }

    public String getSituacaoPesquisa() {
        return situacaoPesquisa;
    }

    public String getStatusPesquisa() {
        return statusPesquisa;
    }

    public Integer getIdTerceiroPesquisa() {
        return idTerceiroPesquisa;
    }

    public void setIdTerceiroPesquisa(Integer idTerceiroPesquisa) {
        this.idTerceiroPesquisa = idTerceiroPesquisa;
    }

    public Integer getIdProcedimentoPesquisa() {
        return idProcedimentoPesquisa;
    }

    public void setIdProcedimentoPesquisa(Integer idProcedimentoPesquisa) {
        this.idProcedimentoPesquisa = idProcedimentoPesquisa;
    }

    public Integer getIdStatusPesquisa() {
        return idStatusPesquisa;
    }

    public void setIdStatusPesquisa(Integer idStatusPesquisa) {
        this.idStatusPesquisa = idStatusPesquisa;
    }

    public String getStatusAdiantamento() {
        return statusAdiantamento;
    }

    public void setStatusAdiantamento(String statusAdiantamento) {
        this.statusAdiantamento = statusAdiantamento;
    }
    private List<Orcamento> listaOrcamentoAPesquisar;

    public List<Orcamento> getListaOrcamentoAPesquisar() {
        return listaOrcamentoAPesquisar;
    }

    public void mudarStatus(Orcamento o) {
        System.out.println(o.getIdStatus().getDescricao());
    }

    public String limitarCaracteres(String stringLimitada) {
        String retorno = "";
        if (stringLimitada.length() <= 100) {
            retorno = stringLimitada;
        } else {
            String novaString = stringLimitada.substring(0, 100);
            novaString += "...";
            retorno = novaString;
        }
        return retorno;

    }

    public List<Terceiro> listaDeTerceiros() {
        return tEJB.listarTerceiro();
    }

    public List<Procedimento> listaDeProcedimentos() {
        return proEJB.listarProcedimentos();
    }

    public void pesquisarOrcamentos() {        
        // verificarRegistrosDuplicados();
        // List<Orcamento> listaOrcamentoAVerificar = new ArrayList<>();
        listaOrcamentoAPesquisar = new ArrayList<>();
        listaOrcamentoAPesquisar = oEJB.pesquisarOrcamentos(getDtInicialPesquisa(), getDtFinalPesquisa(), getIdPacientePesquisa(), getSituacaoPesquisa(), getIdOrcamentoPesquisa(), procedimento == null ? null : procedimento.getIdProcedimento(), getIdTerceiroPesquisa(), getIdStatusPesquisa(), getStatusAdiantamento(), getParamSemFechamentos(), "Orçamento");
        if (listaOrcamentoAPesquisar.isEmpty()) {
            Mensagem.addMensagem(3, "Não foi encontrado nenhum registro!");
        }
        BCRUtils.ResetarDatatableFiltros("frmConsultar:tblPesq");
    }

    public List<Orcamento> listarOrcamentoss() {
        return oEJB.listaOrcamentos();
    }

    public void cancelarOrcamento(Orcamento orcamento) {
        try {
            orcamento.setSituacao("Cancelado");
            orcamento.setIdMotivo(motivo);
            orcamento.setIdStatus(stEJB.selecionarPorTipo(5));
            orcamento.setDtCancelamento(new Date());
            orcamento.setIdUsuarioCancelamento(uEJB.retornaUsuarioDaSessao());
            oEJB.cancelar(orcamento);
            Mensagem.addMensagem(1, "Orçamento cancelado com sucesso!");
            listaOrcamentoAPesquisar = oEJB.pesquisarOrcamentos(dtInicialPesquisa, dtFinalPesquisa, idPacientePesquisa, situacaoPesquisa, idOrcamentoPesquisa, getIdProcedimentoPesquisa(), getIdTerceiroPesquisa(), getIdStatusPesquisa(), getStatusAdiantamento(), getParamSemFechamentos(), "Orçamento");
            aEJB.Salvar(BCRUtils.criarAuditoria("cancelamento", "Orçamento ID: " + orcamento.getIdOrcamento(), us.retornaUsuario(), new Date(), "Orçamento"));
            BCRUtils.ResetarDatatableFiltros("frmConsultar:tblPesq");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar cancelar o orçamento!");
        }

    }

    public List<Motivo> listarMotivos() {
        return mEJB.listarMotivos();
    }

    public void selecionarMotivo(Motivo motivo) {
        orcamento.setSubMotivo(motivo.getDescricao());
    }

    public String verificaEditarDesabilitado(Orcamento orcamento) {
        if (orcamento.getStatus() == "Liberado" || orcamento.getSituacao() == "Cancelado" || orcamento.getSituacao() == "Realizado") {
            System.out.println("veio true");
            return "true";
        } else {
            System.out.println("veio false");
            return "false";
        }

    }

    public void imprimirOrcamento(Integer idOrcto, String tipo) {
        Salvar();
        calcularTotalOrcamentoSalvar(idOrcto);
        switch (tipo) {
            case "A":
                RelatorioFactory.RelatorioOrcamento("/WEB-INF/Relatorios/reportOrcamento.jasper", idOrcto);
                break;
            case "O":
                RelatorioFactory.RelatorioOrcamento("/WEB-INF/Relatorios/reportOrcamentoAnaliticoObservacao.jasper", idOrcto);
                break;
            default:
                RelatorioFactory.RelatorioOrcamentoPorTipo("/WEB-INF/Relatorios/reportOrcamentoSintetico.jasper", idOrcto);
        }
    }

    public void imprimirListagemOrcamento() {
        if (situacaoPesquisa == null) {
            situacaoPesquisa = "%";
        }

        if (paramProcedimento.equals(0) || paramProcedimento == 0) {
            paramProcedimento = null;
        }

        if (paramTerceiro.equals(0) || paramTerceiro == 0) {
            paramTerceiro = null;
        }

        if (paramTipoData.equals("Hora")) {
            final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
            System.out.println(df.format(horarioInicialPesquisa));
            System.out.println(df.format(horarioFinalPesquisa));

            RelatorioFactory.RelatorioListagemOrcamentoHora("/WEB-INF/Relatorios/reportListagemOrcamentos.jasper", "C://SMA//logo.png", situacaoPesquisa, dtInicialPesquisa, dtFinalPesquisa, idStatusPesquisa, paramProcedimento, paramTerceiro, idPacientePesquisa, paramAdiantamento, paramSemFechamentos, paramTipoData, df.format(horarioInicialPesquisa), df.format(horarioFinalPesquisa));
        } else if (paramAdiantamento == null) {
            if (paramSemFechamentos == "" || paramSemFechamentos == null) {
                RelatorioFactory.RelatorioListagemOrcamento("/WEB-INF/Relatorios/reportListagemOrcamentos.jasper", "C://SMA//logo.png", situacaoPesquisa, dtInicialPesquisa, dtFinalPesquisa, idStatusPesquisa, paramProcedimento, paramTerceiro, idPacientePesquisa, paramAdiantamento, paramSemFechamentos, paramTipoData);
            } else {
                List<Orcamento> listaOrcamentos = new ArrayList<>();
                List<String> listaIdOrcamentos = new ArrayList<>();
                listaOrcamentos = oEJB.pesquisarOrcamentos(dtInicialPesquisa, dtFinalPesquisa, idPacientePesquisa, null, null, paramProcedimento, paramTerceiro, idStatusPesquisa, paramAdiantamento, paramSemFechamentos, paramTipoData);
                for (Orcamento orc : listaOrcamentos) {
                    //System.out.println("ID: " + orc.getIdOrcamento());
                    listaIdOrcamentos.add(orc.getIdOrcamento().toString());
                }
                if (listaIdOrcamentos.isEmpty()) {
                    Mensagem.addMensagem(3, "Não foram encontrados registros.");
                } else {
                    RelatorioFactory.RelatorioListagemOrcamentoLista("/WEB-INF/Relatorios/reportListagemOrcamentos2.jasper", "C://SMA//logo.png", situacaoPesquisa, dtInicialPesquisa, dtFinalPesquisa, idStatusPesquisa, paramProcedimento, paramTerceiro, idPacientePesquisa, paramAdiantamento, paramSemFechamentos, listaIdOrcamentos);
                }
            }
        } else {
            RelatorioFactory.RelatorioListagemOrcamento("/WEB-INF/Relatorios/reportListagemOrcamentosAdiantamento.jasper", "C://SMA//logo.png", situacaoPesquisa, dtInicialPesquisa, dtFinalPesquisa, idStatusPesquisa, paramProcedimento, paramTerceiro, idPacientePesquisa, paramAdiantamento, paramSemFechamentos, paramTipoData);
        }
    }

    public void imprimirListagemOrcamentoEnvioRecebimento() {
        if (situacaoPesquisa == null) {
            situacaoPesquisa = "%";
        }

        if (paramProcedimento.equals(0) || paramProcedimento == 0) {
            paramProcedimento = null;
        }

        if (paramTerceiro.equals(0) || paramTerceiro == 0) {
            paramTerceiro = null;
        }

        if (paramSemFechamentos == "" || paramSemFechamentos == null) {
            RelatorioFactory.RelatorioListagemOrcamento("/WEB-INF/Relatorios/reportOrcamentosEnvioRecebimento.jasper", "C://SMA//logo.png", situacaoPesquisa, dtInicialPesquisa, dtFinalPesquisa, idStatusPesquisa, paramProcedimento, paramTerceiro, idPacientePesquisa, paramAdiantamento, paramSemFechamentos, paramTipoData);
        } else {
            List<Orcamento> listaOrcamentos = new ArrayList<>();
            List<String> listaIdOrcamentos = new ArrayList<>();
            listaOrcamentos = oEJB.pesquisarOrcamentos(dtInicialPesquisa, dtFinalPesquisa, idPacientePesquisa, null, null, paramProcedimento, paramTerceiro, idStatusPesquisa, paramAdiantamento, paramSemFechamentos, "Orçamento");
            for (Orcamento orc : listaOrcamentos) {
                //System.out.println("ID: " + orc.getIdOrcamento());
                listaIdOrcamentos.add(orc.getIdOrcamento().toString());
            }
            if (listaIdOrcamentos.isEmpty()) {
                Mensagem.addMensagem(3, "Não foram encontrados registros.");
            } else {
                RelatorioFactory.RelatorioListagemOrcamentoLista("/WEB-INF/Relatorios/reportListagemOrcamentos2.jasper", "C://SMA//logo.png", situacaoPesquisa, dtInicialPesquisa, dtFinalPesquisa, idStatusPesquisa, paramProcedimento, paramTerceiro, idPacientePesquisa, paramAdiantamento, paramSemFechamentos, listaIdOrcamentos);
            }
        }
    }

    public void imprimirListagemOrcamentoPorAgrupamento() {
        if (paramProcedimento.equals(0) || paramProcedimento == 0) {
            paramProcedimento = null;
        }

        if (paramTerceiro.equals(0) || paramTerceiro == 0) {
            paramTerceiro = null;
        }
        RelatorioFactory.RelatorioListagemOrcamentoPorAgrupamento("/WEB-INF/Relatorios/reportOrcamentosAgrupadosMedicos.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, idStatusPesquisa, paramProcedimento, paramTerceiro, paramTipoData);

    }

    public void imprimirListagemOrcamentoPorPaciente() {
        if (paramProcedimento.equals(0) || paramProcedimento == 0) {
            paramProcedimento = null;
        }

        if (paramTerceiro.equals(0) || paramTerceiro == 0) {
            paramTerceiro = null;
        }

        RelatorioFactory.RelatorioListagemOrcamentoPorAgrupamento("/WEB-INF/Relatorios/reportOrcamentosAgrupadosPaciente.jasper", "C://SMA//logo.png", dtInicialPesquisa, dtFinalPesquisa, idStatusPesquisa, paramProcedimento, paramTerceiro, paramTipoData);

    }

    public void imprimirReciboAdiantamento(Integer idOrcamento) {
        Double valorTotalAdiantamento = 0D;
        for (AdiantamentosHistorico ah : listaAdiantamentos) {
            valorTotalAdiantamento += ah.getValor();
        }
        Extenso e = new Extenso();
        String valorExtenso = e.write(new BigDecimal(valorTotalAdiantamento));
        RelatorioFactory.RelatorioReciboAdiantamento("/WEB-INF/Relatorios/reportReciboAdiantamento.jasper", "C://SMA//logo.png", idOrcamento, valorExtenso);
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Double getValorAdiantamento() {
        return valorAdiantamento;
    }

    public void setValorAdiantamento(Double valorAdiantamento) {
        this.valorAdiantamento = valorAdiantamento;
    }

    public List<AdiantamentosHistorico> getListaAdiantamentos() {
        return listaAdiantamentos;
    }

    public void setListaAdiantamentos(List<AdiantamentosHistorico> listaAdiantamentos) {
        this.listaAdiantamentos = listaAdiantamentos;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public Double getValorRestante() {
        return valorRestante;
    }

    public void setValorRestante(Double valorRestante) {
        this.valorRestante = valorRestante;
    }

    public Integer getParamProcedimento() {
        return paramProcedimento;
    }

    public void setParamProcedimento(Integer paramProcedimento) {
        this.paramProcedimento = paramProcedimento;
    }

    public Integer getParamTerceiro() {
        return paramTerceiro;
    }

    public void setParamTerceiro(Integer paramTerceiro) {
        this.paramTerceiro = paramTerceiro;
    }

    public String getParamAdiantamento() {
        return paramAdiantamento;
    }

    public void setParamAdiantamento(String paramAdiantamento) {
        this.paramAdiantamento = paramAdiantamento;
    }

    public String getNomePacientePesquisa() {
        return nomePacientePesquisa;
    }

    public void setNomePacientePesquisa(String nomePacientePesquisa) {
        this.nomePacientePesquisa = nomePacientePesquisa;
    }

    public String getParamSemFechamentos() {
        return paramSemFechamentos;
    }

    public void setParamSemFechamentos(String paramSemFechamentos) {
        this.paramSemFechamentos = paramSemFechamentos;
    }

    public Orcamento getOrcamentoDuplicado() {
        return orcamentoDuplicado;
    }

    public void setOrcamentoDuplicado(Orcamento orcamentoDuplicado) {
        this.orcamentoDuplicado = orcamentoDuplicado;
    }

    public OrcamentoItem getItemDuplicado() {
        return itemDuplicado;
    }

    public void setItemDuplicado(OrcamentoItem itemDuplicado) {
        this.itemDuplicado = itemDuplicado;
    }

    public Emails getEmail() {
        return email;
    }

    public void setEmail(Emails email) {
        this.email = email;
    }

    public String getTextoModelo() {
        return textoModelo;
    }

    public void setTextoModelo(String textoModelo) {
        this.textoModelo = textoModelo;
    }

    public boolean isRecebimentoEmail() {
        return recebimentoEmail;
    }

    public void setRecebimentoEmail(boolean recebimentoEmail) {
        this.recebimentoEmail = recebimentoEmail;
    }

    public String getParamTipoData() {
        return paramTipoData;
    }

    public void setParamTipoData(String paramTipoData) {
        this.paramTipoData = paramTipoData;
    }

    public Date getHorarioInicialPesquisa() {
        return horarioInicialPesquisa;
    }

    public void setHorarioInicialPesquisa(Date horarioInicialPesquisa) {
        this.horarioInicialPesquisa = horarioInicialPesquisa;
    }

    public Date getHorarioFinalPesquisa() {
        return horarioFinalPesquisa;
    }

    public void setHorarioFinalPesquisa(Date horarioFinalPesquisa) {
        this.horarioFinalPesquisa = horarioFinalPesquisa;
    }

    public String getPagadorAdiantamento() {
        return pagadorAdiantamento;
    }

    public void setPagadorAdiantamento(String pagadorAdiantamento) {
        this.pagadorAdiantamento = pagadorAdiantamento;
    }

    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public LazyDataModel<Procedimento> getModelProcedimento() {
        return modelProcedimento;
    }

    public void setModelProcedimento(LazyDataModel<Procedimento> modelProcedimento) {
        this.modelProcedimento = modelProcedimento;
    }

}
