package br.com.ux.view.integracao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import br.com.ux.controller.AreaProcedimentoEJB;
import br.com.ux.controller.AtendimentoPacienteEJB;
import br.com.ux.controller.CBOSaudeEJB;
import br.com.ux.controller.ContaPacienteEJB;
import br.com.ux.controller.EspecialidadeProcedimentoEJB;
import br.com.ux.controller.GrupoProcedimentoEJB;
import br.com.ux.controller.LogIntegracaoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.ProcedimentoPacienteEJB;
import br.com.ux.controller.ServicoEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.integracao.BuscaDadosMySQLEJB;
import br.com.ux.controller.integracao.BuscaDadosTasyEJB;
import br.com.ux.controller.integracao.SincronizacaoEJB;
import br.com.ux.controller.integracao.TimerEJB;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.tasy.ContaPaciente;
import br.com.ux.util.BCRUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class IntegracaoMB implements Serializable {

    @EJB
    BuscaDadosTasyEJB bdTEJB;
    @EJB
    BuscaDadosMySQLEJB bdMEJB;
    @EJB
    SincronizacaoEJB syncEJB;

    @EJB
    LogIntegracaoEJB logEJB;

    @EJB
    PacienteEJB pEJB;
    @EJB
    TerceiroEJB tEJB;
    @EJB
    CBOSaudeEJB csEJB;
    @EJB
    AreaProcedimentoEJB apEJB;
    @EJB
    EspecialidadeProcedimentoEJB epEJB;
    @EJB
    GrupoProcedimentoEJB gpEJB;
    @EJB
    ServicoEJB sEJB;
    @EJB
    AtendimentoPacienteEJB appEJB;
    @EJB
    ContaPacienteEJB cpEJB;
    @EJB
    ProcedimentoPacienteEJB ppEJB;
    @EJB
    TimerEJB tmEJB;

    public IntegracaoMB() {

    }

    // Vinculação dos pacientes do TASY com o SMA.
    // Se o paciente não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.    
    public void vincularPacientes() throws InterruptedException {
        //tmEJB.doAction();
    }

    // Sincronização de dados dos médicos do TASY com o SMA.
    // Se o médico não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    @Asynchronous
    public void vincularMedicos() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Vinculação dos médicos iniciada.", "vincularMedicos", dtInicio, null, null);
        logEJB.Salvar(logInicio);

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaCadastroMedicoTasy();

        // Importar informações básicas para cadastro ou vincular médico        
        importarCadastrosBasicos(lista, 12, dtInicio, logInicio);

        // Compara, vincula e persiste as novas informações no SMA.
        tEJB.VincularListaMedicosTasy(lista, logInicio);

        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Vinculação dos médicos realizada com sucesso.", "vincularMedicos", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }

    public void importarCadastrosBasicos(List<Object[]> lista, Integer posicaoArrayVerificador, Date dtInicio, LogIntegracao logPai) {
        // Verifica se na lista de médicos há algum cbo a importar sob demanda.
        for (Object[] c : lista) {
            // CBO Saude
            if (c[posicaoArrayVerificador] != null) {
                if (csEJB.selecionarPorIDCboSaudeTasy(((BigDecimal) c[posicaoArrayVerificador]).intValue()) == null) {
                    csEJB.cadastrarCboSaude(bdTEJB.retornaCadastroCBOSaudeTasyPorID(((BigDecimal) c[posicaoArrayVerificador]).intValue()), logPai);
                }
            }
        }
    }

    // Sincronização de dados do CBO_SAUDE do TASY com o SMA.
    // Se o cbo não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    // É necessário executar somente uma vez.
    @Asynchronous
    public void vincularOuImportarCBOSaude() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Vinculação do CBO Saúde iniciada.", "vincularOuImportarCBOSaude", dtInicio, null, null);
        logEJB.Salvar(logInicio);

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaCadastroCBOSaudeTasy();
        // Compara, vincula e persiste as novas informações no SMA.
        csEJB.VincularListaCboSaudeTasy(lista, logInicio);

        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Vinculação do CBO Saúde realizada com sucesso.", "vincularOuImportarCBOSaude", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }

    // Sincronização de dados do Area Procedimento do TASY com o SMA.
    // Se a area de procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    // É necessário executar somente uma vez.
    @Asynchronous
    public void vincularOuImportarAreaProcedimento() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Vinculação do Area de Procedimentos iniciada.", "vincularOuImportarAreaProcedimento", dtInicio, null, null);
        logEJB.Salvar(logInicio);

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaCadastroAreaProcedimentoTasy();
        // Compara, vincula e persiste as novas informações no SMA.
        apEJB.VincularListaAreaProcedimentoTasy(lista, logInicio);

        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Vinculação do Area de Procedimentos realizada com sucesso.", "vincularOuImportarAreaProcedimento", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }

    // Sincronização de dados do Especialidade Procedimento do TASY com o SMA.
    // Se a especialidade procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    // É necessário executar somente uma vez.
    @Asynchronous
    public void vincularOuImportarEspecialidadeProcedimento() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Vinculação do Especialidade Procedimentos iniciada.", "vincularOuImportarEspecialidadeProcedimento", dtInicio, null, null);
        logEJB.Salvar(logInicio);

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaCadastroEspecialidadeProcedimentoTasy();
        // Compara, vincula e persiste as novas informações no SMA.     

        epEJB.VincularListaEspecialidadeProcedimentoTasy(lista, logInicio);

        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Vinculação do Especialidade Procedimentos realizada com sucesso.", "vincularOuImportarEspecialidadeProcedimento", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }

    // Sincronização de dados do Especialidade Procedimento do TASY com o SMA.
    // Se a especialidade procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    // É necessário executar somente uma vez.
    @Asynchronous
    public void vincularOuImportarGrupoProcedimento() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Vinculação do Grupo de Procedimentos iniciada.", "vincularOuImportarGrupoProcedimento", dtInicio, null, null);
        logEJB.Salvar(logInicio);

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaCadastroGrupoProcedimentoTasy();
        // Compara, vincula e persiste as novas informações no SMA.     

        gpEJB.VincularListaGrupoProcedimentoTasy(lista, logInicio);

        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Vinculação do Grupo de Procedimentos realizada com sucesso.", "vincularOuImportarGrupoProcedimento", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }

    // Sincronização de dados de Procedimento do TASY com o SMA.
    // Se o procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    // É necessário executar somente uma vez.
    @Asynchronous
    public void vincularOuImportarProcedimentosCBHPM() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Vinculação dos Procedimentos iniciada.", "vincularOuImportarProcedimentosCBHPM", dtInicio, null, null);
        logEJB.Salvar(logInicio);

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaCadastroProcedimentosCBHPMTasy();

        // Compara, vincula e persiste as novas informações no SMA.             
        sEJB.VincularListaServicosTasy(lista, logInicio);

        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Vinculação dos Procedimentos realizada com sucesso.", "vincularOuImportarProcedimentosCBHPM", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }

    public void importarTabelaCBHPM() {
        System.out.println("Importando area de Procedimento ...");
        vincularOuImportarAreaProcedimento();
        System.out.println("Importando especialidade de Procedimento ...");
        vincularOuImportarEspecialidadeProcedimento();
        System.out.println("Importando grupo de Procedimento ...");
        vincularOuImportarGrupoProcedimento();
        System.out.println("Importando procedimentos ...");
        vincularOuImportarProcedimentosCBHPM();
    }

    // Sincronização de dados dos atendimento do TASY com o SMA.
    // Se o procedimento não tiver cadastro no SMA, realiza o cadastro e vincula aos dados do TASY.
    @Asynchronous
    public void vincularOuImportarAtendimentoPaciente() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Importação dos Atendimentos pacientes iniciada.", "vincularOuImportarAtendimentoPaciente", dtInicio, null, null);
        logEJB.Salvar(logInicio);

        // Busca as informações do banco de dados do TASY
        List<Object[]> lista = bdTEJB.listaAtendimentosPacientesTasy();

        // Compara, vincula e persiste as novas informações no SMA.             
        appEJB.VincularListaAtendimentoPacienteTasy(lista, logInicio);

        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Vinculação dos Atendimentos pacientes realizada com sucesso.", "vincularOuImportarAtendimentoPaciente", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }

    // Sincronização de dados da Conta Paciente do TASY com o SMA.
    // Se a conta paciente não existir no SMA, realiza o cadastro e vincula aos dados do TASY.        
    public void vincularOuImportarContaPaciente() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Importação das Contas Paciente iniciada.", "vincularOuImportarContaPaciente", dtInicio, null, null);
        logEJB.Salvar(logInicio);
        
        for (Object[] c  : bdTEJB.listaAtendimentosPacientesTasy()) {
            // Busca as informações do banco de dados do TASY
            List<Object[]> lista = bdTEJB.listaContaPacientesTasyPorAtendimento(((BigDecimal) c[0]).intValue());
             // Compara, vincula e persiste as novas informações no SMA.
            cpEJB.vincularListaContaPacienteTasy(lista, logInicio);
        }
        
        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Importação das Contas Paciente finalizada.", "vincularOuImportarContaPaciente", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }

    // Sincronização de dados da Procedimento Paciente do TASY com o SMA.
    // Se a procedimento paciente não existir no SMA, realiza o cadastro e vincula aos dados do TASY.    
    public void vincularOuImportarProcedimentoPaciente() {
        Date dtInicio = new Date();
        LogIntegracao logInicio = BCRUtils.gerarLogIntegracao("Importação das Procedimento Paciente iniciada.", "vincularOuImportarContaPaciente", dtInicio, null, null);
        logEJB.Salvar(logInicio);

        for (ContaPaciente cp : cpEJB.listarContaPaciente()) {
            // Busca as informações do banco de dados do TASY
            List<Object[]> lista = bdTEJB.listaProcedimentosPacientesTasyPorContaPaciente(cp.getIdContaPacienteTasy());
            // Compara, vincula e persiste as novas informações no SMA.             
            ppEJB.VincularListaProcedimentoPacienteTasy(lista, logInicio);
        }

        // Gerar e salvar log de integração.
        Date dtFim = new Date();
        LogIntegracao logFim = BCRUtils.gerarLogIntegracao("Importação das Procedimento Paciente finalizada.", "vincularOuImportarContaPaciente", dtInicio, dtFim, logInicio);
        logEJB.Salvar(logFim);
    }
    
    public void importarCBHPM() {
        // Por serem menores, pode buscar todos os cadastros
        List<Object[]> listaAreaProcedimentos = bdTEJB.listaCadastroAreaProcedimentoTasy();
        List<Object[]> listaEspecialidades = bdTEJB.listaCadastroEspecialidadeProcedimentoTasy();
        List<Object[]> listaGruposProcedimentos = bdTEJB.listaCadastroGrupoProcedimentoTasy();

        // Por serem maiores, busca somente da ultima sincronização em diante.
        // Serviços/Procedimentos
        List<Object[]> listaProcedimentos;
        listaProcedimentos = bdTEJB.listaCadastroProcedimentosCBHPMTasy();
        syncEJB.importarTabelaCBHPM(listaAreaProcedimentos, listaEspecialidades, listaGruposProcedimentos, listaProcedimentos, new LogIntegracao());
        System.out.println("Sicronização finalizada ...");
    }

}
