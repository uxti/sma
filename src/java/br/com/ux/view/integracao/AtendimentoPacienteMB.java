/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view.integracao;

import br.com.ux.controller.AtendimentoPacienteEJB;
import br.com.ux.controller.OrcamentoEJB;
import br.com.ux.controller.PacienteEJB;
import br.com.ux.controller.integracao.SincronizacaoEJB;
import br.com.ux.model.Orcamento;
import br.com.ux.model.Paciente;
import br.com.ux.model.tasy.AtendimentoPaciente;
import br.com.ux.util.Mensagem;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class AtendimentoPacienteMB implements Serializable{

    @EJB
    AtendimentoPacienteEJB atEJB;
    @EJB
    OrcamentoEJB oEJB;
    @EJB
    SincronizacaoEJB sEJB;

    public AtendimentoPacienteMB() {
    }

    public List<AtendimentoPaciente> listaAtendimentoPacientePorPaciente(Paciente pac) {
        if (pac != null) {
            if (pac.getIdPacienteTasy() == null) {
                if (sEJB.cadastrarPacienteTasySobDemandaPorNome(pac.getNome(), pac.getCpf().replace(".", "").replace("-", ""))) {
                    return atEJB.listarAtendimentoPacientePorPaciente(pac.getIdPaciente());
                } else {
                    Mensagem.addMensagem(3, "Este paciente não está vinculado a um paciente TASY.");
                    return null;
                }              
            } else {
                return atEJB.listarAtendimentoPacientePorPaciente(pac.getIdPaciente());
            }             
        } else {
            Mensagem.addMensagem(3, "Este paciente não está cadastrado.");
            return null;
        }          
    }

    public void vincularOrcamentoAoAtendimentoPaciente(Orcamento o, AtendimentoPaciente ap) {
        try {
            o.setIdAtendimentoPaciente(ap);
            oEJB.atualizarOrcamento(o);
            Mensagem.addMensagem(1, "Orçamento vinculado com sucesso.");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(1, "Erro ao vincular orçamento.");
        }
    }
    
    public List<AtendimentoPaciente> listarAtendimentosPacientesSemOrcamento() {
        return atEJB.listarAtendimentoPacienteSemVinculo();
    }
    
}
