/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view.integracao;

import br.com.ux.controller.AssociacaoEJB;
import br.com.ux.controller.AtendimentoPacienteEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.CirurgiaEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.ContaPacienteEJB;
import br.com.ux.controller.ProcedimentoPacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.controller.integracao.BuscaDadosTasyEJB;
import br.com.ux.model.Associacao;
import br.com.ux.model.Banco;
import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.RegraRepasseMedicos;
import br.com.ux.model.Repasse;
import br.com.ux.model.Servico;
import br.com.ux.model.TipoInternacao;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class AtendimentoExternoMB implements Serializable {

    @EJB
    CirurgiaEJB cEJB;
    private List<Cirurgia> listaFechamentos;
    private LazyDataModel<Cirurgia> listaAtendimentosExterno;

    @EJB
    AtendimentoPacienteEJB apEJB;
    private TipoInternacao tipoInternacao;

    @EJB
    UsuarioEJB uEJB;
    @EJB
    ChequeEJB chEJB;
    @EJB
    ContaCorrenteEJB ccEJB;
    @EJB
    CaixaEJB cxEJB;
    @EJB
    ContaPacienteEJB cpEJB;
    @EJB
    RepasseEJB rrEJB;

    @EJB
    AssociacaoEJB assEJB;

    @EJB
    BuscaDadosTasyEJB bdTEJB;

    @EJB
    ProcedimentoPacienteEJB ppEJB;

    public Cirurgia cirurgia;
    public Cheque cheque;

    private boolean validacaoCGC;
    private boolean CPFCPNJ;
    private String movimento;

    UsuarioSessao us = new UsuarioSessao();

    public AtendimentoExternoMB() {
    }

    public List<Cirurgia> retornaListaAtendimentoExternoSemPagamento() {
        return cEJB.retornaListaAtendimentoExternoSemPagamento();
    }

    @PostConstruct
    public void carregarListaAtendimentosExternos() {
        listaAtendimentosExterno = new LazyDataModel<Cirurgia>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Cirurgia> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuilder sf = new StringBuilder();
                int contar = 0;
                for (String filterProperty : filters.keySet()) {
                    // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p.").append(filterProperty).append(" like'").append(filterValue).append("%' AND p.externo = TRUE ");
                        } else {
                            sf.append(" where p.").append(filterProperty).append(" like'").append(filterValue).append("%' AND p.externo = TRUE");
                        }
                    } else {
                        sf.append(" and p.").append(filterProperty).append(" like'").append(filterValue).append("%' AND p.externo = TRUE");
                    }
                    contar++;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    if (sf.length() > 0) {
                        sf.append(" order by p.").append(sortField).append(" asc");
                    } else {
                        sf.append(" where p.externo = TRUE order by p.").append(sortField).append(" asc");
                    }
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    if (sf.length() > 0) {
                        sf.append(" order by p.").append(sortField).append(" desc");
                    } else {
                        sf.append(" where p.externo = TRUE order by p.").append(sortField).append(" desc");
                    }

                }
                Clausula = sf.toString();
                //System.out.println(Clausula);
                setRowCount(Integer.parseInt(cEJB.contarFechamentos(Clausula).toString()));
                listaFechamentos = cEJB.listarFechamentosLazyModeWhere(first, pageSize, Clausula);
                return listaFechamentos;
            }
        };
    }

    public void pagarEmDinheiro(Cirurgia c) {
        try {
            System.out.println("1");
            cirurgia = c;
            System.out.println("2");
            criarPagamentoEmDinheiro();
            System.out.println("3");
            lancarRepassesContaCorrente(cheque, cirurgia.getCirurgiaItemList(), "Dinheiro");
            System.out.println("4");
            Receber(cheque);
            System.out.println("5");
            cEJB.atualizarExternoComoPago(cirurgia.getIdCirurgia());
            System.out.println("6");
            Mensagem.addMensagem(1, "Pagamento salvo com sucesso.");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar salvar pagamento!" + e);
            System.out.println(e);
        }
    }

    public void pagarEmCheque(String mov, String cpf, String emitenteCheque, Banco banco, Date dtVencimento, String numeroCheque, String observacao, String conta, Double valor, Cirurgia c) {
        try {
            Associacao a = assEJB.carregarAssociacao();
            cirurgia = c;
            String tipoMovimento;
            tipoMovimento = "D".equals(mov) ? "Dinheiro" : mov.equals("C") ? "Cheque" : "N".equals(mov) ? "Nota Promissória" : "B".equals(mov) ? "Depósito Bancário" : "A".equals(mov) ? "Cartão - Débito" : "R".equals(mov) ? "Cartão - Crédito" : "";

            cheque = new Cheque();

            cheque.setIdPaciente(cirurgia.getIdPaciente());
            if (mov.equals("C")) {
                cheque.setEmitente(emitenteCheque);
                cheque.setCpf(cpf);
                cheque.setNumeroCheque(numeroCheque);
                cheque.setIdBanco(banco);
            }

            cheque.setMovimento(mov);
            cheque.setDtCompensacaoVencimento(dtVencimento);
            cheque.setDtRecebimentoEmissao(new Date());

            cheque.setObservacao(observacao);
            cheque.setConta(conta);
            cheque.setValor(valor);
            cheque.setNumeroParcela(1);
            cheque.setIdUsuario(uEJB.retornaUsuarioDaSessao());

            cheque.setIdCirurgia(cirurgia);
            cheque.setTipo("N");
            cheque.setDtCompensado(BCRUtils.addDia(new Date(), a.getPrazoCompensacaoCheque()));
            cheque.setSituacao("Recebido");
            cheque.setCompensado("S");

            chEJB.Salvar(cheque, us.retornaUsuario());

            // CHARLES 11072017
            // Nova funcionalidade: Se for pagamento por cartão (Débito e Crédito), NÃO LANÇAR OS REPASSES.
            if (!mov.equals("A") && !mov.equals("R")) {

                // CHARLES 11072017
                // Se for pagamento por deposito bancário, descontar uma taxa no repasse aos médicos.
//                if (mov.equals("B")) {
//                    List<CirurgiaItem> list = cirurgia.getCirurgiaItemList();
//                    Double taxaDesconto = a.getTaxaDescontoRepasseDeposito();
//                    for (CirurgiaItem ci : list) {
//                        Double valorDesconto = ((ci.getValorTotal() * taxaDesconto) / 100);
//                        ci.setValorTotal(ci.getValorTotal() - valorDesconto);
//                        ci.setDesconto(valorDesconto);
//                        ci.setValorTotalGeral(ci.getValorTotal() + ci.getValorTasy());
//                        cEJB.SalvarItem(ci);
//                    }
//                    cirurgia.setCirurgiaItemList(list);
//                    c.setValor(calcularTotalItensGeral(cirurgia.getCirurgiaItemList()));
//                }
                lancarRepassesContaCorrente(cheque, cirurgia.getCirurgiaItemList(), tipoMovimento);
                Receber(cheque);
            }

            cirurgia.setPago(true);
            cEJB.atualizarFechamento(cirurgia);
            Mensagem.addMensagem(1, "Pagamento salvo com sucesso.");
            RequestContext.getCurrentInstance().execute("PF('dlgLancarCheque').hide()");

        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar salvar pagamento!" + e);
            System.out.println(e);
        }
    }

    public void criarPagamentoEmDinheiro() {
        limparCampos();
        if (cirurgia.getValor() != null) {
            movimento = "D";
            cheque.setMovimento("D");
            cheque.setCompensado("S");
            cheque.setValor(cirurgia.getValor());
            cheque.setIdPaciente(cirurgia.getIdPaciente());
            cheque.setDtRecebimentoEmissao(new Date());
            cheque.setDtCompensacaoVencimento(new Date());
            cheque.setDtCompensado(new Date());
            if (cheque.getIdPaciente().getCpf() != null) {
                cheque.setCpf(cheque.getIdPaciente().getCpf());
            }
            cheque.setEmitente(cheque.getIdPaciente().getNome());
            validacaoCGC = true;
            cheque.setNumeroParcela(1);
            cheque.setIdUsuario(uEJB.retornaUsuarioPorUsername(us.retornaUsuario()));
            if (cirurgia.getIdTerceiro() != null) {
                if (cirurgia.getIdTerceiro().getTerceiroMedHosp()) {
                    cheque.setTipo("H");
                } else {
                    cheque.setTipo("N");
                }
            } else {
                cheque.setTipo("N");
            }
            cheque.setIdCirurgia(cirurgia);
            chEJB.Salvar(cheque, us.retornaUsuario());
        }
    }

    public void lancarRepassesContaCorrente(Cheque ch, List<CirurgiaItem> listaItens, String tipoPagamento) {
        try {
            for (CirurgiaItem ci : listaItens) {
                if (ci.getIdProcedimentoPaciente().getIdRegraRepasse().getRepasseSecundario()) {
                    // Lança o novo valor do repasse após o rateio
                    if (ci.getIdProcedimentoPaciente().getIdRegraRepasse().getRegraRepasseMedicosList().isEmpty()) {
                        Mensagem.addMensagem(3, "Verifique a regra de repasse nº ".concat(ci.getIdProcedimentoPaciente().getIdRegraRepasse().getIdRegraRepasse().toString()).concat(". Lista de médicos relacionados não encontrado."));
                    } else {
                        try {
                            for (RegraRepasseMedicos rrm : ci.getIdProcedimentoPaciente().getIdRegraRepasse().getRegraRepasseMedicosList()) {
                                Double valorRateioRepasse = BCRUtils.arredondar((ci.getValorTotalGeral() * rrm.getFatorRepasseSecundario()) / 100);
                                Repasse repasse = new Repasse();
                                repasse.setIdCirurgia(cirurgia);
                                repasse.setNumeroParcela(ch.getNumeroParcela());
                                repasse.setIdCheque(ch);
                                repasse.setIdPaciente(ch.getIdPaciente());
                                repasse.setValor(valorRateioRepasse);
                                repasse.setDtLancamento(cheque.getDtRecebimentoEmissao());
                                repasse.setDtRecebimento(cheque.getDtRecebimentoEmissao());
                                repasse.setDtVencimento(cheque.getDtCompensacaoVencimento());
                                repasse.setIdTerceiro(rrm.getIdTerceiro());
                                repasse.setTipoPagamento('P');
                                repasse.setPercentualRecebido(100D);
                                repasse.setSituacaoPaciente('P');
                                repasse.setStatus('F');
                                repasse.setValor(0D);
                                repasse.setValorRecebido(valorRateioRepasse);
                                repasse.setValorDocumento(valorRateioRepasse);
                                repasse.setIdCirurgiaItem(ci);
                                repasse.setFormaPagamento(tipoPagamento);
                                repasse.setSituacaoTerceiro('L');
                                lancarContaCorrente(repasse);
                            }
                        } catch (Exception e) {
                            Mensagem.addMensagem(3, "Verifique a regra de repasse nº ".concat(ci.getIdProcedimentoPaciente().getIdRegraRepasse().getIdRegraRepasse().toString()).concat(". Erro ao realizar os repasses relacionados."));
                            System.out.println(e);
                        }

                    }
                } else if (!ci.getValorTotalGeral().equals(0D)) {

                    // CHARLES - 23022018
                    // Só lançar os repasses se o valor for diferente de zero. :-/
                    Repasse repasse = new Repasse();
                    repasse.setIdCirurgia(cirurgia);
                    repasse.setNumeroParcela(ch.getNumeroParcela());
                    repasse.setIdCheque(ch);
                    repasse.setIdPaciente(ch.getIdPaciente());
                    repasse.setValor(ci.getValorTotal());
                    repasse.setDtLancamento(cheque.getDtRecebimentoEmissao());
                    repasse.setDtRecebimento(cheque.getDtRecebimentoEmissao());
                    repasse.setDtVencimento(cheque.getDtCompensacaoVencimento());
                    repasse.setIdTerceiro(ci.getIdTerceiro());
                    repasse.setTipoPagamento('P');
                    repasse.setPercentualRecebido(100D);
                    repasse.setSituacaoPaciente('P');
                    repasse.setStatus('F');
                    repasse.setValor(0D);
                    repasse.setValorRecebido(ci.getValorTotal());
                    repasse.setValorDocumento(ci.getValorTotal());
                    repasse.setIdCirurgiaItem(ci);
                    repasse.setFormaPagamento(tipoPagamento);
                    repasse.setSituacaoTerceiro('L');
                    lancarContaCorrente(repasse);
                }
                // Este salvar está duplicando os repasses.
                // rrEJB.SalvarSimples(repasse);                
            }
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void lancarContaCorrente(Repasse repasse) {
        try {
            ContaCorrente cc = new ContaCorrente();
            cc.setIdTerceiro(repasse.getIdTerceiro());
            cc.setIdRepasse(repasse);
            cc.setIdCirurgia(cirurgia);
            cc.setIdPaciente(repasse.getIdPaciente());
            cc.setIdCheque(repasse.getIdCheque().getIdCheque());
            cc.setTipo("C");
            cc.setDtLancamento(new Date());
            cc.setSituacao("Não Liberado");
            cc.setStatus("Não Retirado");
            cc.setValor(repasse.getValorDocumento());
            if (repasse.getIdCirurgiaItem().getIdProcedimentoPaciente().getIdRegraRepasse().getRepasseSecundario()) {
                cc.setObservacao("Rateio referente ao atendimento externo nº " + cc.getIdCirurgia().getIdCirurgia() + ", paciente " + repasse.getIdPaciente().getNome() + ".");
            } else {
                cc.setObservacao("Atendimento externo avulso nº " + cc.getIdCirurgia().getIdCirurgia() + " referente ao paciente " + repasse.getIdPaciente().getNome() + ".");
            }

            ccEJB.SalvarSimples(cc, null);
            //System.out.println(repasse.getIdCirurgiaItem().getIdCirurgiaItem());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void Receber(Cheque ch) {
        Usuario u = uEJB.retornaUsuarioPorUsername(us.retornaUsuario());
        try {
            Date d = new Date();
            Long numAleatorio = d.getTime();

            // Lançar pagamentos no Caixa
            Caixa caixa = new Caixa();
            caixa.setNumLote(numAleatorio.toString());
            caixa.setIdCheque(ch);
            caixa.setIdUsuario(u);
            caixa.setDtCaixa(ch.getDtRecebimentoEmissao());
            caixa.setTipoLancamento('C');
            caixa.setValor(ch.getValor());

            caixa.setStatus("Não Baixado");
            caixa.setIdCirurgia(cirurgia);
            caixa.setHistorico("Atendimento Externo nº " + cirurgia.getIdCirurgia() + ". Paciente: " + cirurgia.getIdPaciente().getNome());
            caixa.setNumeroParcela(ch.getNumeroParcela());
            cxEJB.Salvar(caixa);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void estornarPagamento(Cirurgia c) {
        try {
            for (ContaCorrente cc : c.getContaCorrenteList()) {
                ccEJB.Excluir(cc, null);
                rrEJB.Excluir(cc.getIdRepasse());
            }
            cxEJB.excluirCaixaPorFechamento(c.getIdCirurgia());
            chEJB.excluirChequesPorFechamento(c.getIdCirurgia(), us.retornaUsuario());

            c.setPago(Boolean.FALSE);
            // Zerar as listas vinculadas.
            c.setRepasseList(null);
            c.setContaCorrenteList(null);
            c.setChequeList(null);

            cEJB.atualizarFechamento(c);
            Mensagem.addMensagem(1, "Estorno realizado com sucesso");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao estornar.");
        }
    }

    

    public boolean verificaSePagamentosJaForamTransferidos(Cirurgia c) {
        return cxEJB.retornaSePagamentoFoiTransferido(c);
    }

    public void calcularTotalItemIndex(Cirurgia c, List<CirurgiaItem> lista, CirurgiaItem ci, int index) {
        try {
            Double valorTotalGeral;
            valorTotalGeral = ci.getValorTotal() + ci.getValorTasy();
            valorTotalGeral = valorTotalGeral - ci.getDesconto();
            Double valorAjustado = cpEJB.retornaValorAjustadoPorRegraRepasseTaxa(ci.getIdServico(), ci.getIdTerceiro(), valorTotalGeral, ci.getIdCirurgia().getIdContaPaciente().getIdAtendimentoPaciente().getCategoria(), ci.getIdCirurgia().getIdContaPaciente().getIdAtendimentoPaciente().getUrgencia() == null ? null : ci.getIdCirurgia().getIdContaPaciente().getIdAtendimentoPaciente().getUrgencia());

            ci.setValorTotal(valorAjustado);                      // Valor SMA = novo total geral * taxa de repasse
            ci.setValorTotalGeral(valorTotalGeral);               // Valor Total Geral = novo total geral já com desconto
            ci.setValorUnitario(valorTotalGeral);                 // Valor Total Geral = Valor Unitário
            ci.setValorTasy(valorTotalGeral - valorAjustado);     // Valor TASY  = novo total geral - Valor SMA    

            lista.remove(index);
            lista.add(index, ci);
            c.setValor(calcularTotalItensGeral(lista));
            cEJB.AtualizarExterno(c, lista);
            atualizarValorTasy(ci);
            Mensagem.addMensagem(1, "Valores recalculados e atualizados com sucesso.");
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao calcular o item.");
            System.out.println(e);
        }

    }

    private Double calcularTotalItensGeral(List<CirurgiaItem> lista) {
        Double valorTotal = 0D;
        for (CirurgiaItem cirurgiaItem : lista) {
            valorTotal += cirurgiaItem.getValorTotal();
        }
        return valorTotal;
    }

    private void atualizarValorTasy(CirurgiaItem ci) {
        try {
            bdTEJB.atualizarValoresTasy(ci);
            System.out.println("tasy ok.");
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void limparCampos() {
        cheque = new Cheque();
        validacaoCGC = false;
        CPFCPNJ = false;
        movimento = "C";
    }

    public void copiarParaAreaTransferencia(String s) {
        BCRUtils.copiarToAreaTransferencia(s);
        Mensagem.addMensagem(3, "Copiado");
    }

    public void recalcularRegrasRepasse() {
        try {
            for (Cirurgia c : cEJB.retornaListaAtendimentoExternoSemPagamento()) {
                List<CirurgiaItem> itens = new ArrayList<>();

                for (CirurgiaItem ci : c.getCirurgiaItemList()) {

                    Double valorSMA = cpEJB.retornaValorAjustadoPorRegraRepasse(ci.getIdProcedimentoPaciente());
                    if (ci.getDesconto() == null) {
                        ci.setDesconto(0D);
                    }
                    Double novoValorTotalGeral = 0D;
                    if (ci.getIdProcedimentoPaciente() != null) {
                        novoValorTotalGeral = (valorSMA + ci.getIdProcedimentoPaciente().getValorProcedimento() + ci.getDesconto());
                    }
                    ci = cpEJB.recalcularValoresItem(ci, novoValorTotalGeral);
                    itens.add(ci);
                }
                c.setValor(calcularTotalItensGeral(itens));
                cEJB.AtualizarExterno(c, itens);
            }
            carregarListaAtendimentosExternos();
            Mensagem.addMensagem(1, "Valores recalculados com sucesso.");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao recalcular.");
        }

    }

    public void recalcularRegrasRepassePorFechamento(Cirurgia c) {
        try {
            List<CirurgiaItem> itens = new ArrayList<>();
            String regra = null;
            for (CirurgiaItem ci : c.getCirurgiaItemList()) {
                Servico s = ci.getIdServico();
                Double valorSMA = cpEJB.retornaValorAjustadoPorRegraRepasse(ci.getIdProcedimentoPaciente());
                if (ci.getDesconto() == null) {
                    ci.setDesconto(0D);
                }
                Double novoValorTotalGeral = (valorSMA + ci.getIdProcedimentoPaciente().getValorProcedimento() + ci.getDesconto());
                ci = cpEJB.recalcularValoresItem(ci, novoValorTotalGeral);
                ci.getIdProcedimentoPaciente().setIdServico(s);
                ppEJB.Atualizar(ci.getIdProcedimentoPaciente(), null);
                itens.add(ci);
                if (ci.getIdProcedimentoPaciente().getIdRegraRepasse() != null) {
                    regra = ci.getIdProcedimentoPaciente().getIdRegraRepasse().getDescricao();
                }
            }
            c.setValor(calcularTotalItensGeral(itens));
            cEJB.AtualizarExterno(c, itens);
            carregarListaAtendimentosExternos();
            Mensagem.addMensagem(1, regra != null ? "Valores recalculados com sucesso. Regra selecionada: " + regra : "Valores recalculados com sucesso.");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao recalcular.");
        }

    }

    public LazyDataModel<Cirurgia> getListaAtendimentosExterno() {
        return listaAtendimentosExterno;
    }

    public void setListaAtendimentosExterno(LazyDataModel<Cirurgia> listaAtendimentosExterno) {
        this.listaAtendimentosExterno = listaAtendimentosExterno;
    }

    public List<Cirurgia> getListaFechamentos() {
        return listaFechamentos;
    }

    public void setListaFechamentos(List<Cirurgia> listaFechamentos) {
        this.listaFechamentos = listaFechamentos;
    }

    public TipoInternacao getTipoInternacao() {
        return tipoInternacao;
    }

    public void setTipoInternacao(TipoInternacao tipoInternacao) {
        this.tipoInternacao = tipoInternacao;
    }

    public UsuarioEJB getuEJB() {
        return uEJB;
    }

    public void setuEJB(UsuarioEJB uEJB) {
        this.uEJB = uEJB;
    }

    public Cirurgia getCirurgia() {
        return cirurgia;
    }

    public void setCirurgia(Cirurgia cirurgia) {
        this.cirurgia = cirurgia;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public boolean isValidacaoCGC() {
        return validacaoCGC;
    }

    public void setValidacaoCGC(boolean validacaoCGC) {
        this.validacaoCGC = validacaoCGC;
    }

    public boolean isCPFCPNJ() {
        return CPFCPNJ;
    }

    public void setCPFCPNJ(boolean CPFCPNJ) {
        this.CPFCPNJ = CPFCPNJ;
    }

    public String getMovimento() {
        return movimento;
    }

    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

}
