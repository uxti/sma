/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view.integracao;

import br.com.ux.controller.AreaProcedimentoEJB;
import br.com.ux.model.tasy.AreaProcedimento;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class AreaProcedimentoMB {

    @EJB
    AreaProcedimentoEJB apEJB;
    private AreaProcedimento areaProcedimento;
    
    public AreaProcedimentoMB() {
        novo();
    }
    
    public void novo() {
        areaProcedimento = new AreaProcedimento();
    }
    
    public List<AreaProcedimento> listarAreaProcedimento() {
        return apEJB.listarAreaProcedimento();
    }

    public AreaProcedimento getAreaProcedimento() {
        return areaProcedimento;
    }

    public void setAreaProcedimento(AreaProcedimento areaProcedimento) {
        this.areaProcedimento = areaProcedimento;
    }
    
    
    
}
