/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.DescricaoExameEJB;
import br.com.ux.model.DescricaoExame;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author PokerFace
 */
@ManagedBean
@ViewScoped
public class DescricaoExameMB implements Serializable {

    @EJB
    DescricaoExameEJB dEJB;
    UsuarioSessao us = new UsuarioSessao();
    List<DescricaoExame> listaDescricaoExame = new ArrayList<DescricaoExame>();
    List<DescricaoExame> listaaCarregarLazy;
    private DescricaoExame descricaoExame;
    LazyDataModel<DescricaoExame> model;

    public DescricaoExameMB() {
        novo();
    }

    public void novo() {
        descricaoExame = new DescricaoExame();
        carregaDatatableLazy();

    }

    public void salvar() {
        if (descricaoExame.getIdDescricaoExame() == null) {
            try {
                dEJB.Salvar(descricaoExame, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        } else {
            try {
                dEJB.Salvar(descricaoExame, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("editar");
            }
        }
    }

    public void excluir(DescricaoExame descricaoExame1) {
        try {
            dEJB.Excluir(descricaoExame, us.retornaUsuario());
            novo();
            Mensagem.addMensagemPadraoSucesso("excluir");
            BCRUtils.ResetarDatatableFiltros("formDescricaoExame:dtl");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }

    public DescricaoExame selecionaPorID(Integer id) {
        return descricaoExame = dEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public List<DescricaoExame> listaDescricaoExames() {
        listaDescricaoExame = new ArrayList<DescricaoExame>();
        listaDescricaoExame = dEJB.listaDescricaoExame("E");
        return listaDescricaoExame;
    }

    public List<DescricaoExame> getListaaCarregarLazy() {
        return listaaCarregarLazy;
    }

    public void setListaaCarregarLazy(List<DescricaoExame> listaaCarregarLazy) {
        this.listaaCarregarLazy = listaaCarregarLazy;
    }

    public LazyDataModel<DescricaoExame> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<DescricaoExame> model) {
        this.model = model;
    }

    public DescricaoExameEJB getdEJB() {
        return dEJB;
    }

    public void setdEJB(DescricaoExameEJB dEJB) {
        this.dEJB = dEJB;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public List<DescricaoExame> getListaDescricaoExame() {
        return listaDescricaoExame;
    }

    public void setListaDescricaoExame(List<DescricaoExame> listaDescricaoExame) {
        this.listaDescricaoExame = listaDescricaoExame;
    }

    public DescricaoExame getDescricaoExame() {
        return descricaoExame;
    }

    public void setDescricaoExame(DescricaoExame descricaoExame) {
        this.descricaoExame = descricaoExame;
    }

    private void carregaDatatableLazy() {
        model = new LazyDataModel<DescricaoExame>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<DescricaoExame> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and d." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where d." + filterProperty + " like'%" + filterValue + "%'");
                        }

                    } else {
                        sf.append(" and d." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by d." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by d." + sortField + " desc");
                }
                Clausula = sf.toString();
                setRowCount(Integer.parseInt(dEJB.contarDescExameRegistro(Clausula).toString()));


                listaaCarregarLazy = dEJB.listarDescExameLazyModeWhere(first, pageSize, Clausula);
//                listaPacientesLazy = pEJB.listarPacientesLazyMode(first, pageSize);
                return listaaCarregarLazy;
            }
        };
    }
}
