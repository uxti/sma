/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.EspecialidadeEJB;
import br.com.ux.controller.TerceiroEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Especialidade;
import br.com.ux.model.Terceiro;
import br.com.ux.model.Usuario;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.RelatorioFactory;
import br.com.ux.util.UsuarioSessao;
import br.com.ux.util.ValidarCPF;
import br.com.ux.util.ValidarCNPJ;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author BrunoS
 */
@ManagedBean
@ViewScoped
public class TerceiroMB implements Serializable {

    @EJB
    TerceiroEJB tEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    EspecialidadeEJB eEJB;
    Especialidade especialidade;
    Terceiro terceiro;
    Usuario usuario;
    List<Terceiro> listaaCarregar = new ArrayList<Terceiro>();
    UsuarioSessao us = new UsuarioSessao();
    List<Terceiro> listaTerceiroLazy;
    LazyDataModel<Terceiro> model, model2;
    String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public TerceiroMB() {
        novo();
        carregaModelLazy();
        carregaMedicosModelLazy();
    }

    public void novo() {
        terceiro = new Terceiro();
        usuario = new Usuario();
        especialidade = new Especialidade();
        terceiro.setTipoPessoa('F');
    }

    public void salvar() {
        ValidarCPF vCPF = new ValidarCPF();
        ValidarCNPJ vCNPJ = new ValidarCNPJ();
        String nome = "";
        try {
            try {
                nome = tEJB.verificaCPFRetornaNome(terceiro.getCgccpf(), terceiro.getTipoPessoa().toString());
            } catch (Exception e) {
                nome = "Inexistente";
            }

            if (terceiro.getTerceiroMedHosp() == null) {
                terceiro.setTerceiroMedHosp(Boolean.FALSE);
            }

            if (terceiro.getIdTerceiro() == null) {
                try {
                    if (usuario.getIdUsuario() != null) {
                        terceiro.setIdUsuario(usuario);
                    }
                    if (especialidade.getIdEspecialidade() != null) {
                        terceiro.setIdEspecialidade(especialidade);
                    }
                    if (terceiro.getInativo() == null) {
                        terceiro.setInativo(Boolean.FALSE);
                    }
                    if ((tEJB.consultarCPFCNPJ(terceiro.getCgccpf()).intValue() == 0)) {
                        if (terceiro.getTipoPessoa() == 'F') {
                            if (vCPF.isCPF(terceiro.getCgccpf())) {
                                tEJB.Salvar(terceiro, us.retornaUsuario(), false);
                                Mensagem.addMensagem(1, "Salvo com sucesso!");
                            } else {
                                Mensagem.addMensagem(3, "CPF inválido!");
                            }
                        } else if (vCNPJ.isCNPJ(terceiro.getCgccpf())) {
                            tEJB.Salvar(terceiro, us.retornaUsuario(), false);
                            Mensagem.addMensagem(1, "Salvo com sucesso!");
                        } else {
                            Mensagem.addMensagem(3, "CNPJ inválido!");
                        }
                    } else if (!nome.equals("Inexistente")) {
                        Mensagem.addMensagem(3, "Este CPF ou CNPJ já foi cadastrado para " + nome);
                    }

                } catch (Exception e) {
                    if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                        if (terceiro.getTipoPessoa() == 'F') {
                            Mensagem.addMensagem(3, "CPF ja existe");
                        } else {
                            Mensagem.addMensagem(3, "CNPJ ja existe");
                        }
                    } else {
                        Mensagem.addMensagem(3, "Erro ao tentar Salvar!");
                    }
                }
            } else {
                try {
                    if (usuario.getIdUsuario() != null) {
                        terceiro.setIdUsuario(usuario);
                    }
                    if (especialidade.getIdEspecialidade() != null) {
                        terceiro.setIdEspecialidade(especialidade);
                    }
                    if ((tEJB.consultarCPFCNPJ(terceiro.getCgccpf()).intValue() <= 1)) {
                        if (terceiro.getTipoPessoa() == 'F') {
                            if (vCPF.isCPF(terceiro.getCgccpf())) {
                                tEJB.Salvar(terceiro, us.retornaUsuario(), false);
                                Mensagem.addMensagem(1, "Editado com sucesso!");
                            } else {
                                Mensagem.addMensagem(3, "CPF inválido!");
                            }
                        } else if (vCNPJ.isCNPJ(terceiro.getCgccpf())) {
                            tEJB.Salvar(terceiro, us.retornaUsuario(), false);
                            Mensagem.addMensagem(1, "Editado com sucesso!");
                        } else {
                            Mensagem.addMensagem(3, "CNPJ inválido!");
                        }
                    } else if (!nome.equals("Inexistente")) {
                        Mensagem.addMensagem(3, "Este CPF ou CNPJ já foi cadastrado para " + nome);
                    }
                } catch (Exception e) {
                    if (e.getCause().toString().contains("MySQLIntegrityConstraintViolationException")) {
                        if (terceiro.getTipoPessoa() == 'F') {
                            Mensagem.addMensagem(3, "CPF ja existe");
                        } else {
                            Mensagem.addMensagem(3, "CNPJ ja existe");
                        }
                    } else {
                        Mensagem.addMensagem(3, "Erro ao tentar Editar!");
                    }
                }
            }
            BCRUtils.ResetarDatatableFiltros("consultaPrincipal:dt2");
        } catch (Exception e) {
            System.out.println("Erro ao salvar");
            System.out.println(e);
        }
    }

    public void excluir(Terceiro terceiro) {
        try {
            if (terceiro.getContaCorrenteList().isEmpty() && terceiro.getRepasseList().isEmpty()) {
                tEJB.Excluir(terceiro, us.retornaUsuario());
                Mensagem.addMensagem(1, "Excluido com sucesso!");
                BCRUtils.ResetarDatatableFiltros("consultaPrincipal:dt2");
                novo();
            } else {
                Mensagem.addMensagem(3, "Este cadastro já foi utilizado e não pode ser excluído. Se quiser desabilitar, clique em Editar e desmarque a opção 'Ativo'.");
            }
        } catch (Exception e) {

            Mensagem.addMensagem(3, "Erro ao tentar excluir!" + e.getMessage());
        }
    }

    public void verificaCPFCNPJ() {
        if (!terceiro.getCgccpf().equals("___.___.___-__") || (!terceiro.getCgccpf().equals("__.___.___/____-__"))) {
            System.out.println(terceiro.getCgccpf());
            ValidarCPF vcpf = new ValidarCPF();
            ValidarCNPJ vCPNJ = new ValidarCNPJ();
            String nome = "";
            if (terceiro.getTipoPessoa() == 'F') {
                if (vcpf.isCPF(terceiro.getCgccpf())) {
                    try {
                        nome = tEJB.verificaCPFRetornaNome(terceiro.getCgccpf(), terceiro.getTipoPessoa().toString());
                        if (nome.isEmpty() || nome.equals("")) {
                            nome = "Inexistente";
                        }
                    } catch (Exception e) {
                        nome = "Inexistente";
                    }
                    if (!nome.equals("Inexistente")) {
                        Mensagem.addMensagem(3, "CPF já foi cadastrado para " + nome);
                    }
                } else {
                    Mensagem.addMensagem(3, "Atenção! O CPF informado não é válido!");
                }
            } else if (vCPNJ.isCNPJ(terceiro.getCgccpf())) {
                try {
                    nome = tEJB.verificaCPFRetornaNome(terceiro.getCgccpf(), terceiro.getTipoPessoa().toString());
                    if (nome.isEmpty() || nome.equals("")) {
                        nome = "Inexistente";
                    }
                } catch (Exception e) {
                    nome = "Inexistente";
                }
                if (!nome.equals("Inexistente")) {
                    Mensagem.addMensagem(3, "CNPJ já foi cadastrado para " + nome);
                }
            } else {
                Mensagem.addMensagem(3, "Atenção! O CNPJ informado não é válido!");
            }

        }
    }

    public List<Terceiro> listaTerceiros() {
        return tEJB.listarTerceiro();
    }

    public Terceiro selecionaPorID(Integer id) {
        terceiro = tEJB.selecionarPorID(id, us.retornaUsuario());
        if (terceiro.getIdUsuario() != null) {
            usuario = terceiro.getIdUsuario();
        }
        if (terceiro.getIdEspecialidade() != null) {
            especialidade = terceiro.getIdEspecialidade();
        }
        return terceiro;
    }

    public void carregaConsulta() {
        listaaCarregar = tEJB.listarTerceiro();
    }

    public List<Usuario> listaUsuarios() {
        return uEJB.listarUsuarios();
    }

    public List<Especialidade> listaEspecialidades() {
        return eEJB.listaEspecialidades();
    }
    String espec;

    public String getEspec() {
        return espec;
    }

    public void setEspec(String espec) {
        this.espec = espec;
    }

    public void imprimirListaTerceiro() {
        if (getTipo() == null || getTipo().isEmpty() || getTipo().equals("")) {
            setTipo("%");
        }
        if (getEspec() == null || getEspec().isEmpty() || getEspec().equals("")) {
            setEspec("%");
        }
        RelatorioFactory.ListagemTerceiro("/WEB-INF/Relatorios/reportListagemTerceiro.jasper", getTipo(), getEspec(), us.retornaUsuario());
    }

    public void ConsultaCEP() {
        System.out.println("foi");
        try {
            URL url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep=" + terceiro.getCep() + "&formato=xml");
            Document document = getDocumento(url);
            Element root = document.getRootElement();
            for (Iterator i = root.elementIterator(); i.hasNext();) {
                Element element = (Element) i.next();
                if (element.getQualifiedName().equals("cidade")) {
                    terceiro.setCidade(element.getText());
                    if (element.getText().isEmpty()) {
                        Mensagem.addMensagem(3, "Aviso não foi possivel encontrar o endereço!");
                    }
                }
                if (element.getQualifiedName().equals("bairro")) {
                    terceiro.setBairro(element.getText());
                }
                if (element.getQualifiedName().equals("logradouro")) {
                    terceiro.setLogradouro(element.getText());
                }
                if (element.getQualifiedName().equals("uf")) {
                    terceiro.setUf(element.getText());
                }
                if (element.getQualifiedName().equals("tipo_logradouro")) {
                    terceiro.setTipoLogradouro(element.getText().toUpperCase());
                }
                System.out.println(terceiro.getTipoLogradouro());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }
    //==========================================================================

    public List<Terceiro> getListaTerceiroLazy() {
        return listaTerceiroLazy;
    }

    public void setListaTerceiroLazy(List<Terceiro> listaTerceiroLazy) {
        this.listaTerceiroLazy = listaTerceiroLazy;
    }

    public LazyDataModel<Terceiro> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Terceiro> model) {
        this.model = model;
    }

    public TerceiroEJB gettEJB() {
        return tEJB;
    }

    public void settEJB(TerceiroEJB tEJB) {
        this.tEJB = tEJB;
    }

    public UsuarioEJB getuEJB() {
        return uEJB;
    }

    public void setuEJB(UsuarioEJB uEJB) {
        this.uEJB = uEJB;
    }

    public Terceiro getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(Terceiro terceiro) {
        this.terceiro = terceiro;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Terceiro> getListaaCarregar() {
        return listaaCarregar;
    }

    public void setListaaCarregar(List<Terceiro> listaaCarregar) {
        this.listaaCarregar = listaaCarregar;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public EspecialidadeEJB geteEJB() {
        return eEJB;
    }

    public void seteEJB(EspecialidadeEJB eEJB) {
        this.eEJB = eEJB;
    }

    public Especialidade getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(Especialidade especialidade) {
        this.especialidade = especialidade;
    }

    private void carregaModelLazy() {
        model = new LazyDataModel<Terceiro>() {

            @Override
            public List<Terceiro> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula;
                StringBuilder sf = new StringBuilder();
                int contar = 0;
                for (String filterProperty : filters.keySet()) {
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        sf.append(" where t.").append(filterProperty).append(" like'%").append(filterValue).append("%' AND t.inativo = FALSE");
                    } else {
                        sf.append(" and t.").append(filterProperty).append(" like'%").append(filterValue).append("%' AND t.inativo = FALSE");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by t.").append(sortField).append(" asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by t.").append(sortField).append(" desc");
                }
                Clausula = sf.toString();
                setRowCount(Integer.parseInt(tEJB.contarTerceirosRegistro(Clausula).toString()));
                listaTerceiroLazy = tEJB.listarTerceirosLazyModeWhere(first, pageSize, Clausula);
                return listaTerceiroLazy;
            }
        };
    }
    
    private void carregaMedicosModelLazy() {
        model2 = new LazyDataModel<Terceiro>() {

            @Override
            public List<Terceiro> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula;
                StringBuilder sf = new StringBuilder();
                int contar = 0;
                for (String filterProperty : filters.keySet()) {
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        sf.append(" where t.").append(filterProperty).append(" like'%").append(filterValue).append("%' ");
                    } else {
                        sf.append(" and t.").append(filterProperty).append(" like'%").append(filterValue).append("%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by t.").append(sortField).append(" asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by t.").append(sortField).append(" desc");
                }
                Clausula = sf.toString();
                setRowCount(Integer.parseInt(tEJB.contarTerceirosRegistro(Clausula).toString()));
                listaTerceiroLazy = tEJB.listarTerceirosLazyModeWhere(first, pageSize, Clausula);
                return listaTerceiroLazy;
            }
        };
    }

    public LazyDataModel<Terceiro> getModel2() {
        return model2;
    }

    public void setModel2(LazyDataModel<Terceiro> model2) {
        this.model2 = model2;
    }
    
    
}
