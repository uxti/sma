/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.CaixaEJB;
import br.com.ux.controller.ChequeEJB;
import br.com.ux.controller.CirurgiaEJB;
import br.com.ux.controller.ContaCorrenteEJB;
import br.com.ux.controller.ContaPacienteEJB;
import br.com.ux.controller.RepasseEJB;
import br.com.ux.controller.ServicoEJB;
import br.com.ux.model.Caixa;
import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Motivo;
import br.com.ux.model.Repasse;
import br.com.ux.model.ServicoItemCirurgiaItem;
import br.com.ux.model.Terceiro;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

@ManagedBean
@ViewScoped
public class TrocaRepasseMB {

    @EJB
    RepasseEJB rEJB;
    @EJB
    ServicoEJB sEJB;
    @EJB
    ContaCorrenteEJB ccEJB;
    @EJB
    AuditoriaEJB aEJB;
    @EJB
    ContaPacienteEJB cpEJB;
    @EJB
    CirurgiaEJB cEJB;
    @EJB
    CaixaEJB cxEJB;
    @EJB
    ChequeEJB chEJB;

    // Objetos    
    private Repasse repasse, repasseTemp;
    private Terceiro terceiro;
    private Cheque cheque;
    private ContaCorrente contaCorrenteTemp;
    private ServicoItemCirurgiaItem sici, siciTemp;
    private UsuarioSessao us;
    // Listas
    private List<Repasse> repasses;
    private List<ContaCorrente> contas;
    // Variaveis
    private Double valor, valorTotal, valorTrocado, valorRestante;
    private String observacao, motivo, tipo;
    private DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private Cirurgia cirurgia;

    public TrocaRepasseMB() {
        repasse = new Repasse();
        repasseTemp = new Repasse();
        cheque = new Cheque();
        sici = new ServicoItemCirurgiaItem();
        siciTemp = new ServicoItemCirurgiaItem();
        contaCorrenteTemp = new ContaCorrente();
        us = new UsuarioSessao();
        terceiro = new Terceiro();
        valor = 0D;
        valorTotal = 0D;
        valorTrocado = 0D;
        valorRestante = 0D;
        repasses = new ArrayList<>();
        contas = new ArrayList<>();
        tipo = "";
    }

    public void reiniciarLancamento() {
        valor = 0D;
        repasseTemp = new Repasse();
        terceiro = new Terceiro();
    }

    public void carregarValores(Repasse r) {
        repasse = r;
        valor = repasse.getValorDocumento();
        valorTotal = repasse.getValorDocumento();
        valorTrocado = 0D;
        valorRestante = repasse.getValorDocumento();
        terceiro = new Terceiro();
        sici = new ServicoItemCirurgiaItem();
        repasses = new ArrayList<>();
        contas = new ArrayList<>();
        cheque = r.getIdCheque();
        sici = r.getIdSici();
        tipo = "T";
        motivo = null;
        cirurgia = repasse.getIdCirurgia();
    }

    public void calcularTotais() {
        valorTrocado = 0D;
        valorRestante = 0D;
        for (Repasse rp : repasses) {
            valorTrocado += rp.getValorDocumento();
        }
        valorRestante = valorTotal - valorTrocado;
    }

    public void setarTipo() {
        if (tipo.equals("D")) {
        }
    }

    public void adicionar() {
        try {
            if (terceiro.getIdTerceiro() == null || (valor.equals("") || valor.equals(0D)) || motivo.equals("")) {
                Mensagem.addMensagemGrowl(3, "Insira os campos obrigatórios.");
            } else {
                if (repasse.getIdCirurgia().isExterno()) {
                    repasseTemp = gerarRepasseExterno(repasse.getIdCirurgiaItem());
                    contaCorrenteTemp = gerarContaCorrenteExterno(repasse.getIdCirurgiaItem());
                    repasses.add(repasseTemp);
                    contas.add(contaCorrenteTemp);
                } else {
                    repasseTemp = gerarRepasse();
                    contaCorrenteTemp = gerarContaCorrente();
                    repasses.add(repasseTemp);
                    contas.add(contaCorrenteTemp);
                }
                reiniciarLancamento();
                calcularTotais();
                Mensagem.addMensagemPadraoSucesso("salvar");
            }

        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void excluirTroca(Repasse rr) {
        int indice = 0;
        int indice2 = 0;
        List<Repasse> listaTemp = new ArrayList<>();
        List<ContaCorrente> listaTempCC = new ArrayList<>();
        listaTemp = repasses;
        listaTempCC = contas;
        // Excluir o repasse
        for (Repasse r : listaTemp) {
            if (rr == r) {
                repasses.remove(indice);
                break;
            } else {
                indice++;
            }
        }
        // Excluir a conta Corrente
        for (ContaCorrente cc : listaTempCC) {
            if (cc.getIdRepasse() == rr) {
                contas.remove(indice2);
                Mensagem.addMensagemGrowl(1, "Item Removido com sucesso!");
                break;
            } else {
                indice2++;
            }
        }
        calcularTotais();
    }

    public void salvar() {
        if (tipo.equals("D")) {
            try {
                realizarDevolucao();

                // CHARLES 18072017
                // Excluir os caixas e os pagamentos referente a estes repasses.
                cxEJB.excluirCaixaPorFechamento(repasse.getIdCirurgia().getIdCirurgia());
                chEJB.excluirChequesPorFechamento(repasse.getIdCirurgia().getIdCirurgia(), null);

                cirurgia.setPago(false);
                cEJB.atualizarCirurgia(cirurgia);
                Mensagem.addMensagemGrowl(1, "Devolução realizada com sucesso.");
                RequestContext.getCurrentInstance().execute("PF('dlgTrocaRepasse').hide();");
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Erro ao lançar devolução.");
                System.out.println(e);
            }

        } else {
            try {
                if (!repasses.isEmpty()) {
                    // 08082015 Charles
                    // Salvando apenas a lista de Conta Corrente, pois estava duplicando os registros de repasses.
                    // rEJB.SalvarRepassesSimples(repasses);  
                    ccEJB.SalvarLista(contas, us.retornaUsuario());
                    estornarValores();
                    aEJB.Salvar(BCRUtils.criarAuditoria("trocarepasse", repasse.getIdRepasse().toString() + ". Valor: " + valorTotal + ". Terceiro: " + repasse.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Repasse"));
                    Mensagem.addMensagemGrowl(1, "Troca realizada com sucesso.");
                    RequestContext.getCurrentInstance().execute("PF('dlgTrocaRepasse').hide();");
                } else {
                    Mensagem.addMensagemGrowl(2, "Nenhuma troca foi feita. Nenhum registro foi encontrado. Clique em Adicionar.");
                }
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Erro ao lançar novos pagamentos. Faça a operação novamente.");
                System.out.println(e);
            }
        }
    }

    public void trocarRepasse() {
        try {
            System.out.println("méd: " + terceiro.getNome());

            repasseTemp = gerarRepasseExterno(repasse.getIdCirurgiaItem());
            contaCorrenteTemp = gerarContaCorrenteExterno(repasse.getIdCirurgiaItem());
            repasses.add(repasseTemp);
            contas.add(contaCorrenteTemp);
            
            System.out.println("méd: " + terceiro.getNome());
            
            ccEJB.SalvarLista(contas, us.retornaUsuario());
            estornarValores();
            aEJB.Salvar(BCRUtils.criarAuditoria("trocarepasse", repasse.getIdRepasse().toString() + ". Valor: " + valorTotal + ". Terceiro: " + repasse.getIdTerceiro().getNome(), us.retornaUsuario(), new Date(), "Repasse"));
            Mensagem.addMensagemGrowl(1, "Troca realizada com sucesso.");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagemGrowl(1, "Ocorreu um erro ao realizar a troca. Entre em contato com o suporte.");

        }
    }

    // Gerar novo Repasse
    public Repasse gerarRepasse() {
        siciTemp = gerarSici(sici);
        Repasse repasseT = new Repasse();
        repasseT.setIdTerceiro(terceiro);
        repasseT.setIdSici(siciTemp);
        repasseT.setIdCirurgia(repasse.getIdCirurgia());
        repasseT.setIdForma(repasse.getIdForma());
        repasseT.setIdPaciente(repasse.getIdPaciente());
        repasseT.setDtRecebimento(repasse.getDtRecebimento());
        repasseT.setDtLancamento(new Date());
        repasseT.setIdCheque(cheque);
        repasseT.setDtVencimento(repasse.getDtVencimento());
        repasseT.setNumeroParcela(repasse.getNumeroParcela());
        repasseT.setValor(0D);
        repasseT.setSituacaoPaciente('P');
        repasseT.setSituacaoTerceiro('N');
        repasseT.setStatus(repasse.getStatus());
        repasseT.setValorRecebido(valor);
        repasseT.setTipoPagamento(repasse.getTipoPagamento());
        repasseT.setValorDocumento(valor);
        repasseT.setPercentualRecebido(100D);
        repasseT.setFormaPagamento(repasse.getFormaPagamento());
        repasseT.setIdCaixa(repasse.getIdCaixa());
        repasseT.setObservacao(motivo);
        return repasseT;
    }

    public Repasse gerarRepasseExterno(CirurgiaItem antigoCi) {
        Repasse repasseT = new Repasse();
        repasseT.setIdTerceiro(terceiro);
        repasseT.setIdCirurgiaItem(antigoCi);
        repasseT.setIdCirurgia(repasse.getIdCirurgia());
        repasseT.setIdForma(repasse.getIdForma());
        repasseT.setIdPaciente(repasse.getIdPaciente());
        repasseT.setDtRecebimento(repasse.getDtRecebimento());
        repasseT.setDtLancamento(new Date());
        repasseT.setIdCheque(cheque);
        repasseT.setDtVencimento(repasse.getDtVencimento());
        repasseT.setNumeroParcela(repasse.getNumeroParcela());
        repasseT.setValor(0D);
        repasseT.setSituacaoPaciente('P');
        repasseT.setSituacaoTerceiro('N');
        repasseT.setStatus(repasse.getStatus());
        repasseT.setValorRecebido(valor);
        repasseT.setTipoPagamento(repasse.getTipoPagamento());
        repasseT.setValorDocumento(valor);
        repasseT.setPercentualRecebido(100D);
        repasseT.setFormaPagamento(repasse.getFormaPagamento());
        repasseT.setIdCaixa(repasse.getIdCaixa());
        repasseT.setObservacao("Estornado: " + motivo);
        return repasseT;
    }

    public ContaCorrente gerarContaCorrenteExterno(CirurgiaItem ci) {
        ContaCorrente cc = new ContaCorrente();
        cc.setIdTerceiro(repasseTemp.getIdTerceiro());
        cc.setIdRepasse(repasseTemp);
        cc.setIdCirurgia(repasseTemp.getIdCirurgia());
        cc.setIdCirurgiaItem(ci);
        cc.setIdPaciente(repasseTemp.getIdPaciente());
        cc.setIdCheque(cheque.getIdCheque());
        cc.setTipo("C");
        cc.setDtLancamento(new Date());
        cc.setSituacao("Não Liberado");
        cc.setStatus("Não Retirado");
        cc.setValor(repasseTemp.getValorDocumento());
        cc.setObservacao("Estornado: " + motivo);
        return cc;
    }

    // Gerar novo Conta Corrente
    public ContaCorrente gerarContaCorrente() {
        ContaCorrente cc = new ContaCorrente();
        cc.setIdTerceiro(repasseTemp.getIdTerceiro());
        cc.setIdRepasse(repasseTemp);
        cc.setIdCirurgia(repasseTemp.getIdCirurgia());
        cc.setIdCirurgiaItem(repasseTemp.getIdSici().getIdCirurgiaItem());
        cc.setIdPaciente(repasseTemp.getIdPaciente());
        cc.setIdCheque(cheque.getIdCheque());
        cc.setTipo("C");
        cc.setDtLancamento(new Date());
        cc.setSituacao("Não Liberado");
        cc.setStatus("Não Retirado");
        cc.setValor(repasseTemp.getValorDocumento());
        cc.setObservacao(motivo);
        return cc;
    }

    // Gerar novo Servico Item Cirurgia Item
    public ServicoItemCirurgiaItem gerarSici(ServicoItemCirurgiaItem sici) {
        ServicoItemCirurgiaItem siciLocal = new ServicoItemCirurgiaItem();
        if (sici != null) {
            siciLocal = sici;
        }
        siciLocal.setIdRepasse(null);
        if (sEJB.retornaServicoItemPadraoAvulso() != null) {
            siciLocal.setIdServicoItem(sEJB.retornaServicoItemPadraoAvulso());
        } else {
            siciLocal.setIdServicoItem(null);
        }
        siciLocal.setIdTerceiro(terceiro);
        siciLocal.setValor(valor);
        siciLocal.setTaxa(0D);
        siciLocal.setValorTaxado(valor);
        siciLocal.setPrincipal("S");
        siciLocal.setTaxaValor(0D);
        siciLocal.setValorPago(valor);
        siciLocal.setIdRepasse(repasseTemp);
        return siciLocal;
    }

    public void estornarValores() {
        try {
            if (repasse != null) {
                if (valorRestante.equals(0D)) {
                    repasse.setCaixaList(null);
                    repasse.setIdCaixa(null);
                    repasse.setIdCheque(null);
                }
                repasse.setObservacao("Pagamento estornado. Motivo: " + motivo + ". Valor anterior: R$ " + BCRUtils.arredondar(valorTotal) + ". Usuário: " + us.retornaUsuario() + " em: " + df.format(new Date()));
                repasse.setSacado("N");
                repasse.setSituacaoTerceiro('E');
                repasse.setSituacaoPaciente('E');
                repasse.setValor(0D);
                repasse.setValorDocumento(valorRestante);
                repasse.setValorRecebido(valorRestante);
                rEJB.SalvarSimples(repasse);
                estornarContaCorrente(ccEJB.pesquisarContaCorrentePorRepasse(repasse));
            }
        } catch (Exception e) {
            Mensagem.addMensagemGrowl(3, "Não foi possível continuar. Entre em contato com o suporte.");
            System.out.println(e);
        }

    }

    public void realizarDevolucao() {
        try {
            if (repasse != null) {

                repasse.setCaixaList(null);
                repasse.setIdCaixa(null);
                repasse.setIdCheque(null);
                repasse.setObservacao("Repasse devolvido ao cliente. Motivo: " + motivo + ". Valor anterior: " + valorTotal + ". Usuário: " + us.retornaUsuario() + " em: " + df.format(new Date()));
                repasse.setSacado("N");
                repasse.setSituacaoTerceiro('D');
                repasse.setSituacaoPaciente('D');
                rEJB.SalvarSimples(repasse);

                ContaCorrente cc;
                cc = ccEJB.pesquisarContaCorrentePorRepasse(repasse);
                cc.setDtEstorno(new Date());
                cc.setSituacao("Devolvido");
                cc.setIdCheque(null);
                cc.setStatus("Devolvido");
                cc.setObservacao("Repasse devolvido ao cliente. Motivo: " + motivo + ". Valor anterior: " + valorTotal + ". Usuário: " + us.retornaUsuario() + " em: " + df.format(new Date()));
                ccEJB.AtualizarCC(cc);

            }
        } catch (Exception e) {
            Mensagem.addMensagemGrowl(3, "Não foi possível continuar. Entre em contato com o suporte.");
            System.out.println(e);
        }
    }

    public void estornarContaCorrente(ContaCorrente conta) {
        conta.setDtEstorno(new Date());
        conta.setDtRetirada(null);
        conta.setValor(valorRestante);
        if (valorRestante.equals(0D)) {
            conta.setSituacao("Estornado");
            conta.setStatus("Estornado");
        } else {
            conta.setSituacao("Estorno Parcial");
            conta.setStatus("Estorno Parcial");
        }
        conta.setObservacao("Pagamento estornado. Motivo: " + motivo + ". Valor anterior: " + valorTotal + ". Usuário: " + us.retornaUsuario() + " em: " + df.format(new Date()));
        ccEJB.AtualizarCC(conta);
    }

    public void selecionarMotivo(Motivo mot) {
        motivo = mot.getDescricao();
    }

    public Repasse getRepasse() {
        return repasse;
    }

    public void setRepasse(Repasse repasse) {
        this.repasse = repasse;
    }

    public Terceiro getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(Terceiro terceiro) {
        this.terceiro = terceiro;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getValorTrocado() {
        return valorTrocado;
    }

    public void setValorTrocado(Double valorTrocado) {
        this.valorTrocado = valorTrocado;
    }

    public Double getValorRestante() {
        return valorRestante;
    }

    public void setValorRestante(Double valorRestante) {
        this.valorRestante = valorRestante;
    }

    public List<Repasse> getRepasses() {
        return repasses;
    }

    public void setRepasses(List<Repasse> repasses) {
        this.repasses = repasses;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Repasse getRepasseTemp() {
        return repasseTemp;
    }

    public void setRepasseTemp(Repasse repasseTemp) {
        this.repasseTemp = repasseTemp;
    }

    public ServicoItemCirurgiaItem getSici() {
        return sici;
    }

    public void setSici(ServicoItemCirurgiaItem sici) {
        this.sici = sici;
    }

    public ServicoItemCirurgiaItem getSiciTemp() {
        return siciTemp;
    }

    public void setSiciTemp(ServicoItemCirurgiaItem siciTemp) {
        this.siciTemp = siciTemp;
    }

    public List<ContaCorrente> getContas() {
        return contas;
    }

    public void setContas(List<ContaCorrente> contas) {
        this.contas = contas;
    }

    public ContaCorrente getContaCorrenteTemp() {
        return contaCorrenteTemp;
    }

    public void setContaCorrenteTemp(ContaCorrente contaCorrenteTemp) {
        this.contaCorrenteTemp = contaCorrenteTemp;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public DateFormat getDf() {
        return df;
    }

    public void setDf(DateFormat df) {
        this.df = df;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Cirurgia getCirurgia() {
        return cirurgia;
    }

    public void setCirurgia(Cirurgia cirurgia) {
        this.cirurgia = cirurgia;
    }

}
