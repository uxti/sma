/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.EspecialidadeEJB;
import br.com.ux.model.Especialidade;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author BrunoS
 */
@ManagedBean
@ViewScoped
public class EspecialidadeMB implements Serializable {

    @EJB
    EspecialidadeEJB eEJB;
    Especialidade especialidade;
    UsuarioSessao us = new UsuarioSessao();
    List<Especialidade> listaaCarregar = new ArrayList<Especialidade>();
    List<Especialidade> listaaCarregarLazy;
    LazyDataModel<Especialidade> model;

    public EspecialidadeMB() {
        novo();
        carregaModelLazy();
    }

    public void novo() {
        especialidade = new Especialidade();
    }

    public void salvar() {
        if (especialidade.getIdEspecialidade() == null) {
            try {
                eEJB.Salvar(especialidade, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("salvar");
            }
        } else {
            try {
                eEJB.Salvar(especialidade, us.retornaUsuario());
                novo();
                Mensagem.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                Mensagem.addMensagemPadraoErro("editar");
            }
        }
    }

    public void excluir(Especialidade especialidade) {
        try {
            eEJB.Excluir(especialidade, us.retornaUsuario());
            novo();
            Mensagem.addMensagemPadraoSucesso("excluir");
            BCRUtils.ResetarDatatableFiltros("formPagamento:dtl");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }

    public Especialidade selecionaPorID(Integer id) {
        return especialidade = eEJB.selecionarPorID(id, us.retornaUsuario());
    }

    public List<Especialidade> listaEspecialidades() {
        listaaCarregar = new ArrayList<Especialidade>();
        listaaCarregar = eEJB.listaEspecialidades();
        return listaaCarregar;
    }
    //==========================================================================

    public EspecialidadeEJB geteEJB() {
        return eEJB;
    }

    public void seteEJB(EspecialidadeEJB eEJB) {
        this.eEJB = eEJB;
    }

    public Especialidade getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(Especialidade especialidade) {
        this.especialidade = especialidade;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public List<Especialidade> getListaaCarregar() {
        return listaaCarregar;
    }

    public void setListaaCarregar(List<Especialidade> listaaCarregar) {
        this.listaaCarregar = listaaCarregar;
    }

    public List<Especialidade> getListaaCarregarLazy() {
        return listaaCarregarLazy;
    }

    public void setListaaCarregarLazy(List<Especialidade> listaaCarregarLazy) {
        this.listaaCarregarLazy = listaaCarregarLazy;
    }

    public LazyDataModel<Especialidade> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Especialidade> model) {
        this.model = model;
    }

    private void carregaModelLazy() {
        model = new LazyDataModel<Especialidade>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Especialidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        sf.append(" where e." + filterProperty + " like'%" + filterValue + "%' AND e.inativo = FALSE");
                    } else {
                        sf.append(" and e." + filterProperty + " like'%" + filterValue + "%' AND e.inativo = FALSE");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by e." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by e." + sortField + " desc");
                }
                Clausula = sf.toString();
                setRowCount(Integer.parseInt(eEJB.contarEspecialidadeRegistro(Clausula).toString()));


                listaaCarregarLazy = eEJB.listarEspecialidadeLazyModeWhere(first, pageSize, Clausula);
//                listaPacientesLazy = pEJB.listarPacientesLazyMode(first, pageSize);
                return listaaCarregarLazy;
            }

         
        };
    }
}
