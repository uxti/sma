/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.ProcedimentoEJB;
import br.com.ux.model.Procedimento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class ProcedimentoMB implements Serializable {

    @EJB
    ProcedimentoEJB procedimentoEJB;
    private Procedimento procedimento;
    UsuarioSessao us = new UsuarioSessao();
    private List<Procedimento> listaProcedimentos;
    private List<Procedimento> listaProcedimentosLazy;
    private LazyDataModel<Procedimento> model;

    public ProcedimentoMB() {
        novo();
        model = new LazyDataModel<Procedimento>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Procedimento> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        sf.append(" where p.inativo = FALSE");
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(procedimentoEJB.contarProcedimentoRegistro(Clausula).toString()));

                listaProcedimentosLazy = procedimentoEJB.listarProcedimentoLazyModeWhere(first, pageSize, Clausula);
//                listaPacientesLazy = pEJB.listarPacientesLazyMode(first, pageSize);
                return listaProcedimentosLazy;
            }
        };
    }

    public void novo() {
        procedimento = new Procedimento();
    }

    public void salvar() {
        try {

            procedimento.setInativo(Boolean.FALSE);
            if (procedimento.getIdProcedimento() != null) {

                procedimentoEJB.Salvar(procedimento, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("editar");
            } else {
                procedimentoEJB.Salvar(procedimento, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("salvar");
            }
            novo();

        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void salvar(String padrao) {
        try {
            procedimento.setPadrao(padrao);
            procedimento.setInativo(Boolean.FALSE);
            procedimento.setDescricao(procedimento.getDescricao().toUpperCase());
            if (procedimento.getIdProcedimento() != null) {
                procedimentoEJB.Salvar(procedimento, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("editar");
            } else {
                procedimentoEJB.Salvar(procedimento, us.retornaUsuario());
                Mensagem.addMensagemPadraoSucesso("salvar");
            }
            novo();
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public List<Procedimento> listaProcedimentos() {
        return null;
    }

    public void excluir(Procedimento p) {
        try {
            procedimentoEJB.Excluir(procedimento, us.retornaUsuario());
            Mensagem.addMensagemPadraoSucesso("excluir");
            novo();
            BCRUtils.ResetarDatatableFiltros("formProcedimento:dtl");
        } catch (Exception e) {
            Mensagem.addMensagemPadraoErro("excluir");
        }
    }

    public Procedimento selecionarPorID(Integer id) {
        procedimento = procedimentoEJB.selecionarPorID(id, us.retornaUsuario());
        return procedimento;
    }

    public void selecionarProcedimento(Procedimento p) {
        procedimento = p;
    }

    public Procedimento selecionarPorPadrao(String s) {
        return procedimento = procedimentoEJB.selecionarPorPadrao(s);
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public List<Procedimento> getListaProcedimentos() {
        return listaProcedimentos;
    }

    public void setListaProcedimentos(List<Procedimento> listaProcedimentos) {
        this.listaProcedimentos = listaProcedimentos;
    }

    public List<Procedimento> getListaProcedimentosLazy() {
        return listaProcedimentosLazy;
    }

    public void setListaProcedimentosLazy(List<Procedimento> listaProcedimentosLazy) {
        this.listaProcedimentosLazy = listaProcedimentosLazy;
    }

    public LazyDataModel<Procedimento> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Procedimento> model) {
        this.model = model;
    }
}
