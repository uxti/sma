/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.view;

import br.com.ux.controller.AreaProcedimentoEJB;
import br.com.ux.controller.EspecialidadeProcedimentoEJB;
import br.com.ux.controller.ServicoEJB;
import br.com.ux.model.Servico;
import br.com.ux.model.ServicoItem;
import br.com.ux.model.tasy.AreaProcedimento;
import br.com.ux.model.tasy.EspecialidadeProcedimento;
import br.com.ux.model.tasy.GrupoProcedimentoCbhpm;
import br.com.ux.pojo.Documento;
import br.com.ux.util.BCRUtils;
import br.com.ux.util.Mensagem;
import br.com.ux.util.UsuarioSessao;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

/**
 *
 * @author BrunoS
 */
@ManagedBean
@ViewScoped
public class ServicoMB implements Serializable {

    @EJB
    ServicoEJB sEJB;
    Servico servico;
    List<Servico> listaaCarregar;
    List<Servico> listaaCarregarLazy;
    private List<ServicoItem> itens;
    private ServicoItem servicoItem;
    UsuarioSessao us = new UsuarioSessao();
    private LazyDataModel<Servico> model;
    private LazyDataModel<Servico> listaProcedimentosCBHPMLazy;
    // Controle de itens //
    private boolean semItens, padrao;
    private Boolean cbhpm = Boolean.FALSE;
    private TreeNode root;

    @EJB
    AreaProcedimentoEJB apEJB;
    @EJB
    EspecialidadeProcedimentoEJB epEJB;
    private boolean editar;

    public ServicoMB() {
        novo();
    }

    public void novoItem() {
        servicoItem = new ServicoItem();
        editar = false;
        if (servico.getDescricao() != null) {
            if ((servico.getEscala() * 100) == 100) {
                servicoItem.setBaseCalculo('S');
                servicoItem.setDescritiva(servico.getDescricao());
                servicoItem.setPercentual(100D);
            }
        }
    }

    public void novo() {
        servico = new Servico();
        itens = new ArrayList<ServicoItem>();
        semItens = false;
        padrao = false;
        servicoItem = new ServicoItem();
        listaaCarregarLazy = new ArrayList<Servico>();
        percentualFechamento = 0D;
        model = new LazyDataModel<Servico>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Servico> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE AND s.idGrupoProcedimentoCbhpm is null");
                        } else {
                            sf.append(" where s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE AND s.idGrupoProcedimentoCbhpm is null");
                        }
                    } else {
                        sf.append(" and s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE AND s.idGrupoProcedimentoCbhpm is null");
                    }
//                    Clausula = sf.toString();
//                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by s." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by s." + sortField + " desc");
                }
                Clausula = sf.toString();
                contar = contar + 1;
                setRowCount(Integer.parseInt(sEJB.contarServicosRegistro(Clausula).toString()));

                listaaCarregarLazy = sEJB.listarServicosLazyModeWhere(first, pageSize, Clausula);
//                listaPacientesLazy = pEJB.listarPacientesLazyMode(first, pageSize);
                return listaaCarregarLazy;
            }
        };
    }
//
//    public void salvar2() {
//        List<ServicoItem> itensTemp = new ArrayList<ServicoItem>();
////        sEJB.SalvarServico(servico);
//        for (ServicoItem si : itens) {
//            si.setIdServico(servico);
//            itensTemp.add(si);
//        }
//        sEJB.SalvarServicoItens(itensTemp);
//    }

    public void salvar() {
        Double total = 0D;
        for (ServicoItem si : itens) {
            total = total + si.getPercentual();
        }

        if (padrao) {
            servico.setPadrao("S");
        } else {
            servico.setPadrao("N");
        }

        setPercentualFechamento(total);
        BigDecimal valorExato = new BigDecimal(servico.getEscala() * 100).setScale(2, RoundingMode.HALF_DOWN);

        if (servico.getInativo() == null) {
            servico.setInativo(Boolean.FALSE);
        }

        if (getPercentualFechamento() != valorExato.doubleValue()) {
            Mensagem.addMensagem(3, "Atenção o percentual dos itens não conferem com a escala!");
        } else if (servico.getIdServico() == null) {
            try {
                if (itens.isEmpty()) {
                    Mensagem.addMensagem(3, "Adicione um item!");
                } else {
//                    int index = 0;
                    for (ServicoItem si : itens) {
                        si.setIdServico(servico);
//                        si.setOrdenar(index++);
                    }
                    sEJB.Salvar(servico, itens, us.retornaUsuario());
                    novo();
                    Mensagem.addMensagemPadraoSucesso("salvar");
                }
            } catch (Exception e) {
                System.out.println(e);
                Mensagem.addMensagemPadraoErro("salvar");
            }
        } else {
            try {
                if (itens.isEmpty()) {
                    Mensagem.addMensagem(3, "Adicione um item!");
                } else {
//                    int index = servicoItem.getOrdenar();
                    for (ServicoItem si : itens) {
                        si.setIdServico(servico);
//                        si.setOrdenar(index++);
                    }
                    sEJB.Salvar(servico, itens, us.retornaUsuario());
                    novo();
                    Mensagem.addMensagemPadraoSucesso("editar");
                }
            } catch (Exception e) {
                System.out.println(e);
                Mensagem.addMensagemPadraoErro("editar");
            }
        }
    }

    public void excluir(Servico servico) {
        try {
            if (servico.getServicoItemList().isEmpty() && servico.getCirurgiaItemList().isEmpty() && servico.getOrcamentoItemList().isEmpty()) {
                itens = servico.getServicoItemList();
                sEJB.Excluir(servico, us.retornaUsuario(), itens);
                novo();
                Mensagem.addMensagemPadraoSucesso("excluir");
                BCRUtils.ResetarDatatableFiltros("formMotivo:dtl");
            } else {
                Mensagem.addMensagem(3, "Este serviço já foi utilizado e não pode ser excluído. Se quiser desabilitar, clique em Editar e desmarque a opção 'Ativo'.");
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar excluir!");
        }
    }

    public List<Servico> listaServicos() {
        novo();
        return sEJB.listarServicos();
    }

    public void salvarItemSimples(ServicoItem si) {
        sEJB.SalvarServicoItemSimples(si);
        editar = false;
        Mensagem.addMensagemPadraoSucesso("editar");
    }

    public Servico selecionaPorID(Integer id) {
        servico = sEJB.selecionarPorID(id, us.retornaUsuario());
        itens = servico.getServicoItemList();
        return servico;
    }

    public void selecionaItem(ServicoItem si) {
        servicoItem = new ServicoItem();

        System.out.println("editando serviço item: " + si.getDescritiva());
        servicoItem = si;
        editar = true;
    }

    public void carregaLista() {
        //listaaCarregar = sEJB.listarServicos();
    }

    public void carregarTabelaCbhpm() {
        if (cbhpm) {
            cbhpm = Boolean.FALSE;
        } else {
            root = criarArvoreDeServicos();
            cbhpm = Boolean.TRUE;
        }
//        listaProcedimentosCBHPMLazy = new LazyDataModel<Servico>() {
//            private static final long serialVersionUID = 1L;
//
//            @Override
//            public List<Servico> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
//                String Clausula = "";
//                StringBuffer sf = new StringBuffer();
//                int contar = 0;
//                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
//                    String filterProperty = it.next(); // table column name = field name  
//                    Object filterValue = filters.get(filterProperty);
//                    if (contar == 0) {
//                        if (sf.toString().contains("where")) {
//                            sf.append(" and s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE AND s.idGrupoProcedimentoCbhpm is not null");
//                        } else {
//                            sf.append(" where s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE AND s.idGrupoProcedimentoCbhpm is not null");
//                        }
//                    } else {
//                        sf.append(" and s." + filterProperty + " like'%" + filterValue + "%' AND s.inativo = FALSE");
//                    }
//                }
//                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
//                    sf.append(" order by s." + sortField + " asc");
//                }
//                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
//                    sf.append(" order by s." + sortField + " desc");
//                }
//                Clausula = sf.toString();
//                contar = contar + 1;
//                setRowCount(Integer.parseInt(sEJB.contarServicosRegistro(Clausula).toString()));
//                listaaCarregar = sEJB.listarServicosLazyModeWhere(first, pageSize, Clausula);
//                return listaaCarregar;
//            }
//        };
    }

    public TreeNode criarArvoreDeServicos() {
        TreeNode root = new DefaultTreeNode(new Documento("", "", "", ""), null);

        for (AreaProcedimento ap : apEJB.listarAreaProcedimento()) {
            TreeNode area = new DefaultTreeNode(new Documento(ap.getIdAreaProcedimento().toString(), ap.getIdAreaProcedimentoTasy().toString(), ap.getDescricao(), ""), root);
            for (EspecialidadeProcedimento ep : ap.getEspecialidadeProcedimentoList()) {
                TreeNode esp = new DefaultTreeNode(new Documento(ep.getIdEspecialidadeProcedimento().toString(), ep.getIdEspecialidadeProcedimentoTasy().toString(), ep.getDescricao(), ""), area);
                for (GrupoProcedimentoCbhpm gp : ep.getGrupoProcedimentoCbhpmList()) {
                    TreeNode grp = new DefaultTreeNode(new Documento(gp.getIdGrupoProcedimentoCbhpm().toString(), gp.getIdGrupoProcedimentoTasy().toString(), gp.getDescricao(), ""), esp);
                    for (Servico srv : gp.getServicoList()) {
                        TreeNode proc = new DefaultTreeNode(new Documento(srv.getIdServico().toString(), srv.getIdProcedimentoTasy().toString(), srv.getDescricao(), srv.getClassificacao()), grp);
                    }
                }
            }
        }
        return root;
    }

    public ServicoEJB getsEJB() {
        return sEJB;
    }

    public void setsEJB(ServicoEJB sEJB) {
        this.sEJB = sEJB;
    }

    public LazyDataModel<Servico> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<Servico> model) {
        this.model = model;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public List<Servico> getListaaCarregar() {
        return listaaCarregar;
    }

    public void setListaaCarregar(List<Servico> listaaCarregar) {
        this.listaaCarregar = listaaCarregar;
    }

    public UsuarioSessao getUs() {
        return us;
    }

    public void setUs(UsuarioSessao us) {
        this.us = us;
    }

    public List<ServicoItem> getItens() {
        return itens;
    }

    public void setItens(List<ServicoItem> itens) {
        this.itens = itens;
    }

    public ServicoItem getServicoItem() {
        return servicoItem;
    }

    public void setServicoItem(ServicoItem servicoItem) {
        this.servicoItem = servicoItem;
    }
    public Double percentualFechamento;

    public Double getPercentualFechamento() {
        return percentualFechamento;
    }

    public void setPercentualFechamento(Double percentualFechamento) {
        this.percentualFechamento = percentualFechamento;
    }

    public void addItens() {
        try {
            itens.add(servicoItem);
            servicoItem = new ServicoItem();
            calcularTotais();
            servicoItem.setPercentual(valorTotalRestantePercentual * 100);
            RequestContext.getCurrentInstance().execute("Focus();");
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao tentar remover o item!" + e);
        }

    }
    Double valorTotalPercentual, valorTotalRestantePercentual;

    public void calcularTotais() {
        if (!itens.isEmpty() && (servico != null)) {
            valorTotalPercentual = 0D;
            valorTotalRestantePercentual = 0D;
            for (ServicoItem si : itens) {
                valorTotalPercentual = valorTotalPercentual + si.getPercentual();
            }
            valorTotalRestantePercentual = (servico.getEscala() * 100) - valorTotalPercentual;
            valorTotalRestantePercentual = valorTotalRestantePercentual / 100;
            valorTotalPercentual = valorTotalPercentual / 100;
        }
    }

    public void excluirItem(ServicoItem servicoItem, int indice) {
        if (servicoItem.getIdServicoItem() != null) {
            try {
                sEJB.ExcluirItem(servicoItem, us.retornaUsuario());
                itens.remove(indice);
                Mensagem.addMensagem(1, "Item removido com sucesso!");
                calcularTotais();
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Erro ao tentar remover o item!" + e);
            }

        } else {
            try {
                itens.remove(indice);
                Mensagem.addMensagem(1, "Item removido com sucesso!");
                calcularTotais();
            } catch (Exception e) {
                Mensagem.addMensagem(3, "Erro ao tentar remover o item!" + e);
            }
        }
    }

    public void setarBaseDeCalculo() {
        if (servicoItem.getBaseCalculo().equals('S')) {
            servicoItem.setPercentual(100D);
        }
    }

    public Double getValorTotalPercentual() {
        return valorTotalPercentual;
    }

    public void setValorTotalPercentual(Double valorTotalPercentual) {
        this.valorTotalPercentual = valorTotalPercentual;
    }

    public Double getValorTotalRestantePercentual() {
        return valorTotalRestantePercentual;
    }

    public void setValorTotalRestantePercentual(Double valorTotalRestantePercentual) {
        this.valorTotalRestantePercentual = valorTotalRestantePercentual;
    }

    public boolean isSemItens() {
        return semItens;
    }

    public void setSemItens(boolean semItens) {
        this.semItens = semItens;
    }

    public boolean isPadrao() {
        return padrao;
    }

    public void setPadrao(boolean padrao) {
        this.padrao = padrao;
    }

    public List<Servico> getListaaCarregarLazy() {
        return listaaCarregarLazy;
    }

    public void setListaaCarregarLazy(List<Servico> listaaCarregarLazy) {
        this.listaaCarregarLazy = listaaCarregarLazy;
    }

    public LazyDataModel<Servico> getListaProcedimentosCBHPMLazy() {
        return listaProcedimentosCBHPMLazy;
    }

    public void setListaProcedimentosCBHPMLazy(LazyDataModel<Servico> listaProcedimentosCBHPMLazy) {
        this.listaProcedimentosCBHPMLazy = listaProcedimentosCBHPMLazy;
    }

    public Boolean getCbhpm() {
        return cbhpm;
    }

    public void setCbhpm(Boolean cbhpm) {
        this.cbhpm = cbhpm;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public boolean isEditar() {
        return editar;
    }

    public void setEditar(boolean editar) {
        this.editar = editar;
    }

}
