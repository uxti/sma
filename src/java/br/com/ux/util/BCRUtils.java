/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import br.com.ux.controller.AuditoriaEJB;
import br.com.ux.controller.UsuarioEJB;
import br.com.ux.model.Auditoria;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.LogIntegracao;
import br.com.ux.model.MovimentoCaixa;
import br.com.ux.model.Usuario;
import com.guilhermechapiewski.fluentmail.email.EmailMessage;
import com.guilhermechapiewski.fluentmail.transport.EmailTransportConfiguration;
import com.mysql.jdbc.StringUtils;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Renato
 */
public class BCRUtils {

    @EJB
    AuditoriaEJB aEJB;
    @EJB
    UsuarioEJB uEJB;

    public static String generateCreateQueryBcr(String sql, List<Parametro> listaParametros) {
        String saida;
        StringBuffer strBuf = new StringBuffer();
        strBuf.append(sql);
        int total = 0;
        for (Parametro pp : listaParametros) {
            if (pp.getParametro() != null) {
                strBuf.append(" and ");
                strBuf.append(pp.getCampo() + "=" + pp.getParametro());
            }
            total = total + 1;
        }
        if (strBuf.toString().endsWith("and ")) {
            int tamanho = strBuf.length();
            saida = strBuf.substring(0, tamanho - 4);
        } else {
            saida = strBuf.toString();
        }
        return saida;
    }

    public static void ResetarDatatableFiltros(String form) {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(form);
        if (dataTable != null) {
            dataTable.reset();
        }
    }

    public static String captalizar(String a) {
        String aa = a.substring(0, 1).toUpperCase();
        String ab = a.substring(1);
        String f = aa + ab;
        return f;
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public static Date addDia(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.DAY_OF_MONTH, qtd);
        return cal.getTime();
    }

    public static Date addAno(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.YEAR, qtd);
        return cal.getTime();
    }

    public static Date minusDia(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.DAY_OF_MONTH, qtd);
        return cal.getTime();
    }

    public static Date addDiaSemana(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.DAY_OF_WEEK, qtd);
        return cal.getTime();
    }

    public static Date ultimoDiaDoMes(Date date) {
        Calendar data = Calendar.getInstance();
        data.setTime(date);
        int ultimoDiaMes = data.getActualMaximum(Calendar.DAY_OF_MONTH);
        data.set(Calendar.DAY_OF_MONTH, ultimoDiaMes);
        return data.getTime();
    }

    public static Date primeiroDiaDoMes(Date date) {
        Calendar data = Calendar.getInstance();
        data.setTime(date);
        int primeiroDiaMes = data.getActualMinimum(Calendar.DAY_OF_MONTH);
        data.set(Calendar.DAY_OF_MONTH, primeiroDiaMes);
        return data.getTime();
    }

    public static Date primeiroDiaDoAno(Date date) {
        Calendar data = Calendar.getInstance();
        data.setTime(date);
        int primeiroDiaAno = data.getActualMinimum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_MONTH, primeiroDiaAno);
        return data.getTime();
    }

    public static Date ultimoDiaDoAno(Date date) {
        Calendar data = Calendar.getInstance();
        data.setTime(date);
        int ultimoDiaAno = data.getActualMaximum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_MONTH, ultimoDiaAno);
        return data.getTime();
    }

    public static Double arredondar(Double d) {
        return Math.round(d * 100.0) / 100.0;
    }

    public static MovimentoCaixa criarMovimentoCaixa(String movimento, String desc, Date dtMovimentacao, Double valor, String lote, ContaCorrente idSaque, Usuario usuario) {
        MovimentoCaixa mc = new MovimentoCaixa();
        String data, descricao;
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        data = df.format(new Date());
        
        switch (desc.toLowerCase()) {
            case "transferência":
                descricao = "Transferência aceita em " + data + ". Lote: " + lote;               
                break;
            case "pagamento":
                descricao = "Pagamento realizado em " + data + " ao médico: " + idSaque.getIdTerceiro().getNome();               
                break;
            default:
                descricao = movimento.equals("C") ? "Crédito gerado em " : "Débito gerado em " + data ;               
        }
        mc.setTipoMovimentacao(movimento);
        mc.setDescricao(descricao);
        mc.setDtMovimentacao(dtMovimentacao);
        mc.setValor(valor);
        mc.setLoteTransferencia(lote);
        mc.setIdSaque(idSaque);
        mc.setIdUsuario(usuario);
        return mc;
    }

    public static Auditoria criarAuditoria(String acao, String identificador, String usuario, Date data, String tabela) {
        String tipo = "";
        String descricao = "";
        DateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        switch (acao.toLowerCase().toString()) {
            case "insert":
                tipo = "Cadastro";
                descricao = ". Registro '" + identificador + "' criado.";
                break;
            case "update":
                tipo = "Atualização";
                descricao = ". Registro '" + identificador + "' alterado.";
                break;
            case "delete":
                tipo = "Remoção";
                descricao = ". Registro '" + identificador + "' excluído.";
                break;
            case "pagamento":
                tipo = "Pagamento";
                descricao = ". Pagamento '" + identificador + "' realizado.";
                break;
            case "liberar":
                tipo = "Liberação";
                descricao = ". Liberação de Pagamento nº: '" + identificador + "'.";
                break;
            case "liberarorcamento":
                tipo = "Liberação";
                descricao = ". Liberação do Orçamento nº: '" + identificador + "'. realizada.";
                break;
            case "liberarfechamento":
                tipo = "Liberação";
                descricao = ". Liberação do Fechamento nº: '" + identificador + "'. realizada.";
                break;
            case "compensar":
                tipo = "Compensado";
                descricao = ". Cheque '" + identificador + "' compensado.";
                break;
            case "cancelamento":
                tipo = "Cancelado";
                descricao = ". Cancelamento '" + identificador + "' realizado.";
                break;
            case "troca":
                tipo = "Cancelado";
                descricao = ". Troca de Cheque: '" + identificador + "' realizada.";
                break;
            case "copiar":
                tipo = "Copiado";
                descricao = ". Cópia do Orçamento nº: '" + identificador + "' realizada.";
                break;
            case "transferencia":
                tipo = "Transferência";
                descricao = ". Transferência nº: '" + identificador;
                break;
            case "estorno":
                tipo = "Estorno";
                descricao = ". Estorno do Pagamento nº: '" + identificador;
                break;
            case "avulso":
                tipo = "Avulso";
                descricao = ". Lançamento Avulso nº: '" + identificador;
                break;
            case "trocarepasse":
                tipo = "Troca";
                descricao = ". Troca e estorno do repasse nº: '" + identificador;
                break;
            case "movimentarCheque":
                tipo = "Movimentação";
                descricao = ". Movimentação: '" + identificador;
                break;

            default:
                tipo = "Geral";
                descricao = identificador + "' realizado.";
        }
        Auditoria a = new Auditoria();
        a.setDescricao("Ação: " + tipo + descricao + " Tabela: " + tabela + ". Login: " + usuario + ". Data/Hora: " + f.format(data));
        a.setAcao(tipo);
        a.setUsuario(usuario);
        a.setDtHora(data);
        a.setTabela(tabela);
        return a;
    }

    public static void enviarEmail(String smtpServer, String emailRemetente, String senha, String destinario, String assunto, String mensagem) {
        try {
            EmailTransportConfiguration.configure(smtpServer, true, true, emailRemetente, senha);

            new EmailMessage()
                    .from(emailRemetente)
                    .to(destinario)
                    .withSubject(assunto)
                    .withBody(mensagem)
                    .send();
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao enviar email.");
        }
    }

    public static void enviarEmailHtml(String smtpServer, Integer portaSmtp, String emailPadrao, String senhaPadrao, String assunto, String textoPreVisualizacao, String texto,
            String usuarioEmitente, String emailUsuarioEmitente, String usuarioDestinatario, String emaildestinatario, String emailCC) throws EmailException {
        try {
            String mensagem = "<!doctype html>\n"
                    + "<html>\n"
                    + "  <head>\n"
                    + "    <meta name=\"viewport\" content=\"width=device-width\">\n"
                    + "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                    + "    <title>Simple Transactional Email</title>\n"
                    + "    <style media=\"all\" type=\"text/css\">\n"
                    + "    @media all {\n"
                    + "      .btn-primary table td:hover {\n"
                    + "        background-color: #34495e !important;\n"
                    + "      }\n"
                    + "      .btn-primary a:hover {\n"
                    + "        background-color: #34495e !important;\n"
                    + "        border-color: #34495e !important;\n"
                    + "      }\n"
                    + "    }\n"
                    + "    \n"
                    + "    @media all {\n"
                    + "      .btn-secondary a:hover {\n"
                    + "        border-color: #34495e !important;\n"
                    + "        color: #34495e !important;\n"
                    + "      }\n"
                    + "    }\n"
                    + "    \n"
                    + "    @media only screen and (max-width: 620px) {\n"
                    + "      table[class=body] h1 {\n"
                    + "        font-size: 28px !important;\n"
                    + "        margin-bottom: 10px !important;\n"
                    + "      }\n"
                    + "      table[class=body] h2 {\n"
                    + "        font-size: 22px !important;\n"
                    + "        margin-bottom: 10px !important;\n"
                    + "      }\n"
                    + "      table[class=body] h3 {\n"
                    + "        font-size: 16px !important;\n"
                    + "        margin-bottom: 10px !important;\n"
                    + "      }\n"
                    + "      table[class=body] p,\n"
                    + "      table[class=body] ul,\n"
                    + "      table[class=body] ol,\n"
                    + "      table[class=body] td,\n"
                    + "      table[class=body] span,\n"
                    + "      table[class=body] a {\n"
                    + "        font-size: 16px !important;\n"
                    + "      }\n"
                    + "      table[class=body] .wrapper,\n"
                    + "      table[class=body] .article {\n"
                    + "        padding: 10px !important;\n"
                    + "      }\n"
                    + "      table[class=body] .content {\n"
                    + "        padding: 0 !important;\n"
                    + "      }\n"
                    + "      table[class=body] .container {\n"
                    + "        padding: 0 !important;\n"
                    + "        width: 100% !important;\n"
                    + "      }\n"
                    + "      table[class=body] .header {\n"
                    + "        margin-bottom: 10px !important;\n"
                    + "      }\n"
                    + "      table[class=body] .main {\n"
                    + "        border-left-width: 0 !important;\n"
                    + "        border-radius: 0 !important;\n"
                    + "        border-right-width: 0 !important;\n"
                    + "      }\n"
                    + "      table[class=body] .btn table {\n"
                    + "        width: 100% !important;\n"
                    + "      }\n"
                    + "      table[class=body] .btn a {\n"
                    + "        width: 100% !important;\n"
                    + "      }\n"
                    + "      table[class=body] .img-responsive {\n"
                    + "        height: auto !important;\n"
                    + "        max-width: 100% !important;\n"
                    + "        width: auto !important;\n"
                    + "      }\n"
                    + "      table[class=body] .alert td {\n"
                    + "        border-radius: 0 !important;\n"
                    + "        padding: 10px !important;\n"
                    + "      }\n"
                    + "      table[class=body] .span-2,\n"
                    + "      table[class=body] .span-3 {\n"
                    + "        max-width: none !important;\n"
                    + "        width: 100% !important;\n"
                    + "      }\n"
                    + "      table[class=body] .receipt {\n"
                    + "        width: 100% !important;\n"
                    + "      }\n"
                    + "    }\n"
                    + "    \n"
                    + "    @media all {\n"
                    + "      .ExternalClass {\n"
                    + "        width: 100%;\n"
                    + "      }\n"
                    + "      .ExternalClass,\n"
                    + "      .ExternalClass p,\n"
                    + "      .ExternalClass span,\n"
                    + "      .ExternalClass font,\n"
                    + "      .ExternalClass td,\n"
                    + "      .ExternalClass div {\n"
                    + "        line-height: 100%;\n"
                    + "      }\n"
                    + "      .apple-link a {\n"
                    + "        color: inherit !important;\n"
                    + "        font-family: inherit !important;\n"
                    + "        font-size: inherit !important;\n"
                    + "        font-weight: inherit !important;\n"
                    + "        line-height: inherit !important;\n"
                    + "        text-decoration: none !important;\n"
                    + "      }\n"
                    + "    }\n"
                    + "    </style>\n"
                    + "  </head>\n"
                    + "  <body class=\"\" style=\"font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFF; margin: 0; padding: 0;\">\n"
                    + "    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #FFF;\" width=\"100%\" bgcolor=\"#FFF\">\n"
                    + "      <tr>\n"
                    + "        <td style=\"font-family: sans-serif; font-size: 14px; vertical-align: top;\" valign=\"top\">&nbsp;</td>\n"
                    + "        <td class=\"container\" style=\"font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 700px; padding: 0px; width: 700px;\" width=\"580\" valign=\"top\">\n"
                    + "          <div class=\"content\" style=\"box-sizing: border-box; display: block; max-width: 700px; padding: 0px;\">\n"
                    + "\n"
                    + "            <!-- Texto de Pré-Visualização -->\n"
                    + "            <span class=\"preheader\" style=\"color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;\">" + textoPreVisualizacao + "</span>\n"
                    + "            <table class=\"main\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 3px;\" width=\"100%\">\n"
                    + "\n"
                    + "              <!-- Conteúdo Principal -->\n"
                    + "              <tr>\n"
                    + "                <td class=\"wrapper\" style=\"font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box;\" valign=\"top\">\n"
                    + "                  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\" width=\"100%\">\n"
                    + "                    <tr>\n"
                    + "                      <td style=\"font-family: sans-serif; font-size: 14px; vertical-align: top;\" valign=\"top\">\n"
                    + "                        <p style=\"font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;\">" + texto + "</p>\n"
                    + "                      </td>\n"
                    + "                    </tr>\n"
                    + "                  </table>\n"
                    + "                </td>\n"
                    + "              </tr>\n"
                    + "\n"
                    + "              <!-- Fim do Conteúdo Principal -->\n"
                    + "              </table>\n"
                    + "\n"
                    + "           </div>\n"
                    + "        </td>\n"
                    + "        <td style=\"font-family: sans-serif; font-size: 14px; vertical-align: top;\" valign=\"top\">&nbsp;</td>\n"
                    + "      </tr>\n"
                    + "    </table>\n"
                    + "  </body>\n"
                    + "</html>";
            HtmlEmail email = new HtmlEmail();
            email.setHostName(smtpServer);
            email.setSmtpPort(portaSmtp);
            email.setSslSmtpPort(portaSmtp.toString());
            email.setSSL(true);
            email.setSSLOnConnect(true);
            email.setAuthenticator(new DefaultAuthenticator(emailPadrao, senhaPadrao));
            email.addTo(emaildestinatario);
            email.setFrom(emailUsuarioEmitente, "SMA");
            email.addReplyTo(emailUsuarioEmitente, usuarioEmitente);
            System.out.println("Cópia: " + StringUtils.isNullOrEmpty(emailCC));
            if (!StringUtils.isNullOrEmpty(emailCC)) {
                email.addCc(emailCC);
            }

            email.setSubject(assunto);
            email.setHtmlMsg(mensagem);
            email.send();
        } catch (Exception e) {
            System.out.println(e);
            Mensagem.addMensagem(3, "Erro ao enviar email.");
        }

    }

    public static LogIntegracao gerarLogIntegracao(String log, String metodo, Date dtInicio, Date dtFim, LogIntegracao logPai) {
        LogIntegracao lg = new LogIntegracao();
        lg.setDescricao(log);
        lg.setIdLogIntegracaoPai(logPai);
        lg.setMetodo(metodo);
        lg.setDtInicio(dtInicio);
        lg.setDtFim(dtFim);
        return lg;
    }

    public static void copiarToAreaTransferencia(String s) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        String text = s;
        StringSelection selection = new StringSelection(text);
        clipboard.setContents(selection, null);
    }

//    class CustomProgressListener implements MediaHttpUploaderProgressListener {
//
//        public void progressChanged(MediaHttpUploader uploader) throws IOException {
//            switch (uploader.getUploadState()) {
//                case INITIATION_STARTED:
//                    System.out.println("Initiation has started!");
//                    break;
//                case INITIATION_COMPLETE:
//                    System.out.println("Initiation is complete!");
//                    break;
//                case MEDIA_IN_PROGRESS:
//                    System.out.println(uploader.getProgress());
//                    break;
//                case MEDIA_COMPLETE:
//                    System.out.println("Upload is complete!");
//            }
//        }
//    }
//
//    public void uploadFile() throws FileNotFoundException {
//        File mediaFile = new File("/tmp/driveFile.jpg");
//        InputStreamContent mediaContent = new InputStreamContent("image/jpeg", new BufferedInputStream(new FileInputStream(mediaFile)));
//        mediaContent.setLength(mediaFile.length());
//        Drive.Files.Insert request = drive.files().insert(fileMetadata, mediaContent);
//        request.getMediaHttpUploader().setProgressListener(new CustomProgressListener());
//        request.execute();
//    }
}
