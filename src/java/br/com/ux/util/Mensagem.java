/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Renato
 */
public class Mensagem implements Serializable {

    public static String addMensagem(int tipoMsg, String mensagem) {

        FacesContext fc = FacesContext.getCurrentInstance();
        if (tipoMsg == 1) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, null));
        }
        if (tipoMsg == 2) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, mensagem, null));
        }
        if (tipoMsg == 3) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, null));
        }
        if (tipoMsg == 4) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, mensagem, null));
        }
        return "";
    }

    public static String addMensagemGrowl(int tipoMsg, String mensagem) {

        FacesContext fc = FacesContext.getCurrentInstance();
        if (tipoMsg == 1) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação", mensagem));
        }
        if (tipoMsg == 2) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Alerta", mensagem));
        }
        if (tipoMsg == 3) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", mensagem));
        }
        if (tipoMsg == 4) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Falha", mensagem));
        }
        return "";
    }
    
    public static String addMensagemGrowl2(int tipoMsg, String mensagem) {

        FacesContext fc = FacesContext.getCurrentInstance();
        if (tipoMsg == 1) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem,"Informação"));
        }
        if (tipoMsg == 2) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, mensagem , "Alerta"));
        }
        if (tipoMsg == 3) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem , "Erro"));
        }
        if (tipoMsg == 4) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, mensagem, "Falha"));
        }
        return "";
    }

    public static String addMensagemPadraoSucesso(String tipoMsg) {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (tipoMsg.equalsIgnoreCase("salvar")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Salvo com sucesso!", null));
        }
        if (tipoMsg.equalsIgnoreCase("editar")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Editado com sucesso!", null));
        }
        if (tipoMsg.equalsIgnoreCase("excluir")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Excluído com sucesso!", null));
        }
        return "";
    }

    public static String addMensagemPadraoErro(String tipoMsg) {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (tipoMsg.equalsIgnoreCase("salvar")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao tentar salvar!", null));
        }
        if (tipoMsg.equalsIgnoreCase("editar")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao tentar editar!", null));
        }
        if (tipoMsg.equalsIgnoreCase("excluir")) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao tentar excluir!", null));
        }
        return "";
    }

    public static void addMensagemSucesso(String mensagem, Integer milisecond) {
        RequestContext.getCurrentInstance().execute("alertify.set({ delay: " + milisecond + " });");
        RequestContext.getCurrentInstance().execute("alertify.success('\"" + mensagem + " ')");
    }

    public static void addMensagemErro(String mensagem, Integer milisecond) {
        RequestContext.getCurrentInstance().execute("alertify.set({ delay: " + milisecond + " });");
        RequestContext.getCurrentInstance().execute("alertify.error('\"" + mensagem + " ')");
    }

    public static void addMensagemAviso(String mensagem, Integer milisecond) {
        RequestContext.getCurrentInstance().execute("alertify.set({ delay: " + milisecond + " });");
        RequestContext.getCurrentInstance().execute("alertify.log('\"" + mensagem + " ')");
    }
}
