/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Charles
 */
public class Configuracao {
    private static final String nomeArquivo = "C:\\SMA\\config.properties";
    private static Properties Config = new Properties();     
    
    public static Properties Carregar() {
        try {               
            File file = new File(nomeArquivo);  
            FileInputStream fileInputStream = new FileInputStream(file);
            Config.load(fileInputStream);
            fileInputStream.close();           
        } catch (Exception e) {
            Mensagem.addMensagem(3, e.getMessage());
        }
        return Config;
    }

    
}
