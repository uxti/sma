/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Renato
 */
public class Conexao {

    @PersistenceContext(unitName = "SMAPU")
    public EntityManager em;
    
    @PersistenceContext(unitName = "SMAPU2")
    public EntityManager emTasy;
   
    public static Connection conexaoJDBC() {
        Connection conexao = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if (getApplicationUri().contains("teste") || getApplicationUri().contains("TESTE") || getApplicationUri().contains("Teste")) {
                System.out.println("ta na base de teste");
                conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/sma_teste", "sma", "swordfish130823");
            } else {
                System.out.println("ta na base de produção");
                conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/sma", "sma", "swordfish130823");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        
        return conexao;
    }

    public static String getApplicationUri() {
        try {
            FacesContext ctxt = FacesContext.getCurrentInstance();
            ExternalContext ext = ctxt.getExternalContext();
            URI uri = new URI(ext.getRequestScheme(),
                    null, ext.getRequestServerName(), ext.getRequestServerPort(),
                    ext.getRequestContextPath(), null, null);
//            System.out.println(uri.toASCIIString());
            return uri.toASCIIString();
        } catch (URISyntaxException e) {
            throw new FacesException(e);
        }
    }

    public EntityManager getEm() {
        if (getApplicationUri().contains("teste") || getApplicationUri().contains("TESTE") || getApplicationUri().contains("Teste")) {
            return em;
        } else {
            return em;
        }
    }
    

    public EntityManager getEmTasy() {
        return emTasy;
    }

}
