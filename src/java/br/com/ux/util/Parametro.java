/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

/**
 *
 * @author Renato
 */
public class Parametro {
    private String campo;
    private String parametro;
    private String valorParametroPassado;
    private String tipoPesquisa;

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getTipoPesquisa() {
        return tipoPesquisa;
    }

    public void setTipoPesquisa(String tipoPesquisa) {
        this.tipoPesquisa = tipoPesquisa;
    }

    public String getValorParametroPassado() {
        return valorParametroPassado;
    }

    public void setValorParametroPassado(String valorParametroPassado) {
        this.valorParametroPassado = valorParametroPassado;
    }
    
    
    
    
    
}
