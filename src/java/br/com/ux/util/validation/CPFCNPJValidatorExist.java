/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util.validation;

import br.com.ux.controller.TerceiroEJB;
import java.util.InputMismatchException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Renato
 */
@FacesValidator("cpfcnpjExistValidation")
@Stateless
public class CPFCNPJValidatorExist implements Validator {

    @EJB
    TerceiroEJB tEJB;

    @Override
    public void validate(FacesContext facesContext, UIComponent component, Object value)
            throws ValidatorException {

        String urlValue = value.toString();
        String parametro = (String) component.getAttributes().get("param");
        if (parametro.equals("CPF")) {
            if (!isCPF(urlValue)) {
                FacesMessage msg =
                        new FacesMessage("CPF Inválido!");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            } else {
            }
        } else {
            if (!isCNPJ(urlValue)) {
                FacesMessage msg =
                        new FacesMessage("CNPJ Inválido!");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            } else {
            }
        }
//        Integer parametroID = (Integer) component.getAttributes().get("paramID");
//        if (parametroID != null) {
//            try {
//                Integer resultado = 0;
//                Statement stmt = Conexao.conexaoJDBC().createStatement();
//                String sql = "select count(*) countCGCCPF from terceiro where id_terceiro = '" + parametroID + "' and cgccpf = '" + urlValue + "' ;";
//                System.out.println(sql);
//                ResultSet rs = stmt.executeQuery(sql);
//                while (rs.next()) {
//                    resultado = rs.getInt("countCGCCPF");
//                }
//                if (resultado != 0) {
//                    if (parametro.equals("CPF")) {
//                        if (!isCPF(urlValue)) {
//                            FacesMessage msg =
//                                    new FacesMessage("CPF Inválido!");
//                            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                            throw new ValidatorException(msg);
//                        } else {
//                            try {
//                                Integer total1 = 0;
//                                Statement stmt1 = Conexao.conexaoJDBC().createStatement();
//                                String sql1 = "select count(*) total from terceiro where CGCCPF = '" + urlValue + "';";
//                                ResultSet rs1 = stmt1.executeQuery(sql1);
//                                while (rs1.next()) {
//                                    total1 = rs1.getInt("total");
//                                }
//                                rs1.close();
//                                stmt1.close();
//                                if (total1 > 0) {
//                                    FacesMessage msg =
//                                            new FacesMessage("CPF está sendo usado!");
//                                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                                    throw new ValidatorException(msg);
//                                }
//                            } catch (SQLException ex) {
//                                Logger.getLogger(CPFCNPJValidatorExist.class.getName()).log(Level.SEVERE, null, ex);
//                            }
//                        }
//                    } else {
//                        if (!isCNPJ(urlValue)) {
//                            FacesMessage msg =
//                                    new FacesMessage("CNPJ Inválido!");
//                            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                            throw new ValidatorException(msg);
//                        } else {
//                            try {
//                                Integer total2 = 0;
//                                Statement stmt2 = Conexao.conexaoJDBC().createStatement();
//                                String sql2 = "select count(*) total from terceiro where CGCCPF = '" + urlValue + "';";
//                                ResultSet rs2 = stmt2.executeQuery(sql2);
//                                while (rs2.next()) {
//                                    total2 = rs2.getInt("total");
//                                }
//                                rs2.close();
//                                stmt2.close();
//                                if (total2 > 0) {
//                                    FacesMessage msg =
//                                            new FacesMessage("CNPJ está sendo usado!");
//                                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                                    throw new ValidatorException(msg);
//                                }
//                            } catch (SQLException ex) {
//                                Logger.getLogger(CPFCNPJValidatorExist.class.getName()).log(Level.SEVERE, null, ex);
//                            }
//                        }
//
//                    }
//                }
//                rs.close();
//                stmt.close();
//            } catch (SQLException ex) {
//                Logger.getLogger(CPFCNPJValidatorExist.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } else {
//            if (parametro.equals("CPF")) {
//                if (!isCPF(urlValue)) {
//                    FacesMessage msg =
//                            new FacesMessage("CPF Inválido!");
//                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                    throw new ValidatorException(msg);
//                } else {
//                    try {
//                        Integer total1 = 0;
//                        Statement stmt1 = Conexao.conexaoJDBC().createStatement();
//                        String sql1 = "select count(*) total from terceiro where CGCCPF = '" + urlValue + "';";
//                        ResultSet rs1 = stmt1.executeQuery(sql1);
//                        while (rs1.next()) {
//                            total1 = rs1.getInt("total");
//                        }
//                        rs1.close();
//                        stmt1.close();
//                        if (total1 > 0) {
//                            FacesMessage msg =
//                                    new FacesMessage("CPF está sendo usado!");
//                            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                            throw new ValidatorException(msg);
//                        }
//                    } catch (SQLException ex) {
//                        Logger.getLogger(CPFCNPJValidatorExist.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            } else {
//                if (!isCNPJ(urlValue)) {
//                    FacesMessage msg =
//                            new FacesMessage("CNPJ Inválido!");
//                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                    throw new ValidatorException(msg);
//                } else {
//                    try {
//                        Integer total2 = 0;
//                        Statement stmt2 = Conexao.conexaoJDBC().createStatement();
//                        String sql2 = "select count(*) total from terceiro where CGCCPF = '" + urlValue + "';";
//                        ResultSet rs2 = stmt2.executeQuery(sql2);
//                        while (rs2.next()) {
//                            total2 = rs2.getInt("total");
//                        }
//                        rs2.close();
//                        stmt2.close();
//                        if (total2 > 0) {
//                            FacesMessage msg =
//                                    new FacesMessage("CNPJ está sendo usado!");
//                            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                            throw new ValidatorException(msg);
//                        }
//                    } catch (SQLException ex) {
//                        Logger.getLogger(CPFCNPJValidatorExist.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//
//            }
//        }

    }

    public boolean isCNPJ(String CNPJ) {
        if (CNPJ.equals("") || CNPJ == null) {
            CNPJ = "";
        } else {
            CNPJ = CNPJ.replace(".", "").replace("-", "").replace("/", "");
        }

        // considera-se erro CNPJ's formados por uma sequencia de numeros iguais 
        if (CNPJ.equals("00000000000000")
                || CNPJ.equals("11111111111111")
                || CNPJ.equals("22222222222222")
                || CNPJ.equals("33333333333333")
                || CNPJ.equals("44444444444444")
                || CNPJ.equals("55555555555555")
                || CNPJ.equals("66666666666666")
                || CNPJ.equals("77777777777777")
                || CNPJ.equals("88888888888888")
                || CNPJ.equals("99999999999999")
                || (CNPJ.length() != 14)) {
            return (false);
        }
        char dig13, dig14;
        int sm, i, r, num, peso;
        // "try" - protege o código para eventuais erros de conversao de tipo (int) 
        try {
            // Calculo do 1o. Digito Verificador 
            sm = 0;
            peso = 2;
            for (i = 11; i >= 0; i--) {
                // converte o i-ésimo caractere do CNPJ em um número: 
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posição de '0' na tabela ASCII) 
                num = (int) (CNPJ.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10) {
                    peso = 2;
                }
            }
            r = sm % 11;
            if ((r == 0) || (r == 1)) {
                dig13 = '0';
            } else {
                dig13 = (char) ((11 - r) + 48);
            }
            // Calculo do 2o. Digito Verificador 
            sm = 0;
            peso = 2;
            for (i = 12; i >= 0; i--) {
                num = (int) (CNPJ.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10) {
                    peso = 2;
                }
            }
            r = sm % 11;
            if ((r == 0) || (r == 1)) {
                dig14 = '0';
            } else {
                dig14 = (char) ((11 - r) + 48);
            }
            // Verifica se os dígitos calculados conferem com os dígitos informados. 
            if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13))) {
                return (true);
            } else {
                return (false);
            }
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    public boolean isCPF(String CPF) {
        if (CPF == null) {
            CPF = "";
        } else {
            CPF = CPF.replace(".", "").replace("-", "");
        }
        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") || CPF.equals("11111111111") || CPF.equals("22222222222") || CPF.equals("33333333333") || CPF.equals("44444444444") || CPF.equals("55555555555") || CPF.equals("66666666666") || CPF.equals("77777777777") || CPF.equals("88888888888") || CPF.equals("99999999999") || (CPF.length() != 11)) {
            return (false);
        }
        char dig10, dig11;
        int sm, i, r, num, peso;
        // "try" - protege o codigo para eventuais erros de conversao de tipo (int) 
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0 
                // (48 eh a posicao de '0' na tabela ASCII) 
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig10 = '0';
            } else {
                dig10 = (char) (r + 48);
            }
            // converte no respectivo caractere numerico 
            // Calculo do 2o. Digito Verificador 
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig11 = '0';
            } else {
                dig11 = (char) (r + 48);
            }
            // Verifica se os digitos calculados conferem com os digitos informados. 
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10))) {
                return (true);
            } else {
                return (false);
            }
        } catch (InputMismatchException erro) {
            return (false);
        }
    }
}
