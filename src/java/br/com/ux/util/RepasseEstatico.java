/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import br.com.ux.model.Cheque;
import br.com.ux.model.Repasse;
import java.math.BigInteger;

/**
 *
 * @author Charles
 */
public class RepasseEstatico {
    
    private BigInteger Parcela;
    private Integer idRepasse;
    private Integer idCheque;

    public BigInteger getParcela() {
        return Parcela;
    }

    public void setParcela(BigInteger Parcela) {
        this.Parcela = Parcela;
    }

    public Integer getIdRepasse() {
        return idRepasse;
    }

    public void setIdRepasse(Integer idRepasse) {
        this.idRepasse = idRepasse;
    }

    public Integer getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Integer idCheque) {
        this.idCheque = idCheque;
    }
        
}
