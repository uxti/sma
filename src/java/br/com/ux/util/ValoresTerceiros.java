/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import br.com.ux.model.ServicoItemCirurgiaItem;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Renato
 */
public class ValoresTerceiros {
    private Integer idTerceiro;
    private String terceiro;
    private Double valor;
    private Double valorPago;
    private Double valorRestante;
    
    private List<ServicoItemCirurgiaItem> itensPorTerceiros = new ArrayList<ServicoItemCirurgiaItem>();


    public Integer getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Integer idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public String getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(String terceiro) {
        this.terceiro = terceiro;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<ServicoItemCirurgiaItem> getItensPorTerceiros() {
        return itensPorTerceiros;
    }

    public void setItensPorTerceiros(List<ServicoItemCirurgiaItem> itensPorTerceiros) {
        this.itensPorTerceiros = itensPorTerceiros;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public Double getValorRestante() {
        return valorRestante;
    }

    public void setValorRestante(Double valorRestante) {
        this.valorRestante = valorRestante;
    }
}
