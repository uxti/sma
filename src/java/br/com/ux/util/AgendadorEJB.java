/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import br.com.ux.controller.AssociacaoEJB;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Schedules;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author charles
 */
@Stateless
public class AgendadorEJB {

    private String caminhoDump, caminhoBat, caminhoMySQL, destino, nomeArquivo, nomeArquivoCompactado;
    private String cmd, cmd2;
    private String tablename;
    private String usuario = "sma";
    private String senha = "swordfish130823";
    private String BD = "sma";
    private String caminhoCSV;
    private UploadedFile file;
    private boolean botao;
    private static int TAMANHO_BUFFER = 4096;
    private Date dtUltimoBkp = new Date();
    Timer timer;

    @EJB
    AssociacaoEJB assEJB;
//    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

//    @Schedules({
//        @Schedule(dayOfWeek = "*", hour = "11", minute = "42", second = "1"),
//        @Schedule(dayOfWeek = "*", hour = "18", minute = "0", second = "1"),
//    })
    public void backupAutomatico() {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_HHmmss");
        criarPastas();
        nomeArquivo = destino + "Backups\\" + getBD() + "_" + dateFormat.format(now);
        nomeArquivoCompactado = destino + "Backups\\" + getBD() + "_" + dateFormat.format(now) + ".zip";
        cmd = getCaminhoMySQL() + "mysqldump -u" + usuario + " --default-character-set=latin1" + " -p" + senha + " -x " + getBD() + " -r " + nomeArquivo + ".sql";
        //System.out.println(cmd);
        try {
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            compactar();
            assEJB.atualizarDataUltimoBkp();
        } catch (IOException | InterruptedException e) {
            System.out.println(e);
        }
    }

    public void importarDados() throws SQLException, IOException {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sma", "sma", "swordfish130823");
            Statement stmt = connection.createStatement();

            if (file != null) {
                setCaminhoCSV(file.getFileName());
            }
            System.out.println(caminhoCSV.replace("\\", "\\\\"));
            stmt.executeUpdate("LOAD DATA INFILE \"" + caminhoCSV.replace("\\", "\\\\") + "\" INTO TABLE "
                    + getTablename() + " FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n'");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, getTablename().toUpperCase() + " importado. Importação concluída.", null));
            botao = true;
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao tentar fazer o backup!");
            System.out.println(e);
        }
    }

    public void compactar() {
        try {
            compactarParaZip(nomeArquivoCompactado, nomeArquivo + ".sql");
            apagarArquivo(nomeArquivo + ".sql");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void apagarArquivo(String caminho) {
        try {
            File f = new File(caminho);
            f.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void compactarParaZip(String arqSaida, String arqEntrada) throws IOException {
        int cont;
        byte[] dados = new byte[TAMANHO_BUFFER];
        BufferedInputStream origem = null;
        FileInputStream streamDeEntrada = null;
        FileOutputStream destino = null;
        ZipOutputStream saida = null;
        ZipEntry entry = null;
        try {
            destino = new FileOutputStream(new File(arqSaida));
            saida = new ZipOutputStream(new BufferedOutputStream(destino));
            File file = new File(arqEntrada);
            streamDeEntrada = new FileInputStream(file);
            origem = new BufferedInputStream(streamDeEntrada, TAMANHO_BUFFER);
            entry = new ZipEntry(file.getName());
            saida.putNextEntry(entry);
            while ((cont = origem.read(dados, 0, TAMANHO_BUFFER)) != -1) {
                saida.write(dados, 0, cont);
            }
            origem.close();
            saida.close();
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public void criarPastas() {
        try {
            File diretorio = new File(getDestino());
            File subDiretorio, subDiretorio2;
            if (!diretorio.exists()) {
                boolean success = diretorio.mkdir();
                subDiretorio = new File(getDestino() + "\\Backups");
                if (!subDiretorio.exists()) {
                    success = subDiretorio.mkdir();
                }
                subDiretorio2 = new File(getDestino() + "\\Temp");
                if (!subDiretorio2.exists()) {
                    success = subDiretorio2.mkdir();
                }
                System.out.println(success);
            }
        } catch (Exception e) {
            Mensagem.addMensagem(3, "Erro ao criar a pasta");
        }

    }

    @PostConstruct
    public void carregarDados() {
        //caminhoBat = "D:/SMA/BKPPARAMETRIZADO.bat ";
        //caminhoDump = "D:/xampp/mysql/bin/mysqldump ";
        caminhoMySQL = "C:\\Program Files (x86)\\MySQL\\MySQL Server 5.5\\bin\\";
        destino = "C:\\SMA\\";
        if (assEJB.retornaDataUltimoBkp() != null) {
            dtUltimoBkp = assEJB.retornaDataUltimoBkp();
        }
        botao = false;
    }

    public String getCaminhoDump() {
        return caminhoDump;
    }

    public void setCaminhoDump(String caminhoDump) {
        this.caminhoDump = caminhoDump;
    }

    public String getCaminhoBat() {
        return caminhoBat;
    }

    public void setCaminhoBat(String caminhoBat) {
        this.caminhoBat = caminhoBat;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getCaminhoMySQL() {
        return caminhoMySQL;
    }

    public void setCaminhoMySQL(String caminhoMySQL) {
        this.caminhoMySQL = caminhoMySQL;
    }

    public String getBD() {
        return BD;
    }

    public void setBD(String BD) {
        this.BD = BD;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCaminhoCSV() {
        return caminhoCSV;
    }

    public void setCaminhoCSV(String caminhoCSV) {
        this.caminhoCSV = caminhoCSV;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getNomeArquivoCompactado() {
        return nomeArquivoCompactado;
    }

    public void setNomeArquivoCompactado(String nomeArquivoCompactado) {
        this.nomeArquivoCompactado = nomeArquivoCompactado;
    }

    public static int getTAMANHO_BUFFER() {
        return TAMANHO_BUFFER;
    }

    public static void setTAMANHO_BUFFER(int TAMANHO_BUFFER) {
        AgendadorEJB.TAMANHO_BUFFER = TAMANHO_BUFFER;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getCmd2() {
        return cmd2;
    }

    public void setCmd2(String cmd2) {
        this.cmd2 = cmd2;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isBotao() {
        return botao;
    }

    public void setBotao(boolean botao) {
        this.botao = botao;
    }

    public Date getDtUltimoBkp() {
        return dtUltimoBkp;
    }

    public void setDtUltimoBkp(Date dtUltimoBkp) {
        this.dtUltimoBkp = dtUltimoBkp;
    }

    TimerTask t = new TimerTask() {

        @Override
        public void run() {
            System.out.println("Backup Automatico");
            try {
                backupAutomatico();
            } catch (Exception e) {
                System.out.println("Não atualizou a data do ultimo bkp.");
            }

        }
    };
    public static final long TEMPO = ((60000 * 60) * 8);

    public void iniciar() {
        timer = new Timer();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date time = calendar.getTime();
        timer.schedule(t, time, TEMPO);
    }

    public void reiniciarServidor() throws InterruptedException, IOException {
        try {
            System.out.println(assEJB.carregarAssociacao().getCaminhoBat());
            Process p = Runtime.getRuntime().exec(assEJB.carregarAssociacao().getCaminhoBat());
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            System.out.println(e);
        }

    }

}
