/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

/**
 *
 * @author Renato
 */
public class FiltrosRepasse {
    private Integer idRepasse;
    private Integer idPaciente;
    private Integer idCirurgia;

    public Integer getIdRepasse() {
        return idRepasse;
    }

    public void setIdRepasse(Integer idRepasse) {
        this.idRepasse = idRepasse;
    }

    public Integer getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Integer getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Integer idCirurgia) {
        this.idCirurgia = idCirurgia;
    }
    
    
    
    
}
