/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import br.com.ux.model.Cheque;
import br.com.ux.model.Cirurgia;
import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.ContaCorrente;
import br.com.ux.model.Paciente;
import br.com.ux.model.Procedimento;
import java.util.Date;

/**
 *
 * @author Renato
 */
public class ContaCorrenteEstatica {

    private Date dtProcedimento, dtPrevisao, dtVencimento, dtEstorno, dtRetirada;
    private Cirurgia idAtendimento;
    private Paciente paciente;
    private ContaCorrente contaCorrente;
    private Procedimento procedimento;
    private String funcao, hospital, tipo, tipoInternacao, status, situacao;
    private Double valor, valorSemDesconto;
    private Integer idFuncao;
    private String numeroParcela, totalParcelas;
    private Cheque pagamento;
    private CirurgiaItem item;

    public Date getDtProcedimento() {
        return dtProcedimento;
    }

    public void setDtProcedimento(Date dtProcedimento) {
        this.dtProcedimento = dtProcedimento;
    }

    public Cirurgia getIdAtendimento() {
        return idAtendimento;
    }

    public void setIdAtendimento(Cirurgia idAtendimento) {
        this.idAtendimento = idAtendimento;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getIdFuncao() {
        return idFuncao;
    }

    public void setIdFuncao(Integer idFuncao) {
        this.idFuncao = idFuncao;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getTipoInternacao() {
        return tipoInternacao;
    }

    public void setTipoInternacao(String tipoInternacao) {
        this.tipoInternacao = tipoInternacao;
    }

    public String getNumeroParcela() {
        return numeroParcela;
    }

    public void setNumeroParcela(String numeroParcela) {
        this.numeroParcela = numeroParcela;
    }

    public String getTotalParcelas() {
        return totalParcelas;
    }

    public void setTotalParcelas(String totalParcelas) {
        this.totalParcelas = totalParcelas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Date getDtPrevisao() {
        return dtPrevisao;
    }

    public void setDtPrevisao(Date dtPrevisao) {
        this.dtPrevisao = dtPrevisao;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Date getDtEstorno() {
        return dtEstorno;
    }

    public void setDtEstorno(Date dtEstorno) {
        this.dtEstorno = dtEstorno;
    }

    public Date getDtRetirada() {
        return dtRetirada;
    }

    public void setDtRetirada(Date dtRetirada) {
        this.dtRetirada = dtRetirada;
    }

    public Cheque getPagamento() {
        return pagamento;
    }

    public void setPagamento(Cheque pagamento) {
        this.pagamento = pagamento;
    }

    public Double getValorSemDesconto() {
        return valorSemDesconto;
    }

    public void setValorSemDesconto(Double valorSemDesconto) {
        this.valorSemDesconto = valorSemDesconto;
    }

    public CirurgiaItem getItem() {
        return item;
    }

    public void setItem(CirurgiaItem item) {
        this.item = item;
    }
    
    
    
    
    
    
            
}
