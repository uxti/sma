/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import br.com.ux.model.Cheque;
import br.com.ux.model.Repasse;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Renato
 */
public class Parcela implements Serializable {

    Integer numParcela;
    Double valorParcela;
    Date DataVencimento;
    String forma_pagamento;
    List<Cheque> listaChequeParcelas;
    List<Repasse> listaRepasses;
    String pago;

    public Integer getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(Integer numParcela) {
        this.numParcela = numParcela;
    }

    public Double getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(Double valorParcela) {
        this.valorParcela = valorParcela;
    }

    public Date getDataVencimento() {
        return DataVencimento;
    }

    public void setDataVencimento(Date DataVencimento) {
        this.DataVencimento = DataVencimento;
    }

    public String getPago() {
        return pago;
    }

    public void setPago(String pago) {
        this.pago = pago;
    }

    public List<Cheque> getListaChequeParcelas() {
        return listaChequeParcelas;
    }

    public void setListaChequeParcelas(List<Cheque> listaChequeParcelas) {
        this.listaChequeParcelas = listaChequeParcelas;
    }

    public List<Repasse> getListaRepasses() {
        return listaRepasses;
    }

    public void setListaRepasses(List<Repasse> listaRepasses) {
        this.listaRepasses = listaRepasses;
    }
    
    public String getForma_pagamento() {
        return forma_pagamento;
    }

    public void setForma_pagamento(String forma_pagamento) {
        this.forma_pagamento = forma_pagamento;
    }
}
