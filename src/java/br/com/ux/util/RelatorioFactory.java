/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.util;

import br.com.ux.model.Usuario;
import br.com.ux.pojo.Orcto;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/**
 *
 * @author Renato
 */
public class RelatorioFactory extends Conexao {

    private static Connection getConexao() {
        return Conexao.conexaoJDBC();
    }

    public static void RelatorioCliente(String relatorio) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("USUARIO", usuario);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void Relatorio(String relatorio, Map<String, Object> map) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioOrcamento(String relatorio, Integer id) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("ID_ORCAMENTO", id);
            map.put("logo", "C://SMA//logo.png");
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    public static void RelatorioOrcamentoPorTipo(String relatorio, Integer id) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("ID_ORCAMENTO", id);
            map.put("logo", "C://SMA//logo.png");
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void ComprovantePagamentoMedico(String relatorio, Integer idSaqueImprimir, String logo) {
        System.out.println(logo);
        System.out.println(idSaqueImprimir);

        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("idSaque", idSaqueImprimir);
            map.put("logo", logo);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }

//        try {
//            HttpServletResponse response = (HttpServletResponse) context
//                    .getExternalContext().getResponse();
//
//            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
//            response.setContentType("application/pdf");
//            ServletOutputStream servletOutputStream = response.getOutputStream();
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("idSaque", idSaqueImprimir);
//            map.put("logo", logo);
//            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
//            servletOutputStream.flush();
//            servletOutputStream.close();
//        } catch (JRException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            context.responseComplete();
//        }
    }

    public static void ComprovanteTransferencia(String relatorio, List<String> lote) {
        FacesContext context = FacesContext.getCurrentInstance();

        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();

            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);

            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename=ComprovanteTransferencia.pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("lote", lote);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void ComprovanteTransferenciaIndividual(String relatorio, List<Integer> listaCaixa, Double valorMed, Double valorHosp, String logo) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename=ComprovanteTransferencia.pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("listaCaixa", listaCaixa);
            map.put("pValorMed", valorMed);
            map.put("pValorHosp", valorHosp);
            map.put("logo", logo);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    public static void ComprovanteTransferenciaIndividualValores(String relatorio, List<Integer> listaCaixa, Double valorHosp, Double valorMed, String logo) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename=ComprovanteTransferencia.pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("listaCaixa", listaCaixa);
            map.put("pValorHosp", valorHosp);
            map.put("pValorMed", valorMed);
            map.put("logo", logo);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    public static void reciboPagamento(String relatorio, String logo, Double valor, String valorExtenso, String pagador, String medico, String cpf, String cpfPaciente) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("pValorExtenso", valorExtenso);
            map.put("pPagador", pagador);
            map.put("pValor", valor);
            map.put("pMedico", medico);
            map.put("pCPF", cpf);
            map.put("pCPFPaciente", cpfPaciente);
            map.put("logo", logo);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void ComprovanteTransferenciaDownload(String relatorio, List<String> lote) throws IOException, SQLException {

        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) facesContext
                    .getExternalContext().getResponse();
            InputStream relatorioStream = facesContext.getExternalContext().getResourceAsStream(relatorio);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("lote", lote);
            JasperPrint print = JasperFillManager.fillReport(relatorioStream, map, conexaoJDBC());

            JRExporter exportador = new JRPdfExporter();
            exportador.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
            exportador.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exportador.exportReport();

            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=Comprovante Transferencia.pdf");

        } catch (JRException ex) {
            Logger.getLogger(RelatorioFactory.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void ComprovanteTransferenciaDownload2(String relatorio, List<String> lote) throws IOException, SQLException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("lote", lote);
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.responseComplete();
            ServletContext scontext = (ServletContext) facesContext.getExternalContext().getContext();
            JasperPrint jasperPrint = JasperFillManager.fillReport(scontext.getRealPath(relatorio), map, conexaoJDBC());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
            exporter.exportReport();
            byte[] bytes = baos.toByteArray();
            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename=\"comprovanteTransferencia.pdf\"");
            response.getOutputStream().write(bytes);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ListagemTerceiro(String relatorio, String tipoPessoa, String especialidade, String usuario) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            // map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/report1DetailOrcto.jasper") + System.getProperty("file.separator"));
            map.put("TIPO_PESSOA", tipoPessoa);
            map.put("ESPECIALIDAE", especialidade);
            map.put("USUARIO", usuario);
            map.put("logo", "C://SMA//logo.png");
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void ListagemPaciente(String relatorio, String usuario, Date data_inicio_cadastro, Date data_fim_cadastro, Date data_inicio_nasc, Date data_fim_nasc) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            // map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/report1DetailOrcto.jasper") + System.getProperty("file.separator"));
            map.put("USUARIO", usuario);
            map.put("DATA_FIM_CADASTRO", data_fim_cadastro);
            map.put("DATA_INICIO_CADASTRO", data_inicio_cadastro);
            map.put("DATA_INICIO_NASC", data_inicio_nasc);
            map.put("DATA_FIM_NASC", data_fim_nasc);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void imprimirOrctosMedicos(String relatorio, Map<String, Object> map, List<Orcto> list) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            JRBeanCollectionDataSource fonteDados = new JRBeanCollectionDataSource(list);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, fonteDados);
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void atendimentosPorTerceiro(String relatorio, Integer id_cirurgia, Integer id_terceiro, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("ID_CIRURGIA", id_cirurgia);
            map.put("ID_TERCEIRO", id_terceiro);
            map.put("LOGO", caminho);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void atendimentos(String relatorio, Integer idTerceiro, Date dtIni, Date dtFim, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("ID_CIRURGIA", "%");
            map.put("ID_TERCEIRO", idTerceiro);
            map.put("dtInicio", dtIni);
            map.put("dtFinal", dtFim);
            map.put("LOGO", caminho);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void atendimentosResumo(String relatorio, Integer idTerceiro, String paciente, Date dtIni, Date dtFim, String caminho, String status) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("ID_CIRURGIA", "%");
            map.put("ID_TERCEIRO", idTerceiro);
            map.put("dtInicio", dtIni);
            map.put("dtFinal", dtFim);
            map.put("LOGO", caminho);
            map.put("status", status);
            map.put("pPaciente", paciente);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void extratosPorTerceiro(String relatorio, String id_cirurgia,
            String id_paciente, String id_terceiro, String tipo_atendimento,
            Date data_inicio, Date data_fim, String id_procedimento, String id_servico_item, String status, String situacao, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("ID_TERCEIRO", id_terceiro);
            map.put("ID_CIRURGIA", id_cirurgia);
            map.put("ID_SERVICO_ITEM", id_servico_item);
            map.put("ID_PACIENTE", id_paciente);
            map.put("ID_PROCEDIMENTO", id_procedimento);
            map.put("STATUS", status);
            map.put("SITUACAO", situacao);
            map.put("DATA_INICIO", data_inicio);
            map.put("DATA_FIM", data_fim);
            map.put("LOGO", caminho);
            System.out.println(data_inicio);
            map.put("pini", data_inicio);
            System.out.println(data_fim);
            map.put("pfim", data_fim);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void internacao(String relatorio, Integer id_cirurgia, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("ID_CIRURGIA", id_cirurgia);
            map.put("LOGO", caminho);

            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    public static void recibo(String relatorio, Integer idPaciente, Double valorNumerico, String valorExtenso, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("pPaciente", idPaciente);
            map.put("pValorNumerico", valorNumerico);
            map.put("pValorExtenso", valorExtenso);
            map.put("LOGO", caminho);

            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    public static void reciboExterno(String relatorio, Integer idAtendimentoPaciente, String observacao, Double valorMed, Double valorHosp, String procedimento, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("pAtendimentoPaciente", idAtendimentoPaciente);
            map.put("pValorMed", valorMed);
            map.put("pValorHosp", valorHosp);
            map.put("pProcedimento", procedimento);
            map.put("pObservacao", observacao);
            map.put("LOGO", caminho);

            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    public static void reciboAvulso(String relatorio, Integer idPaciente, Integer idMedico, String observacao, Double valorMed, Double valorHosp, String procedimento, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("pIdPaciente", idPaciente);
            map.put("pIdMedico", idMedico);
            map.put("pValorMed", valorMed);
            map.put("pValorHosp", valorHosp);
            map.put("pProcedimento", procedimento);
            map.put("pObservacao", observacao);
            map.put("LOGO", caminho);

            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    

    public static void listagemCheques(String relatorio, String usuario, Date DT_INI_EMISSAO, Date DT_FIM_EMISSAO, Date DT_INI_VENC,
            Date DT_FIM_VENC, String id_paciente, String id_banco, String tipo, String compensado, String id_terceiro, String caminho) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("USUARIO", usuario);
            map.put("LOGO", caminho);
            map.put("INI_DT_RECEBIMENTO_EMISSAO", DT_INI_EMISSAO);
            map.put("FIM_DT_RECEBIMENTO_EMISSAO", DT_FIM_EMISSAO);
            map.put("INI_DT_COMPENSACAO_VENCIMENTO", DT_INI_VENC);
            map.put("FIM_DT_COMPENSACAO_VENCIMENTO", DT_FIM_VENC);
            map.put("ID_PACIENTE", id_paciente);
            map.put("ID_BANCO", id_banco);
            map.put("TIPO", tipo);
            map.put("COMPENSADO", compensado);
            map.put("ID_TERCEIRO", id_terceiro);

            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioListagemOrcamentoPorAgrupamento(String relatorio, String caminho, Date dtInicio, Date dtFinal, Integer Status, Integer procedimento,
            Integer terceiro, String tipoData) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
//                System.out.println(caminho);
//                System.out.println(idSituacao);
//                System.out.println(dtInicio);
//                System.out.println(dtFinal);
//                System.out.println(Status);
//                System.out.println(procedimento);
//                System.out.println(terceiro);
//                System.out.println(adiantamento);
//                System.out.println(semFechamento);
                map.put("logo", caminho);
                map.put("pData_Inicio", dtInicio);
                map.put("pData_Fim", dtFinal);
                map.put("pStatus", Status);
                map.put("pProcedimento", procedimento);
                map.put("pTerceiro", terceiro);
                map.put("pTipoData", tipoData);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    public static void RelatorioReciboAdiantamento(String relatorio, String caminho, Integer idOrcamento, String valorExtenso) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
//                System.out.println(caminho);
//                System.out.println(idSituacao);
//                System.out.println(dtInicio);
//                System.out.println(dtFinal);
//                System.out.println(Status);
//                System.out.println(procedimento);
//                System.out.println(terceiro);
//                System.out.println(adiantamento);
//                System.out.println(semFechamento);
                map.put("logo", caminho);
                map.put("pIdOrcamento", idOrcamento);
                map.put("pValorExtenso", valorExtenso);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    

    public static void RelatorioListagemOrcamento(String relatorio, String caminho, String idSituacao, Date dtInicio, Date dtFinal, Integer Status, Integer procedimento,
            Integer terceiro, Integer paciente, String adiantamento, String semFechamento, String tipoData) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
                map.put("logo", caminho);
                map.put("pID_Situacao", idSituacao);
                map.put("pData_Inicio", dtInicio);
                map.put("pData_Fim", dtFinal);
                map.put("pStatus", Status);
                map.put("pProcedimento", procedimento);
                map.put("pTerceiro", terceiro);
                map.put("pPaciente", paciente);
                map.put("pAdiantamento", adiantamento);
                map.put("pSomenteFechamento", semFechamento);
                map.put("pTipoData", tipoData);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioListagemOrcamentoHora(String relatorio, String caminho, String idSituacao, Date dtInicio, Date dtFinal, Integer Status, Integer procedimento,
            Integer terceiro, Integer paciente, String adiantamento, String semFechamento, String tipoData, String horaInicial, String horaFinal) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
                map.put("logo", caminho);
                map.put("pID_Situacao", idSituacao);
                map.put("pData_Inicio", dtInicio);
                map.put("pData_Fim", dtFinal);
                map.put("pStatus", Status);
                map.put("pProcedimento", procedimento);
                map.put("pTerceiro", terceiro);
                map.put("pPaciente", paciente);
                map.put("pAdiantamento", adiantamento);
                map.put("pSomenteFechamento", semFechamento);
                map.put("pTipoData", tipoData);
                map.put("pHoraInicial", horaInicial);
                map.put("pHoraFinal", horaFinal);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioListagemOrcamentoLista(String relatorio, String caminho, String idSituacao, Date dtInicio, Date dtFinal, Integer Status, Integer procedimento,
            Integer terceiro, Integer paciente, String adiantamento, String semFechamento, List<String> listaOrcamentos) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
//                System.out.println(caminho);
//                System.out.println(idSituacao);
//                System.out.println(dtInicio);
//                System.out.println(dtFinal);
//                System.out.println(Status);
//                System.out.println(procedimento);
//                System.out.println(terceiro);
//                System.out.println(adiantamento);
//                System.out.println(semFechamento);
//                System.out.println(listaOrcamentos.size());
                map.put("logo", caminho);
                map.put("pID_Situacao", idSituacao);
                map.put("pData_Inicio", dtInicio);
                map.put("pData_Fim", dtFinal);
                map.put("pStatus", Status);
                map.put("pTerceiro", terceiro);
                map.put("pProcedimento", procedimento);
                map.put("pPaciente", paciente);
                map.put("pSemFechamentos", semFechamento);
                map.put("listaOrcamentos", listaOrcamentos);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioListagemFechamento(String relatorio, String caminho, Date dtInicio, Date dtFinal, String pStatus,
            Integer idProcedimento, Integer idTerceiro, Integer idPacientePesquisa, String idServico) {
        if (idProcedimento == 0) {
            idProcedimento = null;
        }
        if (idTerceiro == 0) {
            idTerceiro = null;
        }

        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
                map.put("logo", caminho);
                map.put("pData_Inicio", dtInicio);
                map.put("pData_Fim", dtFinal);
                map.put("pStatus", pStatus);
                map.put("pProcedimento", idProcedimento);
                map.put("pTerceiro", idTerceiro);
                map.put("pPaciente", idPacientePesquisa);
                map.put("pServico", idServico);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioListagemFechamentoResumo(String relatorio, String caminho, Date dtInicio, Date dtFinal, Integer idProcedimento, Integer idTerceiro) {
        if (idProcedimento == 0) {
            idProcedimento = null;
        }
        if (idTerceiro == 0) {
            idTerceiro = null;
        }

        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
                map.put("logo", caminho);
                map.put("pDataInicial", dtInicio);
                map.put("pDataFinal", dtFinal);
                map.put("pProcedimento", idProcedimento);
                map.put("pTerceiro", idTerceiro);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioListagemAvulsos(String relatorio, String caminho, Date dtInicio, Date dtFinal, String pStatus, Integer idProcedimento, Integer idTerceiro, Integer idUsuario, Integer pago, List<String> lista, boolean Observacao) {
        if (idProcedimento == 0) {
            idProcedimento = null;
        }
        if (idTerceiro == 0) {
            idTerceiro = null;
        }
        if (idUsuario == 0) {
            idUsuario = null;
        }

        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
                map.put("logo", caminho);
                map.put("pData_Inicio", dtInicio);
                map.put("pData_Fim", dtFinal);
                map.put("pStatus", pStatus);
                map.put("pProcedimento", idProcedimento);
                map.put("pTerceiro", idTerceiro);
                map.put("pUsuario", idUsuario);
                map.put("pPago", pago);
                map.put("listaUsuarios", lista);
                map.put("pObservacao", Observacao);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioListagemLaudos(String relatorio, String caminho, Date dtInicio, Date dtFinal,
            Integer idProcedimento, Integer idTerceiro, Integer idTerceiroLaudo) {
        if (idProcedimento == 0) {
            idProcedimento = null;
        }
        if (idTerceiro == 0) {
            idTerceiro = null;
        }
        
        if (idTerceiroLaudo == 0) {
            idTerceiroLaudo = null;
        }

        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                Map<String, Object> map = new HashMap<>();
                map.put("logo", caminho);
                map.put("pData_Inicio", dtInicio);
                map.put("pData_Fim", dtFinal);
                map.put("pProcedimento", idProcedimento);
                map.put("pTerceiro", idTerceiro);
                map.put("pTerceiroLaudo", idTerceiroLaudo);
                JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
                servletOutputStream.flush();
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioDashboard(String relatorio, String caminho, Date dtInicio, Date dtFinal) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("pDataInicial", dtInicio);
            map.put("pDataFinal", dtFinal);
            map.put("pLogo", caminho);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
}
