/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "associacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Associacao.findAll", query = "SELECT a FROM Associacao a"),
    @NamedQuery(name = "Associacao.findByIdAssociacao", query = "SELECT a FROM Associacao a WHERE a.idAssociacao = :idAssociacao"),
    @NamedQuery(name = "Associacao.findByNome", query = "SELECT a FROM Associacao a WHERE a.nome = :nome"),
    @NamedQuery(name = "Associacao.findByTelefone", query = "SELECT a FROM Associacao a WHERE a.telefone = :telefone"),
    @NamedQuery(name = "Associacao.findByLogradouro", query = "SELECT a FROM Associacao a WHERE a.logradouro = :logradouro"),
    @NamedQuery(name = "Associacao.findByNumero", query = "SELECT a FROM Associacao a WHERE a.numero = :numero"),
    @NamedQuery(name = "Associacao.findByBairro", query = "SELECT a FROM Associacao a WHERE a.bairro = :bairro"),
    @NamedQuery(name = "Associacao.findByEstado", query = "SELECT a FROM Associacao a WHERE a.estado = :estado"),
    @NamedQuery(name = "Associacao.findByTema", query = "SELECT a FROM Associacao a WHERE a.tema = :tema"),
    @NamedQuery(name = "Associacao.findByCep", query = "SELECT a FROM Associacao a WHERE a.cep = :cep"),
    @NamedQuery(name = "Associacao.findByTipoLogradouro", query = "SELECT a FROM Associacao a WHERE a.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Associacao.findByCidade", query = "SELECT a FROM Associacao a WHERE a.cidade = :cidade")})
public class Associacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ASSOCIACAO")
    private Integer idAssociacao;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "TELEFONE")
    private String telefone;
    @Lob
    @Column(name = "LOGO")
    private byte[] logo;
    @Lob
    @Column(name = "caminho_bat")
    private String caminhoBat;
    @Lob
    @Column(name = "caminho_dump")
    private String caminhoDump;
    @Lob
    @Column(name = "destino")
    private String destino;
    @Column(name = "LOGRADOURO")
    private String logradouro;
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "BAIRRO")
    private String bairro;
    @Column(name = "ESTADO")
    private String estado;
    @Column(name = "tema")
    private String tema;
    @Column(name = "cep")
    private String cep;
    @Column(name = "tipo_logradouro")
    private String tipoLogradouro;
    @Column(name = "cidade")
    private String cidade;
    @Column(name = "dt_ultimo_bkp")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dtUltimoBkp;
    @Column(name = "diferenca_tolerada")
    private Double diferencaTolerada;
    @Column(name = "prazo_compensacao_cheque")
    private Integer prazoCompensacaoCheque;
    @Column(name = "dias_compensar_cheque_hospital")
    private Integer diasCompensarChequeHospital;
    @Column(name = "dias_compensar_cheque_auto")
    private Integer diasCompensarChequeAuto;
    @Column(name = "obs_orcamento")
    private String obsOrcamento;
    @Column(name = "base_teste")
    private Boolean baseTeste;
    @Column(name = "smtp_server")
    private String smtpServer;
    @Column(name = "email_padrao")
    private String emailPadrao;
    @Column(name = "senha_email_padrao")
    private String senhaEmailPadrao;
    @Column(name = "porta_smtp")
    private int portaSmtp;
    @Column(name = "sincronizar_tasy")
    private Boolean sincronizarTasy;
    @Column(name = "modo_demonstrativo")
    private Boolean modoDemonstrativo;
    @Column(name = "imprimir_recibo_atendimento_interno")
    private Boolean imprimirReciboAtendimentoInterno;
    @Column(name = "compensar_cheques_automatico")
    private Boolean compensarChequesAutomatico;
    @Column(name = "habilitar_parciais")
    private Boolean habilitarParciais;
    
    @Column(name = "ignorar_atendimentos_com_titulos")
    private Boolean ignorarAtendimentosComTitulos;
    
    @Column(name = "taxa_desconto_repasse_deposito")
    private Double taxaDescontoRepasseDeposito;
    
    @Column(name = "transferir_cheques_caixa_honorarios")
    private Boolean transferirChequesCaixaHonorarios;
    
    @Column(name = "permitir_pagamento_sem_transferir")
    private Boolean permitirPagamentoSemTransferir;
    
    @Column(name = "gerar_repasses_sugeridos")
    private Boolean gerarRepassesSugeridos;
    @Column(name = "compensar_repasses_hospital_ao_compensar_cheque")
    private Boolean compensarRepassesHospitalAoCompensarCheque;
    @Column(name = "transferir_fechamento_perfil")
    private Boolean transferirFechamentoPerfil;
    

    public Associacao() {
    }

    public Associacao(Integer idAssociacao) {
        this.idAssociacao = idAssociacao;
    }

    public Integer getIdAssociacao() {
        return idAssociacao;
    }

    public void setIdAssociacao(Integer idAssociacao) {
        this.idAssociacao = idAssociacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getCaminhoBat() {
        return caminhoBat;
    }

    public void setCaminhoBat(String caminhoBat) {
        this.caminhoBat = caminhoBat;
    }

    public String getCaminhoDump() {
        return caminhoDump;
    }

    public void setCaminhoDump(String caminhoDump) {
        this.caminhoDump = caminhoDump;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Date getDtUltimoBkp() {
        return dtUltimoBkp;
    }

    public void setDtUltimoBkp(Date dtUltimoBkp) {
        this.dtUltimoBkp = dtUltimoBkp;
    }

    public Double getDiferencaTolerada() {
        return diferencaTolerada;
    }

    public void setDiferencaTolerada(Double diferencaTolerada) {
        this.diferencaTolerada = diferencaTolerada;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAssociacao != null ? idAssociacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Associacao)) {
            return false;
        }
        Associacao other = (Associacao) object;
        if ((this.idAssociacao == null && other.idAssociacao != null) || (this.idAssociacao != null && !this.idAssociacao.equals(other.idAssociacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Associacao[ idAssociacao=" + idAssociacao + " ]";
    }

    public Integer getPrazoCompensacaoCheque() {
        return prazoCompensacaoCheque;
    }

    public void setPrazoCompensacaoCheque(Integer prazoCompensacaoCheque) {
        this.prazoCompensacaoCheque = prazoCompensacaoCheque;
    }

    public String getObsOrcamento() {
        return obsOrcamento;
    }

    public void setObsOrcamento(String obsOrcamento) {
        this.obsOrcamento = obsOrcamento;
    }

    public Boolean getBaseTeste() {
        return baseTeste;
    }

    public void setBaseTeste(Boolean baseTeste) {
        this.baseTeste = baseTeste;
    }   

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getEmailPadrao() {
        return emailPadrao;
    }

    public void setEmailPadrao(String emailPadrao) {
        this.emailPadrao = emailPadrao;
    }

    public String getSenhaEmailPadrao() {
        return senhaEmailPadrao;
    }

    public void setSenhaEmailPadrao(String senhaEmailPadrao) {
        this.senhaEmailPadrao = senhaEmailPadrao;
    }

    public int getPortaSmtp() {
        return portaSmtp;
    }

    public void setPortaSmtp(int portaSmtp) {
        this.portaSmtp = portaSmtp;
    }

    public Integer getDiasCompensarChequeHospital() {
        return diasCompensarChequeHospital;
    }

    public void setDiasCompensarChequeHospital(Integer diasCompensarChequeHospital) {
        this.diasCompensarChequeHospital = diasCompensarChequeHospital;
    }

    public Boolean getSincronizarTasy() {
        return sincronizarTasy;
    }

    public void setSincronizarTasy(Boolean sincronizarTasy) {
        this.sincronizarTasy = sincronizarTasy;
    }

    public Boolean getModoDemonstrativo() {
        return modoDemonstrativo;
    }

    public void setModoDemonstrativo(Boolean modoDemonstrativo) {
        this.modoDemonstrativo = modoDemonstrativo;
    }

    public Boolean getImprimirReciboAtendimentoInterno() {
        return imprimirReciboAtendimentoInterno;
    }

    public void setImprimirReciboAtendimentoInterno(Boolean imprimirReciboAtendimentoInterno) {
        this.imprimirReciboAtendimentoInterno = imprimirReciboAtendimentoInterno;
    }

    public Boolean getCompensarChequesAutomatico() {
        return compensarChequesAutomatico;
    }

    public void setCompensarChequesAutomatico(Boolean compensarChequesAutomatico) {
        this.compensarChequesAutomatico = compensarChequesAutomatico;
    }

    public Boolean getHabilitarParciais() {
        return habilitarParciais;
    }

    public void setHabilitarParciais(Boolean habilitarParciais) {
        this.habilitarParciais = habilitarParciais;
    }

    public Boolean getTransferirChequesCaixaHonorarios() {
        return transferirChequesCaixaHonorarios;
    }

    public void setTransferirChequesCaixaHonorarios(Boolean transferirChequesCaixaHonorarios) {
        this.transferirChequesCaixaHonorarios = transferirChequesCaixaHonorarios;
    }

    public Boolean getGerarRepassesSugeridos() {
        return gerarRepassesSugeridos;
    }

    public void setGerarRepassesSugeridos(Boolean gerarRepassesSugeridos) {
        this.gerarRepassesSugeridos = gerarRepassesSugeridos;
    }

    public Double getTaxaDescontoRepasseDeposito() {
        return taxaDescontoRepasseDeposito;
    }

    public void setTaxaDescontoRepasseDeposito(Double taxaDescontoRepasseDeposito) {
        this.taxaDescontoRepasseDeposito = taxaDescontoRepasseDeposito;
    }

    public Boolean getCompensarRepassesHospitalAoCompensarCheque() {
        return compensarRepassesHospitalAoCompensarCheque;
    }

    public void setCompensarRepassesHospitalAoCompensarCheque(Boolean compensarRepassesHospitalAoCompensarCheque) {
        this.compensarRepassesHospitalAoCompensarCheque = compensarRepassesHospitalAoCompensarCheque;
    }

    public Boolean getTransferirFechamentoPerfil() {
        return transferirFechamentoPerfil;
    }

    public void setTransferirFechamentoPerfil(Boolean transferirFechamentoPerfil) {
        this.transferirFechamentoPerfil = transferirFechamentoPerfil;
    }

    public Integer getDiasCompensarChequeAuto() {
        return diasCompensarChequeAuto;
    }

    public void setDiasCompensarChequeAuto(Integer diasCompensarChequeAuto) {
        this.diasCompensarChequeAuto = diasCompensarChequeAuto;
    }

    public Boolean getPermitirPagamentoSemTransferir() {
        return permitirPagamentoSemTransferir;
    }

    public void setPermitirPagamentoSemTransferir(Boolean permitirPagamentoSemTransferir) {
        this.permitirPagamentoSemTransferir = permitirPagamentoSemTransferir;
    }

    public Boolean getIgnorarAtendimentosComTitulos() {
        return ignorarAtendimentosComTitulos;
    }

    public void setIgnorarAtendimentosComTitulos(Boolean ignorarAtendimentosComTitulos) {
        this.ignorarAtendimentosComTitulos = ignorarAtendimentosComTitulos;
    }
}
