/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "servico_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServicoItem.findAll", query = "SELECT s FROM ServicoItem s"),
    @NamedQuery(name = "ServicoItem.findByIdServicoItem", query = "SELECT s FROM ServicoItem s WHERE s.idServicoItem = :idServicoItem"),
    @NamedQuery(name = "ServicoItem.findByDescritiva", query = "SELECT s FROM ServicoItem s WHERE s.descritiva = :descritiva"),
    @NamedQuery(name = "ServicoItem.findByOrdenar", query = "SELECT s FROM ServicoItem s WHERE s.ordenar = :ordenar"),
    @NamedQuery(name = "ServicoItem.findByBaseCalculo", query = "SELECT s FROM ServicoItem s WHERE s.baseCalculo = :baseCalculo"),
    @NamedQuery(name = "ServicoItem.findByPercentual", query = "SELECT s FROM ServicoItem s WHERE s.percentual = :percentual")})
public class ServicoItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SERVICO_ITEM")
    private Integer idServicoItem;
    @Column(name = "DESCRITIVA")
    private String descritiva;
    @Column(name = "ORDENAR")
    private Integer ordenar;
    @Column(name = "BASE_CALCULO")
    private Character baseCalculo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PERCENTUAL")
    private Double percentual;
    @JoinColumn(name = "ID_SERVICO", referencedColumnName = "ID_SERVICO")
    @ManyToOne
    private Servico idServico;
    @OneToMany(mappedBy = "idServicoItem")
    private List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList;

    public ServicoItem() {
    }

    public ServicoItem(Integer idServicoItem) {
        this.idServicoItem = idServicoItem;
    }

    public Integer getIdServicoItem() {
        return idServicoItem;
    }

    public void setIdServicoItem(Integer idServicoItem) {
        this.idServicoItem = idServicoItem;
    }

    public String getDescritiva() {
        return descritiva;
    }

    public void setDescritiva(String descritiva) {
        this.descritiva = descritiva;
    }

    public Integer getOrdenar() {
        return ordenar;
    }

    public void setOrdenar(Integer ordenar) {
        this.ordenar = ordenar;
    }

    public Character getBaseCalculo() {
        return baseCalculo;
    }

    public void setBaseCalculo(Character baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    public Double getPercentual() {
        return percentual;
    }

    public void setPercentual(Double percentual) {
        this.percentual = percentual;
    }

    public Servico getIdServico() {
        return idServico;
    }

    public void setIdServico(Servico idServico) {
        this.idServico = idServico;
    }

    @XmlTransient
    public List<ServicoItemCirurgiaItem> getServicoItemCirurgiaItemList() {
        return servicoItemCirurgiaItemList;
    }

    public void setServicoItemCirurgiaItemList(List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList) {
        this.servicoItemCirurgiaItemList = servicoItemCirurgiaItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idServicoItem != null ? idServicoItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServicoItem)) {
            return false;
        }
        ServicoItem other = (ServicoItem) object;
        if ((this.idServicoItem == null && other.idServicoItem != null) || (this.idServicoItem != null && !this.idServicoItem.equals(other.idServicoItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.ServicoItem[ idServicoItem=" + idServicoItem + " ]";
    }
    
}
