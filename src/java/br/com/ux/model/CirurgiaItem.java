/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.tasy.ProcedimentoPaciente;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "cirurgia_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CirurgiaItem.findAll", query = "SELECT c FROM CirurgiaItem c"),
    @NamedQuery(name = "CirurgiaItem.findByIdCirurgiaItem", query = "SELECT c FROM CirurgiaItem c WHERE c.idCirurgiaItem = :idCirurgiaItem"),
    @NamedQuery(name = "CirurgiaItem.findByQuantidade", query = "SELECT c FROM CirurgiaItem c WHERE c.quantidade = :quantidade"),
    @NamedQuery(name = "CirurgiaItem.findByValorUnitario", query = "SELECT c FROM CirurgiaItem c WHERE c.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "CirurgiaItem.findByValorTotal", query = "SELECT c FROM CirurgiaItem c WHERE c.valorTotal = :valorTotal"),
    @NamedQuery(name = "CirurgiaItem.findByCirurgiaidResponsavel", query = "SELECT c FROM CirurgiaItem c WHERE c.cirurgiaidResponsavel = :cirurgiaidResponsavel")})
public class CirurgiaItem implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CIRURGIA_ITEM")
    private Integer idCirurgiaItem;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "QUANTIDADE")
    private Double quantidade;
    @Column(name = "VALOR_UNITARIO")
    private Double valorUnitario;
    @Column(name = "VALOR_TOTAL")
    private Double valorTotal;
    @Column(name = "valor_tasy")
    private Double valorTasy;
    @Column(name = "valor_total_geral")
    private Double valorTotalGeral;
    @Column(name = "desconto")
    private Double desconto;
    @Lob
    @Column(name = "OBSERVACAO")
    private String observacao;
    @Column(name = "origem")
    private String origem;
    @Column(name = "CIRURGIAID_RESPONSAVEL")
    private Integer cirurgiaidResponsavel;
    
    @OneToMany(mappedBy = "idCirurgiaItem")
    private List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList;
    @OneToMany(mappedBy = "idCirurgiaItem")
    private List<ContaCorrente> contaCorrenteList;
    @OneToMany(mappedBy = "idCirurgiaItem")
    private List<Repasse> repasseList;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @JoinColumn(name = "ID_CIRURGIA", referencedColumnName = "ID_CIRURGIA")
    @ManyToOne
    private Cirurgia idCirurgia;
    @JoinColumn(name = "ID_SERVICO", referencedColumnName = "ID_SERVICO")
    @ManyToOne
    private Servico idServico;
    @JoinColumn(name = "id_procedimento_paciente", referencedColumnName = "id_procedimento_paciente")
    @ManyToOne
    private ProcedimentoPaciente idProcedimentoPaciente;
    @JoinColumn(name = "id_orcamento_item", referencedColumnName = "id_orcamento_item")
    @ManyToOne
    private OrcamentoItem idOrcamentoItem;
    

    public CirurgiaItem() {
    }

    public CirurgiaItem(Integer idCirurgiaItem) {
        this.idCirurgiaItem = idCirurgiaItem;
    }

    public Integer getIdCirurgiaItem() {
        return idCirurgiaItem;
    }

    public void setIdCirurgiaItem(Integer idCirurgiaItem) {
        this.idCirurgiaItem = idCirurgiaItem;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Integer getCirurgiaidResponsavel() {
        return cirurgiaidResponsavel;
    }

    public void setCirurgiaidResponsavel(Integer cirurgiaidResponsavel) {
        this.cirurgiaidResponsavel = cirurgiaidResponsavel;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }
    
    @XmlTransient
    public List<ServicoItemCirurgiaItem> getServicoItemCirurgiaItemList() {
        return servicoItemCirurgiaItemList;
    }

    public void setServicoItemCirurgiaItemList(List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList) {
        this.servicoItemCirurgiaItemList = servicoItemCirurgiaItemList;
    }

    public Double getValorTasy() {
        return valorTasy;
    }

    public void setValorTasy(Double valorTasy) {
        this.valorTasy = valorTasy;
    }
    
    

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public Cirurgia getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Cirurgia idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    public Servico getIdServico() {
        return idServico;
    }

    public void setIdServico(Servico idServico) {
        this.idServico = idServico;
    }

    public Double getValorTotalGeral() {
        return valorTotalGeral;
    }

    public void setValorTotalGeral(Double valorTotalGeral) {
        this.valorTotalGeral = valorTotalGeral;
    }

    public OrcamentoItem getIdOrcamentoItem() {
        return idOrcamentoItem;
    }

    public void setIdOrcamentoItem(OrcamentoItem idOrcamentoItem) {
        this.idOrcamentoItem = idOrcamentoItem;
    }
    
    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public ProcedimentoPaciente getIdProcedimentoPaciente() {
        return idProcedimentoPaciente;
    }

    public void setIdProcedimentoPaciente(ProcedimentoPaciente idProcedimentoPaciente) {
        this.idProcedimentoPaciente = idProcedimentoPaciente;
    }

    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCirurgiaItem != null ? idCirurgiaItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CirurgiaItem)) {
            return false;
        }
        CirurgiaItem other = (CirurgiaItem) object;
        if ((this.idCirurgiaItem == null && other.idCirurgiaItem != null) || (this.idCirurgiaItem != null && !this.idCirurgiaItem.equals(other.idCirurgiaItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.CirurgiaItem[ idCirurgiaItem=" + idCirurgiaItem + " ]";
    }
    
}
