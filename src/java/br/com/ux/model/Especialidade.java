/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "especialidade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Especialidade.findAll", query = "SELECT e FROM Especialidade e"),
    @NamedQuery(name = "Especialidade.findByIdEspecialidade", query = "SELECT e FROM Especialidade e WHERE e.idEspecialidade = :idEspecialidade"),
    @NamedQuery(name = "Especialidade.findByDescricao", query = "SELECT e FROM Especialidade e WHERE e.descricao = :descricao")})
public class Especialidade implements Serializable {
    @Column(name = "inativo")
    private Boolean inativo = Boolean.FALSE;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ESPECIALIDADE")
    private Integer idEspecialidade;
    @Column(name = "DESCRICAO")
    private String descricao;
    @OneToMany(mappedBy = "idEspecialidade")
    private List<Terceiro> terceiroList;

    public Especialidade() {
    }

    public Especialidade(Integer idEspecialidade) {
        this.idEspecialidade = idEspecialidade;
    }

    public Integer getIdEspecialidade() {
        return idEspecialidade;
    }

    public void setIdEspecialidade(Integer idEspecialidade) {
        this.idEspecialidade = idEspecialidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<Terceiro> getTerceiroList() {
        return terceiroList;
    }

    public void setTerceiroList(List<Terceiro> terceiroList) {
        this.terceiroList = terceiroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEspecialidade != null ? idEspecialidade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Especialidade)) {
            return false;
        }
        Especialidade other = (Especialidade) object;
        if ((this.idEspecialidade == null && other.idEspecialidade != null) || (this.idEspecialidade != null && !this.idEspecialidade.equals(other.idEspecialidade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Especialidade[ idEspecialidade=" + idEspecialidade + " ]";
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }
    
}
