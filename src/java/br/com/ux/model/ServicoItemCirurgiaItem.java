/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "servico_item_cirurgia_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServicoItemCirurgiaItem.findAll", query = "SELECT s FROM ServicoItemCirurgiaItem s"),
    @NamedQuery(name = "ServicoItemCirurgiaItem.findByIdSici", query = "SELECT s FROM ServicoItemCirurgiaItem s WHERE s.idSici = :idSici"),
    @NamedQuery(name = "ServicoItemCirurgiaItem.findByValor", query = "SELECT s FROM ServicoItemCirurgiaItem s WHERE s.valor = :valor"),
    @NamedQuery(name = "ServicoItemCirurgiaItem.findByTaxa", query = "SELECT s FROM ServicoItemCirurgiaItem s WHERE s.taxa = :taxa"),
    @NamedQuery(name = "ServicoItemCirurgiaItem.findByValorTaxado", query = "SELECT s FROM ServicoItemCirurgiaItem s WHERE s.valorTaxado = :valorTaxado"),
    @NamedQuery(name = "ServicoItemCirurgiaItem.findByPrincipal", query = "SELECT s FROM ServicoItemCirurgiaItem s WHERE s.principal = :principal"),
    @NamedQuery(name = "ServicoItemCirurgiaItem.findByTaxaValor", query = "SELECT s FROM ServicoItemCirurgiaItem s WHERE s.taxaValor = :taxaValor"),
    @NamedQuery(name = "ServicoItemCirurgiaItem.findByValorPago", query = "SELECT s FROM ServicoItemCirurgiaItem s WHERE s.valorPago = :valorPago")})
public class ServicoItemCirurgiaItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SICI")
    private Integer idSici;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "TAXA")
    private Double taxa;
    @Column(name = "VALOR_TAXADO")
    private Double valorTaxado;
    @Column(name = "PRINCIPAL")
    private String principal;
    @Column(name = "TAXA_VALOR")
    private Double taxaValor;
    @Column(name = "VALOR_PAGO")
    private Double valorPago;
    @JoinColumn(name = "ID_REPASSE", referencedColumnName = "ID_REPASSE")
    @ManyToOne
    private Repasse idRepasse;
    @JoinColumn(name = "ID_CIRURGIA_ITEM", referencedColumnName = "ID_CIRURGIA_ITEM")
    @ManyToOne
    private CirurgiaItem idCirurgiaItem;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @JoinColumn(name = "ID_SERVICO_ITEM", referencedColumnName = "ID_SERVICO_ITEM")
    @ManyToOne
    private ServicoItem idServicoItem;
    @JoinColumn(name = "ID_CIRURGIA", referencedColumnName = "ID_CIRURGIA")
    @ManyToOne
    private Cirurgia idCirurgia;
    @OneToMany(mappedBy = "idSici")
    private List<Repasse> repasseList;

    public ServicoItemCirurgiaItem() {
    }

    public ServicoItemCirurgiaItem(Integer idSici) {
        this.idSici = idSici;
    }

    public Integer getIdSici() {
        return idSici;
    }

    public void setIdSici(Integer idSici) {
        this.idSici = idSici;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public Double getValorTaxado() {
        return valorTaxado;
    }

    public void setValorTaxado(Double valorTaxado) {
        this.valorTaxado = valorTaxado;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public Double getTaxaValor() {
        return taxaValor;
    }

    public void setTaxaValor(Double taxaValor) {
        this.taxaValor = taxaValor;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public Repasse getIdRepasse() {
        return idRepasse;
    }

    public void setIdRepasse(Repasse idRepasse) {
        this.idRepasse = idRepasse;
    }

    public CirurgiaItem getIdCirurgiaItem() {
        return idCirurgiaItem;
    }

    public void setIdCirurgiaItem(CirurgiaItem idCirurgiaItem) {
        this.idCirurgiaItem = idCirurgiaItem;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public ServicoItem getIdServicoItem() {
        return idServicoItem;
    }

    public void setIdServicoItem(ServicoItem idServicoItem) {
        this.idServicoItem = idServicoItem;
    }

    public Cirurgia getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Cirurgia idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    @XmlTransient
    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSici != null ? idSici.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServicoItemCirurgiaItem)) {
            return false;
        }
        ServicoItemCirurgiaItem other = (ServicoItemCirurgiaItem) object;
        if ((this.idSici == null && other.idSici != null) || (this.idSici != null && !this.idSici.equals(other.idSici))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.ServicoItemCirurgiaItem[ idSici=" + idSici + " ]";
    }
    
}
