/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "tipo_internacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoInternacao.findAll", query = "SELECT t FROM TipoInternacao t"),
    @NamedQuery(name = "TipoInternacao.findByIdTipoInternacao", query = "SELECT t FROM TipoInternacao t WHERE t.idTipoInternacao = :idTipoInternacao"),
    @NamedQuery(name = "TipoInternacao.findByDescricao", query = "SELECT t FROM TipoInternacao t WHERE t.descricao = :descricao")})
public class TipoInternacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_TIPO_INTERNACAO")
    private Integer idTipoInternacao;
    @Column(name = "DESCRICAO")
    private String descricao;
    @OneToMany(mappedBy = "idTipoInternacao")
    private List<Orcamento> orcamentoList;
    @OneToMany(mappedBy = "idTipoInternacao")
    private List<Cirurgia> cirurgiaList;

    public TipoInternacao() {
    }

    public TipoInternacao(Integer idTipoInternacao) {
        this.idTipoInternacao = idTipoInternacao;
    }

    public Integer getIdTipoInternacao() {
        return idTipoInternacao;
    }

    public void setIdTipoInternacao(Integer idTipoInternacao) {
        this.idTipoInternacao = idTipoInternacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoInternacao != null ? idTipoInternacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoInternacao)) {
            return false;
        }
        TipoInternacao other = (TipoInternacao) object;
        if ((this.idTipoInternacao == null && other.idTipoInternacao != null) || (this.idTipoInternacao != null && !this.idTipoInternacao.equals(other.idTipoInternacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.TipoInternacao[ idTipoInternacao=" + idTipoInternacao + " ]";
    }
    
}
