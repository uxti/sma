/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.Terceiro;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "cbo_saude")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CboSaude.findAll", query = "SELECT c FROM CboSaude c"),
    @NamedQuery(name = "CboSaude.findByIdCboSaude", query = "SELECT c FROM CboSaude c WHERE c.idCboSaude = :idCboSaude"),
    @NamedQuery(name = "CboSaude.findByCodigoCbo", query = "SELECT c FROM CboSaude c WHERE c.codigoCbo = :codigoCbo"),
    @NamedQuery(name = "CboSaude.findByDescricao", query = "SELECT c FROM CboSaude c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "CboSaude.findByIdCboSaudeTasy", query = "SELECT c FROM CboSaude c WHERE c.idCboSaudeTasy = :idCboSaudeTasy"),
    @NamedQuery(name = "CboSaude.findByAtivo", query = "SELECT c FROM CboSaude c WHERE c.ativo = :ativo")})
public class CboSaude implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCboSaude")
    private List<RegraRepasse> regraRepasseList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cbo_saude")
    private Integer idCboSaude;
    @Column(name = "codigo_cbo")
    private String codigoCbo;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "id_cbo_saude_tasy")
    private Integer idCboSaudeTasy;
    @Column(name = "ativo")
    private Boolean ativo;
    @OneToMany(mappedBy = "idCboSaude")
    private List<Terceiro> terceiroList;

    public CboSaude() {
    }

    public CboSaude(Integer idCboSaude) {
        this.idCboSaude = idCboSaude;
    }

    public Integer getIdCboSaude() {
        return idCboSaude;
    }

    public void setIdCboSaude(Integer idCboSaude) {
        this.idCboSaude = idCboSaude;
    }

    public String getCodigoCbo() {
        return codigoCbo;
    }

    public void setCodigoCbo(String codigoCbo) {
        this.codigoCbo = codigoCbo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getIdCboSaudeTasy() {
        return idCboSaudeTasy;
    }

    public void setIdCboSaudeTasy(Integer idCboSaudeTasy) {
        this.idCboSaudeTasy = idCboSaudeTasy;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @XmlTransient
    public List<Terceiro> getTerceiroList() {
        return terceiroList;
    }

    public void setTerceiroList(List<Terceiro> terceiroList) {
        this.terceiroList = terceiroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCboSaude != null ? idCboSaude.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CboSaude)) {
            return false;
        }
        CboSaude other = (CboSaude) object;
        if ((this.idCboSaude == null && other.idCboSaude != null) || (this.idCboSaude != null && !this.idCboSaude.equals(other.idCboSaude))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.CboSaude[ idCboSaude=" + idCboSaude + " ]";
    }

    @XmlTransient
    public List<RegraRepasse> getRegraRepasseList() {
        return regraRepasseList;
    }

    public void setRegraRepasseList(List<RegraRepasse> regraRepasseList) {
        this.regraRepasseList = regraRepasseList;
    }
    
}
