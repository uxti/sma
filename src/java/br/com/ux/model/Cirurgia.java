/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.tasy.ContaPaciente;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "cirurgia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cirurgia.findAll", query = "SELECT c FROM Cirurgia c"),
    @NamedQuery(name = "Cirurgia.findByIdCirurgia", query = "SELECT c FROM Cirurgia c WHERE c.idCirurgia = :idCirurgia"),
    @NamedQuery(name = "Cirurgia.findByTipoAtendimento", query = "SELECT c FROM Cirurgia c WHERE c.tipoAtendimento = :tipoAtendimento"),
    @NamedQuery(name = "Cirurgia.findByDtCirurgia", query = "SELECT c FROM Cirurgia c WHERE c.dtCirurgia = :dtCirurgia"),
    @NamedQuery(name = "Cirurgia.findByValor", query = "SELECT c FROM Cirurgia c WHERE c.valor = :valor"),
    @NamedQuery(name = "Cirurgia.findByStatus", query = "SELECT c FROM Cirurgia c WHERE c.status = :status"),
    @NamedQuery(name = "Cirurgia.findByFormaPagamento", query = "SELECT c FROM Cirurgia c WHERE c.formaPagamento = :formaPagamento"),
    @NamedQuery(name = "Cirurgia.findByObservacao", query = "SELECT c FROM Cirurgia c WHERE c.observacao = :observacao"),
    @NamedQuery(name = "Cirurgia.findByResponsavel", query = "SELECT c FROM Cirurgia c WHERE c.responsavel = :responsavel"),
    @NamedQuery(name = "Cirurgia.findByGerarPagamentoResponsavel", query = "SELECT c FROM Cirurgia c WHERE c.gerarPagamentoResponsavel = :gerarPagamentoResponsavel")})
public class Cirurgia implements Serializable {

    @Column(name = "dt_internacao")
    @Temporal(TemporalType.DATE)
    private Date dtInternacao;
    @Column(name = "dt_finalizacao")
    @Temporal(TemporalType.DATE)
    private Date dtFinalizacao;
    @Column(name = "DT_ULTIMA_ALTERACAO")
    @Temporal(TemporalType.DATE)
    private Date dtUltimaAlteracao;
    @JoinColumn(name = "id_usuario", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "ID_USUARIO_ULTIMA_ALTERACAO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioUltimaAlteracao;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CIRURGIA")
    private Integer idCirurgia;
    @Column(name = "TIPO_ATENDIMENTO")
    private String tipoAtendimento;
    @Column(name = "DT_CIRURGIA")
    @Temporal(TemporalType.DATE)
    private Date dtCirurgia;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "desconto_total")
    private Double descontoTotal;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "FORMA_PAGAMENTO")
    private String formaPagamento;
    @Column(name = "OBSERVACAO")
    private String observacao;
    @Column(name = "RESPONSAVEL")
    private String responsavel;
    @Column(name = "GERAR_PAGAMENTO_RESPONSAVEL")
    private String gerarPagamentoResponsavel;
    @Column(name = "pago")
    private boolean pago;
    @Column(name = "cancelado")
    private boolean cancelado;
    @OneToMany(mappedBy = "idCirurgia")
    private List<Caixa> caixaList;
    @OneToMany(mappedBy = "idCirurgia")
    private List<Alerta> alertaList;
    @OneToMany(mappedBy = "idCirurgia")
    private List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList;
    @OneToMany(mappedBy = "idCirurgia")
    private List<Repasse> repasseList;
    @OneToMany(mappedBy = "idCirurgia")
    private List<ContaCorrente> contaCorrenteList;
    @OneToMany(mappedBy = "idCirurgia")
    private List<CirurgiaItem> cirurgiaItemList;
    @JoinColumn(name = "ID_UNIDADE", referencedColumnName = "ID_UNIDADE")
    @ManyToOne
    private UnidadeAtendimento idUnidade;
    @JoinColumn(name = "ID_FORMA", referencedColumnName = "ID_FORMA")
    @ManyToOne
    private FormaPagamento idForma;
    @JoinColumn(name = "ID_PROCEDIMENTO", referencedColumnName = "ID_PROCEDIMENTO")
    @ManyToOne
    private Procedimento idProcedimento;
    @JoinColumn(name = "ID_PACIENTE", referencedColumnName = "ID_PACIENTE")
    @ManyToOne
    private Paciente idPaciente;
    @JoinColumn(name = "ID_RESPONSAVEL", referencedColumnName = "ID_PACIENTE")
    @ManyToOne
    private Paciente idResponsavel;
    @JoinColumn(name = "ID_PLANO", referencedColumnName = "ID_PLANO")
    @ManyToOne
    private Plano idPlano;
    @JoinColumn(name = "ID_CONVENIO", referencedColumnName = "ID_CONVENIO")
    @ManyToOne
    private Convenio idConvenio;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @JoinColumn(name = "ID_TIPO_INTERNACAO", referencedColumnName = "ID_TIPO_INTERNACAO")
    @ManyToOne
    private TipoInternacao idTipoInternacao;
    @JoinColumn(name = "ID_ORCAMENTO", referencedColumnName = "ID_ORCAMENTO")
    @ManyToOne
    private Orcamento idOrcamento;
    @JoinColumn(name = "id_conta_paciente", referencedColumnName = "id_conta_paciente")
    @ManyToOne
    private ContaPaciente idContaPaciente;
    @OneToMany(mappedBy = "idCirurgia")
    private List<Cheque> chequeList;
    @Column(name = "externo")
    private boolean externo;

    public Cirurgia() {
    }

    public Cirurgia(Integer idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    public Integer getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Integer idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    public String getTipoAtendimento() {
        return tipoAtendimento;
    }

    public void setTipoAtendimento(String tipoAtendimento) {
        this.tipoAtendimento = tipoAtendimento;
    }

    public Date getDtCirurgia() {
        return dtCirurgia;
    }

    public void setDtCirurgia(Date dtCirurgia) {
        this.dtCirurgia = dtCirurgia;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public Date getDtUltimaAlteracao() {
        return dtUltimaAlteracao;
    }

    public void setDtUltimaAlteracao(Date dtUltimaAlteracao) {
        this.dtUltimaAlteracao = dtUltimaAlteracao;
    }

    public Usuario getIdUsuarioUltimaAlteracao() {
        return idUsuarioUltimaAlteracao;
    }

    public void setIdUsuarioUltimaAlteracao(Usuario idUsuarioUltimaAlteracao) {
        this.idUsuarioUltimaAlteracao = idUsuarioUltimaAlteracao;
    }

    public String getGerarPagamentoResponsavel() {
        return gerarPagamentoResponsavel;
    }

    public void setGerarPagamentoResponsavel(String gerarPagamentoResponsavel) {
        this.gerarPagamentoResponsavel = gerarPagamentoResponsavel;
    }


    @XmlTransient
    public List<Caixa> getCaixaList() {
        return caixaList;
    }

    public void setCaixaList(List<Caixa> caixaList) {
        this.caixaList = caixaList;
    }

    @XmlTransient
    public List<Alerta> getAlertaList() {
        return alertaList;
    }

    public void setAlertaList(List<Alerta> alertaList) {
        this.alertaList = alertaList;
    }

    @XmlTransient
    public List<ServicoItemCirurgiaItem> getServicoItemCirurgiaItemList() {
        return servicoItemCirurgiaItemList;
    }

    public void setServicoItemCirurgiaItemList(List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList) {
        this.servicoItemCirurgiaItemList = servicoItemCirurgiaItemList;
    }

    @XmlTransient
    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    @XmlTransient
    public List<CirurgiaItem> getCirurgiaItemList() {
        return cirurgiaItemList;
    }

    public void setCirurgiaItemList(List<CirurgiaItem> cirurgiaItemList) {
        this.cirurgiaItemList = cirurgiaItemList;
    }

    public UnidadeAtendimento getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(UnidadeAtendimento idUnidade) {
        this.idUnidade = idUnidade;
    }

    public FormaPagamento getIdForma() {
        return idForma;
    }

    public void setIdForma(FormaPagamento idForma) {
        this.idForma = idForma;
    }

    public Procedimento getIdProcedimento() {
        return idProcedimento;
    }

    public void setIdProcedimento(Procedimento idProcedimento) {
        this.idProcedimento = idProcedimento;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Paciente getIdResponsavel() {
        return idResponsavel;
    }

    public void setIdResponsavel(Paciente idResponsavel) {
        this.idResponsavel = idResponsavel;
    }

    public Plano getIdPlano() {
        return idPlano;
    }

    public void setIdPlano(Plano idPlano) {
        this.idPlano = idPlano;
    }

    public Convenio getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(Convenio idConvenio) {
        this.idConvenio = idConvenio;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public TipoInternacao getIdTipoInternacao() {
        return idTipoInternacao;
    }

    public void setIdTipoInternacao(TipoInternacao idTipoInternacao) {
        this.idTipoInternacao = idTipoInternacao;
    }

    public Orcamento getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(Orcamento idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public boolean isCancelado() {
        return cancelado;
    }

    public void setCancelado(boolean cancelado) {
        this.cancelado = cancelado;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCirurgia != null ? idCirurgia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cirurgia)) {
            return false;
        }
        Cirurgia other = (Cirurgia) object;
        if ((this.idCirurgia == null && other.idCirurgia != null) || (this.idCirurgia != null && !this.idCirurgia.equals(other.idCirurgia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Cirurgia[ idCirurgia=" + idCirurgia + " ]";
    }

    public Date getDtInternacao() {
        return dtInternacao;
    }

    public void setDtInternacao(Date dtInternacao) {
        this.dtInternacao = dtInternacao;
    }

    public Date getDtFinalizacao() {
        return dtFinalizacao;
    }

    public void setDtFinalizacao(Date dtFinalizacao) {
        this.dtFinalizacao = dtFinalizacao;
    }

    public boolean isPago() {
        return pago;
    }

    public void setPago(boolean pago) {
        this.pago = pago;
    }

    public ContaPaciente getIdContaPaciente() {
        return idContaPaciente;
    }

    public void setIdContaPaciente(ContaPaciente idContaPaciente) {
        this.idContaPaciente = idContaPaciente;
    }

    public boolean isExterno() {
        return externo;
    }

    public void setExterno(boolean externo) {
        this.externo = externo;
    }

    public Double getDescontoTotal() {
        return descontoTotal;
    }

    public void setDescontoTotal(Double descontoTotal) {
        this.descontoTotal = descontoTotal;
    }

    
    
    
    
}
