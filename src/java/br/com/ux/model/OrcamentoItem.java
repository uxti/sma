/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "orcamento_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrcamentoItem.findAll", query = "SELECT o FROM OrcamentoItem o"),
    @NamedQuery(name = "OrcamentoItem.findByIdOrcamentoItem", query = "SELECT o FROM OrcamentoItem o WHERE o.idOrcamentoItem = :idOrcamentoItem"),
    @NamedQuery(name = "OrcamentoItem.findByTipoServico", query = "SELECT o FROM OrcamentoItem o WHERE o.tipoServico = :tipoServico"),
    @NamedQuery(name = "OrcamentoItem.findByQuantidade", query = "SELECT o FROM OrcamentoItem o WHERE o.quantidade = :quantidade"),
    @NamedQuery(name = "OrcamentoItem.findByValorUnitario", query = "SELECT o FROM OrcamentoItem o WHERE o.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "OrcamentoItem.findByTaxa", query = "SELECT o FROM OrcamentoItem o WHERE o.taxa = :taxa"),
    @NamedQuery(name = "OrcamentoItem.findByValorTotal", query = "SELECT o FROM OrcamentoItem o WHERE o.valorTotal = :valorTotal")})
public class OrcamentoItem implements Serializable {

    @OneToMany(mappedBy = "idOrcamentoItem")
    private List<CirurgiaItem> cirurgiaItemList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ORCAMENTO_ITEM")
    private Integer idOrcamentoItem;
    @Column(name = "TIPO_SERVICO")
    private String tipoServico;
    @Column(name = "QUANTIDADE")
    private Double quantidade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR_UNITARIO")
    private Double valorUnitario;
    @Column(name = "TAXA")
    private Double taxa;
    @Column(name = "VALOR_TOTAL")
    private Double valorTotal;
    @Lob
    @Column(name = "OBSERVACAO")
    private String observacao;
    @JoinColumn(name = "ID_ORCAMENTO", referencedColumnName = "ID_ORCAMENTO")
    @ManyToOne(optional = false)
    private Orcamento idOrcamento;
    @JoinColumn(name = "ID_SERVICO", referencedColumnName = "ID_SERVICO")
    @ManyToOne
    private Servico idServico;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;

    public OrcamentoItem() {
    }

    public OrcamentoItem(Integer idOrcamentoItem) {
        this.idOrcamentoItem = idOrcamentoItem;
    }

    public Integer getIdOrcamentoItem() {
        return idOrcamentoItem;
    }

    public void setIdOrcamentoItem(Integer idOrcamentoItem) {
        this.idOrcamentoItem = idOrcamentoItem;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Orcamento getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(Orcamento idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public Servico getIdServico() {
        return idServico;
    }

    public void setIdServico(Servico idServico) {
        this.idServico = idServico;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrcamentoItem != null ? idOrcamentoItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrcamentoItem)) {
            return false;
        }
        OrcamentoItem other = (OrcamentoItem) object;
        if ((this.idOrcamentoItem == null && other.idOrcamentoItem != null) || (this.idOrcamentoItem != null && !this.idOrcamentoItem.equals(other.idOrcamentoItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.OrcamentoItem[ idOrcamentoItem=" + idOrcamentoItem + " ]";
    }

    @XmlTransient
    public List<CirurgiaItem> getCirurgiaItemList() {
        return cirurgiaItemList;
    }

    public void setCirurgiaItemList(List<CirurgiaItem> cirurgiaItemList) {
        this.cirurgiaItemList = cirurgiaItemList;
    }
    
}
