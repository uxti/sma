/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "exame")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Exame.findAll", query = "SELECT e FROM Exame e"),
    @NamedQuery(name = "Exame.findByIdExame", query = "SELECT e FROM Exame e WHERE e.idExame = :idExame"),
    @NamedQuery(name = "Exame.findByDtExame", query = "SELECT e FROM Exame e WHERE e.dtExame = :dtExame"),
    @NamedQuery(name = "Exame.findByDtRecebimento", query = "SELECT e FROM Exame e WHERE e.dtRecebimento = :dtRecebimento"),
    @NamedQuery(name = "Exame.findByValor", query = "SELECT e FROM Exame e WHERE e.valor = :valor"),
    @NamedQuery(name = "Exame.findByObservacao", query = "SELECT e FROM Exame e WHERE e.observacao = :observacao"),
    @NamedQuery(name = "Exame.findByTipoLancamento", query = "SELECT e FROM Exame e WHERE e.tipoLancamento = :tipoLancamento"),
    @NamedQuery(name = "Exame.findByPadrao", query = "SELECT e FROM Exame e WHERE e.padrao = :padrao"),
    @NamedQuery(name = "Exame.findByTipoPagamento", query = "SELECT e FROM Exame e WHERE e.tipoPagamento = :tipoPagamento"),
    @NamedQuery(name = "Exame.findBySituacao", query = "SELECT e FROM Exame e WHERE e.situacao = :situacao")})
public class Exame implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_EXAME")
    private Integer idExame;
    @Column(name = "DT_EXAME")
    @Temporal(TemporalType.DATE)
    private Date dtExame;
    @Column(name = "DT_RECEBIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dtRecebimento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "OBSERVACAO")
    private String observacao;
    @Column(name = "TIPO_LANCAMENTO")
    private String tipoLancamento;
    @Column(name = "PADRAO")
    private String padrao;
    @Column(name = "TIPO_PAGAMENTO")
    private String tipoPagamento;
    @Column(name = "SITUACAO")
    private String situacao;
    @OneToMany(mappedBy = "idExame")
    private List<Alerta> alertaList;
    @OneToMany(mappedBy = "idExame")
    private List<Repasse> repasseList;
    @OneToMany(mappedBy = "idExame")
    private List<ContaCorrente> contaCorrenteList;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "ID_DESCRICAO_EXAME", referencedColumnName = "ID_DESCRICAO_EXAME")
    @ManyToOne
    private DescricaoExame idDescricaoExame;
    @JoinColumn(name = "ID_FORMA", referencedColumnName = "ID_FORMA")
    @ManyToOne
    private FormaPagamento idForma;
    @JoinColumn(name = "ID_PACIENTE", referencedColumnName = "ID_PACIENTE")
    @ManyToOne(optional = false)
    private Paciente idPaciente;
    @OneToMany(mappedBy = "idExame")
    private List<Cheque> chequeList;

    public Exame() {
    }

    public Exame(Integer idExame) {
        this.idExame = idExame;
    }

    public Integer getIdExame() {
        return idExame;
    }

    public void setIdExame(Integer idExame) {
        this.idExame = idExame;
    }

    public Date getDtExame() {
        return dtExame;
    }

    public void setDtExame(Date dtExame) {
        this.dtExame = dtExame;
    }

    public Date getDtRecebimento() {
        return dtRecebimento;
    }

    public void setDtRecebimento(Date dtRecebimento) {
        this.dtRecebimento = dtRecebimento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getTipoLancamento() {
        return tipoLancamento;
    }

    public void setTipoLancamento(String tipoLancamento) {
        this.tipoLancamento = tipoLancamento;
    }

    public String getPadrao() {
        return padrao;
    }

    public void setPadrao(String padrao) {
        this.padrao = padrao;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    @XmlTransient
    public List<Alerta> getAlertaList() {
        return alertaList;
    }

    public void setAlertaList(List<Alerta> alertaList) {
        this.alertaList = alertaList;
    }

    @XmlTransient
    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public DescricaoExame getIdDescricaoExame() {
        return idDescricaoExame;
    }

    public void setIdDescricaoExame(DescricaoExame idDescricaoExame) {
        this.idDescricaoExame = idDescricaoExame;
    }

    public FormaPagamento getIdForma() {
        return idForma;
    }

    public void setIdForma(FormaPagamento idForma) {
        this.idForma = idForma;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idExame != null ? idExame.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exame)) {
            return false;
        }
        Exame other = (Exame) object;
        if ((this.idExame == null && other.idExame != null) || (this.idExame != null && !this.idExame.equals(other.idExame))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Exame[ idExame=" + idExame + " ]";
    }
    
}
