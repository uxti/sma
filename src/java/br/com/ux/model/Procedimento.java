/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "procedimento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Procedimento.findAll", query = "SELECT p FROM Procedimento p"),
    @NamedQuery(name = "Procedimento.findByIdProcedimento", query = "SELECT p FROM Procedimento p WHERE p.idProcedimento = :idProcedimento"),
    @NamedQuery(name = "Procedimento.findByDescricao", query = "SELECT p FROM Procedimento p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "Procedimento.findByPadrao", query = "SELECT p FROM Procedimento p WHERE p.padrao = :padrao")})
public class Procedimento implements Serializable {
    @OneToMany(mappedBy = "idProcedimento")
    private List<Cheque> chequeList;
    @Column(name = "inativo")
    private Boolean inativo = Boolean.FALSE;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private Double valor;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PROCEDIMENTO")
    private Integer idProcedimento;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "PADRAO")
    private String padrao;
    @Column(name = "id_servico")
    private Integer idServico;
    @OneToMany(mappedBy = "idProcedimento")
    private List<Orcamento> orcamentoList;
    @OneToMany(mappedBy = "idProcedimento")
    private List<Cirurgia> cirurgiaList;

    public Procedimento() {
    }

    public Procedimento(Integer idProcedimento) {
        this.idProcedimento = idProcedimento;
    }

    public Integer getIdProcedimento() {
        return idProcedimento;
    }

    public void setIdProcedimento(Integer idProcedimento) {
        this.idProcedimento = idProcedimento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public String getPadrao() {
        return padrao;
    }

    public void setPadrao(String padrao) {
        this.padrao = padrao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
        
    @XmlTransient
    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProcedimento != null ? idProcedimento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Procedimento)) {
            return false;
        }
        Procedimento other = (Procedimento) object;
        if ((this.idProcedimento == null && other.idProcedimento != null) || (this.idProcedimento != null && !this.idProcedimento.equals(other.idProcedimento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Procedimento[ idProcedimento=" + idProcedimento + " ]";
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    public Integer getIdServico() {
        return idServico;
    }

    public void setIdServico(Integer idServico) {
        this.idServico = idServico;
    }
    
    
    
}
