/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "operacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Operacao.findAll", query = "SELECT o FROM Operacao o"),
    @NamedQuery(name = "Operacao.findByIdOperacao", query = "SELECT o FROM Operacao o WHERE o.idOperacao = :idOperacao"),
    @NamedQuery(name = "Operacao.findByDescricao", query = "SELECT o FROM Operacao o WHERE o.descricao = :descricao"),
    @NamedQuery(name = "Operacao.findByTipo", query = "SELECT o FROM Operacao o WHERE o.tipo = :tipo")})
public class Operacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_OPERACAO")
    private Integer idOperacao;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "TIPO")
    private String tipo;
    @OneToMany(mappedBy = "idOperacao")
    private List<Caixa> caixaList;

    public Operacao() {
    }

    public Operacao(Integer idOperacao) {
        this.idOperacao = idOperacao;
    }

    public Integer getIdOperacao() {
        return idOperacao;
    }

    public void setIdOperacao(Integer idOperacao) {
        this.idOperacao = idOperacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<Caixa> getCaixaList() {
        return caixaList;
    }

    public void setCaixaList(List<Caixa> caixaList) {
        this.caixaList = caixaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOperacao != null ? idOperacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operacao)) {
            return false;
        }
        Operacao other = (Operacao) object;
        if ((this.idOperacao == null && other.idOperacao != null) || (this.idOperacao != null && !this.idOperacao.equals(other.idOperacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Operacao[ idOperacao=" + idOperacao + " ]";
    }
    
}
