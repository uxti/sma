/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author charl
 */
@Entity
@Table(name = "movimento_caixa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MovimentoCaixa.findAll", query = "SELECT m FROM MovimentoCaixa m"),
    @NamedQuery(name = "MovimentoCaixa.findByIdMovimentoCaixa", query = "SELECT m FROM MovimentoCaixa m WHERE m.idMovimentoCaixa = :idMovimentoCaixa"),
    @NamedQuery(name = "MovimentoCaixa.findByTipoMovimentacao", query = "SELECT m FROM MovimentoCaixa m WHERE m.tipoMovimentacao = :tipoMovimentacao"),
    @NamedQuery(name = "MovimentoCaixa.findByDescricao", query = "SELECT m FROM MovimentoCaixa m WHERE m.descricao = :descricao"),
    @NamedQuery(name = "MovimentoCaixa.findByDtMovimentacao", query = "SELECT m FROM MovimentoCaixa m WHERE m.dtMovimentacao = :dtMovimentacao"),
    @NamedQuery(name = "MovimentoCaixa.findByValor", query = "SELECT m FROM MovimentoCaixa m WHERE m.valor = :valor"),
    @NamedQuery(name = "MovimentoCaixa.findByLoteTransferencia", query = "SELECT m FROM MovimentoCaixa m WHERE m.loteTransferencia = :loteTransferencia"),
    @NamedQuery(name = "MovimentoCaixa.findByDtAtualizacao", query = "SELECT m FROM MovimentoCaixa m WHERE m.dtAtualizacao = :dtAtualizacao")})
public class MovimentoCaixa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_movimento_caixa")
    private Integer idMovimentoCaixa;
    @Column(name = "tipo_movimentacao")
    private String tipoMovimentacao;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "dt_movimentacao")
    @Temporal(TemporalType.DATE)
    private Date dtMovimentacao;
    @Column(name = "valor")
    private double valor;
    @Column(name = "lote_transferencia")
    private String loteTransferencia;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAtualizacao;
    @JoinColumn(name = "id_saque", referencedColumnName = "ID_CONTA_CORRENTE")
    @ManyToOne
    private ContaCorrente idSaque;    
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;

    public MovimentoCaixa() {
    }

    public MovimentoCaixa(Integer idMovimentoCaixa) {
        this.idMovimentoCaixa = idMovimentoCaixa;
    }

    public MovimentoCaixa(Integer idMovimentoCaixa, String tipoMovimentacao, String descricao, Date dtMovimentacao, double valor, String loteTransferencia, Date dtAtualizacao) {
        this.idMovimentoCaixa = idMovimentoCaixa;
        this.tipoMovimentacao = tipoMovimentacao;
        this.descricao = descricao;
        this.dtMovimentacao = dtMovimentacao;
        this.valor = valor;
        this.loteTransferencia = loteTransferencia;
        this.dtAtualizacao = dtAtualizacao;
    }

    public Integer getIdMovimentoCaixa() {
        return idMovimentoCaixa;
    }

    public void setIdMovimentoCaixa(Integer idMovimentoCaixa) {
        this.idMovimentoCaixa = idMovimentoCaixa;
    }

    public String getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(String tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDtMovimentacao() {
        return dtMovimentacao;
    }

    public void setDtMovimentacao(Date dtMovimentacao) {
        this.dtMovimentacao = dtMovimentacao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getLoteTransferencia() {
        return loteTransferencia;
    }

    public void setLoteTransferencia(String loteTransferencia) {
        this.loteTransferencia = loteTransferencia;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    public ContaCorrente getIdSaque() {
        return idSaque;
    }

    public void setIdSaque(ContaCorrente idSaque) {
        this.idSaque = idSaque;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMovimentoCaixa != null ? idMovimentoCaixa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MovimentoCaixa)) {
            return false;
        }
        MovimentoCaixa other = (MovimentoCaixa) object;
        if ((this.idMovimentoCaixa == null && other.idMovimentoCaixa != null) || (this.idMovimentoCaixa != null && !this.idMovimentoCaixa.equals(other.idMovimentoCaixa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.MovimentoCaixa[ idMovimentoCaixa=" + idMovimentoCaixa + " ]";
    }
    
}
