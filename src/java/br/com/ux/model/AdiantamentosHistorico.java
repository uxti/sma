/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "adiantamentos_historico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdiantamentosHistorico.findAll", query = "SELECT a FROM AdiantamentosHistorico a"),
    @NamedQuery(name = "AdiantamentosHistorico.findByIdAdiantamentoHistorico", query = "SELECT a FROM AdiantamentosHistorico a WHERE a.idAdiantamentoHistorico = :idAdiantamentoHistorico"),
    @NamedQuery(name = "AdiantamentosHistorico.findByTipo", query = "SELECT a FROM AdiantamentosHistorico a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "AdiantamentosHistorico.findByValor", query = "SELECT a FROM AdiantamentosHistorico a WHERE a.valor = :valor"),
    @NamedQuery(name = "AdiantamentosHistorico.findByNumeroCheque", query = "SELECT a FROM AdiantamentosHistorico a WHERE a.numeroCheque = :numeroCheque"),
    @NamedQuery(name = "AdiantamentosHistorico.findByStatus", query = "SELECT a FROM AdiantamentosHistorico a WHERE a.status = :status"),
    @NamedQuery(name = "AdiantamentosHistorico.findByObservacao", query = "SELECT a FROM AdiantamentosHistorico a WHERE a.observacao = :observacao")})
public class AdiantamentosHistorico implements Serializable {

    @Size(max = 255)
    @Column(name = "pagador")
    private String pagador;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ADIANTAMENTO_HISTORICO")
    private Integer idAdiantamentoHistorico;
    @Column(name = "TIPO")
    private String tipo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "NUMERO_CHEQUE")
    private String numeroCheque;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "OBSERVACAO")
    private String observacao;
    @OneToMany(mappedBy = "idAdiantamentoHistorico")
    private List<Orcamento> orcamentoList;
    @JoinColumn(name = "ID_ORCAMENTO", referencedColumnName = "ID_ORCAMENTO")
    @ManyToOne
    private Orcamento idOrcamento;

    public AdiantamentosHistorico() {
    }

    public AdiantamentosHistorico(Integer idAdiantamentoHistorico) {
        this.idAdiantamentoHistorico = idAdiantamentoHistorico;
    }

    public Integer getIdAdiantamentoHistorico() {
        return idAdiantamentoHistorico;
    }

    public void setIdAdiantamentoHistorico(Integer idAdiantamentoHistorico) {
        this.idAdiantamentoHistorico = idAdiantamentoHistorico;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }

    public Orcamento getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(Orcamento idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public String getPagador() {
        return pagador;
    }

    public void setPagador(String pagador) {
        this.pagador = pagador;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAdiantamentoHistorico != null ? idAdiantamentoHistorico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdiantamentosHistorico)) {
            return false;
        }
        AdiantamentosHistorico other = (AdiantamentosHistorico) object;
        if ((this.idAdiantamentoHistorico == null && other.idAdiantamentoHistorico != null) || (this.idAdiantamentoHistorico != null && !this.idAdiantamentoHistorico.equals(other.idAdiantamentoHistorico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.AdiantamentosHistorico[ idAdiantamentoHistorico=" + idAdiantamentoHistorico + " ]";
    }

    
    
}
