/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "repasse")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Repasse.findAll", query = "SELECT r FROM Repasse r"),
    @NamedQuery(name = "Repasse.findByIdRepasse", query = "SELECT r FROM Repasse r WHERE r.idRepasse = :idRepasse"),
    @NamedQuery(name = "Repasse.findByDtRecebimento", query = "SELECT r FROM Repasse r WHERE r.dtRecebimento = :dtRecebimento"),
    @NamedQuery(name = "Repasse.findByDtRepasse", query = "SELECT r FROM Repasse r WHERE r.dtRepasse = :dtRepasse"),
    @NamedQuery(name = "Repasse.findByDtLancamento", query = "SELECT r FROM Repasse r WHERE r.dtLancamento = :dtLancamento"),
    @NamedQuery(name = "Repasse.findByDtVencimento", query = "SELECT r FROM Repasse r WHERE r.dtVencimento = :dtVencimento"),
    @NamedQuery(name = "Repasse.findByNumeroParcela", query = "SELECT r FROM Repasse r WHERE r.numeroParcela = :numeroParcela"),
    @NamedQuery(name = "Repasse.findByValor", query = "SELECT r FROM Repasse r WHERE r.valor = :valor"),
    @NamedQuery(name = "Repasse.findByObservacao", query = "SELECT r FROM Repasse r WHERE r.observacao = :observacao"),
    @NamedQuery(name = "Repasse.findBySituacaoPaciente", query = "SELECT r FROM Repasse r WHERE r.situacaoPaciente = :situacaoPaciente"),
    @NamedQuery(name = "Repasse.findBySituacaoTerceiro", query = "SELECT r FROM Repasse r WHERE r.situacaoTerceiro = :situacaoTerceiro"),
    @NamedQuery(name = "Repasse.findByStatus", query = "SELECT r FROM Repasse r WHERE r.status = :status"),
    @NamedQuery(name = "Repasse.findByAcrescimo", query = "SELECT r FROM Repasse r WHERE r.acrescimo = :acrescimo"),
    @NamedQuery(name = "Repasse.findByDesconto", query = "SELECT r FROM Repasse r WHERE r.desconto = :desconto"),
    @NamedQuery(name = "Repasse.findByJuros", query = "SELECT r FROM Repasse r WHERE r.juros = :juros"),
    @NamedQuery(name = "Repasse.findByValorRecebido", query = "SELECT r FROM Repasse r WHERE r.valorRecebido = :valorRecebido"),
    @NamedQuery(name = "Repasse.findByValorRepassado", query = "SELECT r FROM Repasse r WHERE r.valorRepassado = :valorRepassado"),
    @NamedQuery(name = "Repasse.findByTipoPagamento", query = "SELECT r FROM Repasse r WHERE r.tipoPagamento = :tipoPagamento"),
    @NamedQuery(name = "Repasse.findByValorDocumento", query = "SELECT r FROM Repasse r WHERE r.valorDocumento = :valorDocumento"),
    @NamedQuery(name = "Repasse.findByPercentualRecebido", query = "SELECT r FROM Repasse r WHERE r.percentualRecebido = :percentualRecebido"),
    @NamedQuery(name = "Repasse.findByFormaPagamento", query = "SELECT r FROM Repasse r WHERE r.formaPagamento = :formaPagamento"),
    @NamedQuery(name = "Repasse.findBySacado", query = "SELECT r FROM Repasse r WHERE r.sacado = :sacado"),
    @NamedQuery(name = "Repasse.findByCirurgiaidResponsavel", query = "SELECT r FROM Repasse r WHERE r.cirurgiaidResponsavel = :cirurgiaidResponsavel"),
    @NamedQuery(name = "Repasse.findByExameidDescricaoExame", query = "SELECT r FROM Repasse r WHERE r.exameidDescricaoExame = :exameidDescricaoExame")})
public class Repasse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_REPASSE")
    private Integer idRepasse;
    @Column(name = "DT_RECEBIMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtRecebimento;
    @Column(name = "DT_REPASSE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtRepasse;
    @Column(name = "DT_LANCAMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtLancamento;
    @Column(name = "DT_VENCIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dtVencimento;
    @Column(name = "NUMERO_PARCELA")
    private Integer numeroParcela;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "OBSERVACAO")
    private String observacao;
    @Column(name = "SITUACAO_PACIENTE")
    private Character situacaoPaciente;
    @Column(name = "SITUACAO_TERCEIRO")
    private Character situacaoTerceiro;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "ACRESCIMO")
    private Double acrescimo;
    @Column(name = "DESCONTO")
    private Double desconto;
    @Column(name = "JUROS")
    private Double juros;
    @Column(name = "VALOR_RECEBIDO")
    private Double valorRecebido;
    @Column(name = "VALOR_REPASSADO")
    private Double valorRepassado;
    @Column(name = "TIPO_PAGAMENTO")
    private Character tipoPagamento;
    @Column(name = "VALOR_DOCUMENTO")
    private Double valorDocumento;
    @Column(name = "PERCENTUAL_RECEBIDO")
    private Double percentualRecebido;
    @Column(name = "FORMA_PAGAMENTO")
    private String formaPagamento;
    @Column(name = "SACADO")
    private String sacado;
    @Column(name = "CIRURGIAID_RESPONSAVEL")
    private Integer cirurgiaidResponsavel;
    @Column(name = "EXAMEID_DESCRICAO_EXAME")
    private Integer exameidDescricaoExame;
    @OneToMany(mappedBy = "idRepasse")
    private List<Caixa> caixaList;
    @OneToMany(mappedBy = "idRepasse")
    private List<Alerta> alertaList;
    @OneToMany(mappedBy = "idRepasse")
    private List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList;
    @JoinColumn(name = "ID_CHEQUE", referencedColumnName = "ID_CHEQUE")
    @ManyToOne
    private Cheque idCheque;
    @JoinColumn(name = "ID_SICI", referencedColumnName = "ID_SICI")
    @ManyToOne
    private ServicoItemCirurgiaItem idSici;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @JoinColumn(name = "ID_EXAME", referencedColumnName = "ID_EXAME")
    @ManyToOne
    private Exame idExame;
    @JoinColumn(name = "ID_PACIENTE", referencedColumnName = "ID_PACIENTE")
    @ManyToOne
    private Paciente idPaciente;
    @JoinColumn(name = "ID_FORMA", referencedColumnName = "ID_FORMA")
    @ManyToOne
    private FormaPagamento idForma;
    @JoinColumn(name = "ID_CIRURGIA", referencedColumnName = "ID_CIRURGIA")
    @ManyToOne
    private Cirurgia idCirurgia;
    @JoinColumn(name = "id_caixa", referencedColumnName = "ID_CAIXA")
    @ManyToOne
    private Caixa idCaixa;
    @JoinColumn(name = "id_cirurgia_item", referencedColumnName = "id_cirurgia_item")
    @ManyToOne
    private CirurgiaItem idCirurgiaItem;
    @OneToMany(mappedBy = "idRepasse")
    private List<ContaCorrente> contaCorrenteList;
    @Transient
    public boolean Pagar;

    public Repasse() {
    }

    public Repasse(Integer idRepasse) {
        this.idRepasse = idRepasse;
    }

    public Integer getIdRepasse() {
        return idRepasse;
    }

    public void setIdRepasse(Integer idRepasse) {
        this.idRepasse = idRepasse;
    }

    public Date getDtRecebimento() {
        return dtRecebimento;
    }

    public void setDtRecebimento(Date dtRecebimento) {
        this.dtRecebimento = dtRecebimento;
    }

    public Date getDtRepasse() {
        return dtRepasse;
    }

    public void setDtRepasse(Date dtRepasse) {
        this.dtRepasse = dtRepasse;
    }

    public Date getDtLancamento() {
        return dtLancamento;
    }

    public void setDtLancamento(Date dtLancamento) {
        this.dtLancamento = dtLancamento;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Integer getNumeroParcela() {
        return numeroParcela;
    }

    public void setNumeroParcela(Integer numeroParcela) {
        this.numeroParcela = numeroParcela;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Character getSituacaoPaciente() {
        return situacaoPaciente;
    }

    public void setSituacaoPaciente(Character situacaoPaciente) {
        this.situacaoPaciente = situacaoPaciente;
    }

    public Character getSituacaoTerceiro() {
        return situacaoTerceiro;
    }

    public void setSituacaoTerceiro(Character situacaoTerceiro) {
        this.situacaoTerceiro = situacaoTerceiro;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Double getAcrescimo() {
        return acrescimo;
    }

    public void setAcrescimo(Double acrescimo) {
        this.acrescimo = acrescimo;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Double getJuros() {
        return juros;
    }

    public void setJuros(Double juros) {
        this.juros = juros;
    }

    public Double getValorRecebido() {
        return valorRecebido;
    }

    public void setValorRecebido(Double valorRecebido) {
        this.valorRecebido = valorRecebido;
    }

    public Double getValorRepassado() {
        return valorRepassado;
    }

    public void setValorRepassado(Double valorRepassado) {
        this.valorRepassado = valorRepassado;
    }

    public Character getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(Character tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public Double getValorDocumento() {
        return valorDocumento;
    }

    public void setValorDocumento(Double valorDocumento) {
        this.valorDocumento = valorDocumento;
    }

    public Double getPercentualRecebido() {
        return percentualRecebido;
    }

    public void setPercentualRecebido(Double percentualRecebido) {
        this.percentualRecebido = percentualRecebido;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public String getSacado() {
        return sacado;
    }

    public void setSacado(String sacado) {
        this.sacado = sacado;
    }

    public Integer getCirurgiaidResponsavel() {
        return cirurgiaidResponsavel;
    }

    public void setCirurgiaidResponsavel(Integer cirurgiaidResponsavel) {
        this.cirurgiaidResponsavel = cirurgiaidResponsavel;
    }

    public Integer getExameidDescricaoExame() {
        return exameidDescricaoExame;
    }

    public void setExameidDescricaoExame(Integer exameidDescricaoExame) {
        this.exameidDescricaoExame = exameidDescricaoExame;
    }

    @XmlTransient
    public List<Caixa> getCaixaList() {
        return caixaList;
    }

    public void setCaixaList(List<Caixa> caixaList) {
        this.caixaList = caixaList;
    }

    @XmlTransient
    public List<Alerta> getAlertaList() {
        return alertaList;
    }

    public void setAlertaList(List<Alerta> alertaList) {
        this.alertaList = alertaList;
    }

    @XmlTransient
    public List<ServicoItemCirurgiaItem> getServicoItemCirurgiaItemList() {
        return servicoItemCirurgiaItemList;
    }

    public void setServicoItemCirurgiaItemList(List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList) {
        this.servicoItemCirurgiaItemList = servicoItemCirurgiaItemList;
    }

    public Cheque getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Cheque idCheque) {
        this.idCheque = idCheque;
    }

    public ServicoItemCirurgiaItem getIdSici() {
        return idSici;
    }

    public void setIdSici(ServicoItemCirurgiaItem idSici) {
        this.idSici = idSici;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public Exame getIdExame() {
        return idExame;
    }

    public void setIdExame(Exame idExame) {
        this.idExame = idExame;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public FormaPagamento getIdForma() {
        return idForma;
    }

    public void setIdForma(FormaPagamento idForma) {
        this.idForma = idForma;
    }

    public Cirurgia getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Cirurgia idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    public boolean isPagar() {
        return Pagar;
    }

    public void setPagar(boolean Pagar) {
        this.Pagar = Pagar;
    }
    
    public Caixa getIdCaixa() {
        return idCaixa;
    }

    public CirurgiaItem getIdCirurgiaItem() {
        return idCirurgiaItem;
    }

    public void setIdCirurgiaItem(CirurgiaItem idCirurgiaItem) {
        this.idCirurgiaItem = idCirurgiaItem;
    }

    public void setIdCaixa(Caixa idCaixa) {
        this.idCaixa = idCaixa;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRepasse != null ? idRepasse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Repasse)) {
            return false;
        }
        Repasse other = (Repasse) object;
        if ((this.idRepasse == null && other.idRepasse != null) || (this.idRepasse != null && !this.idRepasse.equals(other.idRepasse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Repasse[ idRepasse=" + idRepasse + " ]";
    }
    
}
