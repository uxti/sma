/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.Orcamento;
import br.com.ux.model.Usuario;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "emails")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Emails.findAll", query = "SELECT e FROM Emails e"),
    @NamedQuery(name = "Emails.findByIdEmail", query = "SELECT e FROM Emails e WHERE e.idEmail = :idEmail"),
    @NamedQuery(name = "Emails.findByDtEnvio", query = "SELECT e FROM Emails e WHERE e.dtEnvio = :dtEnvio"),
    @NamedQuery(name = "Emails.findByCategoria", query = "SELECT e FROM Emails e WHERE e.categoria = :categoria"),
    @NamedQuery(name = "Emails.findByAssunto", query = "SELECT e FROM Emails e WHERE e.assunto = :assunto"),
    @NamedQuery(name = "Emails.findByRemetente", query = "SELECT e FROM Emails e WHERE e.remetente = :remetente"),
    @NamedQuery(name = "Emails.findByDestinatario", query = "SELECT e FROM Emails e WHERE e.destinatario = :destinatario"),
    @NamedQuery(name = "Emails.findByCc", query = "SELECT e FROM Emails e WHERE e.cc = :cc"),
    @NamedQuery(name = "Emails.findByCco", query = "SELECT e FROM Emails e WHERE e.cco = :cco")})
public class Emails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_email")
    private Integer idEmail;
    @Column(name = "dt_envio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEnvio;
    @Column(name = "categoria")
    private String categoria;
    @Column(name = "assunto")
    private String assunto;
    @Column(name = "remetente")
    private String remetente;
    @Column(name = "destinatario")
    private String destinatario;
    @Column(name = "cc")
    private String cc;
    @Column(name = "cco")
    private String cco;
    @Lob
    @Column(name = "mensagem")
    private String mensagem;
    @JoinColumn(name = "id_orcamento", referencedColumnName = "ID_ORCAMENTO")
    @ManyToOne
    private Orcamento idOrcamento;
    @JoinColumn(name = "id_usuario_emitente", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioEmitente;
    @JoinColumn(name = "id_usuario_destinatario", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioDestinatario;

    public Emails() {
    }

    public Emails(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public Date getDtEnvio() {
        return dtEnvio;
    }

    public void setDtEnvio(Date dtEnvio) {
        this.dtEnvio = dtEnvio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getRemetente() {
        return remetente;
    }

    public void setRemetente(String remetente) {
        this.remetente = remetente;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getCco() {
        return cco;
    }

    public void setCco(String cco) {
        this.cco = cco;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Orcamento getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(Orcamento idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public Usuario getIdUsuarioEmitente() {
        return idUsuarioEmitente;
    }

    public void setIdUsuarioEmitente(Usuario idUsuarioEmitente) {
        this.idUsuarioEmitente = idUsuarioEmitente;
    }

    public Usuario getIdUsuarioDestinatario() {
        return idUsuarioDestinatario;
    }

    public void setIdUsuarioDestinatario(Usuario idUsuarioDestinatario) {
        this.idUsuarioDestinatario = idUsuarioDestinatario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmail != null ? idEmail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Emails)) {
            return false;
        }
        Emails other = (Emails) object;
        if ((this.idEmail == null && other.idEmail != null) || (this.idEmail != null && !this.idEmail.equals(other.idEmail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.Emails[ idEmail=" + idEmail + " ]";
    }
    
}
