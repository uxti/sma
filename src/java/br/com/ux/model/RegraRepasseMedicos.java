/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author charl
 */
@Entity
@Table(name = "regra_repasse_medicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegraRepasseMedicos.findAll", query = "SELECT r FROM RegraRepasseMedicos r"),
    @NamedQuery(name = "RegraRepasseMedicos.findByIdRegraRepasseMedicos", query = "SELECT r FROM RegraRepasseMedicos r WHERE r.idRegraRepasseMedicos = :idRegraRepasseMedicos"),
    @NamedQuery(name = "RegraRepasseMedicos.findByIdTerceiro", query = "SELECT r FROM RegraRepasseMedicos r WHERE r.idTerceiro = :idTerceiro"),
    @NamedQuery(name = "RegraRepasseMedicos.findByFatorRepasseSecundario", query = "SELECT r FROM RegraRepasseMedicos r WHERE r.fatorRepasseSecundario = :fatorRepasseSecundario")})
public class RegraRepasseMedicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_regra_repasse_medicos")
    private Integer idRegraRepasseMedicos;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "fator_repasse_secundario")
    private Double fatorRepasseSecundario;
    @JoinColumn(name = "id_regra_repasse", referencedColumnName = "id_regra_repasse")
    @ManyToOne
    private RegraRepasse idRegraRepasse;
    
    @JoinColumn(name = "id_terceiro", referencedColumnName = "id_terceiro")
    @ManyToOne
    private Terceiro idTerceiro;

    public RegraRepasseMedicos() {
    }

    public RegraRepasseMedicos(Integer idRegraRepasseMedicos) {
        this.idRegraRepasseMedicos = idRegraRepasseMedicos;
    }

    public Integer getIdRegraRepasseMedicos() {
        return idRegraRepasseMedicos;
    }

    public void setIdRegraRepasseMedicos(Integer idRegraRepasseMedicos) {
        this.idRegraRepasseMedicos = idRegraRepasseMedicos;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }    

    public Double getFatorRepasseSecundario() {
        return fatorRepasseSecundario;
    }

    public void setFatorRepasseSecundario(Double fatorRepasseSecundario) {
        this.fatorRepasseSecundario = fatorRepasseSecundario;
    }

    public RegraRepasse getIdRegraRepasse() {
        return idRegraRepasse;
    }

    public void setIdRegraRepasse(RegraRepasse idRegraRepasse) {
        this.idRegraRepasse = idRegraRepasse;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegraRepasseMedicos != null ? idRegraRepasseMedicos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegraRepasseMedicos)) {
            return false;
        }
        RegraRepasseMedicos other = (RegraRepasseMedicos) object;
        if ((this.idRegraRepasseMedicos == null && other.idRegraRepasseMedicos != null) || (this.idRegraRepasseMedicos != null && !this.idRegraRepasseMedicos.equals(other.idRegraRepasseMedicos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.RegraRepasseMedicos[ idRegraRepasseMedicos=" + idRegraRepasseMedicos + " ]";
    }
    
}
