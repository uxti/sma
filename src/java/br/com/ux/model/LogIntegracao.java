/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "log_integracao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogIntegracao.findAll", query = "SELECT l FROM LogIntegracao l"),
    @NamedQuery(name = "LogIntegracao.findByIdLogIntegracao", query = "SELECT l FROM LogIntegracao l WHERE l.idLogIntegracao = :idLogIntegracao"),
    @NamedQuery(name = "LogIntegracao.findByMetodo", query = "SELECT l FROM LogIntegracao l WHERE l.metodo = :metodo"),
    @NamedQuery(name = "LogIntegracao.findByDtInicio", query = "SELECT l FROM LogIntegracao l WHERE l.dtInicio = :dtInicio"),
    @NamedQuery(name = "LogIntegracao.findByDtFim", query = "SELECT l FROM LogIntegracao l WHERE l.dtFim = :dtFim")})
public class LogIntegracao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_integracao")
    private Integer idLogIntegracao;
    @Column(name = "metodo")
    private String metodo;
    @Lob
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "dt_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtInicio;
    @Column(name = "dt_fim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtFim;
    @OneToMany(mappedBy = "idLogIntegracaoPai")
    private List<LogIntegracao> logIntegracaoList;
    @JoinColumn(name = "id_log_integracao_pai", referencedColumnName = "id_log_integracao")
    @ManyToOne
    private LogIntegracao idLogIntegracaoPai;

    public LogIntegracao() {
    }

    public LogIntegracao(Integer idLogIntegracao) {
        this.idLogIntegracao = idLogIntegracao;
    }

    public Integer getIdLogIntegracao() {
        return idLogIntegracao;
    }

    public void setIdLogIntegracao(Integer idLogIntegracao) {
        this.idLogIntegracao = idLogIntegracao;
    }

    public String getMetodo() {
        return metodo;
    }

    public void setMetodo(String metodo) {
        this.metodo = metodo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDtInicio() {
        return dtInicio;
    }

    public void setDtInicio(Date dtInicio) {
        this.dtInicio = dtInicio;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    @XmlTransient
    public List<LogIntegracao> getLogIntegracaoList() {
        return logIntegracaoList;
    }

    public void setLogIntegracaoList(List<LogIntegracao> logIntegracaoList) {
        this.logIntegracaoList = logIntegracaoList;
    }

    public LogIntegracao getIdLogIntegracaoPai() {
        return idLogIntegracaoPai;
    }

    public void setIdLogIntegracaoPai(LogIntegracao idLogIntegracaoPai) {
        this.idLogIntegracaoPai = idLogIntegracaoPai;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogIntegracao != null ? idLogIntegracao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogIntegracao)) {
            return false;
        }
        LogIntegracao other = (LogIntegracao) object;
        if ((this.idLogIntegracao == null && other.idLogIntegracao != null) || (this.idLogIntegracao != null && !this.idLogIntegracao.equals(other.idLogIntegracao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.LogIntegracao[ idLogIntegracao=" + idLogIntegracao + " ]";
    }
    
}
