/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "caixa_pai")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CaixaPai.findAll", query = "SELECT c FROM CaixaPai c"),
    @NamedQuery(name = "CaixaPai.findByIdCaixaPai", query = "SELECT c FROM CaixaPai c WHERE c.idCaixaPai = :idCaixaPai"),
    @NamedQuery(name = "CaixaPai.findByDtCaixaPai", query = "SELECT c FROM CaixaPai c WHERE c.dtCaixaPai = :dtCaixaPai"),
    @NamedQuery(name = "CaixaPai.findByTipoCaixa", query = "SELECT c FROM CaixaPai c WHERE c.tipoCaixa = :tipoCaixa"),
    @NamedQuery(name = "CaixaPai.findBySaldoAnterior", query = "SELECT c FROM CaixaPai c WHERE c.saldoAnterior = :saldoAnterior"),
    @NamedQuery(name = "CaixaPai.findBySaldo", query = "SELECT c FROM CaixaPai c WHERE c.saldo = :saldo"),
    @NamedQuery(name = "CaixaPai.findBySituacao", query = "SELECT c FROM CaixaPai c WHERE c.situacao = :situacao")})
public class CaixaPai implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CAIXA_PAI")
    private Integer idCaixaPai;
    @Column(name = "DT_CAIXA_PAI")
    @Temporal(TemporalType.DATE)
    private Date dtCaixaPai;
    @Column(name = "TIPO_CAIXA")
    private String tipoCaixa;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SALDO_ANTERIOR")
    private Double saldoAnterior;
    @Column(name = "SALDO")
    private Double saldo;
    @Column(name = "SITUACAO")
    private String situacao;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne(optional = false)
    private Usuario idUsuario;

    public CaixaPai() {
    }

    public CaixaPai(Integer idCaixaPai) {
        this.idCaixaPai = idCaixaPai;
    }

    public Integer getIdCaixaPai() {
        return idCaixaPai;
    }

    public void setIdCaixaPai(Integer idCaixaPai) {
        this.idCaixaPai = idCaixaPai;
    }

    public Date getDtCaixaPai() {
        return dtCaixaPai;
    }

    public void setDtCaixaPai(Date dtCaixaPai) {
        this.dtCaixaPai = dtCaixaPai;
    }

    public String getTipoCaixa() {
        return tipoCaixa;
    }

    public void setTipoCaixa(String tipoCaixa) {
        this.tipoCaixa = tipoCaixa;
    }

    public Double getSaldoAnterior() {
        return saldoAnterior;
    }

    public void setSaldoAnterior(Double saldoAnterior) {
        this.saldoAnterior = saldoAnterior;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCaixaPai != null ? idCaixaPai.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaixaPai)) {
            return false;
        }
        CaixaPai other = (CaixaPai) object;
        if ((this.idCaixaPai == null && other.idCaixaPai != null) || (this.idCaixaPai != null && !this.idCaixaPai.equals(other.idCaixaPai))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.CaixaPai[ idCaixaPai=" + idCaixaPai + " ]";
    }
    
}
