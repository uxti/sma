/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "texto_emails")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TextoEmails.findAll", query = "SELECT t FROM TextoEmails t"),
    @NamedQuery(name = "TextoEmails.findByIdTextoEmail", query = "SELECT t FROM TextoEmails t WHERE t.idTextoEmail = :idTextoEmail")})
public class TextoEmails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_texto_email")
    private Integer idTextoEmail;
    @Lob
    @Column(name = "texto")
    private String texto;
    @Column(name = "etiqueta")
    private String etiqueta;

    public TextoEmails() {
    }

    public TextoEmails(Integer idTextoEmail) {
        this.idTextoEmail = idTextoEmail;
    }

    public Integer getIdTextoEmail() {
        return idTextoEmail;
    }

    public void setIdTextoEmail(Integer idTextoEmail) {
        this.idTextoEmail = idTextoEmail;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTextoEmail != null ? idTextoEmail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TextoEmails)) {
            return false;
        }
        TextoEmails other = (TextoEmails) object;
        if ((this.idTextoEmail == null && other.idTextoEmail != null) || (this.idTextoEmail != null && !this.idTextoEmail.equals(other.idTextoEmail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.TextoEmails[ idTextoEmail=" + idTextoEmail + " ]";
    }
    
}
