/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "alerta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Alerta.findAll", query = "SELECT a FROM Alerta a"),
    @NamedQuery(name = "Alerta.findByIdAlerta", query = "SELECT a FROM Alerta a WHERE a.idAlerta = :idAlerta"),
    @NamedQuery(name = "Alerta.findByDescricao", query = "SELECT a FROM Alerta a WHERE a.descricao = :descricao"),
    @NamedQuery(name = "Alerta.findByData", query = "SELECT a FROM Alerta a WHERE a.data = :data"),
    @NamedQuery(name = "Alerta.findByDataAtualizacao", query = "SELECT a FROM Alerta a WHERE a.dataAtualizacao = :dataAtualizacao"),
    @NamedQuery(name = "Alerta.findByStatus", query = "SELECT a FROM Alerta a WHERE a.status = :status"),
    @NamedQuery(name = "Alerta.findByVisto", query = "SELECT a FROM Alerta a WHERE a.visto = :visto"),
    @NamedQuery(name = "Alerta.findByTipo", query = "SELECT a FROM Alerta a WHERE a.tipo = :tipo")})
public class Alerta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_alerta")
    private Integer idAlerta;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @Column(name = "data_atualizacao")
    @Temporal(TemporalType.DATE)
    private Date dataAtualizacao;
    @Column(name = "status")
    private String status;
    @Column(name = "visto")
    private String visto;
    @Column(name = "tipo")
    private String tipo;
    @JoinColumn(name = "id_usuario_destinatario", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioDestinatario;
    @JoinColumn(name = "id_usuario", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_repasse", referencedColumnName = "ID_REPASSE")
    @ManyToOne
    private Repasse idRepasse;
    @JoinColumn(name = "id_orcamento", referencedColumnName = "ID_ORCAMENTO")
    @ManyToOne
    private Orcamento idOrcamento;
    @JoinColumn(name = "id_exame", referencedColumnName = "ID_EXAME")
    @ManyToOne
    private Exame idExame;
    @JoinColumn(name = "id_cirurgia", referencedColumnName = "ID_CIRURGIA")
    @ManyToOne
    private Cirurgia idCirurgia;
    @JoinColumn(name = "id_cheque", referencedColumnName = "ID_CHEQUE")
    @ManyToOne
    private Cheque idCheque;

    public Alerta() {
    }

    public Alerta(Integer idAlerta) {
        this.idAlerta = idAlerta;
    }

    public Integer getIdAlerta() {
        return idAlerta;
    }

    public void setIdAlerta(Integer idAlerta) {
        this.idAlerta = idAlerta;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVisto() {
        return visto;
    }

    public void setVisto(String visto) {
        this.visto = visto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Usuario getIdUsuarioDestinatario() {
        return idUsuarioDestinatario;
    }

    public void setIdUsuarioDestinatario(Usuario idUsuarioDestinatario) {
        this.idUsuarioDestinatario = idUsuarioDestinatario;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Repasse getIdRepasse() {
        return idRepasse;
    }

    public void setIdRepasse(Repasse idRepasse) {
        this.idRepasse = idRepasse;
    }

    public Orcamento getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(Orcamento idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public Exame getIdExame() {
        return idExame;
    }

    public void setIdExame(Exame idExame) {
        this.idExame = idExame;
    }

    public Cirurgia getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Cirurgia idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    public Cheque getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Cheque idCheque) {
        this.idCheque = idCheque;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAlerta != null ? idAlerta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alerta)) {
            return false;
        }
        Alerta other = (Alerta) object;
        if ((this.idAlerta == null && other.idAlerta != null) || (this.idAlerta != null && !this.idAlerta.equals(other.idAlerta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Alerta[ idAlerta=" + idAlerta + " ]";
    }
    
}
