/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.tasy.AreaProcedimento;
import br.com.ux.model.tasy.EspecialidadeProcedimento;
import br.com.ux.model.tasy.GrupoProcedimentoCbhpm;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author charl
 */
@Entity
@Table(name = "regra_repasse")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegraRepasse.findAll", query = "SELECT r FROM RegraRepasse r"),
    @NamedQuery(name = "RegraRepasse.findByIdRegraRepasse", query = "SELECT r FROM RegraRepasse r WHERE r.idRegraRepasse = :idRegraRepasse"),
    @NamedQuery(name = "RegraRepasse.findByDescricao", query = "SELECT r FROM RegraRepasse r WHERE r.descricao = :descricao"),
    @NamedQuery(name = "RegraRepasse.findByFatorProcedimento", query = "SELECT r FROM RegraRepasse r WHERE r.fatorProcedimento = :fatorProcedimento"),
    @NamedQuery(name = "RegraRepasse.findByFatorMedico", query = "SELECT r FROM RegraRepasse r WHERE r.fatorMedico = :fatorMedico"),
    @NamedQuery(name = "RegraRepasse.findByDtCadastro", query = "SELECT r FROM RegraRepasse r WHERE r.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "RegraRepasse.findByDtAtualizacao", query = "SELECT r FROM RegraRepasse r WHERE r.dtAtualizacao = :dtAtualizacao"),
    @NamedQuery(name = "RegraRepasse.findByAtivo", query = "SELECT r FROM RegraRepasse r WHERE r.ativo = :ativo"),
    @NamedQuery(name = "RegraRepasse.findByRepasseSecundario", query = "SELECT r FROM RegraRepasse r WHERE r.repasseSecundario = :repasseSecundario"),
    @NamedQuery(name = "RegraRepasse.findByFatorTerceiroSecundario", query = "SELECT r FROM RegraRepasse r WHERE r.fatorTerceiroSecundario = :fatorTerceiroSecundario")})
public class RegraRepasse implements Serializable {

    @OneToMany(mappedBy = "idRegraRepasse")
    private List<Servico> servicoList;
    @OneToMany(mappedBy = "idRegraRepasse")
    private List<RegraRepasseMedicos> regraRepasseMedicosList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_regra_repasse")
    private Integer idRegraRepasse;
    @Size(max = 250)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "categoria_convenio")
    private String categoriaConvenio;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "fator_procedimento")
    private Double fatorProcedimento;
    @Column(name = "fator_medico")
    private Double fatorMedico;
    @Column(name = "valor_hospital")
    private Double valorHospital;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.DATE)
    private Date dtAtualizacao;
    @Column(name = "ativo")
    private Boolean ativo;
    @Column(name = "urgencia")
    private Boolean urgencia;
    @Column(name = "repasse_secundario")
    private Boolean repasseSecundario;
    @Column(name = "fator_terceiro_secundario")
    private Double fatorTerceiroSecundario;
    @JoinColumn(name = "id_terceiro_secundario", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiroSecundario;
    @JoinColumn(name = "id_servico", referencedColumnName = "ID_SERVICO")
    @ManyToOne
    private Servico idServico;
    @JoinColumn(name = "id_medico", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idMedico;
    @JoinColumn(name = "id_grupo_procedimento_cbhpm", referencedColumnName = "id_grupo_procedimento_cbhpm")
    @ManyToOne
    private GrupoProcedimentoCbhpm idGrupoProcedimentoCbhpm;
    @JoinColumn(name = "id_area_procedimento", referencedColumnName = "id_area_procedimento")
    @ManyToOne
    private AreaProcedimento idAreaProcedimento;
    @JoinColumn(name = "id_cbo_saude", referencedColumnName = "id_cbo_saude")
    @ManyToOne
    private CboSaude idCboSaude;
    @JoinColumn(name = "id_especialidade_procedimento", referencedColumnName = "id_especialidade_procedimento")
    @ManyToOne
    private EspecialidadeProcedimento idEspecialidadeProcedimento;

    public RegraRepasse() {
    }

    public RegraRepasse(Integer idRegraRepasse) {
        this.idRegraRepasse = idRegraRepasse;
    }

    public Integer getIdRegraRepasse() {
        return idRegraRepasse;
    }

    public void setIdRegraRepasse(Integer idRegraRepasse) {
        this.idRegraRepasse = idRegraRepasse;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getFatorProcedimento() {
        return fatorProcedimento;
    }

    public void setFatorProcedimento(Double fatorProcedimento) {
        this.fatorProcedimento = fatorProcedimento;
    }

    public Double getFatorMedico() {
        return fatorMedico;
    }

    public void setFatorMedico(Double fatorMedico) {
        this.fatorMedico = fatorMedico;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getRepasseSecundario() {
        return repasseSecundario;
    }

    public void setRepasseSecundario(Boolean repasseSecundario) {
        this.repasseSecundario = repasseSecundario;
    }

    public Double getFatorTerceiroSecundario() {
        return fatorTerceiroSecundario;
    }

    public void setFatorTerceiroSecundario(Double fatorTerceiroSecundario) {
        this.fatorTerceiroSecundario = fatorTerceiroSecundario;
    }

    public Terceiro getIdTerceiroSecundario() {
        return idTerceiroSecundario;
    }

    public void setIdTerceiroSecundario(Terceiro idTerceiroSecundario) {
        this.idTerceiroSecundario = idTerceiroSecundario;
    }

    public Servico getIdServico() {
        return idServico;
    }

    public void setIdServico(Servico idServico) {
        this.idServico = idServico;
    }

    public Terceiro getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Terceiro idMedico) {
        this.idMedico = idMedico;
    }

    public GrupoProcedimentoCbhpm getIdGrupoProcedimentoCbhpm() {
        return idGrupoProcedimentoCbhpm;
    }

    public void setIdGrupoProcedimentoCbhpm(GrupoProcedimentoCbhpm idGrupoProcedimentoCbhpm) {
        this.idGrupoProcedimentoCbhpm = idGrupoProcedimentoCbhpm;
    }

    public AreaProcedimento getIdAreaProcedimento() {
        return idAreaProcedimento;
    }

    public void setIdAreaProcedimento(AreaProcedimento idAreaProcedimento) {
        this.idAreaProcedimento = idAreaProcedimento;
    }

    public CboSaude getIdCboSaude() {
        return idCboSaude;
    }

    public void setIdCboSaude(CboSaude idCboSaude) {
        this.idCboSaude = idCboSaude;
    }

    public EspecialidadeProcedimento getIdEspecialidadeProcedimento() {
        return idEspecialidadeProcedimento;
    }

    public void setIdEspecialidadeProcedimento(EspecialidadeProcedimento idEspecialidadeProcedimento) {
        this.idEspecialidadeProcedimento = idEspecialidadeProcedimento;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegraRepasse != null ? idRegraRepasse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegraRepasse)) {
            return false;
        }
        RegraRepasse other = (RegraRepasse) object;
        if ((this.idRegraRepasse == null && other.idRegraRepasse != null) || (this.idRegraRepasse != null && !this.idRegraRepasse.equals(other.idRegraRepasse))) {
            return false;
        }
        return true;
    }
   
    @Override
    public String toString() {
        return "br.com.ux.model.RegraRepasse[ idRegraRepasse=" + idRegraRepasse + " ]";
    }

    @XmlTransient
    public List<Servico> getServicoList() {
        return servicoList;
    }

    public void setServicoList(List<Servico> servicoList) {
        this.servicoList = servicoList;
    }

    @XmlTransient
    public List<RegraRepasseMedicos> getRegraRepasseMedicosList() {
        return regraRepasseMedicosList;
    }

    public void setRegraRepasseMedicosList(List<RegraRepasseMedicos> regraRepasseMedicosList) {
        this.regraRepasseMedicosList = regraRepasseMedicosList;
    }

    public Double getValorHospital() {
        return valorHospital;
    }

    public void setValorHospital(Double valorHospital) {
        this.valorHospital = valorHospital;
    }

    public Boolean getUrgencia() {
        return urgencia;
    }

    public void setUrgencia(Boolean urgencia) {
        this.urgencia = urgencia;
    }

    public String getCategoriaConvenio() {
        return categoriaConvenio;
    }

    public void setCategoriaConvenio(String categoriaConvenio) {
        this.categoriaConvenio = categoriaConvenio;
    }
    
    
    
}
