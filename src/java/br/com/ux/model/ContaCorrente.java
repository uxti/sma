/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "conta_corrente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContaCorrente.findAll", query = "SELECT c FROM ContaCorrente c"),
    @NamedQuery(name = "ContaCorrente.findByIdContaCorrente", query = "SELECT c FROM ContaCorrente c WHERE c.idContaCorrente = :idContaCorrente"),
    @NamedQuery(name = "ContaCorrente.findByTipo", query = "SELECT c FROM ContaCorrente c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "ContaCorrente.findByObservacao", query = "SELECT c FROM ContaCorrente c WHERE c.observacao = :observacao"),
    @NamedQuery(name = "ContaCorrente.findByDtLancamento", query = "SELECT c FROM ContaCorrente c WHERE c.dtLancamento = :dtLancamento"),
    @NamedQuery(name = "ContaCorrente.findByDtEstorno", query = "SELECT c FROM ContaCorrente c WHERE c.dtEstorno = :dtEstorno"),
    @NamedQuery(name = "ContaCorrente.findByDtRetirada", query = "SELECT c FROM ContaCorrente c WHERE c.dtRetirada = :dtRetirada"),
    @NamedQuery(name = "ContaCorrente.findByDtLiberacao", query = "SELECT c FROM ContaCorrente c WHERE c.dtLiberacao = :dtLiberacao"),
    @NamedQuery(name = "ContaCorrente.findByIdCheque", query = "SELECT c FROM ContaCorrente c WHERE c.idCheque = :idCheque"),
    @NamedQuery(name = "ContaCorrente.findBySituacao", query = "SELECT c FROM ContaCorrente c WHERE c.situacao = :situacao"),
    @NamedQuery(name = "ContaCorrente.findByStatus", query = "SELECT c FROM ContaCorrente c WHERE c.status = :status"),
    @NamedQuery(name = "ContaCorrente.findByValor", query = "SELECT c FROM ContaCorrente c WHERE c.valor = :valor"),
    @NamedQuery(name = "ContaCorrente.findByIdSaque", query = "SELECT c FROM ContaCorrente c WHERE c.idSaque = :idSaque"),
    @NamedQuery(name = "ContaCorrente.findByCirurgiaidResponsavel", query = "SELECT c FROM ContaCorrente c WHERE c.cirurgiaidResponsavel = :cirurgiaidResponsavel"),
    @NamedQuery(name = "ContaCorrente.findByExameidDescricaoExame", query = "SELECT c FROM ContaCorrente c WHERE c.exameidDescricaoExame = :exameidDescricaoExame")})
public class ContaCorrente implements Serializable {

    @Size(max = 1000)
    @Column(name = "observacao")
    private String observacao;
    @Column(name = "inativo")
    private Boolean inativo;
    @Column(name = "taxado")
    private Boolean taxado;
    @Column(name = "taxa_deposito")
    private Boolean taxaDeposito;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSaque")
    private List<MovimentoCaixa> movimentoCaixaList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CONTA_CORRENTE")
    private Integer idContaCorrente;
    @Column(name = "TIPO")
    private String tipo;
    
    @Column(name = "DT_LANCAMENTO")
    @Temporal(TemporalType.DATE)
    private Date dtLancamento;
    @Column(name = "DT_ESTORNO")
    @Temporal(TemporalType.DATE)
    private Date dtEstorno;
    @Column(name = "DT_RETIRADA")
    @Temporal(TemporalType.DATE)
    private Date dtRetirada;
    @Column(name = "DT_PAGAMENTO_MEDICO")
    @Temporal(TemporalType.DATE)
    private Date dtPagamentoMedico;
    @Column(name = "DT_LIBERACAO")
    @Temporal(TemporalType.DATE)
    private Date dtLiberacao;
    @Column(name = "ID_CHEQUE")
    private Integer idCheque;
    @Column(name = "SITUACAO")
    private String situacao;
    @Column(name = "STATUS")
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "ID_SAQUE")
    private Integer idSaque;
    @Column(name = "CIRURGIAID_RESPONSAVEL")
    private Integer cirurgiaidResponsavel;
    @Column(name = "EXAMEID_DESCRICAO_EXAME")
    private Integer exameidDescricaoExame;
    @JoinColumn(name = "ID_REPASSE", referencedColumnName = "ID_REPASSE")
    @ManyToOne
    private Repasse idRepasse;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @JoinColumn(name = "ID_PACIENTE", referencedColumnName = "ID_PACIENTE")
    @ManyToOne
    private Paciente idPaciente;
    @JoinColumn(name = "ID_CIRURGIA", referencedColumnName = "ID_CIRURGIA")
    @ManyToOne
    private Cirurgia idCirurgia;
    @JoinColumn(name = "ID_EXAME", referencedColumnName = "ID_EXAME")
    @ManyToOne
    private Exame idExame;
    @JoinColumn(name = "ID_CAIXA", referencedColumnName = "ID_CAIXA")
    @ManyToOne
    private Caixa idCaixa;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "ID_CIRURGIA_ITEM", referencedColumnName = "ID_CIRURGIA_ITEM")
    @ManyToOne
    private CirurgiaItem idCirurgiaItem;

    public ContaCorrente() {
    }

    public ContaCorrente(Integer idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    public Integer getIdContaCorrente() {
        return idContaCorrente;
    }

    public void setIdContaCorrente(Integer idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Date getDtLancamento() {
        return dtLancamento;
    }

    public void setDtLancamento(Date dtLancamento) {
        this.dtLancamento = dtLancamento;
    }

    public Date getDtEstorno() {
        return dtEstorno;
    }

    public void setDtEstorno(Date dtEstorno) {
        this.dtEstorno = dtEstorno;
    }

    public Date getDtRetirada() {
        return dtRetirada;
    }

    public void setDtRetirada(Date dtRetirada) {
        this.dtRetirada = dtRetirada;
    }

    public Date getDtLiberacao() {
        return dtLiberacao;
    }

    public void setDtLiberacao(Date dtLiberacao) {
        this.dtLiberacao = dtLiberacao;
    }

    public Integer getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getIdSaque() {
        return idSaque;
    }

    public void setIdSaque(Integer idSaque) {
        this.idSaque = idSaque;
    }

    public Integer getCirurgiaidResponsavel() {
        return cirurgiaidResponsavel;
    }

    public void setCirurgiaidResponsavel(Integer cirurgiaidResponsavel) {
        this.cirurgiaidResponsavel = cirurgiaidResponsavel;
    }

    public Integer getExameidDescricaoExame() {
        return exameidDescricaoExame;
    }

    public void setExameidDescricaoExame(Integer exameidDescricaoExame) {
        this.exameidDescricaoExame = exameidDescricaoExame;
    }

    public Repasse getIdRepasse() {
        return idRepasse;
    }

    public void setIdRepasse(Repasse idRepasse) {
        this.idRepasse = idRepasse;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Cirurgia getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Cirurgia idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    public Exame getIdExame() {
        return idExame;
    }

    public void setIdExame(Exame idExame) {
        this.idExame = idExame;
    }

    public Caixa getIdCaixa() {
        return idCaixa;
    }

    public void setIdCaixa(Caixa idCaixa) {
        this.idCaixa = idCaixa;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public CirurgiaItem getIdCirurgiaItem() {
        return idCirurgiaItem;
    }

    public void setIdCirurgiaItem(CirurgiaItem idCirurgiaItem) {
        this.idCirurgiaItem = idCirurgiaItem;
    }

    public Date getDtPagamentoMedico() {
        return dtPagamentoMedico;
    }

    public void setDtPagamentoMedico(Date dtPagamentoMedico) {
        this.dtPagamentoMedico = dtPagamentoMedico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContaCorrente != null ? idContaCorrente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContaCorrente)) {
            return false;
        }
        ContaCorrente other = (ContaCorrente) object;
        if ((this.idContaCorrente == null && other.idContaCorrente != null) || (this.idContaCorrente != null && !this.idContaCorrente.equals(other.idContaCorrente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.ContaCorrente[ idContaCorrente=" + idContaCorrente + " ]";
    }


    public boolean isTaxaDeposito() {
        return taxaDeposito;
    }

    public void setTaxaDeposito(boolean taxaDeposito) {
        this.taxaDeposito = taxaDeposito;
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }

    public Boolean getTaxado() {
        return taxado;
    }

    public void setTaxado(Boolean taxado) {
        this.taxado = taxado;
    }

    public Boolean getTaxaDeposito() {
        return taxaDeposito;
    }

    public void setTaxaDeposito(Boolean taxaDeposito) {
        this.taxaDeposito = taxaDeposito;
    }

    @XmlTransient
    public List<MovimentoCaixa> getMovimentoCaixaList() {
        return movimentoCaixaList;
    }

    public void setMovimentoCaixaList(List<MovimentoCaixa> movimentoCaixaList) {
        this.movimentoCaixaList = movimentoCaixaList;
    }
    
    
    

}
