/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ux.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "plano_convenio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanoConvenio.findAll", query = "SELECT p FROM PlanoConvenio p"),
    @NamedQuery(name = "PlanoConvenio.findByIdPlanoConvenio", query = "SELECT p FROM PlanoConvenio p WHERE p.idPlanoConvenio = :idPlanoConvenio")})
public class PlanoConvenio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PLANO_CONVENIO")
    private Integer idPlanoConvenio;
    @JoinColumn(name = "ID_CONVENIO", referencedColumnName = "ID_CONVENIO")
    @ManyToOne(optional = false)
    private Convenio idConvenio;
    @JoinColumn(name = "ID_PLANO", referencedColumnName = "ID_PLANO")
    @ManyToOne(optional = false)
    private Plano idPlano;

    public PlanoConvenio() {
    }

    public PlanoConvenio(Integer idPlanoConvenio) {
        this.idPlanoConvenio = idPlanoConvenio;
    }

    public Integer getIdPlanoConvenio() {
        return idPlanoConvenio;
    }

    public void setIdPlanoConvenio(Integer idPlanoConvenio) {
        this.idPlanoConvenio = idPlanoConvenio;
    }

    public Convenio getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(Convenio idConvenio) {
        this.idConvenio = idConvenio;
    }

    public Plano getIdPlano() {
        return idPlano;
    }

    public void setIdPlano(Plano idPlano) {
        this.idPlano = idPlano;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanoConvenio != null ? idPlanoConvenio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanoConvenio)) {
            return false;
        }
        PlanoConvenio other = (PlanoConvenio) object;
        if ((this.idPlanoConvenio == null && other.idPlanoConvenio != null) || (this.idPlanoConvenio != null && !this.idPlanoConvenio.equals(other.idPlanoConvenio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.PlanoConvenio[ idPlanoConvenio=" + idPlanoConvenio + " ]";
    }

}
