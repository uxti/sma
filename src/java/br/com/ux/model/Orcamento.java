/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.tasy.AtendimentoPaciente;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "orcamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orcamento.findAll", query = "SELECT o FROM Orcamento o"),
    @NamedQuery(name = "Orcamento.findByIdOrcamento", query = "SELECT o FROM Orcamento o WHERE o.idOrcamento = :idOrcamento"),
    @NamedQuery(name = "Orcamento.findByDtOrcamento", query = "SELECT o FROM Orcamento o WHERE o.dtOrcamento = :dtOrcamento"),
    @NamedQuery(name = "Orcamento.findByDtValidade", query = "SELECT o FROM Orcamento o WHERE o.dtValidade = :dtValidade"),
    @NamedQuery(name = "Orcamento.findByDtPrevisao", query = "SELECT o FROM Orcamento o WHERE o.dtPrevisao = :dtPrevisao"),
    @NamedQuery(name = "Orcamento.findByDtCancelamento", query = "SELECT o FROM Orcamento o WHERE o.dtCancelamento = :dtCancelamento"),
    @NamedQuery(name = "Orcamento.findByDtExecucao", query = "SELECT o FROM Orcamento o WHERE o.dtExecucao = :dtExecucao"),
    @NamedQuery(name = "Orcamento.findByFormaPagamento", query = "SELECT o FROM Orcamento o WHERE o.formaPagamento = :formaPagamento"),
    @NamedQuery(name = "Orcamento.findByStatus", query = "SELECT o FROM Orcamento o WHERE o.status = :status"),
    @NamedQuery(name = "Orcamento.findByValor", query = "SELECT o FROM Orcamento o WHERE o.valor = :valor"),
    @NamedQuery(name = "Orcamento.findByGerarPagamentoResponsavel", query = "SELECT o FROM Orcamento o WHERE o.gerarPagamentoResponsavel = :gerarPagamentoResponsavel"),
    @NamedQuery(name = "Orcamento.findByValorEntrada", query = "SELECT o FROM Orcamento o WHERE o.valorEntrada = :valorEntrada"),
    @NamedQuery(name = "Orcamento.findBySituacao", query = "SELECT o FROM Orcamento o WHERE o.situacao = :situacao"),
    @NamedQuery(name = "Orcamento.findByResponsavel", query = "SELECT o FROM Orcamento o WHERE o.responsavel = :responsavel"),
    @NamedQuery(name = "Orcamento.findByPacienteNaoCadastradado", query = "SELECT o FROM Orcamento o WHERE o.pacienteNaoCadastradado = :pacienteNaoCadastradado")})
public class Orcamento implements Serializable {

    @Column(name = "dt_agendado")
    @Temporal(TemporalType.DATE)
    private Date dtAgendado;
    @Column(name = "hora_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horaCadastro;

    @OneToMany(mappedBy = "idOrcamento")
    private List<Emails> emailsList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ORCAMENTO")
    private Integer idOrcamento;
    @Column(name = "DT_ORCAMENTO")
    @Temporal(TemporalType.DATE)
    private Date dtOrcamento;
    @Column(name = "DT_VALIDADE")
    @Temporal(TemporalType.DATE)
    private Date dtValidade;
    @Column(name = "DT_PREVISAO")
    @Temporal(TemporalType.DATE)
    private Date dtPrevisao;
    @Column(name = "DT_ADIANTAMENTO")
    @Temporal(TemporalType.DATE)
    private Date dtAdiantamento;
    @Column(name = "DT_CANCELAMENTO")
    @Temporal(TemporalType.DATE)
    private Date dtCancelamento;
    @Column(name = "dt_modificacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtModificacao;
    @Column(name = "DT_EXECUCAO")
    @Temporal(TemporalType.DATE)
    private Date dtExecucao;
    @Column(name = "FORMA_PAGAMENTO")
    private String formaPagamento;
    @Lob
    @Column(name = "OBSERVACAO")
    private String observacao;
    @Column(name = "STATUS")
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "GERAR_PAGAMENTO_RESPONSAVEL")
    private String gerarPagamentoResponsavel;
    @Lob
    @Column(name = "SUB_MOTIVO")
    private String subMotivo;
    @Column(name = "valor_entrada")
    private Double valorEntrada;
    @Column(name = "SITUACAO")
    private String situacao;
    @Column(name = "RESPONSAVEL")
    private String responsavel;
    @Column(name = "paciente_nao_cadastradado")
    private String pacienteNaoCadastradado;
    @Column(name = "dt_envio_email")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEnvioEmail;
    @Column(name = "dt_recebimento_email_opme")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtRecebimentoEmailOpme;
    @Column(name = "dt_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dtNascimento;
    @Column(name = "telefone")
    private String telefone;
    @OneToMany(mappedBy = "idOrcamento")
    private List<Alerta> alertaList;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @JoinColumn(name = "id_status", referencedColumnName = "id_status")
    @ManyToOne
    private Status idStatus;
    @JoinColumn(name = "ID_RESPONSAVEL", referencedColumnName = "ID_PACIENTE")
    @ManyToOne
    private Paciente idResponsavel;
    @JoinColumn(name = "ID_TIPO_INTERNACAO", referencedColumnName = "ID_TIPO_INTERNACAO")
    @ManyToOne
    private TipoInternacao idTipoInternacao;
    @JoinColumn(name = "ID_MOTIVO", referencedColumnName = "ID_MOTIVO")
    @ManyToOne
    private Motivo idMotivo;
    @JoinColumn(name = "ID_CONVENIO", referencedColumnName = "ID_CONVENIO")
    @ManyToOne
    private Convenio idConvenio;
    @JoinColumn(name = "ID_UNIDADE", referencedColumnName = "ID_UNIDADE")
    @ManyToOne
    private UnidadeAtendimento idUnidade;
    @JoinColumn(name = "ID_PACIENTE", referencedColumnName = "ID_PACIENTE")
    @ManyToOne
    private Paciente idPaciente;
    @JoinColumn(name = "ID_PROCEDIMENTO", referencedColumnName = "ID_PROCEDIMENTO")
    @ManyToOne
    private Procedimento idProcedimento;
    @JoinColumn(name = "ID_ADIANTAMENTO_HISTORICO", referencedColumnName = "ID_ADIANTAMENTO_HISTORICO")
    @ManyToOne
    private AdiantamentosHistorico idAdiantamentoHistorico;
    @JoinColumn(name = "ID_FORMA", referencedColumnName = "ID_FORMA")
    @ManyToOne
    private FormaPagamento idForma;
    @JoinColumn(name = "ID_PLANO", referencedColumnName = "ID_PLANO")
    @ManyToOne
    private Plano idPlano;
    @JoinColumn(name = "ID_USUARIO_CRIACAO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioCriacao;
    @JoinColumn(name = "ID_USUARIO_ULTIMA_ALTERACAO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioUltimaAlteracao;
    @JoinColumn(name = "ID_USUARIO_CANCELAMENTO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioCancelamento;
    @JoinColumn(name = "ID_USUARIO_ADIANTAMENTO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioAdiantamento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idOrcamento")
    private List<OrcamentoItem> orcamentoItemList;
    @OneToMany(mappedBy = "idOrcamento")
    private List<AdiantamentosHistorico> adiantamentosHistoricoList;
    @OneToMany(mappedBy = "idOrcamento")
    private List<Cirurgia> cirurgiaList;
    @JoinColumn(name = "id_atendimento_paciente", referencedColumnName = "id_atendimento_paciente")
    @ManyToOne
    private AtendimentoPaciente idAtendimentoPaciente;

    public Orcamento() {
    }

    public Orcamento(Integer idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public Integer getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(Integer idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public Date getDtOrcamento() {
        return dtOrcamento;
    }

    public void setDtOrcamento(Date dtOrcamento) {
        this.dtOrcamento = dtOrcamento;
    }

    public Date getDtValidade() {
        return dtValidade;
    }

    public void setDtValidade(Date dtValidade) {
        this.dtValidade = dtValidade;
    }

    public Date getDtAdiantamento() {
        return dtAdiantamento;
    }

    public void setDtAdiantamento(Date dtAdiantamento) {
        this.dtAdiantamento = dtAdiantamento;
    }

    public Date getDtPrevisao() {
        return dtPrevisao;
    }

    public void setDtPrevisao(Date dtPrevisao) {
        this.dtPrevisao = dtPrevisao;
    }

    public Date getDtCancelamento() {
        return dtCancelamento;
    }

    public void setDtCancelamento(Date dtCancelamento) {
        this.dtCancelamento = dtCancelamento;
    }

    public Date getDtModificacao() {
        return dtModificacao;
    }

    public void setDtModificacao(Date dtModificacao) {
        this.dtModificacao = dtModificacao;
    }

    public Date getDtExecucao() {
        return dtExecucao;
    }

    public void setDtExecucao(Date dtExecucao) {
        this.dtExecucao = dtExecucao;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getGerarPagamentoResponsavel() {
        return gerarPagamentoResponsavel;
    }

    public void setGerarPagamentoResponsavel(String gerarPagamentoResponsavel) {
        this.gerarPagamentoResponsavel = gerarPagamentoResponsavel;
    }

    public String getSubMotivo() {
        return subMotivo;
    }

    public void setSubMotivo(String subMotivo) {
        this.subMotivo = subMotivo;
    }

    public Double getValorEntrada() {
        return valorEntrada;
    }

    public void setValorEntrada(Double valorEntrada) {
        this.valorEntrada = valorEntrada;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getPacienteNaoCadastradado() {
        return pacienteNaoCadastradado;
    }

    public void setPacienteNaoCadastradado(String pacienteNaoCadastradado) {
        this.pacienteNaoCadastradado = pacienteNaoCadastradado;
    }

    @XmlTransient
    public List<Alerta> getAlertaList() {
        return alertaList;
    }

    public void setAlertaList(List<Alerta> alertaList) {
        this.alertaList = alertaList;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public Paciente getIdResponsavel() {
        return idResponsavel;
    }

    public void setIdResponsavel(Paciente idResponsavel) {
        this.idResponsavel = idResponsavel;
    }

    public TipoInternacao getIdTipoInternacao() {
        return idTipoInternacao;
    }

    public void setIdTipoInternacao(TipoInternacao idTipoInternacao) {
        this.idTipoInternacao = idTipoInternacao;
    }

    public Motivo getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Motivo idMotivo) {
        this.idMotivo = idMotivo;
    }

    public Convenio getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(Convenio idConvenio) {
        this.idConvenio = idConvenio;
    }

    public UnidadeAtendimento getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(UnidadeAtendimento idUnidade) {
        this.idUnidade = idUnidade;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Procedimento getIdProcedimento() {
        return idProcedimento;
    }

    public void setIdProcedimento(Procedimento idProcedimento) {
        this.idProcedimento = idProcedimento;
    }

    public AdiantamentosHistorico getIdAdiantamentoHistorico() {
        return idAdiantamentoHistorico;
    }

    public void setIdAdiantamentoHistorico(AdiantamentosHistorico idAdiantamentoHistorico) {
        this.idAdiantamentoHistorico = idAdiantamentoHistorico;
    }

    public FormaPagamento getIdForma() {
        return idForma;
    }

    public void setIdForma(FormaPagamento idForma) {
        this.idForma = idForma;
    }

    public Plano getIdPlano() {
        return idPlano;
    }

    public void setIdPlano(Plano idPlano) {
        this.idPlano = idPlano;
    }

    public Usuario getIdUsuarioCriacao() {
        return idUsuarioCriacao;
    }

    public void setIdUsuarioCriacao(Usuario idUsuarioCriacao) {
        this.idUsuarioCriacao = idUsuarioCriacao;
    }

    public Usuario getIdUsuarioUltimaAlteracao() {
        return idUsuarioUltimaAlteracao;
    }

    public void setIdUsuarioUltimaAlteracao(Usuario idUsuarioUltimaAlteracao) {
        this.idUsuarioUltimaAlteracao = idUsuarioUltimaAlteracao;
    }

    public Usuario getIdUsuarioAdiantamento() {
        return idUsuarioAdiantamento;
    }

    public void setIdUsuarioAdiantamento(Usuario idUsuarioAdiantamento) {
        this.idUsuarioAdiantamento = idUsuarioAdiantamento;
    }

    public Usuario getIdUsuarioCancelamento() {
        return idUsuarioCancelamento;
    }

    public void setIdUsuarioCancelamento(Usuario idUsuarioCancelamento) {
        this.idUsuarioCancelamento = idUsuarioCancelamento;
    }

    @XmlTransient
    public List<OrcamentoItem> getOrcamentoItemList() {
        return orcamentoItemList;
    }

    public void setOrcamentoItemList(List<OrcamentoItem> orcamentoItemList) {
        this.orcamentoItemList = orcamentoItemList;
    }

    @XmlTransient
    public List<AdiantamentosHistorico> getAdiantamentosHistoricoList() {
        return adiantamentosHistoricoList;
    }

    public void setAdiantamentosHistoricoList(List<AdiantamentosHistorico> adiantamentosHistoricoList) {
        this.adiantamentosHistoricoList = adiantamentosHistoricoList;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrcamento != null ? idOrcamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orcamento)) {
            return false;
        }
        Orcamento other = (Orcamento) object;
        if ((this.idOrcamento == null && other.idOrcamento != null) || (this.idOrcamento != null && !this.idOrcamento.equals(other.idOrcamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Orcamento[ idOrcamento=" + idOrcamento + " ]";
    }

    public Status getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Status idStatus) {
        this.idStatus = idStatus;
    }

    public Date getDtEnvioEmail() {
        return dtEnvioEmail;
    }

    public void setDtEnvioEmail(Date dtEnvioEmail) {
        this.dtEnvioEmail = dtEnvioEmail;
    }

    public Date getDtRecebimentoEmailOpme() {
        return dtRecebimentoEmailOpme;
    }

    public void setDtRecebimentoEmailOpme(Date dtRecebimentoEmailOpme) {
        this.dtRecebimentoEmailOpme = dtRecebimentoEmailOpme;
    }

    public Date getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(Date dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @XmlTransient
    public List<Emails> getEmailsList() {
        return emailsList;
    }

    public void setEmailsList(List<Emails> emailsList) {
        this.emailsList = emailsList;
    }

    public AtendimentoPaciente getIdAtendimentoPaciente() {
        return idAtendimentoPaciente;
    }

    public void setIdAtendimentoPaciente(AtendimentoPaciente idAtendimentoPaciente) {
        this.idAtendimentoPaciente = idAtendimentoPaciente;
    }

    public Date getDtAgendado() {
        return dtAgendado;
    }

    public void setDtAgendado(Date dtAgendado) {
        this.dtAgendado = dtAgendado;
    }

    public Date getHoraCadastro() {
        return horaCadastro;
    }

    public void setHoraCadastro(Date horaCadastro) {
        this.horaCadastro = horaCadastro;
    }
    
    
}
