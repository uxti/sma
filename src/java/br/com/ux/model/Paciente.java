/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.tasy.AtendimentoPaciente;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p"),
    @NamedQuery(name = "Paciente.findByIdPaciente", query = "SELECT p FROM Paciente p WHERE p.idPaciente = :idPaciente"),
    @NamedQuery(name = "Paciente.findByNome", query = "SELECT p FROM Paciente p WHERE p.nome = :nome"),
    @NamedQuery(name = "Paciente.findByCpf", query = "SELECT p FROM Paciente p WHERE p.cpf = :cpf"),
    @NamedQuery(name = "Paciente.findByRg", query = "SELECT p FROM Paciente p WHERE p.rg = :rg"),
    @NamedQuery(name = "Paciente.findByDtNascimento", query = "SELECT p FROM Paciente p WHERE p.dtNascimento = :dtNascimento"),
    @NamedQuery(name = "Paciente.findByTelefone", query = "SELECT p FROM Paciente p WHERE p.telefone = :telefone"),
    @NamedQuery(name = "Paciente.findByTelefoneCelular", query = "SELECT p FROM Paciente p WHERE p.telefoneCelular = :telefoneCelular"),
    @NamedQuery(name = "Paciente.findByCep", query = "SELECT p FROM Paciente p WHERE p.cep = :cep"),
    @NamedQuery(name = "Paciente.findByLogradouro", query = "SELECT p FROM Paciente p WHERE p.logradouro = :logradouro"),
    @NamedQuery(name = "Paciente.findByNumero", query = "SELECT p FROM Paciente p WHERE p.numero = :numero"),
    @NamedQuery(name = "Paciente.findByBairro", query = "SELECT p FROM Paciente p WHERE p.bairro = :bairro"),
    @NamedQuery(name = "Paciente.findByCidade", query = "SELECT p FROM Paciente p WHERE p.cidade = :cidade"),
    @NamedQuery(name = "Paciente.findByUf", query = "SELECT p FROM Paciente p WHERE p.uf = :uf"),
    @NamedQuery(name = "Paciente.findByComplemento", query = "SELECT p FROM Paciente p WHERE p.complemento = :complemento"),
    @NamedQuery(name = "Paciente.findBySexo", query = "SELECT p FROM Paciente p WHERE p.sexo = :sexo"),
    @NamedQuery(name = "Paciente.findByDtCadastro", query = "SELECT p FROM Paciente p WHERE p.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "Paciente.findByTipoLogradouro", query = "SELECT p FROM Paciente p WHERE p.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Paciente.findByNomeMae", query = "SELECT p FROM Paciente p WHERE p.nomeMae = :nomeMae"),
    @NamedQuery(name = "Paciente.findByNomePai", query = "SELECT p FROM Paciente p WHERE p.nomePai = :nomePai")})
public class Paciente implements Serializable {

    @OneToMany(mappedBy = "idPaciente")
    private List<AtendimentoPaciente> atendimentoPacienteList;
    @Column(name = "inativo")
    private Boolean inativo = Boolean.FALSE;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PACIENTE")
    private Integer idPaciente;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "CPF")
    private String cpf;
    @Column(name = "RG")
    private String rg;
    @Column(name = "DT_NASCIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dtNascimento;
    @Column(name = "TELEFONE")
    private String telefone;
    @Column(name = "TELEFONE_CELULAR")
    private String telefoneCelular;
    @Column(name = "CEP")
    private String cep;
    @Column(name = "LOGRADOURO")
    private String logradouro;
    @Column(name = "NUMERO")
    private Integer numero;
    @Column(name = "BAIRRO")
    private String bairro;
    @Column(name = "CIDADE")
    private String cidade;
    @Column(name = "UF")
    private String uf;
    @Column(name = "COMPLEMENTO")
    private String complemento;
    @Column(name = "SEXO")
    private Character sexo;
    @Column(name = "DT_CADASTRO")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @Column(name = "TIPO_LOGRADOURO")
    private String tipoLogradouro;
    @Column(name = "NOME_MAE")
    private String nomeMae;
    @Column(name = "NOME_PAI")
    private String nomePai;
    @Column(name = "id_paciente_tasy")
    private Integer idPacienteTasy;    
    @OneToMany(mappedBy = "idResponsavel")
    private List<Orcamento> orcamentoList;
    @OneToMany(mappedBy = "idPaciente")
    private List<Orcamento> orcamentoList1;
    @OneToMany(mappedBy = "idPaciente")
    private List<Repasse> repasseList;
    @OneToMany(mappedBy = "idPaciente")
    private List<ContaCorrente> contaCorrenteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPaciente")
    private List<Exame> exameList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPaciente")
    private List<PacienteResponsavel> pacienteResponsavelList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPacienteResponsavel")
    private List<PacienteResponsavel> pacienteResponsavelList1;
    @OneToMany(mappedBy = "idPaciente")
    private List<Cirurgia> cirurgiaList;
    @OneToMany(mappedBy = "idResponsavel")
    private List<Cirurgia> cirurgiaList1;
    @OneToMany(mappedBy = "idPaciente")
    private List<Cheque> chequeList;

    public Paciente() {
    }

    public Paciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Integer getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Date getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(Date dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public String getNomePai() {
        return nomePai;
    }

    public void setNomePai(String nomePai) {
        this.nomePai = nomePai;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList1() {
        return orcamentoList1;
    }

    public void setOrcamentoList1(List<Orcamento> orcamentoList1) {
        this.orcamentoList1 = orcamentoList1;
    }

    @XmlTransient
    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    @XmlTransient
    public List<Exame> getExameList() {
        return exameList;
    }

    public void setExameList(List<Exame> exameList) {
        this.exameList = exameList;
    }

    @XmlTransient
    public List<PacienteResponsavel> getPacienteResponsavelList() {
        return pacienteResponsavelList;
    }

    public void setPacienteResponsavelList(List<PacienteResponsavel> pacienteResponsavelList) {
        this.pacienteResponsavelList = pacienteResponsavelList;
    }

    @XmlTransient
    public List<PacienteResponsavel> getPacienteResponsavelList1() {
        return pacienteResponsavelList1;
    }

    public void setPacienteResponsavelList1(List<PacienteResponsavel> pacienteResponsavelList1) {
        this.pacienteResponsavelList1 = pacienteResponsavelList1;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList1() {
        return cirurgiaList1;
    }

    public void setCirurgiaList1(List<Cirurgia> cirurgiaList1) {
        this.cirurgiaList1 = cirurgiaList1;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPaciente != null ? idPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paciente)) {
            return false;
        }
        Paciente other = (Paciente) object;
        if ((this.idPaciente == null && other.idPaciente != null) || (this.idPaciente != null && !this.idPaciente.equals(other.idPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Paciente[ idPaciente=" + idPaciente + " ]";
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }

    public Integer getIdPacienteTasy() {
        return idPacienteTasy;
    }

    public void setIdPacienteTasy(Integer idPacienteTasy) {
        this.idPacienteTasy = idPacienteTasy;
    }

    @XmlTransient
    public List<AtendimentoPaciente> getAtendimentoPacienteList() {
        return atendimentoPacienteList;
    }

    public void setAtendimentoPacienteList(List<AtendimentoPaciente> atendimentoPacienteList) {
        this.atendimentoPacienteList = atendimentoPacienteList;
    }
    
    
    
}
