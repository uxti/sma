/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model.tasy;

import br.com.ux.model.CirurgiaItem;
import br.com.ux.model.RegraRepasse;
import br.com.ux.model.Servico;
import br.com.ux.model.Terceiro;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "procedimento_paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcedimentoPaciente.findAll", query = "SELECT p FROM ProcedimentoPaciente p"),
    @NamedQuery(name = "ProcedimentoPaciente.findByIdProcedimentoPaciente", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.idProcedimentoPaciente = :idProcedimentoPaciente"),
    @NamedQuery(name = "ProcedimentoPaciente.findByDtCadastro", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "ProcedimentoPaciente.findByDtAtualizacao", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.dtAtualizacao = :dtAtualizacao"),
    @NamedQuery(name = "ProcedimentoPaciente.findByQtdProcedimento", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.qtdProcedimento = :qtdProcedimento"),
    @NamedQuery(name = "ProcedimentoPaciente.findByValorTotal", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.valorTotal = :valorTotal"),
    @NamedQuery(name = "ProcedimentoPaciente.findByValorProcedimento", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.valorProcedimento = :valorProcedimento"),
    @NamedQuery(name = "ProcedimentoPaciente.findByValorMedico", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.valorMedico = :valorMedico"),
    @NamedQuery(name = "ProcedimentoPaciente.findByStatusSync", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.statusSync = :statusSync"),
    @NamedQuery(name = "ProcedimentoPaciente.findByIdProcedimentoPacienteTasy", query = "SELECT p FROM ProcedimentoPaciente p WHERE p.idProcedimentoPacienteTasy = :idProcedimentoPacienteTasy")})
public class ProcedimentoPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_procedimento_paciente")
    private Integer idProcedimentoPaciente;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.DATE)
    private Date dtAtualizacao;
    @Column(name = "dt_laudo")
    @Temporal(TemporalType.DATE)
    private Date dtLaudo;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "qtd_procedimento")
    private Double qtdProcedimento;
    @Column(name = "valor_total")
    private Double valorTotal;
    @Column(name = "valor_procedimento")
    private Double valorProcedimento;
    @Column(name = "valor_medico")
    private Double valorMedico;
    @Column(name = "valor_rateio_secundario")
    private Double valorRateioSecundario;
    @Column(name = "cd_motivo_exc_conta")
    private Integer cdMotivoExcConta;
    @Column(name = "status_sync")
    private Integer statusSync;
    @Column(name = "NR_SEQ_PROC_PACOTE")
    private Integer nrSeqProcPacote;
    @Column(name = "id_procedimento_paciente_tasy")
    private Integer idProcedimentoPacienteTasy;

    @JoinColumn(name = "id_medico", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idMedico;
    
    @JoinColumn(name = "id_medico_laudo", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idMedicoLaudo;

    @JoinColumn(name = "id_conta_paciente", referencedColumnName = "id_conta_paciente")
    @ManyToOne
    private ContaPaciente idContaPaciente;

    @JoinColumn(name = "id_servico", referencedColumnName = "ID_SERVICO")
    @ManyToOne
    private Servico idServico;

    @JoinColumn(name = "id_regra_repasse", referencedColumnName = "id_regra_repasse")
    @ManyToOne
    private RegraRepasse idRegraRepasse;

    @OneToMany(mappedBy = "idProcedimentoPaciente")
    private List<CirurgiaItem> CirurgiaItemList;

    public ProcedimentoPaciente() {
    }

    public ProcedimentoPaciente(Integer idProcedimentoPaciente) {
        this.idProcedimentoPaciente = idProcedimentoPaciente;
    }

    public Integer getIdProcedimentoPaciente() {
        return idProcedimentoPaciente;
    }

    public void setIdProcedimentoPaciente(Integer idProcedimentoPaciente) {
        this.idProcedimentoPaciente = idProcedimentoPaciente;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    public Double getQtdProcedimento() {
        return qtdProcedimento;
    }

    public void setQtdProcedimento(Double qtdProcedimento) {
        this.qtdProcedimento = qtdProcedimento;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getValorProcedimento() {
        return valorProcedimento;
    }

    public void setValorProcedimento(Double valorProcedimento) {
        this.valorProcedimento = valorProcedimento;
    }

    public Double getValorMedico() {
        return valorMedico;
    }

    public void setValorMedico(Double valorMedico) {
        this.valorMedico = valorMedico;
    }

    public Integer getStatusSync() {
        return statusSync;
    }

    public void setStatusSync(Integer statusSync) {
        this.statusSync = statusSync;
    }

    public Integer getIdProcedimentoPacienteTasy() {
        return idProcedimentoPacienteTasy;
    }

    public void setIdProcedimentoPacienteTasy(Integer idProcedimentoPacienteTasy) {
        this.idProcedimentoPacienteTasy = idProcedimentoPacienteTasy;
    }

    public Terceiro getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Terceiro idMedico) {
        this.idMedico = idMedico;
    }

    public ContaPaciente getIdContaPaciente() {
        return idContaPaciente;
    }

    public void setIdContaPaciente(ContaPaciente idContaPaciente) {
        this.idContaPaciente = idContaPaciente;
    }

    public Servico getIdServico() {
        return idServico;
    }

    public void setIdServico(Servico idServico) {
        this.idServico = idServico;
    }

    public List<CirurgiaItem> getCirurgiaItemList() {
        return CirurgiaItemList;
    }

    public void setCirurgiaItemList(List<CirurgiaItem> CirurgiaItemList) {
        this.CirurgiaItemList = CirurgiaItemList;
    }

    public Integer getCdMotivoExcConta() {
        return cdMotivoExcConta;
    }

    public void setCdMotivoExcConta(Integer cdMotivoExcConta) {
        this.cdMotivoExcConta = cdMotivoExcConta;
    }

    public RegraRepasse getIdRegraRepasse() {
        return idRegraRepasse;
    }

    public void setIdRegraRepasse(RegraRepasse idRegraRepasse) {
        this.idRegraRepasse = idRegraRepasse;
    }

    public Double getValorRateioSecundario() {
        return valorRateioSecundario;
    }

    public Integer getNrSeqProcPacote() {
        return nrSeqProcPacote;
    }

    public void setNrSeqProcPacote(Integer nrSeqProcPacote) {
        this.nrSeqProcPacote = nrSeqProcPacote;
    }
       

    public void setValorRateioSecundario(Double valorRateioSecundario) {
        this.valorRateioSecundario = valorRateioSecundario;
    }

    public Date getDtLaudo() {
        return dtLaudo;
    }

    public void setDtLaudo(Date dtLaudo) {
        this.dtLaudo = dtLaudo;
    }

    public Terceiro getIdMedicoLaudo() {
        return idMedicoLaudo;
    }

    public void setIdMedicoLaudo(Terceiro idMedicoLaudo) {
        this.idMedicoLaudo = idMedicoLaudo;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProcedimentoPaciente != null ? idProcedimentoPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcedimentoPaciente)) {
            return false;
        }
        ProcedimentoPaciente other = (ProcedimentoPaciente) object;
        if ((this.idProcedimentoPaciente == null && other.idProcedimentoPaciente != null) || (this.idProcedimentoPaciente != null && !this.idProcedimentoPaciente.equals(other.idProcedimentoPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.tasy.ProcedimentoPaciente[ idProcedimentoPaciente=" + idProcedimentoPaciente + " ]";
    }

}
