/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model.tasy;

import br.com.ux.model.Orcamento;
import br.com.ux.model.Servico;
import br.com.ux.model.Paciente;
import br.com.ux.model.Terceiro;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "atendimento_paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtendimentoPaciente.findAll", query = "SELECT a FROM AtendimentoPaciente a"),
    @NamedQuery(name = "AtendimentoPaciente.findByIdAtendimentoPaciente", query = "SELECT a FROM AtendimentoPaciente a WHERE a.idAtendimentoPaciente = :idAtendimentoPaciente"),
    @NamedQuery(name = "AtendimentoPaciente.findByNumeroAtendimentoPacienteTasy", query = "SELECT a FROM AtendimentoPaciente a WHERE a.numeroAtendimentoPacienteTasy = :numeroAtendimentoPacienteTasy"),
    @NamedQuery(name = "AtendimentoPaciente.findByStatus", query = "SELECT a FROM AtendimentoPaciente a WHERE a.status = :status"),
    @NamedQuery(name = "AtendimentoPaciente.findByDtEntrada", query = "SELECT a FROM AtendimentoPaciente a WHERE a.dtEntrada = :dtEntrada"),
    @NamedQuery(name = "AtendimentoPaciente.findByTipoAtendimento", query = "SELECT a FROM AtendimentoPaciente a WHERE a.tipoAtendimento = :tipoAtendimento"),
    @NamedQuery(name = "AtendimentoPaciente.findByDescricaoTipoAtendimento", query = "SELECT a FROM AtendimentoPaciente a WHERE a.descricaoTipoAtendimento = :descricaoTipoAtendimento"),
    @NamedQuery(name = "AtendimentoPaciente.findByCategoria", query = "SELECT a FROM AtendimentoPaciente a WHERE a.categoria = :categoria"),
    @NamedQuery(name = "AtendimentoPaciente.findByDtEncerramento", query = "SELECT a FROM AtendimentoPaciente a WHERE a.dtEncerramento = :dtEncerramento"),
    @NamedQuery(name = "AtendimentoPaciente.findByDtInternacao", query = "SELECT a FROM AtendimentoPaciente a WHERE a.dtInternacao = :dtInternacao"),
    @NamedQuery(name = "AtendimentoPaciente.findByStatusSync", query = "SELECT a FROM AtendimentoPaciente a WHERE a.statusSync = :statusSync")})
public class AtendimentoPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_atendimento_paciente")
    private Integer idAtendimentoPaciente;
    @Column(name = "numero_atendimento_paciente_tasy")
    private Integer numeroAtendimentoPacienteTasy;
    @Column(name = "status")
    private String status;
    @Column(name = "dt_entrada")
    @Temporal(TemporalType.DATE)
    private Date dtEntrada;
    @Column(name = "tipo_atendimento")
    private String tipoAtendimento;
    @Column(name = "descricao_tipo_atendimento")
    private String descricaoTipoAtendimento;
    @Column(name = "categoria")
    private String categoria;
    @Column(name = "dt_encerramento")
    @Temporal(TemporalType.DATE)
    private Date dtEncerramento;
    @Column(name = "dt_internacao")
    @Temporal(TemporalType.DATE)
    private Date dtInternacao;
    @Column(name = "status_sync")
    private Integer statusSync;
    @Lob
    @Column(name = "observacao")
    private String observacao;
    @JoinColumn(name = "id_medico_resp", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idMedicoResp;
    @JoinColumn(name = "id_paciente", referencedColumnName = "ID_PACIENTE")
    @ManyToOne
    private Paciente idPaciente;
    @JoinColumn(name = "id_servico", referencedColumnName = "ID_SERVICO")
    @ManyToOne
    private Servico idServico;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAtendimentoPaciente")
    private List<ContaPaciente> contaPacienteList;
    @OneToMany(mappedBy = "idAtendimentoPaciente")
    private List<Orcamento> orcamentoList;
    @Column(name = "urgencia")
    private Boolean urgencia;

    public AtendimentoPaciente() {
    }

    public AtendimentoPaciente(Integer idAtendimentoPaciente) {
        this.idAtendimentoPaciente = idAtendimentoPaciente;
    }

    public Integer getIdAtendimentoPaciente() {
        return idAtendimentoPaciente;
    }

    public void setIdAtendimentoPaciente(Integer idAtendimentoPaciente) {
        this.idAtendimentoPaciente = idAtendimentoPaciente;
    }

    public Integer getNumeroAtendimentoPacienteTasy() {
        return numeroAtendimentoPacienteTasy;
    }

    public void setNumeroAtendimentoPacienteTasy(Integer numeroAtendimentoPacienteTasy) {
        this.numeroAtendimentoPacienteTasy = numeroAtendimentoPacienteTasy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDtEntrada() {
        return dtEntrada;
    }

    public void setDtEntrada(Date dtEntrada) {
        this.dtEntrada = dtEntrada;
    }

    public String getTipoAtendimento() {
        return tipoAtendimento;
    }

    public void setTipoAtendimento(String tipoAtendimento) {
        this.tipoAtendimento = tipoAtendimento;
    }

    public String getDescricaoTipoAtendimento() {
        return descricaoTipoAtendimento;
    }

    public void setDescricaoTipoAtendimento(String descricaoTipoAtendimento) {
        this.descricaoTipoAtendimento = descricaoTipoAtendimento;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Date getDtEncerramento() {
        return dtEncerramento;
    }

    public void setDtEncerramento(Date dtEncerramento) {
        this.dtEncerramento = dtEncerramento;
    }

    public Date getDtInternacao() {
        return dtInternacao;
    }

    public void setDtInternacao(Date dtInternacao) {
        this.dtInternacao = dtInternacao;
    }

    public Integer getStatusSync() {
        return statusSync;
    }

    public void setStatusSync(Integer statusSync) {
        this.statusSync = statusSync;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Terceiro getIdMedicoResp() {
        return idMedicoResp;
    }

    public void setIdMedicoResp(Terceiro idMedicoResp) {
        this.idMedicoResp = idMedicoResp;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Servico getIdServico() {
        return idServico;
    }

    public void setIdServico(Servico idServico) {
        this.idServico = idServico;
    }

    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }
    
    

    @XmlTransient
    public List<ContaPaciente> getContaPacienteList() {
        return contaPacienteList;
    }

    public void setContaPacienteList(List<ContaPaciente> contaPacienteList) {
        this.contaPacienteList = contaPacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtendimentoPaciente != null ? idAtendimentoPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtendimentoPaciente)) {
            return false;
        }
        AtendimentoPaciente other = (AtendimentoPaciente) object;
        if ((this.idAtendimentoPaciente == null && other.idAtendimentoPaciente != null) || (this.idAtendimentoPaciente != null && !this.idAtendimentoPaciente.equals(other.idAtendimentoPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.tasy.AtendimentoPaciente[ idAtendimentoPaciente=" + idAtendimentoPaciente + " ]";
    }

    public Boolean getUrgencia() {
        return urgencia;
    }

    public void setUrgencia(Boolean urgencia) {
        this.urgencia = urgencia;
    }
    
    
    
}
