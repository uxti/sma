/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model.tasy;

import br.com.ux.model.Cirurgia;
import br.com.ux.model.Terceiro;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "conta_paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContaPaciente.findAll", query = "SELECT c FROM ContaPaciente c"),
    @NamedQuery(name = "ContaPaciente.findByIdContaPaciente", query = "SELECT c FROM ContaPaciente c WHERE c.idContaPaciente = :idContaPaciente"),
    @NamedQuery(name = "ContaPaciente.findByNumeroAtendimentoPacienteTasy", query = "SELECT c FROM ContaPaciente c WHERE c.numeroAtendimentoPacienteTasy = :numeroAtendimentoPacienteTasy"),
    @NamedQuery(name = "ContaPaciente.findByIdContaPacienteTasy", query = "SELECT c FROM ContaPaciente c WHERE c.idContaPacienteTasy = :idContaPacienteTasy"),
    @NamedQuery(name = "ContaPaciente.findByDtPeriodoInical", query = "SELECT c FROM ContaPaciente c WHERE c.dtPeriodoInical = :dtPeriodoInical"),
    @NamedQuery(name = "ContaPaciente.findByDtAtualizacao", query = "SELECT c FROM ContaPaciente c WHERE c.dtAtualizacao = :dtAtualizacao"),
    @NamedQuery(name = "ContaPaciente.findByDtAcerto", query = "SELECT c FROM ContaPaciente c WHERE c.dtAcerto = :dtAcerto"),
    @NamedQuery(name = "ContaPaciente.findByStatusSync", query = "SELECT c FROM ContaPaciente c WHERE c.statusSync = :statusSync"),
    @NamedQuery(name = "ContaPaciente.findByValorTotal", query = "SELECT c FROM ContaPaciente c WHERE c.valorTotal = :valorTotal"),
    @NamedQuery(name = "ContaPaciente.findByValorProcedimento", query = "SELECT c FROM ContaPaciente c WHERE c.valorProcedimento = :valorProcedimento"),
    @NamedQuery(name = "ContaPaciente.findByValorMaterial", query = "SELECT c FROM ContaPaciente c WHERE c.valorMaterial = :valorMaterial"),
    @NamedQuery(name = "ContaPaciente.findByValorMedico", query = "SELECT c FROM ContaPaciente c WHERE c.valorMedico = :valorMedico"),
    @NamedQuery(name = "ContaPaciente.findByStatus", query = "SELECT c FROM ContaPaciente c WHERE c.status = :status"),
    @NamedQuery(name = "ContaPaciente.findByValorDiaria", query = "SELECT c FROM ContaPaciente c WHERE c.valorDiaria = :valorDiaria")})
public class ContaPaciente implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idContaPaciente")
    private List<ProcedimentoPaciente> procedimentoPacienteList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_conta_paciente")
    private Integer idContaPaciente;
    @Column(name = "numero_atendimento_paciente_tasy")
    private Integer numeroAtendimentoPacienteTasy;
    @Column(name = "id_conta_paciente_tasy")
    private Integer idContaPacienteTasy;
    @Column(name = "dt_periodo_inical")
    @Temporal(TemporalType.DATE)
    private Date dtPeriodoInical;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.DATE)
    private Date dtAtualizacao;
    @Column(name = "dt_acerto")
    @Temporal(TemporalType.DATE)
    private Date dtAcerto;
    @Column(name = "status_sync")
    private Integer statusSync;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_total")
    private Double valorTotal;
    @Column(name = "valor_procedimento")
    private Double valorProcedimento;
    @Column(name = "valor_material")
    private Double valorMaterial;
    @Column(name = "valor_medico")
    private Double valorMedico;
    @Column(name = "status")
    private String status;
    @Column(name = "valor_diaria")
    private Double valorDiaria;
    @Column(name = "cancelada")
    private Boolean cancelada;
    @JoinColumn(name = "id_atendimento_paciente", referencedColumnName = "id_atendimento_paciente")
    @ManyToOne(optional = false)
    private AtendimentoPaciente idAtendimentoPaciente;
    @JoinColumn(name = "cd_medico_executor", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro cdMedicoExecutor;
    @OneToMany(mappedBy = "idContaPaciente")
    private List<Cirurgia> cirurgiaList;

    public ContaPaciente() {
    }

    public ContaPaciente(Integer idContaPaciente) {
        this.idContaPaciente = idContaPaciente;
    }

    public Integer getIdContaPaciente() {
        return idContaPaciente;
    }

    public void setIdContaPaciente(Integer idContaPaciente) {
        this.idContaPaciente = idContaPaciente;
    }

    public Integer getNumeroAtendimentoPacienteTasy() {
        return numeroAtendimentoPacienteTasy;
    }

    public void setNumeroAtendimentoPacienteTasy(Integer numeroAtendimentoPacienteTasy) {
        this.numeroAtendimentoPacienteTasy = numeroAtendimentoPacienteTasy;
    }

    public Integer getIdContaPacienteTasy() {
        return idContaPacienteTasy;
    }

    public void setIdContaPacienteTasy(Integer idContaPacienteTasy) {
        this.idContaPacienteTasy = idContaPacienteTasy;
    }

    public Date getDtPeriodoInical() {
        return dtPeriodoInical;
    }

    public void setDtPeriodoInical(Date dtPeriodoInical) {
        this.dtPeriodoInical = dtPeriodoInical;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    public Date getDtAcerto() {
        return dtAcerto;
    }

    public void setDtAcerto(Date dtAcerto) {
        this.dtAcerto = dtAcerto;
    }

    public Integer getStatusSync() {
        return statusSync;
    }

    public void setStatusSync(Integer statusSync) {
        this.statusSync = statusSync;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getValorProcedimento() {
        return valorProcedimento;
    }

    public void setValorProcedimento(Double valorProcedimento) {
        this.valorProcedimento = valorProcedimento;
    }

    public Double getValorMaterial() {
        return valorMaterial;
    }

    public void setValorMaterial(Double valorMaterial) {
        this.valorMaterial = valorMaterial;
    }

    public Double getValorMedico() {
        return valorMedico;
    }

    public void setValorMedico(Double valorMedico) {
        this.valorMedico = valorMedico;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getValorDiaria() {
        return valorDiaria;
    }

    public void setValorDiaria(Double valorDiaria) {
        this.valorDiaria = valorDiaria;
    }

    public AtendimentoPaciente getIdAtendimentoPaciente() {
        return idAtendimentoPaciente;
    }

    public void setIdAtendimentoPaciente(AtendimentoPaciente idAtendimentoPaciente) {
        this.idAtendimentoPaciente = idAtendimentoPaciente;
    }

    public Terceiro getCdMedicoExecutor() {
        return cdMedicoExecutor;
    }

    public void setCdMedicoExecutor(Terceiro cdMedicoExecutor) {
        this.cdMedicoExecutor = cdMedicoExecutor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContaPaciente != null ? idContaPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContaPaciente)) {
            return false;
        }
        ContaPaciente other = (ContaPaciente) object;
        if ((this.idContaPaciente == null && other.idContaPaciente != null) || (this.idContaPaciente != null && !this.idContaPaciente.equals(other.idContaPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.tasy.ContaPaciente[ idContaPaciente=" + idContaPaciente + " ]";
    }

    @XmlTransient
    public List<ProcedimentoPaciente> getProcedimentoPacienteList() {
        return procedimentoPacienteList;
    }

    public void setProcedimentoPacienteList(List<ProcedimentoPaciente> procedimentoPacienteList) {
        this.procedimentoPacienteList = procedimentoPacienteList;
    }

    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    public Boolean getCancelada() {
        return cancelada;
    }

    public void setCancelada(Boolean cancelada) {
        this.cancelada = cancelada;
    }
    
    
    
    
    
}
