/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model.tasy;

import br.com.ux.model.RegraRepasse;
import br.com.ux.model.Servico;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "grupo_procedimento_cbhpm")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GrupoProcedimentoCbhpm.findAll", query = "SELECT g FROM GrupoProcedimentoCbhpm g"),
    @NamedQuery(name = "GrupoProcedimentoCbhpm.findByIdGrupoProcedimentoCbhpm", query = "SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.idGrupoProcedimentoCbhpm = :idGrupoProcedimentoCbhpm"),
    @NamedQuery(name = "GrupoProcedimentoCbhpm.findByIdGrupoProcedimentoTasy", query = "SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.idGrupoProcedimentoTasy = :idGrupoProcedimentoTasy"),
    @NamedQuery(name = "GrupoProcedimentoCbhpm.findByCapitulo", query = "SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.capitulo = :capitulo"),
    @NamedQuery(name = "GrupoProcedimentoCbhpm.findByOrigem", query = "SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.origem = :origem"),
    @NamedQuery(name = "GrupoProcedimentoCbhpm.findByAtivo", query = "SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.ativo = :ativo"),
    @NamedQuery(name = "GrupoProcedimentoCbhpm.findByDtCadastro", query = "SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "GrupoProcedimentoCbhpm.findByDtAtualizacao", query = "SELECT g FROM GrupoProcedimentoCbhpm g WHERE g.dtAtualizacao = :dtAtualizacao")})
public class GrupoProcedimentoCbhpm implements Serializable {

    @OneToMany(mappedBy = "idGrupoProcedimentoCbhpm")
    private List<RegraRepasse> regraRepasseList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_grupo_procedimento_cbhpm")
    private Integer idGrupoProcedimentoCbhpm;
    @Column(name = "id_grupo_procedimento_tasy")
    private Integer idGrupoProcedimentoTasy;
    @Lob
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "capitulo")
    private String capitulo;
    @Column(name = "origem")
    private String origem;
    @Column(name = "ativo")
    private Boolean ativo;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.DATE)
    private Date dtAtualizacao;
    @OneToMany(mappedBy = "idGrupoProcedimentoCbhpm")
    private List<Servico> servicoList;
    @JoinColumn(name = "id_especialidade_procedimento", referencedColumnName = "id_especialidade_procedimento")
    @ManyToOne(optional = false)
    private EspecialidadeProcedimento idEspecialidadeProcedimento;

    public GrupoProcedimentoCbhpm() {
    }

    public GrupoProcedimentoCbhpm(Integer idGrupoProcedimentoCbhpm) {
        this.idGrupoProcedimentoCbhpm = idGrupoProcedimentoCbhpm;
    }

    public Integer getIdGrupoProcedimentoCbhpm() {
        return idGrupoProcedimentoCbhpm;
    }

    public void setIdGrupoProcedimentoCbhpm(Integer idGrupoProcedimentoCbhpm) {
        this.idGrupoProcedimentoCbhpm = idGrupoProcedimentoCbhpm;
    }

    public Integer getIdGrupoProcedimentoTasy() {
        return idGrupoProcedimentoTasy;
    }

    public void setIdGrupoProcedimentoTasy(Integer idGrupoProcedimentoTasy) {
        this.idGrupoProcedimentoTasy = idGrupoProcedimentoTasy;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCapitulo() {
        return capitulo;
    }

    public void setCapitulo(String capitulo) {
        this.capitulo = capitulo;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    @XmlTransient
    public List<Servico> getServicoList() {
        return servicoList;
    }

    public void setServicoList(List<Servico> servicoList) {
        this.servicoList = servicoList;
    }

    public EspecialidadeProcedimento getIdEspecialidadeProcedimento() {
        return idEspecialidadeProcedimento;
    }

    public void setIdEspecialidadeProcedimento(EspecialidadeProcedimento idEspecialidadeProcedimento) {
        this.idEspecialidadeProcedimento = idEspecialidadeProcedimento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupoProcedimentoCbhpm != null ? idGrupoProcedimentoCbhpm.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoProcedimentoCbhpm)) {
            return false;
        }
        GrupoProcedimentoCbhpm other = (GrupoProcedimentoCbhpm) object;
        if ((this.idGrupoProcedimentoCbhpm == null && other.idGrupoProcedimentoCbhpm != null) || (this.idGrupoProcedimentoCbhpm != null && !this.idGrupoProcedimentoCbhpm.equals(other.idGrupoProcedimentoCbhpm))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.tasy.GrupoProcedimentoCbhpm[ idGrupoProcedimentoCbhpm=" + idGrupoProcedimentoCbhpm + " ]";
    }

    @XmlTransient
    public List<RegraRepasse> getRegraRepasseList() {
        return regraRepasseList;
    }

    public void setRegraRepasseList(List<RegraRepasse> regraRepasseList) {
        this.regraRepasseList = regraRepasseList;
    }
    
}
