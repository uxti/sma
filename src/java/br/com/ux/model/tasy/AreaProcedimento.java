/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model.tasy;

import br.com.ux.model.RegraRepasse;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "area_procedimento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AreaProcedimento.findAll", query = "SELECT a FROM AreaProcedimento a"),
    @NamedQuery(name = "AreaProcedimento.findByIdAreaProcedimento", query = "SELECT a FROM AreaProcedimento a WHERE a.idAreaProcedimento = :idAreaProcedimento"),
    @NamedQuery(name = "AreaProcedimento.findByIdAreaProcedimentoTasy", query = "SELECT a FROM AreaProcedimento a WHERE a.idAreaProcedimentoTasy = :idAreaProcedimentoTasy"),
    @NamedQuery(name = "AreaProcedimento.findByDescricao", query = "SELECT a FROM AreaProcedimento a WHERE a.descricao = :descricao"),
    @NamedQuery(name = "AreaProcedimento.findByAtivo", query = "SELECT a FROM AreaProcedimento a WHERE a.ativo = :ativo"),
    @NamedQuery(name = "AreaProcedimento.findByDtCadastro", query = "SELECT a FROM AreaProcedimento a WHERE a.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "AreaProcedimento.findByDtAtualizacao", query = "SELECT a FROM AreaProcedimento a WHERE a.dtAtualizacao = :dtAtualizacao")})
public class AreaProcedimento implements Serializable {

    @OneToMany(mappedBy = "idAreaProcedimento")
    private List<RegraRepasse> regraRepasseList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_area_procedimento")
    private Integer idAreaProcedimento;
    @Column(name = "id_area_procedimento_tasy")
    private Integer idAreaProcedimentoTasy;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "ativo")
    private Boolean ativo;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.DATE)
    private Date dtAtualizacao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAreaProcedimento")
    private List<EspecialidadeProcedimento> especialidadeProcedimentoList;

    public AreaProcedimento() {
    }

    public AreaProcedimento(Integer idAreaProcedimento) {
        this.idAreaProcedimento = idAreaProcedimento;
    }

    public Integer getIdAreaProcedimento() {
        return idAreaProcedimento;
    }

    public void setIdAreaProcedimento(Integer idAreaProcedimento) {
        this.idAreaProcedimento = idAreaProcedimento;
    }

    public Integer getIdAreaProcedimentoTasy() {
        return idAreaProcedimentoTasy;
    }

    public void setIdAreaProcedimentoTasy(Integer idAreaProcedimentoTasy) {
        this.idAreaProcedimentoTasy = idAreaProcedimentoTasy;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    @XmlTransient
    public List<EspecialidadeProcedimento> getEspecialidadeProcedimentoList() {
        return especialidadeProcedimentoList;
    }

    public void setEspecialidadeProcedimentoList(List<EspecialidadeProcedimento> especialidadeProcedimentoList) {
        this.especialidadeProcedimentoList = especialidadeProcedimentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAreaProcedimento != null ? idAreaProcedimento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AreaProcedimento)) {
            return false;
        }
        AreaProcedimento other = (AreaProcedimento) object;
        if ((this.idAreaProcedimento == null && other.idAreaProcedimento != null) || (this.idAreaProcedimento != null && !this.idAreaProcedimento.equals(other.idAreaProcedimento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.tasy.AreaProcedimento[ idAreaProcedimento=" + idAreaProcedimento + " ]";
    }

    @XmlTransient
    public List<RegraRepasse> getRegraRepasseList() {
        return regraRepasseList;
    }

    public void setRegraRepasseList(List<RegraRepasse> regraRepasseList) {
        this.regraRepasseList = regraRepasseList;
    }
    
}
