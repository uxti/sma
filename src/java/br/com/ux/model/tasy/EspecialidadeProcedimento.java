/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model.tasy;

import br.com.ux.model.RegraRepasse;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "especialidade_procedimento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EspecialidadeProcedimento.findAll", query = "SELECT e FROM EspecialidadeProcedimento e"),
    @NamedQuery(name = "EspecialidadeProcedimento.findByIdEspecialidadeProcedimento", query = "SELECT e FROM EspecialidadeProcedimento e WHERE e.idEspecialidadeProcedimento = :idEspecialidadeProcedimento"),
    @NamedQuery(name = "EspecialidadeProcedimento.findByIdEspecialidadeProcedimentoTasy", query = "SELECT e FROM EspecialidadeProcedimento e WHERE e.idEspecialidadeProcedimentoTasy = :idEspecialidadeProcedimentoTasy"),
    @NamedQuery(name = "EspecialidadeProcedimento.findByDescricao", query = "SELECT e FROM EspecialidadeProcedimento e WHERE e.descricao = :descricao"),
    @NamedQuery(name = "EspecialidadeProcedimento.findByAtivo", query = "SELECT e FROM EspecialidadeProcedimento e WHERE e.ativo = :ativo"),
    @NamedQuery(name = "EspecialidadeProcedimento.findByDtCadastro", query = "SELECT e FROM EspecialidadeProcedimento e WHERE e.dtCadastro = :dtCadastro")})
public class EspecialidadeProcedimento implements Serializable {

    @OneToMany(mappedBy = "idEspecialidadeProcedimento")
    private List<RegraRepasse> regraRepasseList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_especialidade_procedimento")
    private Integer idEspecialidadeProcedimento;
    @Column(name = "id_especialidade_procedimento_tasy")
    private Integer idEspecialidadeProcedimentoTasy;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "ativo")
    private Boolean ativo;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @JoinColumn(name = "id_area_procedimento", referencedColumnName = "id_area_procedimento")
    @ManyToOne(optional = false)
    private AreaProcedimento idAreaProcedimento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEspecialidadeProcedimento")
    private List<GrupoProcedimentoCbhpm> grupoProcedimentoCbhpmList;

    public EspecialidadeProcedimento() {
    }

    public EspecialidadeProcedimento(Integer idEspecialidadeProcedimento) {
        this.idEspecialidadeProcedimento = idEspecialidadeProcedimento;
    }

    public Integer getIdEspecialidadeProcedimento() {
        return idEspecialidadeProcedimento;
    }

    public void setIdEspecialidadeProcedimento(Integer idEspecialidadeProcedimento) {
        this.idEspecialidadeProcedimento = idEspecialidadeProcedimento;
    }

    public Integer getIdEspecialidadeProcedimentoTasy() {
        return idEspecialidadeProcedimentoTasy;
    }

    public void setIdEspecialidadeProcedimentoTasy(Integer idEspecialidadeProcedimentoTasy) {
        this.idEspecialidadeProcedimentoTasy = idEspecialidadeProcedimentoTasy;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public AreaProcedimento getIdAreaProcedimento() {
        return idAreaProcedimento;
    }

    public void setIdAreaProcedimento(AreaProcedimento idAreaProcedimento) {
        this.idAreaProcedimento = idAreaProcedimento;
    }

    @XmlTransient
    public List<GrupoProcedimentoCbhpm> getGrupoProcedimentoCbhpmList() {
        return grupoProcedimentoCbhpmList;
    }

    public void setGrupoProcedimentoCbhpmList(List<GrupoProcedimentoCbhpm> grupoProcedimentoCbhpmList) {
        this.grupoProcedimentoCbhpmList = grupoProcedimentoCbhpmList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEspecialidadeProcedimento != null ? idEspecialidadeProcedimento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EspecialidadeProcedimento)) {
            return false;
        }
        EspecialidadeProcedimento other = (EspecialidadeProcedimento) object;
        if ((this.idEspecialidadeProcedimento == null && other.idEspecialidadeProcedimento != null) || (this.idEspecialidadeProcedimento != null && !this.idEspecialidadeProcedimento.equals(other.idEspecialidadeProcedimento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.tasy.EspecialidadeProcedimento[ idEspecialidadeProcedimento=" + idEspecialidadeProcedimento + " ]";
    }

    @XmlTransient
    public List<RegraRepasse> getRegraRepasseList() {
        return regraRepasseList;
    }

    public void setRegraRepasseList(List<RegraRepasse> regraRepasseList) {
        this.regraRepasseList = regraRepasseList;
    }
    
}
