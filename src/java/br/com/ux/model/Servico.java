/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.tasy.AtendimentoPaciente;
import br.com.ux.model.tasy.GrupoProcedimentoCbhpm;
import br.com.ux.model.tasy.ProcedimentoPaciente;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "servico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servico.findAll", query = "SELECT s FROM Servico s"),
    @NamedQuery(name = "Servico.findByIdServico", query = "SELECT s FROM Servico s WHERE s.idServico = :idServico"),
    @NamedQuery(name = "Servico.findByDescricao", query = "SELECT s FROM Servico s WHERE s.descricao = :descricao"),
    @NamedQuery(name = "Servico.findByTipo", query = "SELECT s FROM Servico s WHERE s.tipo = :tipo"),
    @NamedQuery(name = "Servico.findByEscala", query = "SELECT s FROM Servico s WHERE s.escala = :escala"),
    @NamedQuery(name = "Servico.findByPadrao", query = "SELECT s FROM Servico s WHERE s.padrao = :padrao"),
    @NamedQuery(name = "Servico.findByInativo", query = "SELECT s FROM Servico s WHERE s.inativo = :inativo"),
    @NamedQuery(name = "Servico.findByLancamentoAvulso", query = "SELECT s FROM Servico s WHERE s.lancamentoAvulso = :lancamentoAvulso"),
    @NamedQuery(name = "Servico.findByClassificacao", query = "SELECT s FROM Servico s WHERE s.classificacao = :classificacao"),
    @NamedQuery(name = "Servico.findByDtCadastro", query = "SELECT s FROM Servico s WHERE s.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "Servico.findByDtAtualizacao", query = "SELECT s FROM Servico s WHERE s.dtAtualizacao = :dtAtualizacao"),
    @NamedQuery(name = "Servico.findByIdProcedimentoTasy", query = "SELECT s FROM Servico s WHERE s.idProcedimentoTasy = :idProcedimentoTasy")})
public class Servico implements Serializable {

    @Column(name = "cadastro_demonstrativo")
    private Boolean cadastroDemonstrativo;
    @JoinColumn(name = "id_regra_repasse", referencedColumnName = "id_regra_repasse")
    @ManyToOne
    private RegraRepasse idRegraRepasse;

    @Column(name = "cbhpm")
    private Boolean cbhpm;
    @OneToMany(mappedBy = "idServico")
    private List<RegraRepasse> regraRepasseList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SERVICO")
    private Integer idServico;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "TIPO")
    private String tipo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ESCALA")
    private Double escala;
    @Column(name = "PADRAO")
    private String padrao;
    @Column(name = "inativo")
    private Boolean inativo;
    @Column(name = "lancamento_avulso")
    private Boolean lancamentoAvulso;
    @Lob
    @Column(name = "descricao_cbhpm")
    private String descricaoCbhpm;
    @Column(name = "classificacao")
    private String classificacao;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.DATE)
    private Date dtAtualizacao;
    @Column(name = "id_procedimento_tasy")
    private Integer idProcedimentoTasy;
    
    @Column(name = "nome_procedimento_interno_tasy")
    private String nomeProcedimentoInternoTasy;
    @OneToMany(mappedBy = "idServico")
    private List<AtendimentoPaciente> atendimentoPacienteList;
    @JoinColumn(name = "id_grupo_procedimento_cbhpm", referencedColumnName = "id_grupo_procedimento_cbhpm")
    @ManyToOne
    private GrupoProcedimentoCbhpm idGrupoProcedimentoCbhpm;

    @OneToMany(mappedBy = "idServico")
    private List<ServicoItem> servicoItemList;
    @OneToMany(mappedBy = "idServico")
    private List<CirurgiaItem> cirurgiaItemList;

    @OneToMany(mappedBy = "idServico")
    private List<OrcamentoItem> orcamentoItemList;

    public Servico() {
    }

    public List<ServicoItem> getServicoItemList() {
        return servicoItemList;
    }

    public void setServicoItemList(List<ServicoItem> servicoItemList) {
        this.servicoItemList = servicoItemList;
    }

    public Servico(Integer idServico) {
        this.idServico = idServico;
    }

    public Integer getIdServico() {
        return idServico;
    }

    public void setIdServico(Integer idServico) {
        this.idServico = idServico;
    }

    public String getDescricao() {
        if (getNomeProcedimentoInternoTasy() != null) {
            if (getNomeProcedimentoInternoTasy().equals("")) {
                return getNomeProcedimentoInternoTasy();
            } else {
                return descricao;
            }
        } else {
            return descricao;
        }
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getEscala() {
        return escala;
    }

    public void setEscala(Double escala) {
        this.escala = escala;
    }

    public String getPadrao() {
        return padrao;
    }

    public void setPadrao(String padrao) {
        this.padrao = padrao;
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }

    public Boolean getLancamentoAvulso() {
        return lancamentoAvulso;
    }

    public void setLancamentoAvulso(Boolean lancamentoAvulso) {
        this.lancamentoAvulso = lancamentoAvulso;
    }

    public String getDescricaoCbhpm() {
        return descricaoCbhpm;
    }

    public void setDescricaoCbhpm(String descricaoCbhpm) {
        this.descricaoCbhpm = descricaoCbhpm;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    public Integer getIdProcedimentoTasy() {
        return idProcedimentoTasy;
    }

    public void setIdProcedimentoTasy(Integer idProcedimentoTasy) {
        this.idProcedimentoTasy = idProcedimentoTasy;
    }

    @XmlTransient
    public List<AtendimentoPaciente> getAtendimentoPacienteList() {
        return atendimentoPacienteList;
    }

    public void setAtendimentoPacienteList(List<AtendimentoPaciente> atendimentoPacienteList) {
        this.atendimentoPacienteList = atendimentoPacienteList;
    }

    public GrupoProcedimentoCbhpm getIdGrupoProcedimentoCbhpm() {
        return idGrupoProcedimentoCbhpm;
    }

    public void setIdGrupoProcedimentoCbhpm(GrupoProcedimentoCbhpm idGrupoProcedimentoCbhpm) {
        this.idGrupoProcedimentoCbhpm = idGrupoProcedimentoCbhpm;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idServico != null ? idServico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servico)) {
            return false;
        }
        Servico other = (Servico) object;
        if ((this.idServico == null && other.idServico != null) || (this.idServico != null && !this.idServico.equals(other.idServico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ux.model.tasy.Servico[ idServico=" + idServico + " ]";
    }

    public List<CirurgiaItem> getCirurgiaItemList() {
        return cirurgiaItemList;
    }

    public void setCirurgiaItemList(List<CirurgiaItem> cirurgiaItemList) {
        this.cirurgiaItemList = cirurgiaItemList;
    }

    public List<OrcamentoItem> getOrcamentoItemList() {
        return orcamentoItemList;
    }

    public void setOrcamentoItemList(List<OrcamentoItem> orcamentoItemList) {
        this.orcamentoItemList = orcamentoItemList;
    }

    public Boolean getCbhpm() {
        return cbhpm;
    }

    public void setCbhpm(Boolean cbhpm) {
        this.cbhpm = cbhpm;
    }

    @XmlTransient
    public List<RegraRepasse> getRegraRepasseList() {
        return regraRepasseList;
    }

    public void setRegraRepasseList(List<RegraRepasse> regraRepasseList) {
        this.regraRepasseList = regraRepasseList;
    }

   

    public String getNomeProcedimentoInternoTasy() {
        return nomeProcedimentoInternoTasy;
    }

    public void setNomeProcedimentoInternoTasy(String nomeProcedimentoInternoTasy) {
        this.nomeProcedimentoInternoTasy = nomeProcedimentoInternoTasy;
    }

    public Boolean getCadastroDemonstrativo() {
        return cadastroDemonstrativo;
    }

    public void setCadastroDemonstrativo(Boolean cadastroDemonstrativo) {
        this.cadastroDemonstrativo = cadastroDemonstrativo;
    }

    public RegraRepasse getIdRegraRepasse() {
        return idRegraRepasse;
    }

    public void setIdRegraRepasse(RegraRepasse idRegraRepasse) {
        this.idRegraRepasse = idRegraRepasse;
    }

}
