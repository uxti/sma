/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "cheque")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cheque.findAll", query = "SELECT c FROM Cheque c"),
    @NamedQuery(name = "Cheque.findByIdCheque", query = "SELECT c FROM Cheque c WHERE c.idCheque = :idCheque"),
    @NamedQuery(name = "Cheque.findByIdRepasse", query = "SELECT c FROM Cheque c WHERE c.idRepasse = :idRepasse"),
    @NamedQuery(name = "Cheque.findByConta", query = "SELECT c FROM Cheque c WHERE c.conta = :conta"),
    @NamedQuery(name = "Cheque.findByNumeroCheque", query = "SELECT c FROM Cheque c WHERE c.numeroCheque = :numeroCheque"),
    @NamedQuery(name = "Cheque.findByValor", query = "SELECT c FROM Cheque c WHERE c.valor = :valor"),
    @NamedQuery(name = "Cheque.findByDtRecebimentoEmissao", query = "SELECT c FROM Cheque c WHERE c.dtRecebimentoEmissao = :dtRecebimentoEmissao"),
    @NamedQuery(name = "Cheque.findByDtCompensacaoVencimento", query = "SELECT c FROM Cheque c WHERE c.dtCompensacaoVencimento = :dtCompensacaoVencimento"),
    @NamedQuery(name = "Cheque.findByDtCompensado", query = "SELECT c FROM Cheque c WHERE c.dtCompensado = :dtCompensado"),
    @NamedQuery(name = "Cheque.findByTipo", query = "SELECT c FROM Cheque c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "Cheque.findByCompensado", query = "SELECT c FROM Cheque c WHERE c.compensado = :compensado"),
    @NamedQuery(name = "Cheque.findByMovimento", query = "SELECT c FROM Cheque c WHERE c.movimento = :movimento"),
    @NamedQuery(name = "Cheque.findByEmitente", query = "SELECT c FROM Cheque c WHERE c.emitente = :emitente"),
    @NamedQuery(name = "Cheque.findByCpf", query = "SELECT c FROM Cheque c WHERE c.cpf = :cpf"),
    @NamedQuery(name = "Cheque.findByNumeroParcela", query = "SELECT c FROM Cheque c WHERE c.numeroParcela = :numeroParcela"),
    @NamedQuery(name = "Cheque.findByTipoCartao", query = "SELECT c FROM Cheque c WHERE c.tipoCartao = :tipoCartao"),
    @NamedQuery(name = "Cheque.findByCancelado", query = "SELECT c FROM Cheque c WHERE c.cancelado = :cancelado"),
    @NamedQuery(name = "Cheque.findByAvulso", query = "SELECT c FROM Cheque c WHERE c.avulso = :avulso"),
    @NamedQuery(name = "Cheque.findByDevolvido", query = "SELECT c FROM Cheque c WHERE c.devolvido = :devolvido")})
public class Cheque implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CHEQUE")
    private Integer idCheque;
    @Column(name = "ID_REPASSE")
    private Integer idRepasse;
    @Column(name = "CONTA")
    private String conta;
    @Column(name = "NUMERO_CHEQUE")
    private String numeroCheque;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "DT_RECEBIMENTO_EMISSAO")
    @Temporal(TemporalType.DATE)
    private Date dtRecebimentoEmissao;
    @Column(name = "DT_COMPENSACAO_VENCIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dtCompensacaoVencimento;
    @Column(name = "dt_compensado")
    @Temporal(TemporalType.DATE)
    private Date dtCompensado;
    @Column(name = "TIPO")
    private String tipo;
    @Lob
    @Column(name = "OBSERVACAO")
    private String observacao;
    @Column(name = "COMPENSADO")
    private String compensado;
    @Column(name = "movimento")
    private String movimento;
    @Column(name = "EMITENTE")
    private String emitente;
    @Column(name = "situacao")
    private String situacao;
    @Column(name = "CPF")
    private String cpf;
    @Column(name = "NUMERO_PARCELA")
    private Integer numeroParcela;
    @Column(name = "tipo_cartao")
    private String tipoCartao;
    @Column(name = "cancelado")
    private String cancelado;
    @Column(name = "avulso")
    private Boolean avulso;
    @Column(name = "devolvido")
    private Boolean devolvido;
    @Column(name = "emitir_recibo")
    private String emitirRecibo;
    @Column(name = "lancamento_manual")
    private String lancamentoManual;

    @JoinColumn(name = "ID_CIRURGIA", referencedColumnName = "ID_CIRURGIA")
    @ManyToOne
    private Cirurgia idCirurgia;
    @JoinColumn(name = "ID_BANCO", referencedColumnName = "ID_BANCO")
    @ManyToOne
    private Banco idBanco;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @JoinColumn(name = "ID_EXAME", referencedColumnName = "ID_EXAME")
    @ManyToOne
    private Exame idExame;
    @JoinColumn(name = "ID_PACIENTE", referencedColumnName = "ID_PACIENTE")
    @ManyToOne
    private Paciente idPaciente;
    @JoinColumn(name = "ID_FORMA", referencedColumnName = "ID_FORMA")
    @ManyToOne
    private FormaPagamento idForma;
    @JoinColumn(name = "id_procedimento", referencedColumnName = "ID_PROCEDIMENTO")
    @ManyToOne
    private Procedimento idProcedimento;
    @OneToMany(mappedBy = "idCheque")
    private List<Repasse> repasseList;
    @OneToMany(mappedBy = "idCheque")
    private List<Caixa> caixaList;

    public Cheque() {
    }

    public Cheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    public Integer getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    public Integer getIdRepasse() {
        return idRepasse;
    }

    public void setIdRepasse(Integer idRepasse) {
        this.idRepasse = idRepasse;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Date getDtRecebimentoEmissao() {
        return dtRecebimentoEmissao;
    }

    public void setDtRecebimentoEmissao(Date dtRecebimentoEmissao) {
        this.dtRecebimentoEmissao = dtRecebimentoEmissao;
    }

    public Date getDtCompensacaoVencimento() {
        return dtCompensacaoVencimento;
    }

    public void setDtCompensacaoVencimento(Date dtCompensacaoVencimento) {
        this.dtCompensacaoVencimento = dtCompensacaoVencimento;
    }

    public Date getDtCompensado() {
        return dtCompensado;
    }

    public void setDtCompensado(Date dtCompensado) {
        this.dtCompensado = dtCompensado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getCompensado() {
        return compensado;
    }

    public void setCompensado(String compensado) {
        this.compensado = compensado;
    }

    public String getMovimento() {
        return movimento;
    }

    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    public String getEmitente() {
        return emitente;
    }

    public void setEmitente(String emitente) {
        this.emitente = emitente;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Integer getNumeroParcela() {
        return numeroParcela;
    }

    public void setNumeroParcela(Integer numeroParcela) {
        this.numeroParcela = numeroParcela;
    }

    public String getTipoCartao() {
        return tipoCartao;
    }

    public void setTipoCartao(String tipoCartao) {
        this.tipoCartao = tipoCartao;
    }

    public String getCancelado() {
        return cancelado;
    }

    public void setCancelado(String cancelado) {
        this.cancelado = cancelado;
    }

    public Boolean getAvulso() {
        return avulso;
    }

    public void setAvulso(Boolean avulso) {
        this.avulso = avulso;
    }

    public Boolean getDevolvido() {
        return devolvido;
    }

    public void setDevolvido(Boolean devolvido) {
        this.devolvido = devolvido;
    }

    public Cirurgia getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Cirurgia idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    public Banco getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Banco idBanco) {
        this.idBanco = idBanco;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public Exame getIdExame() {
        return idExame;
    }

    public void setIdExame(Exame idExame) {
        this.idExame = idExame;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public FormaPagamento getIdForma() {
        return idForma;
    }

    public void setIdForma(FormaPagamento idForma) {
        this.idForma = idForma;
    }

    public Procedimento getIdProcedimento() {
        return idProcedimento;
    }

    public void setIdProcedimento(Procedimento idProcedimento) {
        this.idProcedimento = idProcedimento;
    }

    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }

    public List<Caixa> getCaixaList() {
        return caixaList;
    }

    public void setCaixaList(List<Caixa> caixaList) {
        this.caixaList = caixaList;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCheque != null ? idCheque.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cheque)) {
            return false;
        }
        Cheque other = (Cheque) object;
        if ((this.idCheque == null && other.idCheque != null) || (this.idCheque != null && !this.idCheque.equals(other.idCheque))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Cheque[ idCheque=" + idCheque + " ]";
    }

    public String getEmitirRecibo() {
        return emitirRecibo;
    }

    public void setEmitirRecibo(String emitirRecibo) {
        this.emitirRecibo = emitirRecibo;
    }

    public String getLancamentoManual() {
        return lancamentoManual;
    }

    public void setLancamentoManual(String lancamentoManual) {
        this.lancamentoManual = lancamentoManual;
    }
    
    

    
    
    

    
    
}
