/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "forma_pagamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FormaPagamento.findAll", query = "SELECT f FROM FormaPagamento f"),
    @NamedQuery(name = "FormaPagamento.findByIdForma", query = "SELECT f FROM FormaPagamento f WHERE f.idForma = :idForma"),
    @NamedQuery(name = "FormaPagamento.findByDescricao", query = "SELECT f FROM FormaPagamento f WHERE f.descricao = :descricao"),
    @NamedQuery(name = "FormaPagamento.findByTipo", query = "SELECT f FROM FormaPagamento f WHERE f.tipo = :tipo"),
    @NamedQuery(name = "FormaPagamento.findByNumMeses", query = "SELECT f FROM FormaPagamento f WHERE f.numMeses = :numMeses"),
    @NamedQuery(name = "FormaPagamento.findByPadrao", query = "SELECT f FROM FormaPagamento f WHERE f.padrao = :padrao"),
    @NamedQuery(name = "FormaPagamento.findByForma", query = "SELECT f FROM FormaPagamento f WHERE f.forma = :forma")})
public class FormaPagamento implements Serializable {
    @Column(name = "inativo")
    private Boolean inativo = Boolean.FALSE;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_FORMA")
    private Integer idForma;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "TIPO")
    private Character tipo;
    @Column(name = "NUM_MESES")
    private Integer numMeses;
    @Column(name = "padrao")
    private String padrao;
    @Column(name = "FORMA")
    private String forma;
    @OneToMany(mappedBy = "idForma")
    private List<Orcamento> orcamentoList;
    @OneToMany(mappedBy = "idForma")
    private List<Repasse> repasseList;
    @OneToMany(mappedBy = "idForma")
    private List<Exame> exameList;
    @OneToMany(mappedBy = "idForma")
    private List<Cirurgia> cirurgiaList;
    @OneToMany(mappedBy = "idForma")
    private List<Cheque> chequeList;

    public FormaPagamento() {
    }

    public FormaPagamento(Integer idForma) {
        this.idForma = idForma;
    }

    public Integer getIdForma() {
        return idForma;
    }

    public void setIdForma(Integer idForma) {
        this.idForma = idForma;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Integer getNumMeses() {
        return numMeses;
    }

    public void setNumMeses(Integer numMeses) {
        this.numMeses = numMeses;
    }

    public String getPadrao() {
        return padrao;
    }

    public void setPadrao(String padrao) {
        this.padrao = padrao;
    }

    public String getForma() {
        return forma;
    }

    public void setForma(String forma) {
        this.forma = forma;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }

    @XmlTransient
    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }

    @XmlTransient
    public List<Exame> getExameList() {
        return exameList;
    }

    public void setExameList(List<Exame> exameList) {
        this.exameList = exameList;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idForma != null ? idForma.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormaPagamento)) {
            return false;
        }
        FormaPagamento other = (FormaPagamento) object;
        if ((this.idForma == null && other.idForma != null) || (this.idForma != null && !this.idForma.equals(other.idForma))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.FormaPagamento[ idForma=" + idForma + " ]";
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }
    
}
