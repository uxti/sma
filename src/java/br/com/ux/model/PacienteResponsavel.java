/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "paciente_responsavel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PacienteResponsavel.findAll", query = "SELECT p FROM PacienteResponsavel p"),
    @NamedQuery(name = "PacienteResponsavel.findByIdPacienteResposanvel", query = "SELECT p FROM PacienteResponsavel p WHERE p.idPacienteResposanvel = :idPacienteResposanvel")})
public class PacienteResponsavel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PACIENTE_RESPOSANVEL")
    private Integer idPacienteResposanvel;
    @JoinColumn(name = "ID_PACIENTE", referencedColumnName = "ID_PACIENTE")
    @ManyToOne(optional = false)
    private Paciente idPaciente;
    @JoinColumn(name = "ID_PACIENTE_RESPONSAVEL", referencedColumnName = "ID_PACIENTE")
    @ManyToOne(optional = false)
    private Paciente idPacienteResponsavel;

    public PacienteResponsavel() {
    }

    public PacienteResponsavel(Integer idPacienteResposanvel) {
        this.idPacienteResposanvel = idPacienteResposanvel;
    }

    public Integer getIdPacienteResposanvel() {
        return idPacienteResposanvel;
    }

    public void setIdPacienteResposanvel(Integer idPacienteResposanvel) {
        this.idPacienteResposanvel = idPacienteResposanvel;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Paciente getIdPacienteResponsavel() {
        return idPacienteResponsavel;
    }

    public void setIdPacienteResponsavel(Paciente idPacienteResponsavel) {
        this.idPacienteResponsavel = idPacienteResponsavel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPacienteResposanvel != null ? idPacienteResposanvel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PacienteResponsavel)) {
            return false;
        }
        PacienteResponsavel other = (PacienteResponsavel) object;
        if ((this.idPacienteResposanvel == null && other.idPacienteResposanvel != null) || (this.idPacienteResposanvel != null && !this.idPacienteResposanvel.equals(other.idPacienteResposanvel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.PacienteResponsavel[ idPacienteResposanvel=" + idPacienteResposanvel + " ]";
    }
    
}
