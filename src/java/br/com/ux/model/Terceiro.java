/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import br.com.ux.model.tasy.AtendimentoPaciente;
import br.com.ux.model.tasy.ContaPaciente;
import br.com.ux.model.tasy.ProcedimentoPaciente;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "terceiro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Terceiro.findAll", query = "SELECT t FROM Terceiro t"),
    @NamedQuery(name = "Terceiro.findByIdTerceiro", query = "SELECT t FROM Terceiro t WHERE t.idTerceiro = :idTerceiro"),
    @NamedQuery(name = "Terceiro.findByNome", query = "SELECT t FROM Terceiro t WHERE t.nome = :nome"),
    @NamedQuery(name = "Terceiro.findByTipoPessoa", query = "SELECT t FROM Terceiro t WHERE t.tipoPessoa = :tipoPessoa"),
    @NamedQuery(name = "Terceiro.findByCgccpf", query = "SELECT t FROM Terceiro t WHERE t.cgccpf = :cgccpf"),
    @NamedQuery(name = "Terceiro.findByIe", query = "SELECT t FROM Terceiro t WHERE t.ie = :ie"),
    @NamedQuery(name = "Terceiro.findByTerceiroMedHosp", query = "SELECT t FROM Terceiro t WHERE t.terceiroMedHosp = :terceiroMedHosp"),
    @NamedQuery(name = "Terceiro.findByNomeFantasia", query = "SELECT t FROM Terceiro t WHERE t.nomeFantasia = :nomeFantasia"),
    @NamedQuery(name = "Terceiro.findByRazaoSocial", query = "SELECT t FROM Terceiro t WHERE t.razaoSocial = :razaoSocial"),
    @NamedQuery(name = "Terceiro.findByRg", query = "SELECT t FROM Terceiro t WHERE t.rg = :rg"),
    @NamedQuery(name = "Terceiro.findByTelefone", query = "SELECT t FROM Terceiro t WHERE t.telefone = :telefone"),
    @NamedQuery(name = "Terceiro.findByTipoConselho", query = "SELECT t FROM Terceiro t WHERE t.tipoConselho = :tipoConselho"),
    @NamedQuery(name = "Terceiro.findByNumeroConselho", query = "SELECT t FROM Terceiro t WHERE t.numeroConselho = :numeroConselho"),
    @NamedQuery(name = "Terceiro.findBySexo", query = "SELECT t FROM Terceiro t WHERE t.sexo = :sexo"),
    @NamedQuery(name = "Terceiro.findByCep", query = "SELECT t FROM Terceiro t WHERE t.cep = :cep"),
    @NamedQuery(name = "Terceiro.findByLogradouro", query = "SELECT t FROM Terceiro t WHERE t.logradouro = :logradouro"),
    @NamedQuery(name = "Terceiro.findByNumero", query = "SELECT t FROM Terceiro t WHERE t.numero = :numero"),
    @NamedQuery(name = "Terceiro.findByBairro", query = "SELECT t FROM Terceiro t WHERE t.bairro = :bairro"),
    @NamedQuery(name = "Terceiro.findByCidade", query = "SELECT t FROM Terceiro t WHERE t.cidade = :cidade"),
    @NamedQuery(name = "Terceiro.findByUf", query = "SELECT t FROM Terceiro t WHERE t.uf = :uf"),
    @NamedQuery(name = "Terceiro.findByComplemento", query = "SELECT t FROM Terceiro t WHERE t.complemento = :complemento"),
    @NamedQuery(name = "Terceiro.findByTipoLogradouro", query = "SELECT t FROM Terceiro t WHERE t.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Terceiro.findByCelular", query = "SELECT t FROM Terceiro t WHERE t.celular = :celular")})
public class Terceiro implements Serializable {

    @OneToMany(mappedBy = "idMedico")
    private List<RegraRepasse> regraRepasseList;

    @OneToMany(mappedBy = "idMedico")
    private List<ProcedimentoPaciente> procedimentoPacienteList;

    @OneToMany(mappedBy = "cdMedicoExecutor")
    private List<ContaPaciente> contaPacienteList;

    @OneToMany(mappedBy = "idMedicoResp")
    private List<AtendimentoPaciente> atendimentoPacienteList;

    @JoinColumn(name = "id_cbo_saude", referencedColumnName = "id_cbo_saude")
    @ManyToOne
    private CboSaude idCboSaude;
    @Column(name = "inativo")
    private Boolean inativo =  Boolean.FALSE;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_TERCEIRO")
    private Integer idTerceiro;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "TIPO_PESSOA")
    private Character tipoPessoa;
    @Column(name = "CGCCPF")
    private String cgccpf;
    @Column(name = "IE")
    private String ie;
    @Column(name = "TERCEIRO_MED_HOSP")
    private Boolean terceiroMedHosp;
    @Column(name = "NOME_FANTASIA")
    private String nomeFantasia;
    @Column(name = "RAZAO_SOCIAL")
    private String razaoSocial;
    @Column(name = "RG")
    private String rg;
    @Column(name = "TELEFONE")
    private String telefone;
    @Column(name = "TIPO_CONSELHO")
    private String tipoConselho;
    @Column(name = "NUMERO_CONSELHO")
    private String numeroConselho;
    @Column(name = "SEXO")
    private Character sexo;
    @Column(name = "CEP")
    private String cep;
    @Column(name = "LOGRADOURO")
    private String logradouro;
    @Column(name = "NUMERO")
    private Integer numero;
    @Column(name = "BAIRRO")
    private String bairro;
    @Column(name = "CIDADE")
    private String cidade;
    @Column(name = "UF")
    private String uf;
    @Column(name = "COMPLEMENTO")
    private String complemento;
    @Column(name = "TIPO_LOGRADOURO")
    private String tipoLogradouro;
    @Column(name = "celular")
    private String celular;
    @Column(name = "cadastro_demonstrativo")
    private Boolean cadastroDemonstrativo;
    @Column(name = "id_terceiro_tasy")
    private Integer idTerceiroTasy;
    @OneToMany(mappedBy = "idTerceiro")
    private List<Usuario> usuarioList;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "ID_ESPECIALIDADE", referencedColumnName = "ID_ESPECIALIDADE")
    @ManyToOne
    private Especialidade idEspecialidade;
    @OneToMany(mappedBy = "idTerceiro")
    private List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList;
    @OneToMany(mappedBy = "idTerceiro")
    private List<Orcamento> orcamentoList;
    @OneToMany(mappedBy = "idTerceiro")
    private List<Repasse> repasseList;
    @OneToMany(mappedBy = "idTerceiro")
    private List<ContaCorrente> contaCorrenteList;
    @OneToMany(mappedBy = "idTerceiro")
    private List<OrcamentoItem> orcamentoItemList;
    @OneToMany(mappedBy = "idTerceiro")
    private List<CirurgiaItem> cirurgiaItemList;
    @OneToMany(mappedBy = "idTerceiro")
    private List<Exame> exameList;
    @OneToMany(mappedBy = "idTerceiro")
    private List<Cirurgia> cirurgiaList;
    @OneToMany(mappedBy = "idTerceiro")
    private List<Cheque> chequeList;

    public Terceiro() {
    }

    public Terceiro(Integer idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public Integer getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Integer idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(Character tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getCgccpf() {
        return cgccpf;
    }

    public void setCgccpf(String cgccpf) {
        this.cgccpf = cgccpf;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public Boolean getTerceiroMedHosp() {
        return terceiroMedHosp;
    }

    public void setTerceiroMedHosp(Boolean terceiroMedHosp) {
        this.terceiroMedHosp = terceiroMedHosp;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTipoConselho() {
        return tipoConselho;
    }

    public void setTipoConselho(String tipoConselho) {
        this.tipoConselho = tipoConselho;
    }

    public String getNumeroConselho() {
        return numeroConselho;
    }

    public void setNumeroConselho(String numeroConselho) {
        this.numeroConselho = numeroConselho;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Especialidade getIdEspecialidade() {
        return idEspecialidade;
    }

    public void setIdEspecialidade(Especialidade idEspecialidade) {
        this.idEspecialidade = idEspecialidade;
    }

    @XmlTransient
    public List<ServicoItemCirurgiaItem> getServicoItemCirurgiaItemList() {
        return servicoItemCirurgiaItemList;
    }

    public void setServicoItemCirurgiaItemList(List<ServicoItemCirurgiaItem> servicoItemCirurgiaItemList) {
        this.servicoItemCirurgiaItemList = servicoItemCirurgiaItemList;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }

    @XmlTransient
    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    @XmlTransient
    public List<OrcamentoItem> getOrcamentoItemList() {
        return orcamentoItemList;
    }

    public void setOrcamentoItemList(List<OrcamentoItem> orcamentoItemList) {
        this.orcamentoItemList = orcamentoItemList;
    }

    @XmlTransient
    public List<CirurgiaItem> getCirurgiaItemList() {
        return cirurgiaItemList;
    }

    public void setCirurgiaItemList(List<CirurgiaItem> cirurgiaItemList) {
        this.cirurgiaItemList = cirurgiaItemList;
    }

    @XmlTransient
    public List<Exame> getExameList() {
        return exameList;
    }

    public void setExameList(List<Exame> exameList) {
        this.exameList = exameList;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTerceiro != null ? idTerceiro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Terceiro)) {
            return false;
        }
        Terceiro other = (Terceiro) object;
        if ((this.idTerceiro == null && other.idTerceiro != null) || (this.idTerceiro != null && !this.idTerceiro.equals(other.idTerceiro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Terceiro[ idTerceiro=" + idTerceiro + " ]";
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }

    public Integer getIdTerceiroTasy() {
        return idTerceiroTasy;
    }

    public void setIdTerceiroTasy(Integer idTerceiroTasy) {
        this.idTerceiroTasy = idTerceiroTasy;
    }

    public CboSaude getIdCboSaude() {
        return idCboSaude;
    }

    public void setIdCboSaude(CboSaude idCboSaude) {
        this.idCboSaude = idCboSaude;
    }

    @XmlTransient
    public List<AtendimentoPaciente> getAtendimentoPacienteList() {
        return atendimentoPacienteList;
    }

    public void setAtendimentoPacienteList(List<AtendimentoPaciente> atendimentoPacienteList) {
        this.atendimentoPacienteList = atendimentoPacienteList;
    }

    @XmlTransient
    public List<ContaPaciente> getContaPacienteList() {
        return contaPacienteList;
    }

    public void setContaPacienteList(List<ContaPaciente> contaPacienteList) {
        this.contaPacienteList = contaPacienteList;
    }

    @XmlTransient
    public List<ProcedimentoPaciente> getProcedimentoPacienteList() {
        return procedimentoPacienteList;
    }

    public void setProcedimentoPacienteList(List<ProcedimentoPaciente> procedimentoPacienteList) {
        this.procedimentoPacienteList = procedimentoPacienteList;
    }

    @XmlTransient
    public List<RegraRepasse> getRegraRepasseList() {
        return regraRepasseList;
    }

    public void setRegraRepasseList(List<RegraRepasse> regraRepasseList) {
        this.regraRepasseList = regraRepasseList;
    }

    public Boolean getCadastroDemonstrativo() {
        return cadastroDemonstrativo;
    }

    public void setCadastroDemonstrativo(Boolean cadastroDemonstrativo) {
        this.cadastroDemonstrativo = cadastroDemonstrativo;
    }
    
    
    
    
}
