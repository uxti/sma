/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "descricao_exame")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DescricaoExame.findAll", query = "SELECT d FROM DescricaoExame d"),
    @NamedQuery(name = "DescricaoExame.findByIdDescricaoExame", query = "SELECT d FROM DescricaoExame d WHERE d.idDescricaoExame = :idDescricaoExame"),
    @NamedQuery(name = "DescricaoExame.findByDescricao", query = "SELECT d FROM DescricaoExame d WHERE d.descricao = :descricao"),
    @NamedQuery(name = "DescricaoExame.findByTipo", query = "SELECT d FROM DescricaoExame d WHERE d.tipo = :tipo"),
    @NamedQuery(name = "DescricaoExame.findByPadrao", query = "SELECT d FROM DescricaoExame d WHERE d.padrao = :padrao")})
public class DescricaoExame implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DESCRICAO_EXAME")
    private Integer idDescricaoExame;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "padrao")
    private String padrao;
    @OneToMany(mappedBy = "idDescricaoExame")
    private List<Exame> exameList;

    public DescricaoExame() {
    }

    public DescricaoExame(Integer idDescricaoExame) {
        this.idDescricaoExame = idDescricaoExame;
    }

    public Integer getIdDescricaoExame() {
        return idDescricaoExame;
    }

    public void setIdDescricaoExame(Integer idDescricaoExame) {
        this.idDescricaoExame = idDescricaoExame;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPadrao() {
        return padrao;
    }

    public void setPadrao(String padrao) {
        this.padrao = padrao;
    }

    @XmlTransient
    public List<Exame> getExameList() {
        return exameList;
    }

    public void setExameList(List<Exame> exameList) {
        this.exameList = exameList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDescricaoExame != null ? idDescricaoExame.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DescricaoExame)) {
            return false;
        }
        DescricaoExame other = (DescricaoExame) object;
        if ((this.idDescricaoExame == null && other.idDescricaoExame != null) || (this.idDescricaoExame != null && !this.idDescricaoExame.equals(other.idDescricaoExame))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.DescricaoExame[ idDescricaoExame=" + idDescricaoExame + " ]";
    }
    
}
