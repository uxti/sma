/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "caixa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Caixa.findAll", query = "SELECT c FROM Caixa c"),
    @NamedQuery(name = "Caixa.findByIdCaixa", query = "SELECT c FROM Caixa c WHERE c.idCaixa = :idCaixa"),
    @NamedQuery(name = "Caixa.findByIdCaixaPai", query = "SELECT c FROM Caixa c WHERE c.idCaixaPai = :idCaixaPai"),
    @NamedQuery(name = "Caixa.findByDtCaixa", query = "SELECT c FROM Caixa c WHERE c.dtCaixa = :dtCaixa"),
    @NamedQuery(name = "Caixa.findByTipoLancamento", query = "SELECT c FROM Caixa c WHERE c.tipoLancamento = :tipoLancamento"),
    @NamedQuery(name = "Caixa.findByValor", query = "SELECT c FROM Caixa c WHERE c.valor = :valor"),
    @NamedQuery(name = "Caixa.findByHistorico", query = "SELECT c FROM Caixa c WHERE c.historico = :historico"),
    @NamedQuery(name = "Caixa.findBySituacao", query = "SELECT c FROM Caixa c WHERE c.situacao = :situacao"),
    @NamedQuery(name = "Caixa.findByStatus", query = "SELECT c FROM Caixa c WHERE c.status = :status"),
    @NamedQuery(name = "Caixa.findByNumeroParcela", query = "SELECT c FROM Caixa c WHERE c.numeroParcela = :numeroParcela"),
    @NamedQuery(name = "Caixa.findByNumLote", query = "SELECT c FROM Caixa c WHERE c.numLote = :numLote"),
    @NamedQuery(name = "Caixa.findByLiberado", query = "SELECT c FROM Caixa c WHERE c.liberado = :liberado"),
    @NamedQuery(name = "Caixa.findByLoteOrigem", query = "SELECT c FROM Caixa c WHERE c.loteOrigem = :loteOrigem"),
    @NamedQuery(name = "Caixa.findByCirurgiaidResponsavel", query = "SELECT c FROM Caixa c WHERE c.cirurgiaidResponsavel = :cirurgiaidResponsavel"),
    @NamedQuery(name = "Caixa.findByCaixaPaidtCaixaPai", query = "SELECT c FROM Caixa c WHERE c.caixaPaidtCaixaPai = :caixaPaidtCaixaPai")})
public class Caixa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CAIXA")
    private Integer idCaixa;
    @Column(name = "ID_CAIXA_PAI")
    private Integer idCaixaPai;
    @Column(name = "DT_CAIXA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCaixa;
    @Column(name = "TIPO_LANCAMENTO")
    private Character tipoLancamento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "HISTORICO")
    private String historico;
    @Lob
    @Column(name = "OBSERVACAO")
    private String observacao;
    @Column(name = "SITUACAO")
    private String situacao;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "NUMERO_PARCELA")
    private Integer numeroParcela;
    @Column(name = "NUM_LOTE")
    private String numLote;
    @Column(name = "LIBERADO")
    private String liberado;
    @Column(name = "LOTE_ORIGEM")
    private String loteOrigem;
    @Column(name = "CIRURGIAID_RESPONSAVEL")
    private Integer cirurgiaidResponsavel;
    @Column(name = "CAIXA_PAIDT_CAIXA_PAI")
    @Temporal(TemporalType.DATE)
    private Date caixaPaidtCaixaPai;
    @JoinColumn(name = "id_usuario_destino", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuarioDestino;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "ID_CHEQUE", referencedColumnName = "ID_CHEQUE")
    @ManyToOne
    private Cheque idCheque;
    @JoinColumn(name = "ID_OPERACAO", referencedColumnName = "ID_OPERACAO")
    @ManyToOne
    private Operacao idOperacao;
    @JoinColumn(name = "ID_REPASSE", referencedColumnName = "ID_REPASSE")
    @ManyToOne
    private Repasse idRepasse;
    @JoinColumn(name = "ID_CIRURGIA", referencedColumnName = "ID_CIRURGIA")
    @ManyToOne
    private Cirurgia idCirurgia;
    @OneToMany(mappedBy = "idCaixa")
    private List<Repasse> repasseList;
    @OneToMany(mappedBy = "idCaixa")
    private List<ContaCorrente> contaCorrenteList;

    public Caixa() {
    }

    public Caixa(Integer idCaixa) {
        this.idCaixa = idCaixa;
    }

    public Integer getIdCaixa() {
        return idCaixa;
    }

    public void setIdCaixa(Integer idCaixa) {
        this.idCaixa = idCaixa;
    }

    public Integer getIdCaixaPai() {
        return idCaixaPai;
    }

    public void setIdCaixaPai(Integer idCaixaPai) {
        this.idCaixaPai = idCaixaPai;
    }

    public Date getDtCaixa() {
        return dtCaixa;
    }

    public void setDtCaixa(Date dtCaixa) {
        this.dtCaixa = dtCaixa;
    }

    public Character getTipoLancamento() {
        return tipoLancamento;
    }

    public void setTipoLancamento(Character tipoLancamento) {
        this.tipoLancamento = tipoLancamento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNumeroParcela() {
        return numeroParcela;
    }

    public void setNumeroParcela(Integer numeroParcela) {
        this.numeroParcela = numeroParcela;
    }

    public String getNumLote() {
        return numLote;
    }

    public void setNumLote(String numLote) {
        this.numLote = numLote;
    }

    public String getLiberado() {
        return liberado;
    }

    public void setLiberado(String liberado) {
        this.liberado = liberado;
    }

    public String getLoteOrigem() {
        return loteOrigem;
    }

    public void setLoteOrigem(String loteOrigem) {
        this.loteOrigem = loteOrigem;
    }

    public Integer getCirurgiaidResponsavel() {
        return cirurgiaidResponsavel;
    }

    public void setCirurgiaidResponsavel(Integer cirurgiaidResponsavel) {
        this.cirurgiaidResponsavel = cirurgiaidResponsavel;
    }

    public Date getCaixaPaidtCaixaPai() {
        return caixaPaidtCaixaPai;
    }

    public void setCaixaPaidtCaixaPai(Date caixaPaidtCaixaPai) {
        this.caixaPaidtCaixaPai = caixaPaidtCaixaPai;
    }

    public Usuario getIdUsuarioDestino() {
        return idUsuarioDestino;
    }

    public void setIdUsuarioDestino(Usuario idUsuarioDestino) {
        this.idUsuarioDestino = idUsuarioDestino;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Cheque getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Cheque idCheque) {
        this.idCheque = idCheque;
    }

    public Operacao getIdOperacao() {
        return idOperacao;
    }

    public void setIdOperacao(Operacao idOperacao) {
        this.idOperacao = idOperacao;
    }

    public Repasse getIdRepasse() {
        return idRepasse;
    }

    public void setIdRepasse(Repasse idRepasse) {
        this.idRepasse = idRepasse;
    }

    public Cirurgia getIdCirurgia() {
        return idCirurgia;
    }

    public void setIdCirurgia(Cirurgia idCirurgia) {
        this.idCirurgia = idCirurgia;
    }

    @XmlTransient
    public List<Repasse> getRepasseList() {
        return repasseList;
    }

    public void setRepasseList(List<Repasse> repasseList) {
        this.repasseList = repasseList;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCaixa != null ? idCaixa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Caixa)) {
            return false;
        }
        Caixa other = (Caixa) object;
        if ((this.idCaixa == null && other.idCaixa != null) || (this.idCaixa != null && !this.idCaixa.equals(other.idCaixa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Caixa[ idCaixa=" + idCaixa + " ]";
    }
    
}
