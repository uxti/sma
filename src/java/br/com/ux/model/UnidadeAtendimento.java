/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "unidade_atendimento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UnidadeAtendimento.findAll", query = "SELECT u FROM UnidadeAtendimento u"),
    @NamedQuery(name = "UnidadeAtendimento.findByIdUnidade", query = "SELECT u FROM UnidadeAtendimento u WHERE u.idUnidade = :idUnidade"),
    @NamedQuery(name = "UnidadeAtendimento.findByDescricao", query = "SELECT u FROM UnidadeAtendimento u WHERE u.descricao = :descricao")})
public class UnidadeAtendimento implements Serializable {
    @Column(name = "inativo")
    private Boolean inativo = Boolean.FALSE;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_UNIDADE")
    private Integer idUnidade;
    @Column(name = "DESCRICAO")
    private String descricao;
    @OneToMany(mappedBy = "idUnidade")
    private List<Orcamento> orcamentoList;
    @OneToMany(mappedBy = "idUnidade")
    private List<Cirurgia> cirurgiaList;

    public UnidadeAtendimento() {
    }

    public UnidadeAtendimento(Integer idUnidade) {
        this.idUnidade = idUnidade;
    }

    public Integer getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(Integer idUnidade) {
        this.idUnidade = idUnidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUnidade != null ? idUnidade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnidadeAtendimento)) {
            return false;
        }
        UnidadeAtendimento other = (UnidadeAtendimento) object;
        if ((this.idUnidade == null && other.idUnidade != null) || (this.idUnidade != null && !this.idUnidade.equals(other.idUnidade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.UnidadeAtendimento[ idUnidade=" + idUnidade + " ]";
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }
    
}
