/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.findByIdUsuario", query = "SELECT u FROM Usuario u WHERE u.idUsuario = :idUsuario"),
    @NamedQuery(name = "Usuario.findByUsuario", query = "SELECT u FROM Usuario u WHERE u.usuario = :usuario"),
    @NamedQuery(name = "Usuario.findBySenha", query = "SELECT u FROM Usuario u WHERE u.senha = :senha"),
    @NamedQuery(name = "Usuario.findByAtivo", query = "SELECT u FROM Usuario u WHERE u.ativo = :ativo"),
    @NamedQuery(name = "Usuario.findByEmail", query = "SELECT u FROM Usuario u WHERE u.email = :email"),
    @NamedQuery(name = "Usuario.findByReceberRepasse", query = "SELECT u FROM Usuario u WHERE u.receberRepasse = :receberRepasse"),
    @NamedQuery(name = "Usuario.findByReceberRepasseHospital", query = "SELECT u FROM Usuario u WHERE u.receberRepasseHospital = :receberRepasseHospital")})
public class Usuario implements Serializable {

    @OneToMany(mappedBy = "idUsuarioEmitente")
    private List<Emails> emailsList;
    @OneToMany(mappedBy = "idUsuarioDestinatario")
    private List<Emails> emailsList1;
    @OneToMany(mappedBy = "idUsuarioAdiantamento")
    private List<Orcamento> orcamentoList;
    @OneToMany(mappedBy = "idUsuarioCriacao")
    private List<Orcamento> orcamentoList1;
    @OneToMany(mappedBy = "idUsuarioUltimaAlteracao")
    private List<Orcamento> orcamentoList2;
    @OneToMany(mappedBy = "idUsuario")
    private List<Cirurgia> cirurgiaList;
    @Column(name = "inativo")
    private Boolean inativo = Boolean.FALSE;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_USUARIO")
    private Integer idUsuario;
    @Column(name = "USUARIO")
    private String usuario;
    @Column(name = "SENHA")
    private String senha;
    @Column(name = "ATIVO")
    private Boolean ativo;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "RECEBER_REPASSE")
    private Boolean receberRepasse;
    @Column(name = "trocar_senha")
    private Boolean trocarSenha;
    @Column(name = "RECEBER_REPASSE_HOSPITAL")
    private Boolean receberRepasseHospital;
    @Column(name = "VER_TRANSFERENCIAS")
    private Boolean verTransferencias;
    @Column(name = "nome_completo")
    private String nomeCompleto;
    @Column(name = "CPF")
    private String cpf;;
    @OneToMany(mappedBy = "idUsuarioDestino")
    private List<Caixa> caixaList;
    @OneToMany(mappedBy = "idUsuario")
    private List<Caixa> caixaList1;
    @OneToMany(mappedBy = "idUsuarioDestinatario")
    private List<Alerta> alertaList;
    @OneToMany(mappedBy = "idUsuario")
    private List<Alerta> alertaList1;
    @JoinColumn(name = "ID_PAPEL", referencedColumnName = "ID_PAPEL")
    @ManyToOne
    private Papel idPapel;
    @JoinColumn(name = "ID_TERCEIRO", referencedColumnName = "ID_TERCEIRO")
    @ManyToOne
    private Terceiro idTerceiro;
    @OneToMany(mappedBy = "idUsuario")
    private List<Terceiro> terceiroList;
    @OneToMany(mappedBy = "idUsuario")
    private List<ContaCorrente> contaCorrenteList;
    @OneToMany(mappedBy = "idUsuario")
    private List<Exame> exameList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuario")
    private List<CaixaPai> caixaPaiList;
    @OneToMany(mappedBy = "idUsuario")
    private List<Cheque> chequeList;

    public Usuario() {
    }

    public Usuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getReceberRepasse() {
        return receberRepasse;
    }

    public void setReceberRepasse(Boolean receberRepasse) {
        this.receberRepasse = receberRepasse;
    }

    public Boolean getReceberRepasseHospital() {
        return receberRepasseHospital;
    }

    public void setReceberRepasseHospital(Boolean receberRepasseHospital) {
        this.receberRepasseHospital = receberRepasseHospital;
    }

    @XmlTransient
    public List<Caixa> getCaixaList() {
        return caixaList;
    }

    public void setCaixaList(List<Caixa> caixaList) {
        this.caixaList = caixaList;
    }

    @XmlTransient
    public List<Caixa> getCaixaList1() {
        return caixaList1;
    }

    public void setCaixaList1(List<Caixa> caixaList1) {
        this.caixaList1 = caixaList1;
    }

    @XmlTransient
    public List<Alerta> getAlertaList() {
        return alertaList;
    }

    public void setAlertaList(List<Alerta> alertaList) {
        this.alertaList = alertaList;
    }

    @XmlTransient
    public List<Alerta> getAlertaList1() {
        return alertaList1;
    }

    public void setAlertaList1(List<Alerta> alertaList1) {
        this.alertaList1 = alertaList1;
    }

    public Papel getIdPapel() {
        return idPapel;
    }

    public void setIdPapel(Papel idPapel) {
        this.idPapel = idPapel;
    }

    public Terceiro getIdTerceiro() {
        return idTerceiro;
    }

    public void setIdTerceiro(Terceiro idTerceiro) {
        this.idTerceiro = idTerceiro;
    }

    public Boolean getVerTransferencias() {
        return verTransferencias;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public void setVerTransferencias(Boolean verTransferencias) {
        this.verTransferencias = verTransferencias;
    }
    
    @XmlTransient
    public List<Terceiro> getTerceiroList() {
        return terceiroList;
    }

    public void setTerceiroList(List<Terceiro> terceiroList) {
        this.terceiroList = terceiroList;
    }

    @XmlTransient
    public List<ContaCorrente> getContaCorrenteList() {
        return contaCorrenteList;
    }

    public void setContaCorrenteList(List<ContaCorrente> contaCorrenteList) {
        this.contaCorrenteList = contaCorrenteList;
    }

    @XmlTransient
    public List<Exame> getExameList() {
        return exameList;
    }

    public void setExameList(List<Exame> exameList) {
        this.exameList = exameList;
    }

    @XmlTransient
    public List<CaixaPai> getCaixaPaiList() {
        return caixaPaiList;
    }

    public void setCaixaPaiList(List<CaixaPai> caixaPaiList) {
        this.caixaPaiList = caixaPaiList;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Usuario[ idUsuario=" + idUsuario + " ]";
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }

    public Boolean getTrocarSenha() {
        return trocarSenha;
    }

    public void setTrocarSenha(Boolean trocarSenha) {
        this.trocarSenha = trocarSenha;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList() {
        return orcamentoList;
    }

    public void setOrcamentoList(List<Orcamento> orcamentoList) {
        this.orcamentoList = orcamentoList;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList1() {
        return orcamentoList1;
    }

    public void setOrcamentoList1(List<Orcamento> orcamentoList1) {
        this.orcamentoList1 = orcamentoList1;
    }

    @XmlTransient
    public List<Orcamento> getOrcamentoList2() {
        return orcamentoList2;
    }

    public void setOrcamentoList2(List<Orcamento> orcamentoList2) {
        this.orcamentoList2 = orcamentoList2;
    }

    @XmlTransient
    public List<Cirurgia> getCirurgiaList() {
        return cirurgiaList;
    }

    public void setCirurgiaList(List<Cirurgia> cirurgiaList) {
        this.cirurgiaList = cirurgiaList;
    }

    @XmlTransient
    public List<Emails> getEmailsList() {
        return emailsList;
    }

    public void setEmailsList(List<Emails> emailsList) {
        this.emailsList = emailsList;
    }

    @XmlTransient
    public List<Emails> getEmailsList1() {
        return emailsList1;
    }

    public void setEmailsList1(List<Emails> emailsList1) {
        this.emailsList1 = emailsList1;
    }
}
