/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Charles
 */
@Entity
@Table(name = "papel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Papel.findAll", query = "SELECT p FROM Papel p"),
    @NamedQuery(name = "Papel.findByIdPapel", query = "SELECT p FROM Papel p WHERE p.idPapel = :idPapel"),
    @NamedQuery(name = "Papel.findByRole", query = "SELECT p FROM Papel p WHERE p.role = :role"),
    @NamedQuery(name = "Papel.findByDescricao", query = "SELECT p FROM Papel p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "Papel.findByMenupaciente", query = "SELECT p FROM Papel p WHERE p.menupaciente = :menupaciente"),
    @NamedQuery(name = "Papel.findByMenuterceiro", query = "SELECT p FROM Papel p WHERE p.menuterceiro = :menuterceiro"),
    @NamedQuery(name = "Papel.findByMenuitensorcto", query = "SELECT p FROM Papel p WHERE p.menuitensorcto = :menuitensorcto"),
    @NamedQuery(name = "Papel.findByMenuplano", query = "SELECT p FROM Papel p WHERE p.menuplano = :menuplano"),
    @NamedQuery(name = "Papel.findByMenuexame", query = "SELECT p FROM Papel p WHERE p.menuexame = :menuexame"),
    @NamedQuery(name = "Papel.findByMenumotivo", query = "SELECT p FROM Papel p WHERE p.menumotivo = :menumotivo"),
    @NamedQuery(name = "Papel.findByMenubanco", query = "SELECT p FROM Papel p WHERE p.menubanco = :menubanco"),
    @NamedQuery(name = "Papel.findByMenuconvenio", query = "SELECT p FROM Papel p WHERE p.menuconvenio = :menuconvenio"),
    @NamedQuery(name = "Papel.findByMenuprocedimento", query = "SELECT p FROM Papel p WHERE p.menuprocedimento = :menuprocedimento"),
    @NamedQuery(name = "Papel.findByMenuespecialidade", query = "SELECT p FROM Papel p WHERE p.menuespecialidade = :menuespecialidade"),
    @NamedQuery(name = "Papel.findByMenuunidadeatendimento", query = "SELECT p FROM Papel p WHERE p.menuunidadeatendimento = :menuunidadeatendimento"),
    @NamedQuery(name = "Papel.findByMenuformapagto", query = "SELECT p FROM Papel p WHERE p.menuformapagto = :menuformapagto"),
    @NamedQuery(name = "Papel.findByMenuoperacaocaixa", query = "SELECT p FROM Papel p WHERE p.menuoperacaocaixa = :menuoperacaocaixa"),
    @NamedQuery(name = "Papel.findByMenuorcamento", query = "SELECT p FROM Papel p WHERE p.menuorcamento = :menuorcamento"),
    @NamedQuery(name = "Papel.findByMenucirurgia", query = "SELECT p FROM Papel p WHERE p.menucirurgia = :menucirurgia"),
    @NamedQuery(name = "Papel.findByMenulancarexame", query = "SELECT p FROM Papel p WHERE p.menulancarexame = :menulancarexame"),
    @NamedQuery(name = "Papel.findByMenucontrolecaixa", query = "SELECT p FROM Papel p WHERE p.menucontrolecaixa = :menucontrolecaixa"),
    @NamedQuery(name = "Papel.findByMenulancarcaixa", query = "SELECT p FROM Papel p WHERE p.menulancarcaixa = :menulancarcaixa"),
    @NamedQuery(name = "Papel.findByMenuestornorecebimento", query = "SELECT p FROM Papel p WHERE p.menuestornorecebimento = :menuestornorecebimento"),
    @NamedQuery(name = "Papel.findByMenucontrolecheques", query = "SELECT p FROM Papel p WHERE p.menucontrolecheques = :menucontrolecheques"),
    @NamedQuery(name = "Papel.findByMenuconsultaorcamento", query = "SELECT p FROM Papel p WHERE p.menuconsultaorcamento = :menuconsultaorcamento"),
    @NamedQuery(name = "Papel.findByMenuconsultacirurgia", query = "SELECT p FROM Papel p WHERE p.menuconsultacirurgia = :menuconsultacirurgia"),
    @NamedQuery(name = "Papel.findByMenuconsultaterceiro", query = "SELECT p FROM Papel p WHERE p.menuconsultaterceiro = :menuconsultaterceiro"),
    @NamedQuery(name = "Papel.findByMenuconsultapaciente", query = "SELECT p FROM Papel p WHERE p.menuconsultapaciente = :menuconsultapaciente"),
    @NamedQuery(name = "Papel.findByMenuconsultacontacorrente", query = "SELECT p FROM Papel p WHERE p.menuconsultacontacorrente = :menuconsultacontacorrente"),
    @NamedQuery(name = "Papel.findByMenulistagemterceiro", query = "SELECT p FROM Papel p WHERE p.menulistagemterceiro = :menulistagemterceiro"),
    @NamedQuery(name = "Papel.findByMenulistagempaciente", query = "SELECT p FROM Papel p WHERE p.menulistagempaciente = :menulistagempaciente"),
    @NamedQuery(name = "Papel.findByMenulistagemorcamento", query = "SELECT p FROM Papel p WHERE p.menulistagemorcamento = :menulistagemorcamento"),
    @NamedQuery(name = "Papel.findByMenulistagemcirurgia", query = "SELECT p FROM Papel p WHERE p.menulistagemcirurgia = :menulistagemcirurgia"),
    @NamedQuery(name = "Papel.findByMenulistagemexame", query = "SELECT p FROM Papel p WHERE p.menulistagemexame = :menulistagemexame"),
    @NamedQuery(name = "Papel.findByMenuusuario", query = "SELECT p FROM Papel p WHERE p.menuusuario = :menuusuario"),
    @NamedQuery(name = "Papel.findByMenupermissao", query = "SELECT p FROM Papel p WHERE p.menupermissao = :menupermissao"),
    @NamedQuery(name = "Papel.findByMenuauditoria", query = "SELECT p FROM Papel p WHERE p.menuauditoria = :menuauditoria"),
    @NamedQuery(name = "Papel.findByMenuassociacao", query = "SELECT p FROM Papel p WHERE p.menuassociacao = :menuassociacao"),
    @NamedQuery(name = "Papel.findByMenubackup", query = "SELECT p FROM Papel p WHERE p.menubackup = :menubackup"),
    @NamedQuery(name = "Papel.findByVercaixas", query = "SELECT p FROM Papel p WHERE p.vercaixas = :vercaixas"),
    @NamedQuery(name = "Papel.findByAbrircaixa", query = "SELECT p FROM Papel p WHERE p.abrircaixa = :abrircaixa"),
    @NamedQuery(name = "Papel.findByFecharcaixa", query = "SELECT p FROM Papel p WHERE p.fecharcaixa = :fecharcaixa"),
    @NamedQuery(name = "Papel.findByReabrirfecharcaixa", query = "SELECT p FROM Papel p WHERE p.reabrirfecharcaixa = :reabrirfecharcaixa"),
    @NamedQuery(name = "Papel.findByLancarcaixa", query = "SELECT p FROM Papel p WHERE p.lancarcaixa = :lancarcaixa"),
    @NamedQuery(name = "Papel.findByEditarpaciente", query = "SELECT p FROM Papel p WHERE p.editarpaciente = :editarpaciente"),
    @NamedQuery(name = "Papel.findByInfopaciente", query = "SELECT p FROM Papel p WHERE p.infopaciente = :infopaciente"),
    @NamedQuery(name = "Papel.findByExcluirpaciente", query = "SELECT p FROM Papel p WHERE p.excluirpaciente = :excluirpaciente"),
    @NamedQuery(name = "Papel.findByNovopaciente", query = "SELECT p FROM Papel p WHERE p.novopaciente = :novopaciente"),
    @NamedQuery(name = "Papel.findByEditarterceiro", query = "SELECT p FROM Papel p WHERE p.editarterceiro = :editarterceiro"),
    @NamedQuery(name = "Papel.findByInfoterceiro", query = "SELECT p FROM Papel p WHERE p.infoterceiro = :infoterceiro"),
    @NamedQuery(name = "Papel.findByExcluirterceiro", query = "SELECT p FROM Papel p WHERE p.excluirterceiro = :excluirterceiro"),
    @NamedQuery(name = "Papel.findByNovoterceiro", query = "SELECT p FROM Papel p WHERE p.novoterceiro = :novoterceiro"),
    @NamedQuery(name = "Papel.findByEditaritensorcamento", query = "SELECT p FROM Papel p WHERE p.editaritensorcamento = :editaritensorcamento"),
    @NamedQuery(name = "Papel.findByInfoitensorcamento", query = "SELECT p FROM Papel p WHERE p.infoitensorcamento = :infoitensorcamento"),
    @NamedQuery(name = "Papel.findByExcluiritensorcamento", query = "SELECT p FROM Papel p WHERE p.excluiritensorcamento = :excluiritensorcamento"),
    @NamedQuery(name = "Papel.findByNovoitensorcamento", query = "SELECT p FROM Papel p WHERE p.novoitensorcamento = :novoitensorcamento"),
    @NamedQuery(name = "Papel.findByEditardescexame", query = "SELECT p FROM Papel p WHERE p.editardescexame = :editardescexame"),
    @NamedQuery(name = "Papel.findByExcluirdescexame", query = "SELECT p FROM Papel p WHERE p.excluirdescexame = :excluirdescexame"),
    @NamedQuery(name = "Papel.findByNovodescexame", query = "SELECT p FROM Papel p WHERE p.novodescexame = :novodescexame"),
    @NamedQuery(name = "Papel.findByEditarmotivo", query = "SELECT p FROM Papel p WHERE p.editarmotivo = :editarmotivo"),
    @NamedQuery(name = "Papel.findByExcluirmotivo", query = "SELECT p FROM Papel p WHERE p.excluirmotivo = :excluirmotivo"),
    @NamedQuery(name = "Papel.findByNovomotivo", query = "SELECT p FROM Papel p WHERE p.novomotivo = :novomotivo"),
    @NamedQuery(name = "Papel.findByEditarbanco", query = "SELECT p FROM Papel p WHERE p.editarbanco = :editarbanco"),
    @NamedQuery(name = "Papel.findByExcluirbanco", query = "SELECT p FROM Papel p WHERE p.excluirbanco = :excluirbanco"),
    @NamedQuery(name = "Papel.findByNovobanco", query = "SELECT p FROM Papel p WHERE p.novobanco = :novobanco"),
    @NamedQuery(name = "Papel.findByEditarconvenio", query = "SELECT p FROM Papel p WHERE p.editarconvenio = :editarconvenio"),
    @NamedQuery(name = "Papel.findByExcluirconvenio", query = "SELECT p FROM Papel p WHERE p.excluirconvenio = :excluirconvenio"),
    @NamedQuery(name = "Papel.findByNovoconvenio", query = "SELECT p FROM Papel p WHERE p.novoconvenio = :novoconvenio"),
    @NamedQuery(name = "Papel.findByEditarprocedimento", query = "SELECT p FROM Papel p WHERE p.editarprocedimento = :editarprocedimento"),
    @NamedQuery(name = "Papel.findByExcluirprocedimento", query = "SELECT p FROM Papel p WHERE p.excluirprocedimento = :excluirprocedimento"),
    @NamedQuery(name = "Papel.findByNovoprocedimento", query = "SELECT p FROM Papel p WHERE p.novoprocedimento = :novoprocedimento"),
    @NamedQuery(name = "Papel.findByEditarespecialidade", query = "SELECT p FROM Papel p WHERE p.editarespecialidade = :editarespecialidade"),
    @NamedQuery(name = "Papel.findByExcluirespecialidade", query = "SELECT p FROM Papel p WHERE p.excluirespecialidade = :excluirespecialidade"),
    @NamedQuery(name = "Papel.findByNovoespecialidade", query = "SELECT p FROM Papel p WHERE p.novoespecialidade = :novoespecialidade"),
    @NamedQuery(name = "Papel.findByEditarunidadeatendimento", query = "SELECT p FROM Papel p WHERE p.editarunidadeatendimento = :editarunidadeatendimento"),
    @NamedQuery(name = "Papel.findByExcluirunidadeatendimento", query = "SELECT p FROM Papel p WHERE p.excluirunidadeatendimento = :excluirunidadeatendimento"),
    @NamedQuery(name = "Papel.findByNovounidadeatendimento", query = "SELECT p FROM Papel p WHERE p.novounidadeatendimento = :novounidadeatendimento"),
    @NamedQuery(name = "Papel.findByEditarformapagamento", query = "SELECT p FROM Papel p WHERE p.editarformapagamento = :editarformapagamento"),
    @NamedQuery(name = "Papel.findByExcluirformapagamento", query = "SELECT p FROM Papel p WHERE p.excluirformapagamento = :excluirformapagamento"),
    @NamedQuery(name = "Papel.findByNovoformapagamento", query = "SELECT p FROM Papel p WHERE p.novoformapagamento = :novoformapagamento"),
    @NamedQuery(name = "Papel.findByEditaroperacaocaixa", query = "SELECT p FROM Papel p WHERE p.editaroperacaocaixa = :editaroperacaocaixa"),
    @NamedQuery(name = "Papel.findByExcluiroperacaocaixa", query = "SELECT p FROM Papel p WHERE p.excluiroperacaocaixa = :excluiroperacaocaixa"),
    @NamedQuery(name = "Papel.findByNovooperacaocaixa", query = "SELECT p FROM Papel p WHERE p.novooperacaocaixa = :novooperacaocaixa"),
    @NamedQuery(name = "Papel.findByEditarorcamento", query = "SELECT p FROM Papel p WHERE p.editarorcamento = :editarorcamento"),
    @NamedQuery(name = "Papel.findByInfoorcamento", query = "SELECT p FROM Papel p WHERE p.infoorcamento = :infoorcamento"),
    @NamedQuery(name = "Papel.findByExcluirorcamento", query = "SELECT p FROM Papel p WHERE p.excluirorcamento = :excluirorcamento"),
    @NamedQuery(name = "Papel.findByNovoorcamento", query = "SELECT p FROM Papel p WHERE p.novoorcamento = :novoorcamento"),
    @NamedQuery(name = "Papel.findByCopiarorcamento", query = "SELECT p FROM Papel p WHERE p.copiarorcamento = :copiarorcamento"),
    @NamedQuery(name = "Papel.findByLiberarorcamento", query = "SELECT p FROM Papel p WHERE p.liberarorcamento = :liberarorcamento"),
    @NamedQuery(name = "Papel.findByCancelarorcamento", query = "SELECT p FROM Papel p WHERE p.cancelarorcamento = :cancelarorcamento"),
    @NamedQuery(name = "Papel.findByInfointernacao", query = "SELECT p FROM Papel p WHERE p.infointernacao = :infointernacao"),
    @NamedQuery(name = "Papel.findByEditarinternacao", query = "SELECT p FROM Papel p WHERE p.editarinternacao = :editarinternacao"),
    @NamedQuery(name = "Papel.findByExcluirinternacao", query = "SELECT p FROM Papel p WHERE p.excluirinternacao = :excluirinternacao"),
    @NamedQuery(name = "Papel.findByLiberarinternacao", query = "SELECT p FROM Papel p WHERE p.liberarinternacao = :liberarinternacao"),
    @NamedQuery(name = "Papel.findByCancelarinternacao", query = "SELECT p FROM Papel p WHERE p.cancelarinternacao = :cancelarinternacao"),
    @NamedQuery(name = "Papel.findByNovainternacao", query = "SELECT p FROM Papel p WHERE p.novainternacao = :novainternacao"),
    @NamedQuery(name = "Papel.findByMovimentoFinanceiro", query = "SELECT p FROM Papel p WHERE p.movimentoFinanceiro = :movimentoFinanceiro"),
    @NamedQuery(name = "Papel.findByMovimentoRepasse", query = "SELECT p FROM Papel p WHERE p.movimentoRepasse = :movimentoRepasse"),
    @NamedQuery(name = "Papel.findByReceberLancamento", query = "SELECT p FROM Papel p WHERE p.receberLancamento = :receberLancamento"),
    @NamedQuery(name = "Papel.findByExcluirCheque", query = "SELECT p FROM Papel p WHERE p.excluirCheque = :excluirCheque"),
    @NamedQuery(name = "Papel.findByEditarCheque", query = "SELECT p FROM Papel p WHERE p.editarCheque = :editarCheque"),
    @NamedQuery(name = "Papel.findByCompensarCheque", query = "SELECT p FROM Papel p WHERE p.compensarCheque = :compensarCheque"),
    @NamedQuery(name = "Papel.findByRealizarTransferencia", query = "SELECT p FROM Papel p WHERE p.realizarTransferencia = :realizarTransferencia"),
    @NamedQuery(name = "Papel.findByVerTransferencia", query = "SELECT p FROM Papel p WHERE p.verTransferencia = :verTransferencia")})
public class Papel implements Serializable {

    @Column(name = "permitir_estorno")
    private Boolean permitirEstorno;

    @Column(name = "VER_TRANSFER\u00caNCIA")
    private Boolean verTransferência;
    @Column(name = "RESUMO_PACIENTE")
    private Boolean resumoPaciente;
    @Column(name = "resumo_medico")
    private Boolean resumoMedico;
    @Column(name = "DASHBOARD")
    private Boolean dashboard;
    @Column(name = "inativo")
    private Boolean inativo = Boolean.FALSE;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PAPEL")
    private Integer idPapel;
    @Column(name = "ROLE")
    private String role;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "MENUPACIENTE")
    private Boolean menupaciente;
    @Column(name = "MENUTERCEIRO")
    private Boolean menuterceiro;
    @Column(name = "MENUITENSORCTO")
    private Boolean menuitensorcto;
    @Column(name = "MENUPLANO")
    private Boolean menuplano;
    @Column(name = "MENUEXAME")
    private Boolean menuexame;
    @Column(name = "MENUMOTIVO")
    private Boolean menumotivo;
    @Column(name = "MENUBANCO")
    private Boolean menubanco;
    @Column(name = "MENUCONVENIO")
    private Boolean menuconvenio;
    @Column(name = "MENUPROCEDIMENTO")
    private Boolean menuprocedimento;
    @Column(name = "MENUESPECIALIDADE")
    private Boolean menuespecialidade;
    @Column(name = "MENUUNIDADEATENDIMENTO")
    private Boolean menuunidadeatendimento;
    @Column(name = "MENUFORMAPAGTO")
    private Boolean menuformapagto;
    @Column(name = "MENUOPERACAOCAIXA")
    private Boolean menuoperacaocaixa;
    @Column(name = "MENUORCAMENTO")
    private Boolean menuorcamento;
    @Column(name = "MENUCIRURGIA")
    private Boolean menucirurgia;
    @Column(name = "MENULANCAREXAME")
    private Boolean menulancarexame;
    @Column(name = "MENUCONTROLECAIXA")
    private Boolean menucontrolecaixa;
    @Column(name = "MENULANCARCAIXA")
    private Boolean menulancarcaixa;
    @Column(name = "MENUESTORNORECEBIMENTO")
    private Boolean menuestornorecebimento;
    @Column(name = "MENUCONTROLECHEQUES")
    private Boolean menucontrolecheques;
    @Column(name = "MENUCONSULTAORCAMENTO")
    private Boolean menuconsultaorcamento;
    @Column(name = "MENUCONSULTACIRURGIA")
    private Boolean menuconsultacirurgia;
    @Column(name = "MENUCONSULTATERCEIRO")
    private Boolean menuconsultaterceiro;
    @Column(name = "MENUCONSULTAPACIENTE")
    private Boolean menuconsultapaciente;
    @Column(name = "MENUCONSULTACONTACORRENTE")
    private Boolean menuconsultacontacorrente;
    @Column(name = "MENULISTAGEMTERCEIRO")
    private Boolean menulistagemterceiro;
    @Column(name = "MENULISTAGEMPACIENTE")
    private Boolean menulistagempaciente;
    @Column(name = "MENULISTAGEMORCAMENTO")
    private Boolean menulistagemorcamento;
    @Column(name = "MENULISTAGEMCIRURGIA")
    private Boolean menulistagemcirurgia;
    @Column(name = "MENULISTAGEMEXAME")
    private Boolean menulistagemexame;
    @Column(name = "MENUUSUARIO")
    private Boolean menuusuario;
    @Column(name = "MENUPERMISSAO")
    private Boolean menupermissao;
    @Column(name = "MENUAUDITORIA")
    private Boolean menuauditoria;
    @Column(name = "MENUASSOCIACAO")
    private Boolean menuassociacao;
    @Column(name = "MENUBACKUP")
    private Boolean menubackup;
    @Column(name = "VERCAIXAS")
    private Boolean vercaixas;
    @Column(name = "ABRIRCAIXA")
    private Boolean abrircaixa;
    @Column(name = "FECHARCAIXA")
    private Boolean fecharcaixa;
    @Column(name = "REABRIRFECHARCAIXA")
    private Boolean reabrirfecharcaixa;
    @Column(name = "LANCARCAIXA")
    private Boolean lancarcaixa;
    @Column(name = "EDITARPACIENTE")
    private Boolean editarpaciente;
    @Column(name = "INFOPACIENTE")
    private Boolean infopaciente;
    @Column(name = "EXCLUIRPACIENTE")
    private Boolean excluirpaciente;
    @Column(name = "NOVOPACIENTE")
    private Boolean novopaciente;
    @Column(name = "EDITARTERCEIRO")
    private Boolean editarterceiro;
    @Column(name = "INFOTERCEIRO")
    private Boolean infoterceiro;
    @Column(name = "EXCLUIRTERCEIRO")
    private Boolean excluirterceiro;
    @Column(name = "NOVOTERCEIRO")
    private Boolean novoterceiro;
    @Column(name = "EDITARITENSORCAMENTO")
    private Boolean editaritensorcamento;
    @Column(name = "INFOITENSORCAMENTO")
    private Boolean infoitensorcamento;
    @Column(name = "EXCLUIRITENSORCAMENTO")
    private Boolean excluiritensorcamento;
    @Column(name = "NOVOITENSORCAMENTO")
    private Boolean novoitensorcamento;
    @Column(name = "EDITARDESCEXAME")
    private Boolean editardescexame;
    @Column(name = "EXCLUIRDESCEXAME")
    private Boolean excluirdescexame;
    @Column(name = "NOVODESCEXAME")
    private Boolean novodescexame;
    @Column(name = "EDITARMOTIVO")
    private Boolean editarmotivo;
    @Column(name = "EXCLUIRMOTIVO")
    private Boolean excluirmotivo;
    @Column(name = "NOVOMOTIVO")
    private Boolean novomotivo;
    @Column(name = "EDITARBANCO")
    private Boolean editarbanco;
    @Column(name = "EXCLUIRBANCO")
    private Boolean excluirbanco;
    @Column(name = "NOVOBANCO")
    private Boolean novobanco;
    @Column(name = "EDITARCONVENIO")
    private Boolean editarconvenio;
    @Column(name = "EXCLUIRCONVENIO")
    private Boolean excluirconvenio;
    @Column(name = "NOVOCONVENIO")
    private Boolean novoconvenio;
    @Column(name = "EDITARPROCEDIMENTO")
    private Boolean editarprocedimento;
    @Column(name = "EXCLUIRPROCEDIMENTO")
    private Boolean excluirprocedimento;
    @Column(name = "NOVOPROCEDIMENTO")
    private Boolean novoprocedimento;
    @Column(name = "EDITARESPECIALIDADE")
    private Boolean editarespecialidade;
    @Column(name = "EXCLUIRESPECIALIDADE")
    private Boolean excluirespecialidade;
    @Column(name = "NOVOESPECIALIDADE")
    private Boolean novoespecialidade;
    @Column(name = "EDITARUNIDADEATENDIMENTO")
    private Boolean editarunidadeatendimento;
    @Column(name = "EXCLUIRUNIDADEATENDIMENTO")
    private Boolean excluirunidadeatendimento;
    @Column(name = "NOVOUNIDADEATENDIMENTO")
    private Boolean novounidadeatendimento;
    @Column(name = "EDITARFORMAPAGAMENTO")
    private Boolean editarformapagamento;
    @Column(name = "EXCLUIRFORMAPAGAMENTO")
    private Boolean excluirformapagamento;
    @Column(name = "NOVOFORMAPAGAMENTO")
    private Boolean novoformapagamento;
    @Column(name = "EDITAROPERACAOCAIXA")
    private Boolean editaroperacaocaixa;
    @Column(name = "EXCLUIROPERACAOCAIXA")
    private Boolean excluiroperacaocaixa;
    @Column(name = "NOVOOPERACAOCAIXA")
    private Boolean novooperacaocaixa;
    @Column(name = "EDITARORCAMENTO")
    private Boolean editarorcamento;
    @Column(name = "INFOORCAMENTO")
    private Boolean infoorcamento;
    @Column(name = "EXCLUIRORCAMENTO")
    private Boolean excluirorcamento;
    @Column(name = "NOVOORCAMENTO")
    private Boolean novoorcamento;
    @Column(name = "COPIARORCAMENTO")
    private Boolean copiarorcamento;
    @Column(name = "LIBERARORCAMENTO")
    private Boolean liberarorcamento;
    @Column(name = "CANCELARORCAMENTO")
    private Boolean cancelarorcamento;
    @Column(name = "INFOINTERNACAO")
    private Boolean infointernacao;
    @Column(name = "EDITARINTERNACAO")
    private Boolean editarinternacao;
    @Column(name = "EXCLUIRINTERNACAO")
    private Boolean excluirinternacao;
    @Column(name = "LIBERARINTERNACAO")
    private Boolean liberarinternacao;
    @Column(name = "CANCELARINTERNACAO")
    private Boolean cancelarinternacao;
    @Column(name = "NOVAINTERNACAO")
    private Boolean novainternacao;
    @Column(name = "MOVIMENTO_FINANCEIRO")
    private Boolean movimentoFinanceiro;
    @Column(name = "MOVIMENTO_REPASSE")
    private Boolean movimentoRepasse;
    @Column(name = "RECEBER_LANCAMENTO")
    private Boolean receberLancamento;
    @Column(name = "EXCLUIR_CHEQUE")
    private Boolean excluirCheque;
    @Column(name = "EDITAR_CHEQUE")
    private Boolean editarCheque;
    @Column(name = "COMPENSAR_CHEQUE")
    private Boolean compensarCheque;
    @Column(name = "REALIZAR_TRANSFERENCIA")
    private Boolean realizarTransferencia;
    @Column(name = "VER_TRANSFERENCIA")
    private Boolean verTransferencia;
    @Column(name = "LANCAMENTO_CHEQUE_AVULSO")
    private Boolean lancamentoChequeAvulso;
    @Column(name = "LANCAMENTO_AVULSO")
    private Boolean lancamentoAvulso;
    @Column(name = "movimentar_cheque")
    private Boolean movimentarCheque;
    @Column(name = "relatorio_orcamentos")
    private Boolean relatorioOrcamentos;
    @Column(name = "relatorio_fechamento_alta")
    private Boolean relatorioFechamentoAlta;
    @Column(name = "relatorio_lancamento_avulso")
    private Boolean relatorioLancamentoAvulso;
    @Column(name = "relatorio_lancamento_cheques")
    private Boolean relatorioLancamentoCheques;
    @Column(name = "relatorio_resumo_pagamentos")
    private Boolean relatorioResumoPagamentos;
    @Column(name = "relatorio_laudos")
    private Boolean relatorioLaudos;
    @Column(name = "ver_lancamentos_avulsos")
    private Boolean verLancamentosAvulsos;
    @Column(name = "ver_lancamentos_avulsos_perfil")
    private Boolean verLancamentosAvulsosPerfil;
    @Column(name = "ver_todas_transferencias")
    private Boolean verTodasTransferencias;
    @Column(name = "PRONTO_ATENDIMENTO")
    private Boolean prontoAtendimento;
    @Column(name = "ver_notas_promissorias")
    private Boolean verNotasPromissorias;
    
    @Column(name = "ver_regra_repasse")
    private Boolean verRegraRepasse;
    @Column(name = "inserir_regra_repasse")
    private Boolean inserirRegraRepasse;
    @Column(name = "editar_regra_repasse")
    private Boolean editarRegraRepasse;
    @Column(name = "excluir_regra_repasse")
    private Boolean excluirRegraRepasse;
    @Column(name = "permitir_somente_lancar_pagamentos_atendimento_interno")
    private Boolean permitirSomenteLancarPagamentosAtendimentoInterno;
    @Column(name = "permitir_somente_itens_atendimento_interno")
    private Boolean permitirSomenteItensAtendimentoInterno;
    @Column(name = "permitir_desconto_fechamento_interno")
    private Boolean permitirDescontoFechamentoInterno;
    
    @OneToMany(mappedBy = "idPapel")
    private List<Usuario> usuarioList;

    public Papel() {
    }

    public Papel(Integer idPapel) {
        this.idPapel = idPapel;
    }

    public Integer getIdPapel() {
        return idPapel;
    }

    public void setIdPapel(Integer idPapel) {
        this.idPapel = idPapel;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMenupaciente() {
        return menupaciente;
    }

    public void setMenupaciente(Boolean menupaciente) {
        this.menupaciente = menupaciente;
    }

    public Boolean getMenuterceiro() {
        return menuterceiro;
    }

    public void setMenuterceiro(Boolean menuterceiro) {
        this.menuterceiro = menuterceiro;
    }

    public Boolean getMenuitensorcto() {
        return menuitensorcto;
    }

    public void setMenuitensorcto(Boolean menuitensorcto) {
        this.menuitensorcto = menuitensorcto;
    }

    public Boolean getMenuplano() {
        return menuplano;
    }

    public void setMenuplano(Boolean menuplano) {
        this.menuplano = menuplano;
    }

    public Boolean getMenuexame() {
        return menuexame;
    }

    public void setMenuexame(Boolean menuexame) {
        this.menuexame = menuexame;
    }

    public Boolean getMenumotivo() {
        return menumotivo;
    }

    public void setMenumotivo(Boolean menumotivo) {
        this.menumotivo = menumotivo;
    }

    public Boolean getMenubanco() {
        return menubanco;
    }

    public void setMenubanco(Boolean menubanco) {
        this.menubanco = menubanco;
    }

    public Boolean getMenuconvenio() {
        return menuconvenio;
    }

    public void setMenuconvenio(Boolean menuconvenio) {
        this.menuconvenio = menuconvenio;
    }

    public Boolean getMenuprocedimento() {
        return menuprocedimento;
    }

    public void setMenuprocedimento(Boolean menuprocedimento) {
        this.menuprocedimento = menuprocedimento;
    }

    public Boolean getMenuespecialidade() {
        return menuespecialidade;
    }

    public void setMenuespecialidade(Boolean menuespecialidade) {
        this.menuespecialidade = menuespecialidade;
    }

    public Boolean getMenuunidadeatendimento() {
        return menuunidadeatendimento;
    }

    public void setMenuunidadeatendimento(Boolean menuunidadeatendimento) {
        this.menuunidadeatendimento = menuunidadeatendimento;
    }

    public Boolean getMenuformapagto() {
        return menuformapagto;
    }

    public void setMenuformapagto(Boolean menuformapagto) {
        this.menuformapagto = menuformapagto;
    }

    public Boolean getMenuoperacaocaixa() {
        return menuoperacaocaixa;
    }

    public void setMenuoperacaocaixa(Boolean menuoperacaocaixa) {
        this.menuoperacaocaixa = menuoperacaocaixa;
    }

    public Boolean getMenuorcamento() {
        return menuorcamento;
    }

    public void setMenuorcamento(Boolean menuorcamento) {
        this.menuorcamento = menuorcamento;
    }

    public Boolean getMenucirurgia() {
        return menucirurgia;
    }

    public void setMenucirurgia(Boolean menucirurgia) {
        this.menucirurgia = menucirurgia;
    }

    public Boolean getMenulancarexame() {
        return menulancarexame;
    }

    public void setMenulancarexame(Boolean menulancarexame) {
        this.menulancarexame = menulancarexame;
    }

    public Boolean getMenucontrolecaixa() {
        return menucontrolecaixa;
    }

    public void setMenucontrolecaixa(Boolean menucontrolecaixa) {
        this.menucontrolecaixa = menucontrolecaixa;
    }

    public Boolean getMenulancarcaixa() {
        return menulancarcaixa;
    }

    public void setMenulancarcaixa(Boolean menulancarcaixa) {
        this.menulancarcaixa = menulancarcaixa;
    }

    public Boolean getMenuestornorecebimento() {
        return menuestornorecebimento;
    }

    public void setMenuestornorecebimento(Boolean menuestornorecebimento) {
        this.menuestornorecebimento = menuestornorecebimento;
    }

    public Boolean getMenucontrolecheques() {
        return menucontrolecheques;
    }

    public void setMenucontrolecheques(Boolean menucontrolecheques) {
        this.menucontrolecheques = menucontrolecheques;
    }

    public Boolean getMenuconsultaorcamento() {
        return menuconsultaorcamento;
    }

    public void setMenuconsultaorcamento(Boolean menuconsultaorcamento) {
        this.menuconsultaorcamento = menuconsultaorcamento;
    }

    public Boolean getMenuconsultacirurgia() {
        return menuconsultacirurgia;
    }

    public void setMenuconsultacirurgia(Boolean menuconsultacirurgia) {
        this.menuconsultacirurgia = menuconsultacirurgia;
    }

    public Boolean getMenuconsultaterceiro() {
        return menuconsultaterceiro;
    }

    public void setMenuconsultaterceiro(Boolean menuconsultaterceiro) {
        this.menuconsultaterceiro = menuconsultaterceiro;
    }

    public Boolean getMenuconsultapaciente() {
        return menuconsultapaciente;
    }

    public void setMenuconsultapaciente(Boolean menuconsultapaciente) {
        this.menuconsultapaciente = menuconsultapaciente;
    }

    public Boolean getMenuconsultacontacorrente() {
        return menuconsultacontacorrente;
    }

    public void setMenuconsultacontacorrente(Boolean menuconsultacontacorrente) {
        this.menuconsultacontacorrente = menuconsultacontacorrente;
    }

    public Boolean getMenulistagemterceiro() {
        return menulistagemterceiro;
    }

    public void setMenulistagemterceiro(Boolean menulistagemterceiro) {
        this.menulistagemterceiro = menulistagemterceiro;
    }

    public Boolean getMenulistagempaciente() {
        return menulistagempaciente;
    }

    public void setMenulistagempaciente(Boolean menulistagempaciente) {
        this.menulistagempaciente = menulistagempaciente;
    }

    public Boolean getMenulistagemorcamento() {
        return menulistagemorcamento;
    }

    public void setMenulistagemorcamento(Boolean menulistagemorcamento) {
        this.menulistagemorcamento = menulistagemorcamento;
    }

    public Boolean getMenulistagemcirurgia() {
        return menulistagemcirurgia;
    }

    public void setMenulistagemcirurgia(Boolean menulistagemcirurgia) {
        this.menulistagemcirurgia = menulistagemcirurgia;
    }

    public Boolean getMenulistagemexame() {
        return menulistagemexame;
    }

    public void setMenulistagemexame(Boolean menulistagemexame) {
        this.menulistagemexame = menulistagemexame;
    }

    public Boolean getMenuusuario() {
        return menuusuario;
    }

    public void setMenuusuario(Boolean menuusuario) {
        this.menuusuario = menuusuario;
    }

    public Boolean getMenupermissao() {
        return menupermissao;
    }

    public void setMenupermissao(Boolean menupermissao) {
        this.menupermissao = menupermissao;
    }

    public Boolean getMenuauditoria() {
        return menuauditoria;
    }

    public void setMenuauditoria(Boolean menuauditoria) {
        this.menuauditoria = menuauditoria;
    }

    public Boolean getMenuassociacao() {
        return menuassociacao;
    }

    public void setMenuassociacao(Boolean menuassociacao) {
        this.menuassociacao = menuassociacao;
    }

    public Boolean getMenubackup() {
        return menubackup;
    }

    public void setMenubackup(Boolean menubackup) {
        this.menubackup = menubackup;
    }

    public Boolean getVercaixas() {
        return vercaixas;
    }

    public void setVercaixas(Boolean vercaixas) {
        this.vercaixas = vercaixas;
    }

    public Boolean getAbrircaixa() {
        return abrircaixa;
    }

    public void setAbrircaixa(Boolean abrircaixa) {
        this.abrircaixa = abrircaixa;
    }

    public Boolean getFecharcaixa() {
        return fecharcaixa;
    }

    public void setFecharcaixa(Boolean fecharcaixa) {
        this.fecharcaixa = fecharcaixa;
    }

    public Boolean getReabrirfecharcaixa() {
        return reabrirfecharcaixa;
    }

    public void setReabrirfecharcaixa(Boolean reabrirfecharcaixa) {
        this.reabrirfecharcaixa = reabrirfecharcaixa;
    }

    public Boolean getLancarcaixa() {
        return lancarcaixa;
    }

    public void setLancarcaixa(Boolean lancarcaixa) {
        this.lancarcaixa = lancarcaixa;
    }

    public Boolean getEditarpaciente() {
        return editarpaciente;
    }

    public void setEditarpaciente(Boolean editarpaciente) {
        this.editarpaciente = editarpaciente;
    }

    public Boolean getInfopaciente() {
        return infopaciente;
    }

    public void setInfopaciente(Boolean infopaciente) {
        this.infopaciente = infopaciente;
    }

    public Boolean getExcluirpaciente() {
        return excluirpaciente;
    }

    public void setExcluirpaciente(Boolean excluirpaciente) {
        this.excluirpaciente = excluirpaciente;
    }

    public Boolean getNovopaciente() {
        return novopaciente;
    }

    public void setNovopaciente(Boolean novopaciente) {
        this.novopaciente = novopaciente;
    }

    public Boolean getEditarterceiro() {
        return editarterceiro;
    }

    public void setEditarterceiro(Boolean editarterceiro) {
        this.editarterceiro = editarterceiro;
    }

    public Boolean getInfoterceiro() {
        return infoterceiro;
    }

    public void setInfoterceiro(Boolean infoterceiro) {
        this.infoterceiro = infoterceiro;
    }

    public Boolean getExcluirterceiro() {
        return excluirterceiro;
    }

    public void setExcluirterceiro(Boolean excluirterceiro) {
        this.excluirterceiro = excluirterceiro;
    }

    public Boolean getNovoterceiro() {
        return novoterceiro;
    }

    public void setNovoterceiro(Boolean novoterceiro) {
        this.novoterceiro = novoterceiro;
    }

    public Boolean getEditaritensorcamento() {
        return editaritensorcamento;
    }

    public void setEditaritensorcamento(Boolean editaritensorcamento) {
        this.editaritensorcamento = editaritensorcamento;
    }

    public Boolean getInfoitensorcamento() {
        return infoitensorcamento;
    }

    public void setInfoitensorcamento(Boolean infoitensorcamento) {
        this.infoitensorcamento = infoitensorcamento;
    }

    public Boolean getExcluiritensorcamento() {
        return excluiritensorcamento;
    }

    public void setExcluiritensorcamento(Boolean excluiritensorcamento) {
        this.excluiritensorcamento = excluiritensorcamento;
    }

    public Boolean getNovoitensorcamento() {
        return novoitensorcamento;
    }

    public void setNovoitensorcamento(Boolean novoitensorcamento) {
        this.novoitensorcamento = novoitensorcamento;
    }

    public Boolean getEditardescexame() {
        return editardescexame;
    }

    public void setEditardescexame(Boolean editardescexame) {
        this.editardescexame = editardescexame;
    }

    public Boolean getExcluirdescexame() {
        return excluirdescexame;
    }

    public void setExcluirdescexame(Boolean excluirdescexame) {
        this.excluirdescexame = excluirdescexame;
    }

    public Boolean getNovodescexame() {
        return novodescexame;
    }

    public void setNovodescexame(Boolean novodescexame) {
        this.novodescexame = novodescexame;
    }

    public Boolean getEditarmotivo() {
        return editarmotivo;
    }

    public void setEditarmotivo(Boolean editarmotivo) {
        this.editarmotivo = editarmotivo;
    }

    public Boolean getExcluirmotivo() {
        return excluirmotivo;
    }

    public void setExcluirmotivo(Boolean excluirmotivo) {
        this.excluirmotivo = excluirmotivo;
    }

    public Boolean getNovomotivo() {
        return novomotivo;
    }

    public void setNovomotivo(Boolean novomotivo) {
        this.novomotivo = novomotivo;
    }

    public Boolean getEditarbanco() {
        return editarbanco;
    }

    public void setEditarbanco(Boolean editarbanco) {
        this.editarbanco = editarbanco;
    }

    public Boolean getExcluirbanco() {
        return excluirbanco;
    }

    public void setExcluirbanco(Boolean excluirbanco) {
        this.excluirbanco = excluirbanco;
    }

    public Boolean getNovobanco() {
        return novobanco;
    }

    public void setNovobanco(Boolean novobanco) {
        this.novobanco = novobanco;
    }

    public Boolean getEditarconvenio() {
        return editarconvenio;
    }

    public void setEditarconvenio(Boolean editarconvenio) {
        this.editarconvenio = editarconvenio;
    }

    public Boolean getExcluirconvenio() {
        return excluirconvenio;
    }

    public void setExcluirconvenio(Boolean excluirconvenio) {
        this.excluirconvenio = excluirconvenio;
    }

    public Boolean getNovoconvenio() {
        return novoconvenio;
    }

    public void setNovoconvenio(Boolean novoconvenio) {
        this.novoconvenio = novoconvenio;
    }

    public Boolean getEditarprocedimento() {
        return editarprocedimento;
    }

    public void setEditarprocedimento(Boolean editarprocedimento) {
        this.editarprocedimento = editarprocedimento;
    }

    public Boolean getExcluirprocedimento() {
        return excluirprocedimento;
    }

    public void setExcluirprocedimento(Boolean excluirprocedimento) {
        this.excluirprocedimento = excluirprocedimento;
    }

    public Boolean getNovoprocedimento() {
        return novoprocedimento;
    }

    public void setNovoprocedimento(Boolean novoprocedimento) {
        this.novoprocedimento = novoprocedimento;
    }

    public Boolean getEditarespecialidade() {
        return editarespecialidade;
    }

    public void setEditarespecialidade(Boolean editarespecialidade) {
        this.editarespecialidade = editarespecialidade;
    }

    public Boolean getExcluirespecialidade() {
        return excluirespecialidade;
    }

    public void setExcluirespecialidade(Boolean excluirespecialidade) {
        this.excluirespecialidade = excluirespecialidade;
    }

    public Boolean getNovoespecialidade() {
        return novoespecialidade;
    }

    public void setNovoespecialidade(Boolean novoespecialidade) {
        this.novoespecialidade = novoespecialidade;
    }

    public Boolean getEditarunidadeatendimento() {
        return editarunidadeatendimento;
    }

    public void setEditarunidadeatendimento(Boolean editarunidadeatendimento) {
        this.editarunidadeatendimento = editarunidadeatendimento;
    }

    public Boolean getExcluirunidadeatendimento() {
        return excluirunidadeatendimento;
    }

    public void setExcluirunidadeatendimento(Boolean excluirunidadeatendimento) {
        this.excluirunidadeatendimento = excluirunidadeatendimento;
    }

    public Boolean getNovounidadeatendimento() {
        return novounidadeatendimento;
    }

    public void setNovounidadeatendimento(Boolean novounidadeatendimento) {
        this.novounidadeatendimento = novounidadeatendimento;
    }

    public Boolean getEditarformapagamento() {
        return editarformapagamento;
    }

    public void setEditarformapagamento(Boolean editarformapagamento) {
        this.editarformapagamento = editarformapagamento;
    }

    public Boolean getExcluirformapagamento() {
        return excluirformapagamento;
    }

    public void setExcluirformapagamento(Boolean excluirformapagamento) {
        this.excluirformapagamento = excluirformapagamento;
    }

    public Boolean getNovoformapagamento() {
        return novoformapagamento;
    }

    public void setNovoformapagamento(Boolean novoformapagamento) {
        this.novoformapagamento = novoformapagamento;
    }

    public Boolean getEditaroperacaocaixa() {
        return editaroperacaocaixa;
    }

    public void setEditaroperacaocaixa(Boolean editaroperacaocaixa) {
        this.editaroperacaocaixa = editaroperacaocaixa;
    }

    public Boolean getExcluiroperacaocaixa() {
        return excluiroperacaocaixa;
    }

    public void setExcluiroperacaocaixa(Boolean excluiroperacaocaixa) {
        this.excluiroperacaocaixa = excluiroperacaocaixa;
    }

    public Boolean getNovooperacaocaixa() {
        return novooperacaocaixa;
    }

    public void setNovooperacaocaixa(Boolean novooperacaocaixa) {
        this.novooperacaocaixa = novooperacaocaixa;
    }

    public Boolean getEditarorcamento() {
        return editarorcamento;
    }

    public void setEditarorcamento(Boolean editarorcamento) {
        this.editarorcamento = editarorcamento;
    }

    public Boolean getInfoorcamento() {
        return infoorcamento;
    }

    public void setInfoorcamento(Boolean infoorcamento) {
        this.infoorcamento = infoorcamento;
    }

    public Boolean getExcluirorcamento() {
        return excluirorcamento;
    }

    public void setExcluirorcamento(Boolean excluirorcamento) {
        this.excluirorcamento = excluirorcamento;
    }

    public Boolean getNovoorcamento() {
        return novoorcamento;
    }

    public void setNovoorcamento(Boolean novoorcamento) {
        this.novoorcamento = novoorcamento;
    }

    public Boolean getCopiarorcamento() {
        return copiarorcamento;
    }

    public void setCopiarorcamento(Boolean copiarorcamento) {
        this.copiarorcamento = copiarorcamento;
    }

    public Boolean getLiberarorcamento() {
        return liberarorcamento;
    }

    public void setLiberarorcamento(Boolean liberarorcamento) {
        this.liberarorcamento = liberarorcamento;
    }

    public Boolean getCancelarorcamento() {
        return cancelarorcamento;
    }

    public void setCancelarorcamento(Boolean cancelarorcamento) {
        this.cancelarorcamento = cancelarorcamento;
    }

    public Boolean getInfointernacao() {
        return infointernacao;
    }

    public void setInfointernacao(Boolean infointernacao) {
        this.infointernacao = infointernacao;
    }

    public Boolean getEditarinternacao() {
        return editarinternacao;
    }

    public void setEditarinternacao(Boolean editarinternacao) {
        this.editarinternacao = editarinternacao;
    }

    public Boolean getExcluirinternacao() {
        return excluirinternacao;
    }

    public void setExcluirinternacao(Boolean excluirinternacao) {
        this.excluirinternacao = excluirinternacao;
    }

    public Boolean getLiberarinternacao() {
        return liberarinternacao;
    }

    public void setLiberarinternacao(Boolean liberarinternacao) {
        this.liberarinternacao = liberarinternacao;
    }

    public Boolean getCancelarinternacao() {
        return cancelarinternacao;
    }

    public void setCancelarinternacao(Boolean cancelarinternacao) {
        this.cancelarinternacao = cancelarinternacao;
    }

    public Boolean getNovainternacao() {
        return novainternacao;
    }

    public void setNovainternacao(Boolean novainternacao) {
        this.novainternacao = novainternacao;
    }

    public Boolean getMovimentoFinanceiro() {
        return movimentoFinanceiro;
    }

    public void setMovimentoFinanceiro(Boolean movimentoFinanceiro) {
        this.movimentoFinanceiro = movimentoFinanceiro;
    }

    public Boolean getMovimentoRepasse() {
        return movimentoRepasse;
    }

    public void setMovimentoRepasse(Boolean movimentoRepasse) {
        this.movimentoRepasse = movimentoRepasse;
    }

    public Boolean getReceberLancamento() {
        return receberLancamento;
    }

    public void setReceberLancamento(Boolean receberLancamento) {
        this.receberLancamento = receberLancamento;
    }

    public Boolean getExcluirCheque() {
        return excluirCheque;
    }

    public void setExcluirCheque(Boolean excluirCheque) {
        this.excluirCheque = excluirCheque;
    }

    public Boolean getEditarCheque() {
        return editarCheque;
    }

    public void setEditarCheque(Boolean editarCheque) {
        this.editarCheque = editarCheque;
    }

    public Boolean getCompensarCheque() {
        return compensarCheque;
    }

    public void setCompensarCheque(Boolean compensarCheque) {
        this.compensarCheque = compensarCheque;
    }

    public Boolean getRealizarTransferencia() {
        return realizarTransferencia;
    }

    public void setRealizarTransferencia(Boolean realizarTransferencia) {
        this.realizarTransferencia = realizarTransferencia;
    }

    public Boolean getVerTransferencia() {
        return verTransferencia;
    }

    public void setVerTransferencia(Boolean verTransferencia) {
        this.verTransferencia = verTransferencia;
    }

    public Boolean getLancamentoAvulso() {
        return lancamentoAvulso;
    }

    public void setLancamentoAvulso(Boolean lancamentoAvulso) {
        this.lancamentoAvulso = lancamentoAvulso;
    }

    public Boolean getLancamentoChequeAvulso() {
        return lancamentoChequeAvulso;
    }

    public void setLancamentoChequeAvulso(Boolean lancamentoChequeAvulso) {
        this.lancamentoChequeAvulso = lancamentoChequeAvulso;
    }

    public Boolean getMovimentarCheque() {
        return movimentarCheque;
    }

    public void setMovimentarCheque(Boolean movimentarCheque) {
        this.movimentarCheque = movimentarCheque;
    }

    public Boolean getRelatorioOrcamentos() {
        return relatorioOrcamentos;
    }

    public void setRelatorioOrcamentos(Boolean relatorioOrcamentos) {
        this.relatorioOrcamentos = relatorioOrcamentos;
    }

    public Boolean getRelatorioFechamentoAlta() {
        return relatorioFechamentoAlta;
    }

    public void setRelatorioFechamentoAlta(Boolean relatorioFechamentoAlta) {
        this.relatorioFechamentoAlta = relatorioFechamentoAlta;
    }

    public Boolean getRelatorioLancamentoAvulso() {
        return relatorioLancamentoAvulso;
    }

    public void setRelatorioLancamentoAvulso(Boolean relatorioLancamentoAvulso) {
        this.relatorioLancamentoAvulso = relatorioLancamentoAvulso;
    }

    public Boolean getRelatorioLancamentoCheques() {
        return relatorioLancamentoCheques;
    }

    public void setRelatorioLancamentoCheques(Boolean relatorioLancamentoCheques) {
        this.relatorioLancamentoCheques = relatorioLancamentoCheques;
    }

    public Boolean getRelatorioResumoPagamentos() {
        return relatorioResumoPagamentos;
    }

    public void setRelatorioResumoPagamentos(Boolean relatorioResumoPagamentos) {
        this.relatorioResumoPagamentos = relatorioResumoPagamentos;
    }

    public Boolean getResumoPaciente() {
        return resumoPaciente;
    }

    public void setResumoPaciente(Boolean resumoPaciente) {
        this.resumoPaciente = resumoPaciente;
    }

    public Boolean getDashboard() {
        return dashboard;
    }

    public void setDashboard(Boolean dashboard) {
        this.dashboard = dashboard;
    }

    public Boolean getProntoAtendimento() {
        return prontoAtendimento;
    }

    public void setProntoAtendimento(Boolean prontoAtendimento) {
        this.prontoAtendimento = prontoAtendimento;
    }

    public Boolean getVerLancamentosAvulsos() {
        return verLancamentosAvulsos;
    }

    public void setVerLancamentosAvulsos(Boolean verLancamentosAvulsos) {
        this.verLancamentosAvulsos = verLancamentosAvulsos;
    }

    public Boolean getVerTodasTransferencias() {
        return verTodasTransferencias;
    }

    public void setVerTodasTransferencias(Boolean verTodasTransferencias) {
        this.verTodasTransferencias = verTodasTransferencias;
    }

    public Boolean getVerNotasPromissorias() {
        return verNotasPromissorias;
    }

    public void setVerNotasPromissorias(Boolean verNotasPromissorias) {
        this.verNotasPromissorias = verNotasPromissorias;
    }
            
    
    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPapel != null ? idPapel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Papel)) {
            return false;
        }
        Papel other = (Papel) object;
        if ((this.idPapel == null && other.idPapel != null) || (this.idPapel != null && !this.idPapel.equals(other.idPapel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Papel[ idPapel=" + idPapel + " ]";
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }

    public Boolean getVerTransferência() {
        return verTransferência;
    }

    public void setVerTransferência(Boolean verTransferência) {
        this.verTransferência = verTransferência;
    }   

    public Boolean getVerLancamentosAvulsosPerfil() {
        return verLancamentosAvulsosPerfil;
    }

    public void setVerLancamentosAvulsosPerfil(Boolean verLancamentosAvulsosPerfil) {
        this.verLancamentosAvulsosPerfil = verLancamentosAvulsosPerfil;
    }

    public Boolean getVerRegraRepasse() {
        return verRegraRepasse;
    }

    public void setVerRegraRepasse(Boolean verRegraRepasse) {
        this.verRegraRepasse = verRegraRepasse;
    }

    public Boolean getInserirRegraRepasse() {
        return inserirRegraRepasse;
    }

    public void setInserirRegraRepasse(Boolean inserirRegraRepasse) {
        this.inserirRegraRepasse = inserirRegraRepasse;
    }

    public Boolean getEditarRegraRepasse() {
        return editarRegraRepasse;
    }

    public void setEditarRegraRepasse(Boolean editarRegraRepasse) {
        this.editarRegraRepasse = editarRegraRepasse;
    }

    public Boolean getExcluirRegraRepasse() {
        return excluirRegraRepasse;
    }

    public void setExcluirRegraRepasse(Boolean excluirRegraRepasse) {
        this.excluirRegraRepasse = excluirRegraRepasse;
    }

    public Boolean getResumoMedico() {
        return resumoMedico;
    }

    public void setResumoMedico(Boolean resumoMedico) {
        this.resumoMedico = resumoMedico;
    }

    public Boolean getRelatorioLaudos() {
        return relatorioLaudos;
    }

    public void setRelatorioLaudos(Boolean relatorioLaudos) {
        this.relatorioLaudos = relatorioLaudos;
    }

    public Boolean getPermitirSomenteLancarPagamentosAtendimentoInterno() {
        return permitirSomenteLancarPagamentosAtendimentoInterno;
    }

    public void setPermitirSomenteLancarPagamentosAtendimentoInterno(Boolean permitirSomenteLancarPagamentosAtendimentoInterno) {
        this.permitirSomenteLancarPagamentosAtendimentoInterno = permitirSomenteLancarPagamentosAtendimentoInterno;
    }

    public Boolean getPermitirSomenteItensAtendimentoInterno() {
        return permitirSomenteItensAtendimentoInterno;
    }

    public void setPermitirSomenteItensAtendimentoInterno(Boolean permitirSomenteItensAtendimentoInterno) {
        this.permitirSomenteItensAtendimentoInterno = permitirSomenteItensAtendimentoInterno;
    }

    public Boolean getPermitirDescontoFechamentoInterno() {
        return permitirDescontoFechamentoInterno;
    }

    public void setPermitirDescontoFechamentoInterno(Boolean permitirDescontoFechamentoInterno) {
        this.permitirDescontoFechamentoInterno = permitirDescontoFechamentoInterno;
    }

    public Boolean getPermitirEstorno() {
        return permitirEstorno;
    }

    public void setPermitirEstorno(Boolean permitirEstorno) {
        this.permitirEstorno = permitirEstorno;
    }
    
    
    
    
    
    
}
