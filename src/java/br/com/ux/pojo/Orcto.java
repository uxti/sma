/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.pojo;

import java.util.Date;

/**
 *
 * @author Renato
 */
public class Orcto {

    private Integer id_orcto;
    private String paciente;
    private String procedimento;
    private String terceiro;
    private String descricao;
    private String SITUACAO;
    private Date DT_ORCAMENTO;
    private Date DT_PREVISAO;
    private Double total;

    public String getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(String terceiro) {
        this.terceiro = terceiro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    

    public Integer getId_orcto() {
        return id_orcto;
    }

    public void setId_orcto(Integer id_orcto) {
        this.id_orcto = id_orcto;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(String procedimento) {
        this.procedimento = procedimento;
    }

    public String getSITUACAO() {
        return SITUACAO;
    }

    public void setSITUACAO(String SITUACAO) {
        this.SITUACAO = SITUACAO;
    }

    public Date getDT_ORCAMENTO() {
        return DT_ORCAMENTO;
    }

    public void setDT_ORCAMENTO(Date DT_ORCAMENTO) {
        this.DT_ORCAMENTO = DT_ORCAMENTO;
    }

    public Date getDT_PREVISAO() {
        return DT_PREVISAO;
    }

    public void setDT_PREVISAO(Date DT_PREVISAO) {
        this.DT_PREVISAO = DT_PREVISAO;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
