/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.pojo;

/**
 *
 * @author Charles
 */
public class OrcamentoItemDuplicado {
    private Integer idOrcamento;
    private Integer idServico;
    private Integer idOrcamentoItem;

    public Integer getIdOrcamentoItem() {
        return idOrcamentoItem;
    }

    public void setIdOrcamentoItem(Integer idOrcamentoItem) {
        this.idOrcamentoItem = idOrcamentoItem;
    }
    
    public Integer getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(Integer idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public Integer getIdServico() {
        return idServico;
    }

    public void setIdServico(Integer idServico) {
        this.idServico = idServico;
    }
}
