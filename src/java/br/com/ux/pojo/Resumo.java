/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.pojo;

import br.com.ux.model.Cirurgia;
import br.com.ux.model.Orcamento;
import br.com.ux.model.Procedimento;
import br.com.ux.model.Repasse;
import java.util.Date;
import java.util.List;

public class Resumo {
    
    public Orcamento orcamento;
    public Cirurgia fechamento;
    public Procedimento procedimento;
    public Date dtLancamento, dtRetirado;
    public String situacao;
    public Double valor;

    public Orcamento getOrcamento() {
        return orcamento;
    }

    public void setOrcamento(Orcamento orcamento) {
        this.orcamento = orcamento;
    }

    public Cirurgia getFechamento() {
        return fechamento;
    }

    public void setFechamento(Cirurgia fechamento) {
        this.fechamento = fechamento;
    }

    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public Date getDtLancamento() {
        return dtLancamento;
    }

    public void setDtLancamento(Date dtLancamento) {
        this.dtLancamento = dtLancamento;
    }

    public Date getDtRetirado() {
        return dtRetirado;
    }

    public void setDtRetirado(Date dtRetirado) {
        this.dtRetirado = dtRetirado;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
}
