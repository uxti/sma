/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.pojo.tasy;

/**
 *
 * 
 * @author Charles
 */
public class GrupoProcedimentoTasy {
    private Integer cdGrupoProc;
    private String dsGrupoProc, capitulo, origem;
    private boolean ativo;

    public Integer getCdGrupoProc() {
        return cdGrupoProc;
    }

    public void setCdGrupoProc(Integer cdGrupoProc) {
        this.cdGrupoProc = cdGrupoProc;
    }

    public String getDsGrupoProc() {
        return dsGrupoProc;
    }

    public void setDsGrupoProc(String dsGrupoProc) {
        this.dsGrupoProc = dsGrupoProc;
    }

    public String getCapitulo() {
        return capitulo;
    }

    public void setCapitulo(String capitulo) {
        this.capitulo = capitulo;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
    
    
}
