/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pojo;

/**
 *
 * @author Charles
 */
public class MedicoTasy {
    
    private String cdPessoaFisica, nmPessoaFisica, nrCPF, rg, nrTelefone, nrTelefoneCelular, tipoConselho, nrCrm, ieSexo;
    private boolean ieSituacao;

    public String getCdPessoaFisica() {
        return cdPessoaFisica;
    }

    public void setCdPessoaFisica(String cdPessoaFisica) {
        this.cdPessoaFisica = cdPessoaFisica;
    }

    public String getNmPessoaFisica() {
        return nmPessoaFisica;
    }

    public void setNmPessoaFisica(String nmPessoaFisica) {
        this.nmPessoaFisica = nmPessoaFisica;
    }

    public String getNrCPF() {
        return nrCPF;
    }

    public void setNrCPF(String nrCPF) {
        this.nrCPF = nrCPF;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getNrTelefone() {
        return nrTelefone;
    }

    public void setNrTelefone(String nrTelefone) {
        this.nrTelefone = nrTelefone;
    }

    public String getNrTelefoneCelular() {
        return nrTelefoneCelular;
    }

    public void setNrTelefoneCelular(String nrTelefoneCelular) {
        this.nrTelefoneCelular = nrTelefoneCelular;
    }

    public String getTipoConselho() {
        return tipoConselho;
    }

    public void setTipoConselho(String tipoConselho) {
        this.tipoConselho = tipoConselho;
    }

    public String getNrCrm() {
        return nrCrm;
    }

    public void setNrCrm(String nrCrm) {
        this.nrCrm = nrCrm;
    }

    public String getIeSexo() {
        return ieSexo;
    }

    public void setIeSexo(String ieSexo) {
        this.ieSexo = ieSexo;
    }

    public boolean isIeSituacao() {
        return ieSituacao;
    }

    public void setIeSituacao(boolean ieSituacao) {
        this.ieSituacao = ieSituacao;
    }
    
    
    

}
