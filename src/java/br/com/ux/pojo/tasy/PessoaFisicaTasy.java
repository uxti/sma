/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.pojo.tasy;

import java.util.Date;

/**
 *
 * @author Charles
 */
public class PessoaFisicaTasy {
    private String cdPessoaFisica, nmPessoaFisica, nrCPF, nrTelefone, nrTelefoneCelular, nmPessoaMae;
    private Date dtNascimento;

    public String getCdPessoaFisica() {
        return cdPessoaFisica;
    }

    public void setCdPessoaFisica(String cdPessoaFisica) {
        this.cdPessoaFisica = cdPessoaFisica;
    }

    public String getNmPessoaFisica() {
        return nmPessoaFisica;
    }

    public void setNmPessoaFisica(String nmPessoaFisica) {
        this.nmPessoaFisica = nmPessoaFisica;
    }

    public String getNrCPF() {
        return nrCPF;
    }

    public void setNrCPF(String nrCPF) {
        this.nrCPF = nrCPF;
    }

    public String getNrTelefone() {
        return nrTelefone;
    }

    public void setNrTelefone(String nrTelefone) {
        this.nrTelefone = nrTelefone;
    }

    public String getNrTelefoneCelular() {
        return nrTelefoneCelular;
    }

    public void setNrTelefoneCelular(String nrTelefoneCelular) {
        this.nrTelefoneCelular = nrTelefoneCelular;
    }

    public String getNmPessoaMae() {
        return nmPessoaMae;
    }

    public void setNmPessoaMae(String nmPessoaMae) {
        this.nmPessoaMae = nmPessoaMae;
    }

    public Date getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(Date dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

        
    

    
    
}
