/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.pojo.tasy;

/**
 *
 * @author Charles
 */
public class ProcedimentoTasy {
    private String cdProcedimento, cdGrupoProc, dsProcedimento, ieClassificacao;
    private boolean ativo;

    public String getCdProcedimento() {
        return cdProcedimento;
    }

    public void setCdProcedimento(String cdProcedimento) {
        this.cdProcedimento = cdProcedimento;
    }

    public String getCdGrupoProc() {
        return cdGrupoProc;
    }

    public void setCdGrupoProc(String cdGrupoProc) {
        this.cdGrupoProc = cdGrupoProc;
    }

    public String getDsProcedimento() {
        return dsProcedimento;
    }

    public void setDsProcedimento(String dsProcedimento) {
        this.dsProcedimento = dsProcedimento;
    }

    public String getIeClassificacao() {
        return ieClassificacao;
    }

    public void setIeClassificacao(String ieClassificacao) {
        this.ieClassificacao = ieClassificacao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
    
    
    
}
