/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ux.pojo;

import java.util.Objects;

/**
 *
 * @author Charles
 */
public class Documento {
    
    private String codigo;
    private String codigoTasy;     
    private String descricao;
    private String classificacao;

    public Documento(String codigo, String codigoTasy, String descricao, String classificacao) {
        this.codigo = codigo;
        this.codigoTasy = codigoTasy;
        this.descricao = descricao;
        this.classificacao = classificacao;
    }
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoTasy() {
        return codigoTasy;
    }

    public void setCodigoTasy(String codigoTasy) {
        this.codigoTasy = codigoTasy;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }
    
    
}
