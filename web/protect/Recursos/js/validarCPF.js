function validarCPF( cpf ){

    if(cpf == "___.___.___-__"){
        return true;
    }else{
        var filtro = /^\d{3}.\d{3}.\d{3}-\d{2}$/i;
	
        if(!filtro.test(cpf))
        {
            alertify.confirm("CPF inválido. Tente novamente!", function (e) {
                if (e) {
                    document.getElementById("formCadastro:cpf").focus()
                } else {
                    document.getElementById("formCadastro:cpf").focus()
                }
            }).hide();
            //        jQuery('#cpfinv').show();  
            //        jQuery('#cpfva').hide();
            return false;
        }
   
        cpf = remove(cpf, ".");
        cpf = remove(cpf, "-");
	
        if(cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" ||
            cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" ||
            cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" ||
            cpf == "88888888888" || cpf == "99999999999")
            {
            alertify.confirm("CPF inválido. Tente novamente!", function (e) {
                if (e) {
                    document.getElementById("formCadastro:cpf").focus()
                } else {
                    document.getElementById("formCadastro:cpf").focus()
                }
            }).hide();
            //        jQuery('#cpfinv').show();  
            //        jQuery('#cpfva').hide();
            return false;
        }

        soma = 0;
        for(i = 0; i < 9; i++)
        {
            soma += parseInt(cpf.charAt(i)) * (10 - i);
        }
	
        resto = 11 - (soma % 11);
        if(resto == 10 || resto == 11)
        {
            resto = 0;
        }
        if(resto != parseInt(cpf.charAt(9))){
            alertify.confirm("CPF inválido. Tente novamente!", function (e) {
                if (e) {
                    document.getElementById("formCadastro:cpf").focus()
                } else {
                    document.getElementById("formCadastro:cpf").focus()
                }
            }).hide();
            //        jQuery('#cpfinv').show();  
            //        jQuery('#cpfva').hide();
            return false;
        }
	
        soma = 0;
        for(i = 0; i < 10; i ++)
        {
            soma += parseInt(cpf.charAt(i)) * (11 - i);
        }
        resto = 11 - (soma % 11);
        if(resto == 10 || resto == 11)
        {
            resto = 0;
        }
	
        if(resto != parseInt(cpf.charAt(10))){
            alertify.confirm("CPF inválido. Tente novamente!", function (e) {
                if (e) {
                    document.getElementById("formCadastro:cpf").focus()
                } else {
                    document.getElementById("formCadastro:cpf").focus()
                }
            }).hide();
            //        jQuery('#cpfinv').show();  
            //        jQuery('#cpfva').hide();
        
            return false;
        }
        //    jQuery('#cpfinv').hide();  
        //    jQuery('#cpfva').show();  
        document.getElementById("formCadastro:cpf").style.border="solid 1px #e5e6e7";
        return true;
    }
    
}
 
function remove(str, sub) {
    i = str.indexOf(sub);
    r = "";
    if (i == -1) return str;
    {
        r += str.substring(0,i) + remove(str.substring(i + sub.length), sub);
    }
	
    return r;
}

/**
   * MASCARA ( mascara(o,f) e execmascara() ) CRIADAS POR ELCIO LUIZ
   * elcio.com.br
   */
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function cpf_mask(v){
    v=v.replace(/\D/g,"")                 //Remove tudo o que não é dígito
    v=v.replace(/(\d{3})(\d)/,"$1.$2")    //Coloca ponto entre o terceiro e o quarto dígitos
    v=v.replace(/(\d{3})(\d)/,"$1.$2")    //Coloca ponto entre o setimo e o oitava dígitos
    v=v.replace(/(\d{3})(\d)/,"$1-$2")   //Coloca ponto entre o decimoprimeiro e o decimosegundo dígitos
    return v
}








function randomiza(n) {
    var ranNum = Math.round(Math.random()*n);
    return ranNum;
}
			
function mod(dividendo,divisor) {
    return Math.round(dividendo - (Math.floor(dividendo/divisor)*divisor));
}
			
function gerarCPF(idCampo) {
    comPontos = false; // TRUE para ativar e FALSE para desativar a pontuação.
    var n = 9;
    var n1 = randomiza(n);
    var n2 = randomiza(n);
    var n3 = randomiza(n);
    var n4 = randomiza(n);
    var n5 = randomiza(n);
    var n6 = randomiza(n);
    var n7 = randomiza(n);
    var n8 = randomiza(n);
    var n9 = randomiza(n);
    var d1 = n9*2+n8*3+n7*4+n6*5+n5*6+n4*7+n3*8+n2*9+n1*10;
    d1 = 11 - ( mod(d1,11) );
    if (d1>=10) d1 = 0;
    var d2 = d1*2+n9*3+n8*4+n7*5+n6*6+n5*7+n4*8+n3*9+n2*10+n1*11;
    d2 = 11 - ( mod(d2,11) );
    if (d2>=10) d2 = 0;
    retorno = '';
    if (comPontos) cpf = ''+n1+n2+n3+'.'+n4+n5+n6+'.'+n7+n8+n9+'-'+d1+d2;
    else cpf = ''+n1+n2+n3+n4+n5+n6+n7+n8+n9+d1+d2;
    alert(cpf);
    document.getElementsByName(idCampo).value = cpf ;
				
}